<?php

/* A PHP function generates the list of translated announcements and links
   from this list, using also a static mapping between the language code
   and full name. */

/* These entries should be translated. .. */
$language_codes = array(
  "af" => "Afrikaans",
  "ar" => "Arabic",
  "as" => "Assamese",
  "ast" => "Asturian",
  "az" => "Azerbaijani",
  "be" => "Belarusian",
  "be@latin" => "Belarusian Latin",
  "bg" => "Bulgarian",
  "bn" => "Bengali",
  "bn_IN" => "Bengali (India)",
  "bo" => "Tibetan",
  "br" => "Breton",
  "bs" => "Bosnian",
  "ca" => "Català",
  "ca@valencia" => "Catalan (Valencian)",
  "crh" => "Crimean Tatar",
  "cs" => "Česky",
  "csb" => "Kashubian",
  "cy" => "Welsh",
  "da" => "Danish",
  "de" => "Deutsch",
  "el" => "Ελληνικά",
  "en_GB" => "British English",
  "en" => "English",
  "eo" => "Esperanto",
  "es" => "Español",
  "et" => "Eesti",
  "eu" => "Euskara",
  "fa" => "Farsi",
  "fi" => "Suomi",
  "fo" => "Faroese",
  "fr" => "Français",
  "fy" => "Frisian",
  "ga" => "Irish Gaelic",
  "gl" => "Galician",
  "gu" => "Gujarati",
  "ha" => "Hausa",
  "he" => "עברית",
  "hi" => "Hindi",
  "hne" => "Chhattisgarhi",
  "hr" => "Croatian",
  "hsb" => "Upper Sorbian",
  "hu" => "Hungarian",
  "hy" => "Armenian",
  "ia" => "Interlingua",
  "id" => "Indonesian",
  "is" => "Icelandic",
  "it" => "Italiano",
  "ja" => "Japanese",
  "ka" => "Georgian",
  "kk" => "Kazakh",
  "km" => "Khmer",
  "kn" => "Kannada",
  "ko" => "한국어",
  "ku" => "Kurdish",
  "lb" => "Luxembourgish",
  "lo" => "Lao",
  "lt" => "Lithuanian",
  "lv" => "Latvian",
  "mai" => "Maithili",
  "mi" => "Maori",
  "ml" => "Malayalam",
  "mk" => "Macedonian",
  "mn" => "Mongolian",
  "mr" => "मराठी",
  "ms" => "Malay",
  "mt" => "Maltese",
  "ne" => "Nepali",
  "nl" => "Nederlands",
  "nb" => "Norwegian Bokmal",
  "nds" => "Low Saxon",
  "nso" => "Northern Sotho",
  "nn" => "Norwegian Nynorsk",
  "oc" => "Occitan",
  "or" => "Oriya",
  "pa" => "Panjabi/Punjabi",
  "ps" => "Pashto",
  "pl" => "Polski",
  "pt" => "Português",
  "pt_BR" => "Português brasileiro",
  "ro" => "Română",
  "ru" => "Русский",
  "rw" => "Kinyarwanda",
  "se" => "Northern Sami",
  "si" => "Sinhala",
  "sk" => "Slovak",
  "sl" => "Slovenščina",
  "sq" => "Albanian",
  "sr" => "Serbian",
  "sr@ijekavian" => "Serbian Ijekavian",
  "sr@ijekavianlatin" => "Serbian Ijekavian Latin",
  "sr@latin" => "Serbian Latin",
  "ss" => "Swati",
  "sv" => "Svenska",
  "ta" => "Tamil",
  "te" => "Telugu",
  "tg" => "Tajik",
  "th" => "Thai",
  "tr" => "Turkish",
  "tt" => "Tatar",
  "ug" => "Uyghur",
  "uk" => "Українська",
  "uz" => "Uzbek",
  "uz@cyrillic" => "Uzbek (Cyrillic)",
  "ven" => "Venda",
  "vi" => "Vietnamese",
  "wa" => "Walloon",
  "xh" => "Xhosa",
  "zh_CN" => "Chinese Simplified",
  "zh_HK" => "Chinese (Hong Kong)",
  "zh_TW" => "Chinese Traditional",
  "zu" => "Zulu",
);

include 'release_data.php';

function print_release_bar($release) {
  global $language_codes;
  global $announcement_langs;
  global $announcement_main_files;

  if (!isset($announcement_main_files[$release])) {
    /* try the fallback to the static list */
    print_release_bar_static($release);
  } else {
    $release_main_file = $announcement_main_files[$release];

    $first_element = True;
    if (!empty($announcement_langs[$release])) {
      echo '<p>';
      print i18n("Also available in:");
      echo '<br /><br />';
    }
    foreach ($announcement_langs[$release] as $rel_lang) {
      if (isset($language_codes[$rel_lang])) {
        $curr_lang = $language_codes[$rel_lang];
      } else {
        /* This should not happen: in case, fallback to the language code */ 
        $curr_lang = $rel_lang;
      }

      $separator_element = ' | ';
      if ($first_element) {
        $first_element = False;
        $separator_element = '';
      }
      echo $separator_element.'<a href="'.$release_main_file.'?site_locale='
           .$rel_lang.'">'.$curr_lang.'</a>';
    }
    if (!empty($announcement_langs[$release])) {
      echo '<br /><br /></p>';
    }
  }
}

function print_release_bar_static($release) {
/* legacy releases, which do not use gettext for translation */

  if ($release == '4.9') {
      // add here...
      echo '<a href="index.php">English</a> | ';
      echo '<a href="index-es.php">Español</a> | ';
      echo '<a href="index-et.php">Eesti</a> | ';
      echo '<a href="index-pt_BR.php">Português brasileiro</a> | ';
      echo '<a href="index-sl.php">Slovenščina</a> | ';
      echo '<a href="index-uk.php">Українська</a> ';
  }
  
  if ($release == '4.8') {
      // add here...
      echo '<a href="index-fa.php">Persian</a>';
  }
  
  if ($release == '4.7') {
      // add here...
      echo '<a href="index-et.php">Eesti</a> | ';
      echo '<a href="index.php">English</a> | ';
      echo '<a href="index-ru.php">Русский</a> | ';
      echo '<a href="index-uk.php">Українська</a> ';
  }
  
  if ($release == '4.6') {
      // add here...
      echo '<a href="index-et.php">Eesti</a> | ';
      echo '<a href="index.php">English</a> | ';
      echo '<a href="index-ru.php">Русский</a> | ';
      echo '<a href="index-uk.php">Українська</a> ';
  }
  
  if ($release == '4.5') {
      // add here...
      echo '<a href="index.php">English</a> | ';
      echo '<a href="index-es.php">Español</a> | ';
      echo '<a href="index-ru.php">Русский</a> | ';
      echo '<a href="index-uk.php">Українська</a> ';
  }
  
  if ($release == '4.4') {
      echo '<a href="index.php">English</a> | ';
      echo '<a href="index-pt_BR.php">Brazilian Portuguese</a> | ';
      echo '<a href="index-es.php">Español</a> | ';
      echo '<a href="index-et.php">Estonian</a> | ';
      echo '<a href="index-nl.php">Dutch</a> | ';
      echo '<a href="index-it.php">Italiano</a> | ';
      echo '<a href="index-sl.php">Slovenian</a> | ';
      echo '<a href="index-sv.php">Svenska</a> | ';
      echo '<a href="index-uk.php">Українська</a> | ';
      echo '<a href="http://fr.kde.org/announcements/4.4/index.php">Français</a> | ';
  }
  
  if ($release == '4.3') {
      echo '<a href="index.php">English</a> | ';
      echo '<a href="index-es.php">Español</a> | ';
      echo '<a href="index-et.php">Estonian</a> | ';
      echo '<a href="http://fr.kde.org/announcements/4.3/">Français</a> | ';
      echo '<a href="index-pt_BR.php">Brazilian Portuguese</a> | ';
      echo '<a href="index-sl.php">Slovenian</a> | ';
      echo '<a href="index-uk.php">Українська</a> | ';
      echo '<a href="index-wa.php">Walloon</a> | ';
  }
  
  if ($release == '4.2') {
      echo '<a href="index.php">English</a> | ';
      echo '<a href="index-pt_BR.php">Brazilian Portuguese</a> | ';
      echo '<a href="index-ca.php">Català</a> | ';
      echo '<a href="http://www.kdecn.org/announcements/4.2/">简体中文</a> | ';
      echo '<a href="index-de.php">Deutsch</a> | ';
      echo '<a href="index-nl.php">Dutch</a> | ';
      echo '<a href="index-es.php">Español</a> | ';
      echo '<a href="http://fr.kde.org/announcements/4.2/">Français</a> | ';
      echo '<a href="index-gu.php">Gujarati</a> | ';
      echo '<a href="index-he.php">עברית</a> | ';
      echo '<a href="index-it.php">Italiano</a> | ';
      echo '<a href="index-ml.php">Malayalam</a> | ';
      echo '<a href="http://www.kde.org.pl/Nowo%C5%9Bci/KDE_4/Wydano_KDE_4.2.0">Polski</a> | ';
      echo '<a href="index-ru.php">Русский</a> | ';
      echo '<a href="index-sl.php">Slovenian</a> | ';
      echo '<a href="index-sv.php">Svenska</a> | ';
      echo '<a href="index-uk.php">Українська</a> | ';
  }
  
  if ($release == '4.2-beta2') {
      echo '<a href="announce-4.2-beta2.php">English</a> | ';
      echo '<a href="announce-4.2-beta2-es.php">Español</a> ';
  }
  
  if ($release === '4.1') {
      echo '<a href="index.php">English</a> | ';
      echo '<a href="index-ca.php">Català</a> | ';
      echo '<a href="index-de.php">Deutsch</a> | ';
      echo '<a href="index-es.php">Español</a> | ';
      echo '<a href="http://fr.kde.org/announcements/4.1/">Français</a> | ';
      echo '<a href="index-it.php">Italiano</a> | ';
      echo '<a href="index-ml.php">Malayalam</a> | ';
      echo '<a href="index-pl.php">Polski</a> | ';
      echo '<a href="index-pt.php">Portuguese</a> | ';
      echo '<a href="index-sl.php">Slovenian</a> | ';
      echo '<a href="index-sv.php">Svenska</a> | ';
      echo '<a href="http://www.kdecn.org/announcements/4.1/index.php">简体中文</a> | ';
      echo '<a href="index-mr.php">मराठी</a>';
  }
  
  if ($release == '4.1-beta2') {
      echo '<a href="announce-4.1-beta2.php">English</a> | ';
      echo '<a href="announce-4.1-beta2-pt_BR.php">Brazilian Portuguese</a> | ';
      echo '<a href="announce-4.1-beta2-de.php">Deutsch</a> | ';
      echo '<a href="announce-4.1-beta2-it.php">Italiano</a> | ';
      echo '<a href="announce-4.1-beta2-sv.php">Svenska</a> ';
  }
  
  if ($release == '4.1-beta1') {
      echo '<a href="announce-4.1-beta1.php">English</a> | ';
      echo '<a href="announce-4.1-beta1-pt_BR.php">Brazilian Portuguese</a> | ';
      echo '<a href="announce-4.1-beta1-de.php">Deutsch</a> | ';
      echo '<a href="announce-4.1-beta1-es.php">Español</a> | ';
      echo '<a href="http://fr.kde.org/announcements/announce-4.1-beta1.php">Français</a> | ';
      echo '<a href="announce-4.1-beta1-it.php">Italiano</a> | ';
      echo '<a href="announce-4.1-beta1-mr.php">मराठी</a> | ';
      echo '<a href="announce-4.1-beta1-sv.php">Svenska</a> ';
  }
}

print_release_bar($release);

?>
