<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Kirigami 1.1");
  $site_root = "../";
  $release = 'kirigami-1.1';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<!-- video
<figure style="float: none">
<iframe style="text-align: center" width="560" height="315" src="https://www.youtube.com/embed/A9MtFqkRFwQ?rel=0" frameborder="0" allowfullscreen></iframe>
</figure>
<br clear="all" />
-->


<p>
<?php i18n("Monday, 26 September 2016. "); ?>
<?php i18n("After weeks of development and two small bugfix releases, we are happy to announce the first Kirigami minor release, version 1.1.");?>
</p>

<p>
<?php i18n("The Menu class features some changes and fixes which give greater control over the action triggered by submenus and leaf nodes in the menu tree. Submenus now know which entry is their parent, and allow the submenu's view to be reset when the application needs it to.");?>
</p>

<p>
<?php i18n("The OverlaySheet now allows to embed ListView and GridView instances in it as well.")?>
</p>

<p>
<?php i18n("The Drawer width now is standardized so all applications look coherent from one another and the title now elides if it doesn't fit. We also introduced the GlobalDrawer.bannerClicked signal to let applications react to banner interaction.");?>
</p>
<p>
<?php i18n("SwipeListItem has been polished to make sure its contents can fit to the space they have and we introduced the Separator component.");?>
</p>

<p>
<?php i18n("A nice fix for desktop Kirigami applications: The application window now has a default shortcut to close the application, depending on the system preferences, commonly Ctrl+Q.")?>
</p>

<p>
<?php i18n("Naturally this release also contains smaller fixes and general polishing.")?>
</p>

<p>
<?php i18n("As announced <a href=\"https://www.kde.org/announcements/plasma-5.7.95.php\">recently</a>, Plasma 5.8 will ship a revamped version of Discover based on Kirigami. As such we expect that all major distributions will ship Kirigami packages by then if they are not already doing so.");?>
</p>

<h2><?php i18n("Source Download");?></h2>

<p><?php i18n("The source code for Kirigami releases is available for download from <a href='http://download.kde.org/stable/kirigami/'>download.kde.org</a>.");?></p>

<p><?php i18n("More information is on the <a href='https://techbase.kde.org/Kirigami'>Kirigami wiki page</a>.");?></p>

<p><?php i18n("GPG signatures are available alongside the source code for
verification. They are signed by release manager <a
href='https://sks-keyservers.net/pks/lookup?op=vindex&search=0xeaaf29b42a678c20'>Marco Martin with 0xeaaf29b42a678c20</a>.");?></p>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. ");?>
 </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
