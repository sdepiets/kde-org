<?php
  $page_title = "KDE 2.0 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>
<P>DATELINE OCTOBER 23, 2000</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">New KDE Release Is a Major Advance for Linux<SUP>&reg;</SUP> Desktop</H3>
<P><STRONG>Next Generation of Leading Desktop for Linux<SUP>&reg;</SUP>
and Other UNIXes<SUP>&reg;</SUP> Ships</STRONG></P>
<P>October 23, 2000 (The INTERNET).  The <A HREF="/">KDE
Team</A> today announced the release of KDE 2.0
KDE's powerful, modular, Internet-enabled desktop.  This highly anticipated
release constitutes the next generation of the
<A HREF="../awards">award-winning</A> KDE 1
series, which culminated in the
<A HREF="announce-1.1.2.php">release of
KDE 1.1.2</A> just over a year ago.  KDE 2.0 is the work product
of hundreds of dedicated developers originating from over 30 countries.
</P>
<P>
"With the experience gained from developing KDE 1, we
almost completely re-engineered KDE 2 to make it even more intuitive,
powerful and user friendly," explained
<A HREF="&#109;&#0097;&#x69;lt&#0111;:et&#x74;r&#0105;c&#x68;&#0064;k&#x64;&#x65;&#46;&#x6f;&#x72;g">Matthias Ettrich</A>,
founder of the KDE project.
"We think that current KDE users will be pleasantly surprised with the
remarkable improvements we have achieved.  KDE 2 offers
the desktop user the benefit of standards compliance and an array of
new technologies, from
<A HREF="#Konqueror">Konqueror</A>, a full featured web browser and
file manager, to <A HREF="#KOffice">KOffice</A>, an integrated
office suite, as well as a slew of usability enhancements, such as KDE's
expanded themeability and configurability and a new KDE Help Center.
It also offers developers an assortment of powerful new tools -- from
<A HREF="#KParts">KParts</A>, KDE's component object technology,
to <A HREF="#KIO">KIO</A>, KDE's network transparent I/O architecture
-- for rapid development and deployment of first-rate free or
proprietary software."
</P>
<P>
"KDE 2.0 is an important release," stated Ransom Love, president and
CEO of <A HREF="http://www.caldera.com/">Caldera Systems, Inc.</A>
"Our customers are anxious to migrate not only their servers but also
their desktops to the Linux technology.  KDE 2.0 will be a key component
of OpenLinux eDesktop, our solution for a seamless and cost-effective
transition strategy."
</P>
<P>
"SuSE Linux views KDE 2 as one of
the key milestones to vault Linux to the same landslide success on the
desktop that it already has in the server space," added Dirk Hohndel,
CTO of <A HREF="http://www.suse.com/">Suse AG</A>.  "We are excited to
be able to offer KDE 2.0 as the
default desktop with our next version of the SuSE Linux OS.  I am
confident that third party developers will realize the enormous
potential KDE 2 offers and will migrate their applications
to Linux/KDE."
</P>
<P>
"As Linux-Mandrake focuses on making Linux easy to use, we are very
pleased to include KDE 2, a major evolution of the already superb KDE 1,
in our upcoming Linux-Mandrake 7.2 release", added
<A HREF="mailto:gduval@mandrakesoft.com">Ga�l Duval</A>,
co-founder of <A HREF="http://www.linux-mandrake.com/">Mandrakesoft</A>.
"With KDE 2 and KOffice, the KDE team demonstrates again their deep
commitment to make Linux a viable desktop alternative for all users."
</P>
<P>
"Corel has had a long, successful relationship with the KDE project, and
the release of KDE 2.0 is an important milestone for Linux," said Rene
Schmidt, Executive Vice President for Linux Products,
<A HREF="http://linux.corel.com/">Corel Corporation</A>.
"We believe that our customers will be ecstatic over the improvements
and new features of this landmark version. The enhancements to the
framework provide power for the desktop in the simple and elegant
fashion that our customers have grown to expect."
</P>
<P>
KDE 2.0 includes the core KDE libraries,
the core desktop environment, the initial release of the KOffice suite, as well
as the over 100 applications from the other standard base KDE packages: 
Administration, Games,
Graphics, Multimedia, Network, Personal Information Management (PIM),
Toys and Utilities.  KDE 2.0 is currently available in 15 languages
and translations into 20 additional languages will be available in the
coming weeks.
</P>
<P>
All of KDE 2.0 is available for free under an Open Source license. 
Likewise,
<A HREF="http://www.trolltech.com/">Trolltech's</A><SUP>tm</SUP>
Qt<SUP>&reg;</SUP> 2.2.1, the GUI toolkit on which KDE is based,
is now available for free under two Open Source licenses:  the
<A HREF="http://www.trolltech.com/products/download/freelicense/license.html">Q
Public License</A> and the <A HREF="http://www.gnu.org/copyleft/gpl.html">GNU
General Public License</A>.
</P>
<P>
More information about KDE 2 is available in a
<A HREF="http://devel-home.kde.org/~granroth/LWE2000/index.html">slideshow
presentation</A> and on
<A HREF="../">KDE's web site</A>, including an evolving
<A HREF="../info/faq-2.x.php">FAQ</A> to answer questions about
migrating to KDE 2.0 from KDE 1.x, a number of
<A HREF="../screenshots/kde2shots.php">screenshots</A>, <A HREF="http://developer.kde.org/documentation/kde2arch.html">developer information</A> and
a developer's
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html?rev=2.3">KDE 1 - KDE 2 porting guide</A>.
</P>
<P>
<STRONG><EM>KDE 2:  The K Desktop Environment</EM></STRONG>.
<A NAME="Konqueror"></A><A HREF="http://konqueror.kde.org/">Konqueror</A>
is KDE 2's next-generation web browser,
file manager and document viewer.  Widely heralded as a
technological break-through for the Linux desktop, the standards-compliant
Konqueror has a component-based architecture which combines the features and
functionality of Internet Explorer<SUP>&reg;</SUP>/Netscape
Communicator<SUP>&reg;</SUP> and Windows Explorer<SUP>&reg;</SUP>.
Konqueror will support the full gamut of current Internet technologies,
including JavaScript, Java<SUP>&reg;</SUP>, HTML 4.0, CSS-1 and -2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator<SUP>&reg;</SUP> plug-ins (for
playing Flash<SUP>TM</SUP>, RealAudio<SUP>TM</SUP>, RealVideo<SUP>TM</SUP>
and similar technologies).  The great bulk of this technology is already
in place and functional for KDE 2.0.
</P>
<P>
KDE 2 also ships with the highly anticipated initial release of
<A NAME="KOffice"></A>the <A HREF="http://www.koffice.org/">KOffice
suite</A>.  The integrated
suite consists of a spreadsheet application (KSpread), a vector drawing
application (KIllustrator), a frame-based word-processing application
(KWord), a presentation program
(KPresenter), and a chart and diagram application (KChart).  Native file
formats are XML-based, and work on filters for proprietary binary file
formats is progressing.  Combined with a powerful scripting language and the
ability to embed individuals components within each other using KDE's
component technology (KParts), the free KOffice suite will soon provide
all the necessary functionality to all but the most demanding power users.
</P>
<P>
In addition, <A NAME="KIO">KIO's</A> network transparency offers
seamless support for accessing
or browsing files on Linux, NFS shares, MS Windows<SUP>&reg;</SUP>
SMB shares, HTTP pages, FTP directories and LDAP directories.  The modular,
plug-in nature of KDE's file architecture makes it simple to add additional
protocols (such as IPX or WebDAV) to KDE, which would then automatically be
available to all KDE applications.
</P>
<P>
KDE 2 introduces a new multimedia architecture based on <A
NAME="arts">aRts</A>, the Analog Realtime Synthesizer. ARts enables
playing multiple audio or video streams concurrently, whether on the
desktop or over a network.  ARts is a full-featured sound system, and
includes filters, a modular analog synthesizer and a mixer.  Its
architecture allows developers to create additional filter plugins and
users to apply sequences of filters using a graphical drag-n-drop
approach.  Video support is <a
href="http://mpeglib.sourceforge.net/">available</a> for MPEG versions
1, 2 and 4 (experimental), as well as the AVI and DivX formats.
</P>
<P>
KDE's <A NAME="customizability">customizability</A> touches every
aspect of this next-generation
desktop.  KDE's sophisticated theme support starts with Qt's
style engine, which permits developers and artists to create their
own widget designs.  KDE 2.0 ships with over 14 of these styles,
some of which emulate the look of various operating systems, and additionally
does an excellent job of
<A HREF="ftp://ftp.kde.org/pub/kde/Incoming/gnome.png">importing themes</A>
from GTK and GNOME.  Other configuration options permit users to:  choose among
icon themes and system sounds (using a simple drop-and-replace approach);
configure key bindings; select from over 30 languages; customize toolbar
layouts and entries and menu composition; employ single-click or double-click
to activate desktop items; navigate the desktop using a keyboard
instead of a mouse; and much, much more.  Moreover, KDE 2 fully
supports Unicode and KHTML is the only free HTML rendering engine on
Linux/X11 that features nascent support for BiDi scripts
such as Arabic and Hebrew.
</P>
<P>
Besides the exceptional compliance with Internet and file-sharing standards
<A HREF="#Konqueror">mentioned above</A>, KDE 2 achieves exceptional
compliance with the available Linux desktop standards.  KWin, KDE's new
re-engineered window manager, complies to the new
<A HREF="http://www.freedesktop.org/standards/wm-spec.html">Window Manager
Specification</A>.  Konqueror and KDE comply to the <A
HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/kio/DESKTOP_ENTRY_STANDARD?rev=1.9">Desktop
Entry Standard</A>.  KDE 2 generally complies with the
<A HREF="http://www.newplanetsoftware.com/xdnd/">X Drag-and-Drop (XDND)
protocol</A> as well as with the
<A HREF="http://www.rzg.mpg.de/rzg/batch/NEC/sx4a_doc/g1ae04e/chap12.html">X11R6 session management protocol (XSMP)</A>.
</P>
<P>
<STRONG><EM>KDE 2:  The K Development Environment</EM></STRONG>.
KDE 2.0 offers developers a rich set of major technological improvements over
the critically acclaimed KDE 1 series.  Chief among these are
the <A HREF="#DCOP">Desktop COmmunication Protocol (DCOP)</A>, the
<A HREF="#KIO">I/O libraries (KIO)</A>, <A HREF="#KParts">the component
object model (KParts)</A>, an <A HREF="#XMLGUI">XML-based GUI class</A>, and 
a <A HREF="#KHTML">standards-compliant HTML rendering engine (KHTML)</A>.
</P>
<P>
<A NAME="DCOP">DCOP</A> is a client-to-client communications
protocol intermediated by a
server over the standard X11 ICE library.  The protocol supports both
message passing and remote procedure calls using an XML-RPC to DCOP "gateway".
Bindings for C, C++ and Python, as well as experimental Java bindings, are
available.
</P>
<P>
<A NAME="KIO-devel">KIO</A> implements application I/O in a separate
process to enable a
non-blocking GUI without the use of threads.  The class is network transparent
and hence can be used seamlessly to access HTTP, FTP, POP, IMAP,
NFS, SMB, LDAP and local files.  Moreover, its modular
and extensible design permits developers to "drop in" additional protocols,
such as WebDAV, which will then automatically be available to all KDE
applications.  KIO also implements a trader which can locate handlers
for specified mimetypes; these handlers can then be embedded within
the requesting application using the KParts technology.
</P>
<P>
<A NAME="KParts">KParts</A>, KDE 2's component object model, allows
an application to embed another within itself.  The technology handles
all aspects of the embedding, such as positioning toolbars and inserting
the proper menus when the embedded component is activated or deactivated.
KParts can also interface with the KIO trader to locate available handlers for
specific mimetypes or services/protocols.
This technology is used extensively by the KOffice suite and
Konqueror.
</P>
<P>
The <A NAME="XMLGUI">XML GUI</A> employs XML to create and position
menus, toolbars and possibly
other aspects of the GUI. This technology offers developers and users
the advantage of simplified configurability of these user interface elements
across applications and automatic compliance with the
<A HREF="http://developer.kde.org/documentation/standards/">KDE Standards
and Style Guide</A> irrespective of modifications to the standards.
</P>
<P>
<A NAME="KHTML">KHTML</A> is an HTML 4.0 compliant rendering
and drawing engine.  The class
will support the full gamut of current Internet technologies, including
JavaScript<SUP>TM</SUP>, Java<SUP>&reg;</SUP>, HTML 4.0, CSS-2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator<SUP>&reg;</SUP> plugins (for
viewing Flash<SUP>TM</SUP>,
RealAudio<SUP>TM</SUP>, RealVideo<SUP>TM</SUP> and similar technologies).
The KHTML class can easily
be used by an application as either a widget (using normal X Window
parenting) or as a component (using the KParts technology).
KHTML, in turn, has the capacity to embed components within itself
using the KParts technology.
</P>
<H4>Downloading and Compiling KDE 2.0</H4>
<P>
The source packages for KDE 2.0 are available for free download at
<A HREF="http://ftp.kde.org/stable/2.0/distribution/tar/generic/src/">http://ftp.kde.org/stable/2.0/distribution/tar/generic/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<A HREF="../mirrors/ftp.php">mirrors</A>.  KDE 2.0 requires
qt-2.2.1, which is available from the above locations under the name
<A HREF="http://ftp.kde.org/stable/2.0/distribution/tar/generic/src/qt-x11-2.2.1.tar.gz">qt-x11-2.2.1.tar.gz</A>.  KDE 2.0 will not work with
older versions of Qt.
</P>
<P>
For further instructions on compiling and installing KDE 2.0, please consult
the <A HREF="http://developer.kde.org/build/index.html">installation
instructions</A> and, if you encounter problems, the
<A HREF="http://developer.kde.org/build/index.html">compilation FAQ</A>.
</P>
<H4>Installing Binary Packages</H4>
<P>
Some distributors choose to provide binary packages of KDE for certain
versions of their distribution.  Some of these binary packages for KDE
2.0
will be available for free download under
<A HREF="http://ftp.kde.org/stable/2.0/distribution/">http://ftp.kde.org/stable/2.0/distribution/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="../mirrors/ftp.php">mirrors</A>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.
</P>
<P>KDE 2.0 requires qt-2.2.1, the free version of which is available
from the above locations usually under the name qt-x11-2.2.1.  KDE 2.0
will not work with older versions of Qt.
<P>
At the time of this release, pre-compiled packages are available for:
</P>
<UL>
<LI><A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/COL-2.4/">Caldera OpenLinux 2.4</A></LI>
<LI><A HREF="http://ftp.kde.org/stable/2.0/distribution/deb/Debian/dists/potato/">Debian GNU/Linux 2.2 (potato)</A> and <A HREF="http://ftp.kde.org/stable/2.0/distribution/deb/Debian/dists/woody/">Debian GNU/Linux Devel (woody)</A></LI>
<LI><A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/Mandrake/">Linux-Mandrake 7.2</A></LI>
<!--
PPC has compiler problems, will release later

<LI><A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/ppc-glibc21/">LinuxPPC (glibc 2.1)</A></LI>
-->
<LI><A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/RedHat/rh7/">RedHat Linux 7.0</A>
<!-- TO COME LATER
and <A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/RedHat/rh6.2/">RedHat Linux 6.2</A>
-->
</LI>
<LI><A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/6.4-i386/">SuSE Linux 6.4 (i386)</A>, <A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/7.0-i386/">SuSE Linux 7.0 (i386)</A>, <A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/7.0-ppc/">SuSE Linux 7.0 (ppc)</A> and <A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/7.0-sparc/">SuSE Linux 7.0 (sparc)</A></LI>
<LI><A HREF="http://ftp.kde.org/stable/2.0/distribution/tar/True64/">Tru64 Systems</A></LI>
</UL>
<P>
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages will become available over the
coming days and weeks.
</P>
<H4>About KDE</H4>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.
</P>
<P>
For more information about KDE, please visit KDE's
<A HREF="/">web site</A>.
</P>
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
g&#114;a&#x6e;&#114;&#x6f;th&#x40;&#x6b;de.&#00111;&#114;g<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
&#x70;ou&#x72;&#064;&#x6b;d&#101;&#00046;&#111;&#x72;g<BR>
(1) 718 456 1165
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
faur&#101;&#0064;&#107;d&#x65;.or&#x67;<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
k&#0111;no&#108;&#100;&#064;&#107;&#100;&#x65;&#46;o&#x72;&#103;<BR>
(49) 179 2252249
</TD></TR>
</TABLE>

<?php include "footer.inc" ?>
