Announcing KDE 3.4

The KDE Project ships a new major release of their leading Open Source desktop
environment.

March 16, 2005 (The Internet) - After more than a half year of development the
KDE Project is happy to be able to announce a new major release of the
award-winning K Desktop Environment. Among the many new features that have been
incorporated, the improvements in accessibility are most remarkable. 

One of the milestones in this new release will be the advanced KDE
Text-to-speech framework. It integrates into KDE's PDF-viewer, editor,
webbrowser and into the new speaker-tool KSayIt. It also allows to read out
notifications from all KDE applications. Especially partially-sighted people and
speech-impaired users will benefit, but it should also prove a fun desktop
experience overall.

For people with low vision, several high contrast themes including a complete
monochrome icon set have been added. Other accessibility applications have been
improved. KMouseTool which can click the mouse for people with, for example,
carpal tunnel syndrome or tendinitis; KMouth to allow the computer to speak for
the speech impaired; and KMagnifier to magnify sections of screen for
partially-sighted users. Standard accessibility features including "Sticky
Keys", "Slow Keys" and "Bounce Keys" are also available and are now more easily
accessed via keyboard gestures. All of these features combine to open the world
of computing to a much wider audience and to a section of the population that is
often overlooked. The KDE project will continue its close cooperation with the
accessibility community to reach even more people in the future. 

Another milestone will be the improvements of KDE's personal information
management suite Kontact and of KDE's instant messenger Kopete. Kontact
has improved usability including a new message composer and start 
screen, and its support for the free software groupware solution Kolab has 
been updated to Kolab 2.0. This means that KDE has now a complete groupware 
solution including an open-source server interoperable with proprietary MS 
Windows Outlook clients. Other supported groupware servers include 
eGroupware, GroupWise, OpenGroupware.org and SLOX. Kopete features an improved
contact list showing contact photos, improved Kontact integration and supports
AIM, Gadu-Gadu, GroupWise, ICQ, IRC, Jabber, Lotus Sametime, MSN, Yahoo, and
the sending of SMS.

With KDE being based on an international community there are more than 49
translations available and even more to be expected for future service packs of
KDE 3.4. This is why KDE serves best the needs of today's world wide Linux
community.

KDE 3.4 is available for free under Open Source licenses and boasts eighteen
packages of optional applications including accessibility, development, games,
PIM, network, utilities, administration, edutainment, multimedia, graphics and
more.


Reactions from the accessibility community

"With each new release, KDE continues to enhance its support for people with
disabilities", Janina Sajka, chair of the Accessibility Workgroup of the Free
Standards Group, said. "This is making KDE more and more attractive to more
persons with disabilities. And, it's also helping KDE meet various social
inclusion objectives worldwide, such as the Sec. 508 requirements of the U.S.
Government." 

Lars Stetten from the Accessibility User Group Linaccess said about the release:
"The new accessibility features in KDE 3.4 are an important step for the future,
to enable disabled people to get to know the KDE desktop and to join its
community." 


Highlights at a glance

* Text-to-speech system with support built into Konqueror, Kate, KPDF and the standalone application KSayIt

* Support for text to speech synthesis is integrated with the desktop

* Completely redesigned, more flexible trash system

* Kicker with improved look and feel

* KPDF now enables you to select, copy & paste text and images from PDFs, along with many other improvements

* Kontact supports now various groupware servers, including eGroupware, GroupWise, Kolab, OpenGroupware.org and SLOX

* Kopete supports Novell Groupwise and Lotus Sametime and gets integrated into Kontact

* DBUS/HAL support allows to keep dynamic device icons in media:/ and on the desktop in sync with the state of all devices

* KHTML has improved standard support and now close to full support for CSS 2.1 and the CSS 3 Selectors module

* Better synchronization between 2 PCs

* A new high contrast style and a complete monochrome icon set

* An icon effect to paint all icons in two chosen colors, converting third party application icons into high contrast monochrome icons

* Akregator allows you to read news from your favourite RSS-enabled websites in one application

* Juk has now an album cover management via Google Image Search

* KMail now stores passwords securely with KWallet

* SVG files can now be used as wallpapers

* KHTML plug-ins are now configurable, so the user can selectively disable ones that are not used. This does not include Netscape-style plug-ins. Netscape plug-in in CPU usage can be manually lowered, and plug-ins are more stable.

* more than 6,500 bugs have been fixed

* more than 1,700 wishes have been fullfilled

* more than 80,000 contributions with several million lines of code and documentation added or changed


Getting KDE 3.4

Full information on how to download and install KDE 3.4 is available on our
official website at http://www.kde.org/info. Being free and open source
software, it is available for download at no cost. If you use a major Linux
distribution then precompiled packages may be available from your distributions
website or from http://download.kde.org. The source code can also be downloaded
from there. Both ArkLinux and Kubuntu have targeted a release including KDE 3.4
right after the 3.4 release. If you prefer to build KDE from source you should
consider using Konstruct, a tool that automatically downloads, configures and
builds KDE 3.4 for you. 

Many more KDE applications are freely available from KDE-Apps.org and different
look and feel improvents can be downloaded from KDE-Look.org. 


Supporting KDE

KDE is an open source project that exists and grows only because of the help of
many volunteers that donate their time and effort. KDE is always looking for new
volunteers and contributions, whether its help with coding, bug fixing or
reporting, writing documentation, translations, promotion, money, etc. All
contributions are gratefully appreciated and eagerly accepted. Please read
through the Supporting KDE page for further information. 
We look forward to hearing from you soon! 


About KDE 

KDE is an independent project of hundreds of developers, translators, artists
and other professionals worldwide collaborating over the Internet to create and
freely distribute a sophisticated, customizable and stable desktop and office
environment employing a flexible, component-based, network-transparent
architecture and offering an outstanding development platform. KDE provides a
stable, mature desktop, a full, component-based office suite (KOffice), a large
set of networking and administration tools and utilities, and an efficient,
intuitive development environment featuring the excellent IDE KDevelop. KDE is
working proof that the Open Source "Bazaar-style" software development model can
yield first-rate technologies on par with and superior to even the most complex
commercial software. 

Trademark Notices. KDE and K Desktop Environment are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds. UNIX is a registered
trademark of The Open Group in the United States and other countries. All other
trademarks and copyrights referred to in this announcement are the property of
their respective owners. 


Press Contacts

Africa
 Uwe Thiem
 P.P.Box 30955
 Windhoek
 Namibia
 Phone: +264 - 61 - 24 92 49
 info-africa kde.org
 
Asia
 Sirtaj S. Kang 
 C-324 Defence Colony 
 New Delhi 
 India 110024 
 Phone: +91-981807-8372 
 info-asia kde.org 

Europe
 Matthias Kalle Dalheimer
 Rysktorp
 S-683 92 Hagfors
 Sweden
 Phone: +46-563-540023
 Fax: +46-563-540028
 info-europe kde.org 

North America
 George Staikos 
 889 Bay St. #205 
 Toronto, ON, M5S 3K5 
 Canada
 Phone: (416)-925-4030 
 info-northamerica kde.org

Oceania
 Hamish Rodda
 11 Eucalyptus Road
 Eltham VIC 3095
 Australia
 Phone: (+61)402 346684
 info-oceania kde.org

South America
 Helio Chissini de Castro
 R. José de Alencar 120, apto 1906
 Curitiba, PR 80050-240
 Brazil
 Phone: +55(41)262-0782 / +55(41)360-2670
 info-southamerica kde.org
 
