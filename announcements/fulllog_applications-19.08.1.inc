<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Fix Bug 403208 - Short name of favorite folder results in improperly shown number of unread messages. <a href='http://commits.kde.org/akonadi/bebfedd536e62acf859b825a093a4eddcf7721f2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403208'>#403208</a></li>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/akonadi/b229d5ad0fc91a54e954d5131e1d6d3a3be6e49c'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-calendar' href='https://cgit.kde.org/akonadi-calendar.git'>akonadi-calendar</a> <a href='#akonadi-calendar' onclick='toggle("ulakonadi-calendar", this)'>[Hide]</a></h3>
<ul id='ulakonadi-calendar' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/akonadi-calendar/e24f589bd52cdf65fe22b31e7a9360892b85f2a0'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-contacts' href='https://cgit.kde.org/akonadi-contacts.git'>akonadi-contacts</a> <a href='#akonadi-contacts' onclick='toggle("ulakonadi-contacts", this)'>[Hide]</a></h3>
<ul id='ulakonadi-contacts' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/akonadi-contacts/c8b837fc1b1f1031e67c8d40f4e2122af565678f'>Commit.</a> </li>
<li>Hide label when we don't have plugins. <a href='http://commits.kde.org/akonadi-contacts/547c1a5d3d1e1ff404c9fb1aa65880bf81ab77b0'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-mime' href='https://cgit.kde.org/akonadi-mime.git'>akonadi-mime</a> <a href='#akonadi-mime' onclick='toggle("ulakonadi-mime", this)'>[Hide]</a></h3>
<ul id='ulakonadi-mime' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/akonadi-mime/7b87d4cf89e5f9985b004855ecc39f8ac6793f66'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-notes' href='https://cgit.kde.org/akonadi-notes.git'>akonadi-notes</a> <a href='#akonadi-notes' onclick='toggle("ulakonadi-notes", this)'>[Hide]</a></h3>
<ul id='ulakonadi-notes' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/akonadi-notes/99f5e2940c0a010268be828313d34ebfe3f1015c'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Fall back to nummerical owner and group if no text representation exists. <a href='http://commits.kde.org/ark/462adefebae543343bc7b67a21d4160f444e0bc7'>Commit.</a> </li>
</ul>
<h3><a name='audiocd-kio' href='https://cgit.kde.org/audiocd-kio.git'>audiocd-kio</a> <a href='#audiocd-kio' onclick='toggle("ulaudiocd-kio", this)'>[Hide]</a></h3>
<ul id='ulaudiocd-kio' style='display: block'>
<li>Add missing ShowPreviews key to audiocd.json. <a href='http://commits.kde.org/audiocd-kio/1169ad1fa6bffbf947ca61bfa5d5830afae601ef'>Commit.</a> </li>
</ul>
<h3><a name='calendarsupport' href='https://cgit.kde.org/calendarsupport.git'>calendarsupport</a> <a href='#calendarsupport' onclick='toggle("ulcalendarsupport", this)'>[Hide]</a></h3>
<ul id='ulcalendarsupport' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/calendarsupport/a8ebd6fc52d59a8ff7bf7c99b95f905fe6810ce3'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Fixing bugs in new folders in tabs feature. <a href='http://commits.kde.org/dolphin/a6403716439ae72bfdf86a14af94ea9d212f08ee'>Commit.</a> </li>
<li>Making sure that DBus objects are all created before the service. <a href='http://commits.kde.org/dolphin/56b3059f774ae5917185d91a380c9f0a95e7584f'>Commit.</a> </li>
<li>Fix places text when the URL has a trailing slash. <a href='http://commits.kde.org/dolphin/c5a713763daf113c78697341bfea7dc0860192a3'>Commit.</a> </li>
<li>Reset progress bar text when directory loading starts. <a href='http://commits.kde.org/dolphin/ada16756d69c012c089733f6ce4e9271f84ccca6'>Commit.</a> </li>
<li>Fixing bug where split view opens with no URLs. <a href='http://commits.kde.org/dolphin/91709cb6fe61dbec300a8d51d3064a57d9f63f37'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411193'>#411193</a></li>
</ul>
<h3><a name='eventviews' href='https://cgit.kde.org/eventviews.git'>eventviews</a> <a href='#eventviews' onclick='toggle("uleventviews", this)'>[Hide]</a></h3>
<ul id='uleventviews' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/eventviews/db66e1d94599f663294c33c74c4ff4b766783427'>Commit.</a> </li>
</ul>
<h3><a name='grantleetheme' href='https://cgit.kde.org/grantleetheme.git'>grantleetheme</a> <a href='#grantleetheme' onclick='toggle("ulgrantleetheme", this)'>[Hide]</a></h3>
<ul id='ulgrantleetheme' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/grantleetheme/d75e1db71fb39531f3e6138f9916b0698847a454'>Commit.</a> </li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Hide]</a></h3>
<ul id='ulincidenceeditor' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/incidenceeditor/7f8cbd10f92d92e22ff142d99b88a5518d9bc597'>Commit.</a> </li>
<li>Fix Bug 411137 - Esc key closes the editor without saving. <a href='http://commits.kde.org/incidenceeditor/f21fc4afeb69a39bcedc6ea0a8ee17d8e3e77a70'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411137'>#411137</a></li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>Speedup JuK restarts by fixing bug preventing collection data from being saved on shutdown. <a href='http://commits.kde.org/juk/d4dc1bee178b0361069d6d94e463368084d0e8d4'>Commit.</a> </li>
</ul>
<h3><a name='k3b' href='https://cgit.kde.org/k3b.git'>k3b</a> <a href='#k3b' onclick='toggle("ulk3b", this)'>[Hide]</a></h3>
<ul id='ulk3b' style='display: block'>
<li>Fix taglib includes to match the way taglib was meant to be used: #include <tag.h>. <a href='http://commits.kde.org/k3b/449b42e51d5b05bf276b9b5520bb092bc6024914'>Commit.</a> </li>
</ul>
<h3><a name='kaccounts-providers' href='https://cgit.kde.org/kaccounts-providers.git'>kaccounts-providers</a> <a href='#kaccounts-providers' onclick='toggle("ulkaccounts-providers", this)'>[Hide]</a></h3>
<ul id='ulkaccounts-providers' style='display: block'>
<li>Fix build with Qt 5.13. <a href='http://commits.kde.org/kaccounts-providers/fd6b3ebfa73782de44f952f95394a6cbccd74ca4'>Commit.</a> </li>
</ul>
<h3><a name='kajongg' href='https://cgit.kde.org/kajongg.git'>kajongg</a> <a href='#kajongg' onclick='toggle("ulkajongg", this)'>[Hide]</a></h3>
<ul id='ulkajongg' style='display: block'>
<li>D23219: Fix jenkins build of kajongg by allowing spaces in folder/file names. <a href='http://commits.kde.org/kajongg/d8dafc9b991357b9b017a37145d366cd7e2c1a5c'>Commit.</a> </li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Make debug output clearer. <a href='http://commits.kde.org/kalarm/6095bb2eb2c518de0b7ee8e8e521cbd1918548d2'>Commit.</a> </li>
<li>Fix command line options which don't work if KAlarm not already running. <a href='http://commits.kde.org/kalarm/cc783e686d4c893e11f9310c615f322c8ecab3d7'>Commit.</a> </li>
<li>Output warning message if can't create alarm due to no default calendar. <a href='http://commits.kde.org/kalarm/74fe4cc36876a064356423cee8366f6d5aead9ec'>Commit.</a> </li>
<li>Bug 411296: Fix D-Bus alarm creation failing if time zone is omitted. <a href='http://commits.kde.org/kalarm/bfee8785549b17201fb139bb72a9dd20a117cb4b'>Commit.</a> </li>
<li>Update version number. <a href='http://commits.kde.org/kalarm/033687f1e0d18e6e15dd1fed735caa46c2e1a99d'>Commit.</a> </li>
<li>Update change log. <a href='http://commits.kde.org/kalarm/b8861aa59c4512436f2a3a87dbd87ed85d9f1cd8'>Commit.</a> </li>
<li>Bug 410596: Don't leave dangling pointers in AlarmCalendar when removeKAEvents doesn't remove the whole collection. <a href='http://commits.kde.org/kalarm/1cefdb07c558721dea7a90a2216b61d21425a8d5'>Commit.</a> </li>
<li>KAlarm: simplify the starting of KOrganizer to use DBus activation. <a href='http://commits.kde.org/kalarm/285946e2a895643f1d06111a86e6f01809229fb2'>Commit.</a> </li>
</ul>
<h3><a name='kalarmcal' href='https://cgit.kde.org/kalarmcal.git'>kalarmcal</a> <a href='#kalarmcal' onclick='toggle("ulkalarmcal", this)'>[Hide]</a></h3>
<ul id='ulkalarmcal' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kalarmcal/f04527f66d9ffb59767844361d163743111717e1'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>Fix stop button of search and replace plugin. <a href='http://commits.kde.org/kate/5a2c14c3e0b633f6887775bcfce1c1f87e1a06c0'>Commit.</a> </li>
</ul>
<h3><a name='kblog' href='https://cgit.kde.org/kblog.git'>kblog</a> <a href='#kblog' onclick='toggle("ulkblog", this)'>[Hide]</a></h3>
<ul id='ulkblog' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kblog/4566ff3e086039fe4fbba0fc31c4bbd41e3e7f6d'>Commit.</a> </li>
</ul>
<h3><a name='kcalcore' href='https://cgit.kde.org/kcalcore.git'>kcalcore</a> <a href='#kcalcore' onclick='toggle("ulkcalcore", this)'>[Hide]</a></h3>
<ul id='ulkcalcore' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kcalcore/530036c0a1ac58a19aedbab2989021bfa6be1cc6'>Commit.</a> </li>
<li>Restore backward compatibility for the version header. <a href='http://commits.kde.org/kcalcore/28eab632238352ab46f6c1db11f6ebf7d3ce0690'>Commit.</a> </li>
</ul>
<h3><a name='kcalutils' href='https://cgit.kde.org/kcalutils.git'>kcalutils</a> <a href='#kcalutils' onclick='toggle("ulkcalutils", this)'>[Hide]</a></h3>
<ul id='ulkcalutils' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kcalutils/029abc99794ac2bcda1d9086bf287aa84e945da7'>Commit.</a> </li>
</ul>
<h3><a name='kcontacts' href='https://cgit.kde.org/kcontacts.git'>kcontacts</a> <a href='#kcontacts' onclick='toggle("ulkcontacts", this)'>[Hide]</a></h3>
<ul id='ulkcontacts' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kcontacts/2dd0d671001ac95230b832c7d9d29df6abec32aa'>Commit.</a> </li>
</ul>
<h3><a name='kdav' href='https://cgit.kde.org/kdav.git'>kdav</a> <a href='#kdav' onclick='toggle("ulkdav", this)'>[Hide]</a></h3>
<ul id='ulkdav' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kdav/ba0c76c8d55687d1441b1d593c254ea662a0139b'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Fix disabling clip only disable audio part of an AV clip. <a href='http://commits.kde.org/kdenlive/7c915ded1c85862293e7408d63f4b06895f98287'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411466'>#411466</a></li>
<li>Fix regression breaking timeline resize. <a href='http://commits.kde.org/kdenlive/9a51d703cd15a020aac35cbe6492f120305d65bc'>Commit.</a> </li>
<li>Fix timelinekeyboard focus on start and grab not correctly ended. <a href='http://commits.kde.org/kdenlive/853bbd53c8a6d72822526b4a757e667919df6b5f'>Commit.</a> </li>
<li>Default effects to video. <a href='http://commits.kde.org/kdenlive/9b1a25ec9905d3702c3ad5f20975eb89f820ec01'>Commit.</a> </li>
<li>Fix disabling autoscroll. <a href='http://commits.kde.org/kdenlive/1444cbd35987a57956330ab96a340b936490f89e'>Commit.</a> </li>
<li>Convert old custom effects to new customAudio/Video naming. <a href='http://commits.kde.org/kdenlive/4d0c9d43fb5e7ecffbf008763b4d30169e41db0e'>Commit.</a> </li>
<li>Fix group move sometimes moving clip very far from expected location. <a href='http://commits.kde.org/kdenlive/90d5443c7a0bed99fa1dc5a33e668074ce112bc9'>Commit.</a> </li>
<li>Ctrl resize in monitor effects keeps center position. <a href='http://commits.kde.org/kdenlive/60a1ba38540ce0a46bb700cc97a0f4c4cda02800'>Commit.</a> </li>
<li>Shift resize in monitor effect keeps aspect ratio. <a href='http://commits.kde.org/kdenlive/0e18e35c3cb1b9d6e5d748e1954a34a1b24b12fd'>Commit.</a> </li>
<li>Update appdata version. <a href='http://commits.kde.org/kdenlive/b603fa9ed8be9c0f82baaf40b10fb0691e65fed9'>Commit.</a> </li>
<li>Fix effect/composition list filter working on untranslated strings. <a href='http://commits.kde.org/kdenlive/2ce5ccdbe986febd3d36798dda2851a901888337'>Commit.</a> </li>
<li>Fix custom effects not recognized as audio. <a href='http://commits.kde.org/kdenlive/202bfe70285cb1959723ccf412f4d6fc308b1745'>Commit.</a> </li>
<li>Fix encoder speed ignored. <a href='http://commits.kde.org/kdenlive/6473cee38c4078ef8799ede4e5135ac12a2bad41'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411000'>#411000</a></li>
<li>Late update of version in appdata.. <a href='http://commits.kde.org/kdenlive/11ce38946ed8a77210869932d9f99c5ee897e26e'>Commit.</a> </li>
<li>Use the parameter readable and translatable name instead of its formal name for the color edit widget. <a href='http://commits.kde.org/kdenlive/c1d94545180236dda43ab819be899b0af4dd8283'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Allow to wrap attachment text (as diff). <a href='http://commits.kde.org/kdepim-addons/8d3bd8845d3c3d052a936c1d4cf75e5dc7b7fdea'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-apps-libs' href='https://cgit.kde.org/kdepim-apps-libs.git'>kdepim-apps-libs</a> <a href='#kdepim-apps-libs' onclick='toggle("ulkdepim-apps-libs", this)'>[Hide]</a></h3>
<ul id='ulkdepim-apps-libs' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kdepim-apps-libs/10f7be0bb46ac8a7ad1064d268123b4999e5fabe'>Commit.</a> </li>
</ul>
<h3><a name='kidentitymanagement' href='https://cgit.kde.org/kidentitymanagement.git'>kidentitymanagement</a> <a href='#kidentitymanagement' onclick='toggle("ulkidentitymanagement", this)'>[Hide]</a></h3>
<ul id='ulkidentitymanagement' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kidentitymanagement/9cb25cd0672e4d309f238300df68f1e1cc6b7079'>Commit.</a> </li>
</ul>
<h3><a name='kimap' href='https://cgit.kde.org/kimap.git'>kimap</a> <a href='#kimap' onclick='toggle("ulkimap", this)'>[Hide]</a></h3>
<ul id='ulkimap' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kimap/a323fd8fb11cf0a3db6d64573067d2bad04cd0f0'>Commit.</a> </li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Hide]</a></h3>
<ul id='ulkio-extras' style='display: block'>
<li>Fixing implementation of FileJob interface in smb/sftp slaves. <a href='http://commits.kde.org/kio-extras/28fdc41f1d450d142425dbcb55183f59a62f59bf'>Commit.</a> </li>
<li>Fixing bug where MTP slave does not return error in stat()/mimetype(). <a href='http://commits.kde.org/kio-extras/8c42ec63200fae3a2d34c56e789f5acb021898a9'>Commit.</a> </li>
</ul>
<h3><a name='kldap' href='https://cgit.kde.org/kldap.git'>kldap</a> <a href='#kldap' onclick='toggle("ulkldap", this)'>[Hide]</a></h3>
<ul id='ulkldap' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kldap/44eaef30fdedf882d30a8ef4714ceb6cadbdf1e4'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix Bug 409662 - Missing "Save Attachments..." from Multiple Emails. <a href='http://commits.kde.org/kmail/1ed64bf4065afae609592a0c48272e3ca0ce14db'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/409662'>#409662</a>. Fixes bug <a href='https://bugs.kde.org/392143'>#392143</a></li>
<li>Fix using correct parent widget. <a href='http://commits.kde.org/kmail/89bf5d1acb9c4a32b2691ed283b126bc3cc85332'>Commit.</a> </li>
</ul>
<h3><a name='kmailtransport' href='https://cgit.kde.org/kmailtransport.git'>kmailtransport</a> <a href='#kmailtransport' onclick='toggle("ulkmailtransport", this)'>[Hide]</a></h3>
<ul id='ulkmailtransport' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kmailtransport/7fa64684ef3a6e351810fce15f986bf11c0f475d'>Commit.</a> </li>
</ul>
<h3><a name='kmbox' href='https://cgit.kde.org/kmbox.git'>kmbox</a> <a href='#kmbox' onclick='toggle("ulkmbox", this)'>[Hide]</a></h3>
<ul id='ulkmbox' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kmbox/7cea5c674ca24f1cc09de5638997ffd8730dbf95'>Commit.</a> </li>
</ul>
<h3><a name='kmime' href='https://cgit.kde.org/kmime.git'>kmime</a> <a href='#kmime' onclick='toggle("ulkmime", this)'>[Hide]</a></h3>
<ul id='ulkmime' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kmime/27106f23e12b31e20c470eb3101ab4cfa31d2856'>Commit.</a> </li>
</ul>
<h3><a name='kolourpaint' href='https://cgit.kde.org/kolourpaint.git'>kolourpaint</a> <a href='#kolourpaint' onclick='toggle("ulkolourpaint", this)'>[Hide]</a></h3>
<ul id='ulkolourpaint' style='display: block'>
<li>Fix crash when doing open recent. <a href='http://commits.kde.org/kolourpaint/326969aa8c6bddc9823dfd3a9fec3a6ea424bacc'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Fix the fallback code used when restoring session fails. <a href='http://commits.kde.org/konsole/1df621390e82603794c46337821f9bf83b1ee369'>Commit.</a> </li>
<li>Add scrollbuttons for tabs when tabbar is full. <a href='http://commits.kde.org/konsole/ed44f288c2d26213e31c5b69b30a9e56ecfe8633'>Commit.</a> See bug <a href='https://bugs.kde.org/403059'>#403059</a></li>
<li>Restore "Show 'new tab' button" option in Konsole Settings. <a href='http://commits.kde.org/konsole/f780383096c1bc2b4ffe684a2a67013781dc2027'>Commit.</a> See bug <a href='https://bugs.kde.org/411158'>#411158</a></li>
<li>Port to non-deprecated KWindowSystem::unminimizeWindow. <a href='http://commits.kde.org/konsole/08b48e5fbecd143a64e599df8e8f71470cf87ba3'>Commit.</a> </li>
<li>Fix crash on "Close Current Tab" handling. <a href='http://commits.kde.org/konsole/3c25aafdbaf743665aa2fc0c2c6effc36c35e09c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/410607'>#410607</a></li>
</ul>
<h3><a name='kontact' href='https://cgit.kde.org/kontact.git'>kontact</a> <a href='#kontact' onclick='toggle("ulkontact", this)'>[Hide]</a></h3>
<ul id='ulkontact' style='display: block'>
<li>Use non-deprecated overload of KWindowSystem::minimizeWindow. <a href='http://commits.kde.org/kontact/5d8b0c79ac4884b6069def528d7957bcf4611ebd'>Commit.</a> </li>
</ul>
<h3><a name='kontactinterface' href='https://cgit.kde.org/kontactinterface.git'>kontactinterface</a> <a href='#kontactinterface' onclick='toggle("ulkontactinterface", this)'>[Hide]</a></h3>
<ul id='ulkontactinterface' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kontactinterface/06e72007d75c5bbc7e7707f0070728f0b6fd7bfa'>Commit.</a> </li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Hide]</a></h3>
<ul id='ulkopete' style='display: block'>
<li>Fix build with glibc 2.28. <a href='http://commits.kde.org/kopete/16ecb62296a9e78074c8f9123484e000a20f460c'>Commit.</a> </li>
</ul>
<h3><a name='korganizer' href='https://cgit.kde.org/korganizer.git'>korganizer</a> <a href='#korganizer' onclick='toggle("ulkorganizer", this)'>[Hide]</a></h3>
<ul id='ulkorganizer' style='display: block'>
<li>Use non-deprecated overload of KWindowSystem::minimizeWindow. <a href='http://commits.kde.org/korganizer/71d8efd6455f668603301a1b131dc583b8ae7f54'>Commit.</a> </li>
<li>Use non-deprecated overload of KWindowSystem::minimizeWindow. <a href='http://commits.kde.org/korganizer/50953dade980a84471942eb07521d7e34c42c7c6'>Commit.</a> </li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Hide]</a></h3>
<ul id='ulkpimtextedit' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/kpimtextedit/b24d7875fa933a506b04f37bd40bce7c88fbbecc'>Commit.</a> </li>
<li>Stop texttospeech when we close it. Add parent to button too. <a href='http://commits.kde.org/kpimtextedit/d14cb29aea550e50320635116711dbed8c4e6fd8'>Commit.</a> </li>
</ul>
<h3><a name='ksmtp' href='https://cgit.kde.org/ksmtp.git'>ksmtp</a> <a href='#ksmtp' onclick='toggle("ulksmtp", this)'>[Hide]</a></h3>
<ul id='ulksmtp' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/ksmtp/26b0e580912f50bb9806dc5e63dfc096a7182e34'>Commit.</a> </li>
</ul>
<h3><a name='ktnef' href='https://cgit.kde.org/ktnef.git'>ktnef</a> <a href='#ktnef' onclick='toggle("ulktnef", this)'>[Hide]</a></h3>
<ul id='ulktnef' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/ktnef/890e4496f7cf8d2ddf2b1feb12df9946e78c0e65'>Commit.</a> </li>
</ul>
<h3><a name='ktouch' href='https://cgit.kde.org/ktouch.git'>ktouch</a> <a href='#ktouch' onclick='toggle("ulktouch", this)'>[Hide]</a></h3>
<ul id='ulktouch' style='display: block'>
<li>Fix Tooltip Dimensions of Learning Progress Chart. <a href='http://commits.kde.org/ktouch/799d2d1a1619caef5787b91a5cbb92a1dd8d6b7a'>Commit.</a> </li>
<li>Fix Visual Glitches in Dark Themes. <a href='http://commits.kde.org/ktouch/23a4623c24b21ee9af60892ad6a83fdadfa727b7'>Commit.</a> </li>
</ul>
<h3><a name='kwordquiz' href='https://cgit.kde.org/kwordquiz.git'>kwordquiz</a> <a href='#kwordquiz' onclick='toggle("ulkwordquiz", this)'>[Hide]</a></h3>
<ul id='ulkwordquiz' style='display: block'>
<li>Remove unused private field 'm_column'. <a href='http://commits.kde.org/kwordquiz/d7f9a913a55f8f51902283cd2062c42a41fdd87d'>Commit.</a> </li>
<li>Do not crash on exit. <a href='http://commits.kde.org/kwordquiz/a59d1d14566ff119144a867537aa74191d0c7aa6'>Commit.</a> </li>
</ul>
<h3><a name='libgravatar' href='https://cgit.kde.org/libgravatar.git'>libgravatar</a> <a href='#libgravatar' onclick='toggle("ullibgravatar", this)'>[Hide]</a></h3>
<ul id='ullibgravatar' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/libgravatar/4069caacd877785763b4efc16b9235405bf0f8f4'>Commit.</a> </li>
</ul>
<h3><a name='libkdepim' href='https://cgit.kde.org/libkdepim.git'>libkdepim</a> <a href='#libkdepim' onclick='toggle("ullibkdepim", this)'>[Hide]</a></h3>
<ul id='ullibkdepim' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/libkdepim/30a7254e8fd7ef0878780922be54168bdc2e2af3'>Commit.</a> </li>
</ul>
<h3><a name='libkgapi' href='https://cgit.kde.org/libkgapi.git'>libkgapi</a> <a href='#libkgapi' onclick='toggle("ullibkgapi", this)'>[Hide]</a></h3>
<ul id='ullibkgapi' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/libkgapi/be1aa945fa63cbac42a1e26afc9553f98ffb2c91'>Commit.</a> </li>
</ul>
<h3><a name='libkleo' href='https://cgit.kde.org/libkleo.git'>libkleo</a> <a href='#libkleo' onclick='toggle("ullibkleo", this)'>[Hide]</a></h3>
<ul id='ullibkleo' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/libkleo/a9604b32439bbcd455ace7701fefd1028e441fbd'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Hide]</a></h3>
<ul id='ullibksieve' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/libksieve/4fb2ba28a59a175125aee2689a0a302526e94189'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Fix unittest: Calcutta -> Kolkata. <a href='http://commits.kde.org/lokalize/60954915c8f94e87c4728f8fb826c6a79fdbb06f'>Commit.</a> </li>
<li>Use QTEST_GUILESS_MAIN for tests. <a href='http://commits.kde.org/lokalize/43cc0d25c4fa693f5eb57f6d8e461a5919707c8e'>Commit.</a> </li>
<li>Fix Breton plural line. <a href='http://commits.kde.org/lokalize/164876db33adc786fcec0ebade38ac9b083f4ff8'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/mailcommon/bd37dbbe7aa0b77c7f5de0fc07aaf896522e7d07'>Commit.</a> </li>
</ul>
<h3><a name='mailimporter' href='https://cgit.kde.org/mailimporter.git'>mailimporter</a> <a href='#mailimporter' onclick='toggle("ulmailimporter", this)'>[Hide]</a></h3>
<ul id='ulmailimporter' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/mailimporter/6692be8b3bbb07ea298bb4e1da96d3f6bdbde535'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/messagelib/32fbf815dc0c9172bc8f38f4ae160456e6e5c085'>Commit.</a> </li>
<li>Fix memory leak in PluginEditorGrammar. <a href='http://commits.kde.org/messagelib/798c9313e43e9f158c5b23dffd8f532674770d94'>Commit.</a> </li>
<li>Fix crash when viewer is null. <a href='http://commits.kde.org/messagelib/029d96744e13aa51aee539e6f1ff33d265a6d357'>Commit.</a> </li>
<li>Allow to wrap attachment text (as diff). <a href='http://commits.kde.org/messagelib/6afe5a209a4674a9abe1e640d00b7c23e14e04f2'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Enhance bounds check in TextDocumentGenerator. <a href='http://commits.kde.org/okular/b5b273ad6403e7030f6ede264efc0e4579ac89e3'>Commit.</a> See bug <a href='https://bugs.kde.org/410844'>#410844</a></li>
</ul>
<h3><a name='parley' href='https://cgit.kde.org/parley.git'>parley</a> <a href='#parley' onclick='toggle("ulparley", this)'>[Hide]</a></h3>
<ul id='ulparley' style='display: block'>
<li>Use QTEST_GUILESS_MAIN for testentry test. <a href='http://commits.kde.org/parley/ed92bda9af6aff2452ba9cf9114a83d1c2df2179'>Commit.</a> </li>
</ul>
<h3><a name='pim-data-exporter' href='https://cgit.kde.org/pim-data-exporter.git'>pim-data-exporter</a> <a href='#pim-data-exporter' onclick='toggle("ulpim-data-exporter", this)'>[Hide]</a></h3>
<ul id='ulpim-data-exporter' style='display: block'>
<li>Remove debug. <a href='http://commits.kde.org/pim-data-exporter/b9308033b736ed83b5d1da9156eeedde69f06d64'>Commit.</a> </li>
</ul>
<h3><a name='pimcommon' href='https://cgit.kde.org/pimcommon.git'>pimcommon</a> <a href='#pimcommon' onclick='toggle("ulpimcommon", this)'>[Hide]</a></h3>
<ul id='ulpimcommon' style='display: block'>
<li>Include KDE* cmake macros before any others. <a href='http://commits.kde.org/pimcommon/1b44ee798997cd9b40eaad6bfa84abb4641bc9c3'>Commit.</a> </li>
</ul>
<h3><a name='print-manager' href='https://cgit.kde.org/print-manager.git'>print-manager</a> <a href='#print-manager' onclick='toggle("ulprint-manager", this)'>[Hide]</a></h3>
<ul id='ulprint-manager' style='display: block'>
<li>Try to find the smb backend for cups as runtime dependency. <a href='http://commits.kde.org/print-manager/c16ef75e27d155637dca337a9b533e61a4196e81'>Commit.</a> </li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Hide]</a></h3>
<ul id='ulspectacle' style='display: block'>
<li>FIX: Close spectacle if it's started with: -b -c. <a href='http://commits.kde.org/spectacle/331d3ceaaf74ce376a08b2efcf4394e45f8a8e97'>Commit.</a> See bug <a href='https://bugs.kde.org/411263'>#411263</a></li>
<li>Fix Quickeditor only be shown on one screen on  Wayland. <a href='http://commits.kde.org/spectacle/4177fde76d32ef0551dae11e12fa7b5cfdb6e02b'>Commit.</a> </li>
<li>Don't exit when running in gui mode and notification times out. <a href='http://commits.kde.org/spectacle/7f30ee858368695a10ebbe628aabf0ef688f60cf'>Commit.</a> </li>
<li>Allow Spectacle to build with KConfig <= 5.56.0. <a href='http://commits.kde.org/spectacle/b3337d18458eb6bdecaacfb3c9399f684b561f19'>Commit.</a> </li>
</ul>
<h3><a name='step' href='https://cgit.kde.org/step.git'>step</a> <a href='#step' onclick='toggle("ulstep", this)'>[Hide]</a></h3>
<ul id='ulstep' style='display: block'>
<li>Fix minor typos. <a href='http://commits.kde.org/step/a20d416f1d527d5fe68fd409c79d5c41a24b2fb6'>Commit.</a> </li>
<li>Fix crash when deleting a SoftBody. <a href='http://commits.kde.org/step/614fc9ab1bbaa64cb16b5f52b948cb0c651db6a6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408574'>#408574</a></li>
<li>Fix crash on exit. <a href='http://commits.kde.org/step/824c2cab8a9896b406fece80d0fcaa4077430d25'>Commit.</a> </li>
<li>Add MIME type for Step files. <a href='http://commits.kde.org/step/c55a449ce23bc5ebecaea5fe8eaa930502920d44'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/296317'>#296317</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Fix build with cmake < 3.7. <a href='http://commits.kde.org/umbrello/efafb44002bcc418743e77a3371cea999aa24285'>Commit.</a> </li>
<li>Fixed grammer and punctuation. <a href='http://commits.kde.org/umbrello/dc1ceae3b05d4293bfd675870e0d910d8d0e0cfc'>Commit.</a> </li>
<li>Fixup. <a href='http://commits.kde.org/umbrello/d506eedc48844566b29f0ce758d9e996611b2457'>Commit.</a> See bug <a href='https://bugs.kde.org/410904'>#410904</a></li>
<li>Fix 'Umbrello could not open imported source files'. <a href='http://commits.kde.org/umbrello/ba0d6ac58fcf46b8f5dc552443ae19d97c41cf1e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/410904'>#410904</a></li>
</ul>
