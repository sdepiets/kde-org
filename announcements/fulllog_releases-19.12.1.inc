<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='artikulate' href='https://cgit.kde.org/artikulate.git'>artikulate</a> <a href='#artikulate' onclick='toggle("ulartikulate", this)'>[Hide]</a></h3>
<ul id='ulartikulate' style='display: block'>
<li>Avoid access of empty list's item. <a href='http://commits.kde.org/artikulate/cd5e03f16557aade4252786d1b2ddbc2a672a70d'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Improve scroll wheel speed by basing it on label height, not icon height. <a href='http://commits.kde.org/dolphin/403de19d9c036dd28481d3b62bdb0f49f0792fbf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386379'>#386379</a></li>
</ul>
<h3><a name='dolphin-plugins' href='https://cgit.kde.org/dolphin-plugins.git'>dolphin-plugins</a> <a href='#dolphin-plugins' onclick='toggle("uldolphin-plugins", this)'>[Hide]</a></h3>
<ul id='uldolphin-plugins' style='display: block'>
<li>Fixed broken SVN Commit dialog. <a href='http://commits.kde.org/dolphin-plugins/e8f6c72a3779cc41342f781e60de063882b2ab39'>Commit.</a> </li>
</ul>
<h3><a name='elisa' href='https://cgit.kde.org/elisa.git'>elisa</a> <a href='#elisa' onclick='toggle("ulelisa", this)'>[Hide]</a></h3>
<ul id='ulelisa' style='display: block'>
<li>Set the cover art in the header to a fixed source size. <a href='http://commits.kde.org/elisa/0f31ea5c730bbeba3e9e21b044faceba254a56b5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413359'>#413359</a></li>
<li>Use textEdided signal in EditableMetaDataDelegate. <a href='http://commits.kde.org/elisa/eb2fd038582a802e0dfe75bcb6e357c3ac64c349'>Commit.</a> </li>
<li>Set the background source size to a fixed value. <a href='http://commits.kde.org/elisa/e5fe012b4d03693aa03aa2d690d9c9cbe6c4493b'>Commit.</a> See bug <a href='https://bugs.kde.org/413359'>#413359</a></li>
<li>Fix compilation of Android music indexer. <a href='http://commits.kde.org/elisa/62c603c42d112e93ce0fb69671dc7db975f7ab27'>Commit.</a> </li>
<li>Handle missing artist when albumartist exists in models. <a href='http://commits.kde.org/elisa/7198602845df19f4b5b8eb7bab99edc579bcfcc8'>Commit.</a> </li>
<li>Fix resizing of configuration dialog. <a href='http://commits.kde.org/elisa/0ef3ee473ec2589f1919e7bc95380a7ffe3bee2a'>Commit.</a> </li>
<li>Convert KCM based configuration dialog to a simple dialog. <a href='http://commits.kde.org/elisa/1e328cd90eee05157a860d8beeb14fba344c54b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414982'>#414982</a></li>
<li>Fix compilation without Baloo. <a href='http://commits.kde.org/elisa/0f7e8b7d97bba0c5f7087fbb3e9b7c282b90e7c8'>Commit.</a> </li>
<li>Fix indexers state management in MusicListenersManager. <a href='http://commits.kde.org/elisa/2033765492eaeea449e63a07a01001aff990bc45'>Commit.</a> </li>
<li>Fix local file indexer not to remove unmodified files after first scan. <a href='http://commits.kde.org/elisa/12a5d2955566329827a9f3304f3da3bef5786cde'>Commit.</a> </li>
<li>Exclude paths excluded in baloo configuration. <a href='http://commits.kde.org/elisa/d33daa180dd6f43a995efe6a07e84975a41a6b90'>Commit.</a> </li>
<li>Almost working switch from baloo to files indexing on config changes. <a href='http://commits.kde.org/elisa/0eacedb23f942b8068883c6bc198c4f2513dc6d8'>Commit.</a> </li>
<li>Properly detect if all paths can be handled by baloo. <a href='http://commits.kde.org/elisa/68753ca5e6f28706a9d9fb488576ab371e66bdc0'>Commit.</a> </li>
<li>Start baloo only if all paths are indexed by baloo. <a href='http://commits.kde.org/elisa/8f3e49a81e9df6ad0ed25d6a1e53a2a925703e88'>Commit.</a> </li>
<li>Ignore baloo indexer when some paths are not indexed. <a href='http://commits.kde.org/elisa/262e265eeed29f5f9d06e2fcf46e59edb5e6466b'>Commit.</a> </li>
<li>Add back missing icons for windows task bar integration. <a href='http://commits.kde.org/elisa/23a77a4b60fedf3e5411c485ef42424039c6f8ba'>Commit.</a> </li>
<li>Ensure that the metadata dialog has a title and standard buttons. <a href='http://commits.kde.org/elisa/77bc9b24bd0e4d50194e9afa7187bba989a620f6'>Commit.</a> </li>
<li>Use global menu only on linux due to behavior difference on Windows. <a href='http://commits.kde.org/elisa/0ad92f7d174b25bc3c4a7a29184f8665a58cd119'>Commit.</a> </li>
<li>Fix new issue introduced by !39 merge request. <a href='http://commits.kde.org/elisa/4ec4503d82e349a4d32fad417174e2c5eccddcca'>Commit.</a> </li>
<li>As requested during review, put back extra parameter in TracksListener::trackByFileNameInList. <a href='http://commits.kde.org/elisa/e87e2c60ee19961a0a0e0cfb29cf8af1541bae52'>Commit.</a> </li>
<li>Many clazy level 2 fixes. <a href='http://commits.kde.org/elisa/0c22a3e685f2c046cabfcf63317f2eeefb9f90b7'>Commit.</a> </li>
<li>Correct clazy level 1 warnings from Elisa source code. <a href='http://commits.kde.org/elisa/e1b17e32c4950d149ea4ebe00b071e3eb5608735'>Commit.</a> </li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Hide]</a></h3>
<ul id='ulincidenceeditor' style='display: block'>
<li>Fix Bug 414977 - broken layout does no longer resize attendee list. <a href='http://commits.kde.org/incidenceeditor/faef62cef5f4bd871bf489f3cfd6522e52a036fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414977'>#414977</a></li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>PlayerManager: Unify volume slots. <a href='http://commits.kde.org/juk/50559afaa50d1a2801e71f3d791e679ee80930a6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405973'>#405973</a></li>
<li>Save config after it is closed by the window manager. <a href='http://commits.kde.org/juk/444a4c9f102ddbb50baf1f0f4367c2f9ced8a4c7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359681'>#359681</a></li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Make defer dialogue accessible when a full screen window is active. <a href='http://commits.kde.org/kalarm/870c2a6173d83b1989e948bdfb97cf0c2f9bb886'>Commit.</a> </li>
<li>Only show 'Cancel Deferral' in defer dialogue if a deferral is already active. <a href='http://commits.kde.org/kalarm/7fdd9d20020acd4bbdee66ef4313b0c47c3cd264'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>Unbreak build with cmake < 3.7 (VERSION_GREATER_EQUAL only avail >= 3.7). <a href='http://commits.kde.org/kate/e0446fd5461f1e377568fbc515d980722ddaa60b'>Commit.</a> </li>
<li>Fix crash when pressing ESC after external tools plugin was un/reloaded. <a href='http://commits.kde.org/kate/5aa3b009cf531ef412d97513f1b7e768be345f75'>Commit.</a> </li>
<li>Sort settings file by language id. <a href='http://commits.kde.org/kate/1f614a1963dae6cf8b4e1e209bcf6b1181e8fbb0'>Commit.</a> </li>
<li>Initial support for javascript + typescript LSP. <a href='http://commits.kde.org/kate/0f0161d77e9ede3384a9083b3d1211ea3124fe29'>Commit.</a> </li>
<li>Lsp: update info of OCaml server. <a href='http://commits.kde.org/kate/20cd727e7f550700b8dbaa771ce0b2eb2025b627'>Commit.</a> </li>
<li>Lspclient: aid compilation on older version by explicit json object conversion. <a href='http://commits.kde.org/kate/6645379e5f04065a5ccc62b7ba702bc0a68ab67e'>Commit.</a> </li>
<li>Relax ocaml regex. <a href='http://commits.kde.org/kate/83bc059cc24f6dbcb2f9b97b0c91afa033a86d86'>Commit.</a> </li>
<li>Allow highlighting mode => language id mapping to be configured in the JSON config. <a href='http://commits.kde.org/kate/ce8ab61a3ea007a8e9b3b4f4f2adb3592492486c'>Commit.</a> </li>
<li>Add icons to metadata of plugins supporting KDevelop/Plugin. <a href='http://commits.kde.org/kate/7ca0173c81b30a9a8409fe5168f33ca44e20c8bc'>Commit.</a> </li>
</ul>
<h3><a name='kbackup' href='https://cgit.kde.org/kbackup.git'>kbackup</a> <a href='#kbackup' onclick='toggle("ulkbackup", this)'>[Hide]</a></h3>
<ul id='ulkbackup' style='display: block'>
<li>Improve appdata file. <a href='http://commits.kde.org/kbackup/4552c2fb4ce79101cffab0c06e57ab1c6943dc10'>Commit.</a> </li>
</ul>
<h3><a name='kcalc' href='https://cgit.kde.org/kcalc.git'>kcalc</a> <a href='#kcalc' onclick='toggle("ulkcalc", this)'>[Hide]</a></h3>
<ul id='ulkcalc' style='display: block'>
<li>Fix warning on launch from QCommandLineParser. <a href='http://commits.kde.org/kcalc/ac05c80883dc6894d0d0851bacec56250ae5f0a2'>Commit.</a> </li>
</ul>
<h3><a name='kcalutils' href='https://cgit.kde.org/kcalutils.git'>kcalutils</a> <a href='#kcalutils' onclick='toggle("ulkcalutils", this)'>[Hide]</a></h3>
<ul id='ulkcalutils' style='display: block'>
<li>Fix kcalutils-testincidenceformatter color. <a href='http://commits.kde.org/kcalutils/21a1aa5ade773830c1a790a114851cb72166761d'>Commit.</a> </li>
</ul>
<h3><a name='kdegraphics-thumbnailers' href='https://cgit.kde.org/kdegraphics-thumbnailers.git'>kdegraphics-thumbnailers</a> <a href='#kdegraphics-thumbnailers' onclick='toggle("ulkdegraphics-thumbnailers", this)'>[Hide]</a></h3>
<ul id='ulkdegraphics-thumbnailers' style='display: block'>
<li>Prevent division by zero and force ghostscript run. <a href='http://commits.kde.org/kdegraphics-thumbnailers/af3f38a4d8f6ab66a463dd13e78385f34f4af933'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406463'>#406463</a></li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Adjust clip borders. <a href='http://commits.kde.org/kdenlive/379efc4ea6ad265895ccaba40d5f1cdd6d3f586c'>Commit.</a> </li>
<li>Ensure we don't insert thousand separator in number conversion, and only send integer coordinates on keyframe import. <a href='http://commits.kde.org/kdenlive/3315ad4f41ed1e6e2e7fb02c62d9281eac1cc81c'>Commit.</a> </li>
<li>Better qml interface scaling. <a href='http://commits.kde.org/kdenlive/43bcc5533138d64592aac780ffbaf5ee3bccad5e'>Commit.</a> </li>
<li>Update Copyright year to 2020. <a href='http://commits.kde.org/kdenlive/e480a56dcb0bd5cf98de6d3a9407ec122bd8abc0'>Commit.</a> </li>
<li>Use project name as default render name. <a href='http://commits.kde.org/kdenlive/9b9abe43d74ac0eacf7ad7a7418d8fcb360f6ac9'>Commit.</a> </li>
<li>Fix transparent rendering. <a href='http://commits.kde.org/kdenlive/96993b973f2e682728fb275aa94468f2fe7c9e69'>Commit.</a> </li>
<li>Fix extract zone (improve ffmpeg arguments and drop locale specific seconds conversion). <a href='http://commits.kde.org/kdenlive/ebed22b1c827f0e4988c51dfa99f99836613176d'>Commit.</a> See bug <a href='https://bugs.kde.org/411970'>#411970</a></li>
<li>Dont' put colon in cut clip names. <a href='http://commits.kde.org/kdenlive/6cc7cb33009f5d165a3eb2c5b5f6059b55c89b6a'>Commit.</a> </li>
<li>Add select all in bin. <a href='http://commits.kde.org/kdenlive/064400d6ff172603aaa4323039231dd797743f88'>Commit.</a> </li>
<li>Fix extract zone. <a href='http://commits.kde.org/kdenlive/b6dc6f5eba0edbe0ec046b4bd91a9e22dd1b1d72'>Commit.</a> See bug <a href='https://bugs.kde.org/411970'>#411970</a></li>
<li>When a clip is dropped in bin, focus on it. <a href='http://commits.kde.org/kdenlive/42b760c8510f4218b79431998f7cbebcb12fa304'>Commit.</a> </li>
<li>Fix timeline seeking not reflected on effect stack. <a href='http://commits.kde.org/kdenlive/55055a432638fddd2377e7f21bbb0d396baac9f3'>Commit.</a> </li>
<li>Update Appdata version. <a href='http://commits.kde.org/kdenlive/eae996db698fa3308a83df0bc9dbd806600e6239'>Commit.</a> </li>
<li>Fix duplicate track compositing on project opening and broken opacity with background. <a href='http://commits.kde.org/kdenlive/534765c9c2f74abcef3ec73a957c2a4c7b845e16'>Commit.</a> </li>
<li>Fix fades offset. <a href='http://commits.kde.org/kdenlive/30c3512db3d2ec6dc46897320c52720783d9e680'>Commit.</a> </li>
<li>Default to QtAngle for Windows. <a href='http://commits.kde.org/kdenlive/ffbd82326a3ad29b875792870afb2e55bb4d3509'>Commit.</a> </li>
<li>Fix proxying of slideshow clips. <a href='http://commits.kde.org/kdenlive/e083567cc30cf5af56ea441651b2322d63eb63f8'>Commit.</a> See bug <a href='https://bugs.kde.org/415448'>#415448</a></li>
<li>Fix possible crash in effectstack. <a href='http://commits.kde.org/kdenlive/84f26e14c0bac2e2ea81013c257b860705db3f9f'>Commit.</a> </li>
<li>Small adjustments to timeline fades ui. <a href='http://commits.kde.org/kdenlive/3339a0f96393a44fb2864fc497abeaff9d4677e6'>Commit.</a> </li>
<li>Don't show unsupported effect groups in UI (causing crash). <a href='http://commits.kde.org/kdenlive/d568a0847e27677cd69fe2bd95d23fefd66a61c8'>Commit.</a> </li>
<li>Minor improvement to show clip in project bin. <a href='http://commits.kde.org/kdenlive/b3cb1389d82cf1fd0110aa8da53c960f8f3da152'>Commit.</a> </li>
<li>Cleanup & fix titled background distorted. <a href='http://commits.kde.org/kdenlive/2a0c0d9164cc79759742d504dff7f41e6326a0f0'>Commit.</a> </li>
<li>Fix timeline clip duration not updated after clip reload. <a href='http://commits.kde.org/kdenlive/b18469b8c5b384a55425676a0815ce89bcf70149'>Commit.</a> </li>
<li>Make title widget smaller. <a href='http://commits.kde.org/kdenlive/e61403cd48e0674e151f40c24179bf0f7418cf99'>Commit.</a> </li>
<li>Fix crash creating proxy on clip with subclip. <a href='http://commits.kde.org/kdenlive/a60666fd6a8969360003e152f857a479fe721c70'>Commit.</a> </li>
<li>Fix crash caused by incorrect group saved. <a href='http://commits.kde.org/kdenlive/908442dbccc3b86df24424d8551407bf916a5d92'>Commit.</a> </li>
<li>Fix possible crash on group move (no >= in std::sort). <a href='http://commits.kde.org/kdenlive/86b2cd2e88fc1452e56489d969420fba382ea8d2'>Commit.</a> </li>
<li>Fix monitor audio thumbnail disappearing on proxy disable. <a href='http://commits.kde.org/kdenlive/f8f05818f255ae7b23e3d3de703918fc75cd538b'>Commit.</a> </li>
<li>Fix default font size and color for first start. <a href='http://commits.kde.org/kdenlive/528633a459f02eefb3cd9bf55021ca5a313defa2'>Commit.</a> </li>
<li>Make sure drag mode doesn't persist when switching to icon view. <a href='http://commits.kde.org/kdenlive/f9c980c55460a13cace55aad50ec466e6d587d65'>Commit.</a> </li>
<li>Fix clip losing thumb/length on profile switch. <a href='http://commits.kde.org/kdenlive/718fa543653783472d2dd25af8af43f13979a10b'>Commit.</a> </li>
<li>Fix loop zone broken regression. <a href='http://commits.kde.org/kdenlive/cce6cb4b1073248f7ce30b4b316cfd97844550b0'>Commit.</a> </li>
<li>Don't unnecessarily trigger timeline thumbnail reload. <a href='http://commits.kde.org/kdenlive/c28eb54c61b442b2c5a8ab00672103417f465456'>Commit.</a> </li>
<li>Fix tests. <a href='http://commits.kde.org/kdenlive/af1d590a36d192523dae98987b5d67e89f6a9eac'>Commit.</a> </li>
<li>Fix tests. <a href='http://commits.kde.org/kdenlive/0a053c1fbbb243f0b8aad82a6165b37765158e42'>Commit.</a> </li>
<li>Fix empty i18 warnings on startup. <a href='http://commits.kde.org/kdenlive/e1af19eae216d3d152ae9bbb532b22991660b4b9'>Commit.</a> </li>
<li>Fix various 1 frame offset issues in monitor and zone handling. <a href='http://commits.kde.org/kdenlive/33ffef5133b0734d8919de22ac4c8fbc30d91887'>Commit.</a> </li>
<li>Use new syntax. <a href='http://commits.kde.org/kdenlive/7b06b0b0cf97ddcfaf9cf3326c1a2e71de9ae314'>Commit.</a> </li>
<li>Fix timeline preview not invalidated when disabling effect. <a href='http://commits.kde.org/kdenlive/95070257e4d8b3aae126738ba01c8e316b4f815c'>Commit.</a> </li>
<li>Fix timeline preview not disabled on render. <a href='http://commits.kde.org/kdenlive/2faf8c4545c7e7a30f4f8199078d291aab206f1d'>Commit.</a> </li>
<li>Immediatly pause when switching between play forwards/backwards. <a href='http://commits.kde.org/kdenlive/bca65171338579b5afb27d4c92c84d98c3bf7909'>Commit.</a> </li>
<li>Don't use active track tag to indicate muted tracks, instead fade its clips. <a href='http://commits.kde.org/kdenlive/0855e9e046886fd0ec9472147e2080376b03411c'>Commit.</a> </li>
<li>Fix saving project with several clip groups selected discarded those groups. <a href='http://commits.kde.org/kdenlive/e58e316f0c9a5c4141010ad7518c49b6c43177fe'>Commit.</a> </li>
<li>Rename KDE_APPLICATIONS_VERSION to RELEASE_SERVICE. <a href='http://commits.kde.org/kdenlive/5382856bf69cc697edc0c67e53f83042ecb13882'>Commit.</a> </li>
<li>Fix missing clips erased from timeline on opening project. <a href='http://commits.kde.org/kdenlive/cbef877f5b9e0ae9495e1a5f75bb3a4a1a92abff'>Commit.</a> </li>
<li>If clip resize not sticking to start/end. <a href='http://commits.kde.org/kdenlive/0e2843f295b0a983b74a71c3b0832d86150714f4'>Commit.</a> </li>
<li>Optimise group move (don't attempt a track move if not possible). <a href='http://commits.kde.org/kdenlive/614337175bd59578724c3e01c1d2fa5b060cdf6e'>Commit.</a> </li>
<li>Use const &values for clip/composition sorting on group move. <a href='http://commits.kde.org/kdenlive/5df474e0d1c7074bbd6b71d1254e836e1660dd2d'>Commit.</a> </li>
<li>DOn't attempt to load empty data as JSon. <a href='http://commits.kde.org/kdenlive/a11cc87f0f01c1ce2ad4775500edcc308d394b7a'>Commit.</a> </li>
<li>Update qml headers for Qt 5.11. <a href='http://commits.kde.org/kdenlive/6973df8e76910805ae8a586232a3ae7a84cbb2f4'>Commit.</a> </li>
<li>Fix playing clip monitor seems to pause a few frames before end and seeking allowed past clip end. <a href='http://commits.kde.org/kdenlive/b02d33f53f6e6a064f1968b003432d5d0c9c324a'>Commit.</a> </li>
<li>* Fix model insert/delete track, so we don'tneed to reset view, makes these operation much faster. <a href='http://commits.kde.org/kdenlive/4b448742470a55f367c3615f8f3fbac144815aec'>Commit.</a> </li>
<li>Update master appdata version. <a href='http://commits.kde.org/kdenlive/54502a9d88aaeb493c7a172a8aa42bf7957a9380'>Commit.</a> </li>
<li>Update screenshots. <a href='http://commits.kde.org/kdenlive/0cc2342ed9f4246a1900f5d0bece4f0d92f91c00'>Commit.</a> </li>
<li>Disable assert Catch test failing on some systems. <a href='http://commits.kde.org/kdenlive/b2fa3d167337ade858679cdba8186bc02e40ed4f'>Commit.</a> </li>
<li>Replace icon view "back" button with an "Up" icon in bin toolbar. <a href='http://commits.kde.org/kdenlive/3490ab78084d31786ea0d1885b88e61aba540603'>Commit.</a> </li>
<li>Fix playhead disappeared. <a href='http://commits.kde.org/kdenlive/f7518d23dcad32bee70edf9081006741ca03357d'>Commit.</a> </li>
<li>Adjust rendering threads for faster rendering. <a href='http://commits.kde.org/kdenlive/bb87d2933a47a9759cd93bc2bd224e14521e4b3a'>Commit.</a> </li>
<li>Add flatpak nightly manifest. <a href='http://commits.kde.org/kdenlive/930fb8d6e5d79c4974c1b500f80bfabed4811820'>Commit.</a> </li>
<li>Fix freeze on Windows switching monitor. <a href='http://commits.kde.org/kdenlive/e715f23a7b562b6c8d3b035445bba60b8a971192'>Commit.</a> </li>
<li>Remove debug. <a href='http://commits.kde.org/kdenlive/621f10fca6662f3a13679df51a7f0cb60cd5c483'>Commit.</a> </li>
<li>Cleanup seeking logic, get rid of confusing blue bar indicating real MLT position, pause on seek. <a href='http://commits.kde.org/kdenlive/1cf58f62e8c3cc5efe90b9b36605f6e52debe134'>Commit.</a> </li>
<li>Fix default audio mixer size. Related to #429. <a href='http://commits.kde.org/kdenlive/5f78606863edea87f68307497d32f899b4d1553d'>Commit.</a> </li>
<li>Fix minor typo. <a href='http://commits.kde.org/kdenlive/2d1cb9c49a743a23cbe70f9aa07e0fc6785b7e10'>Commit.</a> </li>
<li>Deprecate old normalize audio (volume based) in favor of loudness. <a href='http://commits.kde.org/kdenlive/6817efd9045fab81c96e1c1ae61aa72c6c844677'>Commit.</a> </li>
<li>Better startup layout for smaller screens. <a href='http://commits.kde.org/kdenlive/dcf4e4b554f336e4249c0631991e7ef48e917d23'>Commit.</a> </li>
<li>Fix dragging favorite effect to master. <a href='http://commits.kde.org/kdenlive/741e4196a15879947ca2704b6cac5470aaab8051'>Commit.</a> </li>
<li>Fix drag from monitor. <a href='http://commits.kde.org/kdenlive/46a6a95f0cd58ab8c4d4f5655ff6778a30929644'>Commit.</a> </li>
<li>Fix curve parameter after introducing odd only possibility. <a href='http://commits.kde.org/kdenlive/69044c1c7964bd4f2087ae0135e271550edf4185'>Commit.</a> </li>
<li>Don't show monitor audio overly for clips with no audio. <a href='http://commits.kde.org/kdenlive/c24fe986d40abef2f6605ea438f376b464232d45'>Commit.</a> </li>
<li>Fix handling of dock widget title bars. <a href='http://commits.kde.org/kdenlive/b8b1f0fbca1d1154e199d813b7463eba206942ad'>Commit.</a> </li>
<li>Fix composition description not appearing in info box. <a href='http://commits.kde.org/kdenlive/5cb2e98c38a04984f9cd5030f2315322a68d3af5'>Commit.</a> </li>
<li>Fix some UI strings todo. <a href='http://commits.kde.org/kdenlive/3ab5c48d86758fe9fff7ee3605db9b80b0f50ed1'>Commit.</a> </li>
<li>Fix undocked widgets losing title bar. Fixes #368. <a href='http://commits.kde.org/kdenlive/acb8f339094470fd8daf3621124e9b5c01156edf'>Commit.</a> </li>
<li>Expose luma options in dissolve composition. <a href='http://commits.kde.org/kdenlive/c75e34e6551181071334474b71a734ec5e761bc0'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Fix save config. <a href='http://commits.kde.org/kdepim-addons/f19d716ea2534520b32952883bd4b886e4c10d28'>Commit.</a> </li>
<li>Fix insert text. <a href='http://commits.kde.org/kdepim-addons/aab4198cd208745fbb93aba86cc8fca0bd30c480'>Commit.</a> </li>
<li>Activate by default. <a href='http://commits.kde.org/kdepim-addons/c547a5680c4ad5a5965c3a24c105b25ece0a77a9'>Commit.</a> </li>
</ul>
<h3><a name='kgeography' href='https://cgit.kde.org/kgeography.git'>kgeography</a> <a href='#kgeography' onclick='toggle("ulkgeography", this)'>[Hide]</a></h3>
<ul id='ulkgeography' style='display: block'>
<li>Russia districts: Fix Kaliningrad not being shown as part of Northwestern. <a href='http://commits.kde.org/kgeography/7571c2906b2a2dfcc76fc89c4df7161de134f596'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414857'>#414857</a></li>
</ul>
<h3><a name='kidentitymanagement' href='https://cgit.kde.org/kidentitymanagement.git'>kidentitymanagement</a> <a href='#kidentitymanagement' onclick='toggle("ulkidentitymanagement", this)'>[Hide]</a></h3>
<ul id='ulkidentitymanagement' style='display: block'>
<li>Check if 'cat' executable exists before using it. <a href='http://commits.kde.org/kidentitymanagement/2034e49c649c362dcb07756a21b8b7cff88f1647'>Commit.</a> </li>
</ul>
<h3><a name='kig' href='https://cgit.kde.org/kig.git'>kig</a> <a href='#kig' onclick='toggle("ulkig", this)'>[Hide]</a></h3>
<ul id='ulkig' style='display: block'>
<li>Fix https://bugs.kde.org/show_bug.cgi?id=401512. <a href='http://commits.kde.org/kig/4c689131bd8575b825574566cf8296a586bcbad8'>Commit.</a> </li>
<li>Add Extension to Filename to Save to Kig Format. <a href='http://commits.kde.org/kig/b2967c9feee3ce0a06c65d353df39bcac0b51ee1'>Commit.</a> See bug <a href='https://bugs.kde.org/404366'>#404366</a></li>
<li>Add a QDialogButtonBox to the Coordinate Precision Dialog. <a href='http://commits.kde.org/kig/63e4bc0f9f82162f3b78f0c565c29359ae906ec2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414641'>#414641</a></li>
</ul>
<h3><a name='kitinerary' href='https://cgit.kde.org/kitinerary.git'>kitinerary</a> <a href='#kitinerary' onclick='toggle("ulkitinerary", this)'>[Hide]</a></h3>
<ul id='ulkitinerary' style='display: block'>
<li>Fix address extraction in NH Hotel booking confirmations. <a href='http://commits.kde.org/kitinerary/12cabf13ceeab5e59c7287f19fa99d0af96811fa'>Commit.</a> </li>
<li>NH extractor: Be more robust against mangled HTML during forwarding. <a href='http://commits.kde.org/kitinerary/f101c17f81107ed590d2418c07924aa9dc2bb155'>Commit.</a> </li>
<li>Fix ticket token comparison. <a href='http://commits.kde.org/kitinerary/9310e2112c2799d038c118fcf6c0e8bd0ed6a88d'>Commit.</a> </li>
<li>Match COPYING.LIB content to LGPL version referenced in the code. <a href='http://commits.kde.org/kitinerary/d52720b199cf93f5b3519117de6d4664c9e56cd5'>Commit.</a> </li>
<li>Fix 0080BL sub-block iteration boundaries check. <a href='http://commits.kde.org/kitinerary/88f8de87063b72cf00b8c06eec6144024c8210b3'>Commit.</a> </li>
<li>Extract Indian Railways SMS confirmations. <a href='http://commits.kde.org/kitinerary/5d6f3915507689af4aaa05df6f351ee71c96b5c0'>Commit.</a> </li>
<li>Consider Indian Railways station data base as well during post-processing. <a href='http://commits.kde.org/kitinerary/b4d7ed66c9e42d2317b1abb4c79cbee3c5ca7b78'>Commit.</a> </li>
<li>Simplify the generic UIC 918.3 extractor a bit. <a href='http://commits.kde.org/kitinerary/808f4331c621b2e2a7d3b2b44e2c3dc11be74493'>Commit.</a> </li>
<li>Try to fix build without Poppler. <a href='http://commits.kde.org/kitinerary/501686048f7b503b0de93206f2ad97b488df6a41'>Commit.</a> </li>
<li>Make regular expression on np4 booking code less precise. <a href='http://commits.kde.org/kitinerary/e65397b4ebe1f5120b4fd8334ccc85fd9c1bae48'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix Bug 415160 - KMail build fails if messagelib is built without Qca-qt5. <a href='http://commits.kde.org/kmail/f025305936237292ec4d0a8a9fddc1d6cd89a0bd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415160'>#415160</a></li>
</ul>
<h3><a name='kolf' href='https://cgit.kde.org/kolf.git'>kolf</a> <a href='#kolf' onclick='toggle("ulkolf", this)'>[Hide]</a></h3>
<ul id='ulkolf' style='display: block'>
<li>Fix stroke count being increased more than once when hitting the ball. <a href='http://commits.kde.org/kolf/22d04effdb4231f78a020c82be025946c8417c18'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414791'>#414791</a></li>
<li>Remove unused function. <a href='http://commits.kde.org/kolf/b6dbfcaf2662374b79b59092eaebb4fb4a7049e7'>Commit.</a> </li>
</ul>
<h3><a name='kolourpaint' href='https://cgit.kde.org/kolourpaint.git'>kolourpaint</a> <a href='#kolourpaint' onclick='toggle("ulkolourpaint", this)'>[Hide]</a></h3>
<ul id='ulkolourpaint' style='display: block'>
<li>Fix warning about help and version already being added. <a href='http://commits.kde.org/kolourpaint/6c649caef830f90d3e0aef211149e05b8e209be2'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Update copyright year. <a href='http://commits.kde.org/konsole/7ee9350bbab8cb0a719e45ee85b2ebc4fd29f129'>Commit.</a> </li>
<li>ColorScheme: fix stack-use-after-scope detected by ASAN. <a href='http://commits.kde.org/konsole/2fc470507e42452be1249346634b43eb8bdbdd02'>Commit.</a> </li>
<li>Copy _colorRandomization when copying a new ColorScheme. <a href='http://commits.kde.org/konsole/771c1733fd5ee9cfb854212b83df6de148608a65'>Commit.</a> See bug <a href='https://bugs.kde.org/415242'>#415242</a></li>
<li>Fix token buffer indexing. <a href='http://commits.kde.org/konsole/23ddfcc4fcd3cce1b52a68f44b8a44f121d11919'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415249'>#415249</a>. Fixes bug <a href='https://bugs.kde.org/415463'>#415463</a></li>
<li>Revert removal to allow Bookmark tabs as Folders to work. <a href='http://commits.kde.org/konsole/7f2f55c30bfd91167b3fe9700acef706986984ae'>Commit.</a> </li>
<li>Revert supportTabs() removal to fix bookmenu issue. <a href='http://commits.kde.org/konsole/5b5ad9f93eb7f2fd39a2191a7be15572dcdaa2ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415549'>#415549</a></li>
</ul>
<h3><a name='kpat' href='https://cgit.kde.org/kpat.git'>kpat</a> <a href='#kpat' onclick='toggle("ulkpat", this)'>[Hide]</a></h3>
<ul id='ulkpat' style='display: block'>
<li>Add oars content rating. <a href='http://commits.kde.org/kpat/052912364654e9cb6dcc78e25493309212c8dbf6'>Commit.</a> </li>
</ul>
<h3><a name='krfb' href='https://cgit.kde.org/krfb.git'>krfb</a> <a href='#krfb' onclick='toggle("ulkrfb", this)'>[Hide]</a></h3>
<ul id='ulkrfb' style='display: block'>
<li>Fix uninitialized memory read when calling rfbEncryptBytes. <a href='http://commits.kde.org/krfb/9b422f13381006bd07b96a74a440fc10ce044a80'>Commit.</a> </li>
<li>Correctly populate max color values in server screen format. <a href='http://commits.kde.org/krfb/bb59ce2776f3c8ba155c802f135778ba586f1531'>Commit.</a> </li>
</ul>
<h3><a name='kwalletmanager' href='https://cgit.kde.org/kwalletmanager.git'>kwalletmanager</a> <a href='#kwalletmanager' onclick='toggle("ulkwalletmanager", this)'>[Hide]</a></h3>
<ul id='ulkwalletmanager' style='display: block'>
<li>Do not use future versions for disabling deprecated API in stable branches. <a href='http://commits.kde.org/kwalletmanager/6f7ae97b22df48a1b95f98ee74312ddd0c6c7047'>Commit.</a> </li>
</ul>
<h3><a name='libksane' href='https://cgit.kde.org/libksane.git'>libksane</a> <a href='#libksane' onclick='toggle("ullibksane", this)'>[Hide]</a></h3>
<ul id='ullibksane' style='display: block'>
<li>Fix color-options layout & KSaneOptCombo::setValue(). <a href='http://commits.kde.org/libksane/3130174cae4e0410c7eef605a43a14550cd29e8a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414601'>#414601</a>. Fixes bug <a href='https://bugs.kde.org/414602'>#414602</a></li>
<li>Optimize support of scanners with different image sources or duplex unit. <a href='http://commits.kde.org/libksane/aada3bf426105405052f67890255bdd952d882ca'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414600'>#414600</a></li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Move variable before attachmeent. <a href='http://commits.kde.org/mailcommon/e48b8b32f6adf26eeec6b2415ee062926125131b'>Commit.</a> </li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Hide]</a></h3>
<ul id='ulmarble' style='display: block'>
<li>Fix build with gpsd 3.20. <a href='http://commits.kde.org/marble/51e0d9122cf2871167d7466faf16fc33d660ae25'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Clear status when we clear text. <a href='http://commits.kde.org/messagelib/3b8c48308308bdca9b21626c0aecf5626664c01c'>Commit.</a> </li>
<li>Fix Bug 415254 - [regression] HTML viewer complains about "external references to images etc." when there are no images. <a href='http://commits.kde.org/messagelib/b551d400afe823b4357b9e3a0ed7ba233e27a77e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415254'>#415254</a></li>
<li>Add code for debugging containsExternalReferences. We need to improve it. <a href='http://commits.kde.org/messagelib/7750e056f6751196a293e81aef1734514bf97c77'>Commit.</a> </li>
<li>Test excludeExtraHeader. Need to improve regexp. <a href='http://commits.kde.org/messagelib/34ab26ced039167a87f71a8b0a39630480305d99'>Commit.</a> See bug <a href='https://bugs.kde.org/415254'>#415254</a></li>
<li>Fix autotest. <a href='http://commits.kde.org/messagelib/516c155f1af6b0a147f057fc5a4e7bbedd43f540'>Commit.</a> </li>
<li>Revert "Fix autotest. Code for showing "we have external ref" was fixed". <a href='http://commits.kde.org/messagelib/bfd2d115a4b0c5c11bb7db6c0703229ce42a51ed'>Commit.</a> </li>
<li>Fix Bug 415254 - [regression] HTML viewer complains about "external references to images etc." when there are no images. <a href='http://commits.kde.org/messagelib/56f1d42f68ed2f597fc599e4952ee81e579cd355'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415254'>#415254</a></li>
<li>Remove duplicate line. <a href='http://commits.kde.org/messagelib/f2e8e04f8a2e4f904d71f7e6e7d15efb6570f6aa'>Commit.</a> </li>
<li>Fix split element. <a href='http://commits.kde.org/messagelib/08b771562b04403a8fd5190af26e0ff4bc8d0947'>Commit.</a> </li>
<li>Fix some autotest. <a href='http://commits.kde.org/messagelib/f6d79b5eec4d7884d29a457901be5629b147fb06'>Commit.</a> </li>
<li>Fix autotest. Code for showing "we have external ref" was fixed. <a href='http://commits.kde.org/messagelib/6ff17d3a5666b198d8c55d6350f3d33c50184c2f'>Commit.</a> </li>
<li>Fetch DKIMResultAttribute too. <a href='http://commits.kde.org/messagelib/1226da21c21a47fb09d02c03fc91fa69e18f820c'>Commit.</a> </li>
<li>Make basicobjecttreeparsertest independent of host timezone. <a href='http://commits.kde.org/messagelib/9c1db0e190f42322d52d27e1109cee0f84f3975a'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Fix crash when closing print preview dialog. <a href='http://commits.kde.org/okular/6e170312d8560963108365ee9e5c124d5c76f0e9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415340'>#415340</a></li>
<li>Fix man page formatting. <a href='http://commits.kde.org/okular/feb72a016fac45316e5e569ef8523e370186a284'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415468'>#415468</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Remove obsolete code. <a href='http://commits.kde.org/umbrello/317c4ffef32ef390b7a04c77f9b74bd3c4830ee1'>Commit.</a> </li>
<li>Fix 'No undo support for adding connected widgets'. <a href='http://commits.kde.org/umbrello/a6c4b6a8e647b366bb1eb417fcf86eb1906ee9bf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415888'>#415888</a></li>
<li>Fix 'Different name when adding connected states compared to adding via the toolbar'. <a href='http://commits.kde.org/umbrello/508dff46748d1824caac0304089cc20b3330302c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415890'>#415890</a></li>
<li>Fix 'Adding connected widgets cannot be cancelled'. <a href='http://commits.kde.org/umbrello/ed0b70850d2c5486de3a2df5788e03412db5805a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415889'>#415889</a></li>
<li>Limit building local KDev::Tests library for KF5 only when kdevplatform library has been found. <a href='http://commits.kde.org/umbrello/b554c49ac2e7dcc5063fb9260b9833dded941eab'>Commit.</a> See bug <a href='https://bugs.kde.org/415849'>#415849</a></li>
<li>Library KDev::Tests: remove Qt5::Tests dependencies. <a href='http://commits.kde.org/umbrello/22c769b39c1f53db16c6c40f98a30fd7b5125907'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415849'>#415849</a></li>
<li>Embed KDev::Tests library to not depend on kdevplatform for KF5 builds. <a href='http://commits.kde.org/umbrello/c7488f8a561d5e34106c5c76bc2ce5323feafd38'>Commit.</a> See bug <a href='https://bugs.kde.org/415849'>#415849</a></li>
<li>Fix 'The initial cursor position in the properties dialog of a state should be set to the name field'. <a href='http://commits.kde.org/umbrello/57df796dbb90e14d6e5a0699cdd63ae39d381380'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415784'>#415784</a></li>
<li>Fix 'Adding a connected state should allow entering of a state name'. <a href='http://commits.kde.org/umbrello/7c40aa2dd6ba0dc8dbd7597a4b94ac00f4489ebb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415779'>#415779</a></li>
</ul>
