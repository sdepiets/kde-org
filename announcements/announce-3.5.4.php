<?php
  $page_title = "KDE 3.5.4 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>DATELINE August 2, 2006</p>
<p>FOR IMMEDIATE RELEASE</p>

Also available in: <a href="http://cat.kde.org/index.php/Notes_de_premsa/KDE_3.5.4">Catalan</a> 
<a href="http://www.kde.nl/uitgaven/aankondiging_kde3.5.4.html">Dutch</a> 
<a href="http://www.is.kde.org/announcements/announce-3.5.4.php">Icelandic</a>
<a href="http://www.kde-it.org/index.php?option=com_content&task=view&id=32&Itemid=2">Italian</a>
<a href="announce-3.5.4-pt.php">Portuguese</a>
<a href="announce-3.5.4-sl.php">Slovenian</a> <a href="announce-3.5.4-sv.php">Swedish</a> <a href="http://kde-vi.org/index.php/Kde-3.5.4.php">Vietnamese</a>

<h3 align="center">
   KDE Project Ships Fourth Translation and Service 
   Release for Leading Free Software Desktop
</h3>

<p align="justify">
  <strong>
    KDE 3.5.4 features translations in 65 languages, 
    enhanced removable device support and 
    improvements in the HTML rendering engine (KHTML).
  </strong>
</p>

<p align="justify">
  August 2, 2006 (The INTERNET).  The <a href="http://www.kde.org/">KDE
  Project</a> today announced the immediate availability of KDE 3.5.4,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes. KDE now supports 65
  languages, making it available to more people than most non-free software and can 
  be easily extended to support others by communities who wish to contribute to the
  open source project.
</p>

<p align="justify">
  Significant enhancements include:
</p>

<ul>
  <li>Improved removable device support in Linux (users can now mount all devices supported 
  by <a href="http://www.freedesktop.org/wiki/Software/hal">FreeDesktop's HAL</a> and control how it will be done)
  </li>
  <li>Speed optimisations in <a href="http://konsole.kde.org">Konsole</a> and <a href="http://kate.kde.org">Kate</a></li>
  <li>Multiple holidays can now start on the same date in <a href="http://korganizer.kde.org">KOrganizer</a></li>
  <li>Lots of fixes in <a href="http://www.konqueror.org">Konqueror</a>'s HTML engine, KHTML</li>
  <li>The dialog for sending client-side SSL certificates is now more usable</li>
  <li><a href="http://knetworkconf.sourceforge.net">KNetworkConf</a> now supports Fedora Core 5 and handles WEP keys better</li>
</ul>

<p align="justify">
  Over 10 new features were added and more than 100 bugs were fixed. For a more 
  detailed list of improvements since the 
  <a href="announce-3.5.3.php">KDE 3.5.3 release</a> in May 2006, 
  please refer to the
  <a href="changelogs/changelog3_5_3to3_5_4.php">KDE 3.5.4 Changelog</a>.
</p>

<p align="justify">
  KDE 3.5.4 ships with a basic desktop and fifteen other packages (PIM, administration,
  network, edutainment, utilities, multimedia, games,
  artwork, web development and more). KDE's award-winning tools and
  applications are available in <strong>65 languages</strong>.
</p>

<h4>
  Installing KDE 3.5.4 Binary Packages
</h4>
<p align="justify">
  <em>Package Creators</em>.
  Some operating system vendors have kindly provided binary packages of
  KDE 3.5.4 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  download server at
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.4/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="/info/3.5.4.php">KDE 3.5.4 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.5.4
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.5.4 may be
  <a href="http://download.kde.org/stable/3.5.4/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.5.4
  are available from the <a href="/info/3.5.4.php">KDE
  3.5.4 Info Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> project that exists and grows only because of the
help of many volunteers that donate their time and effort. KDE
is always looking for new volunteers and contributions, whether its
help with coding, bug fixing or reporting, writing documentation,
translations, promotion, money, etc. All contributions are gratefully
appreciated and eagerly accepted. Please read through the <a href="/community/donations/">Supporting
KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an <a href="/awards/">award-winning</a>, independent <a href="/people/">project of hundreds</a>
  of developers, translators, artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.</p>

<p align="justify">
  KDE provides a stable, mature desktop including a state-of-the-art browser
  (<a href="http://konqueror.kde.org/">Konqueror</a>), a personal information
  management suite (<a href="http://kontact.org/">Kontact</a>), a full 
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking application and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE<sup>&#174;</sup> and the K Desktop Environment<sup>&#174;</sup> logo are 
  registered trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<h4>Press Contacts</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>Asia and India</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     India 410206<br/>
     Phone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>

<?php

  include("footer.inc");
?>
