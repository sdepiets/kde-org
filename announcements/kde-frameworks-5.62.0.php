<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.62.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.62.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
September 14, 2019. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.62.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Fix the attica pkgconfig file");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Fixes a crash in Peruse triggered by baloo");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add new activities and virtual desktops icons");?></li>
<li><?php i18n("Make small recent documents icons look like documents and improve clock emblems");?></li>
<li><?php i18n("Create new \"Recent folders\" icon (bug 411635)");?></li>
<li><?php i18n("Add \"preferences-desktop-navigation\" icon (bug 402910)");?></li>
<li><?php i18n("Add 22px dialog-scripts, change script actions/places icons to match it");?></li>
<li><?php i18n("Improve \"user-trash\" icon");?></li>
<li><?php i18n("Use empty/filled style for monochrome empty/full trash");?></li>
<li><?php i18n("Make notification icons use outline style");?></li>
<li><?php i18n("Make user-trash icons look like trashcans (bug 399613)");?></li>
<li><?php i18n("Add breeze icons for ROOT cern files");?></li>
<li><?php i18n("Remove applets/22/computer (bug 410854)");?></li>
<li><?php i18n("Add view-barcode-qr icons");?></li>
<li><?php i18n("Krita has split from Calligra and now uses Krita name instead of calligrakrita (bug 411163)");?></li>
<li><?php i18n("Add battery-ups icons (bug 411051)");?></li>
<li><?php i18n("Make \"monitor\" a link to \"computer\" icon");?></li>
<li><?php i18n("Add FictionBook 2 icons");?></li>
<li><?php i18n("Add icon for kuiviewer, needs updating -&gt; bug 407527");?></li>
<li><?php i18n("Symlink \"port\" to \"anchor\", which displays more appropriate iconography");?></li>
<li><?php i18n("Change radio to device icon, add more sizes");?></li>
<li><?php i18n("Make <code>pin</code> icon point to an icon that looks like a pin, not something unrelated");?></li>
<li><?php i18n("Fix missing digit and pixel-perfect alignment of depth action icons (bug 406502)");?></li>
<li><?php i18n("Make 16px folder-activities look more like larger sizes");?></li>
<li><?php i18n("add latte-dock icon from latte dock repo for kde.org/applications");?></li>
<li><?php i18n("icon for kdesrc-build used by kde.org/applications to be redrawn");?></li>
<li><?php i18n("Rename media-show-active-track-amarok to media-track-show-active");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("ECMAddQtDesignerPlugin: pass code sample indirectly via variable name arg");?></li>
<li><?php i18n("Keep 'lib' as default LIBDIR on Arch Linux based systems");?></li>
<li><?php i18n("Enable autorcc by default");?></li>
<li><?php i18n("Define install location for JAR/AAR files for Android");?></li>
<li><?php i18n("Add ECMAddQtDesignerPlugin");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Add Term::Type::files() and Term::Type::directories() to filter only directories or excluding them");?></li>
<li><?php i18n("Add @since 5.62 for newly added setters");?></li>
<li><?php i18n("Add proper logging using ECMQtDeclareLoggingCategory");?></li>
<li><?php i18n("Add setter to Type, Activity, Agent and UrlFilter query fields");?></li>
<li><?php i18n("Use special values constants in terms.cpp");?></li>
<li><?php i18n("Allow date range filtering of resource events using Date Term");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("[kactivities] Use new activities icon");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Fix creating archives on Android content: URLs");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Fix memory leak in KConfigWatcher");?></li>
<li><?php i18n("Disable KCONFIG_USE_DBUS on Android");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)");?></li>
<li><?php i18n("[KColorSchemeManager] Optimize preview generation");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KProcessInfo::name() now returns only the name of the executable. For the full command line use KProcessInfo::command()");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("Avoid enabling kcrash if it's only included via a plugin (bug 401637)");?></li>
<li><?php i18n("Disable kcrash when running under rr");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Fix race on kcrash auto-restarts");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Warn if KPackage is invalid");?></li>
<li><?php i18n("[GridDelegate] Don't select unselected item when clicking on any of its action buttons (bug 404536)");?></li>
<li><?php i18n("[ColorButton] Forward accepted signal from ColorDialog");?></li>
<li><?php i18n("use zero-based coordinate system on the plot");?></li>
</ul>

<h3><?php i18n("KDesignerPlugin");?></h3>

<ul>
<li><?php i18n("Deprecate kgendesignerplugin, drop bundle plugim for all KF5 widgets");?></li>
</ul>

<h3><?php i18n("KDE WebKit");?></h3>

<ul>
<li><?php i18n("Use ECMAddQtDesignerPlugin instead of private copy");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("KF5DocToolsMacros.cmake: Use non-deprecated KDEInstallDirs variables (bug 410998)");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Implement writing of images");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Display filename where we return an error");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Localize long number strings (bug 409077)");?></li>
<li><?php i18n("Support passing target to ki18n_wrap_ui macro");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Undoing trashing files on the desktop has been fixed (bug 391606)");?></li>
<li><?php i18n("kio_trash: split up copyOrMove, for a better error than \"should never happen\"");?></li>
<li><?php i18n("FileUndoManager: clearer assert when forgetting to record");?></li>
<li><?php i18n("Fix exit and crash in kio_file when put() fails in readData");?></li>
<li><?php i18n("[CopyJob] Fix crash when copying an already existing dir and pressing \"Skip\" (bug 408350)");?></li>
<li><?php i18n("[KUrlNavigator] Add MIME types supported by krarc to isCompressedPath (bug 386448)");?></li>
<li><?php i18n("Added dialog to set execute permission for executable file when trying to run it");?></li>
<li><?php i18n("[KPropertiesDialog] Always check mount point being null (bug 411517)");?></li>
<li><?php i18n("[KRun] Check mime type for isExecutableFile first");?></li>
<li><?php i18n("Add an icon for the trash root and a proper label (bug 392882)");?></li>
<li><?php i18n("Add support for handling QNAM SSL errors to KSslErrorUiData");?></li>
<li><?php i18n("Making FileJob behave consistently");?></li>
<li><?php i18n("[KFilePlacesView] Use asynchronous KIO::FileSystemFreeSpaceJob");?></li>
<li><?php i18n("rename internal 'kioslave' helper executable to 'kioslave5' (bug 386859)");?></li>
<li><?php i18n("[KDirOperator] Middle-elide labels that are too long to fit (bug 404955)");?></li>
<li><?php i18n("[KDirOperator] Add follow new directories options");?></li>
<li><?php i18n("KDirOperator: Only enable \"Create New\" menu if the selected item is a directory");?></li>
<li><?php i18n("KIO FTP: Fix file copy hanging when copying to existing file (bug 409954)");?></li>
<li><?php i18n("KIO: port to non-deprecated KWindowSystem::setMainWindow");?></li>
<li><?php i18n("Make file bookmark names consistent");?></li>
<li><?php i18n("Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)");?></li>
<li><?php i18n("[KDirOperator] Use more human-readable sort order descriptions");?></li>
<li><?php i18n("[Permissions editor] Port icons to use QIcon::fromTheme() (bug 407662)");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Replace the custom overflow button with PrivateActionToolButton in ActionToolBar");?></li>
<li><?php i18n("If a submenu action has an icon set, make sure to also display it");?></li>
<li><?php i18n("[Separator] Match Breeze borders' color");?></li>
<li><?php i18n("Add Kirigami ListSectionHeader component");?></li>
<li><?php i18n("Fix context menu button for pages not showing up");?></li>
<li><?php i18n("Fix PrivateActionToolButton with menu not clearing checked state properly");?></li>
<li><?php i18n("allow to set custom icon for the left drawer handle");?></li>
<li><?php i18n("Rework the visibleActions logic in SwipeListItem");?></li>
<li><?php i18n("Allow usage of QQC2 actions on Kirigami components and now make K.Action based on QQC2.Action");?></li>
<li><?php i18n("Kirigami.Icon: Fix loading bigger images when source is a URL (bug 400312)");?></li>
<li><?php i18n("Add icon used by Kirigami.AboutPage");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("Add Q_PROPERTIES interface to KDescendantsProxyModel");?></li>
<li><?php i18n("Port away from deprecated methods in Qt");?></li>
</ul>

<h3><?php i18n("KItemViews");?></h3>

<ul>
<li><?php i18n("Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Avoid duplicate notifications from showing up on Windows and remove whitespaces");?></li>
<li><?php i18n("Have 1024x1024 app icon as fallback icon in Snore");?></li>
<li><?php i18n("Add <code>-pid</code> parameter to Snore backend calls");?></li>
<li><?php i18n("Add snoretoast backend for KNotifications on Windows");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Make it possible to delete contacts from backends");?></li>
<li><?php i18n("Make it possible to modify contacts");?></li>
</ul>

<h3><?php i18n("KPlotting");?></h3>

<ul>
<li><?php i18n("Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Make sure we're checking whether tearing down is due after finishing a job");?></li>
<li><?php i18n("Add a done signal to FindMatchesJob instead of using QObjectDecorator wrongly");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Allow to customize attributes for KSyntaxHighligting themes");?></li>
<li><?php i18n("Fix for all themes: allow turn off attributes in XML highlighting files");?></li>
<li><?php i18n("simplify isAcceptableInput + allow all stuff for input methods");?></li>
<li><?php i18n("simplify typeChars, no need for return code without filtering");?></li>
<li><?php i18n("Mimic QInputControl::isAcceptableInput() when filtering typed characters (bug 389796)");?></li>
<li><?php i18n("try to sanitize line endings on paste (bug 410951)");?></li>
<li><?php i18n("Fix: allow turn off attributes in XML highlighting files");?></li>
<li><?php i18n("improve word completion to use highlighting to detect word boundaries (bug 360340)");?></li>
<li><?php i18n("More porting from QRegExp to QRegularExpression");?></li>
<li><?php i18n("properly check if diff command can be started for swap file diffing (bug 389639)");?></li>
<li><?php i18n("KTextEditor: Fix left border flicker when switching between documents");?></li>
<li><?php i18n("Migrate some more QRegExps to QRegularExpression");?></li>
<li><?php i18n("Allow 0 in line ranges in vim mode");?></li>
<li><?php i18n("Use CMake find_dependency instead of find_package in CMake config file template");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Add decibel power units (dBW and multiples)");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("KWallet: fix starting kwalletmanager, the desktop file name has a '5' in it");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("[server] Wrap proxyRemoveSurface in smart pointer");?></li>
<li><?php i18n("[server] Use cached current mode more and assert validness");?></li>
<li><?php i18n("[server] Cache current mode");?></li>
<li><?php i18n("Implement zwp_linux_dmabuf_v1");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("[KMessageWidget] Pass widget to standardIcon()");?></li>
<li><?php i18n("Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("KWindowSystem: add cmake option KWINDOWSYSTEM_NO_WIDGETS");?></li>
<li><?php i18n("Deprecate slideWindow(QWidget *widget)");?></li>
<li><?php i18n("Add KWindowSystem::setMainWindow(QWindow *) overload");?></li>
<li><?php i18n("KWindowSystem: add setNewStartupId(QWindow *...) overload");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Rename WirelessDevice::lastRequestScanTime to WirelessDevice::lastRequestScan");?></li>
<li><?php i18n("Add property lastScanTime and lastRequestTime to WirelessDevice");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Make notification icons use outline style");?></li>
<li><?php i18n("make the sizing of the toolbuttons more coherent");?></li>
<li><?php i18n("Allow applets/containments/wallpaper to defer UIReadyConstraint");?></li>
<li><?php i18n("Make notification icons look like bells (bug 384015)");?></li>
<li><?php i18n("Fix incorrect initial tabs position for vertical tab bars (bug 395390)");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Fixed Telegram Desktop plugin on Fedora");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Prevent dragging QQC2 ComboBox contents outside menu");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Make battery serial property constant");?></li>
<li><?php i18n("Expose technology property in battery interface");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("C &amp; ISO C++: add digraphs (folding &amp; preprocessor) (bug 411508)");?></li>
<li><?php i18n("Markdown, TypeScript &amp; Logcat: some fixes");?></li>
<li><?php i18n("Format class: add functions to know if XML files set style attributes");?></li>
<li><?php i18n("combine test.m stuff into existing highlight.m");?></li>
<li><?php i18n("Support for native Matlab strings");?></li>
<li><?php i18n("Gettext: Add \"Translated String\" style and spellChecking attribute (bug 392612)");?></li>
<li><?php i18n("Set the OpenSCAD indentor to C-style instead of none");?></li>
<li><?php i18n("Possibility to change Definition data after loading");?></li>
<li><?php i18n("Highlighting indexer: check kateversion");?></li>
<li><?php i18n("Markdown: multiple improvements and fixes (bug 390309)");?></li>
<li><?php i18n("JSP: support of &lt;script&gt; and &lt;style&gt; ; use IncludeRule ##Java (bug 345003)");?></li>
<li><?php i18n("LESS: import CSS keywords, new highlighting and some improvements");?></li>
<li><?php i18n("JavaScript: remove unnecessary \"Conditional Expression\" context");?></li>
<li><?php i18n("New syntax: SASS. Some fixes for CSS and SCSS (bug 149313)");?></li>
<li><?php i18n("Use CMake find_dependency in CMake config file instead of find_package");?></li>
<li><?php i18n("SCSS: fix interpolation (#{...}) and add the Interpolation color");?></li>
<li><?php i18n("fix additionalDeliminator attribute (bug 399348)");?></li>
<li><?php i18n("C++: contracts are not in C++20");?></li>
<li><?php i18n("Gettext: fix \"previous untranslated string\" and other improvements/fixes");?></li>
<li><?php i18n("Jam: Fix local with variable without initialisation and highlight SubRule");?></li>
<li><?php i18n("implicit fallthrough if there is fallthroughContext");?></li>
<li><?php i18n("Add common GLSL file extensions (.vs, .gs, .fs)");?></li>
<li><?php i18n("Latex: several fixes (math mode, nested verbatim, ...) (bug 410477)");?></li>
<li><?php i18n("Lua: fix color of end with several levels of condition and function nesting");?></li>
<li><?php i18n("Highlighting indexer: all warnings are fatal");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.62");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.11");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
