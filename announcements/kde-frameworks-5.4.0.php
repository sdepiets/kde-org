<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.4.0");
  $site_root = "../";
  $release = '5.4.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
November 06, 2014. KDE today announces the release
of KDE Frameworks 5.4.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<?php i18n("
<h3>Attica</h3>

<ul>
<li>Fix build with Qt 5.4</li>
</ul>

<h3>KArchive</h3>

<ul>
<li>Add support for rcc files</li>
</ul>

<h3>KAuth</h3>

<ul>
<li>Fix install dir when using KDE_INSTALL_USE_QT_SYS_PATHS</li>
</ul>

<h3>KCMUtils</h3>

<ul>
<li>Fix KPluginSelector not adding non .desktop file plugins</li>
</ul>

<h3>KConfigWidgets</h3>

<ul>
<li>Fix URL in KHelpClient::invokeHelp</li>
</ul>

<h3>KCoreAddons</h3>

<ul>
<li>Various build fixes (QNX, Ubuntu 14.04)</li>
</ul>

<h3>KDeclarative</h3>

<ul>
<li>Optimize IconItem in QIconItem in memory and speed</li>
</ul>

<h3>KIO</h3>

<ul>
<li>New job KIO::mkpath()</li>
<li>New job KIO::PasteJob, handles pasting+dropping URLs and data; KIO::paste replaces KIO::pasteClipboard</li>
<li>New function KIO::pasteActionText, to handle the paste action</li>
<li>When clicking on a script or desktop file in a file manager, let the user choose between executing and vieweing as text</li>
<li>KUrlRequester: fixing handling of start directory</li>
<li>Offer also overwrite option when copying multiple files and there is a conflict (regression compared to kdelibs4)</li>
<li>KDirLister: do not kill+restart running update jobs.</li>
<li>KDirLister: don't emit refreshItems for items that haven't changed.</li>
<li>Prevent incorrect disk full error messages on mounts where we cannot determine the amount of free space.</li>
<li>Fix running the file type editor</li>
</ul>

<h3>KNewStuff</h3>

<ul>
<li>Many small fixes and cleanups</li>
</ul>

<h3>KNotifications</h3>

<ul>
<li>Add support for custom icons (convenience method KNotification::Event with icon name)</li>
</ul>

<h3>KTextEditor</h3>

<ul>
<li>Implement \"go to last editing position\" action</li>
<li>Guard against a possibly broken code folding state on disk</li>
</ul>

<h3>KWallet</h3>

<ul>
<li>Emit 'walletListDirty' signal when the 'kwalletd' directory is deleted</li>
</ul>

<h3>KWidgetsAddons</h3>

<ul>
<li>New function KMimeTypeEditor::editMimeType(), to launch keditfiletype5</li>
</ul>

<h3>KXmlGui</h3>

<ul>
<li>Now supports finding ui files in resources (default location: :/kxmlgui5/&lt;componentname&gt;)</li>
</ul>

<h3>Plasma frameworks</h3>

<ul>
<li>Fixes in the Dialog QML component size and position</li>
<li>fixes in the Slider and ProgressBar QML components</li>
<li>new icons</li>
</ul>

<h3>Solid</h3>

<ul>
<li>[not communicated]</li>
</ul>

<h3>Buildsystem changes</h3>

<ul>
<li>New modules FindWaylandScanner and FindQtWaylandScanner, including macros ecm_add_qtwayland_client_protocol and ecm_add_qtwayland_server_protocol</li>
</ul>

<h3>Frameworkintegration</h3>

<ul>
<li>implement support for setting custom labels in file dialogs</li>
</ul>
");?>

<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php
//download directory now misses last bugfix version
$downloadDir = split("\.", $release)[0] . "." . split("\.", $release)[1];

print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%1/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%2.php'>KDE Frameworks %1 Info Page</a>.", $downloadDir, $release);?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.2");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://freenode.net/kde-devel");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
