<?php
  $page_title = "4.6 Beta1 Brings Improved Search, Activities and Mobile Device Support";
  $site_root = "../";
  include "header.inc";
?>

<h1>FOR IMMEDIATE RELEASE</h1>

<!-- // Boilerplate -->

<h3 align="center">
  
</h3>

<p align="justify">
<strong>
    KDE releases 4.6 beta1 of Workspaces, Applications and Development Frameworks, bringing significant improvements to desktop search, a revamped activity system and a significant performance boost to window management and desktop effects. Efforts all across the KDE codebase pay off by making KDE's frameworks more suitable for usage on all devices. The release provides a testing base for a stable release in January 2011.
</strong>
</p>

<h1>Plasma 4.6 Beta1 Previews Mature Themable Widget Set for QML</h1>
<p>
<ul>
    <li>
    Libplasma, KDE's framework for user interfaces for devices, can now be used as widget and component library from applications and widgets written in <b>QML</b>, the Qt Quick declarative UI. Plasma's new QML bindings offer a mature set of touchscreen-friendly widgets, SVG-based theming and access to Plasma's rich set of data-engines.</li>
    <li>
    The Plasma Desktop reintroduces <b>Activities</b>, a mechanism that helps organizing the workspace and grouping applications by their context. Applications can be started and stopped as part of an Activity and widgets used can vary per activity. This way, the desktop provides an easy way to organize and ease the users' workflows. For normal desktop usage, Activities are entirely transparant and opt-in.</li>
    <li>
    The KWin compositing manager has seen major <b>optimizations</b> in rendering and plugin handling, leading to a smoother window management experience and even more stunning desktop effects to enhance the users' workflow. KWin's new <b>scripting interface</b> allows to <a href="http://rohanprabhu.com/?p=116">script</a> more complex window management workflows using Javascript.
    </li>
</ul>
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<img src="announce-4.6-beta1-activities.png" align="center" width="451" height="251" alt="Plasma Desktop's new Activity Manager" title="Plasma Desktop's new Activity Manager" />
<br />
<em>Plasma Desktop's new Activity Manager</em>
</div>
<p>
These are only some of the improvements the Plasma Team delivers with 4.6 coming January. This release being a first beta version, we encourage users to give it a try and report your findings back to us via <a href="http://bugs.kde.org">KDE's bug tracker</a>.
</p>


<h1>Dolphin File Manager 4.6 Beta1 Introduces Faceted Browsing</h1>
<p>
The KDE Applications 4.6 Beta1 which is released today, adds exciting new features throughout the applications. Among the most striking of a large number of applications that are released as part of the KDE software compilation is Dolphin, which has made leaps forward in its searching capabilities and user interface:

</p>
<p>
<ul>
    <li>
    The Dolphin file manager now <a href="http://ppenz.blogspot.com/2010/11/dolphin-improvements-for-kde-sc-46.html">searches inside files</a> even when the Nepomuk semantic desktop is disabled. Nepomuk adds <a href="http://trueg.wordpress.com/2010/09/08/308/">faceted browsing</a>, a mechanism that uses the files' metadata to filter results and directories, or to display files in directories recursively.
    </li>
    <li>
    Columns in the file manager now match the filename's length, making for a clearer overview.
    </li>
    <li>
    The newly developed <b>Git plugin</b> for Dolphin serves as a sign for the impending migration of most of KDE's source code to Git.
    </li>
</ul>
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="announce-4.6-beta1-dolphin.png"><img src="announce-4.6-beta1-dolphin_thumb.png" align="center" width="540" height="376" alt="Faceted Browsing in Dolphin" title="Faceted Browsing in Dolphin" /></a>
<br />
<em>Faceted Browsing in Dolphin</em>
</div>
<p>
Another big ticket item is the introduction of <b>Akonadi</b> in Kontact 4.6, including a mobile version of the powerful groupware client. Many more new features coming with 4.6 can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.6_Feature_Plan">Feature Plan for 4.6</a>.
</p>
<p>
These are only some improvements KDE delivers with 4.6 coming January. This release being a first beta version, we invite testers to participate in the bug-hunting season via <a href="http://bugs.kde.org">KDE's bug tracker</a>. Many more bugfixes and other improvements complement these bigger changes, many of which can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.6_Feature_Plan">Feature Plan for 4.6</a>.
</p>



<h1>KDE Frameworks 4.6 Beta1 Previews Support for upower, udisks and udev</h1>
<p>
The two most significant changes in the KDE development frameworks 4.6 release targets the move away from HAL, which is being phased out by many Linux vendors, towards the <i>u*</i> hardware abstraction components.
</p>
<p>
<ul>
    <li>
    The introduction of <b>device targets</b> makes it easier to customize build and installation of KDE's frameworks, providing leaner install options for <b>mobile target devices</b>. By reducing dependencies between libraries that together form the KDE Platform, these parts become usable individually, thanks to a higher degree of modularization.
    </li>
    <li>
    KDE's Solid framework, a Qt-based API for hardware interaction adds support for <b>upower, udev and udisks</b>, allowing applications and the workspace to run without HAL. The power management framework, Powerdevil 2.0, introduces extendable actions for power-related events, making it easier to customize power management behavior for a certain device or use case. Applications using the Solid framework do not need to be changed to benefit from the new backends.
    </li>
</ul>
</p>
<p>
Many more bugfixes and other improvements complement these bigger changes, many of which can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.6_Feature_Plan">Feature Plan for 4.6</a>. This release being a first beta version, we encourage users to give this release a try and help us improve these frameworks via <a href="http://bugs.kde.org">KDE's bug tracker</a>, but not use it in production environments, as the stabilization phase is still under way.
</p>



<!-- // Boilerplate again -->

<h4>
  Download and Installation
</h4>
<p align="justify">
KDE's software, including all its libraries and its applications, is available for free
under Open Source licenses. KDE software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/unstable/4.5.80/">http://download.kde.org</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.5.80
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks. For a current list of available 
binary packages of which the KDE Project has been informed, please visit the 
<a href="/info/4.5.80.php#binary">4.5.80 Info Page</a>.
</p>

<p align="justify">
  <em>Compiling</em>
  <a name="source_code"></a>
  The complete source code for 4.5.80 may be <a
href="http://download.kde.org/stable/4.5.80/src/">freely downloaded</a>.
Instructions on compiling and installing 4.5.80
  are available from the <a href="/info/4.5.80.php">4.5.80 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or 
become a KDE e.V. supporting member through our new 
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
