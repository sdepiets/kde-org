

<?php
  $page_title = "KDE Launches User Forum";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- // Boilerplate -->
<p>
<strong>
KDE Community Launches Web-based Bulletin Board Using MyBB</strong>
</p>
<p>
The KDE Community today launches the new <a href="http://forum.kde.org">KDE
Forum</a>. The new forum uses the bulletin board software <a
href="http://www.mybboard.net/">MyBB</a> offering users, developers and people
interested in KDE a place to help each other, discuss KDE-related topics and
exchange ideas. The KDE Forum complements 
<a href="http://userbase.kde.org">KDE's UserBase</a>, the home for KDE users
as a valuable support resource.
<p />
In collaboration with KDE, the developers behind MyBB have 
<a href="http://community.mybboard.net/thread-38942.html">decided to release</a>
their forum software under the terms of the GPL Version 3. Chris Boulton, project manager
at the MyBB Group <em>"The KDE forum guys have come to us with the wish to use our
forum software. In this process, we needed to clear out our licensing, also to
make it easier for the KDE community to leverage MyBB. As a result of that, we
release the MyBB forum now under the terms of the GPL. This was very well-received 
by the community around MyBB"</em> Entirely in the spirit of Free Software, the KDE
Forum team has already contributed a number of plugins for MyBB, supporting the 
decision taken by the MyBB team.<br />
Rob la Lau, initiator and administrator of the KDE Forum adds <em>"We've
chosen MyBB as it stands out head and shoulders above other candidates we have
considered to adopt for the new KDE Forums. We know that a good piece of
software makes a lot of difference to the user, after all we want to create a
nice place for everybody to chat about KDE and related questions."</em>
<p />
Sebastian K&uuml;gler, member of the KDE e.V. Board of Directors explains where the
new KDE Forum fits into KDE communication infrastructure: <em>"We've come to
believe that mailinglist don't cut it for all users. KDE becoming increasingly
widely used should also offer those that aren't familiar with the use of email
for such discussions a place. The new KDE forum complements KDE's new end-user
knowledge base Userbase that has been launched last month. At the same time,
we're really happy to see that after our request the MyBB team has decided 
to release their forum software under the terms of the GPL."</em>
</p>

<h2>The Forums</h2>

The forum offers a number of categories
<ul>
	<li><strong>Forum</strong> lists announcement and has a section for
        providing feedback about the forum.</li>
	<li>In <strong>News &nbsp; Releases</strong> you can discuss the latest and
        greatest KDE news, and even follow commits t o the KDE codebase in real
        time.</li>
	<li>The largest category is for <strong>Users</strong>, different KDE apps
        and modules are covered in the subfora here.</li>
	<li>The <strong>Operating Systems</strong> category gives those seeking to
        discuss platform-specific issues a place to post to. </li>
	<li>Want to get feedback about your code? Need to get more detailed
        explanations of a certain topic? The  <strong>Developers</strong> section is
        there for you. </li>
	<li>The <strong>Other Languages</strong> has pointers to local KDE Forums.
        These forums are not part of the "KDE Forums" proper, but nevertheless a place
        you might find answers to your questions and like-minded people. </li>
</ul>
<p>
The KDE Forums admins have set the structure up as a starting point. Sub-communities or
interesting parties can request their own subforum. The KDE Forum team is working
on integrating the new KDE Forum with KDE's existing infrastructure by means of
IRC bots that can announce new threads and posts on IRC channels, by briding
forum posts to mailinglist messages and by offering RSS feeds. 

<p />
So get your account and join the discussion now.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>

