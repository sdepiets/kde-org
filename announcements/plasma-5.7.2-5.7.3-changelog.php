<?php
include_once ("functions.inc");
$translation_file = "kde-org";
$page_title = i18n_noop("Plasma 5.7.3 Complete Changelog");
$site_root = "../";
$release = 'plasma-5.7.3';
include "header.inc";
?>
<p><a href="plasma-5.7.3.php">Plasma 5.7.3</a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Fallback to asking the license to the package manager. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=422191d6a1203eef83a8a6c304b449c322782819'>Commit.</a> </li>
<li>Disable changelogs while updating. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=242fa0f9e72aff82fa0a6e2d3299b73c82754b51'>Commit.</a> </li>
<li>Let PresentUpdatesPage decide when to request the changelog. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=963faeb583717193046cc581ac8cdf8640432e0b'>Commit.</a> </li>
<li>Make sure we clean up resources properly on pk backend. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ba383a1849aa5c4c79f0903ab86e656e9e0c6d09'>Commit.</a> </li>
<li>Don't fetch the changelogs before setting the resources. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6623c3cc42d63b990368e230ee1a9a1698a00a65'>Commit.</a> </li>
<li>Make sure we'll get Discover if present. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=cb340fdf9a2607a1279d714b65d87f6cec54a6ab'>Commit.</a> </li>
</ul>


<h3><a name='kactivitymanagerd' href='http://quickgit.kde.org/?p=kactivitymanagerd.git'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>Allowing kdeglobals to override the default activity name. <a href='http://quickgit.kde.org/?p=kactivitymanagerd.git&amp;a=commit&amp;h=a511a556ab427aa84eb0d8e9c5d1ac1d4e59aba4'>Commit.</a> </li>
</ul>


<h3><a name='kinfocenter' href='http://quickgit.kde.org/?p=kinfocenter.git'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Always load about-distro in ctor. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=8600dff7216c8f983ce991ac2e856b3aa7df28b7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366158'>#366158</a></li>
</ul>


<h3><a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>[kcm] Show output name in output preview. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=71c5187ac9d3dc23989352322a31e445f7938dd9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362570'>#362570</a></li>
<li>Kscreen kcm: block changed signal after save. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=87d3d91202da261eef23659e1a11b5bec77742e0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365467'>#365467</a></li>
</ul>


<h3><a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Handle restart of Compositor Scene correctly for Wayland client. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0286882b3f80b3b68f904dc5d6721e0da640712a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365471'>#365471</a></li>
<li>[kcmkwin/compositing] Remove combobox to select between GLX and EGL. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d52944c2c7585c4d1ffb16b78682f7a6142789d4'>Commit.</a> </li>
</ul>


<h3><a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Fixed tabwidget size calculation from contents. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=7d989d35643adfcd987560b9d3266b76edbf8246'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364426'>#364426</a></li>
</ul>


<h3><a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Fix launching new instance by Shift+LeftMouse. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=266bb7f03a714d9c9d07811247635d3c1aa888f0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366340'>#366340</a></li>
<li>Speed up loading KCM with FileDialog lazy loading. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=60e970472a5ed7cb3a7e58419fe42d80412a32d7'>Commit.</a> </li>
<li>[Kicker] Disable PackageKit integration (aka "Uninstall app") context menu entry. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7dfb92b08584c79ef18a1a709f53759899eeac87'>Commit.</a> </li>
<li>Remove dead code. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=79f842066d0544d879434b50aecee2cff7156336'>Commit.</a> See bug <a href='https://bugs.kde.org/365295'>#365295</a></li>
<li>Anchors in a layout item is always wrong. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=30ed2b91278356fe08232977358ccf4692cc4d4c'>Commit.</a> </li>
<li>Workaround searching lockup. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=14d48bac5f95e4e276b227023fed97d9cdeb6157'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365868'>#365868</a></li>
<li>Correct typo. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=61389485e8148709786d9a77faefc24abe5d51d7'>Commit.</a> </li>
<li>Use desktopsForActivty(currentActivity());. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3b123057a01bff0ae296db7214ee98eb79c22c6f'>Commit.</a> </li>
<li>Fixed first time initialization. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=89cb478eb205e5586751311594f2dcf4ec447199'>Commit.</a> </li>
<li>Make sure the busy indicator is destroyed when a delegate is reused. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ec75a1f68cc8963ea41d794eb79dcef5506c324b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365183'>#365183</a></li>
<li>Disable hover event handling until Flow positions delegate. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=605d34e3ff8007371e013a2a68647b9f3d8b5bac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366153'>#366153</a></li>
<li>[Kicker] Add missing config-workspace.h include to appentry.cpp, otherwise it doesn't know if PackageKitQt5_FOUND was defined or not. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8eaf1e15ad8662b0ef72d83a242893b27f8058df'>Commit.</a> </li>
<li>Prepare for retiring the C++ text label in Task Manager delegates. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c82f9207cda84371a7bda25f8727dfb8cec301d6'>Commit.</a> </li>
<li>Don't leak the dialog. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2fafa2dfd5d7c8fd1f43c93b9a91ab762946fb33'>Commit.</a> </li>
<li>Connect to a signal actually emitted. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7184686f14dd42ce0c378a9e22761a2c71526483'>Commit.</a> </li>
<li>Fix changes of the filter pattern not being applied immediately. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f93e46bbb7849907c647f7e7dfc59c7fd3e3d09d'>Commit.</a> See bug <a href='https://bugs.kde.org/365792'>#365792</a></li>
<li>Loop trough screens only once. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=88e31e49967e56319f0737d3d6d2ab46d2073f51'>Commit.</a> </li>
<li>Don't activate it on release events when a drag was started prior to it. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0065d3548c7db695b06d80250b11d018214967dc'>Commit.</a> See bug <a href='https://bugs.kde.org/360219'>#360219</a></li>
<li>Icontasks is always separate-launchers. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=843d5185c397a2d06878dea18bb23637648eec25'>Commit.</a> </li>
<li>Fix inverted logic. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6d3527cbe5f1c26d4eff61d0d959c70943ca296e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365888'>#365888</a></li>
</ul>


<h3><a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Fix data validation when creating new pppoe connections. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=ad421d63cdeb9751dfd4f411e6c114a703b9e3e9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362175'>#362175</a></li>
<li>Fix crash when activating VPN connection. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=e6e341a5c2c1bc9292e1e67603ea52795e33fe8a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366164'>#366164</a></li>
<li>Update security type from wireless network when merging with connection. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=c647ea09380cd3e1f3f80d4ee293b9bc8480f79a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365704'>#365704</a></li>
</ul>


<h3><a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Check harder to include only desktops in desktops(). <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a2850caf4a442c5e92d4c027b9a1c78c9dee24b5'>Commit.</a> </li>
<li>Properly registering existing activities before loading layout.js. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=475bed7373142afe20927acbc465f269cfa59878'>Commit.</a> </li>
<li>Allow lnf package to specify a default cont. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b15d7a494e3d22e5e361e2ac3330fa4141bb33e5'>Commit.</a> </li>
<li>Apparently containment() can be null. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=214443fdff330629104509de9e1738aadcfe36b6'>Commit.</a> See bug <a href='https://bugs.kde.org/365989'>#365989</a></li>
<li>Treat IsDemandingAttention as IsOnAllVirtualDesktops. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=da5c2fd5058e60d12bfbdfa2b1863433ab20dfbb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365970'>#365970</a></li>
<li>[shell] Tests moved to new plasma-tests repository. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3799151fd53d3aa645ac15785d1f809d3913366b'>Commit.</a> </li>
<li>Don't try to load layout before kamd starts. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f7ef6ee87b8957bebc976b6fc9e0df279cea05f1'>Commit.</a> </li>
<li>Reposition the krunner window if the width changes. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=23f5907256fa439f8e77f204f184693dea513585'>Commit.</a> </li>
</ul>


<h3><a name='user-manager' href='http://quickgit.kde.org/?p=user-manager.git'>User Manager</a> </h3>
<ul id='uluser-manager' style='display: block'>
<li>Fix build. <a href='http://quickgit.kde.org/?p=user-manager.git&amp;a=commit&amp;h=f2f447e662a8ad57458d36cd71afc7e3b186f028'>Commit.</a> </li>
<li>Fix memory leak. <a href='http://quickgit.kde.org/?p=user-manager.git&amp;a=commit&amp;h=e9c7e86d47b6ef04ea9fbe4efadb825c55b297e2'>Commit.</a> </li>
<li>Fix typo in UserManager model roles. <a href='http://quickgit.kde.org/?p=user-manager.git&amp;a=commit&amp;h=eff5c5baf659e40f6a5a34275e5d801f54db48ff'>Commit.</a> </li>
</ul>


<?php
  include("footer.inc");
?>
