<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.14.4 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.14.4";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Fix uncentered crosshairs. <a href='https://commits.kde.org/breeze/a7d07492bf65c41d23505648ca363a7d0e54da86'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17038'>D17038</a></li>
<li>Improve contrast for crosshair cursors. <a href='https://commits.kde.org/breeze/c92f9d186d1fc0931fdc278593f2fbb3fb6feebe'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400110'>#400110</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16861'>D16861</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Kns: fix crash upon removing a category. <a href='https://commits.kde.org/discover/8a6a4b2c566c97d41d9fb8eac5727b3ac3292651'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401435'>#401435</a></li>
<li>Don't use an icon that is only supposed for plasmoids. <a href='https://commits.kde.org/discover/83e8032f79e9e7a21b06c3f5bb8011aa7217dedb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16900'>D16900</a></li>
<li>Flatpak: Always connect to finished before running. <a href='https://commits.kde.org/discover/eb224004994a2f6dd80d0966f8924146c17d43fd'>Commit.</a> </li>
<li>Remove unused function. <a href='https://commits.kde.org/discover/14900b7baa195d9d9660cf99a9a4fb8f9c7da16e'>Commit.</a> </li>
<li>Fix global progress display for updates. <a href='https://commits.kde.org/discover/119c3ffcba622cbc136ec0adcea8b7518379aed2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400891'>#400891</a></li>
<li>Avoid potential race condtion with QFutureWatcher in FWUPD backend. <a href='https://commits.kde.org/discover/bd6f08ada8e3263851e7ffd935ac5b4cd9d4ca9c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16812'>D16812</a></li>
<li>Flatpak: remove duplicated line. <a href='https://commits.kde.org/discover/7a5465680e7d2308952604e19948dc91fb182b0b'>Commit.</a> </li>
</ul>


<h3><a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>Drop ancient commented out code. <a href='https://commits.kde.org/kactivitymanagerd/dd5cfbb6f50ec33924505e89f81b07829083db59'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17118'>D17118</a></li>
<li>Actually initialize kcrash properly. <a href='https://commits.kde.org/kactivitymanagerd/699dbef8f577e306aea8751671ab66d5068774d4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17119'>D17119</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[comic] Correct usage of DataEngineConsumer. <a href='https://commits.kde.org/kdeplasma-addons/0f79d76cf9327b9ab5f1bea80a59ac6d1d44c1ac'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16844'>D16844</a></li>
<li>[comic] Fix error handling in data updated. <a href='https://commits.kde.org/kdeplasma-addons/5b412f94ff39bfd19c2a56b039994d0fa2ed94aa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17095'>D17095</a></li>
<li>[comic] Fix widget context menu. <a href='https://commits.kde.org/kdeplasma-addons/ce5bc062fe15b63fdc9d518c99fd6502cc036fda'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17094'>D17094</a></li>
<li>[KonsoleProfiles applet] Fix initial focus. <a href='https://commits.kde.org/kdeplasma-addons/d6ee79f20adbdc685edc98babccbe71d812de131'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15963'>D15963</a></li>
<li>[weather] Fix broken observation display for temperature of 0 °. <a href='https://commits.kde.org/kdeplasma-addons/0d379c5957e2b69e34839535d1620651c1988e54'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16977'>D16977</a></li>
<li>Datetime runner: Match timezones less restrictively. <a href='https://commits.kde.org/kdeplasma-addons/499b9e49960bb3cb58a8f8d850fbd8c0b6839de7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17064'>D17064</a></li>
<li>Keyboard indicator: Fix warnings. <a href='https://commits.kde.org/kdeplasma-addons/bdcb3ce576bfabea5857c0c9eafbf64176bf9769'>Commit.</a> </li>
<li>[Comics & Weather widgets] give "Configure..." buttons icons. <a href='https://commits.kde.org/kdeplasma-addons/bd558d122885aa7119a5acc2f787e868c2436100'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399888'>#399888</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16287'>D16287</a></li>
<li>[Media Frame] Hide back/forward buttons altogether if there is only one image. <a href='https://commits.kde.org/kdeplasma-addons/6dedea6038c0c22553eab887b8a3fb15761adca9'>Commit.</a> </li>
<li>[Media Frame] Fix futureLength property. <a href='https://commits.kde.org/kdeplasma-addons/2c8d05fae8314024a38f9777bdccb85bdec78a5c'>Commit.</a> </li>
<li>[KonsoleProfiles applet] Fix navigating with the keyboard. <a href='https://commits.kde.org/kdeplasma-addons/943122b4429de6ceb113fb7b3ede6c2b4036965d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15877'>D15877</a></li>
<li>[DateTime Runner] Fix a copyright typo. <a href='https://commits.kde.org/kdeplasma-addons/ed2e23bc44f2ac78415723a997bab74dc6622f21'>Commit.</a> </li>
<li>Use KDEFrameworkCompilerSettings. <a href='https://commits.kde.org/kdeplasma-addons/5444724f0189dc2a68b92427f4e034da6760dac5'>Commit.</a> </li>
</ul>


<h3><a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Prefer to set current mode in resolution combobox. <a href='https://commits.kde.org/kscreen/962ca65570f9c6fb4fb2bde306b45a175ad6a576'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16400'>D16400</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Actually initialize kcrash for xclipboardsync. <a href='https://commits.kde.org/kwin/aace9b1675819feab81d50f694391c7d854d9822'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16923'>D16923</a></li>
<li>[wayland] Don't crash when resizing windows. <a href='https://commits.kde.org/kwin/406b70b04e093c13faf763e2d885797ae037d806'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397577'>#397577</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16846'>D16846</a></li>
<li>[effects/slidingpopups] Don't crash when sliding virtual desktops. <a href='https://commits.kde.org/kwin/ad28da84e78c7eb7ff1e608c4819707b2142daea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400170'>#400170</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16731'>D16731</a></li>
</ul>


<h3><a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a> </h3>
<ul id='ulplasma-browser-integration' style='display: block'>
<li>Include actual player source in metadata. <a href='https://commits.kde.org/plasma-browser-integration/18eb942ba230f943d39289ba156bb964951f6051'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17170'>D17170</a></li>
<li>Listen to KDE Connect device signals. <a href='https://commits.kde.org/plasma-browser-integration/b4a5f02482f6f379125bdc18d35f0587c78ab49a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16889'>D16889</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[Folder View] improve label contrast against challenging backgrounds. <a href='https://commits.kde.org/plasma-desktop/10278e79f11677bd59f7d554eb8e18e580686082'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361228'>#361228</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16968'>D16968</a></li>
<li>[Componentchooser KCM] Make default browser app choice work even when combobox is not used. <a href='https://commits.kde.org/plasma-desktop/26fb5ec20af60682561c7a9678506199966ef2c1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350663'>#350663</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17181'>D17181</a></li>
<li>Minor: Fix -Wextra-semi warning. <a href='https://commits.kde.org/plasma-desktop/00eda23e92aa37c67a979769b803952897b711b0'>Commit.</a> </li>
<li>Fix dismissing the Dashboard by clicking inbetween multi-grid categories. <a href='https://commits.kde.org/plasma-desktop/e12e3cad1d7ee8c2f95cecd5939a648b508a408e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400720'>#400720</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17005'>D17005</a></li>
<li>Revert unintended line. <a href='https://commits.kde.org/plasma-desktop/454347370883c7c9b4e8cb29a6cdcffea9553326'>Commit.</a> </li>
<li>Compress calls to `updateSize`. <a href='https://commits.kde.org/plasma-desktop/ab26ebb18b74d8def8e653dc94516fa7da935a5c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400364'>#400364</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17006'>D17006</a></li>
<li>Remove debug. <a href='https://commits.kde.org/plasma-desktop/f8f8b2763e3a3bfc4811fec81de896f3974f7aa9'>Commit.</a> </li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Openconnect: do not reload the auth dialog endlessly when group changes. <a href='https://commits.kde.org/plasma-nm/00d18e2d9cef69cd8066b63c2c5dc40748c8e61e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395157'>#395157</a></li>
<li>VPN plugins: fix broken layout when using password field. <a href='https://commits.kde.org/plasma-nm/1d8acad500f00f67fd60e30fa74361eb7a0e6c11'>Commit.</a> </li>
<li>Do not request secrets for VPN connections all the time. <a href='https://commits.kde.org/plasma-nm/6f54df7f2476e62843066e6fa9d9433397dfbd83'>Commit.</a> </li>
<li>SSH VPN: put content of the configuration into scroll area. <a href='https://commits.kde.org/plasma-nm/9bca702c164d97f3fb7484b54fad108737e95973'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401004'>#401004</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Fix i18n when the kcm code is embedded in the plasmoid config. <a href='https://commits.kde.org/plasma-pa/891b38da6a2b5ed4554ee7786cec7dd3af3e2db2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16595'>D16595</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Fixed global menu misbehaving and unexpectedly closing with certain applications. <a href='https://commits.kde.org/plasma-workspace/73339208cd3469e37eb0ef5555e3de5544cb0a8e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399975'>#399975</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16297'>D16297</a></li>
<li>[Baloo Runner] Filter duplicates and prefer specific type matches. <a href='https://commits.kde.org/plasma-workspace/867c864e7c0734277497cad0fb5362e9ffd02cc2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16690'>D16690</a></li>
<li>[clipboard] Limit length of clipboard text item visualisation. <a href='https://commits.kde.org/plasma-workspace/d53622d7d9e08a589809c8d820b5fa2c31d98c4b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16842'>D16842</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
