<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.70.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.70.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
May 02, 2020. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.70.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("[FileWatch] Remove redundant watchIndexedFolders() slot");?></li>
<li><?php i18n("[ModifiedFileIndexer] Clarify a comment");?></li>
<li><?php i18n("[FileWatch] Fix watch updates on config changes");?></li>
<li><?php i18n("[KInotify] Fix path matching when removing watches");?></li>
<li><?php i18n("[Extractor] Use categorized logging");?></li>
<li><?php i18n("Use KFileMetaData for XAttr support instead of private reimplementation");?></li>
<li><?php i18n("Revert \"add Baloo DBus signals for moved or removed files\"");?></li>
<li><?php i18n("[QML Monitor] Show remaining time as soon as possible");?></li>
<li><?php i18n("[FileContentIndexer] Fix state update and signal order");?></li>
<li><?php i18n("[Monitor] Fix monitor state and signal ordering");?></li>
<li><?php i18n("[Extractor] Fix progress reporting");?></li>
<li><?php i18n("[Coding] Avoid recurrent detach and size checks");?></li>
<li><?php i18n("[baloo_file] Remove KAboutData from baloo_file");?></li>
<li><?php i18n("[Searchstore] Reserve space for phrase query terms");?></li>
<li><?php i18n("[SearchStore] Allow querying exact matches for non-properties");?></li>
<li><?php i18n("[PhraseAndIterator] Remove temporary arrays when checking matches");?></li>
<li><?php i18n("[Extractor] Better balance idle and busy mode");?></li>
<li><?php i18n("[Extractor] Fix idle monitoring");?></li>
<li><?php i18n("[Extractor] Remove IdleStateMonitor wrapper class");?></li>
<li><?php i18n("[OrpostingIterator] Allow skipping elements, implement skipTo");?></li>
<li><?php i18n("[PhraseAndIterator] Replace recursive next() implementation");?></li>
<li><?php i18n("[AndPostingIterator] Replace recursive next() implementation");?></li>
<li><?php i18n("[PostingIterator] Make sure skipTo also works for first element");?></li>
<li><?php i18n("rename and export newBatchTime signal in filecontentindexer");?></li>
<li><?php i18n("[SearchStore] Handle double values in property queries");?></li>
<li><?php i18n("[AdvancedQueryParser] Move semantic handling of tokens to SearchStore");?></li>
<li><?php i18n("[Inotify] Remove not-so-OptimizedByteArray");?></li>
<li><?php i18n("[Inotify] Remove dead/duplicate code");?></li>
<li><?php i18n("[QueryParser] Replace single-use helper with std::none_of");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Move document corner fold to top right in two icons");?></li>
<li><?php i18n("Add 16px konversation icon");?></li>
<li><?php i18n("correct vscode icon name");?></li>
<li><?php i18n("Add 16px Vvave icon");?></li>
<li><?php i18n("add alligator icon");?></li>
<li><?php i18n("Add preferences-desktop-tablet and preferences-desktop-touchpad icons");?></li>
<li><?php i18n("Update links in README.md");?></li>
<li><?php i18n("build: quote source directory path");?></li>
<li><?php i18n("Allow building from a read-only source location");?></li>
<li><?php i18n("Add expand/collapse icons to accompany existing expand-all/collapse-all icons");?></li>
<li><?php i18n("Add auth-sim-locked and auth-sim-missing");?></li>
<li><?php i18n("Add sim card device icons");?></li>
<li><?php i18n("Add rotation icons");?></li>
<li><?php i18n("Add 16px System Settings icon");?></li>
<li><?php i18n("Change ButtonFocus to Highlight");?></li>
<li><?php i18n("Improve the look of kcachegrind");?></li>
<li><?php i18n("Remove border from format-border-set-* icons");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("android: include the architecture on the apk name");?></li>
<li><?php i18n("ECMAddQch: fix use of quotation marks with PREDEFINED in doxygen config");?></li>
<li><?php i18n("Adapt FindKF5 to stricter checks in newer find_package_handle_standard_args");?></li>
<li><?php i18n("ECMAddQch: help doxygen to handle Q_DECLARE_FLAGS, so such types get docs");?></li>
<li><?php i18n("Fix wayland scanner warnings");?></li>
<li><?php i18n("ECM: attempt to fix KDEInstallDirsTest.relative_or_absolute on Windows");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Fix missing whitespace after \"Platform(s):\" on front page");?></li>
<li><?php i18n("Fix use of quotation marks for PREDEFINED entries in Doxygile.global");?></li>
<li><?php i18n("Teach doxygen about Q_DECL_EQ_DEFAULT &amp; Q_DECL_EQ_DELETE");?></li>
<li><?php i18n("Add drawer on mobile and clean code");?></li>
<li><?php i18n("Teach doxygen about Q_DECLARE_FLAGS, so such types can be documented");?></li>
<li><?php i18n("Port to Aether Bootstrap 4");?></li>
<li><?php i18n("Redo api.kde.org to look more like Aether");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Always create actioncollection");?></li>
<li><?php i18n("[KBookMarksMenu] Set objectName for newBookmarkFolderAction");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("KSettings::Dialog: add support for KPluginInfos without a KService");?></li>
<li><?php i18n("Small optimization: call kcmServices() only once");?></li>
<li><?php i18n("Downgrade warning about old-style KCM to qDebug, until KF6");?></li>
<li><?php i18n("Use ecm_setup_qtplugin_macro_names");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("kconfig_compiler : generate kconfig settings with subgroup");?></li>
<li><?php i18n("Fix some compiler warnings");?></li>
<li><?php i18n("Add force save behavior to KEntryMap");?></li>
<li><?php i18n("Add standard shortcut for \"Show/Hide Hidden Files\" (bug 262551)");?></li>
</ul>

<h3><?php i18n("KContacts");?></h3>

<ul>
<li><?php i18n("Align description in metainfo.yaml with the one of README.md");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("API dox: use ulong typedef with Q_PROPERTY(percent) to avoid doxygen bug");?></li>
<li><?php i18n("API dox: document Q_DECLARE_FLAGS-based flags");?></li>
<li><?php i18n("Mark ancient KLibFactory typedef as deprecated");?></li>
<li><?php i18n("[KJobUiDelegate] Add AutoHandlingEnabled flag");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("Drop klauncher usage from KCrash");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Properly name the content of the kcmcontrols project");?></li>
<li><?php i18n("Tweak kcmcontrols docs");?></li>
<li><?php i18n("Add startCapture method");?></li>
<li><?php i18n("[KeySequenceHelper] Work around Meta modifier behavior");?></li>
<li><?php i18n("Also release the window in the destructor");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Port KToolInvocation::kdeinitExecWait to QProcess");?></li>
<li><?php i18n("Drop delayed second phase");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Nicaragua Holidays");?></li>
<li><?php i18n("Taiwanese holidays");?></li>
<li><?php i18n("Updated Romanian holidays");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("KI18N_WRAP_UI macro: set SKIP_AUTOUIC property on ui file and gen. header");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Add note about porting loadMimeTypeIcon");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("Add support for modern Gimp images/XCF files");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("[RenameDialog] Add an arrow indicating direction from src to dest (bug 268600)");?></li>
<li><?php i18n("KIO_SILENT Adjust API docs to match reality");?></li>
<li><?php i18n("Move handling of untrusted programs to ApplicationLauncherJob");?></li>
<li><?php i18n("Move check for invalid service from KDesktopFileActions to ApplicationLauncherJob");?></li>
<li><?php i18n("Detect executables without +x permission in $PATH to improve error message (bug 415567)");?></li>
<li><?php i18n("Make the HTML file template more useful (bug 419935)");?></li>
<li><?php i18n("Add JobUiDelegate constructor with AutoErrorHandling flag and window");?></li>
<li><?php i18n("Fix cache directory calculation when adding to trash");?></li>
<li><?php i18n("File protocol: ensure KIO::StatAcl works without implicitly depending on KIO::StatBasic");?></li>
<li><?php i18n("Add KIO::StatRecursiveSize detail value so kio_trash only does this on demand");?></li>
<li><?php i18n("CopyJob: when stat'ing the dest, use StatBasic");?></li>
<li><?php i18n("[KFileBookMarkHandler] Port to new KBookmarkMenu-5.69");?></li>
<li><?php i18n("Mark KStatusBarOfflineIndicator as deprecated");?></li>
<li><?php i18n("Replace KLocalSocket with QLocalSocket");?></li>
<li><?php i18n("Avoid crash in release mode after the warning about unexpected child items (bug 390288)");?></li>
<li><?php i18n("Docu: remove mention of non-existing signal");?></li>
<li><?php i18n("[renamedialog] Replace KIconLoader usage with QIcon::fromTheme");?></li>
<li><?php i18n("kio_trash: Add size, modification, access and create date for trash:/ (bug 413091)");?></li>
<li><?php i18n("[KDirOperator] Use new \"Show/Hide Hidden Files\" standard shortcut (bug 262551)");?></li>
<li><?php i18n("Show previews on encrypted filesystems (bug 411919)");?></li>
<li><?php i18n("[KPropertiesDialog] Disable changing remote dir icons (bug 205954)");?></li>
<li><?php i18n("[KPropertiesDialog] Fix QLayout warning");?></li>
<li><?php i18n("API dox: document more of the default property values of KUrlRequester");?></li>
<li><?php i18n("Fix DirectorySizeJob so it doesn't depend on listing order");?></li>
<li><?php i18n("KRun: fix assert when failing to start an application");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Introduce Theme::smallFont");?></li>
<li><?php i18n("Make BasicListItem more useful by giving it a subtitle property");?></li>
<li><?php i18n("Less segfaulty PageRouterAttached");?></li>
<li><?php i18n("PageRouter: find parents of items better");?></li>
<li><?php i18n("Remove unused QtConcurrent from colorutils");?></li>
<li><?php i18n("PlaceholderMessage: Remove Plasma units usage");?></li>
<li><?php i18n("Allow PlaceholderMessage to be text-less");?></li>
<li><?php i18n("vertically center sheets if they don't have a scrollbar (bug 419804)");?></li>
<li><?php i18n("Account for top and bottom margin in default card height");?></li>
<li><?php i18n("Various fixes to new Cards (bug 420406)");?></li>
<li><?php i18n("Icon: improve icon rendering on multi-screen multi-dpi setups");?></li>
<li><?php i18n("Fix error in PlaceholderMessage: actions are disabled, not hidden");?></li>
<li><?php i18n("Introduce PlaceholderMessage component");?></li>
<li><?php i18n("Hotfix: fix bad typing in FormLayout array functions");?></li>
<li><?php i18n("Hotfix for SwipeListItem: use Array.prototype.*.call");?></li>
<li><?php i18n("Hotfix: use Array.prototype.some.call in ContextDrawer");?></li>
<li><?php i18n("Hotfix for D28666: use Array.prototype.*.call instead of invoking functions on 'list' objects");?></li>
<li><?php i18n("Add missing m_sourceChanged variable");?></li>
<li><?php i18n("Use ShadowedRectangle for Card backgrounds (bug 415526)");?></li>
<li><?php i18n("Update the visibility check for ActionToolbar by checking width with less-\"equal\"");?></li>
<li><?php i18n("Couple of 'trivial' fixes for broken code");?></li>
<li><?php i18n("never close when the click is inside the sheet contents (bug 419691)");?></li>
<li><?php i18n("sheet must be under other popups (bug 419930)");?></li>
<li><?php i18n("Add PageRouter component");?></li>
<li><?php i18n("Add ColorUtils");?></li>
<li><?php i18n("Allow setting separate corner radii for ShadowedRectangle");?></li>
<li><?php i18n("Remove the STATIC_LIBRARY option to fix static builds");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("Add KDialogJobUiDelegate(KJobUiDelegate::Flags) constructor");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Implement UString operator= to make gcc happy");?></li>
<li><?php i18n("Silence compiler warning about copy of non-trivial data");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("KNewStuff: Fix file path and process call (bug 420312)");?></li>
<li><?php i18n("KNewStuff: port from KRun::runApplication to KIO::ApplicationLauncherJob");?></li>
<li><?php i18n("Replace Vokoscreen with VokoscreenNG (bug 416460)");?></li>
<li><?php i18n("Introduce more user-visible error reporting for installations (bug 418466)");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Implement updating of notifications on Android");?></li>
<li><?php i18n("Handle multi-line and rich-text notifications on Android");?></li>
<li><?php i18n("Add KNotificationJobUiDelegate(KJobUiDelegate::Flags) constructor");?></li>
<li><?php i18n("[KNotificationJobUiDelegate] Append \"Failed\" for error messages");?></li>
</ul>

<h3><?php i18n("KNotifyConfig");?></h3>

<ul>
<li><?php i18n("Consistently use knotify-config.h to pass in flags about Canberra/Phonon");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Add StatusBarExtension(KParts::Part *) overloaded constructor");?></li>
</ul>

<h3><?php i18n("KPlotting");?></h3>

<ul>
<li><?php i18n("Port foreach (deprecated) to range for");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("DBus Runner: Add service property to request actions once (bug 420311)");?></li>
<li><?php i18n("Print a warning if runner is incompatible with KRunner");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Deprecate KPluginInfo::service(), since the constructor with a KService is deprecated");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("fix drag'n'drop on left side border widget (bug 420048)");?></li>
<li><?php i18n("Store and fetch complete view config in and from session config");?></li>
<li><?php i18n("Revert premature port to unreleased Qt 5.15 which changed meanwhile");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("[NestedListHelper] Fix indentation of selection, add tests");?></li>
<li><?php i18n("[NestedListHelper] Improve indentation code");?></li>
<li><?php i18n("[KRichTextEdit] Make sure headings don't mess with undo stack");?></li>
<li><?php i18n("[KRichTextEdit] Fix scroll jumping around when horizontal rule is added (bug 195828)");?></li>
<li><?php i18n("[KRichTextWidget] Remove ancient workaround and fix regression (commit 1d1eb6f)");?></li>
<li><?php i18n("[KRichTextWidget] Add support for headings");?></li>
<li><?php i18n("[KRichTextEdit] Always treat key press as single modification in undo stack (bug 256001)");?></li>
<li><?php i18n("[findreplace] Handle searching for WholeWordsOnly in Regex mode");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Add imperial gallon and US pint (bug 341072)");?></li>
<li><?php i18n("Add Icelandic Krona to currencies");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("[Wayland] Add to PlasmaWindowManagement protocol windows stacking order");?></li>
<li><?php i18n("[server] Add some sub-surface life cycle signals");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("[KFontChooser] Remove NoFixedCheckBox DisplayFlag, redundant");?></li>
<li><?php i18n("[KFontChooser] Add new DisplayFlag; modify how flags are used");?></li>
<li><?php i18n("[KFontChooser] Make styleIdentifier() more precise by adding font styleName (bug 420287)");?></li>
<li><?php i18n("[KFontRequester] Port from QFontDialog to KFontChooserDialog");?></li>
<li><?php i18n("[KMimeTypeChooser] Add the ability to filter the treeview with a QSFPM (bug 245637)");?></li>
<li><?php i18n("[KFontChooser] Make the code slightly more readable");?></li>
<li><?php i18n("[KFontChooser] Add a checkbox to toggle showing only monospaced fonts");?></li>
<li><?php i18n("Remove not necessary include");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Print meaningful warning when there is no QGuiApplication");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("[KRichTextEditor] Add support for headings");?></li>
<li><?php i18n("[KKeySequenceWidget] Work around Meta modifier behavior");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Replace foreach with range-for");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[PlasmaCore.IconItem] Regression: fix crash on source change (bug 420801)");?></li>
<li><?php i18n("[PlasmaCore.IconItem] Refactor source handling for different types");?></li>
<li><?php i18n("Make applet tooltip text spacing consistent");?></li>
<li><?php i18n("[ExpandableListItem] make it touch-friendly");?></li>
<li><?php i18n("[ExpandableListItem] Use more semantically correct expand and collapse icons");?></li>
<li><?php i18n("Fix PC3 BusyIndicator binding loop");?></li>
<li><?php i18n("[ExpandableListItem] Add new showDefaultActionButtonWhenBusy option");?></li>
<li><?php i18n("Remove rounded borders to plasmoidHeading");?></li>
<li><?php i18n("[ExpandableListItem] Add itemCollapsed signal and don't emit itemExpanded when collapsed");?></li>
<li><?php i18n("Add readmes clarifying state of plasma component versions");?></li>
<li><?php i18n("[configview] Simplify code / workaround Qt5.15 crash");?></li>
<li><?php i18n("Create ExpandableListItem");?></li>
<li><?php i18n("Make animation durations consistent with Kirigami values");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Detect QQC2 version at build time with actual detection");?></li>
<li><?php i18n("[ComboBox] Use transparent dimmer");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("[Solid] Port foreach to range/index for");?></li>
<li><?php i18n("[FakeCdrom] Add a new UnknownMediumType enumerator to MediumType");?></li>
<li><?php i18n("[FstabWatcher] Fix loosing of fstab watcher");?></li>
<li><?php i18n("[Fstab] Do not emit deviceAdded twice on fstab/mtab changes");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("debchangelog: add Groovy Gorilla");?></li>
<li><?php i18n("Update Logtalk language syntax support");?></li>
<li><?php i18n("TypeScript: add the \"awaited\" type operator");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.70");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.12");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
