<?php
  $page_title = "KDE Shows Beta of Summer Release";
  $site_root = "../";
  include "header.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  <?php echo $page_title ?>
</h3>

<p align="justify">
  <strong>
KDE Ships First 4.7 Beta
</strong>
</p>

<p align="justify">
May 25th, 2011. Today, KDE has released a first beta of the upcoming 4.7 release of the Plasma Desktop and Netbook workspaces, the KDE Applications and the KDE Frameworks, which is planned for July 27, 2011. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing new and old functionality.
</p>
<p>
The 4.7 release will bring a number of exciting improvments:
<ul>
    <li>KWin, Plasma's window manager <a href="http://www.kdenews.org/2011/02/18/kwin-embraces-new-platforms-opengl-es-20-support">now supports OpenGL-ES 2.0</a>, improving performance and deployability on mobile devices</li>
    <li>Dolphin, KDE's flexible file manager has seen user interface improvements and now sports a better user experience for searching in files' metadata.</li>
    <li>KDM, KDE's login manager <a href="http://ksmanis.wordpress.com/2011/04/21/hello-planet-and-grub2-support-for-kdm/">now interfaces</a> with the Grub2 bootloader</li>
    <li>Marble, the virtual globe now supports <a href="http://nienhueser.de/blog/?p=321">offline address search</a>, especially making its mobile version more useful on the road</li>
</ul>

</p>


<p>To download source code or packages to install go to the <a href="/info/4.6.80.php">4.7 Beta1 Info Page</a>.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="4.6/screenshots/46-w09.png"><img src="4.6/screenshots/thumbs/46-w09.png" align="center" width="600" alt="The KDE Plasma Desktop Workspace" title="The KDE Plasma Desktop Workspace" /></a>
<br />
<em>The KDE Plasma Desktop Workspace</em>
</div>

<p>To find out more about the Plasma Workspace and KDE Applications, please refer to the
<a href="http://www.kde.org/announcements/4.6/">4.6.0</a>,
<a href="http://www.kde.org/announcements/4.5/">4.5.0</a>,
<a href="http://www.kde.org/announcements/4.4/">4.4.0</a>,
<a href="http://www.kde.org/announcements/4.3/">4.3.0</a>,
<a href="http://www.kde.org/announcements/4.2/">4.2.0</a>,
<a href="http://www.kde.org/announcements/4.1/">4.1.0</a> and
<a href="http://www.kde.org/announcements/4.0/">4.0.0</a> release
notes.
<p />

<p align="justify">
The KDE Software Compilation, including all its libraries and its applications, is available for free
under Open Source licenses. KDE's software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/unstable/4.6.80/">http://download.kde.org</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>


<!-- // Boilerplate again -->

<h4>
  Installing 4.7 Beta1 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.7 Beta1
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.6.80.php#binary">4.7 Beta1 Info
Page</a>.
</p>

<h4>
  Compiling 4.7 Beta1
</h4>
<p align="justify">
  <a name="source_code"></a>
  The complete source code for 4.7 Beta1 may be <a
href="http://download.kde.org/stable/4.6.80/src/">freely downloaded</a>.
Instructions on compiling and installing 4.7 Beta1
  are available from the <a href="/info/4.6.80.php">4.7 Beta1 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or 
become a KDE e.V. supporting member through our new 
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
