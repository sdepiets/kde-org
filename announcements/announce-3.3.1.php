<?php
  $page_title = "KDE 3.3.1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>DATELINE October 12, 2004</p>
<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships First Translation and Service Release for Leading Open Source Desktop
</h3>

<p align="justify">
  <strong>
    KDE Project Ships First Translation and Service Release of the 3.3 Generation
    GNU/Linux - UNIX Desktop, Offering Enterprises and Governments a Compelling
    Free and Open Desktop Solution
  </strong>
</p>

<p align="justify">
  October 12, 2004 (The INTERNET).  The <a href="http://www.kde.org/">KDE
  Project</a> today announced the immediate availability of KDE 3.3.1,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes.  KDE 3.3.1
  ships with a basic desktop and eighteen other packages
  (PIM, administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more).  KDE's award-winning tools and
  applications are available in <strong>50 languages</strong>.
</p>

<p align="justify">
  KDE, including all its libraries and its applications, is
  available <em><strong>for free</strong></em> under Open Source licenses.
  KDE can be obtained in source and numerous binary formats from
  <a href="http://download.kde.org/stable/3.3.1/">http://download.kde.org</a> and can
  also be obtained on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
  or with any of the major GNU/Linux - UNIX systems shipping today.
</p>

<h4>
  <a name="changes">Enhancements</a>
</h4>
<p align="justify">
  KDE 3.3.1 is a maintenance release which provides corrections of problems
  reported using the KDE <a href="http://bugs.kde.org/">bug tracking system</a>
  and greatly enhanced support for existing translations and new translations.
</p>
<p align="justify">
  For a more detailed list of improvements since the KDE 3.3 release in
  August, please refer to the
  <a href="changelogs/changelog3_3to3_3_1.php">KDE 3.3.1 Changelog</a>.
</p>
<p>
  Additional information about the enhancements of the KDE 3.3.x release
  series is available in the
  <a href="announce-3.3.php">KDE 3.3 Announcement</a>.
</p>

<h4>
  Installing KDE 3.3.1 Binary Packages
</h4>
<p align="justify">
  <em>Packaging Policies</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.3.1 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="http://www.kde.org/info/3.3.1.php">KDE 3.3.1 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.3.1
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.3.1 may be
  <a href="http://download.kde.org/stable/3.3.1/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.3.1
  are available from the <a href="/info/3.3.1.php#binary">KDE
  3.3.1 Info Page</a>.
</p>

<h4>
  KDE Sponsorship
</h4>
<p align="justify">
  Besides the superb and invaluable efforts by the
  <a href="http://www.kde.org/people/">KDE developers</a>
  themselves, significant support for KDE development has been provided by
  <a href="http://www.mandrakesoft.com/">MandrakeSoft</a>,
  <a href="http://www.trolltech.com/">TrollTech</a> and
  <a href="http://www.suse.com/">SuSE</a>.  In addition,
  the members of the <a href="http://www.kdeleague.org/">KDE
  League</a> provide significant support for KDE promotion,
  <a href="http://www.ibm.com/">IBM</a> has donated significant hardware
  to the KDE Project, and the
  <a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
  and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
  provide most of the Internet bandwidth for the KDE project.  Thanks!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<h4>Press Contacts</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#109;&#00097;i&#108;t&#111;&#058;&#0105;&#110;fo&#x2d;af&#114;ic&#x61;&#x40;k&#100;&#x65;.&#111;&#114;g">&#x69;n&#x66;o-&#00097;f&#x72;&#105;&#x63;a&#64;&#x6b;d&#x65;.o&#x72;&#x67;</a><br />
</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="m&#97;&#x69;&#x6c;&#116;o:in&#x66;o-&#x61;&#x73;i&#0097;&#064;&#00107;de&#00046;org">inf&#111;-a&#115;&#00105;&#x61;&#x40;k&#x64;e.or&#x67;</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="&#109;ai&#x6c;&#00116;o&#00058;in&#x66;o-e&#00117;r&#111;pe&#x40;kde&#x2e;&#111;rg">&#x69;n&#102;o&#45;eur&#111;&#00112;&#101;&#x40;&#x6b;d&#101;.&#x6f;r&#x67;</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="&#x6d;&#x61;ilto:&#x69;&#00110;&#00102;&#111;&#00045;n&#111;rth&#097;m&#101;&#114;i&#x63;&#97;&#x40;&#x6b;&#x64;e&#x2e;o&#114;&#x67;">in&#x66;o-nor&#116;&#x68;&#97;&#109;eri&#x63;a&#64;kd&#101;.&#111;&#x72;&#0103;</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="ma&#x69;&#x6c;to:info-o&#00099;eani&#97;&#x40;k&#100;e.&#x6f;rg">i&#110;fo-&#111;ce&#x61;nia&#064;&#00107;de&#x2e;org</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="&#109;ai&#x6c;&#116;&#00111;&#58;&#105;&#110;fo&#x2d;so&#117;&#116;ham&#101;&#114;i&#99;a&#x40;k&#100;e&#46;&#x6f;r&#103;">i&#x6e;f&#111;&#0045;&#00115;&#x6f;uth&#0097;m&#101;&#114;ic&#97;&#00064;&#0107;d&#101;.org</a><br />
</td>


</tr></table>

<?php

  include("footer.inc");
?>
