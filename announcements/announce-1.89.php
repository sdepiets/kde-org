<?php
  $page_title = "KDE 1.89 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<center>
<h2>KRASH: DEVELOPERS PREVIEW OF KDE 2.0</h2>
<h2>Gearing Towards 2nd Generation Desktop Environment</h2>
</center>
<center><table width=480><tr><td>
<b>Internet - Dec 15,  1999</b>  -  
<p>
The  <a href="../">K  Desktop  Environment  (KDE)  Team</a>
is happy to announce KRASH, <br>
an unstable developers release of the upcoming KDE 2.0 Desktop <br>
Environment.
</p>

<p>
KRASH is intended to provide a stable API for KDE developers who are <br>
outside the main KDE project  to begin porting their applications to. <br>
While many of the core KDE 2.0  applications are incomplete or bugg<br>
at this point due to a focus on base library functionality, this snapshot <br>
does provide an API consistent with what will be released in the final <br>
version.  Where changes occur, they will be fully documented.
</p>

<p>
This is not a snapshot intended to be usable by end users. We strongly <br>
recommend users remain with the latest stable version, KDE 1.1.2,<br>
until KDE 2.0 will be released. Even people developing for the new <br>
KDE 2.0 might wish to remain with KDE 1.1.2 as their primary desktop <br>
system.
</p>

<p>
KDE 2.0 is scheduled to be released in the spring of 2000. This date is <br>
subject to change without further notice.
</p>

<h3>WHERE TO DOWNLOAD</h3>
 
<p>
KRASH can be downloaded from:
</p>

<p><a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/tar/generic">
ftp://ftp.kde.org/pub/kde/unstable/distribution/tar/generic</a></p>

<p>
Or one of its mirror sites.
</p>

<p> 
KRASH requires a recent snapshot of the Qt 2.1 toolkit. 
<a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/tar/generic/qt-2.1.0-snapshot-19991215.tar.gz">
The 15 Dec 1999<br>
 version of such a snapshot</a> is provided at the  above location for your <br>
convenience.  Please note that this version of Qt has not been released <br>
yet by Trolltech and that this version is not officially supported by either<br>
 KDE or Trolltech. KRASH does NOT work with Qt 1.x or Qt 2.0.
</p>
 
<h3>INCLUDED IN THIS RELEASE</h3>

<table>
<tr><td width=100>kdesupport</td><td>- General support libraries</td></tr>
<tr><td>kdelibs</td><td>- KDE libraries</td></tr>
<tr><td>kdebase</td><td>- KDE desktop environment</td></tr>
<tr><td>koffice</td><td>- KDE office suite</td></tr>
</table>


<h3>GOAL OF THE KRASH (1.89) RELEASE</h3>

<ul>
<li>Stabilize development. 
<li>Offer third party developers a convenient way to become familiar with<br>
Qt2.x / KDE 2.x
</ul>

<h3>REMARKS</h3>

<p>
KRASH is alpha quality software. It is not recommended to use<br>
KRASH for daily work. Although the API of KRASH should be relatively<br>
stable, CHANGES will be made to it before the final KDE 2.0 release.<br>
It is not recommended to publically release applications based on this<br>
snapshot.
</p>
<p>
Distributors should NOT SHIP this release without providing a stable<br>
and complete version of KDE as well.
</p>

<h3>PLANNED CHANGES</h3>

<p>
In the following areas major changes are foreseen between now and KDE 2.0:

<ul>
<li>Interface and launch method of the IO-Slaves (kio / kioslaves) 
<li>File selection dialog (kfile rewrite)
<li>Parts embedding (kparts redesign)
<li>Advanced features of HTML widget (khtml)
<li>Embedding within kicker (kapplet)
<li>Sound system
</ul>
</p>
<h3>
ABOUT KDE
</h3>
KDE is a collaborative Open Source project by hundreds  of  developers<br>
worldwide to create a sophisticated, customizable and  stable  desktop<br>
environment employing a network-transparent, intuitive user interface.<br>
KDE  includes   applications   for   email,   web   browsing,   system<br>
administration,  dial-up   networking,   scheduling,   Palm   Pilot(r)<br>
synchronization, and many other tasks.  KDE is working  proof  of  the<br>
power of the open source software development model.
<p>
The elegance and usefulness of the KDE environment has been recognized<br>
through multiple awards, including Linux  World  magazine's  "Editor's<br>
Choice" award in the Desktop  Environment  category,  and  Ziff-Davis'<br>
"Innovation of the year 1998/1999" award in the Software category.
<p>
For more information about KDE, please visit
<a href="../whatiskde/">http://www.kde.org/whatiskde/</a>.
</table></center>

<?php
  include "footer.inc"
?>
