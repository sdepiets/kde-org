<!-- header goes into the specific page -->
<p align="justify">
KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral, operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a
href="http://download.kde.org/stable/<?php echo $release_full;?>/">http://download.kde.org</a>, samuti
<a href="http://www.kde.org/download/cdrom.php">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="http://www.kde.org/download/distributions.php">
GNU/Linuxi ja UNIX-i süsteemist</a>.
</p>
<p align="justify">
  <a name="packages"><em>Paketid</em></a>.
  Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud <?php echo $release_full;?> 
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
  Mõned binaarpaketid on vabalt allalaaditavad KDE saidilt <a
href="http://download.kde.org/binarydownload.html?url=/stable/<?php echo $release_full;?>/">http://download.kde.org</a>.
  Lähinädalatel võib lisanduda teisigi binaarpakette, samuti praegu
saadaolevate pakettide uuendusi.
<a name="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/<?php echo $release_full;?>.php"><?php echo $release;?>infoleheküljel</a>.
</p>
<p align="justify">
  <a name="source_code"></a>
  Täieliku <?php echo $release_full;?> lähtekoodi võib vabalt alla laadida <a
href="http://download.kde.org/stable/<?php echo $release_full;?>/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti <?php echo $release_full;?> kompileerimiseks ja paigaldamiseks
  leiab samuti <a href="/info/<?php echo $release_full;?>.php#binary"><?php echo $release_full;?>infoleheküljelt</a>.
</p>

<h4>
Nõuded süsteemile
</h4>
<p align="justify">
Võimaldaks ära kasutada väljalasete täit võimsust, soovitame tungivalt pruukida Qt uusimat versiooni, milleks praegu on 4.7.2. See on hädavajalik stabiilsuse kindlustamiseks, sest mõnedki KDE tarkvara parandused toetuvad tegelikult aluseks olevale Qt raamistikule.<br />
Graafikadraiverid võivad teatud olukorras komposiitvõimaluste kasutamisel eelistada OpenGL-ile hoopis XRenderit. Kui täheldate tõsiseid graafikaprobleeme, võib usutavasti aidata töölauaefektide väljalülitamine, sõltuvalt küll konkreetsest graafikadraiverist ja selle seadistustest. KDE tarkvara kõigi võimaluste täielikuks ärakasutamiseks soovitame kasutada ka uusimaid süsteemile mõeldud graafikadraivereid, mis võivad tunduvalt parandada süsteemi kasutamist nii funktsioonide mõttes kui ka eriti üldise jõudluse mõttes. 
</p>

<h4>
Autorid
</h4>
<p>
Käesoleva väljalasketeate koostasid Vivek Prakash, Stefan Majewsky, Guillaume De Bure, Nikhil Marathe, Markus Slopianka, Stuart Jarvis, Jos Poortvliet, Nuno Pinheiro, Carl Symons, Marco Martin, Sebastian Kügler, Nick P ja veel paljud KDE propageerimise meeskonna liikmed. Eesti keelde tõlkis väljalasketeate Marek Laane.
</p>
<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Kontaktid ajakirjandusele</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

