<?php

  $release = '4.6';
  $release_full = '4.6.0';

  $page_title = "KDE's Dolphin Brings Faceted Browsing";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<h2>KDE Releases Applications 4.6.0</h2>
<p>
January, 26th 2011. KDE is happy to announce the release of new versions of many of our applications. From games, education and even small utilities, these applications become more powerful, yet easy to use as they mature in this version. Below are just a few highlights from some of the new applications released today.
</p>
<h2>Dolphin Adds Faceted Browsing</h2>
<p>
The search interfaces of Kfind and Dolphin have been unified in a new, simplified search bar. Faceted browsing makes its public debut in the form of a Filter Panel that enables you to easily browse your indexed files using their metadata. A new sidebar allows for faceted searching, an enhancement to the traditional filebrowsing using metadata as additional filters.
</p>
<?php
centerThumbScreenshot("46-a05.png", "Dolphin's faceted browsing lets you use multiple filters to find files by metadata");
?>
<ul>
    <li>Column View usability improves. Columns width is now adjustable automatically or by the user, file selecting using a rubberband is now available and the horizontal scrollbar is no longer needed for navigating neighboring columns.</li>
    <li>To accompany the move of KDE development from SVN to Git, Dolphin features a new Git plugin, allowing updating and commiting from the GUI. The SVN plugin is of course still available.</li>
    <li>Various Improvents to the Service Menus (<a href="http://ppenz.blogspot.com/2010/11/improved-service-menus.html">http://ppenz.blogspot.com/2010/11/improved-service-menus.html</a>)</li>
</ul>

<h2>Kate Gains SQL Client</h2>
Kate, the powerful text-editor has gained a lot of polish and new features with this release. In particular, this new version offers the ability to:
<ul>
    <li>Recover unsaved data for local files on the next Kate startup (swap file support).</li>
    <li>Always load plugins.</li>
    <li>Add scripts to the menu and bind shortcuts.</li>

    <li>Kate's <a href="http://kate-editor.org/2010/07/29/katesql-a-new-plugin-for-kate/">new SQL Query plugin</a> brings features of a basic SQL client to Kate and supports a wide variety of databases through Qt's SQL module.</li>
    <li>A new GNU Debugger (GDB) Plugin</li>
    <li>A new Highlight Selected Text Plugin.</li>
</ul>

<h2>Graphics Applications Go Social</h2>
<p>
Gwenview, the KDE image viewer, gets a “Share” button to advertise its abilities to export pictures to popular photo sharing and social networking web sites.
</p>
<?php
centerThumbScreenshot("46-a03.png", "Gwenview can share images to many popular social networking sites");
?>
<p>
<b>KSnapshot</b> can now lasso regions to snapshot, has an option to include the mouse pointer and also a “Send to” button to instantly share screenshots.</p>

<?php
centerThumbScreenshot("46-a04.png", "KSnapshot can directly export screenshots to a number of 3rd party services");
?>
The many other KDE applications updated today also receive new features and numerous bug fixes, while also gaining from the latest improvements in the KDE Platform to enhance speed and stability.


<h2>Marble Takes You Home, KStars Renders faster thanks to OpenGL</h2>
<p>
Marble, KDE's virtual globe, continues to shine with improved route planning support, now also allowing downloading of routing data. Its mobile version <b>MarbleToGo</b> is now a very capable turn-by-turn navigator. Pictures and videos are available on <a href="http://dot.kde.org/2010/11/10/kdes-marble-team-holds-first-contributor-sprint">dot.kde.org</a>, an illustrated feature guide can be found <a href="http://edu.kde.org/marble/current_1.0.php">here</a>.
</p>
<?php
centerThumbScreenshot("46-a01.png", "Marble’s advanced routing can use a variety of configurable backends");

centerThumbScreenshot("46-a06.png", "The mobile version of Marble is a capable personal navigation tool");
?>
<p>
KStars, KDE's desktop planetarium has gained optional support for rendering using openGL, enhancing its performance on hardware with openGL capabilities.</s></li>
</p>


<h2>KDE Games</h2>
<ul>
    <li><b>Klickety</b>, an adaptation of the Clickomania game, makes a comeback in KDE Games 4.6, also replacing KSame through a compatibility mode.</li>
    <li>The KGameRenderer framework unifies theming across games, giving a more consistent and smooth look and feel. About a dozen KDE games have been ported to this new architecture, reducing memory usage and allowing them to take advantage of multicore processors.</li>
    <li>Palapeli's new puzzle slicer splits images into distinctive and authentic-looking pieces, a bevel effect makes pieces appear three-dimensional. Palapeli's “Create New Puzzle” dialog has seen usability improvements.</li>
    <li><b>Kajongg</b>'s improved documentation explains the game to beginners. More intelligent robot player, and improved usability and playability due to better tile-handling make this traditional game more fun to play.</li>
</ul>

<?php
centerThumbScreenshot("46-a02.png", "Palapeli, the KDE puzzle game, makes it easy to create jigsaws from your own images");
?>


<h4>Installing KDE Applications</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Released Today:</h2>

<?php

include("trailer-plasma.inc");
include("trailer-platform.inc");

include("footer.inc");
?>
