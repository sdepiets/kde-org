
<h3>
<a href="plasma.php">
Plasma Workspaces Put You in Control
</a>
</h3>

<p>
<a href="plasma.php">
<img src="images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.6.0" width="64" height="64" />
</a>

The<b> KDE Plasma Workspaces</b> gain from a new Activities system, making it easier to associate applications with particular activities such as work or home tasks. Revised power management exposes new features but has a simpler configuration interface. KWin, the Plasma workspace window manager, receives a new scripting and the workspaces receive visual enhancements. <b>Plasma Netbook</b>, optimized for mobile computing devices receives speed enhancements and becomes easier to use via a touchscreen interface. For more details read the <a href="plasma.php">KDE Plasma Workspaces 4.6 announcement</a>.
</p>
