<?php

  $release = '4.6';
  $release_full = '4.6.0';

  $page_title = "KDE Dolphinile lisandus täpsustav otsing";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<h2>KDE laskis välja rakendused 4.6.0</h2>
<p>
26. jaanuar 2011. KDE-l on rõõm teatada paljude rakenduste uute versioonide väljalaskest. Olgu tegemist mängude, õpirakenduste või ka pisikeste tööriistadega, on nad uues versioonis senisest võimsamad, aga ometi veel lihtsamini kasutatavad. Allpool on ära toodud vaid mõned olulised näited täna välja lastud rakenduste uute omaduste kohta.
</p>
<h2>Dolphinile lisandus täpsustav otsing</h2>
<p>
Kfindi ja Dolphini otsinguliidesed ühtlustati uueks lihtsamaks otsinguribaks. Uus täpsustav otsing ehk nõndanimetatud fassettsirvimine avaldub filtripaneelina, mis võimaldab väga lihtsalt sirvida indekseeritud faile nende metaandmeid ära kasutades. See uus külgriba täiustab tunduvalt traditsioonilise failide sirvimise võimalusi.
</p>
<?php
centerThumbScreenshot("46-a05.png", "Dolphini fassettsirvimisel saab failide leidmiseks metaandmete abil kasutada mitmeid filtreid");
?>
<ul>
    <li>veeruvaate kasutamist hõlbustavad parandused. Veergude laius kohandub nüüd automaatselt, aga seda võib ka teha kasutaja; faile saab hõlpsasti valida ka vajalike failide ümber kasti tõmmates; naaberveergudesse liikumiseks ei kasutata enam rõhtsat kerimisriba.</li>
    <li>et KDE arendus on hakanud üle minema SVN-ist Gitti, pakub Dolphin nüüd uut Giti pluginat, mis lubab otse failihalduris sooritada uuendamist ja sissekandmist. Loomulikult on endiselt olemas ka SVN-i plugin.</li>
    <li>mitmesuguseid parandusi on saanud teenustemenüüd (<a href="http://ppenz.blogspot.com/2010/11/improved-service-menus.html">http://ppenz.blogspot.com/2010/11/improved-service-menus.html</a>)</li>
</ul>

<h2>Katel on nüüd SQL-i klient</h2>
Võimas tekstiredaktor Kate on saanud tublisti lihvi ja uusi omadusi, mille seast eriti tasub ära märkida järgmisi:
<ul>
    <li>kohalike failide salvestamata andmete taastamine Kate järgmisel käivitamisel (puhverfailide toetus).</li>
    <li>alati laaditakse pluginad.</li>
    <li>skriptide lisamine menüüsse.</li>

    <li>Kate <a href="http://kate-editor.org/2010/07/29/katesql-a-new-plugin-for-kate/">uus SQL-i päringute plugin</a> annab redaktorile lihtsa SQL-i kliendi omadused, sealjuures on Qt SQL-i mooduli vahendusel toetatud väga mitmesugused andmebaasid.</li>
    <li>uus GNU siluri (GDB) plugin.</li>
    <li>uus valitud teksti esiletõstmise plugin.</li>
</ul>

<h2>Graafikarakendused muutusid sotsiaalseks</h2>
<p>
KDE pildinäitaja Gwenview sai nupu "Jaga", mis võimaldab eksportida pilte levinud fotode jagamise saitidele või sotsiaalvõrgustikesse.
</p>
<?php
centerThumbScreenshot("46-a03.png", "Gwenview võib laadida pilte paljudesse levinud sotsiaalvõrgustikesse");
?>
<p>
<b>KSnapshot</b> võimaldab nüüd vabalt valida ekraani piirkonna, millest pilt teha, kaasata pilti ka hiirekursori ning pakub samuti võimalust saata tehtud pilt otsekohe mõnda sotsiaalvõrgustikku.</p>

<?php
centerThumbScreenshot("46-a04.png", "KSnapshot võib saata ekraanipildi otse veebi");
?>
Paljud teisedki täna uuendatud KDE rakendused on saanud uusi omadusi ja arvukalt veaparandusi, samal ajal võimaldab KDE platvormi uus versioon neil olla senisest kiirem ja stabiilsem.


<h2>Marble viib teid koju, KStars näitab tähti kiiremini tänu OpenGL-ile</h2>
<p>
KDE virtuaalne gloobus Marble jätkas teekondade kavandamise täiustamist, pakkudes nüüd ka võimalust teekonnaandmeid alla laadida. Mobiilse versiooni <b>MarbleToGo</b> võib juba tunnistada väga võimekaks teekonnajuhiseid pakkuvaks navigaatoriks. Pilte ja videoid leiab saidilt <a href="http://dot.kde.org/2010/11/10/kdes-marble-team-holds-first-contributor-sprint">dot.kde.org</a>, illustreeritud tutvustuse aga <a href="http://edu.kde.org/marble/current_1.0.php">siit</a>.
</p>
<?php
centerThumbScreenshot("46-a01.png", "Marble täiustatud teekonna kavandamine võib kasutada mitmeid seadistatavaid taustaprogramme");

centerThumbScreenshot("46-a06.png", "Marble mobiilne versioon on usaldusväärne personaalne navigaator");
?>
<p>
KDE töölauaplanetaarium KStars suudab nüüd renderdamiseks kasutada OpenGL-i, mis parandab tunduvalt jõudlust riistvara korral, mis toetab OpenGL-i võimalusi.</s></li>
</p>


<h2>KDE mängud</h2>
<ul>
    <li><b>Klickety</b>, Clickomania mängu mugandus, on tagasi, asendades või õigemini liidendades ühtlasi mängu KSame.</li>
    <li>KGameRendereri raamistik ühtlustab teemade kasutamist eri mängudes, tagades ühtlasema ja viimistletuma välimuse. Üle kümne KDE mängu tarvitab juba seda uut arhitektuuri, millega kaasneb mälukasutuse vähenemine ning võimalus ära kasutada mitme protsessori eeliseid.</li>
    <li>Palapeli uus puslelõigustaja lõigub pildid just sellisteks tükkideks, nagu kasutaja soovib, kusjuures servasüvenditega on võimalik anda pusletükkidele ruumiline välimus. Omajagu lihtsamalt kasutatavaks on muudetud Palapeli uue pusle loomise dialoog.</li>
    <li><b>Kajonggi</b> täiendatud käsiraamat selgitab mängu olemust ka algajatele. Nutikam robotmängija ning paranenud mängitavus tänu senisest etemale mängukivide käitlemisele muudavad selle traditsioonilise mängu mängimise tõeliseks lõbuks.</li>
</ul>

<?php
centerThumbScreenshot("46-a02.png", "KDE puslemänguga Palapeli saab hõlpsasti pusleks muuta ka enda pilte");
?>


<h4>KDE rakenduste paigaldamine</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Täna ilmusid veel:</h2>

<?php

include("trailer-plasma-et.inc");
include("trailer-platform-et.inc");

include("footer.inc");
?>
