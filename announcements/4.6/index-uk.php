<?php
  $release = '4.6';
  $release_full = '4.6.0';
  $page_title = "KDE надає вам нові можливості керування робочим простором, нові програми та нову платформу розробки";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<p>Also available in:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>

<p>
KDE із задоволенням повідомляє про свій найсвіжіший набір випусків, зокрема оновлену версію робочого простору Плазми KDE, програм KDE та платформи розробки KDE. Ці випуски, які позначено номером версії 4.6, надають у розпорядження користувачів багато нових можливостей у кожній з трьох лінійок продуктів KDE. Серед цих нових можливостей:
</p>

<?php
  centerThumbScreenshot("46-w09.png", "KDE Plasma Desktop, Gwenview and KRunner in 4.6");
?>

<?php
include("trailer-plasma-uk.inc");
include("trailer-applications-uk.inc");
include("trailer-platform-uk.inc");

?>


<h4>
    Розкажіть іншим і будьте свідком результатів: мітка «KDE»
</h4>
<p align="justify">
Спільнота KDE буде вдячна вам за <strong>поширення інформації</strong> у соціальних мережах. Надсилайте статті на сайти новин, використовуйте канали обміну повідомленнями, зокрема delicious, digg, reddit, twitter, identi.ca. Вивантажуйте знімки вікон на служби зберігання зображень, зокрема Facebook, FlickR, ipernity і Picasa, і надсилайте їх до відповідних груп. Створюйте відеосюжети і вивантажуйте їх на YouTube, Blip.tv, Vimeo та інші служби. Не забудьте позначити вивантажений матеріал <em>міткою <strong>kde</strong></em>, щоб зацікавленим особам було легше його знайти, а також щоб полегшити команді KDE створення звітів щодо поширення оголошення про випуск KDE SC <?php echo $release;?>
. <strong>Допоможіть нам поширити інформацію, станьте учасником цієї події!</strong></p>

<p align="justify">
Ви можете спостерігати за подіями навколо випуску <?php echo $release?> у режимі реального часу у соціальній мережі за допомогою
<a href="http://buzz.kde.org"><strong>подачі новин Спільноти KDE</strong></a>. На цьому сайті будуть збиратися всі записи з identi.ca, twitter, youtube, flickr, picasaweb, блогів та багатьох інших сайтів соціальних мереж у режимі реального часу. Подачу новин можна знайти на <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/search?s=kde46"><img src="buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/search?q=kde46"><img src="buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde46"><img src="buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde46"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde46"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde46"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde46"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">мікрокнопки</a></span>
</div>

<h4>Підтримка KDE</h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="Join the Game" align="left"/> </a>
<p align="justify">За допомогою нової <a
href="http://jointhegame.kde.org/">програми підтримки членством</a> ви за 25&euro; на квартал можете підтримати міжнародну спільноту KDE у продовженні розробки вільного програмного забезпечення найкращої якості.</p>
<br clear="all" />
<p>&nbsp;</p>
<?php
  include("footer.inc");
?>
