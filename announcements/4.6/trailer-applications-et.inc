
<h3>
<a href="applications-et.php">
KDE Dolphinile lisandus täpsustav otsing
</a>
</h3>

<p>
<a href="applications-et.php">
<img src="images/applications.png" class="app-icon" alt="KDE rakendused 4.6.0"/>
</a>
Paljude <b>KDE rakenduste</b> meeskonnad lasksid samuti välja uued versioonid. Eriti tasub tähele panna KDE virtuaalse gloobuse Marble tunduvalt parandatud teekondade väljaarvutamise ja näitamise võimalusi ning KDE failihalduri Dolphin täiustatud filtreerimist ja otsimist metaandmete abil, niinimetatud fassettsirvimist. KDE mängud said hulganisti parandusi ning pildinäitaja Gwenview ja ekraanipiltide tegemise rakendus KSnapshot võimaluse saata pilte aega viitmata mitmesse levinud sotsiaalvõrgustikku. Täpsemalt kõneleb kõigest <a href="applications-et.php">KDE rakenduste 4.6 teadaanne</a>.<br /><br />
</p>
