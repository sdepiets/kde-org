<?php

  $release = '4.6';
  $release_full = '4.6.0';
  $page_title = "Mobile Target Makes KDE Platform Lose Weight";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<h2>KDE Releases Development Platform 4.6.0</h2>
<p>
January, 26th 2011. KDE proudly announces the release of KDE Platform 4.6. The foundation for the Plasma workspaces and the KDE applications has seen significant improvements in many areas. Besides the introduction of new technology, a lot of work has been spent on improving the performance and stability of the underlying pillars of the Platform.
</p>
<h2>Low-Fat Mobile Profile Cuts Dependency Chains</h2>
<p>
By modularizing the KDE libraries further, parts of the KDE platform can now be built for mobile and embedded target systems. Reduced cross-library dependencies and allowing certain features to be disabled, allow KDE frameworks to now be easily deployed on mobile devices. The mobile profile is already used for mobile and tablet versions of KDE applications, such as <a href="http://userbase.kde.org/Kontact_Touch">Kontact Touch</a>, KDE's mobile office suite and the <a href="http://www.notmart.org/index.php/Software/Plasma:_now_comes_in_tablets">tablet</a> and handset Plasma user interfaces.
</p>

<h2>Harness the Power of Plasma with QML</h2>
<p>With the new release, the Plasma framework gains support for Plasma widgets written in <b>QML</b>, Qt Quick's declarative UI language. While existing widgets continue to function just as before, QML is now the preferred way to create new widgets. Plasma data engines receive new features, including the ability to share files using a Javascript plugin and a storage service allowing data engines to cache data for offline usage. The new Plasma KPart is now available, making it easy for developers to integrate these new and of course previously existing plasma technologies inside their applications – there is already work on using the Plasma framework in Kontact and Skrooge.
</p>

<h2>UPower, UDev and UDisks Support, Metadata Backup</h2>
<p>
Thanks to Solid's new <b>UPower, UDev and UDisks</b> backends, the deprecated HAL is no longer needed to manage hardware on Linux. Applications do not need to be updated to make use of these new backends. The HAL backend is still available for systems that do not support UPower.
</p>
<p>
<b>Nepomuk</b>, the semantic desktop technology of the KDE Platform, gained <a href="http://vhanda.in/blog/2010/11/nepomuk-backup/">backup and synchronisation</a> support to make transferring meta-data between devices easier. Users can now back up their semantic data using a graphical interface. Additional synchronization features are currently only available from the command line.
</p>

<?php
centerThumbScreenshot("46-p01.png", "Semantic data can now be easily backed up and restored using a graphical interface");
?>

<h2>KWin Becomes Scriptable, Oxygen-GTK Improves cross-desktop Integration</h2>
<p>
<b>Kwin</b>, KDE's window and compositing manager now has a <a href="http://rohanprabhu.com/?p=116">scripting interface</a>. This gives advanced users and distributors more power over the behavior of windows in a KDE Plasma workspace – for example it is possible to set a window as “keep above” until the window is maximized, treating it as a normal window that can be covered when maximized. When the window is un-maximized, automatically set it as “keep above” again. The KWin team is currently working on support for OpenGL-ES, expected to be ready for prime-time with the release of 4.7 in summer 2011, so that KWin will be usable on handheld systems.
</p>
<p>
<b>Oxygen</b>, the visual component of the KDE Platform has also seen extensive work with a complete remake of all mimetype icons, the introduction of the <a href="http://hugo-kde.blogspot.com/2010/11/oxygen-gtk.html">Oxygen-GTK theme engine</a>, enabling better integration of GTK apps into KDE Plasma Workspaces, including gradients and many of the features users expect from Oxygen.
</p>

<?php
centerThumbScreenshot("46-p02.png", "The Oxygen-GTK theme allows KDE and GTK applications to be mixed seamlessly");
?>

<h2>New Bluetooth Framework Allows Easy Device Pairing</h2>
<p>
<b>BlueDevil</b>, a brand new Bluetooth framework, allows easy device pairing and management. BlueDevil replaces KBluetooth and builds on top of the BlueZ stack. It allows the following:
</p>
<ul>
    <li>You can now use a wizard to pair devices,</li>
    <li>Browse files on bluetooth devices using Dolphin or Konqueror, and</li>
    <li>Manage bluetooth settings from KDE’s System Management or the system tray.</li>
    <li>The new framework also ‘listens’ for incoming requests, for example, to receive files or to enter a PIN code.</li>
</ul>

<h4>Installing the KDE Development Platform</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Released Today:</h2>
<?php

include("trailer-plasma.inc");
include("trailer-applications.inc");

include("footer.inc");
?>
