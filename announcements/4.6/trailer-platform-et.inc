
<h3>
<a href="platform-et.php">
Suund mobiilsusele kahandab KDE platvormi kaalu
</a>
</h3>

<p>
<a href="platform-et.php">
<img src="images/platform.png" class="app-icon" alt="KDE arendusplatvorm 4.6.0"/>
</a>
<b>KDE platvorm</b>, millele tuginevad Plasma töötsoonid ja KDE rakendused, sai uusi omadusi, millest võidavad kõik KDE rakendused. Uus suund mobiilsusele muudab hõlpsamaks mobiilsetele seadmetele mõeldud rakenduste arendamise. Plasma raamistik toetab nüüd töölauavidinate kirjutamist Qt deklaratiivses programmeerimiskeeles QML ning pakub uusi JavaScripti liideseid andmete kasutamiseks. KDE rakendustele metaandmete ja otsinguvõimalusi pakkuv Nepomuki tehnoloogia sai graafilise liidese, mille abil andmeid varundada ja taastada. Iganenud HAL-i asemel on nüüd võimalik kasutada UPowerit, UDevi ja UDisksi. Paranes Bluetoothi toetus. Oxygeni vidina- ja stiilikomplekt sai mitmeid täiustusi ning uus GTK rakendustele mõeldud Oxygeni teema võimaldab neid sujuvalt ühendada Plasma töötsoonidega, nii et nad näevad välja nagu KDE rakendused. Täpsemalt kõneleb kõigest <a href="platform-et.php">KDE platvormi 4.6 teadaanne</a>.
</p>
