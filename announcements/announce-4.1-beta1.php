<?php
  $page_title = "KDE 4.1 Beta1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Also available in:
<?php
  $release = '4.1-beta1';
  include "announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  KDE Project Ships First Beta of KDE 4.1
</h3>

<p align="justify">
  <strong>
KDE Community Announces First Beta Release of KDE 4.1</strong>
</p>

<p align="justify">
May 27, 2008 (The INTERNET).  
The <a href="http://www.kde.org/">KDE Project</a> is proud to announce the 
first beta release of KDE 4.1.  Beta 1 is aimed at testers, community 
 members and enthusiasts in order to identify bugs and regressions, so that 
4.1 can fully replace KDE 3 for end users.  KDE 4.1 beta 1 is available as 
binary packages for a wide range of platforms, and as source packages.  KDE 
4.1 is due for final release in July 2008.
</p>


<h4>
  <a name="changes">KDE 4.1 Beta 1 Highlights</a>
</h4>

<p align="justify">
<ul>
    <li>Greatly expanded desktop shell functionality and configurability
    </li>
    <li>KDE Personal Information Management suite ported to KDE 4
    </li>
    <li>Many new and newly ported applications
    </li>
</ul>
</p>

<h4>
  Plasma Grows Up
</h4>
<p align="justify">
Plasma, the innovative new system that creates the menus and panels
that make up a desktop, is maturing rapidly.  It now supports multiple and 
resizeable panels allowing users to compose their desktop as flexibly as 
before.  The application launcher menu, Kickoff, has been comprehensively 
polished with a clean new look and many optimisations.  An overhauled Run 
Command dialog allows power users to quickly launch applications, open 
documents and visit sites.  Performance gains in composited window 
management provide better ergonomics and eye candy, including a Cover Switch 
alt-tab feature and the mandatory wobbly windows effect.
</p>
<div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/plasma-krunner.png">
						<img width="310" height="235" src="announce_4.1-beta1/plasma-krunner-small.png" title="The new KRunner alt-F2 dialog" alt="The new KRunner alt-F2 dialog"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/plasma-panelcontroller.png">
						<img width="462" height="84" src="announce_4.1-beta1/plasma-panelcontroller-small.png" title="Panel management returns" alt="Panel management returns"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/kwin-coverswitch.png">
						<img width="462" height="347" src="announce_4.1-beta1/kwin-coverswitch-small.png" title="KWin's Cover Switch effect" alt="KWin's Cover Switch effect"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/kwin-wobbly1.png">
						<img width="462" height="347" src="announce_4.1-beta1/kwin-wobbly1-small.png" title="Windows that wobble" alt="Windows that wobble"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/kwin-wobbly2.png">
						<img width="462" height="347" src="announce_4.1-beta1/kwin-wobbly2-small.png" title="More wobbling windows" alt="More wobbling windows"/>
				</a>
		</div>
		<div style="clear:both;"></div>
</div>
<h4>
  Kontact Returns
</h4>
<p align="justify">
Kontact, the KDE personal information manager, and its associated tools have 
been ported to KDE 4 and will be released for the first time with KDE 4.1.
  Many features from the KDE 3 Enterprise Branch have been incorporated, 
making Kontact more useful in business settings.  Improvements include new 
components such as KTimeTracker and the KJots note-taking component, a smooth
 new look, better support for multiple calendars and timezones and more 
robust email handling.</p>
<center>
<a href="announce_4.1-beta1/kontact-calendar.png">
<img width="351" height="275" src="announce_4.1-beta1/kontact-calendar-small.png" title="Multiple calendars in use" alt="Multiple calendars in use"/>
</a>
<a href="announce_4.1-beta1/kontact-kjots.png">
<img width="351" height="271" src="announce_4.1-beta1/kontact-kjots-small.png" title="The KJots outliner" alt="The KJots outliner"/>
</a>
</center>

<h4>
KDE 4 Applications Grow
</h4>
<p align="justify">
Across the KDE community, many applications have now been ported to KDE 4 or 
have seen great increases in functionality since KDE 4 was launched.  Dragon 
Player, the lightweight media player, makes its debut.  The KDE CD Player 
returns.   A new printer applet  provides unparalleled printing power and 
flexibility on the Free Software Desktop.  Konqueror gains support for web 
browsing sessions, an Undo mode, and improved smooth scrolling.  A new 
picture browsing mode including a full-screen interface come to Gwenview.  
Dolphin, the file manager, gets tabbed views, and many features appreciated 
by KDE 3 users including Copy To, and an improved folder tree.  Many apps, 
including the desktop and the KDE Education applications, are now providing 
fresh content such as icons, themes, maps, and lesson material via Get New 
Stuff, which has an improved interface.  Zeroconf networking has been added 
to several games and utilities, taking the pain out of setting up games and 
remote access.
</p>
<div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/dolphin-treeview.png">
						<img width="265" height="234" src="announce_4.1-beta1/dolphin-treeview-small.png" title="Dolphin's tree view" alt="Dolphin's tree view"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/dragonplayer.png">
						<img width="442" height="317" src="announce_4.1-beta1/dragonplayer-small.png" title="Dragon media player" alt="Dragon media player"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/games-kdiamond.png">
						<img width="264" height="305" src="announce_4.1-beta1/games-kdiamond-small.png" title="KDiamond puzzle game" alt="KDiamond puzzle game"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/games-kubrick.png">
						<img width="215" height="200" src="announce_4.1-beta1/games-kubrick-small.png" title="The 80s on your desktop" alt="The 80s on your desktop"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/konqueror.png">
						<img width="302" height="294" src="announce_4.1-beta1/konqueror-small.png" title="Konqueror browser" alt="Konqueror browser"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/marble-openstreetmap.png">
						<img width="324" height="281" src="announce_4.1-beta1/marble-openstreetmap-small.png" title="Marble showing OpenStreetMaps" alt="Marble showing OpenStreetMaps"/>
				</a>
		</div>
		<div style="clear:both;"></div>
</div>
<h4>
Refinement Throughout The Frameworks
</h4>
<p align="justify">
Developers have been busy enriching the core KDE libraries and 
infrastructure too.  KHTML gets a speed boost from anticipatory resource 
loading, while WebKit, its offspring, is added to Plasma to allow OSX 
Dashboard widgets to be used in KDE.  The use of the Widgets on Canvas 
feature of Qt 4.4 makes Plasma more stable and lightweight.  KDE's 
characteristic single-click based interface gets a new selection mechanism 
that promises speed and accessibility.  Phonon, the crossplatform media 
framework, gains subtitle support and GStreamer, DirectShow 9 and QuickTime 
backends.  The network management layer is extended to support several 
versions of NetworkManager.  And recognising that the Free Desktop values 
diversity, cross desktop efforts begin, such as supporting the popup 
notification specifications and the freedesktop.org desktop bookmark 
specification, so that other desktops' applications can fit right into a 
KDE 4.1 session.
</p>

<h4>
  KDE 4.1 Final Release
</h4>
<p align="justify">
KDE 4.1 is scheduled for final release on July 29, 2008.  This time based release falls six months after the release of KDE 4.0.
</p>

<h4>
  Get it, run it, test it
</h4>
<p align="justify">
  Community volunteers and Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.0.80 (Beta 1) for most Linux distributions, and Mac OS X and Windows.  Be aware that these packages are not considered ready for production use.  Check your distribution's software management system or for distribution specific instructions see the following links:</p>

<ul>
<li><a href="http://fedoraproject.org/wiki/Releases/Rawhide"></a>Fedora</li>
<li><em>Debian</em> has KDE 4.1beta1 in <em>experimental</em>.</li>
<li><em>Kubuntu</em> packages are in preparation.</li>
<li><a href="http://wiki.mandriva.com/en/2008.1_Notes#Testing_KDE_4">Mandriva</a></li>
<li><a href="http://en.opensuse.org/KDE4#KDE_4_UNSTABLE_Repository_--_Bleeding_Edge">openSUSE</a></li>
<li><a href="http://techbase.kde.org/Projects/KDE_on_Windows/Installation">Windows</a></li>
<li><a href="http://mac.kde.org/">Mac OS X</a></li>
</ul>

<h4>
  Compiling KDE 4.1 Beta 1 (4.0.80)
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 4.0.80 may be <a
  href="http://www.kde.org/info/4.0.80.php">freely downloaded</a>.
Instructions on compiling and installing KDE 4.0.80
  are available from the <a href="/info/4.0.80.php">KDE 4.0.80 Info
  Page</a>, or on <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
project that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>About KDE 4</h4>
<p align="justify">
KDE 4 is the innovative Free Software desktop containing lots of applications
for every day use as well as for specific purposes. Plasma is a new desktop
shell developed for
KDE 4, providing an intuitive interface to interact with the desktop and
applications. The Konqueror web browser integrates the web with the desktop. The
Dolphin file manager, the Okular document reader and the System Settings control
center complete the basic desktop set. 
<br />
KDE is built on the KDE Libraries which provide easy access to resources on the
network by means of KIO and advanced visual capabilities through Qt4. Phonon and
Solid, which are also part of the KDE Libraries add a multimedia framework and
better hardware integration to all KDE applications.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
