<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.19.0");
  $site_root = "../";
  $release = '5.19.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
February 13, 2016. KDE today announces the release
of KDE Frameworks 5.19.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Simplify attica plugin look-up and initialization");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Many new icons");?></li>
<li><?php i18n("Add missing mimetype icons from oxygen icon set");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("ECMAddAppIcon: Use absolute path when operating on icons");?></li>
<li><?php i18n("Make sure the prefix is looked-up on Android");?></li>
<li><?php i18n("Add a FindPoppler module");?></li>
<li><?php i18n("Use PATH_SUFFIXES in ecm_find_package_handle_library_components()");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Don't call exec() from QML (bug 357435)");?></li>
<li><?php i18n("KActivitiesStats library is now in a separate repository");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Also perform preAuthAction for Backends with AuthorizeFromHelperCapability");?></li>
<li><?php i18n("Fix DBus service name of polkit agent");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Fix High-DPI issue in KCMUtils");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("KLineEdit::setUrlDropsEnabled method can not be marked as deprecated");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("add a \"Complementary\" color scheme to kcolorscheme");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("Update docs for KCrash::initialize. Application developers are encourage to call it explicitly.");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Clean up dependencies for KDeclarative/QuickAddons");?></li>
<li><?php i18n("[KWindowSystemProxy] Add setter for showingDesktop");?></li>
<li><?php i18n("DropArea: Fix correctly ignoring dragEnter event with preventStealing");?></li>
<li><?php i18n("DragArea: Implement grabbing delegate item");?></li>
<li><?php i18n("DragDropEvent: Add ignore() function");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Revert BlockingQueuedConnection hack, Qt 5.6 will contain a better fix");?></li>
<li><?php i18n("Make kded register under aliases specified by the kded modules");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("kdelibs4support requires kded (for kdedmodule.desktop)");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Allow querying for a file's origin URL");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Prevent crash in case dbus is not available");?></li>
</ul>

<h3><?php i18n("KDE GUI Addons");?></h3>

<ul>
<li><?php i18n("Fix listing of available palettes in color dialog");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Fix detection of icon link type (aka \"favicon\")");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Reduce use of gettext API");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("Add kra and ora imageio plugins (read-only)");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Ignore viewport on current desktop on init startup information");?></li>
<li><?php i18n("Port klauncher to xcb");?></li>
<li><?php i18n("Use an xcb for interaction with KStartupInfo");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("New class FavIconRequestJob in new lib KIOGui, for favicons retrieval");?></li>
<li><?php i18n("Fix KDirListerCache crash with two listers for an empty dir in the cache (bug 278431)");?></li>
<li><?php i18n("Make Windows implementation of KIO::stat for file:/ protocol error out if the file doesn't exist");?></li>
<li><?php i18n("Don't assume that files in read-only dir can't be deleted on Windows");?></li>
<li><?php i18n("Fix .pri file for KIOWidgets: it depends on KIOCore, not on itself");?></li>
<li><?php i18n("Repair kcookiejar autoload, the values got swapped in 6db255388532a4");?></li>
<li><?php i18n("Make kcookiejar accessible under the dbus service name org.kde.kcookiejar5");?></li>
<li><?php i18n("kssld: install DBus service file for org.kde.kssld5");?></li>
<li><?php i18n("Provide a DBus service file for org.kde.kpasswdserver");?></li>
<li><?php i18n("[kio_ftp] fix display of file/directory modification time/date (bug 354597)");?></li>
<li><?php i18n("[kio_help] fix garbage sent when serving static files");?></li>
<li><?php i18n("[kio_http] Try NTLMv2 authentication if the server denies NTLMv1");?></li>
<li><?php i18n("[kio_http] fix porting bugs which broke caching");?></li>
<li><?php i18n("[kio_http] Fix NTLMv2 stage 3 response creation");?></li>
<li><?php i18n("[kio_http] fix waiting until the cache cleaner listens to the socket");?></li>
<li><?php i18n("kio_http_cache_cleaner: don't exit on startup if cache dir doesn't exist yet");?></li>
<li><?php i18n("Change DBus name of kio_http_cache_cleaner so it doesn't exit if the kde4 one is running");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("KRecursiveFilterProxyModel::match: Fix crash");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("Fix crash in KJob dialogs (bug 346215)");?></li>
</ul>

<h3><?php i18n("Package Framework");?></h3>

<ul>
<li><?php i18n("Avoid finding the same package multiple times from different paths");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("PartManager: stop tracking a widget even if it is no longer top level (bug 355711)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Better behaviour for \"insert braces around\" autobrace feature");?></li>
<li><?php i18n("Change option key to enforce new default, Newline at End of File = true");?></li>
<li><?php i18n("Remove some suspicious setUpdatesEnabled calls (bug 353088)");?></li>
<li><?php i18n("Delay emitting of verticalScrollPositionChanged until all stuff is consistent for folding (bug 342512)");?></li>
<li><?php i18n("Patch updating tag substitution (bug 330634)");?></li>
<li><?php i18n("Only update the palette once for the change event belonging to qApp (bug 358526)");?></li>
<li><?php i18n("Append newlines at EOF by default");?></li>
<li><?php i18n("Add NSIS syntax highlighting file");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Duplicate the file descriptor while opening the file to read the env");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Fix buddy widgets working with KFontRequester");?></li>
<li><?php i18n("KNewPasswordDialog: use KMessageWidget");?></li>
<li><?php i18n("Prevent crash-on-exit in KSelectAction::~KSelectAction");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Change licence header from \"Library GPL 2 or later\" to \"Lesser GPL 2.1 or later\"");?></li>
<li><?php i18n("Fix crash if KWindowSystem::mapViewport is called without a QCoreApplication");?></li>
<li><?php i18n("Cache QX11Info::appRootWindow in eventFilter (bug 356479)");?></li>
<li><?php i18n("Get rid of QApplication dependency (bug 354811)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Add option to disable KGlobalAccel at compilation time");?></li>
<li><?php i18n("Repair path to app shortcut scheme");?></li>
<li><?php i18n("Fix listing of shortcut files (wrong QDir usage)");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Re-check connection state and other properties to be sure they are actual (version 2) (bug 352326)");?></li>
</ul>

<h3><?php i18n("Oxygen Icons");?></h3>

<ul>
<li><?php i18n("Remove broken linked files");?></li>
<li><?php i18n("Add app icons from the kde applications");?></li>
<li><?php i18n("Add breeze places icons into oxygen");?></li>
<li><?php i18n("Sync oxygen mimetype icons with breeze mimetype icons");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Add a property separatorVisible");?></li>
<li><?php i18n("More explicit removal from m_appletInterfaces (bug 358551)");?></li>
<li><?php i18n("Use complementaryColorScheme from KColorScheme");?></li>
<li><?php i18n("AppletQuickItem: Don't try to set initial size bigger than parent size (bug 358200)");?></li>
<li><?php i18n("IconItem: Add usesPlasmaTheme property");?></li>
<li><?php i18n("Don't load toolbox on types not desktop or panel");?></li>
<li><?php i18n("IconItem: Try to load QIcon::fromTheme icons as svg (bug 353358)");?></li>
<li><?php i18n("Ignore check if just one part of size is zero in compactRepresentationCheck (bug 358039)");?></li>
<li><?php i18n("[Units] Return at least 1ms for durations (bug 357532)");?></li>
<li><?php i18n("Add clearActions() to remove every applet interface action");?></li>
<li><?php i18n("[plasmaquick/dialog] Don't use KWindowEffects for Notification window type");?></li>
<li><?php i18n("Deprecate Applet::loadPlasmoid()");?></li>
<li><?php i18n("[PlasmaCore DataModel] Don't reset model when a source is removed");?></li>
<li><?php i18n("Fix margin hints in opague panel background SVG");?></li>
<li><?php i18n("IconItem: Add animated property");?></li>
<li><?php i18n("[Unity] Scale Desktop icon size");?></li>
<li><?php i18n("the button is compose-over-borders");?></li>
<li><?php i18n("paintedWidth/paintedheight for IconItem");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.19");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.3");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
