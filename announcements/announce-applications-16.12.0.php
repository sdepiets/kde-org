<?php
include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships KDE Applications 16.12.0");
  $site_root = "../";
  $version = "16.12.0";
  $release = "applications-".$version;
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n_var("December 15, 2016. Today, KDE introduces KDE Applications 16.12, with an impressive array of upgrades when it comes to better ease of access, the introduction of highly useful functionalities and getting rid of some minor issues, bringing KDE Applications one step closer to offering you the perfect setup for your device.");?>
</p>

<p align="justify">
<?php print i18n_var("<a href='%1'>Okular</a>, <a href='%2'>Konqueror</a>, <a href='%3'>KGpg</a>, <a href='%4'>KTouch</a>, <a href='%5'>Kalzium</a> and more (<a href='%6'>Release Notes</a>) have now been ported to KDE Frameworks 5. We look forward to your feedback and insight into the newest features introduced with this release.", "https://okular.kde.org/", "https://konqueror.org/", "https://www.kde.org/applications/utilities/kgpg/", "https://www.kde.org/applications/education/ktouch/", "https://www.kde.org/applications/education/kalzium/", "https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based");?>
</p>

<p align="justify">
<?php print i18n_var("In the continued effort to make applications easier to build standalone, we have split the kde-baseapps, kdepim and kdewebdev tarballs. You can find the newly created tarballs at <a href='%1'>the Release Notes document</a>", "https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split");?>
</p>


<p align="justify">
<?php print i18n_var("We have discontinued the following packages: kdgantt2, gpgmepp and kuser. This will help us focus on the rest of the code.");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("Kwave sound editor joins KDE Applications!");?></h3>

<figure style="float: right; margin: 0px"><a href="https://www.kde.org/images/screenshots/kwave.png"><img src="https://www.kde.org/images/screenshots/resized/kwave.png" width="540" height="355" /></a></figure>

<p align="justify">
<?php print i18n_var("<a href='%1'>Kwave</a> is a sound editor, it can record, play back, import and edit many sorts of audio files including multi channel files.
Kwave includes some plugins to transform audio files in several ways and presents a graphical view with a complete zoom and scroll capability.", "http://kwave.sourceforge.net/");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("The World as your wallpaper");?></h3>

<figure style="float: right; margin: 0px"><a href="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png"><img src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png?w=400&amp;h=225" width="400" height="225" /></a></figure>

<p align="justify">
<?php print i18n_var("Marble now includes both a Wallpaper and a Widget for Plasma that show the time on top of a satellite view of the earth, with real-time day/night display. These used to be available for Plasma 4; they have now been updated to work on Plasma 5."); ?>
</p>
<p align="justify">
<?php print i18n_var("You can find more information on <a href='%1'>Friedrich W. H. Kossebau's blog</a>.", "https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("Emoticons galore!");?></h3>

<figure style="float: right; margin: 0px"><a href="kcharselect1612.png"><img src="kcharselect1612.png" width="342" height="225" /></a></figure>

<p align="justify">
<?php print i18n_var("KCharSelect has gained the ability to show the Unicode Emoticons block (and other SMP symbol blocks)."); ?>
</p>
<p align="justify">
<?php print i18n_var("It also gained a Bookmarks menu so you can favorite all your loved characters.");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("Math is better with Julia");?></h3>

<figure style="float: right; margin: 0px"><a href="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png"><img src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="259" height="225" /></a></figure>

<p align="justify">
<?php print i18n_var("Cantor has a new backend for Julia, giving its users the ability to use the latest progress in scientific computing."); ?>
</p>
<p align="justify">
<?php print i18n_var("You can find more information on <a href='%1'>Ivan Lakhtanov's blog</a>.", "https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("Advanced archiving");?></h3>

<figure style="float: right; margin: 0px"><a href="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png"><img src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="235" height="225" /></a></figure>

<p align="justify">
<?php print i18n_var("Ark has several new features:"); ?>
</p>
<ul>
<li><?php print i18n_var("Files and folders can now be renamed, copied or moved within the archive");?></li>
<li><?php print i18n_var("It's now possible to select compression and encryption algorithms when creating archives");?></li>
<li><?php print i18n_var("Ark can now open AR files (e.g. Linux *.a static libraries)");?></li>
</ul>

<p align="justify">
<?php print i18n_var("You can find more information on <a href='%1'>Ragnar Thomsen's blog</a>.", "https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/");?>
</p>



<h3 style="clear:both;" ><?php print i18n_var("And more!");?></h3>

<p align="justify">
<?php print i18n_var("Kopete got support for X-OAUTH2 SASL authentication in jabber protocol and fixed some problems with the OTR encryption plugin.");?>
</p>

<p align="justify">
<?php print i18n_var("Kdenlive has a new Rotoscoping effect, support for downloadable content and an updated Motion Tracker. It also provides <a href='%1'>Snap and AppImage</a> files for easier installation.", "https://kdenlive.org/download/");?>
</p>

<p align="justify">
<?php print i18n_var("KMail and Akregator can use Google Safe Browsing to check if a link being clicked is malicious. Both have also added back printing support (needs Qt 5.8).");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("Aggressive Pest Control");?></h3>
<p align="justify">
<?php print i18n_var("More than 130 bugs have been resolved in applications including Dolphin, Akonadi, KAddressBook, KNotes, Akregator, Cantor, Ark, Kdenlive and more!");?>
</p>

<h3 style="clear:both;"><?php print i18n_var("Full Changelog");?></h3>

<p align="justify">
<?php print i18n_var("You can find the full list of changes <a href='%1'>here</a>.", "fulllog_applications.php?version=".$version);?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the KDE Applications 16.12 release. Report bugs. Encourage others to join the KDE Community. Or <a href='%1'>support the nonprofit organization behind the KDE community</a>.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5"); ?>
</p>

<p align="justify">
<?php i18n("Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, Digg, Reddit, and Twitter. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with 'KDE'. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 16.12 release."); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 16.12 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 16.12 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 16.12");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 16.12 may be <a href='http://download.kde.org/stable/applications/16.12.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-16.12.0.php'>KDE Applications 16.12.0 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
