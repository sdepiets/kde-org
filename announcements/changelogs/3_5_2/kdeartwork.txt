2006-01-21 17:42 +0000 [r500969]  thiago

	* branches/KDE/3.5/kdeartwork/kscreensaver/kdesavers/Euphoria.cpp:
	  Right, it should be a floating point type anyways. BUG:120415

2006-01-22 14:06 +0000 [r501238-501235]  mueller

	* branches/KDE/3.5/kdeartwork/kscreensaver/kdesavers/pendulum.cpp,
	  branches/KDE/3.5/kdeartwork/kscreensaver/kdesavers/rotation.cpp,
	  branches/KDE/3.5/kdeartwork/kscreensaver/kdesavers/slideshow.cpp:
	  fix compilation with QT_CLEAN_NAMESPACE/QT_NO_CAST_ASCII etc

	* branches/KDE/3.5/kdeartwork/kwin-styles/smooth-blend/client/config/smoothblendconfig.cc:
	  fix compilation

	* branches/KDE/3.5/kdeartwork/styles/dotnet/dotnet.cpp,
	  branches/KDE/3.5/kdeartwork/styles/phase/phasestyle.cpp: varous
	  smaller fixes

	* branches/KDE/3.5/kdeartwork/configure.in.in: must define
	  QT_CLEAN_NAMESPACE, otherwise debug() from Qt and namespace debug
	  from STL (gcc >= 4.2) clash. *sigh* Praise STL usage inside KDE
	  :-((

2006-01-24 09:54 +0000 [r501911]  osterfeld

	* branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/kxscontrol.cpp:
	  compilation fix: there is no i18n(QString), use
	  i18n(someStr.utf8())

2006-01-28 15:32 +0000 [r503262]  scripty

	* branches/KDE/3.5/kdeartwork/wallpapers/mystical_rightturn.jpg.desktop:
	  .desktop files in KDE are supposed to be in UTF-8 (Backport of
	  revision 503259) (goutte)

2006-02-03 19:21 +0000 [r505383]  jriddell

	* branches/KDE/3.5/kdeartwork/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdeartwork
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdeartwork

2006-02-17 13:48 +0000 [r510512]  coolo

	* branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/juggler3d.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/interaggregate.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/tangram.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/boxfit.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/carousel.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/boing.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/antmaze.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/fliptext.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/cube21.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/fiberlamp.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/glhanoi.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/celtic.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/crackberg.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/timetunnel.desktop
	  (added): xscreensaver 4.23 update

2006-02-17 14:04 +0000 [r510521]  coolo

	* branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/interaggregate.desktop,
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/boxfit.desktop,
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/fiberlamp.desktop,
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/celtic.desktop:
	  categories for new screensavers

2006-03-11 01:35 +0000 [r517446]  nickell

	* branches/KDE/3.5/kdeartwork/kwin-styles/smooth-blend/client/smoothblend.h,
	  branches/KDE/3.5/kdeartwork/kwin-styles/smooth-blend/client/config/smoothblendconfig.h:
	  Change the headers to be more explicit about the license being
	  GPL v2 as mentioned in the COPYING file.

2006-03-17 21:34 +0000 [r519778]  coolo

	* branches/KDE/3.5/kdeartwork/kdeartwork.lsm: tagging 3.5.2

