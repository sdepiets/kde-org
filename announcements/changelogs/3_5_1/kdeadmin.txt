2005-12-02 14:00 +0000 [r484993]  gyurco

	* branches/KDE/3.5/kdeadmin/kuser/propdlg.cpp,
	  branches/KDE/3.5/kdeadmin/kuser/kuserfiles.cpp: Fix a crash in
	  the user dialog where shadow passwords are disabled. Also switch
	  to shadowless mode when shadow file not found. However it's just
	  for confort, it's desired to clear the shadow file name in the
	  config dialog when shadow file not used.

2005-12-16 18:53 +0000 [r489003]  jriddell

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network-conf.in:
	  Add Kubuntu Dapper support

2005-12-19 04:53 +0000 [r489557-489556]  jbaptiste

	* branches/KDE/3.5/kdeadmin/knetworkconf/pixmaps/specifix.png
	  (removed),
	  branches/KDE/3.5/kdeadmin/knetworkconf/pixmaps/rpath.png (added),
	  branches/KDE/3.5/kdeadmin/knetworkconf/pixmaps/Makefile.am:
	  Renamed specifix to rpath.

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfigparser.cpp:
	  Renamed specifix to rpath.

2005-12-19 05:20 +0000 [r489559]  jbaptiste

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service-list.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/parse.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/replace.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/NEWS,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/file.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network-conf.in:
	  - Upgraded to system-tools-backends from 12-19-2995. - Added
	  support for Mandriva 2006.1 (cooker), Fedora 4 and Yoper 2.2. -
	  Remade Ark linux support as last patch broked the backend in some
	  distros.

2005-12-19 05:28 +0000 [r489560]  jbaptiste

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/README.NEW_PLATFORMS
	  (added): New README to remember who patches the backend to send
	  the patch so I can merge those changes with system-tools-backends
	  project at fd.org

2005-12-19 14:42 +0000 [r489697]  jriddell

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/kadddevicedlg.ui,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkinterface.cpp,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/kadddevicecontainer.cpp,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfigparser.cpp,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconf.cpp,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkinterface.h,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfdlg.ui,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfigparser.h,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconf.h:
	  Various fixes, some strings added: let the user edit the gateway
	  for an interface wait for save process to finish before exiting,
	  means it actually saves and doesn't leave a zombie perl process
	  hanging around make it fit on a 1024 screen in systemsettings
	  show informational dialogue while saving
	  http://bugzilla.ubuntu.com/show_bug.cgi?id=9871
	  http://bugzilla.ubuntu.com/show_bug.cgi?id=16921
	  CCMAIL:juan.baptiste@kdemail.net

2005-12-19 15:23 +0000 [r489714]  mhunter

	* branches/KDE/3.5/kdeadmin/kuser/kuser.kcfg: Typographical
	  corrections and changes CCMAIL:kde-i18n-doc@kde.org

2005-12-22 21:16 +0000 [r490709]  jbaptiste

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/kadddevicedlgextension.ui,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/kadddevicedlg.ui,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/kadddevicecontainer.cpp,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconf.cpp:
	  - Moved the Gateway field of the interface to the advanced
	  configuration dialog. - If the interface gateway field is blank,
	  it's value it's set to the value of the default gateway of the
	  routes tab. This is need for Debian-like distros.

2005-12-22 22:15 +0000 [r490724]  jbaptiste

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconf.cpp:
	  If broadcast is empty, then calculate it.

2005-12-26 22:52 +0000 [r491643]  jbaptiste

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconf.cpp:
	  When loading network info, If default gateway is blank and the
	  gateway of the interface that is the default gateway isn't, then
	  set the default gateway to that value.

2005-12-27 20:34 +0000 [r491859]  jbaptiste

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/kadddevicecontainer.cpp,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconf.cpp:
	  - Forgot to validate the gateway address in the advanced options.
	  - Made optionals the gateway and broadcast addresses, if the
	  broadcast address is left blank we calculate it.

2005-12-27 23:43 +0000 [r491911]  mhunter

	* branches/KDE/3.5/kdeadmin/lilo-config/qt/standalone.cpp: What's
	  This? consistency fixes

2005-12-29 19:54 +0000 [r492393]  jbaptiste

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfigparser.cpp,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconf.cpp:
	  BUG: 113799 Changed some error messages to be more descriptive. I
	  Hope it's enough.

2005-12-29 22:20 +0000 [r492428-492426]  jbaptiste

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfigparser.cpp:
	  Don't close kcontrol if the network backend isn't found.

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfigparser.cpp:
	  Don't close kcontrol if the network backend isn't found.

2006-01-01 14:34 +0000 [r493100]  toivo

	* branches/KDE/3.5/kdeadmin/kpackage/main.cpp: Add some directories
	  to the path, needed for Slackware

2006-01-02 14:18 +0000 [r493445]  scripty

	* branches/KDE/3.5/kdeadmin/knetworkconf/pixmaps/kubuntu.png,
	  branches/KDE/3.5/kdeadmin/kpackage/icon/hi64-app-kpackage.png,
	  branches/KDE/3.5/kdeadmin/kpackage/icon/hi128-app-kpackage.png,
	  branches/KDE/3.5/kdeadmin/kuser/icon/hi64-app-kuser.png,
	  branches/KDE/3.5/kdeadmin/kuser/icon/hi128-app-kuser.png: Remove
	  svn:executable from some typical non-executable files (goutte)

2006-01-10 00:43 +0000 [r496197]  adridg

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfigparser.cpp:
	  Remove some comments left over from a previous incarnation. On
	  FreeBSD, don't do the Linux thing of trying to parse a /proc
	  file, just ignore it entirely. This suggests that there needs to
	  be a backends/ script to handle the routing info, but that's more
	  work than just disabling a superfluous error message right now.
	  CCMAIL: kde@freebsd.org

2006-01-10 14:51 +0000 [r496472]  mueller

	* branches/KDE/3.5/kdeadmin/ksysv/configure.in.in: compile ksysv
	  for GNU/kFreeBSD

2006-01-14 16:41 +0000 [r498051]  jamesots

	* branches/KDE/3.5/kdeadmin/kcron/ktview.cpp: Patch from Charles
	  Thorpe to fix bug 106853: crash after selecting variable. BUG:
	  106853

2006-01-17 06:28 +0000 [r499150]  jbaptiste

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service-list.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/NEWS,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service.pl.in:
	  Updated to system-tools-backends cvs from 17012006 that includes
	  all of our latest patches (kubuntu, ark, fedora4, mandriva,
	  yoper, etc).

