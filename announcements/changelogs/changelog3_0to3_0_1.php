<?php
  $page_title = "KDE 3.0 to 3.0.1 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.0 and 3.0.1 releases.
</p>

<!--
<h3>General</h3>
<ul>
<li>The faster malloc replacement for GNU/Linux/glibc systems has been made default.</li>
</ul>
-->

<h3>arts</h3>
<ul>
<li>Build fixes.</li>
</ul>

<h3>kdelibs</h3>
<ul>
<li>dcop: dcopfind can launch the app if not already there.</li>
<li>kabc: Fix import of kde2 addressbooks, in case the libkab addressbook doesn't exist.</li>
<li>kabc: Use KSaveFile for saving vcards, to avoid trashed files when the disk becomes full.</li>
<li>kate: fixed several segfaults.</li>
<li>khtml: Allow to search for regexps independent of kregexpeditor installed.</li>
<li>khtml: Various crashes and rendering problems fixed.</li>
<li>khtml: Support for literal parsing of html tags in &lt;title&gt; tags</li>
<li>khtml: Java embedding race situation fixed.</li>
<li>kio: Improve cache-handling of pages and its embedded objects.</li>
<li>kfile: Better keyboard handling support.</li>
<li>kfile: Added direct access to the floppy in the Filedialog.</li>
<li>kfile: Fixed lan browsing / smb file selection</li>
<li>http ioslave: the Accept-Charset request header has been fixed.</li>
<li>http ioslave: the default User-Agent string has been simplified.</li>
<li>http ioslave: fix http authentication over proxied ssl connections.</li>
<li>http ioslave: only do persistent connections for GET requests.</li>
<li>http ioslave: Properly detect premature loss of connection.</li>
<li>http ioslave: Don't send referrer for anything but http/https/webdav/webdavs</li>
<li>kthemestyle: Use the proper font for menubar entries.</li>
<li>kthemestyle: Improve handling of patterned backgrounds.</li>
<li>kio: improved support for case-insensitive filesystems (FAT).</li>
<li>kdirwatch: various bugs in the FAM support have been fixed.</li>
<li>kab: has been deprecated and its documentation removed.</li>
<li>kssl: various ssl related fixes.</li>
<li>kjs: another fix for a rare crash in the garbage collector.</li>
<li>kjs: improved regexp support.</li>
<li>kjs: fixes in Date object and some global properties and functions</li>
<li>KConfig: make it react properly to language changes.</li>
<li>K*Socket: various bugs have been fixed.</li>
<li>KUniqueApplication: Use proper pid for not-so-unique processes.</li>
<li>KDatePicker: various fixes.</li>
<li>KPrinter: fix crash in wizard when testing a printer.</li>
<li>kdesu: getpeereid() support added.</li>
<li>kaction: fix accidental event duplication.</li>
<li>kaction: make action->setShortcut( KShortcut() ); work.</li>
<li>klistview: improved "full-width column" support.</li>
</ul>

<h3>kdeaddons</h3>
<!-- <ul>
</ul> -->

<h3>kdeadmin</h3>
<ul>
<li>lilo-config: fixed crash.</li>
<li>kcmlinuz: Fixed internationalisation.</li>
</ul>

<h3>kdeartwork</h3>
<ul>
<li>i18n fix for kcontrol module.</li>
</ul>

<h3>kdebase</h3>
<ul>
<li>KFind: Allow to search for 0 byte files, fixed general size search.</li>
<li>KFind: Don't show directories when searching for executable files.</li>
<li>Kicker: Don't lose system tray entries sometimes when Kicker restarts.</li>
<li>Kicker: Hide/show all detailed menu entries when configuration changes.</li>
<li>Kicker: Grave memory leak in Quick Browser fixed.</li>
<li>Konqueror: Don't crash in some cases when a kpart exits.</li>
<li>Konsole: Fixed support for "screen".</li>
<li>Konsole: Drag and Drop: shellQuote single remote file.</li>
<li>Konsole: Scrolllock only reacts to Key_ScrollLock, not Ctrl-S.</li>
<li>Konsole: Shift-MMB now pastes for mouse-aware applications.</li>
<li>Konsole: Doesn't show terminal size hint at startup.</li>
<li>Konsole: Selection fixes, xterm-like behavior.</li>
<li>Konsole: Session editor makes "Apply" active.</li>
<li>kcontrol: Fixes for RTL layouts.</li>
<li>khelpcenter: RTL fixes.</li>
<li>audiocd ioslave: Improved *BSD support</li>
<li>man ioslave: Improved *BSD support</li>
<li>smtp ioslave: Fixing SMTP AUTH with TLS for various SMTP server configurations.</li>
<li>smtp ioslave: Fixing sending of SMTP AUTH PLAIN for servers that expect non-dialog form authentication.</li>
<li>klipper: Disabled synchronizing contents of the clipboard and the selection.</li>
<li>kbookmarkeditor: Fixes for running multiple instances.</li>

</ul>

<h3>kdebindings</h3>
<ul>
<li>Many compile and binding fixes.</li>
</ul>

<h3>kdegames</h3>
<!-- <ul>
</ul> -->

<h3>kdegraphics</h3>
<ul>
<li>KuickShow: Printing fixes.</li>
<li>KuickShow: Allow switching between different directory views.</li>
<li>KuickShow: Fixed wrong sorting of files in one rare case.</li>
</ul>

<h3>kdemultimedia</h3>
<!-- <ul>
</ul> -->

<a name="kdenetwork">&nbsp;</a>
<h3>kdenetwork</h3>
<ul>
<li>Fix save and restore of address book dialog.</li>
<li>kmail: Disable editing of header field combobox for IMAP folders in the search dialog.</li>
<li>kmail: Fix the problem where selecting another folder would reset the header field values when not one of the predefined ones.</li>
<li>kmail: Search case-insensitively for headers.</li>
<li>kmail: Remove newlines from pasted text in header fields.</li>
<li>kmail: Recode also the headers, when the charset of an already existing mail is changed manually.</li>
<li>kmail: Restore unencrypted/unsigned message text also for msgs withattachments which are opened in the composer for editting and fix <a href="http://bugs.kde.org/db/41/41102.html">#41102</a>.</li>
<li>kmail: Don't ask if the message should be encrypted if the user didn't specify.</li>
<li>kmail: Improved behaviour of sign/encrypt actions if identity is changed because this prevents users from trying to encrypt a message without having set a OpenPGP identity which will fail if the user set 'encrypt to self'.</li>
<li>kmail: Correctly handle errors during decryption, i.e. don't use GnuPG's output.</li>
<li>kmail: Fix for Bug <a href="http://bugs.kde.org/db/40/40075.html">#40075</a>.</li>
<li>kmail: Don't show the 'PGP message' color bar message if the message doesn't contain a valid OpenPGP block.</li>
<li>kmail: Don't switch the current mail back to the top when a new mail arrives in the current folder.</li>
<li>kmail: Bug <a href="http://bugs.kde.org/db/39/39796.html">#39796</a>: Copying address to clipboard fails to set selection.</li>
<li>kmail: Don't rfc2047-encode the @ in addr-specs.</li>
<li>kmail: Partial fix for "KMail puts rfc2231 encoded parameter values into DQUOTES".</li>
<li>kmail: Fix Bug <a href="http://bugs.kde.org/db/39/39328.html">#39328</a>: Moving IMAP mail between different IMAP-accounts.</li>
<li>kmail: Handle spaces at line breaks between encoded and unencoded words correctely in RFC2047 encoding.</li>
<li>kmail: Fix an address parsing bug.</li>
<li>kmail: Use User-Agent, not X-Mailer as header field.</li>
<li>kmail: Don't eat in some cases the whole folder, when moving messages between IMAP folders.</li>
<li>kmail: Bug <a href="http://bugs.kde.org/db/38/38809.html">#38809</a>: Attachments lost using filters for forwarding.</li>
<li>kmail: Fix "no stdin at execute filter".</li>
<li>kmail: Don't crash when KRegExpEditor isn't installed.</li>
<li>kmail: Make sure that the (regexp) "Edit" button is [dis|en]abled properly.</li>
<li>kmail: Raise the progressbar in the status bar only when it's visible.</li>
<li>kmail: Enhanced lock file handling (global KMail lock).</li>
<li>kmail: Save and restore size of the address book dialog.</li>
<li>kmail: Don't fail to save attachments &gt; 14MB completely.</li>
<li>kmail: Fix a bug when creating a new identity as copy of the default.</li>
<li>kmail: Harmonize range and default value of "wrap column". Fixes <a href="http://bugs.kde.org/db/39/39665.html">#39665</a>.</li>
</ul>

<h3>kdepim</h3>
<ul>
<li>kpilot: fixed vcal (KOrganizer) conduit crashes.</li>
<li>kpilot: subtle bugfixes in other conduits.</li>
<li>KOrganizer: Fixed time shift problem with timezones different than UTC.</li>
<li>KOrganizer: Fixed memory leaks of calendar loading and saving code.</li>
<li>KOrganizer: A lot of other bugfixes.</li>
</ul>

<h3>kdesdk</h3>
<ul>
<li>cvscheck: Fixed output for branched checkouts.</li>
<li>cvslastchange: Fix diff output for first commit to a branch.</li>
<li>kbabel: Many Bugfixes.</li>
</ul>

<h3>kdetoys</h3>
<ul>
<li>kweather: i18n fixes.</li>
</ul>

<h3>kdeutils</h3>
<ul>
<li>ark: Fixes for various crashes.</li>
<li>ark: Many bugfixes.</li>
<li>kcalc: Bugfixes.</li>
</ul>

<h3>KDevelop</h3>
<ul>
<li>removed flickering in output view, highlight error messages.</li>
<li>avoid that corrupted .kdevses files confuse KDevelop.</li>
<li>fixed crash in class viewer.</li>
<li>fixed view focus problem and appropriate crashes, does also concern to copy &amp; paste problems of the previous version</li>
<li>fixed translation of compiler messages in output view</li>
<li>fixed names of project types in the project wizard list</li>
<li>fixed missing newlines in Output toolview</li>
</ul>

<h3>KDoc</h3>
No changes.


<?php include "footer.inc" ?>
