<?php
  $page_title = "KDE 3.1.2 to 3.1.3 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.1.2 and 3.1.3 releases.
</p>
<p>
Please see the <a href="changelog3_1_1to3_1_2.php">3.1.1 to 3.1.2 changelog</a> for further information.
</p>

<h3>arts</h3>
<ul>
<li>GCC 3.4 compile fixes, compiler warning fixes</li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=59184">#59184</a>: Allow the application to query if the server is suspended.</li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=59183">#59183</a>: arts_suspend() returns 0 if arts doesn't have device open.</li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=60636">#60636</a>: add -V command line switch to set initial volume.</li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=53905">#53905</a>: Compilation fix</li>
<li>Build system update</li>
</ul>

<h3>kdelibs</h3>
<ul>
<li>build system updates, gcc 3.4+ compilation fixes (Dirk Mueller)</li>
<li>ensure that each installed header file can be compiled on its own (has all necessary includes/forward declarations)</li>
<li><a href="/info/security/advisory-20030729-1.txt">SECURITY: kio/khtml:</a> Improve referer handling, always strip out username and password.</li>
<li>Roaming user support (Helge Deller)</li>
<li>artskde: apply patch from #54318</li>
<li>kabc: add quotes around names from LDAP which contain non-alphanumeric characters</li>
<li>kabc: unterminated string handling fixes</li>
<li>kbugreport: only refer to http://bugs.kde.org if the bug-address is submit@bugs.kde.org</li>
<li>kcookiejar: don't deliver cookies when the url is "strange"</li>
<li>kcookiejar: unbreak cookies for https sites once again</li>
<li>kcookiejar: make kcookiejar --remove &lt;domain&gt; work</li>
<li>kdecore: never unload dante socks library, it crashes when doing so</li>
<li>kdecore: Adding KMacroExpander, class for shell-safe expansion of filenames in shellcode</li>
<li>kdeinit: Handle X errors instead of just crashing when they occur.</li>
<li>kdeprint: improvements to handling of cups network printers</li>
<li>kdeprint/smbview: use nmblookup -M -- to make it work with samba 3.0</li>
<li>kdeui: dragging from the "konqueror wheel" button no longer duplicates the window</li>
<li>kdeui: rewritten mouse cursor-autohide code (hopefully much more stable now)</li>
<li>kdeui/kfontdialog: Try very hard to match the font name that was passed with one that actually exists</li>
<li>kdeui/kmainwindow: fix for saving statusbar state when autosave is enabled</li>
<li>kdewidgets: Add some new widgets</li>
<li>keramik: Major fixes to toolbar gradient alignment, as well as some miscellaneous fixes.</li>
<li>khtml: Fix crash reported in #61396</li>
<li>khtml: avoid endless-relayout-loop (#54673)</li>
<li>khtml: several backports of fixes from 3.2 development branch</li>
<li>khtml: several fixes to bugs exposed by DOM2 testsuite</li>
<li>khtml: Fix bug #26196</li>
<li>khtml jpeg loader: use memmove in places where memory regions overlap (valgrind we love you)</li>
<li>kio: crash/memory leak fixes</li>
<li>kio: Fix bug #54196</li>
<li>kio: added ability to add host:port to the no-proxy list</li>
<li>kio: speedup handling local files</li>
<li>kio: improvements to the layout of the HTTP Authentication password dialog</li>
<li>kio: allow rtsp:// protocol redirecting to mms://</li>
<li>kio: KIOSK restriction fixes</li>
<li>kio: signal handling fixes (SIGPIPE etc)</li>
<li>kio_http: text/html mimetype in Accept: gets preference over other text/* types</li>
<li>kio_http: improvements in HTTP referer handling</li>
<li>kiconloader: memory leak fix</li>
<li>kjava: thread handling fixes</li>
<li>kjava: don't send text to the console window if its not visible (saves huge amount of memory on some pages)</li>
<li>kjava: catch exceptions thrown inside Applet.destroy</li>
<li>klocale: Support Macedonian plural forms</li>
</ul>

<h3>kdeaddons</h3>
<ul>
<li>kimgalleryplugin: Performance improvements.</li>
<li>dirfilterplugin: Much better performance on large directories with few file types.</li>
</ul>

<h3>kdeadmin</h3>
<ul>
<li>kuser: UID connected to changed() and Solaris compile fixes (Aaron J. Seigo)</li>
<li>kuser: Fix mem leak (Laurent Montel)</li>
<li>kpackage: fix FreeBSD package display so duplicate categories are not shown. (Andy Fawcett)</li>
<li>kpackage: Fix crash (Laurent Montel)</li>
<li>compiler warning fixes (Dirk Mueller)</li>
</ul>

<h3>kdeartwork</h3>
<ul>
<li>kwin-styles/cde: fixed bugs <a href="http://bugs.kde.org/show_bug.cgi?id=40125">#40125</a>, 45621 and <a href="http://bugs.kde.org/show_bug.cgi?id=46215">#46215</a> (Fredrik Höglund)</li>
<li>remove krayon files to prevent possible trademark conflicts (Adrian Schroeter)</li>
</ul>

<h3>kdebase</h3>
<ul>
<li>kate:  Make saving of remote files during shutdown work. (<a href="http://bugs.kde.org/show_bug.cgi?id=55244">#55244</a>)</li>
<li>kcontrol/colors: Don't reset widget selection dropdown when selecting a different theme (<a href="http://bugs.kde.org/show_bug.cgi?id=58773">#58773</a>)</li>
<li>kcontrol/filetypes: Fixed removing of mimetypes specified by a .kdelnk file</li>
<li>kcontrol/input: fix application of large &amp; white cursors</li>
<li>kcontrol/services: don't show kxmlrpcd, kalarmd and kwrited if they're not installed</li>
<li>kdesud: support empty passwords</li>
<li>kdm: Fix i18n support for the GuiStyle and ColorScheme combos</li>
<li>kdm: fix <a href="http://bugs.kde.org/show_bug.cgi?id=58918">#58918</a>: don't reset local Xserver if termination is requsted</li>
<li>keditbookmarks: Various bugfixes</li>
<li>kio_sftp: Support OpenSSH 3.6+</li>
<li>kio_sftp: Support older sftp protocol revisions</li>
<li>kio_smb: Huge update for samba 3.0.</li>
<li>kicker: Improve icon scaling in service menus</li>
<li>kicker: "Terminal Sessions" special button supports sessions with dots in filename</li>
<li>kicker: "Terminal Sessions" special button with same sort order as the one in Konsole.</li>
<li>kicker: Fix mimetype resolution in the quickbrowser</li>
<li>kicker: Fixes for stuck tooltips</li>
<li>kicker: Do not create local directories for .desktop files used in service buttons 
if no needed</li>
<li>kicker: Fix apply button not working for quickstart menu entries settings (<a href="http://bugs.kde.org/show_bug.cgi?id=49113">#49113</a>)</li>
<li>kicker: Fix hide button preference effects reversed(<a href="http://bugs.kde.org/show_bug.cgi?id=60648">#60648</a>)</li>
<li>kicker:Various fixes for recent apps history</li>
<li>kicker/minipager: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=52592">
bug #52592: don't show redundant tooltips when the page already shows names</a></li>
<li>konsole: Added Ctrl+Shift+N as alternate default shortcut for "New Session".</li>
<li>konsole: Fixed fixed-width with proportional-font drawing routine.</li>
<li>konsole: Fixed problems with mouse and paste support and detached windows.</li>
<li>konsole: Let new color schemes take effect once kcmkonsole tells to do so.</li>
<li>konsole: Wheel mouse fixes for Mode_Mouse1000.</li>
<li>konqueror: Flicker fixes inside the iconview</li>
<li>konqueror: Fix for inter-process copy-past with the listview</li>
<li>konqueror: Make sure the tab close button is properly enabled when a 2nd 
tab is first open through context menu entries</li>
<li>konqueror: Made Ctrl+Enter for new tab in URL combo working again.</li>
<li>konqueror: Various sidebar fixes</li>
<li>konqueror: make sure that the location label truly disappears when disabled</li>
<li>konqueror: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=55163">
bug #55163: konqueror warning for repeated POSTs</a></li>
<li>konqueror: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=57699">
bug #57699: Konqueror crash when dragging image from about:konqueror</a></li>
<li>konqueror: Improve tracking of listviewitems so that we don't crash when they get deleted somewhat unexpectedly (<a href="http://bugs.kde.org/show_bug.cgi?id=59062">#59062</a>)</li>
<li>konqueror: Don't crash when updating toolbars without a part load 
(<a href="http://bugs.kde.org/show_bug.cgi?id=56832">#56832</a>)</li>
<li>konqueror: Properly handle 'stop' when still listing the directory in the info list view (<a href="http://bugs.kde.org/show_bug.cgi?id=54550">#54550</a>)</li>
<li>konqueror: Efficiency fix: make sure only one konqueror instance saves the history
when it changes</li>
<li>konqueror: Don't use deleted KFileItems. (<a href="http://bugs.kde.org/show_bug.cgi?id=59250">#59250</a>)</li>
<li>Don't crash when doing a copy w/o a currentItem (<a href="http://bugs.kde.org/show_bug.cgi?id=58836">#58836</a>)</li>
<li>konqueror: Stop activity when the user tries hard to enter a URL</li>
<li>konqueror: Properly calculate column indices for the meta info listview</li>
<li>konqueror: Hide the find part on a second click on its toolbar button (<a href="http://bugs.kde.org/show_bug.cgi?id=35841">#35841</a>)</li>
<li>konqueror: Handle _top/_blank, and so on, case-insensitively</li>
<li>konqueror: massively improve the speed of bookmark adding when a large number of tabs are open (only appears if kdenetwork is installed)</li>
<li>libkonq: make sure KFileTip does not hold on to stale file items</li>
<li>libkonq: precaution: make sure that no preview jobs hold on to icon view's items 
when it's cleared</li>
<li>libkonq: fixes to cut-and-paste</li>
<li>libkonq: network-friendlier paste button state polling</li>
<li>kwin: Fix deactivation of window when loosing focus</li>
<li>kwin: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=54668">bug #54668: New windows are drawn above open menus</a></li>
<li>kwin: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=54495">bug #54495: Some non-resizable windows can still be resized</a></li>
<li>kwin: Fixes for click raise</li>
<li>kwin: If a window is hover-unshaded, then it's not shaded for the outside world(<a href="http://bugs.kde.org/show_bug.cgi?id=53659">#53659</a>)</li>
<li>kwin: Keramik decoration: Properly detect XShape on Solaris</li>
<li>nsplugins: Fix relative URLs. (<a href="http://bugs.kde.org/show_bug.cgi?id=59700">#59700</a>)</li>
</ul>

<h3>kdebindings</h3>
<ul>
<li>Package / build updates.</li>
</ul>

<h3>kdeedu</h3>
<ul>
<li>kstars: always precess focus coordinates when date changes by a lot and tracking an object (Jason Harris)</li>
<li>kstars: the URL for requesting a DSS image must contain the J2000 coordinates (Jason Harris)</li>
<li>flashkard: <a href="http://bugs.kde.org/show_bug.cgi?id=58932">#58932</a> fix (Scott Wheeler)</li>
<li>Build fixes, cleanups, compiler warning fixes (Dirk Mueller)</li>
</ul>

<h3>kdegames</h3>
<ul>
<li>kbattleship: Improve IA, fix winner checking order</li>
<li>kbattleship: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=35319">#35319</a></li>
<li>atlantik: core management fixes (Rob Kaper)</li>
<li>atlantik: do not just delete players, also remove portfolios when resetting core (Rob Kaper)</li>
<li>atlantik: don't show money in portfolio when not in game (Rob Kaper)</li>
<li>atlantik: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=61118">#61118</a>: Multiple close button problem (Rob Kaper)</li>
<li>atlantik: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=61152">#61152</a>: Fix user sorting (Rob Kaper)</li>
<li>atlantik: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=61130">#61130</a>: Don't tell client that the game has started when it has not (Rob Kaper)</li>
<li>atlantik: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=54517">#54517</a></li>
<li>atlantik: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=54942">#54942</a></li>
<li>kolf: fixing the two most long-standing bugs (Jason Katz-Brown)</li>
<li>kmahjongg: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=51124">#51124</a>: Selected tiel not highlighted when hit is being displayed</li>
<li>kmahjongg: Correct alignment of column headings in highscore dialog.</li>
<li>kpat: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=59136">#59136</a>: "is game lost?" check</li>
<li>kpat: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=58566">#58566</a>: move cards toger for 40 &amp; 8</li>
<li>kpat: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=58101">#58101</a></li>
<li>kmahjongg: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=59189">#59189</a>: i18n names aren't displayed correctly in highscores</li>
<li>konquest: bug <a href="http://bugs.kde.org/show_bug.cgi?id=32255">#32255</a>: Allow 10 players, not just 5.</li>
<li>konquest: bug <a href="http://bugs.kde.org/show_bug.cgi?id=57516">#57516</a>: Fix prompting prolem when first player is eliminated during run</li>
</ul>

<h3>kdegraphics</h3>
<ul>
<li>gcc 3.4+ compile fixes (Dirk Mueller)</li>
<li>kiconedit: Answering Yes to save the modified file acted as No (Adrian Page)</li>
<li>kiconedit: Fix off-by-one error in "Select All"</li>
<li>kamera: fix status output message (Marcus Meissner)</li>
<li>kruler: do not use hardcoded font names</li>
<li>kghostview: the mimetype is application/x-gzpostscript actually for .ps.gz</li>
<li>kghostview: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=58904">#58904</a></li>
<li>kfax: Fix format string problem (Dirk Mueller)</li>
<li>kghostview: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=59041">#59041</a></li>
<li>kmrml: null-terminate clipboard data (Dirk Mueller)</li>
<li>kuickshow: Add license declarations (Carsten Pfeiffer)</li>
</ul>

<h3>kdemultimedia</h3>
<ul>
<li>kscd: add method to determine if KsCD is playing</li>
<li>noatun: memory leak and memory corruption fixes</li>
<li>noatun: Add volume slider for "simple" module</li>
<li>artsbuilder: fix several corruptsions and other crashes</li>
</ul>

<h3><a name="kdenetwork">kdenetwork</a></h3>
<ul>
<li>Desktop Sharing server (krfb): Fix various problem (crashes, hanging
server) caused by port scans while the server is accepting connections</li>
<li>Desktop Sharing server (krfb): Compile fix for older Solaris systems</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=58555">58555</a> (wrong filter rules in filter configuration or POP filter configuration dialogs)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=58679">58679</a> (KMail crashes with a folder called "new")</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=58814">58814</a> (Subject shown as "Unknown" until selected (big5 charset))</li>
<li>KMail: Display the correct folder name in the window title
bar when changing folders with the keyboard.</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=59195">59195</a> (Selecting 'Reply-All' in KMail results in a new message with no recipients under certain circumstances)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=53009">53009</a> (kmail --attach &lt;url&gt; does not preserve mime type in attachment)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=60304">60304</a> (Deleted address still reported as email address already in addressbook)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=60508">60508</a> (When receiving from maildir, cancelling the receive nukes the entire source maildir)</li>
<li>KMail: Remove the Delete icon from the default toolbar.</li>
<li>KMail: Use better background colors for signed/encrypted messages in case of a dark background color</li>
<li>KMail: Don't add detached signature as attachment when forwarding a PGP/MIME signed message inline</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=46185">46185</a> (detached signature of PGP/MIME signed message is shown as attachment in composer when editting a signed message)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=59048">59048</a> (remove of crypto plugin does not work)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=59978">59978</a> (Entry in Crypto Plugin is stored even when its entry is empty)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=48686">48686</a> by clearing the preview pane after all messages have been sent.</li>
</ul>

<h3>kdepim</h3>
<ul>
<li>korganizer: fix french and czech holidays (Dirk Mueller)</li>
<li>kaddressbook: Fix bug #60518</li>
<li>kdateedit: Fix bug #57465</li>
<li>kpilot: crash fixes</li>
<li>kalarmd: crash fix.</li>
<li>knotes: Fix bug #58658</li>
<li>build fixes</li>
</ul>

<h3>kdesdk</h3>
<ul>
<li>cervisia: Immediately save changed settings in the settings dialog (BR #<a href="http://bugs.kde.org/show_bug.cgi?id=45962">45962</a>).</li>
<li>cervisia: Fixed the size of the diff overview widget (BR #<a href="http://bugs.kde.org/show_bug.cgi?id=59275">59275</a>).</li>
<li>cervisia: Fixed crash when hidden files changed (BR #<a href="http://bugs.kde.org/show_bug.cgi?id=57797">57797</a>).</li>
<li>cervisia: Re-added the "clear" command to the context menu of the protocol view (BR #<a href="http://bugs.kde.org/show_bug.cgi?id=59276">59276</a>)</li>
<li>kbabel: Fix TP robot interaction ( BR #<a href="http://bugs.kde.org/show_bug.cgi?id=57642">57642</a>)</li>
<li>kbabel: Allow upgrade translation database from Berkeley Database II to Berkeley Database IV</li>
<li>kbabel: DocBook entities can contain dots</li>
<li>kapptemplate: Fix UIC detection for KDE 2.x projects.</li>
<li>kapptemplate: tail -n / head -n POSIX compliance fix.</li>
</ul>

<h3>kdetoys</h3>
<ul>
</ul>

<h3>kdeutils</h3>
<ul>
<li>kcalc: ignore keypresses combined with alt, Meta or Control</li>
<li>kjots: Fix DnD issues</li>
<li>kedit: correct overwrite/insert handling</li>
<li>kedit: fix session management</li>
<li>ark: fix .rar archive handling.</li>
<li>klaptopdaemon: icon updates.</li>
<li>kdf: fix handling of mountpoints with whitespaces</li>
<li>build system fixes</li>
</ul>

<h3>quanta</h3>
<ul>
<li>Bugfixes:
  <ul>
    <li>use the project base directory as the working directory for script actions [#<a href="http://bugs.kde.org/show_bug.cgi?id=36415">36415</a>]</li>
    <li>warn about existing files on rename [#<a href="http://bugs.kde.org/show_bug.cgi?id=60187">60187</a>]</li>
    <li>encode the subject in a mailto url [#<a href="http://bugs.kde.org/show_bug.cgi?id=60275">60275</a>]</li>
    <li>fix conflicting shortcut for Misc. Tag [#<a href="http://bugs.kde.org/show_bug.cgi?id=60616">60616</a>]</li>
    <li>fix possible crashes when using plugins</li>
    <li>quote also the numbers in the attribute values</li>
    <li>show the directory on the remote PC when using Save As for remote files, instead trying to switch to the file on the local disc</li>
    <li>don't break the doctype line when changing the DTD</li>
    <li>insert valid doctype line when using the Quick Start button</li>
    <li>don't crash when trying to edit tags without proper .tag file (like &lt;b>)</li>
    <li>enable copying from documentation/preview even if the user uses separate clipboard and selection</li>
  </ul>
</li>
<li>Enhancements:
  <ul>
    <li>don't insert spaces before CSS values (it disturbs some browsers) [#<a href="http://bugs.kde.org/show_bug.cgi?id=41227">41227</a>]</li>
    <li>default to the project dir when saving a new document [#<a href="http://bugs.kde.org/show_bug.cgi?id=57654">57654</a>]</li>
    <li>replace %pid with Quanta's pid and %input with the selected input source in the script action line</li>
  </ul>
</li>
</ul>

<?php include "footer.inc" ?>
