<?php
  $page_title = "KDE 3.5.1 to KDE 3.5.2 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible the additions
and corrections that occurred in KDE between the 3.5.1 and 3.5.2 releases.
Nevertheless it should be considered as incomplete.
</p>

<!-- A list of bugs which are fixed but not build in...

-->

<!-- template for a bug
			<li> (<a href="http://bugs.kde.org/show_bug.cgi?id=">bug #</a>)</li>
-->

<h3 id="arts">arts <font size="-2">[<a href="3_5_2/arts.txt">all SVN changes</a>]</font></h3>

<h3 id="kdelibs">kdelibs <font size="-2">[<a href="3_5_2/kdelibs.txt">all SVN changes</a>]</font></h3>
<ul>
  <li>Don't restore files to recent files list that don't exist anymore. <a href="http://bugs.kde.org/show_bug.cgi?id=27581">bug #27581</a></li>
  <li>KIO-KFile: Fix problems with mounting filesystems with ACL (<a href="http://bugs.kde.org/show_bug.cgi?id=114731">bug #114731</a>)</li>
  <li>Kate
	  <ul>
		<li>Make the spelling dialog modal. <a href="http://bugs.kde.org/show_bug.cgi?id=120821">bug #120821</a></li>
		<li>Alternate Bash heredoc style highlights correctly. <a href="http://bugs.kde.org/show_bug.cgi?id=119952">bug #119952</a></li>
	  </ul>
   </li>
	<li>KHTML
		<ul>
			<li>Performance improvements:
			    <ul>
                                  <li>Improve rendering speed of some semi-transparent pixmap backgrounds. (Part of <a href="http://bugs.kde.org/show_bug.cgi?id=114938">bug #114938</a>)</li>
		                  <li>Improve performance of JavaScript error logging widget, to help deal with sites with completely broken JavaScript which continuously causes errors. (<a href="http://bugs.kde.org/show_bug.cgi?id=117834">bug 117834</a>)</li>
				  <li>Better performance for the classname selector</li>
			    </ul>
			</li>
                        <li>Get iframes, objects and some other overlaid widgets to obey their stacking context at last
(a.k.a, keep flash and iframes *under* dhtml menus). (<a href="http://bugs.kde.org/show_bug.cgi?id=31121">bug #31121</a>)</li>
                        <li id="ib1">Implement CSS2.1 quirk about behavior of overflow and root and body. Now we pass Acid2 according to Opera's standard, but in an actual release version.</li>
			<li id="ib2">Implement selectionStart, selectionEnd, setSelectionRange on textarea and text input tags. They are used by editting buttons in phpBB, various wikis, and by autocompletion packages like 
                        Google suggest. Also fix some bugs in synchronization between rendering tree and the DOM for textareas. (<a href="http://bugs.kde.org/show_bug.cgi?id=71451">bug #71451</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=79371">bug #79371</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=113217">bug #113217</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=121297">bug #121297</a>,
                        and <a href="http://bugs.kde.org/show_bug.cgi?id=109092">bug #109092</a>)</li>
			<li>Improved keyboard event support, implementing the API in the latest DOM3 Events working group note. Also improve compatibility with other browsers, and fix a couple of crashers. (<a href="http://bugs.kde.org/show_bug.cgi?id=120408">bug #120408</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=118321">bug #118321</a>)</li>
			<li>Properly scale CSS line height when zooming. (<a href="http://bugs.kde.org/show_bug.cgi?id=105145">bug #105145</a>)</li>
                        <li>Multiple fixes to the table DOM. (<a href="http://bugs.kde.org/show_bug.cgi?id=100105">bug #100105</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=119697">bug #119697</a>)</li>
                        <li>Better frame sizing calculation. (<a href="http://bugs.kde.org/show_bug.cgi?id=60113">bug #60113</a>,
                                <a href="http://bugs.kde.org/show_bug.cgi?id=69074">bug #69074</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=101753">bug #101753</a>)</li>
			<li>Better handling of the case where form-related tags get moved by parser during residual style handling. (<a href="http://bugs.kde.org/show_bug.cgi?id=122273">bug #122273</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=122968">bug #122968</a>)</li>
                        <li>Fix the DOM operation handling to not be so picky about the DTD. This makes us match other browsers better, and lets us remove various evil hacks in the XML parser (<a href="http://bugs.kde.org/show_bug.cgi?id=110426">bug #110426</a>)</li>
			<li>Various getComputedStyle improvements. (<a href="http://bugs.kde.org/show_bug.cgi?id=119327">bug #119327</a>, part of <a href="http://bugs.kde.org/show_bug?id=122072">bug #122072</a>)</li>
                        <li>Various text-indent fixes. (<a href="http://bugs.kde.org/show_bug?id=90510">bug #90510</a>, <a href="http://bugs.kde.org/show_bug?id=96275">bug #96275</a>)</li>
                        <li>Some fixes for namespace handling in XML and CSS.</li>
                        <li>Compatibility improvements in some collections. (<a href="http://bugs.kde.org/show_bug.cgi?id=121326">bug #121326</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=106930">bug #106930</a>, 
                                <a href="http://bugs.kde.org/show_bug.cgi?id=123507">bug #123507</a>)</li>
                        <li>Fix JavaScript timer events over midnight.</li>
			<li>Fix handling of error event. (<a href="http://bugs.kde.org/show_bug.cgi?id=98461">bug #98461</a>)</li>
			<li>Do not assign the same AccessKey to different javascript: links. (<a href="http://bugs.kde.org/show_bug.cgi?id=121144">bug #121144</a>)</li>
                        <li>Do not improperly honor maxlength on file input elements. </li>
                        <li>Properly reload iframe when source re-set. </li>
                        <li>Make usemap case-insensitive.</li>
                        <li>Better parsing of malformed cols and rows parameters of textarea. (<a href="http://bugs.kde.org/show_bug.cgi?id=122287">bug #122287</a>)</li>
                        <li>Various fixes to DOM and non-standard bindings to CSS. (part of <a href="http://bugs.kde.org/show_bug.cgi?id=120925">bug #120925</a>).</li>
			<li>Various crash fixes. (<a href="http://bugs.kde.org/show_bug.cgi?id=121513">bug #121513</a>, 
                            <a href="http://bugs.kde.org/show_bug.cgi?id=115534">bug #115534</a>,
                            <a href="http://bugs.kde.org/show_bug.cgi?id=116457">bug #116457</a>,
                            <a href="http://bugs.kde.org/show_bug.cgi?id=116457">bug #120062</a>)</li>
                        <li>Various rendering fixes, including for <a href="http://bugs.kde.org/show_bug.cgi?id=121653">bug #121653</a>,
                                    <a href="http://bugs.kde.org/show_bug.cgi?id=121653">bug #121653</a>,
                                    <a href="http://bugs.kde.org/show_bug.cgi?id=107521">bug #107521</a>, and many others.</li>
		</ul>
	</li>
        <li>KJS
            <ul>
                <li>Performance fix: do not redundantly re-attach the JavaScript debugger.</li>
                <li>Free up some "future reserved keywords" for use as identifiers, to match other browsers. (part of <a href="http://bugs.kde.org/show_bug.cgi?id=120925">bug #120925</a>)</li>
                <li>Accept trailing semicolons in initializer lists.</li>
		<li>Various RegExp object improvements: support .compile, properly report some errors, plug some memory leaks. 
                    (<a href="http://bugs.kde.org/show_bug.cgi?id=120108">bug #120108</a>)</li>
                <li>Give JavaScript debugger some stack room to operate when handling stack overflow exceptions. (<a href="http://bugs.kde.org/show_bug.cgi?id=121652">bug #121652</a>)</li>
		<li>Fix bugs in latin1 to ucs conversion.</li>
	    </ul>
        </li>
</ul>

<h3 id="kdeaccessibility">kdeaccessibility <font size="-2">[<a href="3_5_2/kdeaccessibility.txt">all SVN changes</a>]</font></h3>

<h3 id="kdeaddons">kdeaddons <font size="-2">[<a href="3_5_2/kdeaddons.txt">all SVN changes</a>]</font></h3>

<h3 id="kdeadmin">kdeadmin <font size="-2">[<a href="3_5_2/kdeadmin.txt">all SVN changes</a>]</font></h3>

<h3 id="kdeartwork">kdeartwork <font size="-2">[<a href="3_5_2/kdeartwork.txt">all SVN changes</a>]</font></h3>

<h3 id="kdebase">kdebase <font size="-2">[<a href="3_5_2/kdebase.txt">all SVN changes</a>]</font></h3>
<ul>
   <li>Klipper
	   <ul>
			<li id="ib3">Handle URLs as URLs and not as Text (<a href="http://bugs.kde.org/show_bug.cgi?id=121114">bug #121114</a>)</li>
            <li>Prevent crashes caused by corrupted history (<a href="http://bugs.kde.org/show_bug.cgi?id=109161">bug #109161</a>)</li>
            <li>XFixes support (especially helps with broken clipboard implementations of some non-KDE applications) (<a href="http://bugs.kde.org/show_bug.cgi?id=101087">bug #101087</a>)</li>
            <li>Images are ignored by default, add 'IgnoreImages=false' to '[General]' in klipperrc (<a href="http://bugs.kde.org/show_bug.cgi?id=109032">bug #109032</a>)</li>
            <li>Avoid repeated action popup with the same URL with some non-KDE applications with broken clipboard implementations (<a href="http://bugs.kde.org/show_bug.cgi?id=97595">bug #97595</a>)</li>
	   </ul>
   </li>
	<li>Konsole
		<ul>
			<li>Correct issue where history size is unlimited when dealing with History options in profiles (<a href="http://bugs.kde.org/show_bug.cgi?id=120046">bug #120046</a>)</li>
			<li>Correctly set Tab bar when set to Dynamic Hide after session restore (<a href="http://bugs.kde.org/show_bug.cgi?id=121688">bug #121688</a>)</li>
		</ul>
	</li>
    <li>Country settings
        <ul>
            <li>
                Fix short date format for Switzerland
                (<a href="http://bugs.kde.org/show_bug.cgi?id=122574">bug #122574</a>)
            </li>
            <li>Fix address format (especially P.O. Box) for Switzerland</li>
        </ul>
    </li>
  <li>KSysGuard
    <ul>
      <li>Show the sensors with values of more than two digits correctly in the applet</li>
    </ul>
  </li>
  <li>Kicker
    <ul>
      <li>Connect the applications to systray correctly on startup (<a href="http://bugs.kde.org/66062">bug #66062</a>)</li>
      <li>Panels properly reserve space at screen edges even for differently sized Xinerama screens (<a href="http://bugs.kde.org/show_bug.cgi?id=94470">bug #94470</a>)</li>
    </ul>
  </li>
 <li>Konqueror
    <ul>
      <li>Resolve symlinks only on the desktop (<a href="http://bugs.kde.org/63014">bug #63014</a>)</li>
    </ul>
 </li>
 <li>KWin
    <ul>
        <li>Added new window-specific rules for OpenOffice.org, XV and Mozilla family applications for turning off focus stealing prevention, as these applications don't work well with it</li>
        <li>Reverted a workaround that as a side-effect had broken systray docking of some applications (<a href="http://bugs.kde.org/show_bug.cgi?id=100177">bug #100177</a>)</li>
        <li>Each virtual desktop has a separate focus chain (<a href="http://bugs.kde.org/show_bug.cgi?id=33701">bug #33701</a>)</li>
    </ul>
 </li>
 <li>KDesktop
    <ul>
        <li>Fixed stacking of some dialogs (<a href="http://bugs.kde.org/show_bug.cgi?id=89951">bug #89951</a>,<a href="http://bugs.kde.org/show_bug.cgi?id=113556">bug #113556</a>)</li>
    </ul>
 </li>
</ul>

<h3 id="kdebindings">kdebindings <font size="-2">[<a href="3_5_2/kdebindings.txt">all SVN changes</a>]</font></h3>

<h3 id="kdeedu">kdeedu <font size="-2">[<a href="3_5_2/kdeedu.txt">all SVN changes</a>]</font></h3>
<ul>
	<li>Kalzium
		<ul>
			<li>Do not crash anymore when switching element in the Detail Information dialog (<a href="http://bugs.kde.org/show_bug.cgi?id=109035">bug #109035</a>)</li>
			<li>Display the correct abundance of crustal rocks (<a href="http://bugs.kde.org/show_bug.cgi?id=120567">bug #120567</a>)</li>
			<li>Fixed typo in the origin of the name of the element Silver (Ag) (<a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=353582">Debian/353582</a>)</li>
			<li>Really show as translated all the strings in the glossary (<a href="http://bugs.kde.org/show_bug.cgi?id=114882">bug #114882</a>)</li>
		</ul>
	</li>
	<li>blinKen
		<ul>
			<li>Only use 'steve.ttf'-font if possible (<a href="http://bugs.kde.org/show_bug.cgi?id=118399">bug #118399</a>)</li>
		</ul>
	</li>
	<li>KBruch
		<ul>
			<li>Layoutfixes for RTL-languages (<a href="http://bugs.kde.org/show_bug.cgi?id=116831">bug #116831</a>)</li>
		</ul>
	</li>
	<li>KHangman
		<ul>
			<li>blank/missing menu comments (tooltips) (<a href="http://bugs.kde.org/show_bug.cgi?id=121577">bug #121577</a>)</li>
		</ul>
	</li>
	<li>KStars
		<ul>
			<li>All time fields in the astrocalculator now show the current UT, not local time (<a href="http://bugs.kde.org/show_bug.cgi?id=119960">bug #119960</a>)</li>
			<li>All deep-sky objects were shown with incorrect position angle (flipped horizontally)(<a href="http://bugs.kde.org/show_bug.cgi?id=114605">bug #114605</a>)</li>
			<li>UI-improvements (<a href="http://bugs.kde.org/show_bug.cgi?id=113465">bug #113465</a>)</li>
			<li>UI-improvements (tools) (<a href="http://bugs.kde.org/show_bug.cgi?id=119963">bug #119963</a>)</li>
			<li>Fix some typos and string improvements in calculator (<a href="http://bugs.kde.org/show_bug.cgi?id=119959">bug #119959</a>)</li>
			<li>Some fixes in the datafile (bugs
				<a href="http://bugs.kde.org/show_bug.cgi?id=115108">#115108</a>,
				<a href="http://bugs.kde.org/show_bug.cgi?id=114607">#114607</a>,
				<a href="http://bugs.kde.org/show_bug.cgi?id=114608">#114608</a>,
				<a href="http://bugs.kde.org/show_bug.cgi?id=114609">#114609</a>)
			</li>
			<li>Do not fade transient label if the object has gained a persistent label by becoming the centered object.</li>
		</ul>
	</li>
	<li>Kig
		<ul>
			<li>Do not apply the line style when drawing the arrow of vectors (<a href="http://bugs.kde.org/show_bug.cgi?id=122285">#122285</a>)</li>
			<li>Do not show inappropriate entries for text labels in the popup menu (
				<a href="https://bugs.kde.org/show_bug.cgi?id=122381">#122381</a>,
				<a href="https://bugs.kde.org/show_bug.cgi?id=122382">#122382</a>)</li>
			<li>Rename the &quot;Size&quot; menu item in the object popup menu to the more appropriate &quot;Set Pen Width&quot;
				(<a href="https://bugs.kde.org/show_bug.cgi?id=122445">#122445</a>)</li>
		</ul>
	</li>
	<li>KWordQuiz
		<ul>
			<li>Don't crash when switching back to editor. (<a href="http://bugs.kde.org/show_bug.cgi?id=122110">#122110</a>)</li>
			<li>Display incorrect answers correctly. (<a href="http://bugs.kde.org/show_bug.cgi?id=122111">#122111</a>)</li>
		</ul>
	</li>
</ul>

<h3 id="kdegames">kdegames <font size="-2">[<a href="3_5_2/kdegames.txt">all SVN changes</a>]</font></h3>
<ul>
	<li>KMahjongg
		<ul>
			<li>Not looking for themes/background/etc on .kde (<a href="http://bugs.kde.org/show_bug.cgi?id=121807">bug #121807</a>)</li>
		</ul>
	</li>
</ul>

<h3 id="kdegraphics">kdegraphics <font size="-2">[<a href="3_5_2/kdegraphics.txt">all SVN changes</a>]</font></h3>
<ul>
	<li>KDVI
		<ul>
			<li>Fix problems with PS header inclusion (<a href="http://bugs.kde.org/show_bug.cgi?id=105477">bug #105477</a>)</li>
		</ul>
	</li>
	<li>KPDF
		<ul>
			<li>Don't destroy the document when overwriting itself with save as...(<a href="http://bugs.kde.org/show_bug.cgi?id=120674">bug #120674</a>)</li>
			<li>Fix crash on some documents (<a href="http://bugs.kde.org/show_bug.cgi?id=120985">bug #120985</a>)</li>
			<li>Fix crash when embedded on Konqueror (<a href="https://bugs.kde.org/show_bug.cgi?id=121556">bug #121556</a>)</li>
		</ul>
	</li>
    <li>KolourPaint
        <ul>
             <li>Printing improvements (<a href="https://bugs.kde.org/show_bug.cgi?id=108976">bug #108976</a>)
                 <ul>
                     <li>Respect image DPI</li>
                     <li>Fit image to page if image is too big</li>
                     <li>Center image on page</li>
                 </ul>
             </li>
        </ul>
    </li>
</ul>

<h3 id="kdemultimedia">kdemultimedia <font size="-2">[<a href="3_5_2/kdemultimedia.txt">all SVN changes</a>]</font></h3>
<ul>
	<li>JuK
		<ul>
			<li>Manual Column widths notice interferes with dragging columns (<a href="http://bugs.kde.org/show_bug.cgi?id=121176">bug #121176</a>)</li>
			<li>Action List in Configure Shortcuts includes up to as many Add to CD items as you have playlists(<a href="http://bugs.kde.org/show_bug.cgi?id=121621">bug #121621</a>)</li>
			<li>Crash after hiding Play Queue fixed (<a href="http://bugs.kde.org/show_bug.cgi?id=100564">bug #100564</a>)</li>
		</ul>
	</li>
	<li>KMix
		<ul>
			<li>KMixApplet, if shown on vertical Kicker Panel, shows always icons (even if there is not enough space) (<a href="http://bugs.kde.org/show_bug.cgi?id=97666">bug #97666</a>)</li>
			<li>Kmix saves the wrong Master Channel under certain cictumstances (<a href="http://bugs.kde.org/show_bug.cgi?id=115045">bug #115045</a>)</li>
			<li>Split channel in KMixApplet shows one channel as a slider, the other numerical (<a href="http://bugs.kde.org/show_bug.cgi?id=122114">bug # 122114</a>)</li>
			<li>Selecting master channel does not update tray icon (<a href="http://bugs.kde.org/show_bug.cgi?id=117719">bug #117719</a>)</li>
		</ul>
	</li>
</ul>

<h3 id="kdenetwork">kdenetwork <font size="-2">[<a href="3_5_2/kdenetwork.txt">all SVN changes</a>]</font></h3>
<ul>
	<li>KWiFiManager
		<ul>
			<li>Make the tray icon transparent also if numbers are not shown (<a href="http://bugs.kde.org/show_bug.cgi?id=105637">bug #105637</a>)</li>
		</ul>
	</li>
</ul>

<h3 id="kdepim">kdepim <font size="-2">[<a href="3_5_2/kdepim.txt">all SVN changes</a>]</font></h3>
<ul>
	<li>Akregator
                <ul>
			<li>Feed archive: Move feed limit up to 500 feeds instead of 384 (due to file limit a process 
                        can have opened at a time)</li>
			<li>Feed archive: Lock feed archive to prevent multiple write access which can corrupt feed
                            archives and cause crashes on startup</li>
			<li>Feed fetching: Do not use Konqueror cache settings, but use &quot;refresh&quot; mode by default
                            (Check with server if the cached file is up-to-date, reload if not)</li>
			<li>Feed parser: read feed description and homepage link for Atom feeds (<a href="http://bugs.kde.org/show_bug.cgi?id=123140">bug #123140</a>)</li>
			<li>Feed parser: Improve parsing of Atom content (especially for 0.3 feeds), 
                            do not strip HTML markup for types like &quot;text/html&quot;</li>
			<li>Feed parser: ignore unknown or invalid version attribute value in the &lt;rss&gt; tag and just assume RSS 2.0. (<a href="http://bugs.kde.org/show_bug.cgi?id=118793">bug #118793</a>)</li>
		</ul>
	</li>
	<li>KAddressBook
		<ul>
			<li id="ib4">Sometimes KAddressBook lost all data. This is now fixed.</li>
			<li>Wait for complete loading of all addressbook resources before execution of commandline and DCOP commands (e.g. "Open in Addressbook" by KMail) (<a href="http://bugs.kde.org/show_bug.cgi?id=87233">bug #87233</a>)</li>
		</ul>
	</li>
	<li>KAlarm
		<ul>
			<li>Fix kalarmd hang when triggering late alarm and KAlarm run mode is on-demand</li>
			<li>Prevent defunct kalarm processes when run mode is on-demand</li>
			<li>Fix column widths when main window is resized, if columns have been reordered</li>
			<li>Add Select All and Deselect actions &amp; shortcuts for alarm and template lists</li>
		</ul>
	</li>
</ul>

<h3 id="kdesdk">kdesdk <font size="-2">[<a href="3_5_2/kdesdk.txt">all SVN changes</a>]</font></h3>
<ul>

<li>KBabel
    <ul>
        <li>
            Improve loading of Gettext PO files, especially in case of recoverable or unrecoverable errors.
            (bugs <a href="http://bugs.kde.org/show_bug.cgi?id=117968">#117968</a>,
            <a href="http://bugs.kde.org/show_bug.cgi?id=120200">#120200</a>,
            <a href="http://bugs.kde.org/show_bug.cgi?id=121236">#121236</a>)
        </li>
        <li>
            Settings of the SVN/CVS dialogs are now project-releated (and not global anymore).
        </li>
        <li>Improve sending PO file(s) as email.</li>
    </ul>
</li>

<li>Umbrello
		<ul>
			<li>Use horizontal and vertical lines when drawing association <a href="http://bugs.kde.org/67223">#67223</a>)</li>
			<li>Import Rose model files (no diagrams yet, <a href="http://bugs.kde.org/81364">#81364</a>)</li>
			<li>Automatically fill useful info into the <a href="http://www.geeksoc.org/~jr/umbrello/uml-devel/9253.html">Perl writer</a> <a href="http://www.geeksoc.org/~jr/umbrello/uml-devel/9254.html">heading template</a></li>
                        <li>Documentation for association roles not saved (<a href="http://bugs.kde.org/105661">#105661</a>)</li>
			<li>Default data types not added for old Java generator (<a href="http://bugs.kde.org/115991">#115991</a>)</li>
			<li>Problem reordering methods in classes/interfaces (<a href="http://bugs.kde.org/119991">#119991</a>, <a href="http://bugs.debian.org/348940">Debian/348940</a>)</li>
			<li>Problem with <a href="http://sourceforge.net/mailarchive/forum.php?thread_id=9558795&amp;forum_id=472">font size computation/word wrap</a> in note widgets</li>
			<li>Custom operations in sequence diagrams become class operations (<a href="http://bugs.kde.org/120337">#120337</a>)</li>
			<li>Fork/join symbol appears as a black box (<a href="http://bugs.kde.org/120455">#120455</a>)</li>
			<li>Multiplicity labels positioned incorrectly when moving entities (<a href="http://bugs.kde.org/120598">#120598</a>)</li>
			<li>Types of entity's attributes are displayed instead of their names (<a href="http://bugs.kde.org/120742">#120742</a>)</li>
			<li>Unable to delete entity from entity list in &quot;UML Model&quot; frame (<a href="http://bugs.kde.org/120750">#120750</a>)</li>
			<li>Interface names not italicized in diagram view (<a href="http://bugs.kde.org/120761">#120761</a>)</li>
			<li>Cannot Resize Sequence Diagram Synchronous Messages (<a href="http://bugs.kde.org/120910">#120910</a>)</li>
			<li>Sequence diagram: constructor message only works at 100 percent zoom (<a href="http://bugs.kde.org/121238">#121238</a>)</li>
			<li>Documentation for associations is not retained (<a href="http://bugs.kde.org/121478">#121478</a>, <a href="http://bugs.kde.org/122063">#122063</a>)</li>
			<li>Crash when importing Python files (<a href="http://bugs.kde.org/121952">#121952</a>)</li>
			<li>&quot;void&quot; is imported as a class (<a href="http://bugs.kde.org/122184">#122184</a>)</li>
			<li>Crash when creating a datatype with the same name as a class (<a href="http://bugs.kde.org/122188">#122188</a>)</li>
			<li>Crash when a non existing data type is used for an argument of a new method (<a href="http://bugs.kde.org/122497">#122497</a>)</li>
			<li>Drag'n'drop doesn't put class under mouse cursor when zoom is not 1:1 (<a href="http://bugs.kde.org/122293">#122293</a>)</li>
			<li>Crash when refusing to rename a class on importing typedef (<a href="http://bugs.kde.org/122914">#122914</a>)</li>
			<li>Java import fails at abstract methods or interfaces (<a href="http://bugs.kde.org/123661">#123661</a>)</li>
		</ul>
	</li>
</ul>

<h3 id="kdetoys">kdetoys <font size="-2">[<a href="3_5_2/kdetoys.txt">all SVN changes</a>]</font></h3>

<h3 id="kdeutils">kdeutils <font size="-2">[<a href="3_5_2/kdeutils.txt">all SVN changes</a>]</font></h3>
<ul>
<li>KJots
	<ul>
		<li>Make non-Unicode books display and save correctly. <a href="http://bugs.kde.org/show_bug.cgi?id=122876">bug #122876</a></li>
		<li>Corrected possible data corruption when switching to/from Unicode mode.</li>
		<li>Added keyboard shortcuts for deleting pages/books.</li>
	</ul>
</li>

</ul>

<h3 id="kdevelop">kdevelop <font size="-2">[<a href="3_5_2/kdevelop.txt">all SVN changes</a>]</font></h3>

<h3 id="kdewebdev">kdewebdev <font size="-2">[<a href="3_5_2/kdewebdev.txt">all SVN changes</a>]</font></h3>

<h4>Quanta Plus</h4>
<ul>
<li>allow opening of read-only remote files, like from http:// [<a href="http://bugs.kde.org/show_bug.cgi?id=120632">bug #120632</a>]
</li>
<li>fix insertion of files to the project [<a href="http://bugs.kde.org/show_bug.cgi?id=120629">bug #120629</a>]</li>
<li>fix detection of existing shortcuts</li>
<li>ignore special areas inside comments</li>
<li>set the user action tooltips correctly</li>
<li>fix random crashing when editing PHP files [bugs <a href="http://bugs.kde.org/show_bug.cgi?id=121280">#121280</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=122475">#122475</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=122252">#122252</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=120983">#120983</a>]</li>
<li>fix a crash when using the attribute tree</li>
<li>fix insertion of relative URLs from the attribute tree</li>
<li>fix resolving of relative URLs when there are symlinks around</li>
<li>fix addition of local directories to the project (creates weird project error like in <a href="http://bugs.kde.org/show_bug.cgi?id=122419">bug #122419</a>)</li>
<li>fix for dtep data of xhtml1.1 [<a href="http://bugs.kde.org/show_bug.cgi?id=122272">bug #122272</a>]</li>
<li>fix some bugs in the new project wizard</li>
<li>always reload a file if the user wants it [<a href="http://bugs.kde.org/121329">bug #121329</a>]</li>
<li>fix previewing of read-only files</li>
<li>reload project tree for remote projects after rescanning the project folder</li>
<li>less reparsing of the document while typing is in progress</li>
<li>open dropped files [<a href="http://bugs.kde.org/show_bug.cgi?id=102605">bug #102605</a>]</li>
<li>don't try to remove an empty, unmodified buffer, if it is the last opened one [<a href="http://bugs.kde.org/show_bug.cgi?id=111599">bug #111599</a>]</li>
<li>remember cursor position for project documents [<a href="http://bugs.kde.org/show_bug.cgi?id=101966">bug #101966</a>]</li>
<li>add the standard show/hide menubar action [<a href="http://bugs.kde.org/show_bug.cgi?id=113064">bug #113064</a>]</li>
<li>add possibility to limit the number of recent files/projects. No
GUI, use "Recent Files Limit" in the [General Options] section [<a href="http://bugs.kde.org/show_bug.cgi?id=113309">bug #113309</a>]</li>
<li>show the project name in the window titlebar</li>
<li>mark uploaded files as uploaded even if upload fails later [<a href="http://bugs.kde.org/111857">bug #111857</a>]</li>
<li>new DCOP methods:
<ul>
  <li>WindowManagerIf::uploadURL(url, profile, markOnly)</li>
  <li>QuantaIf::groupElements(groupName)</li>
</ul>
</li>

</ul>

<h4>Kommander</h4>
<ul>
<li>accept DCOP methods without parantheses</li>
</ul>

<?php include "footer.inc" ?>
