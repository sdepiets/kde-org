2005-05-31 23:42 +0000 [r420398]  mueller

	* dcoppython/tools (removed): removed here too. QPL licensed stuff
	  that is not compiled and does not work anymore.

2005-06-04 12:47 +0000 [r422062]  rdale

	* smoke/qt/qscintilla_header_list (added): * Added the QScintilla
	  headers

2005-06-04 13:15 +0000 [r422073]  rdale

	* korundum/rubylib/rbkconfig_compiler/Makefile.am, qtruby/README,
	  qtruby/rubylib/designer/rbuic/form.cpp, kalyptus/kalyptus,
	  kalyptus/kalyptusCxxToSmoke.pm, qtruby/rubylib/qtruby/Qt.cpp,
	  qtruby/rubylib/designer/rbuic/object.cpp, korundum/ChangeLog,
	  qtruby/rubylib/designer/rbuic/widgetdatabase.cpp, qtruby/INSTALL,
	  smoke/qt/configure.in.in,
	  qtruby/rubylib/designer/rbuic/domtool.cpp,
	  qtruby/rubylib/designer/rbuic/subclassing.cpp,
	  qtruby/rubylib/designer/rbuic/uic.cpp,
	  qtruby/rubylib/qtruby/handlers.cpp,
	  korundum/rubylib/korundum/Korundum.cpp,
	  qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  qtruby/rubylib/designer/rbuic/main.cpp, qtruby/AUTHORS,
	  smoke/qt/Makefile.am, smoke/qt/generate.pl.in, qtruby/ChangeLog:
	  * Promoted various fixes and enhancements to the release branch

2005-06-04 17:13 +0000 [r422160]  bero

	* kjsembed/jsconsolewidget.h: Add missing KJSEMBED_EXPORT

2005-06-08 07:44 +0000 [r423370]  rdale

	* korundum/ChangeLog,
	  korundum/rubylib/korundum/lib/KDE/korundum.rb: * Promoted DCOP
	  signal fix to the release branch

2005-06-15 00:01 +0000 [r425547]  mueller

	* smoke/kde/Makefile.am: fix parallel build

2005-06-15 19:08 +0000 [r425858]  sedwards

	* python/Makefile.am: Merging in build system fixes from trunk. *
	  respects the 'prefix ' value from configure. * BSD compat fix. *
	  makes extra sure that our 'sip' dir is searched first when
	  loading modules.

2005-06-27 15:18 +0000 [r429401]  binner

	* kdebindings.lsm: 3.4.2 preparations

2005-07-12 16:09 +0000 [r434016]  rdale

	* qtruby/rubylib/qtruby/handlers.cpp, qtruby/ChangeLog: * Promoted
	  fix for memory leak bug with QString return types to the release
	  branch.

