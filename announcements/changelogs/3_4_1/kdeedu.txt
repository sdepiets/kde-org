2005-03-05 01:47 +0000 [r394977]  annma

	* khangman/khangman/khangman.kcfg, khangman/khangman/khangman.cpp:
	  backport support for Slovak and bug fix in case language is empty
	  in kdeglobals

2005-03-07 07:23 +0000 [r395411]  benb

	* debian/kdeedu-doc-html.doc-base.klatin (added),
	  debian/kwordquiz.install (added), debian/kvoctrain.install,
	  debian/lx200classic.1, debian/kturtle.README.Debian (added),
	  debian/kig.1, debian/kiten.1, debian/kmessedwords.1,
	  debian/lx200_16.1, debian/kmplot.install,
	  debian/kwordquiz.README.Debian (added),
	  debian/libkdeedu1.install, debian/README.Packaging (removed),
	  debian/control, debian/fliccd.1 (added), debian/kturtle.manpages
	  (added), debian/klatin.menu (added), debian/klatin.install
	  (added), debian/flashkard.manpages (removed), debian/flashkard.1
	  (removed), debian/klatin.override (added),
	  debian/kstars.manpages, debian/keducabuilder.1,
	  debian/kwordquiz.menu (added), debian/kwordquiz.xpm (added),
	  debian/flashkard.install (removed), debian/kstars.1,
	  debian/libkdeedu-dev.install, debian/flashkard.README.Debian
	  (removed), debian/kstars.install, debian/kwordquiz.override
	  (added), debian/kbruch.1, debian/kalzium.install,
	  debian/klatin.xpm (added), debian/lx200autostar.1,
	  debian/khangman.install, debian/rules, debian/libkdeedu1.shlibs,
	  debian/kturtle.menu (added), debian/kpercentage.install,
	  debian/libkdeedu-dev.manpages (added), debian/kvoctrain.1,
	  debian/kturtle.install (added), debian/flashkard.xpm (removed),
	  debian/flashkard.menu (removed), debian/clean-sources (removed),
	  debian/langen2kvtml.1, debian/kitengen.1, debian/kmplot.1,
	  debian/celestrongps.1, debian/kturtle.override (added),
	  debian/copyright, debian/indiserver.1, debian/klatin.docs
	  (added), debian/kverbos.1,
	  debian/kdeedu-doc-html.doc-base.kturtle (added), debian/kig.menu,
	  debian/flashkard.override (removed), debian/kmessedwords.install,
	  debian/kiten.install, debian/kig.install,
	  debian/kdeedu-doc-html.doc-base.kwordquiz (added),
	  debian/kverbos.install, debian/v4ldriver.1 (added),
	  debian/klatin.1 (added), debian/kwordquiz.docs (added),
	  debian/kiten.override, debian/keduca.1, debian/ktouch.1,
	  debian/kturtle.xpm (added), debian/klatin.README.Debian (added),
	  debian/kdeedu-doc-html.doc-base.kbruch, debian/lx200generic.1,
	  debian/ktouch.install, debian/patches/kstars.diff (removed),
	  debian/kalzium.1, debian/kdeedu-doc-html.install,
	  debian/klatin.manpages (added), debian/spotlight2kvtml.1,
	  debian/lx200gps.1, debian/kdeedu-doc-html.doc-base.flashkard
	  (removed), debian/kturtle.docs (added), debian/khangman.1,
	  debian/kbruch.install, debian/flashkard.docs (removed),
	  debian/kpercentage.1, debian/changelog, debian/kvoctrain.docs,
	  debian/kturtle.1 (added), debian/kwordquiz.manpages (added),
	  debian/test_extdatepicker.1 (added),
	  debian/kdeedu-doc-html.doc-base.kmessedwords, debian/klettres.1,
	  debian/test_extdate.1 (added), debian/source.lintian-overrides,
	  debian/v4lphilips.1 (added), debian/kwordquiz.1 (added),
	  debian/klettres.install: Sync with KDE_3_3_BRANCH.

2005-03-10 21:14 +0000 [r396466]  benb

	* debian/kwordquiz.install, debian/libkiten1.shlibs (added),
	  debian/kmplot.install, debian/kstars.install, debian/shlibs.local
	  (added), debian/kdeedu-data.install, debian/kig.install,
	  debian/kiten.install, debian/kmessedwords.install,
	  debian/control, debian/libkiten-dev.install (added),
	  debian/kbruch.install, debian/libkiten1.install (added),
	  debian/changelog, debian/klatin.install, debian/khangman.install,
	  debian/kiten.links (removed), debian/rules,
	  debian/libkdeedu1.shlibs, debian/keduca.install,
	  debian/libkiten1.links (added), debian/ktouch.install,
	  debian/klettres.install: Updated build-deps, install files and
	  shlibs files. Added new binary packages libkiten1 and
	  libkiten-dev.

2005-03-10 21:56 +0000 [r396491]  benb

	* debian/khangman.xpm, debian/kmplot.menu, debian/keduca.xpm:
	  Update menu files and pixmaps.

2005-03-10 22:15 +0000 [r396499]  benb

	* debian/kbruch.xpm: Oops, missed one.

2005-03-11 03:27 +0000 [r396558]  annma

	* klettres/klettres/klettres.cpp,
	  klettres/klettres/data/langs/es.txt (added),
	  klettres/klettres/data/sounds.xml,
	  klettres/klettres/data/langs/Makefile.am: add spanish support

2005-03-11 22:42 +0000 [r396833]  benb

	* debian/kvoctrain.menu, debian/kpercentage.override (removed),
	  debian/kturtle.override (removed), debian/kmplot.menu,
	  debian/kstars.menu, debian/ktouch.override (removed),
	  debian/kig.menu, debian/kiten.menu, debian/kmessedwords.menu,
	  debian/kvoctrain.override (removed), debian/kmplot.override
	  (removed), debian/kstars.override (removed), debian/klatin.menu,
	  debian/kig.override (removed), debian/kiten.override (removed),
	  debian/kmessedwords.override (removed), debian/keduca.menu,
	  debian/klettres.menu, debian/klatin.override (removed),
	  debian/kwordquiz.menu, debian/keduca.override (removed),
	  debian/klettres.override (removed), debian/kwordquiz.override
	  (removed), debian/kverbos.menu, debian/kalzium.menu,
	  debian/kbruch.menu, debian/khangman.menu, debian/changelog,
	  debian/kverbos.override (removed), debian/rules,
	  debian/kalzium.override (removed), debian/kpercentage.menu,
	  debian/kbruch.override (removed), debian/kturtle.menu,
	  debian/ktouch.menu, debian/khangman.override (removed): Remove
	  obsolete kderemove tags from menu files.

2005-03-14 22:23 +0000 [r397656]  benb

	* debian/copyright: Finished license audit for kdeedu 3.4. Updated
	  licensing information for applications, and also added full text
	  of the GFDL for docs.

2005-03-15 13:43 +0000 [r397807]  benb

	* debian/copyright, debian/kmessedwords.1, debian/kturtle.1,
	  debian/keduca.1, debian/spotlight2kvtml.1, debian/kvoctrain.1,
	  debian/klatin.1, debian/keducabuilder.1: Finished author audit
	  for kdeedu 3.4.

2005-03-15 19:18 +0000 [r397876]  annma

	* klettres/klettres/data/sounds.xml: add Romanized Hindi support

2005-03-15 19:46 +0000 [r397882]  annma

	* klettres/klettres/klettres.cpp, klettres/klettres/main.cpp:
	  support for new languages added in KNewStuff obliged to add a few
	  strings due to KNewStuff

2005-03-15 21:24 +0000 [r397903]  annma

	* klettres/klettres/main.cpp: forgot English sounds contributor

2005-03-15 21:34 +0000 [r397905]  benb

	* debian/kdeedu-doc-html.doc-base.klatin, debian/kstars-data.docs,
	  debian/kdeedu-doc-html.doc-base.kturtle,
	  debian/kdeedu-doc-html.doc-base.kalzium, debian/kstars.docs,
	  debian/kturtle.docs: Update doc-base files and miscellaneous
	  installed documentation files.

2005-03-15 22:34 +0000 [r397929]  benb

	* debian/fliccd.1, debian/kstars.1, debian/lx200classic.1,
	  debian/celestrongps.1, debian/lx200autostar.1,
	  debian/indiserver.1, debian/lx200_16.1, debian/kalzium.1,
	  debian/v4lphilips.1, debian/kwordquiz.1, debian/lx200gps.1,
	  debian/v4ldriver.1, debian/lx200generic.1, debian/temma.1
	  (added): Round one of manpage updates for kdeedu 3.4.

2005-03-16 21:50 +0000 [r398202]  benb

	* debian/kmplot.1, debian/kmessedwords.1, debian/kalzium.1,
	  debian/klettres.1, debian/control: Update descriptions of
	  applications.

2005-03-16 22:52 +0000 [r398226]  benb

	* debian/kstars.manpages: Oops.

2005-03-17 12:49 +0000 [r398354]  benb

	* debian/control: Add missing replaces, thanks MadCoder.

2005-03-22 21:18 +0000 [r399821]  harris

	* kstars/kstars/tools/planetviewer.cpp: fixed. BUGS:102210

2005-03-23 21:56 +0000 [r400105]  annma

	* klettres/klettres/data/sounds.xml: fix #102320 BUGS:102320

2005-03-25 23:27 +0000 [r400658]  benb

	* debian/changelog: Note what we did in 3_3_BRANCH.

2005-03-28 14:23 +0000 [r401276]  fedemar

	* kmplot/kmplot/coordsconfigdialog.cpp: Backport from HEAD: * Bug
	  102667: Infinite loop when max=min for axis. *
	  CoordsConfigDialog::slotApply() should call
	  KConfigDialog::slotApply().

2005-04-14 16:12 +0000 [r405558]  pino

	* kig/misc/lists.cc, kig/objects/object_imp_factory.cc,
	  kig/objects/object_imp_factory.h, kig/misc/object_hierarchy.cc,
	  kig/filters/native-filter.cc, kig/misc/object_hierarchy.h:
	  Backport fix for bug 100292: KIG crashes with sine-curve.kig due
	  to an assert failing. BUG: 100292

2005-04-15 18:17 +0000 [r405774]  pino

	* kig/misc/lists.cc, kig/objects/object_imp_factory.cc,
	  kig/objects/object_imp_factory.h, kig/misc/object_hierarchy.cc,
	  kig/misc/object_hierarchy.h: Backport: Better handling of files
	  with an object with type="foo" (with foo as an invalid type).

2005-04-15 18:23 +0000 [r405775]  pino

	* kig/kig/kig_part.cpp: Correct size for some menu item icons in
	  the BRANCH, too.

2005-04-16 08:55 +0000 [r405853]  pino

	* kig/kig/kig_part.cpp: Fixing a small issue when saving a file
	  with no name set. The name wasn't empty, but it contained a temp
	  file. Luckly m_bTemp stores wheth er using a temp file. I hope
	  this could fix #98142. CCBUG: 98142 David, could this solve your
	  problem?

2005-04-23 14:38 +0000 [r407355]  henrique

	* keduca/keduca/kcheckeduca.cpp, keduca/keduca/kradioeduca.cpp: *
	  Backport the fix for bug #97840

2005-05-01 21:32 +0000 [r409121]  binner

	* kstars/kstars/tools/scriptbuilder.cpp: fix disabled icons

2005-05-10 10:32 +0000 [r411876]  binner

	* kdeedu.lsm: update lsm for release

2005-05-10 17:04 +0000 [r412051]  cniehaus

	* kalzium/src/main.cpp: version++

2005-05-10 18:18 +0000 [r412086]  harris

	* kstars/kstars/main.cpp: incrementing kstars version number from
	  "1.1" to "1.1.1" for upcoming kde-3.4.1 release

2005-05-11 12:12 +0000 [r412317]  annma

	* khangman/khangman/khangmanview.cpp,
	  khangman/khangman/khangman.cpp: fix empty line in txt file that
	  generated an empty special char fix German first capital letter
	  remove debug info

2005-05-11 12:25 +0000 [r412326]  annma

	* klettres/klettres/klettres.cpp: fix empty line in txt file that
	  generated an empty special char

2005-05-11 20:22 +0000 [r412489]  fedemar

	* kmplot/kmplot/View.cpp: Print header table correctly when
	  automatic scaling is chosen.

2005-05-15 05:50 +0000 [r414017]  benb

	* debian/changelog, debian/rules: New 3.4 upload to experimental.
	  Merge changes from 3.3.x, disable CVS rules.

2005-05-15 11:00 +0000 [r414113]  cniehaus

	* kalzium/src/kalziumrc: First part of 104990: Fix the orbital data
	  (2 dataset corrects, rest added)

2005-05-15 11:26 +0000 [r414117]  cniehaus

	* kalzium/src/orbitswidget.cpp, kalzium/src/orbitswidget.h: This
	  was not really a bug: The list of hulls was not complete.
	  Solution: Entering the missing data BUG:104990

2005-05-19 15:01 +0000 [r415785]  pino

	* kig/objects/editanglesizebase.ui: Backport: small fix.

2005-05-20 11:43 +0000 [r416046]  cniehaus

	* kalzium/src/element.cpp: I displayed the wrong unit (the numbers
	  where correct) Allready fixed in HEAD/trunk BUG:103296

2005-05-20 11:48 +0000 [r416047]  cniehaus

	* kalzium/src/infodialog_small_impl.cpp: I forgot to use the
	  correct QString, the .utf() was missing (indirectly) BUG:101741

2005-05-21 19:50 +0000 [r416524]  pvicente

	* kstars/kstars/tools/modcalcplanets.cpp: Fixes part of bug 105995.
	  The interactive mode works fine now. Need more time to correct
	  the whole bug CCBUG: 105995 CCMAIL: kstars-devel@kde.org

