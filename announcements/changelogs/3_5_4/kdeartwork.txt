2006-05-26 20:40 +0000 [r545117]  tyrerj

	* branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/kde
	  (removed), branches/KDE/3.5/kdeartwork/IconThemes/Makefile.am,
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/actions/kde.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/kde.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/actions/kde
	  (removed): BUG: 128030 Modify "Makefile.am", remove "kde" dirs
	  from KDEClassic, add "kde.png" files.

2006-06-01 10:56 +0000 [r547222]  lunakl

	* branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/index.theme:
	  Revert r525838 - automatic handling of missing Inherits=
	  apparently doesn't work, and this is the stable branch.

2006-07-02 13:40 +0000 [r557137]  goossens

	* branches/KDE/3.5/kdeartwork/emoticons/KMess/emoticons.xml: make
	  (h) emoticon work too

2006-07-23 14:01 +0000 [r565465]  coolo

	* branches/KDE/3.5/kdeartwork/kdeartwork.lsm: preparing KDE 3.5.4

