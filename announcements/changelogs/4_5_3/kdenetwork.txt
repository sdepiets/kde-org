------------------------------------------------------------------------
r1181623 | poboiko | 2010-10-02 03:22:50 +1300 (Sat, 02 Oct 2010) | 4 lines

Backporting commit 1181622
Add null pointer check
CCBUG: 250586

------------------------------------------------------------------------
r1183602 | scripty | 2010-10-08 15:57:04 +1300 (Fri, 08 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1183612 | murrant | 2010-10-08 18:19:16 +1300 (Fri, 08 Oct 2010) | 5 lines

Backport r1183611 by murrant from trunk to the 4.5 branch:

Set focus to the input line when we the new connection page is activated.


------------------------------------------------------------------------
r1184251 | murrant | 2010-10-10 04:43:49 +1300 (Sun, 10 Oct 2010) | 2 lines

Select all text so it is easy to delete it if you want to go somewhere else.

------------------------------------------------------------------------
r1186718 | uwolfer | 2010-10-17 22:50:07 +1300 (Sun, 17 Oct 2010) | 4 lines

* Merging in KRDC VNC fixes which fix some bugs with wrong colors.
Fixes by Wouter Van Meir.

BUG:247149
------------------------------------------------------------------------
r1186911 | mfuchs | 2010-10-18 10:50:03 +1300 (Mon, 18 Oct 2010) | 3 lines

Backport r1174229
Removes files asynchronoulsy in DataSourceFactory to speed up deleting of unfinished transfers.
Do not stop transfers that are about to be removed, as stoppign involves the scheduler and that can massively slow down deleting many running transfers.
------------------------------------------------------------------------
r1186912 | mfuchs | 2010-10-18 10:50:05 +1300 (Mon, 18 Oct 2010) | 3 lines

Backport r1170561
Removes qobject cast that is not needed.
Does not use KGet::addTransferView, instead sets the model directly where the views are created.
------------------------------------------------------------------------
r1186913 | mfuchs | 2010-10-18 10:50:10 +1300 (Mon, 18 Oct 2010) | 11 lines

Backport r1173966
Uses more of the new classes of kdelibs for Nepomuk operations:
*Nepomuk::KFileMetaDataWidget for context menu of transfers, fixes setting Tags
*Nepomuk::TagWidget for setting tags of groups, these are stored as Nepomuk::Tag now, fixes setting Tags
*MassUpdateJob for setting properties and tags

Adds NepomukController for easy access of useful operations like adding Tags.
Adds an own thread for removing Nepomuk::Resources (in NepomukController) if the files they were associated with do not exist anymore, it speeds up removing Transfers in such cases.
Adds virtual QList<KUrl> Transfer::files() const, that returns all files of a transfer.

Removes btnepomukhandler, metanepomukhandler and all the custom widgets used for Nepomuk stuff.
------------------------------------------------------------------------
r1188782 | scripty | 2010-10-23 16:11:54 +1300 (Sat, 23 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1190559 | scripty | 2010-10-28 15:50:02 +1300 (Thu, 28 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
