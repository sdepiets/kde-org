------------------------------------------------------------------------
r1030628 | coates | 2009-10-02 19:50:39 +0000 (Fri, 02 Oct 2009) | 6 lines

Fixed a bug where the title bar didn't relfect the new game number when changing Klondike or Spider options.
    
Previously, when changing the options in Klondike or Spider, a new random game was opened, but the game number shown in the titlebar wasn't updated. Getting this to work correctly would be tricky, so I took the lazy route and made it just reload the same game number. No need to update the titlebar!

Backport of 1030625.

------------------------------------------------------------------------
r1032979 | scripty | 2009-10-09 03:12:30 +0000 (Fri, 09 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1034574 | ianw | 2009-10-13 06:59:04 +0000 (Tue, 13 Oct 2009) | 1 line

Workaround for loss of Esc shortcut after hovering over menu.  Provided by Christoph Feck.
------------------------------------------------------------------------
r1035861 | scripty | 2009-10-16 03:16:29 +0000 (Fri, 16 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1037762 | aacid | 2009-10-19 22:32:09 +0000 (Mon, 19 Oct 2009) | 4 lines

Backport aacid * r1037761 trunk/KDE/kdegames/kpat/main.cpp 
 Do not create a KComponentData to get translations, just a KLocale
 Fixes layouting problem with arabic

------------------------------------------------------------------------
r1039783 | coates | 2009-10-24 15:38:03 +0000 (Sat, 24 Oct 2009) | 2 lines

Fixed the default card back, at Sean's request. Has already been fixed in trunk.

------------------------------------------------------------------------
r1040314 | scripty | 2009-10-26 04:17:00 +0000 (Mon, 26 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1042072 | scripty | 2009-10-29 04:20:12 +0000 (Thu, 29 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
