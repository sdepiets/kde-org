<?php
  $page_title = "Anuncio de lanzamiento de KDE 4.1";
  $site_root = "../";
  include "header.inc";
?>

También disponible en:
<?php
  $release = '4.1';
  include "../announce-i18n-bar.inc";
?>

<!-- Translated by Sergio Rodriguez -->

<h3 align="center">
La Comunidad de KDE anuncia el lanzamiento de KDE 4.1.0
</h3>

<p align="justify">
  <strong>
KDE lanza su escritorio y aplicaciones mejorados dedicados a Uwe Thiem
  </strong>
</p>

<p align="justify">
29 de julio de 2008.
La <a href="http://www.kde.org/">Comunidad de KDE</a> ha lanzado hoy KDE 4.1.0. Esta
versión es la segunda de la serie KDE 4 que amplía funcionalidades, incorporando
nuevas aplicaciones y características nuevas sobre los pilares de KDE4. KDE 4.1
es la primera versión de KDE4 que contiene la <i>suite</i> de gestión de
información personal KDE-PIM, con su cliente de correo electrónico KMail, la agenda
KOrganizer, el lector de fuentes RSS Akregator, el lector de grupos de noticias
KNode y muchos más componentes integrados en Kontact. Además, el nuevo interfaz de
escritorio Plasma, presentado en KDE 4.0, ha madurado hasta el punto de poder
reemplazar el interfaz de KDE 3 para los usuarios más ocasionales. Como ocurrió
con nuestra versión anterior, se ha dedicado mucho tiempo a mejorar el <i>framework</i>
y las capas inferiores sobre las que KDE está construido.
<br />
Dirk M&uuml;ller, uno de los responsables de lanzamiento de KDE, aporta cifras:
<em>"Ha habido 20803 <i>commits</i> desde KDE 4.0 hasta KDE 4.1, así como 15432
aportaciones de traducción. Casi 35000 <i>commits</i> se han hecho en ramas de
trabajo, siendo algunos de ellos fusionados en KDE 4.1, de modo que esos ni
siquiera se han contado."</em>. M&uuml;ller también nos cuenta que el equipo de
administración de sistemas de KDE ha creado 166 nuevas cuentas para desarrolladores
en el servidor SVN de KDE.
<?php
    screenshot("desktop_thumb.png", "desktop.png", "center",
"El escritorio KDE 4.1", "405");
?>

</p>
<p><strong>Las mejoras clave de KDE 4.1 son:</strong></p>
<ul>
    <li>Vuelve la <i>suite</i> KDE-PIM</li>
    <li>Plasma madura</li>
    <li>Muchas aplicaciones nuevas y mejoras en ellas y en los <i>frameworks</i></li>
</ul>

<h3>
  <a name="changes">In memoriam: Uwe Thiem</a>
</h3>
<p align="justify">
La Comunidad de KDE dedica esta versión a Uwe Thiem, colaborador de KDE desde hace
mucho tiempo que falleció recientemente tras un súbito fallo renal. La muerte de
Uwe llegó de forma totalmente inesperada y conmocionó a sus compañeros colaboradores.
Uwe ha colaborado con KDE (casi literalmente hasta los últimos días de su vida) no sólo
en la programación. Uwe también jugó un papel importante educando a los usuarios de África
acerca de el Software Libre. Con la repentina muerte de Uwe, KDE ha perdido una
inestimable parte de su comunidad y un amigo. Nuestro pésame para su familia
y aquéllos que dejó atrás.
</p>


<h3>
  <a name="changes">Pasado, presente y futuro</a>
</h3>
<p align="justify">
Mientras que KDE 4.1 se centra en ser la primera versión usable para los que quieran
emplear KDE4 pronto, algunas funcionalidades a las que se está acostumbrado en KDE 3.5
no están implementadas todavía. El equipo de KDE está trabajando en ello y luchando
por tenerlas disponibles en una de las siguientes versiones. Mientras que no hay
garantía de que se implementen todas y cada una de las funcionalidades de KDE 3.5,
KDE 4.1 ya proporciona un entorno potente y rico en funcionalidad.<br />
Hay que advertir que algunas opciones de la interfaz de usuario se han movido a un
lugar dentro del contexto de los datos que manipulan, así que hay que asegurarse de
mirar con detenimiento antes de informar de algo que falta.</p>
<p>
KDE 4.1 es un enorme paso adelante en la serie KDE4 y esperemos que marque el ritmo
de futuros desarrollos. KDE 4.2 se espera para enero de 2009.
</p>


<?php
    screenshot("kdepim-screenie_thumb.png", "kdepim-screenie.png", "center",
"KDE PIM ha vuelto", "305");
?>

<h3>
  <a name="changes">Mejoras</a>
</h3>
<p align="justify">
Mientras se estabilizan los nuevos <i>frameworks</i> de KDE 4.1, se ha dirigido más
énfasis hacia las partes visibles al usuario. Siga leyendo para conocer una lista de
mejoras en KDE 4.1. Se puede encontrar información más completa en la página de los
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">objetivos de la versión
KDE 4.1</a> y en el más detallado <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">
plan de funcionalidades de KDE 4.1</a>
</p>

<h4>
  Para los usuarios
</h4>

<ul >
    <li><p align="justify">
		<strong>KDE-PIM</strong> vuelve con la versión 4.1, conteniendo las aplicaciones
		necesarias para la gestión de su información personal y comunicación. KMail como
		cliente de correo electrónico, KOrganizer como agenda, Akregator como lector
		de orígenes RSS y otros están ahora disponibles de nuevo con aspecto KDE 4.1.
		</p>
    </li>
    <li><p align="justify">
        <strong>Dragon Player</strong>, un reproductor de vídeo fácil de usar, se incorpora al escenario.
		</p>
    </li>
    <li><p align="justify">
        <strong>Okteta</strong> es el nuevo editor hexadecimal integrado y con gran funcionalidad.</p>
    </li>
    <li><p align="justify">
        <strong>Step</strong>, el emulador de física, hace el aprendizaje de física fácil y divertido.</p>
    </li>
    <li><p align="justify">
        <strong>KSystemLog</strong>, le ayuda a rastrear qué ocurre en su sistema.</p>
    </li>
    <li><p align="justify">
        <strong>Nuevos juegos</strong> como KDiamond (un clon de bejeweled), Kollision, KBreakOut
        y Kubrick hacen irresistible el tomar un descanso en tu trabajo.</p>
    </li>
    <li><p align="justify">
        <strong>Lokalize</strong> ayuda a los traductores a hacer que KDE4 esté disponible en su idioma
        (si no está entre los más de 50 idiomas que KDE4 ya incorpora).</p>
    </li>
    <li><p align="justify">
        <strong>KSCD</strong>, su reproductor de CD de escritorio ha sido resucitado.</p>
    </li>
</ul>

<p align="justify">
Se han recopilado respuestas a las preguntas más frecuentes en el
<a href="http://www.kde.org/users/faq.php">FAQ del usuario de KDE4</a>,
que también es una buena lectura si quiere aprender más acerca de KDE4.
</p>

<p align="justify">
<?php
    screenshot("dolphin-screenie_thumb.png", "dolphin-screenie.png", "center",
"El nuevo mecanismo de selección de Dolphin", "483");
?>
</p>

<ul>
    <li><p align="justify">
		<strong>Dolphin</strong>, el gestor de archivos de KDE, tiene una nueva vista de árbol
		en la vista principal, así como nuevo soporte para pestañas. Una nueva e innovadora
		selección con una sola pulsación permite una experiencia del usuario más consistente.
		Las acciones contextuales de "copiar a" y "mover a" lo hacen más fácilmente accesible.
		Por supuesto, Konqueror también está disponible como alternativa a Dolphin, tomando
		ventaja además de la mayoría de las anteriores funcionalidades.
		[<a href="#screenshots-dolphin">Capturas de pantalla de Dolphin</a>]</p>

    </li>
    <li><p align="justify">
		<strong>Konqueror</strong>, el navegador Web de KDE, ahora soporta la reapertura de pestañas
		y ventanas ya cerradas. También se incorpora desplazamiento suave por las páginas web.
		</p>
    </li>
    <li><p align="justify">
		<strong>Gwenview</strong>, el visor de imágenes de KDE, obtiene una nueva vista a pantalla
		completa, una barra de miniaturas para acceder fácilmente a otras fotos, un sistema inteligente
		para deshacer acciones y soporte para calificar imágenes.
		[<a href="#screenshots-gwenview">Capturas de pantalla de Gwenview</a>]</p>
    </li>
    <li><p align="justify">
		<strong>KRDC</strong>, el cliente de escritorio remoto de KDE, ahora detecta escritorios
		remotos en la red local automáticamente usando el protocolo ZeroConf.</p>
    </li>

    <li><p align="justify">
        <strong>Marble</strong>, el globo de escritorio de KDE, ahora se integra con <a
        href="http://www.openstreetmap.org/">OpenStreetMap</a> de modo que puedas encontrar tu camino
        en cualquier lugar usando los Mapas Libres.
		[<a href="#screenshots-marble">Capturas de pantalla de Marble</a>]</p>
    </li>
    <li><p align="justify">
		<strong>KSysGuard</strong> ahora permite la monitorización de la salida de los
		procesos o aplicaciones en ejecución, de modo que nunca más haya que volver a ejecutar
		la aplicación desde una terminal cuando se quiera saber qué ocurre.
		</p>
    </li>
    <li><p align="justify">
		Las funcionalidades del gestor de ventanas por composición <strong>KWin</strong> se
		han estabilizado y ampliado. Se han añadido nuevos efectos como el pase de portadas
		o el famoso "ventanas ondeantes".
		[<a href="#screenshots-kwin">Capturas de pantalla de KWin</a>]
		</p>
    </li>
    <li><p align="justify">
		El panel de configuración de <strong>Plasma</strong> se ha ampliado. El nuevo gestor
		del panel hace fácil la personalización, proporcionando una respuesta visual directa.
		También se pueden añadir paneles y ponerlos en distintos bordes de la(s) pantalla(s).
		El nuevo applet <i>folderview</i> permite almacenar ficheros en su escritorio (de hecho
		proporciona una vista de un directorio del sistema). Se pueden poner cero, uno o más
		<i>folderviews</i> en el escritorio, proporcionando un acceso sencillo y flexible a los
		archivos con los que se trabajan.
		[<a href="#screenshots-plasma">Capturas de pantalla de Plasma</a>]
		</p>
    </li>
</ul>



<h4>
  Para desarrolladores
</h4>

<ul>
    <li><p align="justify">
		El <i>framework</i> de almacenamiento PIM <strong>Akonadi</strong> proporciona una manera eficiente
		de almacenar y recuperar datos de correos y contactos entre aplicaciones.
		<a href="http://es.wikipedia.org/wiki/Akonadi">Akonadi</a> permite buscar a través
		de los datos y notifica sus cambios a las aplicaciones que los usan.</p>
    </li>
    <li><p align="justify">
		Las aplicaciones de KDE se pueden escribir usando Python y Ruby. Estos <strong>bindings</strong>
		están <a href="http://techbase.kde.org/Development/Languages">considerados</a> estables, maduros
		y adecuados para los desarrolladores de aplicaciones.</p>
    </li>
    <li><p align="justify">
		<strong>Libksane</strong> proporciona acceso fácil a las aplicaciones de escaneado de imágenes,
		como la nueva Skanlite.
		</p>
    </li>
    <li><p align="justify">
        Nuevo sistema de <strong>emoticonos</strong> usado por KMail and Kopete.</p>
    </li>
    <li><p align="justify">
		Nuevos <i>backends</i> multimedia de <strong>Phonon</strong> para GStreamer, QuickTime
		y DirectShow9, mejorando el soporte multimedia de KDE en Windows y Mac OS.
    </li>

</ul>


<h3>
  Nuevas plataformas
</h3>

<ul>
    <li><p align="justify">
		El soporte para <a href="http://techbase.kde.org/Projects/KDE_on_Solaris"><strong>OpenSolaris</strong></a>
		en KDE está actualmente finalizándose. KDE funciona de forma general en OSOL,
		aunque quedan todavía algunos bugs importantes.
		</p>
    </li>
    <li><p align="justify">
		Los desarrolladores de <strong>Windows</strong> pueden <a href="http://windows.kde.org">descargar</a>
		versiones previas de aplicaciones KDE para su plataforma. Las bibliotecas están
		ya relativamente estabilizadas, aunque no todas las funcionalidades de kdelibs están disponibles
		en Windows todavía. Mientras que algunas aplicaciones ya corren bastante bien en Windows, otras puede que no.</p>
    </li>
    <li><p align="justify">
		<strong>Mac OS X</strong> es otra nueva plataforma en la que KDE está introduciéndose.
		<a href="http://mac.kde.org">KDE on Mac</a> no está todavía listo para producción.
		Mientras que el soporte multimedia a través de Phonon ya está disponible, la integración
		con el hardware y la búsqueda no está terminada todavía.
    </li>
</ul>


<a name="screenshots-dolphin"></a>
<h3>
  Capturas de pantalla
</h3>

<a name="screenshots-dolphin"></a>
<h4>Dolphin</h4>
<?php

    screenshot("dolphin-treeview_thumb.png", "dolphin-treeview.png", "center",
               "La nueva vista de árbol de Dolphin proporciona acceso más rápido a los directorios.
               Tenga en cuenta que está desactivada por defecto.", "448");

    screenshot("dolphin-tagging_thumb.png", "dolphin-tagging.png", "center",
               "Nepomuk proporciona etiquetado y calificaciones en KDE -- y por tanto en Dolphin.", "337");

    screenshot("dolphin-icons_thumb.png", "dolphin-icons.png", "center",
               "La vista previa de iconos y las barras de información proporcionan información visual global.", "390");

    screenshot("dolphin-filterbar_thumb.png", "dolphin-filterbar.png", "center",
               "Encuentra tus archivos más fácilmente con la barra de filtro.", "372");
?>

<a name="screenshots-gwenview"></a>
<h4>Gwenview</h4>
<?php

    screenshot("gwenview-browse_thumb.png", "gwenview-browse.png", "center",
               "Puede navegar por los directorios de imágenes con Gwenview.
				Las acciones realizadas al pasar el ratón por encima ponen las tareas comunes
				al alcance de sus manos", "440");

    screenshot("gwenview-open_thumb.png", "gwenview-open.png", "center",
               "Abrir archivos desde el disco duro es tan fácil gracias a la infraestructura de KDE", "439");

    screenshot("gwenview-thumbnailbar_thumb.png", "gwenview-thumbnailbar.png", "center",
               "La nueva barra de miniaturas permite cambiar entre imágenes fácilmente.
				También está disponible en modo de pantalla completa.", "684");

    screenshot("gwenview-sidebar_thumb.png", "gwenview-sidebar.png", "center",
               "La barra lateral de Gwenview proporciona acceso a información adicional
               y opciones de manipulación de imágenes.", "462");

?>

<a name="screenshots-marble"></a>
<h4>Marble</h4>

<?php

    screenshot("marble-globe_thumb.png", "marble-globe.png", "center",
               "Marble, el globo terráqueo de escritorio.", "427");

    screenshot("marble-osm_thumb.png", "marble-osm.png", "center",
               "La nueva integración de Marble con OpenStreetMap también proporciona información de transporte público.", "425");


?>
<a name="screenshots-kwin"></a>
<h4>KWin</h4>

<?php

    screenshot("kwin-desktopgrid_thumb.png", "kwin-desktopgrid.png", "center",
               "La parrilla de escritorio de KWin visualiza el concepto de los escritorios virtuales y
               hace más fácil recordar dónde dejó aquella ventana que está buscando.", "405");

    screenshot("kwin-coverswitch_thumb.png", "kwin-coverswitch.png", "center",
               "El pase de portadas hace realmente llamativo el cambio entre aplicaciones con Alt+Tab.
               Puede escogerlo en la configuración de los efectos de escritorio de KWin.", "405");

    screenshot("kwin-wobbly_thumb.png", "kwin-wobbly.png", "center",
               "KWin también tiene las obligatorias ventanas ondulantes (desactivadas por defecto).", "405");


?>

<a name="screenshots-plasma"></a>
<h4>Plasma</h4>
<?php

    screenshot("plasma-folderview_thumb.png", "plasma-folderview.png", "center",
               "El nuevo applet folderview permite mostrar el contenido de directorios
               arbitrarios en su escritorio. Suelte un directorio sobre su escritorio desbloqueado
               para crear un nuevo folderview. Un folderview no sólo puede mostrar directorios locales,
               sino que puede tratar con paths de la red.", "405");

    screenshot("panel-controller_thumb.png", "panel-controller.png", "center",
               "El nuevo gestor de paneles permite fácilmente redimensionar y reposicionar paneles.
               Puede también cambiar la posición de los applets arrastrándolos a su nueva posición.", "116");

    screenshot("krunner-screenie_thumb.png", "krunner-screenie.png", "center",
               "Con KRunner, puede arrancar aplicaciones, enviar correos directamente a sus amigos y
				acometer otras pequeñas tareas varias.", "238");

    screenshot("plasma-kickoff_thumb.png", "plasma-kickoff.png", "center",
               "Kickoff, el lanzador de aplicaciones de Plasma, ha recibido un lavado de cara.", "405");

    screenshot("switch-menu_thumb.png", "switch-menu_thumb.png", "center",
               "Puede escoger entre el lanzador de aplicaciones Kickoff o el clásico estilo de menús.", "344");
?>

<h4>
  Problemas conocidos
</h4>

<ul>
    <li><p align="justify">
	Los usuarios de tarjetas <strong>NVidia</strong> con el driver binario proporcionado
	por NVidia pueden sufrir de problemas de rendimiento en el cambio de ventanas y
	redimensionado. Hemos puesto al corriente a los ingenieros de NVidia de estos problemas.
	No obstante, no se ha lanzado un driver corregido todavía. Puede encontrar información
	acerca de mejorar el rendimiento gráfico en el <a href="http://techbase.kde.org/User:Lemma/GPU-Performance">Techbase</a>,
	aunque finalmente hemos de confiar en que NVidia corrija su driver.
	</p>
	</li>
</ul>




<!-- ==================================================================================== -->
<h4>
  Obténgalo, ejecútelo, pruébelo
</h4>
<p align="justify">
	Voluntarios de la Comunidad y proveedores de sistemas operativos Linux/UNIX han
	proporcionado amablemente paquetes binarios de KDE 4.1.0 para algunas distribuciones
	de Linux, Mac OS X y Windows. Compruebe el sistema de gestión de software de su
	sistema operativo.
</p>
<h4>
  Cómo compilar KDE 4.1.0
</h4>
<p align="justify">
  <a name="source_code"></a><em>Código fuente</em>.
  El código fuente al completo de KDE 4.1.0 puede ser <a
  href="http://www.kde.org/info/4.1.0.php">descargado libremente</a>.
Las instrucciones acerca de cómo compilar e instalar KDE 4.1.0
  están disponibles en la <a href="/info/4.1.0.php">Página de información de KDE 4.1.0</a>, o en la <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contactos de prensa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
