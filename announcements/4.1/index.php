<?php
  $page_title = "KDE 4.1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Also available in:
<?php
  $release = '4.1';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
KDE Community Announces The Release of KDE 4.1.0
</h3>

<p align="justify">
  <strong>
    KDE Releases Improved Desktop and Applications Dedicated to Uwe Thiem
  </strong>
</p>

<p align="justify">
July 29, 2008.
The <a href="http://www.kde.org/">KDE Community</a> today released KDE 4.1.0. This
release is the second feature release of the KDE 4 series, sporting new
applications and newly developed features on top of the Pillars of KDE4. KDE 4.1 is
the first KDE4 release to contain the Personal Information Management suite KDE-PIM
with its E-Mail client KMail, the planner KOrganizer, Akregator, the RSS feed
reader, KNode, the newsgroup reader and many more components integrated into the
Kontact shell. Furthermore, the new desktop shell Plasma, introduced in KDE 4.0,
has matured to the point where it can replace the KDE 3 shell for most casual
users. Like with our previous release much time has been devoted to
improving the framework and underlying libraries on which KDE is built.
<br />
Dirk M&uuml;ller, one of KDE's release managers gives numbers: <em>"There have
been 20803 commits made from KDE 4.0 to KDE 4.1 along with 15432
translation checkins. Almost 35000 commits have been done in work branches,
some of them also being merged into KDE 4.1, so those were not even counted."</em>
M&uuml;ller also tells us that KDE's sysadmin team has created 166 new
accounts for developers on KDE's SVN server.

<?php
    screenshot("desktop_thumb.png", "desktop.png", "center",
"The KDE 4.1 desktop", "405");
?>

</p>
<p>
<strong>The key improvements in KDE 4.1 are:</strong>
<ul>
    <li>The KDE-PIM suite is back</li>
    <li>Plasma matures</li>
    <li>Many new and improved applications and frameworks</li>
</ul>

</p>


<h3>
  <a name="changes">In memoriam: Uwe Thiem</a>
</h3>
<p align="justify">
The KDE community dedicates this release to Uwe Thiem, a long-time contributor to
KDE who passed away recently after a sudden kidney failure. The death of Uwe
came totally unexpected and as a shock to his fellow contributors. Uwe has,
quite literally until
the last days of his life contributed to KDE, not only in the form of programming. Uwe
also played an important role in educating users in Africa about Free Software. With
Uwe's sudden death, KDE has lost an invaluable part of its community and a
friend. Our thoughts are with his family and the ones he left behind.
</p>


<h3>
  <a name="changes">Past, present and future</a>
</h3>
<p align="justify">
While KDE 4.1 aims at being the first release suitable for early adopting users,
some features you are used to in KDE 3.5 are not implemented yet.
The KDE team is working on those and
strives to make them available in one of the next releases. While there is no guarantee
that every single feature from KDE 3.5 will be implemented, KDE 4.1 already provides
a powerful and feature-rich working environment.<br />
Note that some options in the UI have moved to a place in the context of the data they
manipulate, so make sure you have a closer look before you report anything missing in
action.<br />
KDE 4.1 is a huge step forward in the KDE4 series and hopefully sets the pace for
future development. KDE 4.2 can be expected in January 2009.
</p>


<?php
    screenshot("kdepim-screenie_thumb.png", "kdepim-screenie.png", "center",
"KDE PIM is back", "305");
?>

<h3>
  <a name="changes">Improvements</a>
</h3>
<p align="justify">
While stabilising the new frameworks in KDE 4.1, more emphasis has shifted towards
end user visible parts. Read on for a list of improvements in KDE 4.1. More complete
information can be found on the
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">KDE 4.1 Release Goals</a>
page and in the more verbose
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">4.1 Feature Plan</a>.
</p>

<h4>
  For users
</h4>
<p align="justify">

<ul>
    <li>
        <strong>KDE-PIM</strong> is back with 4.1, containing the applications
        necessary for your personal information and communication. KMail as
        mail client, KOrganizer as planning component, Akregator as RSS feed
        reader and others are now available again in KDE 4 look.
    </li>
    <li>
        <strong>Dragon Player</strong>, an easy to use video player enters the stage
    </li>
    <li>
        <strong>Okteta</strong> is the new well-integrated and feature-rich hexeditor
    </li>
    <li>
        <strong>Step</strong>, the physics emulator makes learning physics fun and easy
    </li>
    <li>
        <strong>KSystemLog</strong>, helps you keep track of what is going on in your system
    </li>
    <li>
        <strong>New games</strong> such as KDiamond (a bejeweled clone), Kollision, KBreakOut
        and Kubrick make taking a break from your work irresistible
    </li>
    <li>
        <strong>Lokalize</strong>, helps translators to make KDE4 available in your language
        (if it's not among the 50-odd languages KDE4 already supports)
    </li>
    <li>
        <strong>KSCD</strong>, your desktop CD player has been resurrected
    </li>
</ul>

Answers to common questions have been collected in the
<a href="http://www.kde.org/users/faq.php">KDE4 End User FAQ</a>. The <a href="http://www.kde.org/users/glossary.php">Glossary</a>
is also a good read if you want to learn more about KDE4.
</p>

<p align="justify">
<?php
    screenshot("dolphin-screenie_thumb.png", "dolphin-screenie.png", "center",
"Dolphin's new selection mechanism", "483");
?>

<ul>
    <li>
        <strong>Dolphin</strong>, KDE's filemanager has a new treeview in the main
        view, also new is the support for tabs. A new and innovative single-click
        selection allows for a more consistent user experience, and copy-to and move-to
        context actions make those actions easier accessible. Of course Konqueror is also
        available as alternative to Dolphin, taking advantage of most of the above
        features as well. [<a href="#screenshots-dolphin">Dolphin screenshots</a>]
    </li>
    <li>
        <strong>Konqueror</strong>, KDE's webbrowser now has support for re-opening
        already closed windows and tabs, it also scrolls smoothly through webpages.
    </li>
    <li>
        <strong>Gwenview</strong>, KDE's image viewer has gotten a new fullscreen view, a
        thumbnail bar for easy access to other photos, a smart Undo system and support for
        rating images. [<a href="#screenshots-gwenview">Gwenview screenshots</a>]
    </li>
    <li>
        <strong>KRDC</strong>, KDE's remote desktop client now detects remote desktops on
        the local network automatically using the ZeroConf protocol.
    </li>

    <li>
        <strong>Marble</strong>, KDE's desktop globe now integrates with <a
        href="http://www.openstreetmap.org/">OpenStreetMap</a> so you can find your way
        everywhere using the Free Maps. [<a href="#screenshots-marble">Marble screenshots</a>]
    </li>
    <li>
        <strong>KSysGuard</strong>, now supports monitoring process output or running
        applications so there is no need to restart your apps from a terminal anymore
        when you want to know what's going on.
    </li>
    <li>
        <strong>KWin</strong>'s compositing window manager features have been more
        stabilized and extended. New effects such as the Coverswitch window switcher
        and the famous "wobbly windows" have been added. [<a href="#screenshots-kwin">KWin
        screenshots</a>]
    </li>
    <li>
        <strong>Plasma</strong>'s panel configuration has been extended. The new panel controller
        makes it easy to customize your panel providing direct visual feedback. You can also
        add panels and put them on different edges of your screen(s). The new folderview applet
        allows you to store files on your desktop (in fact it provides a view of a directory on
        your system). You can put zero, one or more folderviews onto your desktop, providing for
        easy and flexible access to the files you are working with.
        [<a href="#screenshots-plasma">Plasma screenshots</a>]
    </li>
</ul>
</p>


<h4>
  For developers
</h4>
<p align="justify">

<ul>
    <li>
        The <strong>Akonadi</strong> PIM storage framework provides an efficient way of
        storing and retrieving email and contact data across applications. <a href="http://en.wikipedia.org/wiki/Akonadi">Akonadi</a> supports searching through
        data and notifies applications using it of changes.
    </li>
    <li>
        KDE applications can be written using Python and Ruby. These <strong>language bindings</strong> are
        <a href="http://techbase.kde.org/Development/Languages">considered</a> stable, mature and suitable
        for application developers.
    </li>
    <li>
        <strong>Libksane</strong> provides easy access to image scanning applications such as the new
        scanning application Skanlite.
    </li>
    <li>
        A shared <strong>emoticons</strong> system which is used by KMail and Kopete.
    </li>
    <li>
        New <strong>Phonon</strong> multimedia backends for GStreamer, QuickTime and DirectShow9,
        improving KDE's multimedia support on Windows and Mac OS.
    </li>

</ul>
</p>

<h3>
  New platforms
</h3>
<p align="justify">
<ul>
    <li>
        <a href="http://techbase.kde.org/Projects/KDE_on_Solaris"><strong>OpenSolaris</strong></a>
        support in KDE is currently straightened out. KDE mostly works
        on OSOL, although there are some showstopper bugs left.
    </li>
    <li>
        <strong>Windows</strong> developers are able to <a href="http://windows.kde.org">download</a>
        previews of KDE applications for their platform. The libraries are relatively stable
        already, although not all features of kdelibs are available on Windows yet. Some
        applications already run quite well on Windows, others might not.
    </li>
    <li>
        <strong>Mac OSX</strong> is another new platform KDE is entering.
        <a href="http://mac.kde.org">KDE on Mac</a> is not yet ready for production use. While
        Multimedia support through Phonon is already available, hardware and search integration is
        not finished yet.
    </li>
</ul>
</p>


<a name="screenshots-dolphin"></a>
<h3>
  Screenshots
</h3>
<p align="justify">

<a name="screenshots-dolphin"></a>
<h4>Dolphin</h4>
<?php

    screenshot("dolphin-treeview_thumb.png", "dolphin-treeview.png", "center",
               "Dolphin's new treeview gives you quicker access across directories.
               Note that it's disabled in the default setting.", "448");

    screenshot("dolphin-tagging_thumb.png", "dolphin-tagging.png", "center",
               "Nepomuk provides tagging and rating in KDE -- and thus in Dolphin.", "337");

    screenshot("dolphin-icons_thumb.png", "dolphin-icons.png", "center",
               "Icon preview and information bars provide visual feedback and overview.", "390");

    screenshot("dolphin-filterbar_thumb.png", "dolphin-filterbar.png", "center",
               "Find your files easier with the filter bar.", "372");
?>

<a name="screenshots-gwenview"></a>
<h4>Gwenview</h4>
<?php

    screenshot("gwenview-browse_thumb.png", "gwenview-browse.png", "center",
               "You can browse directories with images with Gwenview. Hover
               actions put common tasks at your fingertips.", "440");

    screenshot("gwenview-open_thumb.png", "gwenview-open.png", "center",
               "Opening files from your harddisk or the network is just as
               easy, thanks to KDE's infrastructure.", "439");

    screenshot("gwenview-thumbnailbar_thumb.png", "gwenview-thumbnailbar.png", "center",
               "The new thumbnail bar lets you switch between images easily. It
               is also available in full screen mode.", "684");

    screenshot("gwenview-sidebar_thumb.png", "gwenview-sidebar.png", "center",
               "Gwenview's sidebar provides access to additional information
               and image manipulation options.", "462");

?>

<a name="screenshots-marble"></a>
<h4>Marble</h4>

<?php

    screenshot("marble-globe_thumb.png", "marble-globe.png", "center",
               "The Marble desktop globe.", "427");

    screenshot("marble-osm_thumb.png", "marble-osm.png", "center",
               "Marble's new OpenStreetMap integration also features public
               transport information.", "425");


?>
<a name="screenshots-kwin"></a>
<h4>KWin</h4>

<?php

    screenshot("kwin-desktopgrid_thumb.png", "kwin-desktopgrid.png", "center",
               "KWin's desktopgrid visualizes the concept of virtual desktops and
               makes it easier to remember where you left that window you're looking for.", "405");

    screenshot("kwin-coverswitch_thumb.png", "kwin-coverswitch.png", "center",
               "The Coverswitcher makes switching applications with Alt+Tab a real eye-catcher.
               You can choose it in KWin's desktop effects settings.", "405");

    screenshot("kwin-wobbly_thumb.png", "kwin-wobbly.png", "center",
               "KWin now also has the mandatory wobbly windows (disabled by default).", "405");


?>

<a name="screenshots-plasma"></a>
<h4>Plasma</h4>
<?php

    screenshot("plasma-folderview_thumb.png", "plasma-folderview.png", "center",
               "The new folderview applet lets you display the content of arbitrary
               directories on your desktop. Drop a directory onto your unlocked
               desktop to create a new folderview. A folderview can not only display local
               directories, but can also cope with locations on the network.", "405");

    screenshot("panel-controller_thumb.png", "panel-controller.png", "center",
               "The new panel controller lets you easily resize and reposition panels.
               You can also change the position of applets on the panel by dragging them
               to their new position.", "116");

    screenshot("krunner-screenie_thumb.png", "krunner-screenie.png", "center",
               "With KRunner, you can start applications, directly email your friends
               and accomplish various other small tasks.", "238");

    screenshot("plasma-kickoff_thumb.png", "plasma-kickoff.png", "center",
               "Plasma's Kickoff application launcher has had a facelift.", "405");

    screenshot("switch-menu_thumb.png", "switch-menu_thumb.png", "center",
               "You can choose between the Kickoff application launcher and the classic
               menu style.", "344");
?>

</p>

<h4>
  Known issues
</h4>
<p align="justify">
<ul>
    <li>Users of <strong>NVidia</strong> cards with the binary driver provided
    by NVidia might suffer from performance problems in window switching and
    resizing. We've made the NVidia engineers aware of those problems. However, no
    fixed NVidia driver has been released yet. You can find information on
    how to improve graphics performance on
    <a href="http://techbase.kde.org/User:Lemma/GPU-Performance">Techbase</a>,
    although we ultimately have to rely on NVidia to fix their driver.</li>
</ul>

</p>
<h4>
  KDE 4.1.0 in the Press
</h4>
<p align="justify">
The release of KDE 4.1.0 has had a warm reception in the press. We've collected
an overview of articles and reviews, some also giving additional information you
might be looking for. Read more one the <a href="inthepress.php">"In the Press"</a>
page.
</p>

<!-- ==================================================================================== -->
<h4>
  Get it, run it, test it
</h4>
<p align="justify">
  Community volunteers and Linux/UNIX OS vendors have kindly provided binary packages
  of KDE 4.1.0 for some Linux distributions, and Mac OS X and Windows.
  Check your operating system's software management system.
</p>
<h4>
  Compiling KDE 4.1.0
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 4.1.0 may be <a
  href="http://www.kde.org/info/4.1.0.php">freely downloaded</a>.
Instructions on compiling and installing KDE 4.1.0
  are available from the <a href="/info/4.1.0.php">KDE 4.1.0 Info
  Page</a>, or on <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
