<?php

  $release = '4.8';
  $release_full = '4.8.0';

  $page_title = "KDE Applications 4.8 Offer Faster, More Scalable File Management";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<p>
KDE is happy to announce the release of new versions of many popular applications. From games to education and entertainment, these applications are more powerful. But they are still easy to use as they mature. Below are highlights from some of the updated applications released today.
</p>

<?php
centerThumbScreenshot("gwenview2.png", "KDE Applications 4.8");
?>

<p>
Enjoy new tricks from a faster and prettier <a href="http://dolphin.kde.org"><strong>Dolphin</strong></a>. The new display engine performs faster when dealing with big directories, slow disks and layout changes. File names no longer get shortened, and icon and text boundaries fit items better.
</p><p>
<?php
centerThumbScreenshot("gwenview.png", "Gwenview 4.8 comes with nicer transitions between images");
?>
<a href="http://www.kde.org/applications/graphics/gwenview/"><strong>Gwenview</strong></a> finetunes its usability and image viewing abilities. A handy, translucent navigation area appears when an image is zoomed-in so scrolling is easier. A fade effect is applied in image transitions. <a href="http://okular.kde.org/"><strong>Okular</strong></a> 4.8, KDE's document viewer brings <a href="http://nightcrawlerinshadow.wordpress.com/2011/08/20/advanced-text-selection-in-okular/">advanced text selection features</a>.
<?php
centerThumbScreenshot("kmail.png", "KMail has seen many performance and stability improvements");
?>

</p><p>
<a href="https://www.kde.org/applications/internet/kmail/"><strong>KMail</strong></a> has seen many bug fixes and performance improvements, following its substantial architectural changes in the previous release. It also has a variety of new frontend features.

</p><p>
<strong>Kate</strong> has many <a href="http://kate-editor.org/2011/12/21/kate-in-kde-4-8/">new features and improvements</a>: a new Search &amp; Replace plugin, useful indicators when lines are changed, and a new editor that can be configured to use document variables (modelines). There are improvements to the heavily reworked code folding, the Vi mode and the Kate/Kwrite handbooks. As for bugs, about 190 issues were resolved after heavy work cleaning up the bug database.

<?php
centerThumbScreenshot("kate.png", "Kate 4.8 shows changes you've made to files");
?>

</p><p>
<a href="http://edu.kde.org/cantor/"><strong>Cantor</strong></a> —KDE's frontend to popular mathematics packages—gains support for Qalculate and Scilab.

<a href="https://www.kde.org/applications/development/lokalize/"><strong>Lokalize</strong></a> —the KDE translation tool—has improved support for TBX (TermBase eXchange) glossaries and XLIFF (XML Localisation Interchange File Format) translation catalogs—two important industry standards. It also has a faster UI and 15 bug fixes.

</p><p>
<a href="http://edu.kde.org/marble/"><b>Marble</b></a> —the virtual globe and world atlas—now integrates with Plasma's KRunner. By allowing for coordinate and bookmark searches, Marble can be opened directly from the Plasma search bar. The new Elevation Profile shows the incline of routes, which can be edited interactively. Stargazers can view and track Earth satellites thanks to Marble participation in the European Space Agency (ESA) Summer of Code in Space. During Google Summer of Code, Marble gained support for display of .osm (OpenStreetMap) files in vector format. Owners of the Nokia N9/N950 are the first to receive the new mobile application Marble Touch. Further details can be found in the <a href="http://edu.kde.org/marble/current_1.3.php">visual changelog </a> at the Marble website.

<div align="center"><iframe width="560" height="315" src="http://www.youtube.com/embed/auxbMI4N6is" frameborder="0" allowfullscreen></iframe></div>

</p>
<h4>Installing KDE Applications</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Announced Today:</h2>

<?php

include("trailer-plasma.inc");
include("trailer-platform.inc");

include("footer.inc");
?>
