
<h3>
<a href="plasma.php">
KDE Plasma Workspaces 4.8 Gain Adaptive Power Management
</a>
</h3>

<p>
<a href="plasma.php">
<img src="images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.8" width="64" height="64" />
</a>

Highlights for Plasma Workspaces include <a href="http://philipp.knechtges.com/?p=10">Kwin optimizations</a>, the redesign of power management, and integration with Activities. The first QtQuick-based Plasma widgets have entered the default installation of Plasma Desktop, with more to follow in future releases. Read the complete <a href="plasma.php">Plasma Workspaces Announcement</a>.

<br /><br />
<br /><br />
</p>
