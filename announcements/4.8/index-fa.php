<?php

  $release = '4.8';
  $release_full = '4.8.0';
  $page_title = "محیطهای کاری پلاسمای کی‌دی‌ای، برنامه‌ها و سکوی ۴٫۸ تجربه‌ی کاربری را افزایش میدهند";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
<div style="direction: rtl;">
<p>همجنین، موجود در زبان‌های:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
تیم توسعه‌ی کی‌دی‌ای مفتخر است مجموعه‌ای از انتشارهای خود، شامل به‌روزرسانی‌های اساسی در محیط‌کاری پلاسمای کی‌دی‌ای، برنامه‌های کی‌دی‌ای و سکوی کی‌دی‌ای را اعلام کند. نسخه ۴٫۸ شامل تعداد بسیار زیادی قابلیت جدید و بهبود در پایداری و کارایی است.
</p>
<?php
  centerThumbScreenshot("plasma-desktop-4.8.png", "پلاسما و برنامه‌های ۴٫۸");
?>

<?php
include("trailer-plasma-fa.inc");
include("trailer-applications-fa.inc");
include("trailer-platform-fa.inc");

?>
برای جشن گرفتن انتشار ۴٫۸، اعضای اجتماعات کاربری <a href="http://community.kde.org/Promo/Events/Release_Parties/4.8">جشن‌های انتشار در نقاط مختلف دنیا</a> را برنامه‌ریزی کردند. به جشنی که به شما نزدیک‌تر است بپیوندید و مشارکت‌کنندگان و استفاده کنندگان باحال کی‌دی‌ای را ملاقات کنید.
<br><br>
شما میتوانید آنچه در سایت‌های اجتماعی اتفاق می‌افتد را در فید زنده‌ی کی‌دی‌ای دنبال کنید. این سایت فعالیتهای واقعی را در آیدنتیکا، توییتر، یوتیوب، فلیکر، پیکاساوب، وبلاگها و دیگر سایتهای شبکه‌های اجتماعی جمع میکنذ. این فید زنده را میتوانید در<a href="http://buzz.kde.org">buzz.kde.org</a> بیابید.


<h3>
از کی‌دی‌ای بگویید و پیام را منتشر کنید. به گفته‌هایتان برچسب «KDE» بزنید. 
</h3>
<p>
کی‌دی‌ای همه را ترغیب به پخش و گسترش پیام در شبکه‌های اجتماعی وب میکند.داستان‌های خود را در سایتهای خبری و کانال‌هایی از قبیل  delicious، digg، reddit، توییتر و آیدنتیکا ارسال کنید. تصاویر محیط کاری خودتان را در سرویسهایی مانند فیس‌بوک، فلیکر، ایپرنیتی و پیکاسا ارسال و آن‌ها را در گروههای مناسب به اشتراک بگذارید. از محیط کاری خود فیلم تهیه کرده (screencast) و آن‌ها را در یوتیوب، بلیپ‌تی‌وی، ویمئو و دیگر سایتها بارگذاری کنید. لطفاً کارهایی را که بارگذاری میکنید با برچسب (تگ) «KDE»  نمایش دهید تا پیدا کردن آن‌ها آسانتر باشد و همچنین تیم تبلیغات کی‌دی‌ای (KDE promo team) پوشش بهتری برای تحلیل انتشار نرم‌افزارهای کی‌دی‌ای ۴٫۸ داشته باشد.

<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.8/&amp;title=KDE%20releases%20version%204.8%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="http://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.8/" data-text="#KDE releases version 4.8 of Plasma Workspaces, Applications and Platform → https://www.kde.org/announcements/4.8/ #kde48" data-count="horizontal" data-via="kdecommunity">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.8/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.8%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.8/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde48"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde48"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde48"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde48"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>

<h3>
درباره این اعلان:
</h3><p>
این اعلان توسط محمدرضا میردامادی به فارسی ترجمه شده است.
</p>

<h4>از کی‌دی‌ای پشتیبانی کنید</h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="Join the Game" align="left"/> </a>
<a
href="http://jointhegame.kde.org/">سیستم جدید دستورالعمل عضو پشتیبانی کننده‌ی .KDE e.V اکنون باز است.</a> با €25 در هر ۳ماه میتوانید اطمینان حاصل کنید که تیم اجتماع بین‌المللی کی‌دی‌ای برای ساختن نرم‌افزار آزاد در کلاس جهانی، به بزرگ شدن خود ادامه میدهد.
<br clear="all" />
<p>&nbsp;</p>
<?php
  include("footer.inc");
?>
</div>
