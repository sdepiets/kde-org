<?php

  $release = '4.8';
  $release_full = '4.8.0';
  $page_title = "KDE Plasma Workspaces, Applications and Platform 4.8 Improve User Experience";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Also available in:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE is delighted to announce its latest set of releases, providing major updates to KDE Plasma Workspaces, KDE Applications, and the KDE Platform. Version 4.8 provides many new features, and improved stability and performance.
</p>
<?php
  centerThumbScreenshot("plasma-desktop-4.8.png", "Plasma and Applications 4.8");
?>

<?php
include("trailer-plasma.inc");
include("trailer-applications.inc");
include("trailer-platform.inc");

?>

To celebrate the 4.8 releases, community members have organized <a href="http://community.kde.org/Promo/Events/Release_Parties/4.8">release parties all over the world</a>. Come to a party nearby and meet your fellow KDE contributors and users.

<h3>
Spread the Word and See What Happens: Tag as "KDE"
</h3>
<p>
KDE encourages everybody to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, Vimeo and others. Please tag uploaded materials with "KDE" to make them easy to find, and so that the KDE promo team can analyze coverage for the 4.8 releases of KDE software.

<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.8/&amp;title=KDE%20releases%20version%204.8%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="http://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.8/" data-text="#KDE releases version 4.8 of Plasma Workspaces, Applications and Platform → https://www.kde.org/announcements/4.8/ #kde48" data-count="horizontal" data-via="kdecommunity">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.8/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.8%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.8/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde48"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde48"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde48"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde48"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>

<h3>
About these release announcements
</h3><p>
These release announcements were prepared by Stu Jarvis, Nikos Pantazis, Dennis Nienhüser, Sebastian Kügler, Carl Symons, Vivek Prakash, Eshat Cakar, other members of the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past six months.
</p>

<h4>Support KDE</h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="Join the Game" align="left"/> </a>
<p align="justify"> KDE e.V.'s new <a
href="http://jointhegame.kde.org/">Supporting Member programme</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.</p>
<br clear="all" />
<p>&nbsp;</p>
<?php
  include("footer.inc");
?>
