<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.58.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.58.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
May 13, 2019. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.58.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("[baloo_file] Wait for extractor process to start");?></li>
<li><?php i18n("[balooctl] Add command to show files failed to index (bug 406116)");?></li>
<li><?php i18n("Add QML to source code types");?></li>
<li><?php i18n("[balooctl] Capture the constant totalsize in the lambda");?></li>
<li><?php i18n("[balooctl] Switch multiline output to new helper");?></li>
<li><?php i18n("[balooctl] Use new helper in json output");?></li>
<li><?php i18n("[balooctl] Use new helper for simple format output");?></li>
<li><?php i18n("[balooctl] Factor out file index status collection from output");?></li>
<li><?php i18n("Keep empty Json metadata docs out of DocumentData DB");?></li>
<li><?php i18n("[balooshow] Allow referencing files by URL from hardlink");?></li>
<li><?php i18n("[balooshow] Suppress warning when URL refers to unindexed file");?></li>
<li><?php i18n("[MTimeDB] Allow timestamp newer than the newest document in range match");?></li>
<li><?php i18n("[MTimeDB] Use exact match when exact match is requested");?></li>
<li><?php i18n("[balooctl] Cleanup handling of different positional arguments");?></li>
<li><?php i18n("[balooctl] Extend options help text, improve error checking");?></li>
<li><?php i18n("[balooctl] Use more understandable names for size in status output");?></li>
<li><?php i18n("[balooctl] clear command: Remove bogus check for documentData, cleanup");?></li>
<li><?php i18n("[kio_search] Fix warning, add UDSEntry for \".\"  in listDir");?></li>
<li><?php i18n("Use hex notation for DocumentOperation flag enum");?></li>
<li><?php i18n("Calculate total DB size correctly");?></li>
<li><?php i18n("Postpone term parsing until needed, do not set both term and searchstring");?></li>
<li><?php i18n("Don't add default valued date filters to json");?></li>
<li><?php i18n("Use compact Json format when converting query URLs");?></li>
<li><?php i18n("[balooshow] Do not print a bogus warning for a non-indexed file");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add non-symbolic 16px versions of find-location and mark-location");?></li>
<li><?php i18n("Symlink preferences-system-windows-effect-flipswitch to preferences-system-tabbox");?></li>
<li><?php i18n("Add \"edit-delete-remove\" icon symlink and add 22px version of \"paint-none\" and \"edit-none\"");?></li>
<li><?php i18n("Use consistent default Kickoff user icon");?></li>
<li><?php i18n("Add an icon for Thunderbolt KCM");?></li>
<li><?php i18n("Sharpen Z's in system-suspend* icons");?></li>
<li><?php i18n("Improve \"widget-alternatives\" icon");?></li>
<li><?php i18n("Add go-up/down/next/previous-skip");?></li>
<li><?php i18n("Update KDE logo to be closer to original");?></li>
<li><?php i18n("Add alternatives icon");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Bug fix: find c++ stl using regex");?></li>
<li><?php i18n("Unconditionally enable -DQT_STRICT_ITERATORS, not just in debug mode");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("KTar: Protect against negative longlink sizes");?></li>
<li><?php i18n("Fix invalid memory write on malformed tar files");?></li>
<li><?php i18n("Fix memory leak when reading some tar files");?></li>
<li><?php i18n("Fix uninitialized memory use when reading malformed tar files");?></li>
<li><?php i18n("Fix stack-buffer-overflow read on malformed files");?></li>
<li><?php i18n("Fix null-dereference on malformed tar files");?></li>
<li><?php i18n("Install krcc.h header");?></li>
<li><?php i18n("Fix double delete on broken files");?></li>
<li><?php i18n("Disallow copy of KArchiveDirectoryPrivate and KArchivePrivate");?></li>
<li><?php i18n("Fix KArchive::findOrCreate running out of stack on VERY LONG paths");?></li>
<li><?php i18n("Introduce and use KArchiveDirectory::addEntryV2");?></li>
<li><?php i18n("removeEntry can fail so it's good to know if it did");?></li>
<li><?php i18n("KZip: fix Heap-use-after-free in broken files");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Force KAuth helpers to have UTF-8 support (bug 384294)");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Add support for KBookmarkOwner to communicate if it has tabs open");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Use size hints from the ApplicationItem itself");?></li>
<li><?php i18n("Fix Oxygen background gradient for QML modules");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Add Notify capability to KConfigXT");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Fix wrong \"Unable to find service type\" warnings");?></li>
<li><?php i18n("New class KOSRelease - a parser for os-release files");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("[KeySequenceItem] Make the clear button have the same height as shortcut button");?></li>
<li><?php i18n("Plotter: Scope GL Program to lifespan of scenegraph node (bug 403453)");?></li>
<li><?php i18n("KeySequenceHelperPrivate::updateShortcutDisplay: Don't show english text to the user");?></li>
<li><?php i18n("[ConfigModule] Pass initial properties in push()");?></li>
<li><?php i18n("Enable glGetGraphicsResetStatus support by default on Qt &gt;= 5.13 (bug 364766)");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Install .desktop file for kded5 (bug 387556)");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("[TagLibExtractor] Fix crash on invalid Speex files (bug 403902)");?></li>
<li><?php i18n("Fix exivextractor crash with malformed files (bug 405210)");?></li>
<li><?php i18n("Declare properties as meta type");?></li>
<li><?php i18n("Change properties attributes for consistency");?></li>
<li><?php i18n("Handle variant list in formatting functions");?></li>
<li><?php i18n("Fix for Windows' LARGE_INTEGER type");?></li>
<li><?php i18n("Fix (compilation) errors for Windows UserMetaData implementation");?></li>
<li><?php i18n("Add missing mimetype to taglib writer");?></li>
<li><?php i18n("[UserMetaData] Handle changes in attribute data size correctly");?></li>
<li><?php i18n("[UserMetaData] Untangle Windows, Linux/BSD/Mac and stub code");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Copy container in Component::cleanUp before iterating");?></li>
<li><?php i18n("Don't use qAsConst over a temporary variable (bug 406426)");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("holidays/plan2/holiday_zm_en-gb - add Zambia holidays");?></li>
<li><?php i18n("holidays/plan2/holiday_lv_lv - fix Midsummer's Day");?></li>
<li><?php i18n("holiday_mu_en - Holidays 2019 in Mauritius");?></li>
<li><?php i18n("holiday_th_en-gb - update for 2019 (bug 402277)");?></li>
<li><?php i18n("Update Japanese holidays");?></li>
<li><?php i18n("Add public holidays for Lower Saxony (Germany)");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("tga: don't try to read more than max_palette_size into palette");?></li>
<li><?php i18n("tga: memset dst if read fails");?></li>
<li><?php i18n("tga: memset the whole palette array, not only the palette_size");?></li>
<li><?php i18n("Initialize the unread bits of _starttab");?></li>
<li><?php i18n("xcf: Fix uninitialized memory use on broken documents");?></li>
<li><?php i18n("ras: Don't overread input on malformed files");?></li>
<li><?php i18n("xcf: layer is const in copy and merge, mark it as such");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("[FileWidget] Replace \"Filter:\" with \"File type:\" when saving with a limited list of mimetypes (bug 79903)");?></li>
<li><?php i18n("Newly created 'Link to Application' files have a generic icon");?></li>
<li><?php i18n("[Properties dialog] Use the string \"Free space\" instead of \"Disk usage\" (bug 406630)");?></li>
<li><?php i18n("Fill UDSEntry::UDS_CREATION_TIME under linux when glibc &gt;= 2.28");?></li>
<li><?php i18n("[KUrlNavigator] Fix URL navigation when exiting archive with krarc and Dolphin (bug 386448)");?></li>
<li><?php i18n("[KDynamicJobTracker] When kuiserver isn't available, also fall back to widget dialog (bug 379887)");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("[aboutpage] hide Authors header if there are no authors");?></li>
<li><?php i18n("Update qrc.in to match .qrc (missing ActionMenuItem)");?></li>
<li><?php i18n("Make sure we don't squeeze out the ActionButton (bug 406678)");?></li>
<li><?php i18n("Pages: export correct contentHeight/implicit sizes");?></li>
<li><?php i18n("[ColumnView] Also check for index in child filter..");?></li>
<li><?php i18n("[ColumnView] Don't let mouse back button go back beyond first page");?></li>
<li><?php i18n("header has immediately the proper size");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("[KUiServerJobTracker] Track kuiserver service life time and re-register jobs if needed");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Remove pixelated border (bug 391108)");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("[Notify by Portal] Support default action and priority hints");?></li>
<li><?php i18n("[KNotification] Add HighUrgency");?></li>
<li><?php i18n("[KNotifications] Update when flags, urls, or urgency change");?></li>
<li><?php i18n("Allow to set urgency for notifications");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Add missing properties in kpackage-generic.desktop");?></li>
<li><?php i18n("kpackagetool: read kpackage-generic.desktop from qrc");?></li>
<li><?php i18n("AppStream generation: make sure we look up for the package structure on packages that have metadata.desktop/json too");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Review kate config pages to improve maintenance friendliness");?></li>
<li><?php i18n("Allow to change the Mode, after changing the Highlighting");?></li>
<li><?php i18n("ViewConfig: Use new generic config interface");?></li>
<li><?php i18n("Fix bookmark pixmap painting on icon bar");?></li>
<li><?php i18n("Ensure the left border miss no change of the count of line number digits");?></li>
<li><?php i18n("Fix to show folding preview when move the mouse from bottom to top");?></li>
<li><?php i18n("Review IconBorder");?></li>
<li><?php i18n("Add input methods to input method status bar button");?></li>
<li><?php i18n("Paint the folding marker in proper color and make it more visible");?></li>
<li><?php i18n("remove default shortcut F6 to show icon border");?></li>
<li><?php i18n("Add action to toggle folding of child ranges (bug 344414)");?></li>
<li><?php i18n("Retitle button \"Close\" to \"Close file\" when a file has been removed on disk (bug 406305)");?></li>
<li><?php i18n("up copy-right, perhaps that should be a define, too");?></li>
<li><?php i18n("avoid conflicting shortcuts for switching tabs");?></li>
<li><?php i18n("KateIconBorder: Fix folding popup width and height");?></li>
<li><?php i18n("avoid view jump to bottom on folding changes");?></li>
<li><?php i18n("DocumentPrivate: Respect indention mode when block selection (bug 395430)");?></li>
<li><?php i18n("ViewInternal: Fix makeVisible(..) (bug 306745)");?></li>
<li><?php i18n("DocumentPrivate: Make bracket handling smart (bug 368580)");?></li>
<li><?php i18n("ViewInternal: Review drop event");?></li>
<li><?php i18n("Allow to close a document whose file was deleted on disk");?></li>
<li><?php i18n("KateIconBorder: Use UTF-8 char instead of special pixmap as dyn wrap indicator");?></li>
<li><?php i18n("KateIconBorder: Ensure Dyn Wrap Marker are shown");?></li>
<li><?php i18n("KateIconBorder: Code cosmetic");?></li>
<li><?php i18n("DocumentPrivate: Support auto bracket in block selection mode (bug 382213)");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Fix l/100 km to MPG conversion (bug 378967)");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Set correct kwalletd_bin_path");?></li>
<li><?php i18n("Export path of kwalletd binary for kwallet_pam");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Add CriticalNotification window type to PlasmaShellSurface protocol");?></li>
<li><?php i18n("Implement wl_eglstream_controller Server Interface");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Update kcharselect-data to Unicode 12.1");?></li>
<li><?php i18n("KCharSelect's internal model: ensure rowCount() is 0 for valid indexes");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Introduce CriticalNotificationType");?></li>
<li><?php i18n("Support NET_WM_STATE_FOCUSED");?></li>
<li><?php i18n("Document that modToStringUser and stringUserToMod only deal with english strings");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("KKeySequenceWidget: Don't show English strings to the user");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("WireGuard: Do not require 'private-key' to be non-empty for 'private-key-flags'");?></li>
<li><?php i18n("WireGuard: workaround wrong secret flag type");?></li>
<li><?php i18n("WireGuard: private-key and preshared-keys can be requested together");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("PlatformComponentsPlugin: fix plugin iid to QQmlExtensionInterface");?></li>
<li><?php i18n("IconItem: remove remaining &amp; unused smooth property bits");?></li>
<li><?php i18n("[Dialog] Add CriticalNotification type");?></li>
<li><?php i18n("Fix wrong group names for 22, 32 px in audio.svg");?></li>
<li><?php i18n("make the mobile text toolbar appear only on press");?></li>
<li><?php i18n("use the new Kirigami.WheelHandler");?></li>
<li><?php i18n("Add more icon sizes for audio, configure, distribute");?></li>
<li><?php i18n("[FrameSvgItem] Update filtering on smooth changes");?></li>
<li><?php i18n("Air/Oxygen desktoptheme: fix progressbar height using \"hint-bar-size\"");?></li>
<li><?php i18n("Fix stylesheet support for audio-volume-medium");?></li>
<li><?php i18n("Update audio, drive, edit, go, list, media, plasmavault icons to match breeze-icons");?></li>
<li><?php i18n("Align z's to pixel grid in system.svg");?></li>
<li><?php i18n("use the mobiletextcursor from proper namespace");?></li>
<li><?php i18n("[FrameSvgItem] Respect smooth property");?></li>
<li><?php i18n("Oxygen desktoptheme: add padding to hands, against jagged outline on rotation");?></li>
<li><?php i18n("SvgItem, IconItem: drop \"smooth\" property override, update node on change");?></li>
<li><?php i18n("Support gzipping of svgz also on windows, using 7z");?></li>
<li><?php i18n("Air/Oxygen desktoptheme: fix hand offsets with hint-*-rotation-center-offset");?></li>
<li><?php i18n("Add invokable public API for emitting contextualActionsAboutToShow");?></li>
<li><?php i18n("Breeze desktoptheme clock: support hand shadow offset hint of Plasma 5.16");?></li>
<li><?php i18n("Keep desktoptheme SVG files uncompressed in repo, install svgz");?></li>
<li><?php i18n("separate mobile text selection to avoid recursive imports");?></li>
<li><?php i18n("Use more appropriate \"Alternatives\" icon and text");?></li>
<li><?php i18n("FrameSvgItem: add \"mask\" property");?></li>
</ul>

<h3><?php i18n("Prison");?></h3>

<ul>
<li><?php i18n("Aztec: Fix padding if the last partial codeword is all one bits");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Avoid nesting Controls in TextField (bug 406851)");?></li>
<li><?php i18n("make the mobile text toolbar appear only on press");?></li>
<li><?php i18n("[TabBar] Update height when TabButtons are added dynamically");?></li>
<li><?php i18n("use the new Kirigami.WheelHandler");?></li>
<li><?php i18n("Support custom icon size for ToolButton");?></li>
<li><?php i18n("It compile fine without foreach");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("[Fstab] Add support for non-network filesystems");?></li>
<li><?php i18n("[FsTab] Add cache for device file system type");?></li>
<li><?php i18n("[Fstab] Preparatory work for enabling filesystems beyond NFS/SMB");?></li>
<li><?php i18n("Fix no member named 'setTime_t' in 'QDateTime' error while building (bug 405554)");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Add syntax highlighting for fish shell");?></li>
<li><?php i18n("AppArmor: don't highlight variable assignments and alias rules within profiles");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.58");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.10");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
