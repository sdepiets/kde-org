<?php
	include_once ("functions.inc");
	$translation_file = "kde-org";
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.12 LTS Beta Makes Your Desktop Smoother and Speedier",
		'cssFile' => '/content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = 'plasma-5.11.95'; // for i18n
	$version = "5.11.95";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px; 
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}

figure {
    position: relative;
    z-index: 2;
    font-size: smaller;
    /* text-shadow: 2px 2px 5px grey; */
    font-style: italic;
    color: #545454;
    text-align: right;
}
</style>

<main class="releaseAnnouncment container">


	<h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n("Plasma 5.12 LTS Beta")?></h1>

	<?php include "./announce-i18n-bar.inc"; ?>

	<!--
	<figure class="videoBlock">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/nMFDrBIA0PM?rel=0" allowfullscreen='true'></iframe>
	</figure>
-->
	
	<figure class="topImage">
		<a href="plasma-5.12/plasma-5.12.png">
			<img src="plasma-5.12/plasma-5.12-wee.png" width="600" height="338" alt="Plasma 5.12 LTS Beta" />
		</a>
		<figcaption><?php print i18n_var("KDE Plasma %1", "5.12 LTS Beta")?></figcaption>
	</figure>

	<p>
		<?php i18n("Monday, 15 Jan 2018.")?>
		<?php print i18n_var("Plasma 5.12 LTS Beta is the second long term support release from the Plasma 5 team. We have been working hard focusing on speed and stability for this release.  Boot time to desktop has been improved by reviewing the code for anything which blocks execution.  The team has been triaging and fixing bugs in every aspect of the codebase, tidying up artwork, removing corner cases and ensuring cross desktop integration.  For the first time we offer our Wayland integration on long term support so you can be sure we will continue to fix bugs over the coming releases.");?>
	</p>

<br clear="all" />

<h2><?php i18n("New in Plasma 5.12 LTS");?></h2>

<br clear="all" />
<h3><?php i18n("Smoother and Speedier");?></h3>

<p><?php i18n("We have been concentrating on speed and memory improvements with this long term support release.  When Plasma is running it now uses less CPU and less memory than previous versions.  The time it takes to start a Plasma desktop has been reduced dramatically.");?></p>

<br clear="all" />
<h3><?php i18n("Plasma on Wayland Now Under LTS promise");?></h3>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/kscreen-wayland.png">
<img src="plasma-5.12/kscreen-wayland-wee.png" width="350" height="338" />
</a>
<figcaption><?php i18n("Display Setup Now Supports Wayland");?></figcaption>
</figure>
<p><?php i18n("Plasma's support for running as Wayland is now more complete and is now suitable for more wide-ranged testing.  It is included as Long Term Support for the first time so we will be fixing bugs in the 5.12 LTS series. New features include:");?></p>

<ul><?php i18n("
<li>Output resolution can be set through KScreen</li>
<li>Enable/Disable outputs through KScreen</li>
<li>Screen rotation</li>
<li>Automatic screen rotation based on orientation sensor</li>
<li>Automatic touch screen calibration</li>
<li>XWayland is no longer required to run the Plasma desktop; applications only supporting X still make use of it.</li>
<li>Wayland windows can be set to fullscreen</li>
<li>Uses real-time scheduling policy to keep input responsive</li>
<li>Automatic selection of the Compositor based on the used platform</li>
<li>Starts implementing window rules</li>
<li>KWin integrated Night Color removes blue light from your screen at night-time; this is a Wayland-only replacement for the great Redshift app on X</li>");?>
</ul>

<p><?php i18n("For those who know their Wayland internals the protocols we added are:");?></p>

<ul><?php i18n("
<li>xdg_shell_unstable_v6</li>
<li>xdg_foreign_unstable_v2</li>
<li>idle_inhibit_unstable_v1</li>
<li>server_decoration_palette</li>
<li>appmenu</li>
<li>wl_data_device_manager raised to version 3</li>");?>
</ul>

<p><?php i18n("Important change of policy: 5.12 is the last release which sees feature development in KWin on X11. With 5.13 onwards only new features relevant to Wayland are going to be added.");?></p>

<p><?php i18n("We have put a lot of work into making Wayland support in Plasma as good as possible but there are still some missing features and issues in certain hardware configurations so we don't yet recommend it for daily use. More information on the <a href='https://community.kde.org/Plasma/Wayland_Showstoppers'>Wayland status wiki page</a>.");?></p>

<br clear="all" />
<h3><?php i18n("Discover");?></h3>
<figure style="float: right; width: 300px;">
<a href="plasma-5.12/discover-app.png">
<img src="plasma-5.12/discover-app-wee.png" style="border: 0px" width="360" height="367" alt="<?php i18n("Discover's new app page");?>" />
</a>
<figcaption><?php i18n("Discover's new app page");?></figcaption>
</figure>
<ul>
<?php i18n("
<li>Improvements in User Interface</li>
<ul>
<li>Redesigned app pages to showcase great software</li>
<li>Leaner headers on non-browsing sections</li>
<li>More compact browsing views, so you can see more apps at once<li>
<li>App screenshots are bigger and have keyboard navigation<li>
<li>Installed app list sorted alphabetically<li>
<li>More complete UI to configure sources</li>
</ul>
<li>Much greater stability</li>
<li>Improvements in Snap and Flatpak support</li>
<li>Support for apt:// URLs</li>
<li>Distributions can enable offline updates</li>
<li>Better usability on phone form factors: uses Kirigami main action, has a view specific for searching</li>
<li>Integrate PackageKit global signals into notifications</li>
<ul>
<li>Distro upgrade for new releases</li>
<li>Reboot notification for when Discover installs or updates a package that needs a reboot</li>
");?>
</ul>
</ul>

<br clear="all" />
<h3><?php i18n("New Features");?></h3>

<figure style="float: right; width: 360px;">
<a href="plasma-5.12/weather-applet.png">
<img src="plasma-5.12/weather-applet-wee.png" style="border: 0px" width="161" height="67" alt="<?php i18n("Weather Applet with Temperature");?>" />
</a>
<figcaption><?php i18n("Weather Applet with Temperature");?></figcaption>
<br />
<a href="plasma-5.12/system-monitor.png">
<img src="plasma-5.12/system-monitor-wee.png" style="border: 0px" width="350" height="125" alt="<?php i18n("CPU usage in System Activity");?>" />
</a>
<figcaption><?php i18n("CPU usage in System Monitor");?></figcaption>
<br />
<a href="plasma-5.12/window-shadows.png">
<img src="plasma-5.12/window-shadows-wee.png" style="border: 0px" width="360" height="164" alt="<?php i18n("Larger, horizontally-centered window shadows");?>" />
</a>
<figcaption><?php i18n("Larger, horizontally-centered window shadows");?></figcaption>
</figure>
<br />

<ul>
<?php i18n("
<li>Night Color feature to reduce evening blue light expose</li>
<li>Usability improvement for the global menu: adding a global menu panel or window decoration button enables it without needing an extra configuration step</li>
<li>Accessibility fixes in KRunner: it can now be completely used with on-screen readers such as Orca</li>
<li>Icon applet now uses website favicon</li>
<li>Notification text selectable again including copy link feature</li>
<li>Slim Kickoff application menu layout</li>
<li>The weather applet can now optionally show temperature next to the weather status icon on the panel</li>
<li>System Activity and System Monitor now show per-process graphs for the CPU usage</li>
<li>Clock widget's text is now sized more appropriately</li>
<li>Windows shadows are now horizontally centered, and larger by default</li>
<li>Properties dialog now shows file metadata</li>
");?>
</ul>

<br clear="all" />
<h2><?php i18n("What’s New Since Plasma 5.8 LTS");?></h2>

<p><?php i18n("If you have been using our previous LTS release, Plasma 5.8, there are many new features for you to look forward to:");?></p>

<br clear="all" />
<h2><?php i18n("Previews in Notifications");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/spectacle-notification.png">
<img src="plasma-5.12/spectacle-notification-wee.png" width="350" height="192" />
</a>
<figcaption><?php i18n("Preview in Notifications");?></figcaption>
</figure>

<p><?php i18n("The notification system gained support for interactive previews that allow you to quickly take a screenshot and drag it into a chat window, an email composer, or a web browser form. This way you never have to leave the application you’re currently working with.");?></p>  

<br clear="all" />
<h2><?php i18n("Task Manager Improvement");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/mute.png">
<img src="plasma-5.12/mute.png" width="340" height="399" />
</a>
<figcaption><?php i18n("Audio Icon and Mute Button Context Menu Entry");?></figcaption>
</figure>

<p><?php i18n("Due to popular demand we implemented switching between windows in Task Manager using Meta + number shortcuts for heavy multi-tasking. Also new in Task Manager is the ability to pin different applications in each of your activities. And should you want to focus on one particular task, applications currently playing audio are marked with an icon similar to how it’s done in modern web browsers. Together with a button to mute the offending application, this can help you stay focused.");?></p>

<br clear="all" />
<h2><?php i18n("Global Menus");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/global-menus-window-bar.png">
<img src="plasma-5.12/global-menus-window-bar-wee.png" width="350" height="258" />
</a>
<figcaption><?php i18n("Global Menu Screenshots, Applet and Window Decoration");?></figcaption>
</figure>

<p><?php i18n("Global Menus have also returned. KDE's pioneering feature to separate the menu bar from the application window allows for new user interface paradigm with either a Plasma Widget showing the menu or neatly tucked away in the window title bar. Setting it up has been greatly simplified in Plasma 5.12: as soon as you add the Global Menu widget or title bar button, the required background service gets started automatically.");?></p>

<br clear="all" />
<h2><?php i18n("Spring Loaded Folders");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/spring_loading.gif">
<img src="plasma-5.12/spring_loading.gif" style="border: 0px" width="350" height="192" alt="<?php i18n("Spring Loading in Folder View");?>" />
</a>
<figcaption><?php i18n("Spring Loading in Folder View");?></figcaption>
</figure>

<p><?php i18n("Folder View is now the default desktop layout. After some years shunning icons on the desktop we have accepted the inevitable and changed to Folder View as the default desktop which brings some icons by default and allows users to put whatever files or folders they want easy access to. For this many improvements have been made to Folder View, including <a href='https://blogs.kde.org/2017/01/31/plasma-510-spring-loading-folder-view-performance-work'>Spring Loading</a> making drag and drop of files powerful and quick, a tighter icon grid, as well as massively improved performance.");?></p>

<br clear="all" />
<h2><?php i18n("Music Controls in Lock Screen");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/lock-music.png">
<img src="plasma-5.12/lock-music-wee.png" width="350" height="329" />
</a>
<figcaption><?php i18n("Media Controls on Lock Screen");?></figcaption>
</figure>

<p><?php i18n("Media controls have been added to the lock screen, which can be disabled since Plasma 5.12 for added privacy. Moreover, music will automatically pause when the system suspends.");?></p>

<br clear="all" />
<h2><?php i18n("New System Settings Interface");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/system-settings.png">
<img src="plasma-5.12/system-settings-wee.png" width="350" height="302" />
</a>
<figcaption><?php i18n("New Look System Settings");?></figcaption>
</figure>

<p><?php i18n("We introduced a new System Settings user interface for easy access to commonly used settings. It is the first step in making this often-used and complex application easier to navigate and more user-friendly. The new design is added as an option, users who prefer the older icon or tree views can move back to their preferred way of navigation.");?></p>

<br clear="all" />
<h2><?php i18n("Plasma Vaults");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/plasma-vault.png">
<img src="plasma-5.12/plasma-vault-wee.png" width="350" height="331" />
</a>
<figcaption><?php i18n("Plasma Vaults");?></figcaption>
</figure>

<p><?php i18n("KDE has a focus on privacy. Our vision is: A world in which everyone has control over their digital life and enjoys freedom and privacy.");?></p>

<p><?php i18n("For users who often deal with sensitive, confidential and private information, the new Plasma Vault offers strong encryption features presented in a user-friendly way. It allows to lock and encrypt sets of documents and hide them from prying eyes even when the user is logged in. Plasma Vault extends Plasma's activities feature with secure storage.");?></p>

<br clear="all" />

<h2><?php i18n("Plasma's Comprehensive Features");?></h2>

<p><?php i18n("Take a look at what Plasma offers, a comprehensive selection of features unparalleled in any desktop software.");?></p>

<h3><?php i18n("Desktop Widgets");?></h3>
<figure style="float: right">
<a href="plasma-5.12/widgets.png">
<img src="plasma-5.12/widgets-wee.png" style="border: 0px" width="250" height="441" alt="<?php i18n("Desktop Widgets");?>" />
</a>
<figcaption><?php i18n("Desktop Widgets");?></figcaption>
</figure>

<p><?php i18n("Cover your desktop in useful widgets to keep you up to date with weather, amused with comics or helping with calculations.");?></p>

<br clear="all" />
<h3><?php i18n("Get Hot New Stuff");?></h3>
<figure style="float: right">
<a href="plasma-5.12/ghns.png">
<img src="plasma-5.12/ghns-wee.png" style="border: 0px" width="350" height="278" alt="<?php i18n("Get Hot New Stuff");?>" />
</a>
<figcaption><?php i18n("Get Hot New Stuff");?></figcaption>
</figure>

<p><?php i18n("Download wallpapers, window style, widgets, desktop effects and dozens of other resources straight to your desktop.  We work with the new <a href=\"http://store.kde.org\">KDE Store</a> to bring you a wide selection of addons for you to install.");?></p>

<br clear="all" />
<h3><?php i18n("Desktop Search");?></h3>
<figure style="float: right">
<a href="plasma-5.12/krunner.png">
<img src="plasma-5.12/krunner-wee.png" style="border: 0px" width="350" height="172" alt="<?php i18n("Desktop Search");?>" />
</a>
<figcaption><?php i18n("Desktop Search");?></figcaption>
</figure>

<p><?php i18n("Plasma will let you easily search your desktop for applications, folders, music, video, files... everything you have.");?></p>

<br clear="all" />
<h3><?php i18n("Unified Look");?></h3>
<figure style="float: right">
<a href="plasma-5.12/gtk-integration.png">
<img src="plasma-5.12/gtk-integration-wee.png" style="border: 0px" width="350" height="261" alt="<?php i18n("Unified Look");?>" />
</a>
<figcaption><?php i18n("Unified Look");?></figcaption>
</figure>

<p><?php i18n("Plasma's default Breeze theme has a unified look across all the common programmer toolkits - Qt 4 &amp; 5, GTK 2 &amp; 3, even LibreOffice.");?></p>

<br clear="all" />
<h3><?php i18n("Phone Integration");?></h3>
<figure style="float: right">
<a href="plasma-5.12/kdeconnect.png">
<img src="plasma-5.12/kdeconnect-wee.png" style="border: 0px" width="350" height="259" alt="<?php i18n("Phone Integration");?>" />
</a>
<figcaption><?php i18n("Phone Integration");?></figcaption>
</figure>
<p><?php i18n("Using KDE Connect you'll be notified on your desktop of text message, can easily transfer files, have your music silenced during calls and even use your phone as a remote control.");?></p>

<br clear="all" />
<h3><?php i18n("Infinitely Customisable");?></h3>
<figure style="float: right">
<a href="plasma-5.12/customisable.png">
<img src="plasma-5.12/customisable-wee.png" style="border: 0px" width="350" height="197" alt="<?php i18n("Infinitely Customisable");?>" />
</a>
<figcaption><?php i18n("Infinitely Customisable");?></figcaption>
</figure>
<p><?php i18n("Plasma is simple by default but you can customise it however you like with new widgets, panels, screens and styles.");?></p>

<br clear="all" />

	<a href="plasma-5.11.5-5.11.95-changelog.php"><?php print i18n_var("Full Plasma 5.12 changelog"); ?></a>

	<!-- // Boilerplate again -->
	<section class="row get-it">
		<article class="col-md">
			<h2><?php i18n("Live Images");?></h2>
			<p>
				<?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
			</p>
			<a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
			<a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
		</article>

		<article class="col-md">
			<h2><?php i18n("Package Downloads");?></h2>
			<p>
				<?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
			</p>
			<a href='https://community.kde.org/Plasma/Packages' class="learn-more"><?php i18n("Package download wiki page");?></a>
		</article>

		<article class="col-md">
			<h2><?php i18n("Source Downloads");?></h2>
			<p>
				<?php i18n("You can install Plasma 5 directly from source.");?>
			</p>
			<a href='http://community.kde.org/Frameworks/Building'><?php i18n("Community instructions to compile it");?></a>
			<a href='../info/plasma-5.12.0.php' class='learn-more'><?php i18n("Source Info Page");?></a>
		</article>
	</section>

	<section class="give-feedback">
		<h2><?php i18n("Feedback");?></h2>

		<p>
			<?php print i18n_var("You can give us feedback and get updates on <a href='%1'><img src='%2' /></a> <a href='%3'>Facebook</a>
			or <a href='%4'><img src='%5' /></a> <a href='%6'>Twitter</a>
			or <a href='%7'><img src='%8' /></a> <a href='%9'>Google+</a>.", "https://www.facebook.com/kde", "https://www.kde.org/announcements/facebook.gif", "https://www.facebook.com/kde", "https://twitter.com/kdecommunity", "https://www.kde.org/announcements/twitter.png", "https://twitter.com/kdecommunity", "https://plus.google.com/105126786256705328374/posts", "https://www.kde.org/announcements/googleplus.png", "https://plus.google.com/105126786256705328374/posts"); ?>
		</p>
		<p>
			<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
		</p>

		<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

		<p><?php i18n("Your feedback is greatly appreciated.");?></p>
	</section>

	<h2>
		<?php i18n("Supporting KDE");?>
	</h2>

	<p align="justify">
		<?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. <a href='%3'>supporting member programme</a>.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/"); ?>
	</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
