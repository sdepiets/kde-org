<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.8.0");
  $site_root = "../";
  $release = '5.8.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
March 13, 2015. KDE today announces the release
of KDE Frameworks 5.8.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<p><?php i18n("New frameworks:");?></p>
<ul>
<li><?php i18n("KPeople, provides access to all contacts and the people who hold them");?></li>
<li><?php i18n("KXmlRpcClient, interaction with XMLRPC services");?></li>
</ul>

<h3><?php i18n("General");?></h3>

<ul>
<li><?php i18n("A number of build fixes for compiling with the upcoming Qt 5.5");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Resources scoring service is now finalized");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Stop failing on ZIP files with redundant data descriptors");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Restore KCModule::setAuthAction");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KPluginMetadata: add support for Hidden key");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Prefer exposing lists to QML with QJsonArray");?></li>
<li><?php i18n("Handle non default devicePixelRatios in images");?></li>
<li><?php i18n("Expose hasUrls in DeclarativeMimeData");?></li>
<li><?php i18n("Allow users to configure how many horizontal lines are drawn");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Fix the build on MacOSX when using Homebrew");?></li>
<li><?php i18n("Better styling of media objects (images, ...) in documentation");?></li>
<li><?php i18n("Encode invalid chars in paths used in XML DTDs, avoiding errors");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Activation timestamp set as dynamic property on triggered QAction.");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Fix QIcon::fromTheme(xxx, someFallback) would not return the fallback");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("Make PSD image reader endianess-agnostic.");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Deprecate UDSEntry::listFields and add the UDSEntry::fields method which returns a QVector without costly conversion.");?></li>
<li><?php i18n("Sync bookmarkmanager only if change was by this process (bug 343735)");?></li>
<li><?php i18n("Fix startup of kssld5 dbus service");?></li>
<li><?php i18n("Implement quota-used-bytes and quota-available-bytes from RFC 4331 to enable free space information in http ioslave.");?></li>
</ul>

<h3><?php i18n("KNotifications");?></h3>

<ul>
<li><?php i18n("Delay the audio init until actually needed");?></li>
<li><?php i18n("Fix notification config not applying instantly");?></li>
<li><?php i18n("Fix audio notifications stopping after first file played");?></li>
</ul>

<h3><?php i18n("KNotifyConfig");?></h3>

<ul>
<li><?php i18n("Add optional dependency on QtSpeech to reenable speaking notifications.");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("KPluginInfo: support stringlists as properties");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Add word count statistics in statusbar");?></li>
<li><?php i18n("vimode: fix crash when removing last line in Visual Line mode");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Make KRatingWidget cope with devicePixelRatio");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("KSelectionWatcher and KSelectionOwner can be used without depending on QX11Info.");?></li>
<li><?php i18n("KXMessages can be used without depending on QX11Info");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Add new properties and methods from NetworkManager 1.0.0");?></li>
</ul>

<h4>Plasma framework</h4>

<ul>
<li><?php i18n("Fix plasmapkg2 for translated systems");?></li>
<li><?php i18n("Improve tooltip layout");?></li>
<li><?php i18n("Make it possible to let plasmoids to load scripts outside the plasma package
...");?></li>
</ul>

<h3><?php i18n("Buildsystem changes (extra-cmake-modules)");?></h3>

<ul>
<li><?php i18n("Extend ecm_generate_headers macro to also support CamelCase.h headers");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "$5.8");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.2");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
