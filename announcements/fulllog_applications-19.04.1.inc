<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Don't use qAsConst over a temporary variable. <a href='http://commits.kde.org/akonadi/67976505f1ae9fcab044c8342a1b2fddb810b5ea'>Commit.</a> </li>
<li>Improve error message "multiple merge candidates" to indicate the collection name. <a href='http://commits.kde.org/akonadi/f2e8d84f89f4bab0632939c7404623ddaeb0163b'>Commit.</a> </li>
</ul>
<h3><a name='akonadiconsole' href='https://cgit.kde.org/akonadiconsole.git'>akonadiconsole</a> <a href='#akonadiconsole' onclick='toggle("ulakonadiconsole", this)'>[Hide]</a></h3>
<ul id='ulakonadiconsole' style='display: block'>
<li>Browser tab: fix sorting by task summary. <a href='http://commits.kde.org/akonadiconsole/51c31b72918c883ab0002e3a7d97c7fb8a29a099'>Commit.</a> </li>
<li>Browser tab: update item details whenever current item changes in the list. <a href='http://commits.kde.org/akonadiconsole/0e3640a61c2f12bda91b4b3f7dd7e82aa2ffa6a7'>Commit.</a> </li>
</ul>
<h3><a name='akregator' href='https://cgit.kde.org/akregator.git'>akregator</a> <a href='#akregator' onclick='toggle("ulakregator", this)'>[Hide]</a></h3>
<ul id='ulakregator' style='display: block'>
<li>Fix Bug 394946 - Delete key no longer works in articles list after using search - keyboard focus stays in search field. <a href='http://commits.kde.org/akregator/49b55ffb92c28ed20eb2e20514dbf6dfe490a946'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394946'>#394946</a></li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>AddToArchive: fix filename preset for folders. <a href='http://commits.kde.org/ark/1ef618e56ad7b71be2e1ead2abf3b7dba93dd2e3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406812'>#406812</a></li>
<li>Fix ark cannot be added as favourite in GNOME. <a href='http://commits.kde.org/ark/882f428e88c5b0b51c3feb27cc6771533e879103'>Commit.</a> </li>
</ul>
<h3><a name='baloo-widgets' href='https://cgit.kde.org/baloo-widgets.git'>baloo-widgets</a> <a href='#baloo-widgets' onclick='toggle("ulbaloo-widgets", this)'>[Hide]</a></h3>
<ul id='ulbaloo-widgets' style='display: block'>
<li>[TagsFileItemAction] Only show action when file is writable. <a href='http://commits.kde.org/baloo-widgets/303e815f3e3473246976b4f0e420d47af02cd453'>Commit.</a> </li>
<li>[TagsFileItemAction] Avoid readding an existing tag via "New tag...". <a href='http://commits.kde.org/baloo-widgets/80000b7414daa3b9d55f5879ca07d872416ed9e7'>Commit.</a> </li>
<li>[TagsFileItemAction] Hide tags action if XAttrs are not supported. <a href='http://commits.kde.org/baloo-widgets/77f3c586364a7b6be495e63802525840dcfa6fd0'>Commit.</a> </li>
<li>[TagsFileItemAction] Fix bad truncation of tag name. <a href='http://commits.kde.org/baloo-widgets/0bce0c4eda2b9cc7bf1e3490693851e7d0ff8efe'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405777'>#405777</a></li>
</ul>
<h3><a name='calendarsupport' href='https://cgit.kde.org/calendarsupport.git'>calendarsupport</a> <a href='#calendarsupport' onclick='toggle("ulcalendarsupport", this)'>[Hide]</a></h3>
<ul id='ulcalendarsupport' style='display: block'>
<li>Don't use qAsConst over a temporary variable. <a href='http://commits.kde.org/calendarsupport/4ca6ba1140eca3269f2c5f112e21370e08352bbe'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>[Scilab] Fix interrupt command and fix extra empty lines in scilab results. <a href='http://commits.kde.org/cantor/291bb1ada3cff18f82a85bb8023d99d81e8c0034'>Commit.</a> </li>
<li>[Octave] Fix expression logic: don't treats warnings from octave as error. <a href='http://commits.kde.org/cantor/5dc8af2afd7e7669b43497b58815596c0b7f2c82'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406777'>#406777</a></li>
<li>[Octave] Some fixes:. <a href='http://commits.kde.org/cantor/4b78512e9a83f67984583b4e393963c77b026e98'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406776'>#406776</a></li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>[PlacesItemModelTest] Ignore user tag places. <a href='http://commits.kde.org/dolphin/d41d3be348e76feab3d31f5eec875f6a392ff40f'>Commit.</a> </li>
<li>Try again to fix PlacesItemModelTest::testDefaultViewProperties(). <a href='http://commits.kde.org/dolphin/33a0d17ea8d4de81322ca51a5acc1356f37798ed'>Commit.</a> </li>
<li>Fix dolphin cannot be added as favourite in GNOME. <a href='http://commits.kde.org/dolphin/9f1b6eb3f4275bbc9fd6b00a4127291bc9f16caf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407198'>#407198</a></li>
<li>Fix missing update of file name label in information panel without preview. <a href='http://commits.kde.org/dolphin/28a949a925193bc71007efa15034df325e797939'>Commit.</a> </li>
<li>Fix adding "Create New..." menu to toolbar. <a href='http://commits.kde.org/dolphin/48c082767e3124b1ac6575f83bfcdcdc3e3e9b62'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405780'>#405780</a></li>
</ul>
<h3><a name='dragon' href='https://cgit.kde.org/dragon.git'>dragon</a> <a href='#dragon' onclick='toggle("uldragon", this)'>[Hide]</a></h3>
<ul id='uldragon' style='display: block'>
<li>Enable high-dpi pixmaps to ensure icons look correct with high-dpi scaling. <a href='http://commits.kde.org/dragon/45a68ef44acad7ef0988fc0b040e5cf693b5ba53'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407134'>#407134</a></li>
<li>Typo--. <a href='http://commits.kde.org/dragon/e3c6d7537463874f93c9e013c408075302ccb8ff'>Commit.</a> </li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Hide]</a></h3>
<ul id='ulincidenceeditor' style='display: block'>
<li>Incidence Dialog: don't show an empty message widget initially. <a href='http://commits.kde.org/incidenceeditor/e8bb93c717003f913a6f7b23b2b5a6436dc32726'>Commit.</a> </li>
</ul>
<h3><a name='k3b' href='https://cgit.kde.org/k3b.git'>k3b</a> <a href='#k3b' onclick='toggle("ulk3b", this)'>[Hide]</a></h3>
<ul id='ulk3b' style='display: block'>
<li>Fix running the tests with CMAKE_RUNTIME_OUTPUT_DIRECTORY set. <a href='http://commits.kde.org/k3b/eb117b98a107fb90678acc4c4f9ea4087c9311eb'>Commit.</a> </li>
</ul>
<h3><a name='kamoso' href='https://cgit.kde.org/kamoso.git'>kamoso</a> <a href='#kamoso' onclick='toggle("ulkamoso", this)'>[Hide]</a></h3>
<ul id='ulkamoso' style='display: block'>
<li>Don't leak gst structures. <a href='http://commits.kde.org/kamoso/f3753e2b12d5c542482e041a49001261210efa40'>Commit.</a> </li>
<li>Add DesktopEntry to notifyrc. <a href='http://commits.kde.org/kamoso/9711a69a91f199662ba65340687ecdbc4c7883c9'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>Tabswitcher_test: pass correct binary name to add_test(). <a href='http://commits.kde.org/kate/b75ca9dc61402124b346cf865c203b402353c1aa'>Commit.</a> </li>
<li>Use add_test signature where target command gets resolved to binary path. <a href='http://commits.kde.org/kate/33ca28425d2cfd74facaa74ab62bcc17a27a8a6f'>Commit.</a> </li>
</ul>
<h3><a name='kcalutils' href='https://cgit.kde.org/kcalutils.git'>kcalutils</a> <a href='#kcalutils' onclick='toggle("ulkcalutils", this)'>[Hide]</a></h3>
<ul id='ulkcalutils' style='display: block'>
<li>Revert "GIT_SILENT: Prepare 5.11.1". <a href='http://commits.kde.org/kcalutils/1f7909e2a2d0ce786ea7eb83363185fd46e47d0c'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Add missing lock in model cleanup. <a href='http://commits.kde.org/kdenlive/d6ee2612cc4a44b97166410b18feab07b346c12a'>Commit.</a> </li>
<li>Move levels effect back to main effects. <a href='http://commits.kde.org/kdenlive/54210884a2f8bfe8654e3d5ee54286f8c92bfeb8'>Commit.</a> </li>
<li>Fix crash closing project with locked tracks. Fixes #177. <a href='http://commits.kde.org/kdenlive/9353f7fa82d49b2b873a9c60e8c82b921b8a1b1f'>Commit.</a> </li>
<li>Speedup selecting bin clip when using proxies (cache original properties). <a href='http://commits.kde.org/kdenlive/fc81e1653ea7e007bbc17a0269b5f20b5c1e4f80'>Commit.</a> </li>
<li>Disable threaded rendering with movit. <a href='http://commits.kde.org/kdenlive/a048e250b86cc9e2de745df55f8bd77be9a1cfb7'>Commit.</a> </li>
<li>Fix wrong thumbnails sometimes displayed. <a href='http://commits.kde.org/kdenlive/7249f2fa72bc49274a31708136487ecb29ee5b07'>Commit.</a> </li>
<li>Ensure fades always start or end at clip border. <a href='http://commits.kde.org/kdenlive/34bed63e95747bc0314f38eff28864221b7cb716'>Commit.</a> </li>
<li>Fix loading of clip zones. <a href='http://commits.kde.org/kdenlive/86588bc24e7a686fae4239372f6fc32d1a522814'>Commit.</a> </li>
<li>Fix transcoding crashes caused by old code. <a href='http://commits.kde.org/kdenlive/395445bea728e6e7386f71c8c1cb6e674fcdcbe0'>Commit.</a> </li>
<li>Fix fades copy/paste. <a href='http://commits.kde.org/kdenlive/f3ac41ab8db8fd55b855f7482870d876fac23af1'>Commit.</a> </li>
<li>Fix broken fadeout. <a href='http://commits.kde.org/kdenlive/48dd96e08cd314441da482b67b23c7f9f2f26193'>Commit.</a> </li>
<li>Fix track red background on undo track deletion. <a href='http://commits.kde.org/kdenlive/ce23fec8122fdbf9a29e4e8228c9161138c84c5b'>Commit.</a> </li>
<li>Update appdata version. <a href='http://commits.kde.org/kdenlive/052e7b00a303d8a1ea00d33880813aef88952870'>Commit.</a> </li>
<li>Zooming in these widgets using CTRL+two-finger scrolling was almost. <a href='http://commits.kde.org/kdenlive/b7f5e7ce5bf5b19f32b49f34a9a04b43d6a5ec44'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406985'>#406985</a></li>
<li>Fix crash on newly created profile change. <a href='http://commits.kde.org/kdenlive/1da40dbc4320c50ed09a611c6103aa6c3b5e6ae0'>Commit.</a> </li>
<li>Always create audio thumbs from original source file, not proxy because proxy clip can have a different audio layout. <a href='http://commits.kde.org/kdenlive/ff3097ac6c69c24f0879cd1eac114a8e51ae18b7'>Commit.</a> </li>
<li>Mark document modified when track compositing is changed. <a href='http://commits.kde.org/kdenlive/fcbfdb00b03eac5d7869b62c2c7f51d3ae3875d6'>Commit.</a> </li>
<li>Fix compositing sort error. <a href='http://commits.kde.org/kdenlive/5d6376f833d6dc6157d9132c0c96c0c77ce5d54b'>Commit.</a> </li>
<li>Fix crash opening old project, fix disabled clips not saved. <a href='http://commits.kde.org/kdenlive/0b58e790d1d792b4ae55c5ed36ecb782eeae724b'>Commit.</a> </li>
<li>Fix crash and broken undo/redo with lift/gamma/gain effect. Fixes #172. <a href='http://commits.kde.org/kdenlive/c900e5b3b7d5dab72299f5cbbff61920fc193b95'>Commit.</a> </li>
<li>Fix clip marker menu. Fixes #168. <a href='http://commits.kde.org/kdenlive/1a176696f9cfccb16b4e7f3560e4b9482c053eac'>Commit.</a> </li>
<li>Fix composition forced track lost on project load. Fixes #169. <a href='http://commits.kde.org/kdenlive/876d46d8b11f5fb2ed9c3919b22e1dad1e995187'>Commit.</a> </li>
<li>Fix spacer / remove space with only 1 clip. Fixes #162. <a href='http://commits.kde.org/kdenlive/ad27e4b9844eadbf4a8c1ed4d6312df4d4015a45'>Commit.</a> </li>
<li>Fix timeline corruption (some operations used a copy of master prod instead of track producer). <a href='http://commits.kde.org/kdenlive/baf386fb4f980e7eeabaadcac9240640d9d80f9b'>Commit.</a> </li>
<li>Renderwidget: Use max number of threads in render. <a href='http://commits.kde.org/kdenlive/4f774201ddc8e3a88c01f7c0758dfe1568441bd9'>Commit.</a> </li>
<li>Fix razor tool not working in some cases. Fixes #160. <a href='http://commits.kde.org/kdenlive/8580bf212ade31f65b1c7745b628f370c0049ad0'>Commit.</a> </li>
<li>Better os detection macro. <a href='http://commits.kde.org/kdenlive/7b2afc2618df79922a5bc4c1c31fcbb1676f22a9'>Commit.</a> </li>
<li>Remove crash, not solving 1st startup not accepting media (see #117). <a href='http://commits.kde.org/kdenlive/48a5cdb420e22dd0938f9159ddb49f0f93147d00'>Commit.</a> </li>
<li>Remove unneeded unlock crashing on Windows. <a href='http://commits.kde.org/kdenlive/0ed3dc18863d956e03b2eee2a19b496be7194d0c'>Commit.</a> </li>
<li>Some fixes in tests. <a href='http://commits.kde.org/kdenlive/c4f94791315303f7a798ece65be5b873eeaa8d96'>Commit.</a> </li>
<li>Forgotten file. <a href='http://commits.kde.org/kdenlive/bd22dce8d09fda05a11d3702dd785ebbfaba6c8d'>Commit.</a> </li>
<li>Improve marker tests, add abort testing feature. <a href='http://commits.kde.org/kdenlive/a6e78fe033a8be8f99643f79290efdc5e8ed6fd9'>Commit.</a> </li>
<li>Add tests for unlimited clips resize. <a href='http://commits.kde.org/kdenlive/b1b811676bb8f4d6b330c57f1d31e3e2bcb7a395'>Commit.</a> </li>
<li>Small fix in tests. <a href='http://commits.kde.org/kdenlive/d6cf6ca01296dc77b6112c4087961d92bfe9c576'>Commit.</a> </li>
<li>Fix AppImage audio recording (switch from wav to flac). <a href='http://commits.kde.org/kdenlive/ac43da1c34eb03a22be31a0085103bea3ffe6864'>Commit.</a> </li>
<li>Dont remember clip duration in case of profile change. Fixes #145. <a href='http://commits.kde.org/kdenlive/6dd5e215ce804a50ae4cf66b82f77fbb5a4407ab'>Commit.</a> </li>
<li>Fix spacer broken when activated over a timeline item. <a href='http://commits.kde.org/kdenlive/a1b45e5cc2389d316d369622031359d804d4d8bf'>Commit.</a> </li>
<li>Improve detection of composition direction. <a href='http://commits.kde.org/kdenlive/1331f66fc62d6695a840021ae031105ea1eb60d2'>Commit.</a> </li>
<li>Unconditionnaly reload producers on profile change. Related to #145. <a href='http://commits.kde.org/kdenlive/ed107a46f0547c34f4173bd46fff024c09204671'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Fix Bug 403317 - KMAIL2; "Share Text" plugin segfaults. <a href='http://commits.kde.org/kdepim-addons/32b4086c7ccfcddba01fe6d852bef8cf4fec9bff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403317'>#403317</a></li>
<li>Fix horizontal line. <a href='http://commits.kde.org/kdepim-addons/8a8b22c2a0bdb376e4274ddf714eeb20739ce01e'>Commit.</a> </li>
<li>Fix render image. <a href='http://commits.kde.org/kdepim-addons/abd207e9c6a88d445c5ce3f01e53ac1a913f253e'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/kdepim-addons/f7c56ebe9a9fceb203ef751120f5a48c4a67ce6f'>Commit.</a> </li>
<li>Fix enable/disable widget. <a href='http://commits.kde.org/kdepim-addons/4406cc6c4416e733cae96657586bdfd1afdbe3e3'>Commit.</a> </li>
<li>Add missing uselocalinstance settings. <a href='http://commits.kde.org/kdepim-addons/0b266d79433553a8fffd3c8ae0db43840397174d'>Commit.</a> See bug <a href='https://bugs.kde.org/406680'>#406680</a></li>
<li>Fix Bug 406680 - LanguageTools: kmail restart loses configuration of local server address in favor of default remote. <a href='http://commits.kde.org/kdepim-addons/7ee05c910ee2bea9bc33d51e8915a4aa1934849c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406680'>#406680</a></li>
<li>Add more autotest. <a href='http://commits.kde.org/kdepim-addons/379e2bcf4b1c7279da09c24544b664e03984b322'>Commit.</a> </li>
<li>Disable more feature. <a href='http://commits.kde.org/kdepim-addons/34c00852a9908127593017ca7e8b42c3f9b741ff'>Commit.</a> </li>
</ul>
<h3><a name='kgeography' href='https://cgit.kde.org/kgeography.git'>kgeography</a> <a href='#kgeography' onclick='toggle("ulkgeography", this)'>[Hide]</a></h3>
<ul id='ulkgeography' style='display: block'>
<li>Use more https. <a href='http://commits.kde.org/kgeography/bac23b265cb8699c4c32a642f0cf0e07ad3d8897'>Commit.</a> </li>
</ul>
<h3><a name='kidentitymanagement' href='https://cgit.kde.org/kidentitymanagement.git'>kidentitymanagement</a> <a href='#kidentitymanagement' onclick='toggle("ulkidentitymanagement", this)'>[Hide]</a></h3>
<ul id='ulkidentitymanagement' style='display: block'>
<li>Fix Bug 391631 - Identity not in list after creation. <a href='http://commits.kde.org/kidentitymanagement/5f20d144e0678a00b2d28454d54185f538ae6118'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391631'>#391631</a></li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix opening the New Mail Notifier agent configuration. <a href='http://commits.kde.org/kmail/3043f4484f77417fba187c55c9256fbba08b4ca2'>Commit.</a> </li>
<li>Fix Bug 407143 - settings window is too small. <a href='http://commits.kde.org/kmail/85a969a9111018662399127766581717c4dc657b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407143'>#407143</a></li>
<li>Really fix load recent file. <a href='http://commits.kde.org/kmail/c60a9174ecf6b31cd1473d3f80731f4428888a25'>Commit.</a> </li>
<li>Use default encoding. <a href='http://commits.kde.org/kmail/fb7c8873bb539944ec3f7f58bf08cf8ae19579c0'>Commit.</a> </li>
<li>Fix get charset. <a href='http://commits.kde.org/kmail/45ec6215226ef7f65072248d75436b251f25c526'>Commit.</a> </li>
<li>Fix Bug 391631 - Identity not in list after creation. <a href='http://commits.kde.org/kmail/0b112f0fe103f28e629946f68bf717bc6c13e7da'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391631'>#391631</a></li>
</ul>
<h3><a name='kmail-account-wizard' href='https://cgit.kde.org/kmail-account-wizard.git'>kmail-account-wizard</a> <a href='#kmail-account-wizard' onclick='toggle("ulkmail-account-wizard", this)'>[Hide]</a></h3>
<ul id='ulkmail-account-wizard' style='display: block'>
<li>Fix remove identity which was created. <a href='http://commits.kde.org/kmail-account-wizard/91133a5abd1894bafc6ada7a70c8fae7b9b72312'>Commit.</a> </li>
<li>Fix remove correct identity. <a href='http://commits.kde.org/kmail-account-wizard/df5608c35aabcfe58ef6f3a0f35c31b12e3aea86'>Commit.</a> </li>
<li>Allow to disable refreshing mime file (only for dev). <a href='http://commits.kde.org/kmail-account-wizard/8c90fd8e410102d5aa427fdaf395051294e46437'>Commit.</a> </li>
<li>Fix crash when we launch with specific type. mTypePage can be null. <a href='http://commits.kde.org/kmail-account-wizard/3b262793c7800cd1e01ee4bea1f765c141abb092'>Commit.</a> </li>
<li>Fix allow to click on homepage url. <a href='http://commits.kde.org/kmail-account-wizard/e515c709d06b245be30188840f517a659931af06'>Commit.</a> </li>
</ul>
<h3><a name='kompare' href='https://cgit.kde.org/kompare.git'>kompare</a> <a href='#kompare' onclick='toggle("ulkompare", this)'>[Hide]</a></h3>
<ul id='ulkompare' style='display: block'>
<li>Fix option for diff executable: allow plain names, use "" as default. <a href='http://commits.kde.org/kompare/557369b2addb839554dddaeec46fb36484753a60'>Commit.</a> </li>
</ul>
<h3><a name='konqueror' href='https://cgit.kde.org/konqueror.git'>konqueror</a> <a href='#konqueror' onclick='toggle("ulkonqueror", this)'>[Hide]</a></h3>
<ul id='ulkonqueror' style='display: block'>
<li>Fix compilation with Qt 5.13 (QByteArray/QString confusions). <a href='http://commits.kde.org/konqueror/211d5e1f7958e38b65af8260dfb8abcc4b422999'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Manual: Fix shortcut for View → Split View → Close Active. <a href='http://commits.kde.org/konsole/76e564468c50525528366662943456ae6125a8cb'>Commit.</a> </li>
<li>Std::stable_sort requires !(a < a). <a href='http://commits.kde.org/konsole/5d591a6a7d8d832a5d37e04eb9021abc67e9f53e'>Commit.</a> </li>
</ul>
<h3><a name='korganizer' href='https://cgit.kde.org/korganizer.git'>korganizer</a> <a href='#korganizer' onclick='toggle("ulkorganizer", this)'>[Hide]</a></h3>
<ul id='ulkorganizer' style='display: block'>
<li>Fix warning about:. <a href='http://commits.kde.org/korganizer/aa75a69c6e40a32a4faaff5eb2757289b79518f7'>Commit.</a> </li>
</ul>
<h3><a name='kpat' href='https://cgit.kde.org/kpat.git'>kpat</a> <a href='#kpat' onclick='toggle("ulkpat", this)'>[Hide]</a></h3>
<ul id='ulkpat' style='display: block'>
<li>Avoid tiny cards on high screen resolutions. <a href='http://commits.kde.org/kpat/e41c3cce214edb2223b5a8969a0db849952e6c22'>Commit.</a> </li>
<li>Compatibility with bh-solver 0.20.0. <a href='http://commits.kde.org/kpat/43fa807465ade38c2b8d845abfda92912625d692'>Commit.</a> </li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Hide]</a></h3>
<ul id='ulkpimtextedit' style='display: block'>
<li>Add missing emoticons. <a href='http://commits.kde.org/kpimtextedit/247e35f47b56a9cb7052905f2d90d40c01f821b1'>Commit.</a> </li>
</ul>
<h3><a name='krdc' href='https://cgit.kde.org/krdc.git'>krdc</a> <a href='#krdc' onclick='toggle("ulkrdc", this)'>[Hide]</a></h3>
<ul id='ulkrdc' style='display: block'>
<li>Do not assume framebuffer and scalines are always 32-bit aligned. <a href='http://commits.kde.org/krdc/8491d4320a13413f1b06a991c7c6728e617a1542'>Commit.</a> </li>
</ul>
<h3><a name='ksirk' href='https://cgit.kde.org/ksirk.git'>ksirk</a> <a href='#ksirk' onclick='toggle("ulksirk", this)'>[Hide]</a></h3>
<ul id='ulksirk' style='display: block'>
<li>KGameWindow::slotMovingArmiesArrived: Change delete to deleteLater. <a href='http://commits.kde.org/ksirk/b6320198cb9fcb80549116fad9e2f77a99f5d5f5'>Commit.</a> </li>
<li>No need to cast to CannonSprite, AnimSprite is enough. <a href='http://commits.kde.org/ksirk/5ba838f7d236ecc66e582e2498ee0576728e8da4'>Commit.</a> </li>
<li>Remove unused variables. <a href='http://commits.kde.org/ksirk/872e20245e75c8c62969089524bc2242cacaea42'>Commit.</a> </li>
<li>Mark functions as override. <a href='http://commits.kde.org/ksirk/3d2a7a5a04cab7ea0c317bdde37550c4fdf1fd61'>Commit.</a> </li>
</ul>
<h3><a name='kteatime' href='https://cgit.kde.org/kteatime.git'>kteatime</a> <a href='#kteatime' onclick='toggle("ulkteatime", this)'>[Hide]</a></h3>
<ul id='ulkteatime' style='display: block'>
<li>Add DesktopEntry to notifyrc. <a href='http://commits.kde.org/kteatime/da98e9a9b2f9f228f0f8554cb184dacda34bc3b4'>Commit.</a> </li>
</ul>
<h3><a name='ktouch' href='https://cgit.kde.org/ktouch.git'>ktouch</a> <a href='#ktouch' onclick='toggle("ulktouch", this)'>[Hide]</a></h3>
<ul id='ulktouch' style='display: block'>
<li>Make QML compilation optional. <a href='http://commits.kde.org/ktouch/a3f82fca1163b05e070a881c4deadb3385aa78bd'>Commit.</a> </li>
</ul>
<h3><a name='kubrick' href='https://cgit.kde.org/kubrick.git'>kubrick</a> <a href='#kubrick' onclick='toggle("ulkubrick", this)'>[Hide]</a></h3>
<ul id='ulkubrick' style='display: block'>
<li>Fix compile on arm. <a href='http://commits.kde.org/kubrick/2ab1d007cefb2870e871ac38c8999ee9f400edfb'>Commit.</a> </li>
</ul>
<h3><a name='kwordquiz' href='https://cgit.kde.org/kwordquiz.git'>kwordquiz</a> <a href='#kwordquiz' onclick='toggle("ulkwordquiz", this)'>[Hide]</a></h3>
<ul id='ulkwordquiz' style='display: block'>
<li>Check if the filename is empty when user wants to save the file. <a href='http://commits.kde.org/kwordquiz/f84f1d9ea31c95696a6714a82f6a4b259d5d1a87'>Commit.</a> </li>
</ul>
<h3><a name='libkcompactdisc' href='https://cgit.kde.org/libkcompactdisc.git'>libkcompactdisc</a> <a href='#libkcompactdisc' onclick='toggle("ullibkcompactdisc", this)'>[Hide]</a></h3>
<ul id='ullibkcompactdisc' style='display: block'>
<li>Add override markers to functions. <a href='http://commits.kde.org/libkcompactdisc/9197a8620a3145e22ab56bb4f4b8ccdeeb4cb41a'>Commit.</a> </li>
</ul>
<h3><a name='libkdepim' href='https://cgit.kde.org/libkdepim.git'>libkdepim</a> <a href='#libkdepim' onclick='toggle("ullibkdepim", this)'>[Hide]</a></h3>
<ul id='ullibkdepim' style='display: block'>
<li>Fix enum here. <a href='http://commits.kde.org/libkdepim/a46420ec2cb9bf42b80b0c3a80fe9f328724289b'>Commit.</a> </li>
<li>Don't use qAsConst over a temporary variable. <a href='http://commits.kde.org/libkdepim/baa5ce4260807a54e01b8c4551eb63b31ddc5924'>Commit.</a> </li>
</ul>
<h3><a name='libkexiv2' href='https://cgit.kde.org/libkexiv2.git'>libkexiv2</a> <a href='#libkexiv2' onclick='toggle("ullibkexiv2", this)'>[Hide]</a></h3>
<ul id='ullibkexiv2' style='display: block'>
<li>Use nullptr. <a href='http://commits.kde.org/libkexiv2/32bb63f7b74ce7ff089def5c720998642a7265ac'>Commit.</a> </li>
<li>Make gcc happy about indentation. <a href='http://commits.kde.org/libkexiv2/dd763e2f8650377c339e67d1e17b5b6b3f6d8a52'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Connect cursorPositionChange to the correct signal. <a href='http://commits.kde.org/lokalize/a0bab6e5b5652a000a98343bba6b2686e8a3419f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406742'>#406742</a></li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Improve scam detection. <a href='http://commits.kde.org/messagelib/a312c80947c4090b2f4d807333cf55dd5120281c'>Commit.</a> </li>
<li>Fix Bug 407086 - Scam detection is too sensitive for URLs that trivially differ and are not a scam. <a href='http://commits.kde.org/messagelib/a21c98334a709bd925faff75e7787a639d300fac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407086'>#407086</a></li>
<li>Remove debug. <a href='http://commits.kde.org/messagelib/d61b6e7792f50fc00f575dd3fd1a9ff1c8776cae'>Commit.</a> </li>
<li>Don't use qAsConst over a temporary variable. <a href='http://commits.kde.org/messagelib/5e29873768ce11f580ca45b31473adc67df6472d'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Fix build with Qt <5.10. <a href='http://commits.kde.org/okular/29ac9b2302d41d8226d2eb524946e07836239924'>Commit.</a> </li>
<li>Fix build with poppler <0.58. <a href='http://commits.kde.org/okular/e01329eb4760c23ad4a0e35e826e9b299a016d23'>Commit.</a> </li>
</ul>
<h3><a name='pim-sieve-editor' href='https://cgit.kde.org/pim-sieve-editor.git'>pim-sieve-editor</a> <a href='#pim-sieve-editor' onclick='toggle("ulpim-sieve-editor", this)'>[Hide]</a></h3>
<ul id='ulpim-sieve-editor' style='display: block'>
<li>Fix Bug 406902 - sieveeditor crash on start. <a href='http://commits.kde.org/pim-sieve-editor/b8fa64aba773fa2266036042b18e78e5d9e761bc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406902'>#406902</a></li>
</ul>
<h3><a name='pimcommon' href='https://cgit.kde.org/pimcommon.git'>pimcommon</a> <a href='#pimcommon' onclick='toggle("ulpimcommon", this)'>[Hide]</a></h3>
<ul id='ulpimcommon' style='display: block'>
<li>QUIET was not a good idea here. So we didn't inform that. <a href='http://commits.kde.org/pimcommon/854791960719ff86958f4bd9dc8b2e3e8b5e6cb2'>Commit.</a> See bug <a href='https://bugs.kde.org/403317'>#403317</a></li>
</ul>
<h3><a name='poxml' href='https://cgit.kde.org/poxml.git'>poxml</a> <a href='#poxml' onclick='toggle("ulpoxml", this)'>[Hide]</a></h3>
<ul id='ulpoxml' style='display: block'>
<li>Fix situations like "</itemizedlist>Some text". <a href='http://commits.kde.org/poxml/3085fdd98daf6b9c988b76497e7a4cc8c3d0ae15'>Commit.</a> </li>
<li>Mark functions with override. <a href='http://commits.kde.org/poxml/62e1e2d44e8fede67c8fba9aceec0624cc31a21f'>Commit.</a> </li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Hide]</a></h3>
<ul id='ulspectacle' style='display: block'>
<li>Respect the user-configured mimetype when dragging an image from the main window. <a href='http://commits.kde.org/spectacle/32058e28dda5fd06774666b3b8e4cfb7e57af689'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407165'>#407165</a></li>
<li>Set compression to 50 is the format is png. <a href='http://commits.kde.org/spectacle/879249b62570b3f6c5e9c4ca23bb4be0ec1a22c9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406263'>#406263</a></li>
<li>Add DesktopEntry to notifyrc. <a href='http://commits.kde.org/spectacle/54da90e7327b1596d48e6d89146ea9dbb75e63fa'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>File import: do not use overwritten temporary file. <a href='http://commits.kde.org/umbrello/04580cac31b1f1a1491a1fc54cfc4c724ce50cd9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406574'>#406574</a>. See bug <a href='https://bugs.kde.org/125102'>#125102</a></li>
</ul>
