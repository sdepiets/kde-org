<?php

  $release = '4.7';
  $release_full = '4.7.0';

  $page_title = "Updated KDE Applications Bring Many Exciting Features";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<p>
KDE is happy to announce the release of new versions of many of our most popular applications. From games through education and entertainment, these applications are more powerful, yet remain easy to use as they continue to mature in this version. Below are a few highlights from some of the updated applications released today.
</p>

<?php
centerThumbScreenshot("dolphin-gwenview.png", "KDE Applications 4.7");
?>

<p>

<strong>Kontact</strong>, KDE’s groupware solution, rejoins the rest of the KDE software for the 4.7 release. With most components now ported to Akonadi, there is increased stability, better connection to new services and sharing of communication information between more applications. The biggest change is the introduction of KMail 2. This has the familiar interface, while under the surface, all mail storage and retrieval has been ported to Akonadi.
</p><p>
<strong>Dolphin</strong>, KDE’s file manager, has a cleaner default appearance. The menu bar is hidden, but easy to reach and restore. The file searching interface has been improved. In addition, Dolphin now has much <a href="http://vishesh-yadav.com/blog/2011/07/03/mercurial-plugin-for-dolphin-work-progress-part-1-2/">deeper integration with source code management</a> systems, including the ability to create and clone repositories, push and pull changes, view diffs and much more. Dolphin and Konqueror both benefit from a new plugin that provides a rating and an annotation menu action for files and folders, leveraging Nepomuk capabilities.
</p>
<?php
centerThumbScreenshot("konqueror-dolphin.png", "Konqueror and Dolphin in 4.7");
?>
<p>
<strong>Marble</strong>, the virtual globe application from KDE Edu, has gained many improvements over the past six months. It now has voice navigation support, a map creation wizard, and new plug-ins. Following the Voice of Marble contest, voice navigation is now available in several languages, with voices provided by the KDE community. For more details, see <a href="http://edu.kde.org/marble/current_1.2.php">Marble's  visual changelog</a>.
</p>
<?php
centerThumbScreenshot("marble.png", "Marble shines in 4.7");
?>
<p>
Image management has become easier with <strong>Gwenview</strong>, the KDE image viewer, now offering the ability to compare two or more pictures side by side. From the browse mode, select two or more pictures, and then switch to the view mode or to the full-screen mode. In the view mode, you can add more pictures from the thumbnail bar.
<?php
centerThumbScreenshot("gwenview.png", "Comparing images in Gwenview 4.7");
?>

</p><p>
Comic fans will be pleased that Okular, the universal file viewer, gains support for reading a directory as a comicbook.
</p><p>
KStars, the essential KDE application for stargazers around the world, has gained a feature to predict a star hopping route and dynamic switching between its OpenGL and native rendering backends. Labels can now be assigned to points on celestial lines; comet trails are rendered in OpenGL mode.
Mathematicians and scientists can now explore higher order functions in Kalgebra and get information on oxidation states for elements in Kalzium.
</p><p>
For software developers, KDevelop has gained support for predefined indentation styles and a Python interpreter using Kross. Improved Python auto-completion and support for lex/yacc file extensions are also included in this release.
</p><p>
KDE’s Advanced Text Editor, Kate, shines with plugin improvements and stability. Detailed information is available on the <a href="http://kate-editor.org/2011/07/09/kate-in-kde-4-7/">Kate website</a>.
</p>

<h2>
DigiKam 2.0 brings face detection and recognition, image versioning support, geotagging and much more
</h2>
<p>
Today also features the release of <strong>DigiKam 2.0</strong>. The road to version 2 took more than a year of heavy development. The team proudly announces the first release of the new generation of DigiKam. This version features long awaited face detection and recognition, image versioning support, XMP metadata sidecar files support, big improvements in tagging and marking photos, reversed geotagging and many other improvements, including a total of 219 fixed bugs.
<!--
(screenshot of the new DigiKam here  http://farm7.static.flickr.com/6124/5964495890_7a5c6a5d2d_o_d.png )
-->
<?php
centerThumbScreenshot("digikam.png", "Digikam 2.0");
?>

</p><p>
Close companion Kipi-plugins is released along with DigiKam 2.0.0. This release features new export tools to three web services - Yandex.Fotki, MediaWiki and Rajce. The GPSSync plugin now has the ability to do reverse-geocoding. And as usual, lots of bugfixes.
The many other KDE Applications updated today also have new features and numerous bug fixes, and all benefit from the latest improvements in the KDE Frameworks.
</p>


<h4>Installing KDE Applications</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Announced Today:</h2>

<?php

include("trailer-plasma.inc");
include("trailer-platform.inc");

include("footer.inc");
?>
