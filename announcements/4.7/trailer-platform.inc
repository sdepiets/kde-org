
<h3>
<a href="platform.php">
Improved Multimedia, Instant Messaging and Semantic Capabilities in the KDE Platform
</a>
</h3>

<p>
<a href="platform.php">
<img src="images/platform.png" class="app-icon" alt="The KDE Development Platform 4.7.0"/>
</a>

A wide range of KDE and third party software will be able to take advantage of extensive work in Phonon and major improvements to the semantic desktop components, with enriched APIs and improved stability. The new KDE Telepathy framework offers integration of instant messaging directly into workspaces and applications. Performance and stability improvements in nearly all components lead to a smoother user experience and a reduced footprint of applications using the KDE Platform 4.7. For full details, read the <a href="platform.php">KDE Platform 4.7 release announcement</a>.
<br /><br />
<br /><br />

</p>
