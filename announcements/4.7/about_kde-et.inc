<?php 
    /*
    This file is for use in announcements. It contains the usual blurb "About KDE",
    along with trademark notices.
    */
?>

<h2>
  Teave KDE kohta
</h2>
<p align="justify">
KDE kujutab endast rahvusvahelist tehnoloogiameeskonda, kes loob vaba 
ja avatud lähtekoodiga tarkvara kasutamiseks nii lauaarvutites kui ka mobiilsetes 
seadmetes. KDE toodete hulka kuuluvad kaasaegne töökeskkond Linuxi ja UNIXi platvormile, 
täielikud kontoritöö ja grupitöö komplektid ning sajad rakendused ja programmid, 
sealhulgas interneti- ja veebi-, multimeedia-, meelelahutus-, õpi-, 
graafika- ja tarkvara arendamise rakendused. KDE tarkvara on tõlgitud enam 
kui 60 keelde ning selle loomisel on peetud silmas kasutamise lihtsust 
ja tänapäeva hõlbustusnõudeid. KDE4 rakendused töötavad raskusteta nii 
Linuxi, BSD, Solarise, Windowsi kui ka Mac OS X süsteemides.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Kaubamärkidest.</em>
  KDE<sup>&#174;</sup> ja K Desktop Environment<sup>&#174;</sup> logo on 
  KDE e.V. registreeritud kaubamärgid.

  Linux on Linus Torvaldsi registreeritud kaubamärk.

  UNIX on The Open Groupi registreeritud kaubamärk Ameerika Ühendriikides
  ja teistes riikides.

  Kõik teised selles teadaandes märgitud kaubamärgid ja autoriõigused kuuluvad
  vastavalt nende omanikele.
  </font>
</p>

<hr />
