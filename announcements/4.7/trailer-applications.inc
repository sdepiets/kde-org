
<h3>
<a href="applications.php">
Updated KDE Applications Bring Many Exciting Features
</a>
</h3>

<p>
<a href="applications.php">
<img src="images/applications.png" class="app-icon" alt="The KDE Applications 4.7"/>
</a>
Many KDE applications are updated. In particular, KDE's groupware solution, Kontact, rejoins the main KDE release cycle with all major components ported to Akonadi. The Digikam Software Collection, KDE's feature-rich photo management tools, come with a major new version. For full details, read the <a href="applications.php">KDE Applications 4.7 release announcement</a>.
<br /><br />
<br /><br />
</p>
