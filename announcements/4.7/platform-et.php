<?php

  $release = '4.7';
  $release_full = '4.7.0';
  $page_title = "KDE platvorm: täiustatud multimeedia- ja semantilised võimalused";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<p>
KDE annab uhkelt teada KDE platvormi 4.7 väljalaskmisest. Plasma töötsoonide ja KDE rakenduste alus on saanud paljudes valdkondades olulisi täiendusi. Lisaks uue tehnoloogia juurutamisele on nähtud palju vaeva alusstruktuuri jõudluse ja stabiilsuse parandamisel.
</p>
<?php
centerThumbScreenshot("systemsettings.png", "KDE platvorm 4.7");
?>

<p>
Meie meediaraamistiku Phonon uusim versioon pakub hulga uusi võimalusi, näiteks Zeitgeisti toetus. Samuti on vaeva nähtud taustaprogrammide kallal: VLC-põhist taustaprogrammi võib nüüd pidada stabiilseks ning see on eelistatud taustaprogramm mitmeplatvormses keskkonnas, GStreamerile põhinevat taustaprogrammi aga võib lugeda stabiilseks Linuxi platvormil. Xine taustaprogrammi enam ei arendata.
</p><p>
KDE platvormi semantilise töölaua komponentide funktsionaalsus ja stabiilsus on paranenud. Nepomuk elas üle võimsaid sisemisi muutusi, mille taga oli soov muuta see stabiilsemaks ja kiiremaks ning pakkuda rakendustele rikkalikumat API-t. Strigi analüsaatorid loevad nüüd metaandmeid omaette protsessis, millega leidis lahenduse üle 35 krahhiga seotud Dolphini ja Konquerori veateate.
</p><p>
KWin pakub nüüd rakenduste arendajatele võimalust lasta komposiit peatada, kui seda soovib täisekraanirežiimis rakendus. See peaks parandama OpenGL mängude ja GPU kiirendatud videotaasesituse jõudlust.
</p><p>
Kuvahaldur <strong>KDM</strong> toetab nüüd <a href="http://ksmanis.wordpress.com/2011/04/21/hello-planet-and-grub2-support-for-kdm/">GRUB2</a>. Kasutajad, kelle GRUB2 menüüs leidub mitmeid operatsioonisüsteeme, saavad nüüd seiskamisdialoogis valida, millises neist alglaadimine sooritada, hoides taaskäivitamise valikule klõpsates hiirenuppu pikemalt all.
<?php
centerThumbScreenshot("kdm.png", "KDM toetab GRUB2 alglaadurit");
?>

</p><p>
KDE süsteemset puhverserverit toetav <strong>KIO Proxy</strong> on saanud rohkelt parandusi ja uusi omadusi, sealhulgas on toetatud SOCKS-puhverserverid, võimalik on hankida mitme puhverserveri URL-aadress korraga ning toetatud on ka süsteemse puhverserveri teabe hankimine Windowsi ja Maci platvormil.
</p><p>
KwebkitPart, mis aitab lõimida WebKiti KDE rakendustesse, on saanud täiustusi reklaamide blokeerimise osas, mis muudab veebilehitsemise KDE WebKiti brauseriga veelgi meeldivamaks kogemuseks.
</p>



<h4>KDE arendusplatvormi paigaldamine</h4>
<?php
  include("boilerplate-et.inc");
?>

<h2>Täna ilmusid veel:</h2>
<?php

include("trailer-plasma-et.inc");
include("trailer-applications-et.inc");

include("footer.inc");
?>
