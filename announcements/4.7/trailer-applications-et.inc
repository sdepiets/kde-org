
<h3>
<a href="applications-et.php">
Uuendatud KDE rakendused pakuvad rohkelt vaimustavaid omadusi
</a>
</h3>

<p>
<a href="applications-et.php">
<img src="images/applications.png" class="app-icon" alt="KDE rakendused 4.7"/>
</a>
Paljud KDE rakendused läbisid uuenduskuuri. Eriti tasub välja tuua KDE grupitöölahenduse Kontact taasühinemist KDE reeglipärase väljalasketsükliga pärast seda, kui kõik selle tähtsamad komponendid porditi Akonadi peale. Samal ajal näeb ilmavalgust KDE võimalusterohke fotohalduri Digikam uus väljalase. Täpsemalt kõneleb kõigest <a href="applications-et.php">KDE rakenduste 4.7 väljalasketeade</a>.
<br /><br />
<br /><br />
</p>
