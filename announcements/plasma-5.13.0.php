<?php
	include_once ("functions.inc");
	$translation_file = "kde-org";
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "KDE Plasma 5.13: Fast, Lightweight and Full Featured.",
		'cssFile' => '/content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = 'plasma-5.13.0'; // for i18n
	$version = "5.13.0";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px; 
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}

figure {
    position: relative;
    z-index: 2;
    font-size: smaller;
    text-shadow: 2px 2px 5px light-grey;
}

</style>

<main class="releaseAnnouncment container">


	<h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Plasma %1", $version)?></h1>

	<?php include "./announce-i18n-bar.inc"; ?>

	<figure class="videoBlock">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/C2kR1_n_d-g?rel=0" allowfullscreen='true'></iframe>
	</figure>
	
	<figure class="topImage">
		<a href="plasma-5.13/plasma-5.13.png">
			<img src="https://www.kde.org/announcements/plasma-5.13/plasma-5.13-wee.png" width="600" height="338" alt="Plasma 5.13">
		</a>
		<figcaption><?php print i18n_var("KDE Plasma %1", "5.13")?></figcaption>
	</figure>

	<p>
		<?php i18n("Tuesday, 12 June 2018.")?>
		<?php print i18n_var("Today KDE unveils the %1 release of Plasma %2.", "final", "5.13.0");?>
	</p>

	<p>
		<?php print i18n_var("Members of the Plasma team have been working hard to continue making Plasma a lightweight and responsive desktop which loads and runs quickly, but remains full-featured with a polished look and feel.  We have spent the last four months optimising startup and minimising memory usage, yielding faster time-to-desktop, better runtime performance and less memory consumption.  Basic features like panel popups were optimised to make sure they run smoothly even on the lowest-end hardware.  Our design teams have not rested either, producing beautiful new integrated lock and login screen graphics.");?>
	</p>

<br clear="all" />

<p><?php i18n("Browse the full Plasma 5.13 changelog to find even more tweaks and bug fixes featured in this release: ");?><a href="plasma-5.12.5-5.13.0-changelog.php"><?php print i18n_var("Full Plasma %1 changelog", "5.13.0"); ?></a></p>

	<br clear="all" />

<h2><?php i18n("New in Plasma 5.13");?></h2>

<br clear="all" />
<h3><?php i18n("Plasma Browser Integration");?></h3>
<p><?php i18n("Plasma Browser Integration is a suite of new features which make Firefox, Chrome and Chromium-based browsers work with your desktop.  Downloads are now displayed in the Plasma notification popup just as when transferring files with Dolphin.  The Media Controls Plasmoid can mute and skip videos and music playing from within the browser.  You can send a link to your phone with KDE Connect.  Browser tabs can be opened directly using KRunner via the Alt-Space keyboard shortcut.  To enable Plasma Browser Integration, add the relevant plugin from the addon store of your favourite browser.");?></p>
<figure style="text-align: right">
<a href="plasma-5.13/pbi-download-integration.png">
<img src="plasma-5.13/pbi-download-integration-wee.png" style="border: 0px" width="350" height="233" alt="<?php i18n("Plasma Browser Integration for Downloads");?>" />
</a>&nbsp;&nbsp;&nbsp;
<a href="plasma-5.13/pbi-video-controls.png">
<img src="plasma-5.13/pbi-video-controls-wee.png" style="border: 0px" width="276" height="233" alt="<?php i18n("Plasma Browser Integration for Media Controls");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Plasma Browser Integration for Downloads and Media Controls");?></figcaption>
</figure>

<br clear="all" />
<h3><?php i18n("System Settings Redesigns");?></h3>
<p><?php i18n("Our settings pages are being redesigned.  The KDE Visual Design Group has reviewed many of the tools in System Settings and we are now implementing those redesigns.  KDE's Kirigami framework gives the pages a slick new look.  We started off with the theming tools, comprising the icons, desktop themes, and cursor themes pages.  The splash screen page can now download new splashscreens from the KDE Store.  The fonts page can now display previews for the sub-pixel anti-aliasing settings.");?></p>
<figure style="text-align: right">
<a href="plasma-5.13/kcm-desktop-theme.png">
<img src="plasma-5.13/kcm-desktop-theme-wee.png" style="border: 0px" width="350" height="252" alt="<?php i18n("Desktop Theme");?>" />
</a>
<a href="plasma-5.13/kcm-fonts-hint-preview.png">
<img src="plasma-5.13/kcm-fonts-hint-preview-wee.png" style="border: 0px" width="350" height="252" alt="<?php i18n("Font Settings");?>" />
</a>
<a href="plasma-5.13/kcm-icons.png">
<img src="plasma-5.13/kcm-icons-wee.png" style="border: 0px" width="350" height="252" alt="<?php i18n("Icon Themes");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Redesigned System Settings Pages");?></figcaption>
</figure>

<br clear="all" />
<h3><?php i18n("New Look for Lock and Login Screens");?></h3>
<p><?php i18n("Our login and lock screens have a fresh new design, displaying the wallpaper of the current Plasma release by default. The lock screen now incorporates a slick fade-to-blur transition to show the controls, allowing it to be easily used like a screensaver.");?></p>
<figure style="text-align: right">
<a href="plasma-5.13/lockscreen.png">
<img src="plasma-5.13/lockscreen-wee.png" style="border: 0px" width="350" height="197" alt="<?php i18n("Lock Screen");?>" />
</a>
<a href="plasma-5.13/login.png">
<img src="plasma-5.13/login-wee.png" style="border: 0px" width="350" height="172" alt="<?php i18n("Login Screen");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Lock and Login Screen new Look");?></figcaption>
</figure>

<br clear="all" />
<figure style="float: right; text-align: right">
<a href="plasma-5.13/kwin-blur-dash.png">
<img src="plasma-5.13/kwin-blur-dash-wee.png" style="border: 0px" width="350" height="219" alt="<?php i18n("Improved Blur Effect in the Dash Menu");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Improved Blur Effect in the Dash Menu");?></figcaption>
</figure>
<h3><?php i18n("Graphics Compositor");?></h3>
<p><?php i18n("Our compositor KWin gained much-improved effects for blur and desktop switching. Wayland work continued, with the return of window rules, the use of high priority EGL Contexts, and initial support for screencasts and desktop sharing.");?></p>

<br clear="all" />
<figure style="float: right; text-align: right">
<a href="plasma-5.13/discover.png">
<img src="plasma-5.13/discover-wee.png" style="border: 0px" width="350" height="327" alt="<?php i18n("Discover's Lists with Ratings, Themed Icons, and Sorting Options");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Discover's Lists with Ratings, Themed Icons, and Sorting Options");?></figcaption>
</figure>
<h3 id="discover"><?php i18n("Discover");?></h3>
<p><?php i18n("Discover, our software and addon installer, has more features and sports improvements to the look and feel.");?></p>

<p><?php i18n("Using our Kirigami UI framework we improved the appearance of lists and category pages, which now use toolbars instead of big banner images.  Lists can now be sorted, and use the new Kirigami Cards widget.  Star ratings are shown on lists and app pages.  App icons use your local icon theme to better match your desktop settings.  All AppStream metadata is now shown on the application page, including all URL types.");?></p>

<p><?php i18n("Work has continued on bundled app formats.  Snap support now allows user control of app permissions, and it's possible to install Snaps that use classic mode.  And the 'snap://' URL format is now supported.  Flatpak support gains the ability to choose the preferred repository to install from when more than one is set up.");?></p>

<br clear="all" />
<h3><?php i18n("Much More");?></h3>
<p><?php i18n("Other changes include:");?></p>
<ul>
<li><?php i18n("A tech preview of <a href='http://blog.broulik.de/2018/03/gtk-global-menu/'>GTK global menu integration</a>.");?></li>
<li><?php i18n("Redesigned Media Player Widget.");?></li>
<li><?php i18n("Plasma Calendar plugin for astronomical events, currently showing: lunar phases & astronomical seasons (equinox, solstices).");?></li>
<li><?php i18n("xdg-desktop-portal-kde, used to give desktop integration for Flatpak and Snap applications, gained support for screenshot and screencast portals.");?></li>
<li><?php i18n("The Digital Clock widget allows copying the current date and time to the clipboard.");?></li>
<li><?php i18n("The notification popup has a button to clear the history.");?></li>
<li><?php i18n("More KRunner plugins to provide easy access to Konsole profiles and the character picker.");?></li>
<li><?php i18n("The Mouse System Settings page has been rewritten for libinput support on X and Wayland.");?></li>
<li><?php i18n("Plasma Vault has a new CryFS backend, commands to remotely close open vaults with KDE Connect, offline vaults, a more polished interface and better error reporting.");?></li>
<li><?php i18n("A new dialog pops up when you first plug in an external monitor so you can easily configure how it should be positioned.");?></li>
<li><?php i18n("Popups in panel open faster due to a new preloading mechanism.");?></li>
<li><?php i18n("Plasma gained the ability to fall back to software rendering if OpenGL drivers unexpectedly fail.");?></li>
</ul><br />
<figure style="text-align: right">
<a href="plasma-5.13/gedit-global-menu.png">
<img src="plasma-5.13/gedit-global-menu-wee.png" style="border: 0px" width="349" height="250" alt="<?php i18n("GEdit with Title Bar Menu");?>" />
</a>&nbsp;&nbsp;
<a href="plasma-5.13/media-plasmoid.png">
<img src="plasma-5.13/media-plasmoid-wee.png" style="border: 0px" width="260" height="250" alt="<?php i18n("Redesigned Media Player Widget");?>" />
</a>&nbsp;&nbsp;
<a href="plasma-5.13/connect-external-monitor.png">
<img src="plasma-5.13/connect-external-monitor-wee.png" style="border: 0px" width="444" height="250" alt="<?php i18n("Connect an External Monitor");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("GEdit with Title Bar Menu. Redesigned Media Player Widget.  Connect an External Monitor Dialog.");?></figcaption>
</figure>



<br clear="all" />
	<!-- // Boilerplate again -->
	<section class="row get-it">
		<article class="col-md">
			<h2><?php i18n("Live Images");?></h2>
			<p>
				<?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
			</p>
			<a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
			<a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
		</article>

		<article class="col-md">
			<h2><?php i18n("Package Downloads");?></h2>
			<p>
				<?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
			</p>
			<a href='https://community.kde.org/Plasma/Packages' class="learn-more"><?php i18n("Package download wiki page");?></a>
		</article>

		<article class="col-md">
			<h2><?php i18n("Source Downloads");?></h2>
			<p>
				<?php i18n("You can install Plasma 5 directly from source.");?>
			</p>
			<a href='http://community.kde.org/Frameworks/Building'><?php i18n("Community instructions to compile it");?></a>
			<a href='../info/plasma-5.13.0.php' class='learn-more'><?php i18n("Source Info Page");?></a>
		</article>
	</section>

	<section class="give-feedback">
		<h2><?php i18n("Feedback");?></h2>

		<p>
			<?php print i18n_var("You can give us feedback and get updates on <a href='%1'><img src='%2' /></a> <a href='%3'>Facebook</a>
			or <a href='%4'><img src='%5' /></a> <a href='%6'>Twitter</a>
			or <a href='%7'><img src='%8' /></a> <a href='%9'>Google+</a>.", "https://www.facebook.com/kde", "https://www.kde.org/announcements/facebook.gif", "https://www.facebook.com/kde", "https://twitter.com/kdecommunity", "https://www.kde.org/announcements/twitter.png", "https://twitter.com/kdecommunity", "https://plus.google.com/105126786256705328374/posts", "https://www.kde.org/announcements/googleplus.png", "https://plus.google.com/105126786256705328374/posts"); ?>
		</p>
		<p>
			<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
		</p>

		<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

		<p><?php i18n("Your feedback is greatly appreciated.");?></p>
	</section>

	<h2>
		<?php i18n("Supporting KDE");?>
	</h2>

	<p align="justify">
		<?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='%3'>Join the Game</a> initiative.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/civicrm/contribute/transact?id=5"); ?>
	</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
