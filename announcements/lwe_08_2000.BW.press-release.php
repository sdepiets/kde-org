<?php
  $page_title = "KDE Desktop Is Show Favorite at LinuxWorld Expo";
  $site_root = "../";
  include "header.inc";
?>

<P>DATELINE AUGUST 22, 2000</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">KDE Desktop Is Show Favorite at LinuxWorld Expo</H3>
<P><STRONG>GNU/Linux Users Speak:  KDE Is the Best Desktop for GNU/Linux<SUP>&reg;</SUP></STRONG></P>
<P>August 22, 2000 (The INTERNET).  <A href="/">KDE</A>,
the most advanced and user-friendly desktop for GNU/Linux - UNIX<SUP>&reg;</SUP>
operating systems, was honored as
<A HREF="http://www.kde.org/img/awards/LWE-award_08_2000.png">Show Favorite</A>
in the Desktop category at the LinuxWorld Expo, held in San Jose,
California last week.</P>

<P>KDE developers and supporters were delighted KDE was selected the best
desktop by GNU/Linux users.
"KDE development has always been dedicated to meeting the needs of
users, and this award from GNU/Linux users proves this dedication is
appreciated," said Dirk Hohndel, Vice President of XFree86, Inc. and
CTO of SuSE Linux AG.  "We believe that KDE and KOffice form an
unparalleled next-generation GNU/Linux desktop and will pave the way to the
widespread acceptance of GNU/Linux for desktop users."
</P>
<P>"And the best is yet to come", added Matthias Ettrich, founder of
the KDE project.  "Tomorrow we will
<A HREF="http://www.kde.org/announcements/announce-1.93.html">announce</A> the
availability of the fourth beta release for KDE 2.  KDE 2, scheduled for
final release in mid-fourth quarter, is a
next-generation, component-based desktop which will set new standards of
quality and ease of use for a UNIX desktop. It will also provide an
advanced, developer-friendly platform for both free and proprietary software."
</P>
<P>
This award reaffirms that KDE is the desktop of choice for GNU/Linux users.
In two surveys
(<A HREF="http://www.borland.com/linux/jul99survey/linuxprimary.html">1</A>,
<A HREF="http://www.borland.com/linux/jul99survey/windowsprimary.html">2</A>)
conducted</A> by <A HREF="http://www.borland.com/">Borland</A> last year,
over half of the respondents indicated they run KDE as their desktop
environment.  Moreover, KDE has earned many other awards, including:</P>
<UL>
<LI>"<A HREF="http://www.zdnet.de/news/artikel/1999/03/19011-wf.htm">Innovation
of the Year 1998/99</A>" in the category "Software" at
<A HREF="http://www2.cebit.de/">CeBIT</A>, the world's largest computer trade
fair;</LI>
<LI>"Readers' Choice awards for 1999"
for best "Window Manager"
from the <A HREF="http://www.linuxjournal.com/">Linux Journal</A>;</LI>
<LI>"<A HREF="http://www.linuxworld.com/linuxworld/lw-1999-08/lw-08-penguin_1-2.html">Editor's
Choice Award</A>" in the "Desktop Environment" category
from <A HREF="http://www.linuxworld.com/">LinuxWorld</A>;</LI>
<LI>"<A HREF="http://www.linuxgazette.com/issue35/richardson.html">Editor's
Choice Awards</A>" for "Most Promising Software Newcomers"
from the <A HREF="http://www.linuxgazette.com/">Linux Gazette</A>; and</LI>
<LI>"Best Of the Net" award for two consecutive years from
<A HREF="http://linux.miningco.com/">The Mining Company</A>.</LI>
</UL>
</P>
<!--  <P>
The availability of a fast, stable, easy-to-use, component-based desktop
like KDE 2 is considered a key to the success of UNIX-based systems on the
corporate and home desktops.
</P> -->
<BR CLEAR="all">
<H4>About KDE</H4>
<P>
KDE's major contributions to GNU/Linux - UNIX are ease of installation,
configuration, use and development. KDE provides users of all nationalities
with an attractive, functional and intuitive desktop based on
reusable components and network transparency.  It also provides a framework for
applications that provide a consistent, yet highly customizable, look-and-feel.
<!-- KDE also offers
a consistent user interface across all UNIX systems and numerous hardware
platforms, from PCs to powerful Internet servers, thereby permitting
organizations to freely switch hardware without incurring the costs
associated with switching operating systems.
</P>
<P> -->
For the developer, KDE also offers the best Open Source development platform
featuring an assortment of superb Open Source, free development tools such as
<A HREF="http://www.kdevelop.org/">KDevelop</A>,
<A HREF="http://www.thekompany.com/projects/kdestudio/index.php3?dhtml_ok=0">KDE Studio</A>,
and <A HREF="http://www.trolltech.com/">Trolltech's</A>
<A HREF="http://www.trolltech.com/products/qt/designer/sshots.html">Qt
Designer</A>, and commercial ones
such as <A HREF="http://www.borland.com/">Borland's</A> Kylix, the
Linux version of <A HREF="http://www.borland.com/delphi/">Delphi</A>.
</P>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide.  Currently development is focused on KDE 2, which will for
the first time offer a fully object-oriented, component-based desktop and
office suite.  KDE 2 promises to make the GNU/Linux desktop as
easy to use as the popular commercial desktops while staying true
to open standards and empowering developers and users with quality
Open Source software. </P>
<P>For more information about KDE, please visit KDE's <A HREF="http://www.kde.org/whatiskde/">web site</A>.</P>
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
&#103;&#x72;&#97;&#x6e;&#114;ot&#104;&#x40;k&#x64;&#0101;&#x2e;org<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
po&#x75;r&#x40;k&#x64;e&#46;&#x6f;&#114;g<BR>
(1) 718-456-1165
</TD></TR>
<!--- <TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
f&#x61;ur&#101;&#x40;&#107;de.&#x6f;r&#00103;<BR>
(44) 1225 837 409
</TD></TR>-->
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
k&#00111;n&#x6f;&#x6c;d&#0064;kd&#x65;&#46;&#x6f;&#114;&#103;<BR>
(49) 179 2252249
</TD></TR>
</TABLE>
<?php include "footer.inc" ?>
