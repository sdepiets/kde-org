<?php
  $page_title = "Server downtime for Cano";
  $site_root = "../";
  include "header.inc";
?>
<p>
To allow for the server which hosts many of our dynamic websites to be moved from one data center to another,
downtime has been scheduled. This will happen on Saturday 18th August 2012, starting around 07:00 UTC.
If all goes well, it will last up to 12 hours (until 19:00 UTC).
</p>
<p>
Please note that the move may complete quicker than that, and due to the use of Incapsula,
cached versions of some content may still be available.
Affected websites include:
<ul>
	<li>dot.kde.org</li>
	<li>forum.kde.org</li>
	<li>userbase.kde.org</li>
	<li>techbase.kde.org</li>
	<li>community.kde.org</li>
	<li>blogs.kde.org</li>
	<li>nepomuk.kde.org</li>
	<li>k3b.kde.org</li>
	<li>akademy.kde.org</li>
	<li>wiki.akademy.kde.org</li>
	<li>www.calligra.org</li>
	<li>www.behindkde.org</li>
	<li>www.desktopsummit.org</li>
	<li>wiki.desktopsummit.org</li>
	<li>kde.in</li>
	<li>ir.kde.org</li>
	<li>br.kde.org</li>
</ul>
</p>

<?php

  include("footer.inc");
?>
