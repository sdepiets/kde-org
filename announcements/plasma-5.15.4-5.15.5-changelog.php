<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.15.5 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.15.5";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Fix build with Qt 4. <a href='https://commits.kde.org/breeze/386d3b8ed1e0595c9fc6e21643ff748402171429'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20201'>D20201</a></li>
</ul>


<h3><a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a> </h3>
<ul id='ulbreeze-gtk' style='display: block'>
<li>Set the default cursor theme to breeze_cursors. <a href='https://commits.kde.org/breeze-gtk/b00e12ff39264fd8c2fb8af4168e998726766dfa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17187'>D17187</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Flatpak: Improve matching of remotes. <a href='https://commits.kde.org/discover/f96849d3e1633550fa99d2dfd860f41072ce35b0'>Commit.</a> </li>
<li>Flatpak: remove unnecessary complexity. <a href='https://commits.kde.org/discover/c04b9dd044f7720d3fe50f43d2034fdb1d5353bb'>Commit.</a> </li>
<li>Flatpak: make sure we don't consider appstream fetched too early. <a href='https://commits.kde.org/discover/1ebbfca3e10df37bd8f4cc409dcc0434ace809c5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406923'>#406923</a></li>
<li>Update copyright year. <a href='https://commits.kde.org/discover/0fca27949ed092fa374fe74b8452a62f7dc2a597'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[weather] Fix default visibility unit for non-metric locales. <a href='https://commits.kde.org/kdeplasma-addons/c06e11928b4c70fe3d124ec247ef8b08bd441a86'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Fix captions with non-BMP characters. <a href='https://commits.kde.org/kwin/57440d1d6b490cdad51266977d0269a08918b82f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376813'>#376813</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19052'>D19052</a></li>
<li>[libkwineffects] Use fully qualified type name for signal. <a href='https://commits.kde.org/kwin/97c3cb73ffdc89ffd50a3b5e166cf62f3c8eaf85'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20733'>D20733</a></li>
<li>[effects/fallapart] Don't animate popups. <a href='https://commits.kde.org/kwin/0a8c436f7f055ff27cda99d757cbd135cc3a6494'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20568'>D20568</a></li>
<li>Fix plugins/qpa build with Qt 5.13. <a href='https://commits.kde.org/kwin/6e1655e3cce082b2e7d0daf21dc58bbe3d4524f2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406056'>#406056</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20239'>D20239</a></li>
<li>Also unset QT_SCALE_FACTOR. <a href='https://commits.kde.org/kwin/79ce5dac9a2af0823c16f80dfff19ef3494d08c3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406195'>#406195</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20234'>D20234</a></li>
</ul>


<h3><a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a> </h3>
<ul id='ulplasma-browser-integration' style='display: block'>
<li>Fix crash when mutating a container while iterating it. <a href='https://commits.kde.org/plasma-browser-integration/f597f7f4865fa4461ce1a85d89a0fa7d40cf2e15'>Commit.</a> </li>
<li>Task Manager media controls now work better with Vivaldi browser. <a href='https://commits.kde.org/plasma-browser-integration/04d079f578586fcf94be94916049c92bdb24b0ea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20803'>D20803</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[WidgetExplorer] Fix blurry previews. <a href='https://commits.kde.org/plasma-desktop/29f0bea8988933bfc27bef56fb17c3ab61d80d56'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21030'>D21030</a></li>
<li>Allow folder view elements to be be dropped using other Action than Copy. <a href='https://commits.kde.org/plasma-desktop/b753301b74ac5f3505fcc29534543baf8e91908f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20863'>D20863</a></li>
<li>[Widget Explorer] Continue using a trash can icon for deleting delegates. <a href='https://commits.kde.org/plasma-desktop/7cb8db2d1b3071b65ed3b797708f2d8f9707ce1a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20638'>D20638</a></li>
<li>[kcolorschemeeditor] Use config path instead of searching again. <a href='https://commits.kde.org/plasma-desktop/c0b1a7e16e69faf74a03156e404a40e1de8feaee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399951'>#399951</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20213'>D20213</a></li>
<li>[Folder View] use Math.floor() instead of Math.round(). <a href='https://commits.kde.org/plasma-desktop/b5d0043c33db8e6309996f1a89cfbe05b91566f8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20420'>D20420</a></li>
<li>[Folder View] Improve label crispness. <a href='https://commits.kde.org/plasma-desktop/05e59e1c77c4a83590b0cd906ecfb698ae5ca3b4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20407'>D20407</a></li>
<li>[Folder View] Use a more reasonable minimum cell width. <a href='https://commits.kde.org/plasma-desktop/1c2dd97e8cd493d7cb92a3e398cc68317e01a996'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403412'>#403412</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20368'>D20368</a></li>
<li>[Folder View] Use a more reasonable minimum cell width. <a href='https://commits.kde.org/plasma-desktop/a86211b7416bc9a89031549c522604873bc814ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20368'>D20368</a></li>
<li>[Kickoff] Return Kickoff to Favorites page after running a search. <a href='https://commits.kde.org/plasma-desktop/82b904bce54b971f1c74133fcfba23cdc2b914b3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18848'>D18848</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Reject invalid keys. <a href='https://commits.kde.org/plasma-nm/257b03b185d30312a8c3308092f77ca7a6b9c84d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20900'>D20900</a></li>
<li>Reset model when there are new available connections. <a href='https://commits.kde.org/plasma-nm/4cb1e69c1a615523d82f7986b8437906017a8d2d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406765'>#406765</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20788'>D20788</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>[Context] Update StreamRestore before inserting it. <a href='https://commits.kde.org/plasma-pa/8f3060794cab4f244a23161274578587b01dea8a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20451'>D20451</a></li>
<li>[StreamRestore] Don't mess with channels. <a href='https://commits.kde.org/plasma-pa/fbdef07f554bbec59d6142837fdf44afd103f6dc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398392'>#398392</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20450'>D20450</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[Widget Explorer] Continue using a trash can icon for "Uninstallable" category. <a href='https://commits.kde.org/plasma-workspace/3cbd317a3d38a66b9948cc14ac27fa45ba567f56'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20637'>D20637</a></li>
<li>Show accurate checked state for system monitor configuration pages. <a href='https://commits.kde.org/plasma-workspace/83045375ec2bd0fd01f6bb71dfca380baa754fc7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17796'>D17796</a></li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Fix invisible monochrome icons in Icon View tooltips too. <a href='https://commits.kde.org/systemsettings/8b2e8d7eb61a82b61df40489eb09ce297a21c3eb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386748'>#386748</a></li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>Destroy gbm_device only when it's initialized. <a href='https://commits.kde.org/xdg-desktop-portal-kde/29609c3eb0a40c37a610f68ec9c9b9d9913b555f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407025'>#407025</a></li>
<li>Settings portal: do not return QDBusVariant directly when calling Read() method. <a href='https://commits.kde.org/xdg-desktop-portal-kde/045302b5292cb62da5843906825c04898d6656d7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406672'>#406672</a></li>
<li>Remove additional call of items initialization. <a href='https://commits.kde.org/xdg-desktop-portal-kde/42626859eabef7c94e23db3481343d11899210da'>Commit.</a> </li>
<li>AppChooser dialog: async loading of items. <a href='https://commits.kde.org/xdg-desktop-portal-kde/73b2cd9327a9b0786bcca8af0d84b28483e0562c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20461'>D20461</a></li>
<li>Select application with a single click. <a href='https://commits.kde.org/xdg-desktop-portal-kde/d15fc4aeb8b0e92f9a249ea2d074f349f57f6c0e'>Commit.</a> </li>
<li>AppChooserDialog: make it more usable. <a href='https://commits.kde.org/xdg-desktop-portal-kde/5ddeff154725a4d2eb5774d2566d3e468282be4d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20403'>D20403</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
