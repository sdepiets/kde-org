<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.49.0");
  $site_root = "../";
  $release = '5.49.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
August 11, 2018. KDE today announces the release
of KDE Frameworks 5.49.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Don't instantiate a QStringRef into a QString only to search in a QStringList");?></li>
<li><?php i18n("Define elements when they're declared");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("[tags_kio] Fix multiple filename copies");?></li>
<li><?php i18n("Revert \"[tags_kio] Use UDS_URL instead of UDS_TARGET_URL.\"");?></li>
<li><?php i18n("[tags_kio] Use UDS_URL instead of UDS_TARGET_URL");?></li>
<li><?php i18n("[tags_kio] Query target filepaths instead of appending paths to the file UDS entry");?></li>
<li><?php i18n("Support special URLs for finding files of a certain type");?></li>
<li><?php i18n("Avoid manipulation of lists with quadratic complexity");?></li>
<li><?php i18n("Use non deprecated fastInsert in baloo");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add <code>drive-optical</code> icon (bug 396432)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Android: Don't hardcode a random version of the Android SDK");?></li>
<li><?php i18n("ECMOptionalAddSubdirectory: Provide a bit more detail");?></li>
<li><?php i18n("Fix variable definition check");?></li>
<li><?php i18n("Change the 'since' version");?></li>
<li><?php i18n("Improve ECMAddAppIconMacro");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Avoid warnings for PolkitQt5-1 headers");?></li>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Fix overflow in rounding code (bug 397008)");?></li>
<li><?php i18n("API dox: remove not-to-be-there \":\"s behind \"@note\"");?></li>
<li><?php i18n("API dox: talk about nullptr, not 0");?></li>
<li><?php i18n("KFormat: Replace unicode literal with unicode codepoint to fix MSVC build");?></li>
<li><?php i18n("KFormat: correct @since tag for new KFormat::formatValue");?></li>
<li><?php i18n("KFormat: Allow usage of quantities beyond bytes and seconds");?></li>
<li><?php i18n("Correct KFormat::formatBytes examples");?></li>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Don't block forever in ensureKdeinitRunning");?></li>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("ensure we are always writing in the engine's root context");?></li>
<li><?php i18n("better readability");?></li>
<li><?php i18n("Improve API docs a bit");?></li>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Fix qtplugins in KStandardDirs");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KEmoticons");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("API dox: add @file to functions-only header to have doxygen cover those");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KDE GUI Addons");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Install the sunrise/sunset computation header");?></li>
<li><?php i18n("Added leap year day as (cultural) holiday for Norway");?></li>
<li><?php i18n("Added ‘name’ entry for Norwegian holiday files");?></li>
<li><?php i18n("Added descriptions for Norwegian holiday files");?></li>
<li><?php i18n("more Japanese holiday updates from phanect");?></li>
<li><?php i18n("holiday_jp_ja, holiday_jp-en_us -  updated (bug 365241)");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Reuse function that already does the same");?></li>
<li><?php i18n("Fix the catalog handling and locale detection on Android");?></li>
<li><?php i18n("Readability, skip no-op statements");?></li>
<li><?php i18n("Fix KCatalog::translate when translation is same as original text");?></li>
<li><?php i18n("a file has been renamed");?></li>
<li><?php i18n("Let ki18n macro file name follow style of other find_package related files");?></li>
<li><?php i18n("Fix the configure check for _nl_msg_cat_cntr");?></li>
<li><?php i18n("Don't generate files in the source directory");?></li>
<li><?php i18n("libintl: Determine if _nl_msg_cat_cntr exists before use (bug 365917)");?></li>
<li><?php i18n("Fix the binary-factory builds");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Install kio related kdebugsettings category file");?></li>
<li><?php i18n("rename private header to _p.h");?></li>
<li><?php i18n("Remove custom icon selection for trash (bug 391200)");?></li>
<li><?php i18n("Top-align labels in properties dialog");?></li>
<li><?php i18n("Present error dialog when user tries to create directory named \".\" or \"..\" (bug 387449)");?></li>
<li><?php i18n("API dox: talk about nullptr, not 0");?></li>
<li><?php i18n("kcoredirlister lstItems benchmark");?></li>
<li><?php i18n("[KSambaShare] Check file that's changed before reloading");?></li>
<li><?php i18n("[KDirOperator] Use alternating background colors for multi-column views");?></li>
<li><?php i18n("avoid memory leak in slave jobs (bug 396651)");?></li>
<li><?php i18n("SlaveInterface: deprecate setConnection/connection, nobody can use them anyway");?></li>
<li><?php i18n("Slightly faster UDS constructor");?></li>
<li><?php i18n("[KFilePlacesModel] Support pretty baloosearch URLs");?></li>
<li><?php i18n("Remove projects.kde.org web shortcut");?></li>
<li><?php i18n("Switch KIO::convertSize() to KFormat::formatByteSize()");?></li>
<li><?php i18n("Use non deprecated fastInsert in file.cpp (first of many to come)");?></li>
<li><?php i18n("Replace Gitorious web shortcut by GitLab");?></li>
<li><?php i18n("Don't show confirmation dialog for Trash action by default (bug 385492)");?></li>
<li><?php i18n("Don't ask for passwords in kfilewidgettest");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("support dynamically adding and removing title (bug 396417)");?></li>
<li><?php i18n("introduce actionsVisible (bug 396413)");?></li>
<li><?php i18n("adapt margins when scrollbar appears/disappear");?></li>
<li><?php i18n("better management of the size (bug 396983)");?></li>
<li><?php i18n("Optimise setting up the palette");?></li>
<li><?php i18n("AbstractApplciationItem shouldn't have its own size, only implicit");?></li>
<li><?php i18n("new signals pagePushed/pageRemoved");?></li>
<li><?php i18n("fix logic");?></li>
<li><?php i18n("add ScenePosition element (bug 396877)");?></li>
<li><?php i18n("No need to emit the intermediary palette for every state");?></li>
<li><?php i18n("hide-&gt;show");?></li>
<li><?php i18n("Collapsible Sidebar Mode");?></li>
<li><?php i18n("kirigami_package_breeze_icons: don't treat lists as elements (bug 396626)");?></li>
<li><?php i18n("fix search/replace regexp (bug 396294)");?></li>
<li><?php i18n("animating a color produces a rather unpleasant effect (bug 389534)");?></li>
<li><?php i18n("color focused item for keyboard navigation");?></li>
<li><?php i18n("remove quit shortcut");?></li>
<li><?php i18n("Remove long-time deprecated Encoding=UTF-8 from desktop format file");?></li>
<li><?php i18n("fix toolbar size (bug 396521)");?></li>
<li><?php i18n("fix handle sizing");?></li>
<li><?php i18n("Honor BUILD_TESTING");?></li>
<li><?php i18n("Show icons for actions that have an icon source rather than an icon name");?></li>
</ul>

<h3><?php i18n("KItemViews");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KMediaPlayer");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Remove long-time deprecated Encoding=UTF-8 from desktop format files");?></li>
<li><?php i18n("Change default sort order in the download dialog to Rating");?></li>
<li><?php i18n("Fix DownloadDialog window margins to meet general theme margins");?></li>
<li><?php i18n("Restore accidentally removed qCDebug");?></li>
<li><?php i18n("Use the right QSharedPointer API");?></li>
<li><?php i18n("Handle empty preview lists");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("API dox: talk about nullptr, not 0");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KPlotting");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KPty");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("API dox: talk about nullptr, not 0");?></li>
<li><?php i18n("Require out-of-source build");?></li>
<li><?php i18n("Add subseq operator to match sub-sequences");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("proper fix for the raw string indenting auto-quoting");?></li>
<li><?php i18n("fix indenter to cope with new syntax file in syntaxhighlighting framework");?></li>
<li><?php i18n("adjust test to new state in syntax-highlighting repository");?></li>
<li><?php i18n("Show \"Search wrapped\" message in center of view for better visibility");?></li>
<li><?php i18n("fix warning, just use isNull()");?></li>
<li><?php i18n("Extend Scripting API");?></li>
<li><?php i18n("fix segfault on rare cases where empty vector occurs for word count");?></li>
<li><?php i18n("enforce clear of scrollbar preview on document clear (bug 374630)");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("API docs: partial revert of earlier commit, didn't really work");?></li>
<li><?php i18n("KFindDialog: give the lineedit focus when showing a reused dialog");?></li>
<li><?php i18n("KFind: reset count when changing the pattern (e.g. in the find dialog)");?></li>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Cleanup RemoteAccess buffers on aboutToBeUnbound instead of object destruction");?></li>
<li><?php i18n("Support cursor hints on locked pointer");?></li>
<li><?php i18n("Reduce unnecessary long wait times on failing signal spies");?></li>
<li><?php i18n("Fix selection and seat auto tests");?></li>
<li><?php i18n("Replace remaining V5 compat global includes");?></li>
<li><?php i18n("Add XDG WM Base support to our XDGShell API");?></li>
<li><?php i18n("Make XDGShellV5 co-compilable with XDGWMBase");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Fix KTimeComboBox input mask for AM/PM times (bug 361764)");?></li>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Fix KMainWindow saving incorrect widget settings (bug 395988)");?></li>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("KXmlRpcClient");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("if an applet is invalid, it has immediately UiReadyConstraint");?></li>
<li><?php i18n("[Plasma PluginLoader] Cache plugins during startup");?></li>
<li><?php i18n("Fix fading node when one textured is atlassed");?></li>
<li><?php i18n("[Containment] Don't load containment actions with plasma/containment_actions KIOSK restriction");?></li>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("Prison");?></h3>

<ul>
<li><?php i18n("Fix Mixed to Upper mode latching in Aztec code generation");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Make sure debugging for kf5.kio.core.copyjob is disabled for the test");?></li>
<li><?php i18n("Revert \"test: A more \"atomic\" way of checking for the signal to happen\"");?></li>
<li><?php i18n("test: A more \"atomic\" way of checking for the signal to happen");?></li>
<li><?php i18n("Add bluetooth plugin");?></li>
<li><?php i18n("[Telegram] Don't wait for Telegram to be closed");?></li>
<li><?php i18n("Prepare to use Arc's status colours in the revision drop-down list");?></li>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Improve sizing of menus (bug 396841)");?></li>
<li><?php i18n("Remove double comparison");?></li>
<li><?php i18n("Port away from string-based connects");?></li>
<li><?php i18n("check for valid icon");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Sonnet: setLanguage should schedule a rehighlight if highlight is enabled");?></li>
<li><?php i18n("Use the current hunspell API");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("CoffeeScript: fix templates in embedded JavaScript code &amp; add escapes");?></li>
<li><?php i18n("Exclude this in Definition::includedDefinitions()");?></li>
<li><?php i18n("Use in-class member initialization where possible");?></li>
<li><?php i18n("add functions to access keywords");?></li>
<li><?php i18n("Add Definition::::formats()");?></li>
<li><?php i18n("Add QVector&lt;Definition&gt; Definition::includedDefinitions() const");?></li>
<li><?php i18n("Add Theme::TextStyle Format::textStyle() const;");?></li>
<li><?php i18n("C++: fix standard floating-point literals (bug 389693)");?></li>
<li><?php i18n("CSS: update syntax and fix some errors");?></li>
<li><?php i18n("C++: update for c++20 and fix some syntax errors");?></li>
<li><?php i18n("CoffeeScript &amp; JavaScript: fix member objects. Add .ts extension in JS (bug 366797)");?></li>
<li><?php i18n("Lua: fix multi-line string (bug 395515)");?></li>
<li><?php i18n("RPM Spec: add MIME type");?></li>
<li><?php i18n("Python: fix escapes in quoted-comments (bug 386685)");?></li>
<li><?php i18n("haskell.xml: don't highlight Prelude data constructors differently from others");?></li>
<li><?php i18n("haskell.xml: remove types from \"prelude function\" section");?></li>
<li><?php i18n("haskell.xml: highlight promoted data constructors");?></li>
<li><?php i18n("haskell.xml: add keywords family, forall, pattern");?></li>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("ThreadWeaver");?></h3>

<ul>
<li><?php i18n("Honor BUILD_TESTING");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.49");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.8");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
