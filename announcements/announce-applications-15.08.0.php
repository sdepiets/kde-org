<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships KDE Applications 15.08.0");
  $site_root = "../";
  $release = "applications-15.08.0";
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<figure style="float: right; margin: 0px">
<a href="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png">
<img src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" width="400" height="295" />
</a>
<figcaption><center><?php print i18n_var("Dolphin in the new look - now KDE Frameworks 5 based");?></center></figcaption>
</figure>

<p align="justify">
<?php print i18n_var("August 19, 2015. Today KDE released KDE Applications 15.08.");?>
</p>

<p align="justify">
<?php print i18n_var("With this release a total of 107 applications have been ported to <a href='%1'>KDE Frameworks 5</a>. The team is striving to bring the best quality to your desktop and these applications. So we're counting on you to send your feedback.", "https://dot.kde.org/2013/09/25/frameworks-5");?>
</p>

<p align="justify">
<?php print i18n_var("With this release there are several new additions to the KDE Frameworks 5-based applications list, including <a href='%1'>Dolphin</a>, <a href='%2'>the Kontact Suite</a>, <a href='%3'>Ark</a>, <a href='%4'>Picmi</a>, etc.", "https://www.kde.org/applications/system/dolphin/", "https://www.kde.org/applications/office/kontact/", "https://www.kde.org/applications/utilities/ark/", "https://games.kde.org/game.php?game=picmi", "https://kdenlive.org/");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("Kontact Suite technical preview");?></h3>
<p align="justify">
<?php print i18n_var("Over the past several months the KDE PIM team did put a lot of effort into porting Kontact to Qt 5 and KDE Frameworks 5. Additionally the data access performance got improved considerably by an optimized communication layer.
The KDE PIM team is working hard on further polishing the Kontact Suite and is looking forward to your feedback.
For more and detailed information about what changed in KDE PIM see <a href='%1'>Laurent Montels blog</a>.", "http://www.aegiap.eu/kdeblog/");?>
</p>

<h3><?php print i18n_var("Kdenlive and Okular");?></h3>
<p align="justify">
<?php print i18n_var("This release of Kdenlive includes lots of fixes in the DVD wizard, along with a large number of bug-fixes and other features which includes the integration of some bigger refactorings. More information about Kdenlive's changes can be seen in its <a href='%1'>extensive changelog</a>. And Okular now supports Fade transition in the presentation mode.", "https://kdenlive.org/discover/15.08.0");?>
</p>

<figure style="float: right; margin: 0px">
<a href="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png">
<img src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" width="400" height="341" />
</a>
<figcaption style="word-wrap: normal"><center><?php print i18n_var("KSudoku with a character puzzle");?></center></figcaption>
</figure>

<h3><?php print i18n_var("Dolphin, Edu and Games");?></h3>

<p align="justify">
<?php print i18n_var("Dolphin was as well ported to KDE Frameworks 5. Marble got improved <a href='%1'>UTM</a> support as well as better support for annotations, editing and  KML overlays.", "https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system");?>
</p>

<p align="justify">
<?php print i18n_var("Ark has had an astonishing number of commits including many small fixes. Kstars received a large number of commits, including improving the flat ADU algorithm and checking for out of bound values, saving Meridian Flip, Guide Deviation, and Autofocus HFR limit in the sequence file, and adding telescope rate and unpark support. KSudoku just got better. Commits include: add GUI and engine for entering in Mathdoku and Killer Sudoku puzzles, and add a new solver based on Donald Knuth's Dancing Links (DLX) algorithm.");?>
</p>


<h3 style="clear:both;"><?php print i18n_var("Other Releases");?></h3>
<p align="justify">
<?php print i18n_var("Along with this release Plasma 4 will be published in its LTS version for the last time as version 4.11.22.");?>
</p>

<p align="justify">
<?php print i18n_var("You can find the full list of changes <a href='%1'>here</a>.", "fulllog_applications-15.08.0.php");?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the KDE Applications 15.08 release. Report bugs. Encourage others to join the KDE Community. Or <a href='%1'>support the nonprofit organization behind the KDE community</a>.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5"); ?>
</p>

<p align="justify">
<?php i18n("Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, and twitter. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with 'KDE'. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 15.08 release."); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 15.08 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 15.08 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 15.08");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 15.08 may be <a href='http://download.kde.org/stable/applications/15.08.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-15.08.0.php'>KDE Applications 15.08.0 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
