#
# This script regenerates all the screenshots from screenshots/png/
#

# Run from screenshots/ with py ../png2jpg.py
# It filles the current dir with all converted screenies from `pwd`/png/
#

import os

"""
png: 22.4 MiB

jpg:
	q 75: 3.7 MiB
	q 80: 4.2 MiB
	q 85: 4.9 MiB
	q 90: 7.9 MiB
"""
q = 85

for img in os.listdir("png"):
  if img[-4:] == ".png":
    jpg = img[0:-4] + ".jpg"
    #cmd = "svn mv " + img + " png/"
    cmd = "convert -quality " + str(q) + " png/" +  img + " " + jpg
    print cmd
    os.system(cmd)
