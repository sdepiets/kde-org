<?php

  $page_title = "Slikovni vodič po KDE 4.0: Namizje";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
  guide_links();
?>

<p>
Also available in:
<a href="desktop.php">English</a>
<a href="desktop-it.php">Italian</a>
<a href="desktop-pa.php">Punjabi</a>
<a href="desktop-sl.php">Slovenian</a>
</p>

<h2>Plasma</h2>
<?php
    screenshot("desktop_thumb.jpg", "desktop.jpg", "center",
"Namizje KDE 4.0");
?>
<p>
Plasma je nova namizna lupina v KDE in prinaša nova orodja za zagon programov,
za predstavitev glavnega uporabniškega vmesnika in omogoča nove načine uporabe
vašega namizja.
</p>
<p>
Nov pogled, imenovan <strong>armaturna plošča</strong>, nadomešča staro funkcijo
»Prikaži namizje«. Prikaz armaturne plošče skrije vsa okna in v ospredje postavi
gradnike. Za prikaz gradnikov (plazmoidov) pritisnite Ctrl+F12. Na voljo so plazmoidi
za pisanje notic, branje virov RSS, spremljanje vremenske napovedi in še mnogo več.
</p>
<?php
    screenshot("dashboard_thumb.jpg", "dashboard.jpg", "center",
                "Plasma in armaturna plošča");
?>

<h3>Zaženite programe, iščite in odprite spletne strani s KRunner-jem</h3>
<p>
<strong>KRunner</strong> vam omogoča hiter zagon programov. Za prikaz pogovornega
okna KRunner pritisnite Alt+F2. Med tipkanjem vam KRunner prikazuje le rezultate,
ki se ujemajo z vnešenim:
<?php
    screenshot("krunner-desktop_thumb.jpg", "krunner-desktop.jpg", "center",
"Zagon programov s KRunner-jem");
?>
<ul>
  <li>
    Vtipkajte ime programa in KRunner prikaže programe, ki se ujemajo z vašo
poizvedbo. Izberite želeni program s seznama spodaj, ali pa pritisnite Enter in
zagnal se bo program, ki je povsem na vrhu in označem kot privzet. Da bi videli
seznam aktivnih procesov, kliknite na gumb »Prikaži aktivnost sistema« ali pa
pritisnite Alt+A. Za grafični prikaz poženite Sistemskega nadzornika.
  </li>
  <li>
    KRunner lahko deluje tudi kot preprosto računalo. Vnesite matematčni izraz
v obliki »=1337*4« in KRunner bo takoj prikazal rezultat.
<?php
    screenshot("krunner-calculator.jpg", FALSE, "center",
                "KRunner kot računalo");
?>

  </li>
  <li>
    Odprite zaznamke v spletnem brskalniku. Do zaznamkov je moč dostopati z
bližnjicami. Vnesite »gg:slikovni vodič po kde 4.0«, da z Googlom najdete ta vodič.
Vnesite »wp:kde«, da odprete članek o KDE na Wikipediji. Daljši seznam spletnih
bližnjic je na voljo v nastavitvah spletnega brskalnika Konqueror.
  </li>
<!-- No search enabled in 4.0
  <li>
    Search through your desktop data. Entering a term always offer to search for 
this term via the desktop search engine. For this to work, your desktop search 
egine needs to be activated. KDE comes with the Strigi desktop search framewor. 
Strigi can be activated and configured by entering the command "kcmshell4 
kcm_npomuk" in a konsole.
  </li>
-->
</ul>
</p>

<h3>KickOff</h3>
<p>
KickOff je novi zaganjalnik programov, oziroma »meni start«.
Kliknite na logotip KDE-ja v spodnjem levem kotu zaslona. Odpre se meni KickOff,
ki ponuja preprost dostop do nameščenih programov, nazadnje uporabljenih datotek
ter programov in več. Prek zavihka Zapusti se lahko odjavite, ugasnete ali zaustavite
svoj računalnik.
<?php
    screenshot("kickoff-favorites_thumb.jpg", "kickoff-favorites.jpg",
        "center", "Zaženite svoje priljubljene programe s KickOff-om");
?>
<ul>
  <li>
	<strong>Priljubljeno</strong>. Tu se nahajajo programi ali dokumenti, ki
	jih pogosto uporabljate. Zato se ta zavihek prikaže najprej. Stvari lahko na
	seznam priljubljenih dodajate preprosto z desnim klikom nanje in izbiro ukaza
	»Dodaj med priljubljene«. Odstranite jih z izbiro ukaza »Odstrani iz priljubljenih«.
  </li>
  <li>
	<strong>Programi</strong>. Ta zavihek prikazuje seznam programov, razporejenih
	v kategorije. Brskajte po kategorijah, da vidite vse programe, ki so nameščeni
	na računalniku. Nazaj se vrnete s klikom na gumb ob levem robu.
  </li>
  <li>
	<strong>Računalnik</strong>. Prek tega zavihka lahko dostopate do nosilcev za
	shranjevanje (trdi diski, USB ključki, itd.). Na voljo so tudi priljubljena mesta
	in Sistemske nastavitve.
  </li>
  <li>
	<strong>Nazadnje uporabljeno</strong>. Na tem zavihku so prikazani programi in
	dokumenti, ki ste jih nedavno uporabljali. Tako lahko hitro nadaljujete z
	delom, ki ste ga začeli pred kratkim.
  </li>
  <li>
	<strong>Zapusti</strong>. Od tu lahko ugasnete svoj računalnik ali pa se odjavite.
	Če bi radi zaustavili računalnik ali pa želite, da preide v stanje pripravljenosti, izberite
	Ugasni in v oknu, ki se pojavi, za dalj časa kliknite na »Ugasni računalnik«.
  </li>
</ul>
</p>

<h3>Pult</h3>

<?php
    screenshot("panel_thumb.jpg", "panel.jpg",
        "center", "Pult vsebuje KickOff, opravilno vrstico, pozivnik, sistemsko vrstico, uro in druge gradnike");
?>
<p>
Če iščete specifično orodje, kliknite na gumb K v spodnjem levem kotu, da se odpre
KickOff, in začnite tipkati.
S tipkanjem filtrirate seznam programov, tako da so vidni samo tisti, ki se
ujemajo z natipkanim. Če na primer vnesete »cd«, dobite seznam programov za
predvajanje glasbenih CD-jev in peko CD-jev ter DVD-jev. Podobno z vnosom
»pregledovalnik« dobite seznam programov za pregledovanje različnih vrst
dokumentov.
</p>
<p>
<strong>Pult</strong> Plasme vsebuje gumb za zagonski meni, sistemsko vrstico in
seznam opravil. Opravilna vrstica lahko prikazuje sličice oken, ki so trenutno
skriti. Sličice odražajo dejansko vsebino oken, tudi ko se le ta spreminja.
<br />
</p>
<p>
Na pultu lahko najdete tudi pozivnik. Uporabite ga za preklapljanje med delovnimi
okolji, ki se imenujejo tudi »virtualna namizja«. Z desnim klikom na pozivnik lahko
nastavite število in postavitev delovnih okolij. Če imate omogočene grafične učinke
za namizje, lahko za celozaslonski pregled vseh delovnih okolij pritisnete Ctrl+F8.
</p>

<p>
Namig: Da odprete KickOff, lahko na slepo premaknete miškin kazalec v spodnji levi kot
in pritisnite levi miškin gumb.
</p>
<p>
Če bi radi izvedeli več o Plasmi, si oglejte
<a href="http://techbase.kde.org/Projects/Plasma/FAQ">pogosto zastavljena
vprašanja o Plasmi</a> na strani TechBase.
</p>

<h2>KWin - upravljalnik oken</h2>
<p>
KWin je KDE-jev preverjen in stabilen upravljalnik oken. Bil je izboljšan in
sedaj izkorišča zmogljivosti modernih grafičnih kartic ter s tem poenostavlja
delo z okni. V meniju, ki ga dobite, če z desnim miškinim gumbom kliknete na
naslovno vrstico okna, lahko izberete ukaz »Nastavi obnašanje okna« in nastavite
okenski upravljalnik. Nastavite lahko na primer, da ob dvokliku na naslovno
vrstico razpnete okno.
</p>
<p>
Namig: KWin vam omogoča preprosto premikanje oken. Potrebno je le pritisniti tipko
Alt, z miško zagrabiti okno kjerkoli in ga povleči. Podobno lahko s pritisnjeno tipko
Alt in desnim gumbom na miški spreminjate velikost oken. Nič več vam ni potrebno
ciljati natančno pri upravljanju z okni.
</p>

<h3>Grafični učinki za namizje</h3>
<p>
Če omogočite grafične učinke namizja, vam KWin ponuja nove načine za delo z
okni. Z desnim miškinim gumbom kliknite na naslovno vrstico, izberite ukaz
»Nastavi obnašanje okna« in pojdite na »Učinki namizja«. Omogočite nastavitev
»Omogoči namizne učinke« in potrdite nove nastavitve s klikom na »V redu« ali
na »Uveljavi«. Po tem bo omogočeno napredno delovanje upravljalnika oken KWin.
</p>
<?php
    screenshot("kwin-presentwindows_thumb.jpg", "kwin-presentwindows.jpg",
            "center", "Preklapljanje med programi z učinkom Predstavi okna");
?>
<p>
    <strong>Predstavi okna</strong> ponuja pregled vseh odprtih oken. Premaknite
kazalec miške v zgornji levi kot zaslona in okna bodo postavljena drug ob drugega.
Nato lahko kliknite na okno, da dobi fokus. Začnete lahko tudi tipkati niz iz naslova
okna in filtriranje bo prikazalo le ujemajoča okna. Za preklop v izbran program nato
pritisnete Enter. Učinek Predstavi okna lahko aktivirate tudi s pritiskom tipk
Ctrl+F9 ali pa Ctrl+F10 (za prikaz oken z vseh navideznih namizij).
</p>
<p>
    <strong>Mreža namizij</strong> oddalji pogled od vašega namizja, tako da se vidi
mreža vaših navideznih namizij ali delovnih okolij. Sedaj lahko premikate okna med
namizji. Če kliknete na eno izmed namizij, se le ta spet približa.
</p>
<p>
Namig: Da preklopite na določeno namizje, lahko pritisnete tudi tipko z
ustrezno številko. Mrežo namizij aktivirate s pritiskom na Ctrl+F8.<br />
Če na okno kliknete s srednjim miškinim gumbom, bo prikazano na vseh namizjih.
</p>
<?php
    screenshot("desktopgrid_thumb.jpg", "desktopgrid.jpg",
            "center", "Učinek Mreža namizij ponuja iste funkcije kot pozivnik");
?>
<p> 
Pult vsebuje gradnik, ki ponuja podobne funkcije in je na voljo, tudi ko učinki
namizja niso omogočeni. Kliknite z desnim gumbom miške na pozivnik na pultu in tako
nastavite število in postavitev navideznih namizij. Gradnik pozivnika lahko
potegnete na pult ali na namizje, kot je razvidno z zaslonskih posnetkov.
</p>
<?php
    screenshot("pager.jpg", FALSE,
            "center", "Pozivnik omogoča preklapljanje med delovnimi okolji");
?>
<p>
    Učinek <strong>Sličice za opravilno vrstico</strong> omogoči oglede vsebine oken,
če z miško lebdite nad gumbom v opravilni vrstici. To lahko pride prav, če želite
spremljati dejavnost v skritem oknu. Sličica služi tudi kot vizualni namig, ki lahko
poenostavi iskanje pravega okna.
<?php
    screenshot("kwin-taskbarthumbnails_thumb.jpg", "kwin-taskbarthumbnails.jpg",
"center", "Preklapljanje med opravili s sličicami za opravilno vrstico");
?>
</p>
<p>
Učinki za namizje, oziroma bolj tehnično, kompozicijske zmožnosti KWina, omogočijo
tudi <strong>učinke prosojnosti</strong> v mnogih programih. Na primer Konzola,
terminalski program za KDE, ima lahko prosojno ozadje, tako da se skozenj lahko
vidijo programi, ki so za oknom.
Lahko spremenite tudi motnost okna, tako da z desnim gumbom miške kliknete na
naslovno vrstico okna in iz podmenija Motnost izberete želeno stopnjo.
<?php
    screenshot("kwin-transparency_thumb.jpg", "kwin-transparency.jpg", "center",
        "Spremenite motnost posameznega okna");
?>
</p>
<p>
Natančnejši nadzor nad učinki vam je na voljo na zavihku »Vsi učinki«. Mnogo učinkov
je mogoče še dodatno nastaviti, tako da si njihovo delovanje prikrojite po svojih
željah. KWin poskuša samodejno omogočiti efekte, glede na zmogljivost grafične
kartice.<br /><br />
Če bi radi vedeli več o KWin in zmožnostih kompozicije, si oglejte
<a href="http://techbase.kde.org/Projects/KWin/4.0-release-notes">opombe
ob izdaji</a> ali <a href="http://techbase.kde.org/Projects/KWin">TechBase</a>.
</p>

<h2>Posebna mesta</h2>
<p>
KDE veliko uporablja posebna mesta na zaslonu - robove in kote, ki so preprostejši
za ciljanje in jih lahko tako hitreje dosežete. Premaknite miško v zgornji desni
kot, kliknite in zaprli boste razpeto okno. Gumb za zaprtje okna ima na levo nekaj
prostora, da ga nebi kliknili pomotoma, ko bi želeli razpeti okno. Vrstni red
gumbov v naslovni vrstici lahko prilagodite svojim delovnim navadam. Kliknite z
desnim gumbom na naslovno vrstico okna in izberite »Nastavi obnašanje okna«.
</p>
<p>

<table width="100%">
       <tr>
               <td width="50%">
                               <a href="guide-sl.php">
                               <img src="images/star-32.png" />
                               Pregled
                               </a>            
               </td>
               <td align="right" width="50%">
                               <a href="applications-sl.php">Naslednja stran: Osnovni programi
                               <img src="images/applications-32.png" /></a>
               </td>
       </tr>
</table>

</p>
<?php
  include("footer.inc");
?>
