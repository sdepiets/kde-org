<?php

error_reporting(E_ALL);

// Some helper functions for the visual guide
function guide_links() {
	$out = "<p><strong>Jump to:</strong> &nbsp;";
	$out .= "<a href=\"guide.php\">Overview</a> &nbsp;";
	$out .= "<a href=\"desktop.php\">Desktop</a> &nbsp;";
	$out .= "<a href=\"applications.php\">Applications</a> &nbsp;";
	$out .= "<a href=\"education.php\">Educational Applications</a> &nbsp;";
	$out .= "<a href=\"games.php\">Games</a></p>";

	print $out;
}

?>