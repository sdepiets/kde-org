<?php
  $page_title = "KDE 4.0 Games";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
?>

Disponibile anche in:
<a href="games.php">Inglese</a>

<p>
<img src="images/games.png" align="right"/>
La comunità che sta attorno a «KDE Games» ha fatto uno sforzo incredibile nel riadattamento e nella revisione di molti giochi da KDE 3. In aggiunta ne sono anche stati aggiunti di nuovi per KDE 4.<br />
Da considerare in questa area sono sicuramente una migliorata intuitività di gioco, una grafica riveduta e corretta e una maggiore indipendenza dalla risoluzione del monitor. Potrai giocare a schermo intero oppure in una piccola finestra senza notare molte differenze.<br />

Molta della grafica è stata completamente rifatta, dando ai giochi di KDE 4.0 un look pulito e moderno.
</p>

<h2>KGoldrunner - un arcade</h2>
<?php
	screenshot("kgoldrunner_thumb.jpg", "kgoldrunner.jpg", "center", "Raccogli le pepite d'oro in KGoldrunner");
?>
<p>
KGoldrunner è un labirinto con delle sfumature da rompicapo. Ha centinaia di livelli dove pepite d'oro devono essere raccolte, ma attento ai nemici che cercheranno ti impedirtelo intutti i modi!
</p>

<h2>KFourInLine - un gioco da tavolo</h2>
<?php
	screenshot("kfourinline_thumb.jpg", "kfourinline.jpg", "center", "Connect four pieces in a row in KFourInLine");
?>
<p>
KFourInLine è un gioco da tavolo per due giocatori basato su Forza4. I giocatori devono cercare di mettere in fila quattro pedine prima dell'avversario utilizzando differenti strategie.
</p>

<h2>LSkat - un gioco di carte</h2>
<?php
	screenshot("lskat_thumb.jpg", "lskat.jpg", "center", "Play Skat");
?>
<p>
Lieutnant Skat (Dal tedesco "Offiziersskat") è un divertente gioco di carte per due giocatori dove uno dei due può essere sostituito dall'intelligenza artificiale del programma.
</p>

<h2>KJumpingCube - un gioco di dadi</h2>
<?php
	screenshot("kjumpingcube_thumb.jpg", "kjumpingcube.jpg", "center", "Gioca con i dadi di KJumpingCube");
?>
<p>
KJumpingcube è un semplice gioco di strategia basato sugli scacchi. Il campo di gioco è costituito da dadi messi uno di fianco all'altro. Il gioco procede facendo click sui dadi, che siano essi ancora da mettere in gioco oppure già di proprietà di uno dei giocatori.
</p>

<h2>KSudoku - un gioco di logica</h2>
<?php
	screenshot("ksudoku_thumb.jpg", "ksudoku.jpg", "center", "Esercita la mente con KSudoku");
?>
<p>
KSudoku è un gioco di logica. Il giocatore deve riempire una griglia con dei caratteri, numeri o lettere, in modo che ogni colonna ogni riga e quadrato contengano solo una volta lo stesso carattere.
</p>

<h2>Konquest - un gioco di strategia</h2>
<?php
	screenshot("konquest_thumb.jpg", "konquest.jpg", "center", "Conquista gli altri pianeti con Konquest");
?>
<p>
In questo gioco i giocatori devono conquistare gli altri pianeti inviandoci un numero di astronavi. L'obbiettivo è di costruire un impero interstellare fino a conquistare i pianeti di tutti gli altri giocatori.
</p>

<p>
Puoi trovare molte più informazioni sui giochi di KDE su sito della comunità <a href="http://games.kde.org">KDE Games</a>.
</p>
<table width="100%">
	<tr>
		<td width="50%">
				<a href="education-it.php">
				<img src="images/education-32.png" />
				Nella pagina precedente: Applicazini educative
				</a>		
		</td>
		<td align="right" width="50%">
				<a href="guide-it.php">Introduzione
				<img src="images/star-32.png" /></a>
		</td>
	</tr>
</table>

<?php
  include("footer.inc");
?>
