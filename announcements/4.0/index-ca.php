<?php
  $page_title = "Anunci de publicació del KDE 4.0";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<a href="index.php">Anglès</a>
<a href="index-bn_IN.php">Bengalí (Índia)</a>
<a href="index-es.php">Espanyol</a>
<a href="index-cz.php">Czech</a>
<a href="http://fr.kde.org/announcements/4.0/">Francès</a>
<a href="http://www.kde.de/infos/ankuendigungen/40/">German</a>
<a href="index-gu.php">Gujarati</a>
<a href="index-he.php">Hebreu</a>
<a href="index-hi.php">Hindi</a>
<a href="index-it.php">Italià</a>
<a href="index-lv.php">Letó</a>
<a href="index-ml.php">Malaialam</a>
<a href="index-mr.php">Marathi</a>
<a href="index-ru.php">Russian</a>
<a href="index-pa.php">Punjabi</a>
<a href="index-pt_BR.php">Portuguese (Brazilian)</a>
<a href="index-nl.php">Neerlandès</a>
<a href="index-sl.php">Eslovè</a>
<a href="index-sv.php">Suec</a>
<a href="index-ta.php">Tamil</a>

<h3 align="center">
   El Projecte KDE publica la quarta versió major de l'entorn d'escriptori lliure més avançat
</h3>
<p align="justify">
  <strong>
    Amb la quarta versió major, la comunitat KDE marca el començament de l'època del KDE 4.
  </strong>
</p>
<p align="justify">
11 de Gener de 2008 (Internet).
</p>

<p>
La comunitat KDE està encantada d'anunciar la disponibilitat immediata del
<a href="http://www.kde.org/announcements/4.0/">KDE 4.0.0</a>. Aquesta important
versió marca alhora el final d'un llarg i intens cicle de desenvolupament intens que ha portat al 
KDE 4.0, així com el principi de l'època del KDE 4.
</p>

<?php
    screenshot("desktop_thumb.jpg", "desktop.jpg", "center",
"L'escriptori del KDE 4.0");
?>

<p>
Les <strong>biblioteques</strong> del KDE 4 han experimentat grans millores en gairebé totes les àrees.
Phonon és un marc multimèdia que proporciona capacitat multimèdia independent de la plataforma a totes
les aplicacions del KDE, i Solid és un marc d'integració del maquinari que fa més fàcil treballar 
amb dispositius i proporciona eines per millorar la gestió d'energia.
<p />
L'<strong>escriptori</strong> del KDE 4 ha guanyat diverses capacitats noves. L'escriptori Plasma 
ofereix una nova interfície d'escriptori, que inclou el plafó, menú i estris d'escriptori. KWin, 
el gestor de finestres del KDE pot crear efectes gràfics
avançats que faciliten la interacció amb les finestres.
<p />
Gran quantitat d'<strong>aplicacions</strong> han rebut també millores. Hi ha hagut
avenços visuals a través de l'us de gràfics vectorials, canvis en les biblioteques de base,
noves característiques en la interfície d'usuari i noves aplicacions. Tot allò que se us acudeixi,
KDE 4.0 ho té.
Okular, el nou visor de documents i Dolphin, el nou gestor de fitxers
són dos exemples de les moltes aplicacions aprofiten les noves tecnologies del KDE 4.0.
<p />
<img src="images/oxybann.png" align="right" />
L'equip de <strong>grafisme</strong> Oxygen proporciona una alenada d'aire fresc a l'escriptori.
Gairebé totes les parts visibles de l'escriptori i les aplicacions del KDE han rebut retocs.
Bellesa i consistència són dos dels conceptes bàsics d'Oxygen.
</p>



<h3>Escriptori</h3>
<ul>
	<li>Plasma és el nou escriptori. Proporciona un plafó, un menú i altres
	  formes intuïtives d'interaccionar amb l'escriptori i les aplicacions.
	</li>
	<li>KWin, el provat gestor de finestres del KDE inclou ara avançades característiques de composició.
	  El dibuixat accelerat per maquinari facilita una interacció més suau i intuïtiva amb
	  les finestres.
	</li>
	<li>Oxygen és el grafisme del KDE 4.0. Oxygen proporciona un concepte de grafisme consistent, net i bell.
	</li>
</ul>
Apreneu més en quant a la nova interfície d'escriptori del KDE a la <a href="desktop.php">guia visual del KDE 4.0</a>.

<h3>Aplications</h3>
<ul>
	<li>Konqueror és el provat navegador web del KDE. Konqueror és lleuger, integrat i 
	    compatible amb els estàndards més nous com CSS 3.</li>
	<li>Dolphin és el nou gestor de fitxers del KDE. Dolphin ha estat desenvolupat amb
	    la usabilitat en ment i és una eina fàcil d'usar però no per això menys potent.
	</li>
	<li>Amb System Settings, s'introdueix una nova interfície de centre de control.
	    El monitor del sistema KSysGuard facilita la monitorització i control
	    dels recursos del sistema.
	</li>
	<li>Okular, el visor de documents del KDE 4 és compatible amb molts formats.
	    Okular és una de les moltes aplicacions del KDE 4 que s'ha millorat en
	    col·laboració amb el <a href="http://openusability.org">projecte OpenUsability</a>
	</li>
	<li>Les aplicacions educatives han estat de les primeres aplicacions
	    en utilitzar la tecnologia del KDE 4. Kalzium, una taula periòdica dels elements
	    i el globus terraqüi Marble són només dues de les perles entre les aplicacions
	    educatives. Llegiu més en quant a les aplicacions educatives a la
		<a href="education.php">Guia Visual</a>
	</li>
	<li>Una gran quantitat de jocs del KDE han estat actualitzats. Els jocs del KDE com KMines, 
	    un joc de buscamines i KPat, un joc de solitaris han estat remodelats. Gràcies al
	    grafisme vectorial i les noves capacitats gràfiques, els jocs són
	    ara més independents de la resolució de pantalla.
	</li>
</ul>
Algunes aplicacions estan descrites amb més detall a<a href="applications.php">la guia visual del KDE 4.0</a>.

<?php
screenshot("dolphin-systemsettings-kickoff_thumb.jpg", "dolphin-systemsettings-kickoff.jpg", "center",
	"El gestor de fitxers, System Settings i el menú");
?>

<h3>Biblioteques</h3>
<p>
<ul>
	<li>Phonon ofereix a les aplicacions capacitats multimèdia com la reproducció d'àudio i vídeo.
	    Internament, Phonon pot fer us de varis dorsals, intercanviables en temps d'execució. 
		El dorsal predeterminat pel KDE 4.0 està basat en Xine i proporciona un gran rendiment
		en molts formats. Phonon també permet escollir el dispositiu de sortida basat en
		el tipus de mitjà.
	</li>
	<li>El marc d'integració del maquinari Solid integra els dispositius fixos i extraïbles a les
	    aplicacions del KDE. Solid també fa d'interfície per la gestió d'energia, gestiona
	    la connexió a la xarxa i la integració amb dispositius Bluetooth.
	    Internament, Solid combina la potència de HAL, NetworkManager i la pila de bluetooth Bluez,
	    tanmateix aquest components són intercanviables amb d'altres sense que les
	    aplicacions es vegin afectades, proporcionant així màxima portabilitat.
	</li>
	<li>KHTML és el motor de pàgines web usat per Konqueror, el navegador web del KDE. KHTML 
	    és lleuger i compatible amb estàndards moderns com CSS 3. KHTML va ser també el primer motor
	    en passar el famós test Acid 2. 
	</li>
	<li>La biblioteca ThreadWeaver, part de kdelibs, proporciona una interfície d'alt nivell
	    per fer un millor ús dels sistemes multinucli actuals, fent que les aplicacions del KDE 
	    es comportin d'una manera més suau i usin els recursos disponibles d'una forma més eficient.
	</li>
	<li>Basat en la biblioteca Qt 4 de Trolltech, KDE 4.0 pot fer ús de les capacitats 
	    visuals avançades i menor consum de memòria d'aquesta llibreria. kdelibs proporciona una
	    excepcional extensió a Qt, afegint gran quantitat de funcionalitat d'alt nivell que és
	    molt útil als desenvolupadors.
	</li>
</ul>
</p>
<p><a href="http://techbase.kde.org">TechBase</a>, la biblioteca de coneixement del KDE, té més informació relativa
a les biblioteques del KDE.</p>


<h4>Feu una visita guiada...</h4>
<p>
La <a href="guide.php">guia visual del KDE 4.0</a> proporciona un resum ràpid de
diverses de les noves i millorades tecnologies del KDE 4.0. Il·lustrada amb captures de pantalla,
us portarà a través de les diferents parts del KDE 4.0 i us mostrarà les excitants tecnologies
i millores per l'usuari. Començareu amb les noves característiques de l'<a href="desktop.php">escriptori</a>
, per continuar amb <a href="applications.php">aplicacions</a> com System Settings, Okular el 
visor de documents i Dolphin el gestor de fitxers, per acabar amb
les <a href="education.php">aplicacions educatives</a> i els <a href="games.php">jocs</a>.
</p>


<h4>Proveu KDE 4.0...</h4>
<p>
Per aquells interessats en obtenir paquets per provar i contribuir, diverses
distribucions ens han notificat que faran disponibles paquets del KDE 4.0 a temps
o una mica després de l'alliberament de la versió. La llista completa es troba a 
<a href="http://www.kde.org/info/4.0.php">la pàgina d'informació del KDE 4.0</a>, on també
podeu trobar enllaços al codi font, informació de com compilar, avisos de seguretat, etc.
</p>
<p>
Les següents distribucions ens han notificat de la disponibilitat de paquets o Live CD 
del KDE 4.0:

<ul>
	<li>
		S'espera una versió alfa basada en KDE4 d'<strong>Arklinux 2008.1</strong> poc temps
		després del KDE 4.0, amb una versió final al cap de 3 o 4 setmanes.
	</li>
	<li>
		<strong>Fedora</strong> inclourà KDE 4.0 a Fedora 9, que <a
		href="http://fedoraproject.org/wiki/Releases/9">s'alliberarà</a>
		a l'abril, hi haurà versions alfa a patir del 24 de gener. Els paquets del KDE 4.0 
		estan a <a
		href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a> el repositori prealfa.
	</li>
	<li>
		Els paquets del KDE 4.0 de <strong>Debian</strong> estan disponibles a "experimental",
		la plataforma de desenvolupament del KDE entrarà a <em>Lenny</em>. Estigueu atents als
		anuncis de l'<a href="http://pkg-kde.alioth.debian.org/">equip KDE de Debian</a>.
	</li>
	<li>
		<strong>Gentoo Linux</strong> proporciona paquets KDE 4.0 a
		<a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
	</li>
	<li>
		<strong>Mandriva</strong> proporcionarà paquets per la versió 2008.0 i té
		planejar produir un Live CD amb l'última versió de Mandriva 2008.1.
	</li>
	<li>
		Els paquets per <strong>openSUSE</strong> <a href="http://en.opensuse.org/KDE4">estan disponibles</a> 
		per openSUSE 10.3 (
		<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">instal·lació amb
		un clic</a>) i openSUSE 10.2. També hi ha un <a href="http://home.kde.org/~binner/kde-four-live/">Live CD</a>
		amb aquests paquets. KDE 4.0 serà part de openSUSE 11.0.
	</li>
	<li>
		Els paquets per <strong>Ubuntu</strong> seran inclosos en la propera "Hardy Heron"
		(8.04) i també estan disponibles com a actualització per la "Gutsy Gibbon" (7.10).
		També hi ha un Live CD per provar KDE 4.0. Podeu trobar més detalls a 
		l'<a href="http://kubuntu.org/announcements/kde-4.0.php">anunci</a> a Ubuntu.org.
	</li>
</ul>
</p>

<h2>Quant a KDE 4</h2>
<p>
KDE 4.0 és un innovador escriptori de Programari Lliure que conté gran quantitat 
d'aplicacions per l'ús diari així com també per propòsits específics. Plasma 
és un nou escriptori desenvolupat pel KDE 4, que proporciona una forma intuïtiva
d'interactuar amb l'escriptori i les aplicacions. El navegador web Konqueror integra la web amb l'escriptori. 
El gestor de fitxers Dolphin, el visor de documents Okular i el centre de control System Settings
completen el conjunt bàsic d'escriptori.
<br />
KDE està construït sobre les biblioteques KDE que proporcionen una forma fàcil d'accedir a recursos
de xarxa gracies a KIO i capacitats visuals avançades a través de Qt4. Phonon i Solid, que també formen part
de les biblioteques del KDE, afegeixen funcionalitat multimèdia i de treball amb el maquinari a totes
les aplicacions del KDE.
</p>

<h2>Quant a KDE</h2>
<p>
El KDE està produït per un equip tecnològic internacional que crea programari lliure per ordinadors portàtils i d'escriptori. 
Entre els productes creats pel KDE hi ha un modern sistema d'escriptori per a plataformes GNU/Linux i UNIX, extensos paquets 
ofimàtics i de treball en grup, i centenars d'aplicacions de diverses categories, incloent aplicacions web i d'internet, 
multimèdia, d'entreteniment, educatives, gràfiques i de desenvolupament de programari. Els programes del KDE estan 
traduïts a més de 60 llengües i estan construïts pensant en la facilitat d'ús i els criteris moderns d'accessibilitat. 
Les completes aplicacions del KDE4 s'executen nativament en sistemes GNU/Linux, GNU/BSD, Solaris, Windows i Mac OS X.
</p>

<h4>Contactes de premsa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
