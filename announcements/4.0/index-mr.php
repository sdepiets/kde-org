<?php

  // $page_title = "KDE 4.0 Released";
  $page_title = "के.डी.ई.  4.0 उपलब्ध";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Also available in:
<a href="index-bn_IN.php">Bengali (India)</a>
<a href="index-ca.php">Catalan</a>
<a href="index-cz.php">Czech</a>
<a href="index-es.php">Spanish</a>
<a href="http://fr.kde.org/announcements/4.0/">French</a>
<a href="http://www.kde.de/infos/ankuendigungen/40/">German</a>
<a href="index-gu.php">Gujarati</a>
<a href="index-he.php">Hebrew</a>
<a href="index-hi.php">Hindi</a>
<a href="index-it.php">Italian</a>
<a href="index-lv.php">Latvian</a>
<a href="index-ml.php">Malayalam</a>
<a href="index-mr.php">Marathi</a>
<a href="index-nl.php">Dutch</a>
<a href="index-pa.php">Punjabi</a>
<a href="index-ru.php">Russian</a>
<a href="index-sl.php">Slovenian</a>
<a href="index-sv.php">Swedish</a>
<a href="index-ta.php">Tamil</a>

<h3 align="center">
<!--   KDE Project Ships Fourth Major Version of cutting edge Free Software Desktop-->
   के.डी.ई.  प्रकल्पाच्या मुक्त डेस्कटॉप्चे चौथी आवृत्ती उपलब्ध!
</h3>
<p align="justify">
  <strong>
<!--    With the fourth major version, the KDE Community marks the beginning of the KDE 4 era.-->
   ह्या चौथ्या मुख्य आवृत्तीबरोबरच के.डी.ई.  समाजाचे नवीन पर्व सुरू झाले आहे.
  </strong>
</p>
<p align="justify">
<!--January 11, 2007 (The INTERNET).-->
जानेवारी ११, २००८ (इन्टरनेट मार्फत)
</p>

<p>
<!--
The KDE Community is thrilled to announce the immediate availability of
<a href="http://www.kde.org/announcements/4.0/">KDE 4.0.0</a>. This significant
release marks both the end of the long and intensive development cycle
leading up to KDE 4.0 and the beginning of the KDE 4 era.
-->

<a href="http://www.kde.org/announcements/4.0/">के.डी.ई. ४.०.०</a> ची त्वरित उपलब्धता 
जाहिर करताना संपूर्ण के.डी.ई. समाजाला अत्यंत आनंद आहे. ही महत्वपूर्ण उन्नती के.डी.ई.   समाजानी केलेल्या दीर्घ आणी तीव्र विकासांचे फळ आहे.
</p>

<?php
    screenshot("desktop_thumb.jpg", "desktop.jpg", "center",
// "KDE 4.0 Desktop" );
   "के.डी.ई.  चा चौथा डेस्कटॉप");
?>

<p>
<!--
The KDE 4 <strong>Libraries</strong> have seen major improvements in almost all areas.
The Phonon multimedia framework provides platform independent multimedia support to all
KDE applications, the Solid hardware integration framework makes interacting with
(removable) devices easier and provides tools for better powermanagement.
-->
सर्वात मोठी सुधारणा के.डी.ई. ४.०  च्या <strong>प्रणालीकोषा</strong> मधे केलेली आहे. 
"फोनॉन" ही बहुमाध्यमिक संरचना सर्व के.डी.ई.  प्रणालीना  हार्डवेअर-स्वतंत्र बहुमाध्यमिक क्षमता देते. 
"सॉलिड" हार्डवेअर-संघटन संरचना ही अस्थायी यंत्र वापरणे व ऊर्जा-प्रबंधन करणे सोयीचे करते.

<p />
<!--
The KDE 4 <strong>Desktop</strong> has gained some major new capabilities. The Plasma desktop shell
offers a new desktop interface, including panel, menu and widgets on the desktop
as well as a dashboard function. KWin, the KDE Window manager now supports advanced
graphical effects to ease interaction with your windows.
-->

के.डी.ई.  ४  <strong>डेस्कटॉप</strong> ला बर्याच नवीन क्षमता मिळाल्या आहेत.
"प्लाझमा" डेस्कटॉपला फलक, प्रणालीसूची, व डेस्कटॉप उपकरणे, तसेच उपकरण-पडदा  मिळाला  आहे. 
"के-विन" विंडो-नियंत्रकतल्या प्रगत चित्र-करामतींमुळे आपले काम सुखद आणी सुरळीत होते.

<p />
<!--
Lots of KDE <strong>Applications</strong> have seen improvements as well. Visual updates through
using vector-based artwork, changes in the underlying libraries, user interface
enhancements, new features, even new applications -- you name it, KDE 4.0 has it.
Okular, the new document viewer and Dolphin, the new filemanager are only two
applications that leverage KDE 4.0's new technologies.
-->
के.डी.ई.  च्या सर्व प्रणालींमधे भरपुर सुधारणा झालेली आहे. सदिष चित्रकलेवर आधारित चित्र करामती,
मूलभूत प्रणलीकोषांमधे सुधरणा, नवीन प्रणली व इतर बरीच नवीन वैशिष्ट्य के.डी.ई.  ४ मधे आहेत.
या सर्व प्रौद्योगिक सुधारणांची २ उतकृष्ठ उदाहरणे म्हणजेच नवीन दस्तावेज प्रणाली "ऑक्युलर",
व दस्तावेज प्रबंधक "डॉलफिन"

<p />
<!--
The Oxygen <strong>Artwork</strong> team provides a breath of fresh air on the desktop.
Nearly all user-visible parts of the KDE desktop and applications have been given a
facelift. Beauty and consistency are two of the basic concepts behind Oxygen.
-->
ऑक्सिजन चित्रकला गटाने डेस्कटॉपचे सौंदर्य वाढविले आहे. के.डी.ई.  डेस्कटॉपची जवळजवळ सर्व प्रणाली दृश्य पूर्णतः बदलली आहेत.
सौंदर्य व सुसंगतता हे ऑक्सिजन गटाचे महत्वपूर्ण उद्देश आहेत.
</p>


<!--<h3>Desktop</h3>-->
<h3>डेस्कटॉप</h3>
<ul>
	<li><!--Plasma is the new desktop shell. Plasma provides a panel, a menu and other
	  intuitive means to interact with the desktop and applications.
          -->"प्लाझमा" हा नवीन डेस्कटॉप फलक, प्रणालीसूची, व इतर  डेस्कटॉप उपकरणांमार्फत संगणकाचा वापर सरल करतो.
	</li>
	<li><!-- Kwin, KDE's proven window manager now supports advanced compositing features.
	  Hardware accelerated painting takes care of a smoother and more intuitive interaction
	  with windows. -->
	  "के-विन" विंडो-नियंत्रक आता विकसित प्रणाली दृश्य दाखवतो. हार्डवेअर सहायक दृतचित्रकला प्रणाली व डेस्कटॉपचा वापर सरल करते. 
	</li>
	<li><!--Oxygen is the के.डी.ई.  4.0 artwork. Oxygen provides a consistent, easy on the eye and
	    beautiful artwork concept.-->
	    के.डी.ई.  ४ ची नवीन "ऑक्सिजन" चित्रकला सर्व प्रणाली दृश्य सुसंगत व सुंदर करते.
	</li>
</ul>
<!-- Learn more about के.डी.ई. 's new desktop interface in the 
<a href="desktop.php">KDE 4.0 Visual Guide</a>.
-->
के.डी.ई.  च्या नवीन डेस्कटॉपची अधिक माहिती
<a href="desktop.php">के.डी.ई.  ४.० प्रणाली दृश्य मार्गदर्शकात</a> मिळू शकते.

<!-- h3>Applications</h3 -->
<h3>प्रणाली</h3>
<ul>
<!--	<li>Konqueror is KDE's proven web browser. Konqueror is light-weight, well integrated,
	    and supports the newest standards such as CSS 3.</li>-->
	<li>कॉंकरर म्हणजेच के.डी.ई.  चा सिद्ध इंटरनेट ब्राऊझर. कॉंकरर छोटा व चपळ असून सर्व नवीन मानक (जसे की सी.एस.एस. ३) पाळतो.</li>

<!--	<li>Dolphin is KDE's new filemanager. Dolphin has been developed with usability in mind 
		and is an easy-to-use, yet powerful tool.
	</li>-->
	<li>के.डी.ई.  चा नवीन दस्तावेज प्रबंधक म्हणजेच डॉलफिन. डॉलफिनचे आरेखन त्याची उपयोगिता लक्षात घेउन केले असल्यामुळे
	    त्याचा वापर प्रभावी असून वापरायला अतीशय सोपा आहे.
	</li>

<!--	<li>With System Settings, a new control centre interface has been introduced. The
	    KSysGuard system monitor makes it easy to monitor and control system resources
	    and activity.
	</li>-->
	<li>प्रणाली समायोजनाकरिता नवीन प्रकारचे नियंत्रणकेंद्र बनवले आहे. "के सिसगार्ड"  मधील या सुधारणा 
            सर्व प्रणाली व साधनांच्या निरीक्षणात व नियंत्रणात सरलता आणतात.
	</li>

<!--	<li>Okular, the KDE 4 document viewer supports lots of file formats.
	    Okular is one of the many KDE 4 applications that has been improved
	    in collaboration with the <a href="http://openusability.org">OpenUsability Project</a>
	</li>-->
	<li>के.डी.ई.  ४ ची दस्तावेज प्रणाली ऑक्युलर बर्याच प्रकारचे दस्तावेज दाखवू शकते. ऑक्युलर मधील सुधारणा या
	    <a href="http://openusability.org">OpenUsability Project</a> च्या सहायतेनी केलेल्या सुधारणांपैकी एक आहे.
	</li>

<!--	<li>Educational Applications are among the first applications that have been ported and
	    developed using KDE 4 technology. Kalzium, a graphical periodic table of elements
	    and the Marble Desktop Globe are only two of many gems among the educational
	    applications. Read more about Educational Applications in the 
		<a href="education.php">Visual Guide</a>
	</li>-->
	<li>के.डी.ई.  ४ संरचनेत सर्वप्रथम शिक्षणप्रणाली   बनवल्या. या रत्नांतच आहेत रसायनशास्त्राची आवर्तसारणी शिकवायला कँलझियम, व भूगोलासाठी मार्बल पृथ्वी.
शिक्षणप्रणालीची अधिक माहिती <a href="education.php">या शिक्षणप्रणाली  मार्गदर्शकात</a> उपलब्ध आहे.
	</li>

<!--	<li>Lots of the KDE Games have been updated. KDE Games such as KMines, a minesweeper game
	    and KPat, a patience game have been facelifted. Thanks to new vector artwork and
	    graphical capabilities, the games have been made more resolution independent.
	</li>-->
	<li>के.डी.ई.  मधील केमाईन्स, केपँट, व इतर बरेच खेळ सुधारले आहेत. सदिश चित्रकला आधारित हे खेळ आता चित्रांश-घनतेवर अवलंबून नाहीत.</li>
</ul>
<!--Some applications are introduced in more detail in the <a href="applications.php">KDE 4.0 
Visual Guide</a>.-->
<a href="applications.php">के.डी.ई.  ४.० प्रणाली मार्गदर्शकात</a>. काही निवडक प्रणालींची अधिक माहिती उपलब्ध आहे.

<?php
screenshot("dolphin-systemsettings-kickoff_thumb.jpg", "dolphin-systemsettings-kickoff.jpg", "center",
	/*"Filemanager, System Settings and Menu in action"*/
	"दस्तावेज प्रबंधक, नियंत्रणकेंद्र, व  प्रणालीसूची" );
?>

<!-- <h3>Libraries</h3> -->
<h3>प्रणालीकोष</h3>
<p>
<ul>
<!--	<li>Phonon offers applications multimedia capabilities such as playing audio and video.
	    Internally, Phonon makes use of various backends, switchable at runtime. 
		The default backend for के.डी.ई.  4.0 will be the Xine backend supplying outstanding support 
		for various formats. Phonon also allows the user to choose output devices based on the 
		type of multimedia.
	</li>-->
	<li>फोनॉन मार्फत सर्व के.डी.ई.  प्रणालीना   बहुमाध्यमिक क्षमता (जसे की गाणी वाजवणे अथवा चलचित्र दाखवणे). 
फोनॉनची आंतरिक प्रक्रिया उपस्थित संरचनांपैकी कोणतीही वापरू शकते, तर सामान्यतः झाईन (Xine) संरचना वापरली आहे.
</li>

<!--	<li>The Solid hardware integration framework integrates fixed and removable devices
	    into KDE applications. Solid also interfaces with the underlying system's
	    power management capabilities, handles network connectivity and integration of
	    Bluetooth devices. Internally, Solid combines the powers of HAL, NetworkManager and
		the Bluez bluetooth stack, but those components are replacable without breaking 
		applications to provide maximum portability.
	</li>-->
	<li>"सॉलिड" हार्डवेअर-संघटन संरचना स्थायी व अस्थायी यंत्र वापरणे, तसेच ऊर्जा-प्रबंधन करणे सोयीचे करते. याशिवाय सॉलिड नेटवर्क, वायरलेस नेटवर्क व ब्लूटूथ
सहजपणे वापरण्याकरिता शैली उपलब्ध करते. याकरिता दिलेली सामान्य प्रणाली जर आपल्याला बदलायची गरज पडली तरी असा बदल उर्वरित प्रणाली बंद न करता 
करू शकता.
	</li>

<!--	<li>KHTML is the webpage rendering engine used by Konqueror, KDE's web browser. KHTML is
	    light-weight and supports modern standards such as CSS 3. KHTML was also the first
	    engine to pass the famous Acid 2 test. 
	</li>-->
	<li>कॉंकररची आंतरिक वेबपेज दाखवण्याची प्रक्रिया म्हणजेच के.एचटीएमएल  (KHTML). KHTML छोटा व चपळ असून सर्व नवीन मानक (जसे की सी.एस.एस. ३) पाळतो.
सर्वमान्य ए.सी.आय.डी.२ परीक्षा सर्वप्रथम KHTML पास झाला.
	</li>

<!--	<li>The ThreadWeaver library, which comes with kdelibs, provides a high-level interface
	    to make better use of today's multi-core systems, making KDE applications feel smoother
	    and more efficiently using resources available on the system.
	</li>-->
	<li>के.डी.ई.  बरोबरचा "थ्रेडवीव्हर" प्रणालीकोष हा अलीकडे मिळू लागलेल्या बहुआंतरिक संगणकाचा पुरेपूर उपयोग करू शकतो. यामुळेच
सर्व प्रणाली वापरणे आनंददायक होते, तसेच सर्व साधनांचा निपुण वापर होतो.
	</li>

<!--	<li>Being built on Trolltech's Qt 4 library, KDE 4.0 can make use of the advanced visual
	    capabilities and smaller memory footprint of this library. kdelibs provides an outstanding
		extension of the Qt library, adding lots of high-level functionality and convenience to
		the developer.
	</li>-->
	<li>ट्रोलटेकच्या क्यू.टी. ४ प्रणालीकोषावर आधारित के.डी.ई.  ४ मधे बर्याच आधुनिक चित्रक्षमता असून कमी स्मृतीक्षमता आवश्यक आहे.
            क्यू.टी. प्रणालीकोषाचा पूरक के.डी.ई.  प्रणालीकोष हा अभियांत्रिकांना क्रियात्मकता व सुविधा पुरवतो.
	</li>

</ul>
</p>
<!--<p>के.डी.ई. 's <a href="http://techbase.के.डी.ई. .org">TechBase</a> knowledge library has more information
about the KDE libraries.</p>-->
<p>के.डी.ई. च्या <a href="http://techbase.kde.org">टेक बेस</a> माहितीसंग्रहात के.डी.ई.  प्रणालीकोषाची अतिरिक्त माहिती उपलब्ध आहे.</p>


<!-- <h4>Take a guided tour...</h4> -->
<h4>के.डी.ई.  ४ ची मार्गदर्शित सैर...</h4>

<!--<p>
The <a href="guide.php">KDE 4.0 Visual Guide</a> provides a quick overview of various new
and improved KDE 4.0 technologies. Illustrated with lots of screenshots, it walks you
through the different parts of KDE 4.0 and shows some of the exciting new technologies and
improvements for the user. New features of the <a href="desktop.php">desktop</a> gets
you started, <a href="applications.php">applications</a> such as System Settings, Okular the
document viewer and Dolphin the filemanager are introduced. 
<a href="education.php">Educational applications</a> are shown as well as 
<a href="games.php">Games</a>.
</p>-->

<p>
<a href="guide.php">के.डी.ई.  ४ - मार्गदर्शित सैर</a> मधे के.डी.ई.  च्या नवीन व आधुनिक प्रौद्योगिक सुधारणांचा सारांश आहे.
सचित्र स्पष्टीकरणासह आपल्याला के.डी.ई.  मधील सुधारणा तसेच नवीन वैशिष्ट्य बघायला मिळतील.
सर्वप्रथम <a href="desktop.php">डेस्कटॉप</a>ची वैशिष्ट्य दर्शावली आहेत. त्याप्रमाणेच विविध 
<a href="applications.php">प्रणाली</a> जसे की नियंत्रणकेंद्र,  दस्तावेज प्रणाली "ऑक्युलर",
व दस्तावेज प्रबंधक "डॉलफिन" पेश केले आहेत. अर्थातच 
<a href="education.php">शैक्शणिक प्रणालीही</a> आहेत, व
<a href="games.php">खेळ</a> कोणी विसरू शकेल का?
</p>


<!--<h4>Give it a spin...</h4>-->
<h4>वापरून बघा की...</h4>

<!--<p>
For those interested in getting packages to test and contribute, several
distributions notified us that they will have KDE 4.0 packages available
at or soon after the release. The complete and current list can be found on the
<a href="http://www.kde.org/info/4.0.php">KDE 4.0 Info Page</a>, where you
can also find links to the source code, information about compiling, security
and other issues.
</p>-->
<p>
आता आपल्याल के.डी.ई.  च्या सुधारात मदत करायची असेल अथवा केवळ वापरून बघायचे असेलच!
बर्याच लिनक्स वितरकांनी कळविले आहे की त्यांच्या मार्फत के.डी.ई.  ४ लवकरच मिळेल. यांची अवगत माहिती इंटरनेट वर 
<a href="http://www.kde.org/info/4.0.php">के.डी.ई.  ४.० माहिती</a> सापडेल. तसेच येथे 
प्रणाली सोर्स, संगणक सुरक्षा, इत्यादि माहिती पण असते.
</p>


<p>
<!--The following distributions have notified us of the availability of packages or Live CDs for
KDE 4.0:-->
के.डी.ई.  ४ उपलब्ध करणारे वितरकः
<ul>
	<li>
<!--		An alpha version of KDE4-based <strong>Arklinux 2008.1</strong> is expected
		shortly after this release, with an expected final release within 3 or 4 weeks.-->
		आर्कलिनक्सचे तपासणी रूप लवकरच उपलब्ध होईल, व पूर्ण रूप साधारण १ महिन्यात मिळेल.
	</li>
	<li>
<!--		<strong>Fedora</strong> will feature KDE 4.0 in Fedora 9, to be <a
		href="http://fedoraproject.org/wiki/Releases/9">released</a>
		in April, with Alpha releases being available from
		24th of January.  KDE 4.0 packages are in the pre-alpha <a
		href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a> repository.-->
		<strong>फेडोरा</strong> च्या नौव्या आवृत्ती मधे के.डी.ई.  ४ असेल, साधारणतः
		<a href="http://fedoraproject.org/wiki/Releases/9">एप्रिल मधे</a>, तर 
		तपासणी रूप साधारणतः २४ जानेवारीला उपलब्ध होईल. सध्या के.डी.ई.  ४ फेडोराच्या 
		<a href="http://fedoraproject.org/wiki/Releases/Rawhide">रॉहाईड</a>
		संग्रहात आहे.
	</li>
	<li>
<!--		<strong>Gentoo Linux</strong> provides KDE 4.0 builds on
		<a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.-->
		<strong>गेन्टू लिनक्स</strong> त्यांच्या <a href="http://kde.gentoo.org">http://kde.gentoo.org</a> वेबसाईटवर
		के.डी.ई.  ४ उपलब्ध करतात.
	</li>
	<li>
<!--		<strong>Ubuntu</strong> packages are included in the upcoming "Hardy Heron"
		(8.04) and also made available as updates for the stable "Gutsy Gibbon" (7.10).
		A Live CD is available for trying out KDE 4.0.
		More details can be found in  the <a href="http://kubuntu.org/announcements/kde-4.0.php">
		announcement</a> on Ubuntu.org.-->
		<strong>उबुंतू</strong>नी त्यांच्या नवीन आवृत्ती - हार्डी हेरॉन (८.०४) - मधे के.डी.ई.  ४ सामिल केले आहेच. शिवाय 
		त्यांच्या प्रचलित आवृत्तीसाठी अपडेट दिला आहे. 
		लाईव्ह सी.डी. (अर्थात सी. डी. वरून) उबुंतू चालवायचे असल्यास त्यांच्या वेबसाईटवर उपलब्ध आहे. अधिक माहिती त्यांच्या 
		<a href="http://kubuntu.org/announcements/kde-4.0.php">घोषणेत</a> मिळेल.
	</li>
	<li>
<!--		<strong>openSUSE</strong> packages <a href="http://en.opensuse.org/KDE4">are available</a> 
		for openSUSE 10.3 (
		<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">one-click 
		install</a>) and openSUSE 10.2. A <a href="http://home.kde.org/~binner/kde-four-live/">KDE 
		Four Live CD</a> with these packages is also available. KDE 4.0 will be part of the upcoming 
		openSUSE 11.0 release.-->
		<strong>ओपन सूसे</strong> साठी सुद्धा <a href="http://en.opensuse.org/KDE4">उपलब्ध आहे</a>.
		<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">आवृत्ती १०.३ साठी त्वरित मिळवा</a>
		तसेच आ. १०.२ साठी उपलब्ध आहे, शिवाय <a href="http://home.kde.org/~binner/kde-four-live/">लाईव्ह सी.डी.</a> पण आहे.
		ओपन सूसेच्या आ. ११.० मधे के.डी.ई.  ४ असेल.
		
	</li>
</ul>
</p>

<!--<h2>About KDE 4</h2>-->
<h2>के.डी.ई. ४ संबंधित माहिती</h2>

<p>
<!--KDE 4.0 is the innovative Free Software desktop containing lots of applications
for every day use as well as for specific purposes. Plasma is a new desktop shell developed for
KDE 4, providing an intuitive interface to interact with the desktop and
applications. The Konqueror web browser integrates the web with the desktop. The Dolphin filemanager,
the Okular document reader and the System Settings control center complete the basic desktop set.-->
के.डी.ई.  ४ या आधुनिक मुक्त सॉफ्टवेअर डेस्कटॉपमधे सर्वसाधारण तसेच विशेष वापरासठी प्रणालींचा समावेष आहे. 
कॉंकरर इंटरनेट व डेस्कटॉप एकत्र आणून दोन्हीचा वापर सरल करतो. याबरोबरच प्लाझमा (डेस्कटॉप फलक), 
डॉलफिन (दस्तावेज प्रबंधक), ऑक्युलर (दस्तावेज प्रणाली) व नियंत्रणकेंद्र मिळून प्राथमिक डेस्कटॉप पूर्ण होतो.

<br />
<!--KDE is built on the KDE Libraries which provide easy access to resources on the network by means of KIO and
advanced visual capabilities through Qt4. Phonon and Solid, which are also part of the KDE Libraries
add a multimedia framework and better hardware integration to all KDE applications.-->
के.डी.ई.  चा पाया म्हणजेच के.डी.ई. व क्यू.टी. प्रणालीकोष, जापैकी के.आय.ओ. मार्फत साधने व क्यू.टी.  द्वारा आधुनिक चित्र-करामती सहजपणे वापरता येतात.
फोनॉन व सॉलिड यांमुळे बहुमाध्यमिक तसेच हार्डवेअर-संघटन संरचना सर्व के.डी.ई.  प्रणालींना वापरण्याची क्षमता मिळते.

</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
