<?php
  $page_title = "KDE 1.1.2 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<center>
<h1>K DESKTOP ENVIRONMENT 1.1.2 SHIPS</h1>
<h4>New Release of Award-Winning Linux Desktop GUI Features<br>
               Look and Feel Improvements and Bugfixes</h2>
</center>
<b>Internet - Sep 13,  1999</b>  -  The  <a href="../">K  Desktop  Environment  (KDE)  Team</a>
today announced the shipment of  KDE  1.1.2,  the
latest release of the Linux de facto standard desktop.
<p>
"The last few KDE releases stressed stability  and  power",  explained
Kurt Granroth, a member  of  the  KDE  core  team.   "With  the  1.1.2
release, we  have  also  made  major  improvements  to  the  aesthetic
aspects of the user experience".
<p>
KDE 1.1.2 includes the KDE Theme Manager, a new tool enabling users to
change  their  desktop  theme  --  ranging  from  colors  and   window
decorations to icons and sounds -- with a  few  simple  mouse  clicks.
This release includes hundreds of new  high  quality  application  and
tool bar icons, created by the very active  KDE  Artists  Team,  which
dramatically enhances the appearance of the desktop,  the  panel,  and
various applications.
<p>
Further changes include:
<ul>
<li> Significant enhancements and bugfixes to the KDE mail client
    (kmail);

<li> New translations -- KDE now supports up to 35 languages;

<li> Improved stability and HTML compatability in the KDE  web  browser
    and file manager (kfm);

<li> Numerous bugfixes in many other packages.
</ul>
<p>KDE 1.1.2 is available for free download from  numerous  <a href="../mirrors/ftp.php">mirrors</a>
and  also  from  KDE's  primary  <a
href="ftp://ftp.kde.org/pub/kde/stable/1.1.2/distribution/">ftp
server</a>.
Practically  all  Linux  and  *BSD  distributions  are   expected   to
incorporate KDE 1.1.2 in future releases.
<p>
Technical support for KDE is provided by Linux vendors who include KDE
in their distributions.  Informal support is also  available  from  an
international group of KDE  enthusiasts  by  joining  the  <a href="news://comp.windows.x.kde">KDE  Usenet
discussion group</a> or  one  of  the  KDE
<a href="http://www.kde.org/mailinglists/">mailing lists</a>

<h4>
ABOUT KDE
</h4>
KDE is a collaborative Open Source project by hundreds  of  developers
worldwide to create a sophisticated, customizable and  stable  desktop
environment employing a network-transparent, intuitive user interface.
KDE  includes   applications   for   email,   web   browsing,   system
administration,  dial-up   networking,   scheduling,   Palm   Pilot(r)
synchronization, and many other tasks.  KDE is working  proof  of  the
power of the open source software development model.
<p>
The elegance and usefulness of the KDE environment has been recognized
through multiple awards, including Linux  World  magazine's  "Editor's
Choice" award in the Desktop  Environment  category,  and  Ziff-Davis'
"Innovation of the year 1998/1999" award in the Software category.
<p>
For more information about KDE, please visit
<a href="../whatiskde/">http://www.kde.org/whatiskde/</a>.

<?php
  include "footer.inc"
?>
