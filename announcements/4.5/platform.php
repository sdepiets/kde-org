<?php
  $release = '4.5';
  $release_full = '4.5.0';
  $page_title = "KDE Development Platform 4.5.0 gains performance, stability, new high-speed cache and support for WebKit";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<p align="justify">
KDE, an international Free Software community, proudly announces the release of the KDE Platform 4.5. This basis for the Plasma Workspaces and Applications developed by KDE has seen significant improvements in many areas. Besides the introduction of new technology, a lot of work has been spent on improving the performance and stability of the underlying pillars of KDE development. KDE Platform has progressed in many areas, both in terms of infrastructure and of development tools.
</p>

<p align="justify">
<ul>
    <li>After years of work, the integration for <b>WebKit</b> has become part of our libraries. WebKit now provides a integration with password storage and other features in Konqueror, the KDE web browser. Other applications can make use of WebKit to display content with the same level of integration as KHTML.</li>

    <li>KHTML continues to develop, gaining support for XPath queries.</li>

    <li>Web content performance has been increased using a new scheduling policy, allowing for more parallel downloads of content from distinct servers, in turn enabling pages to be rendered faster as more of their components are loaded together. The page-rendering speeds of the KHTML and WebKit engines benefit from these changes.</li>

    <li><b>Plasma Workspaces</b> can now be configured using JavaScript templates which can be distributed as small, separate packages. This makes it possible for system administrators and integrators to easily set up custom Plasma configurations for users, or to change the default setup of a standard Plasma Desktop.</li>

    <li><b>KSharedDataCache</b> is a caching mechanism for applications. This new memory-mapped cache is already used for icons and demonstrates noticeable speed-ups there.</li>

    <li>Application developers making use of <b>Kate’s SmartRange interface</b> for their editor components now have several new interfaces to use, details for which can be found in <a href="http://kate-editor.org/2010/07/13/kde-4-5-smartrange-movingrange/">this blog post</a>.</li>

    <li>As KDE Games welcome their first full Python-written application, the KDE bindings developers have added Perl bindings to the officially supported language set. Ruby support in the KDE Platform has also been <a href="http://www.arnorehn.de/blog/2010/07/and-the-bindings-keep-rocking-writing-ruby-kio-slaves/">significantly improved</a>.</li>

    <li>The Phonon Multimedia library can now optionally use PulseAudio.</li>

</ul>
These changes form the underpinnings for many improvements in <a href="plasma.php">Plasma Desktop and Plasma Netbook</a> and in the <a href="applications.php">KDE Applications 4.5</a> that have been released in tandem with the KDE Development Platform 4.5.0.
</p>

<h2>Plans and Work in Progress</h2>
<p align="justify">
The KDE platform team is working on making the KDE Platform more suitable for mobile devices. With the introduction of KDE's platform profiles, it is possible to compile KDE with limited feature sets in order to minimise their footprint. These profiles are still being elaborated to meet mobile apps' needs. The plans include using it for the mobile versions of Kontact, KOffice and the Plasma Mobile workspace. The BlueDevil framework will add better support for many Bluetooth devices in upcoming versions. BlueDevil, which is based on BlueZ, found on many Linux systems, is maturing quickly and is currently going through a series of beta releases. Its developers are aiming at a release included with the KDE Platform 4.6 in January 2011.
</p>

<h4>Installing the KDE Development Platform</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Released Today:</h2>


<a href="plasma.php">
<img src="images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.5.0" width="64" height="64" />
</a>

<h2>Plasma Desktop and Netbook 4.5.0: improved user experience</h2>
<p align="justify">
10th August, 2010. KDE today releases the Plasma Desktop and Plasma Netbook Workspaces 4.5.0. Plasma Desktop received many usability refinements. The Notification and Job handling workflows have been streamlined. The notification area has been cleaned up visually, and its input handling across applications is now made more consistent by the extended use of the Freedesktop.org notification protocol first introduced in Plasma's previous version. <a href="plasma.php"><b>Read The Full Announcement</b></a>
</p>


<a href="applications.php">
<img src="images/applications.png" class="app-icon" alt="The KDE Applications 4.5.0"/>
</a>
<h2>KDE Applications 4.5.0 enhance usability and bring routing</h2>
<p align="justify">
10th August, 2010. The KDE team today releases a new version of the KDE Applications. Many educational titles, tools, games and graphical utilities have seen further enhancement and usability improvements. Routing backed by OpenRouteService has made its entry in Marble, the virtual globe. Konqueror, KDE's webbrowser can now be configured to use WebKit with the KWebKit component available in the Extragear repository. <a href="applications.php"><b>Read The Full Announcement</b></a>
</p>

<?php
  include("footer.inc");
?>
