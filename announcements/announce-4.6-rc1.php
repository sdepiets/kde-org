<?php
  $page_title = "KDE Software Compilation 4.6 RC1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- // Boilerplate -->

<h3 align="center">
  KDE Software Compilation 4.6 RC1 Released: Codename Chanukkah
</h3>

<p align="justify">
  <strong>
KDE Community Ships First Release Candidate of the 4.6 Free Desktop, Applications and
Development Platform
</strong>
</p>

<p align="justify">
December 23rd, 2010. Right before christmas, KDE has published the first candidate for the upcoming release of KDE 4.6.0. The focus at this stage is on fixing bugs and completing translations and artwork. As such, the rework of the Oxygen icon set is nearing completion, many bugs reported by testers in the past weeks have been fixed and stabilization is still in full swing.<br />
KDE's release team has decided to not include the new release of KDE-PIM, containing the Kontact groupware client and the new KMail2 which is based on the Akonadi groupware cache in this release due to open issue with migration of large sets of data from the traditional client. These bugs are being worked on as we speak, but the risk at this point is too high to include the new Kontact in the 4.6.0 release. Therefore, it has been decided to release KMail2 and its companions together with one of the subsequent 4.6 releases, likely 4.6.1 one month later. A 3rd beta version of the kdepim module is <a href="http://www.kdedevelopers.org/node/4365">available</a> already.<br />
As the other components of the frameworks, workspaces and applications are nearing release date, KDE encourages testers to give their favorite software some final thorough testing. The last chance for feedback will be the second release candidate, planned for January, 5th 2011. The final release of 4.6.0 will be available on January, 26th 2011.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="announce-4.6-beta2.png">
<img src="announce-4.6-beta2_thumb.png" align="center" alt="Activities Gallery in 4.6 RC1" title="Activities Gallery in 4.6 RC1" />
</a>
<br />
<em>Activities Gallery in 4.6 RC1</em>
</div>

<p>
To find out more about the KDE Plasma desktop and applications, please also refer to the
<a href="http://www.kde.org/announcements/4.5/">4.5.0</a>,
<a href="http://www.kde.org/announcements/4.4/">4.4.0</a>,
<a href="http://www.kde.org/announcements/4.3/">4.3.0</a>,
<a href="http://www.kde.org/announcements/4.2/">4.2.0</a>,
<a href="http://www.kde.org/announcements/4.1/">4.1.0</a> and
<a href="http://www.kde.org/announcements/4.0/">4.0.0</a> release
notes.
</p>



<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/unstable/4.5.90/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>


<!-- // Boilerplate again -->

<h4>
  Installing KDE SC 4.6 RC1 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE SC 4.6 RC1
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/unstable/4.5.90/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.5.90.php">KDE SC 4.6 RC1 Info
Page</a>.
</p>

<h4>
  Compiling KDE SC 4.6 RC1
</h4>
<p align="justify">
  <a name="source_code"></a>
  The complete source code for KDE SC 4.6 RC1 may be <a
href="http://download.kde.org/unstable/4.5.90/src/">freely downloaded</a>.
Instructions on compiling and installing KDE SC 4.6 RC1
  are available from the <a href="/info/4.5.90.php#binary">KDE SC 4.6 RC1 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
