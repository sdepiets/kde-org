<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  require('../aether/config.php');

  $pageConfig = array_merge($pageConfig, [
      'title' => i18n_noop("KDE Ships KDE Applications 19.08.0"),
      'cssFile' => '/css/announce.css',
      'image' => '/announcements/announce-applications-1908/social.png',
  ]);

  require('../aether/header.php');
  $site_root = "../";
  $release = 'applications-19.08.0';
  $version_text = "19.08";
  $version_number = "19.08.0";
?>

<script src="/js/use-ekko-lightbox.js" defer></script>

<style>
.img-fluid { padding-left: 1em; }
.releaseAnnouncment { max-width: 900px; }
</style>

<main class="container">
<div class="releaseAnnouncment ml-auto mr-auto">

<h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Applications %1", $version_text)?></h1>

<?php include "./announce-i18n-bar.inc"; ?>

<iframe class="mb-3 mt-2" width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/6443ce38-0a96-4b49-8fc5-a50832ed93ce" frameborder="0" allowfullscreen></iframe>

<p>
<?php i18n("August 15, 2019.")?>
<br />
<?php print i18n_var("The KDE community is happy to announce the release of KDE Applications %1.", $version_text);?>
</p>

<p><?php i18n("This release is part of KDE's commitment to continually provide improved versions of the programs we ship to our users. New versions of Applications bring more features and better-designed software that increases the usability and stability of apps like Dolphin, Konsole, Kate, Okular, and all your other favorite KDE utilities. Our aim is to ensure you remain productive, and to make KDE software easier and more enjoyable for you to use."); ?></p>

<p><?php print i18n_var("We hope you enjoy all the new enhancements you'll find in %1!", $version_text); ?></p>

<h2><?php print i18n_var("What's new in KDE Applications %1", $version_text);?></h2>

<p><?php i18n_var("More than %1 bugs have been resolved. These fixes re-implement disabled features, normalize shortcuts, and solve crashes, making your applications friendlier and allowing you to work and play smarter.", "170"); ?></p>

<h3><a href="/applications/system/org.kde.dolphin"><?php i18n("Dolphin")?></a></h3>

<p><?php i18n("Dolphin is KDE's file and folder explorer which you can now launch from anywhere using the new global <keycap>Meta + E</keycap> shortcut. There is also a new feature that minimizes clutter on your desktop. When Dolphin is already running, if you open folders with other apps, those folders will open in a new tab in the existing window instead of a new Dolphin window. Note that this behavior is now on by default, but it can be disabled."); ?></p>

<p><?php i18n("The information panel (located by default to the right of the main Dolphin panel) has been improved in several ways. You can, for example, choose to auto-play media files when you highlight them in the main panel, and you can now select and copy the text displayed in the panel. If you want to change what the information panel shows, you can do so right there in the panel, as Dolphin does not open a separate window when you choose to configure the panel."); ?></p>

<p><?php i18n("We have also solved many of the paper cuts and small bugs, ensuring that your experience of using Dolphin is much smoother overall."); ?></p>

<figure class="text-center mt-3 mb-3">
    <img class="img-fluid" alt="<?php i18n("Dolphin new bookmark feature"); ?>" src="/announcements/announce-applications-1908/dolphin_bookmark.png" />
    <figcaption><?php i18n("Dolphin new bookmark feature"); ?></figcaption>
</figure>

<h3><a href="/applications/graphics/org.kde.gwenview"><?php i18n("Gwenview"); ?></a></h3>

<p><?php i18n("Gwenview is KDE's image viewer, and in this release the developers have improved its thumbnail viewing feature across the board. Gwenview can now use a \"Low resource usage mode\" that loads low-resolution thumbnails (when available). This new mode is much faster and more resource-efficient when loading thumbnails for JPEG images and RAW files. In cases when Gwenview cannot generate a thumbnail for an image, it now displays a placeholder image rather than re-using the thumbnail of the previous image. The problems Gwenview had with displaying thumbnails from Sony and Canon cameras have also been solved."); ?></p>

<p><?php i18n("Apart from changes in the thumbnail department, Gwenview has also implemented a new “Share” menu that allows sending images to various places, and correctly loads and displays files in remote locations accessed using KIO. The new version of Gwenview also displays vastly more EXIF metadata for RAW images."); ?></p>

<figure class="text-center mt-3 mb-3">
    <img class="img-fluid" alt="<?php i18n("Gwenview new “Share” menu"); ?>" src="/announcements/announce-applications-1908/gwenview_share.png" />
    <figcaption><?php i18n("Gwenview new “Share” menu"); ?></figcaption>
</figure>

<h3><a href="/applications/office/org.kde.okular"><?php i18n("Okular"); ?></a></h3>

<p><?php i18n("Developers have introduced many improvements to annotations in Okular, KDE's document viewer. Apart from the improved UI for annotation configuration dialogs, line annotation can now have various visual flourishes added to the ends, allowing them to be turned into arrows, for example. The other thing you can do with annotations is expand and collapse them all at once."); ?></p>

<p><?php i18n("Okular's support for EPub documents has also received a push in this version. Okular doesn't crash anymore when attempting to load malformed ePub files, and its performance with large ePub files has improved significantly. The list of changes in this release includes improved page borders and Presentation mode's Marker tool in High DPI mode."); ?></p>

<figure class="text-center mt-3 mb-3">
    <img style="max-width: 75%;" alt="<?php i18n("Okular annotation tool settings with the new Line end option"); ?>" src="/announcements/announce-applications-1908/okular_line_end.png" />
    <figcaption><?php i18n("Okular annotation tool settings with the new Line end option"); ?></figcaption>
</figure>

<h3><a href="https://kate-editor.org"><?php i18n("Kate"); ?></a></h3>

<p><?php i18n("Thanks to our developers, three annoying bugs have been squashed in this version of KDE's advanced text editor. Kate once again brings its existing window to the front when asked to open a new document from another app. The \"Quick Open\" feature sorts items by most recently used, and pre-selects the top item. The third change is in the \"Recent documents\" feature, which now works when the current configuration is set up not to save individual windows’ settings."); ?></p>

<h3><a href="https://konsole.kde.org"><?php i18n("Konsole"); ?></a></h3>

<p><?php i18n("The most noticeable change in Konsole, KDE's terminal emulator application, is the boost to the tiling feature. You can now split the main pane any which way you want, both vertically and horizontally. The subpanes can then be split again however you want. This version also adds the ability to drag and drop panes, so you can easily rearrange the layout to fit your workflow."); ?></p>

<p><?php i18n("Besides that, the Settings window has received an overhaul to make it clearer and easier to use."); ?></p>

<video class="img-fluid" loop muted autoplay data-peertube="https://peertube.mastodon.host/videos/embed/ae19d1ec-dee7-41b2-a9b7-d78d0ea63411">
    <source src="/announcements/announce-applications-1908/konsole-tabs.webm" type="video/webm">
</video>

<h3><a href="/applications/utilities/org.kde.spectacle"><?php i18n("Spectacle"); ?></a></h3>

<p><?php i18n("Spectacle is KDE's screenshot application and it is getting more and more interesting features with each new version. This version is no exception, as Spectacle now comes with several new features that regulate its Delay functionality. When taking a time-delayed screenshot, Spectacle will display the time remaining in its window title. This information is also visible in its Task Manager item."); ?></p>

<p><?php i18n("Still on the Delay feature, Spectacle's Task Manager button will also show a progress bar, so you can keep track of when the snap will be taken. And, finally, if you un-minimize Spectacle while waiting, you will see that the “Take a new Screenshot” button has turned into a \"Cancel\" button. This button also contains a progress bar, giving you the chance to stop the countdown."); ?></p>

<p><?php i18n("Saving screenshots has a new functionality, too. When you have saved a screenshot, Spectacle shows a message in the app that allows you to open the screenshot or its containing folder."); ?></p>

<video class="img-fluid mb-3 mt-3" loop muted autoplay data-peertube="https://peertube.mastodon.host/videos/embed/1c19fd9c-42e6-4985-9033-59ea5553adee">
    <source src="/announcements/announce-applications-1908/spectacle_progress.webm" type="video/webm">
</video>

<h3><a href="https://kontact.kde.org"><?php i18n("Kontact"); ?></a></h3>

<p><?php i18n("Kontact, KDE's email/calendar/contacts and general groupware suite of programs, brings Unicode color emoji and Markdown support to the email composer. Not only will the new version of KMail let you make your messages look good, but, thanks to the integration with grammar checkers such as LanguageTool and Grammalecte, it will help you check and correct your text."); ?></p>

<figure class="text-center mt-3 mb-3">
    <img class="img-fluid" alt="<?php i18n("Emoji selector"); ?>" src="/announcements/announce-applications-1908/kontact_emoji.png" />
    <figcaption><?php i18n("Emoji selector"); ?></figcaption>
</figure>

<figure class="text-center mt-3 mb-3">
    <img class="img-fluid" alt="<?php i18n("KMail grammar checker integration"); ?>" src="/announcements/announce-applications-1908/kmail_grammar.png" />
    <figcaption><?php i18n("KMail grammar checker integration"); ?></figcaption>
</figure>

<p><?php i18n("When planning events, invitation emails in KMail are no longer deleted after replying to them. It is now possible to move an event from one calendar to another in the event editor in KOrganizer."); ?></p>

<p><?php i18n("Last but not least, KAddressBook can now send SMS messages to contacts through KDE Connect, resulting in a more convenient integration of your desktop and mobile devices."); ?></p>

<h3><a href="https://kdenlive.org/"><?php i18n("Kdenlive"); ?></a></h3>

<p><?php i18n("The new version of Kdenlive, KDE's video editing software, has a new set of keyboard-mouse combos that will help you become more productive. You can, for example, change the speed of a clip in the timeline by pressing CTRL and then dragging on the clip, or activate the thumbnail preview of video clips by holding Shift and moving the mouse over a clip thumbnail in the project bin. Developers have also put a lot of effort into usability by making 3-point editing operations consistent with other video editors, which you will surely appreciate if you're switching to Kdenlive from another editor."); ?></p>

<figure class="text-center mt-3 mb-3">
    <img class="img-fluid" alt="<?php print i18n_var("Kdenlive %1", $version_numer); ?>" src="https://cdn.kde.org/screenshots/kdenlive/19-08.png" />
    <figcaption><?php print i18n_var("Kdenlive %1", $version_number); ?></figcaption>
</figure>

<hr />

<p><?php print i18n_var("There are many more changes of course, that you can view in the <a href=\"%1\">complete changelog</a>. We have added dozens of new features and corrected bugs and improved usability throughout, making using KDE applications even easier and more fun. Look out for the update in your distro or check out the downloads below.", "fulllog_applications-aether.php?version=".$version_number); ?></p>

<!-- Boilerplate -->

<section class="row get-it">
    <article class="col-md">
        <h2><?php i18n("Package Downloads");?></h2>
        <p>
            <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
        </p>
        <p><a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Package download wiki page");?></a></p>
    </article>

    <article class="col-md">
        <h2><?php i18n("Linux App Stores");?></h2>
        <p>
            <?php print i18n_var("Flathub contains <a href='https://flathub.org/apps/search/kde'>%1 packages from KDE applications</a> updated at release time.", 35);?>
        </p>
        <a class="text-center" href="https://flathub.org/apps/search/kde"><img alt="<?php i18n("Get it from the Flathub");?>" src="/announcements/flathub.png" class="img-fluid mb-2" /></a>
        <p class="mt-4">
            <?php print i18n_var("The Snap Store contains <a href='https://snapcraft.io/publisher/kde'>%1 packages from KDE applications</a> updated at release time.", 52);?>
        </p>
        <a class="text-center" href="https://snapcraft.io/publisher/kde"><img alt="<?php i18n("Get it from the Snap Store");?>" src="/announcements/snapcraft.png" class="img-fluid" /> </a>
    </article>

    <article class="col-md">
        <h2><?php i18n("Source Downloads");?></h2>
        <p><?php print i18n_var("The complete source code for KDE Applications %1 may be <a href='https://download.kde.org/stable/applications/%2/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-%3.php'>KDE Applications %4 Info Page</a>.", $version_text, $version_number, $version_number, $version_text);?></p>
    </article>
</section>

<h2><?php i18n("Supporting KDE");?></h2>

<p><?php i18n("KDE is a <a href='https://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.");?></p>

<?php include($site_root . "/contact/about_kde.inc"); ?>

<h2><?php i18n("Press Contacts");?></h2>

<?php include($site_root . "/contact/press_contacts.inc"); ?>

<script>
const video = document.createElement('video');
if (video.canPlayType('video/webm; codecs="vp8, vorbis"') !== "probably") {
    // safari :(
    // change link to peertube
    const width = document.querySelector('.releaseAnnouncment').clientWidth;
    const height = 315 / 560 * width;
    document.querySelectorAll('video').forEach(video => {
        peertube_link = video.dataset.peertube;
        const iframe = document.createElement('iframe');
        iframe.width = width;
        iframe.height = height;
        iframe.sandbox = "allow-same-origin allow-scripts";
        iframe.src = peertube_link;
        iframe.frameborder = 0;
        iframe.allowfullscreen = true;
        video.replaceWith(iframe);
    });
}

function resize(event) {
    const width = document.querySelector('.releaseAnnouncment').clientWidth;
    const height = 315 / 560 * width;
    document.querySelectorAll('iframe').forEach(iframe => {
        iframe.width = width;
        iframe.height = height;
    });
}

window.addEventListener('resize', resize);
resize();
</script>

</div>
</main>
<?php
  require('../aether/footer.php');
?>
