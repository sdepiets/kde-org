<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.13.0");
  $site_root = "../";
  $release = '5.13.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
August 12, 2015. KDE today announces the release
of KDE Frameworks 5.13.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("New frameworks");?></h3>

<ul>
<li><?php i18n("KFileMetadata: file metadata and extraction library");?></li>
<li><?php i18n("Baloo: file indexing and searching framework");?></li>
</ul>

<h3><?php i18n("Changes affecting all frameworks");?></h3>

<ul>
<li><?php i18n("The Qt version requirement has been bumped from 5.2 to 5.3");?></li>
<li><?php i18n("Debug output has been ported to categorized output, for less noise by default");?></li>
<li><?php i18n("Docbook documentation has been reviewed and updated");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Fix crash in directories-only file dialog");?></li>
<li><?php i18n("Don't rely on options()-&gt;initialDirectory() for Qt &lt; 5.4");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Add man pages for kapidox scripts and update maintainer info in setup.py");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("KBookmarkManager: use KDirWatch instead of QFileSystemWatcher to detect user-places.xbel being created.");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("HiDPI fixes for KLineEdit/KComboBox");?></li>
<li><?php i18n("KLineEdit: Don't let the user delete text when the lineedit is readonly");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Don't recommend to use deprecated API");?></li>
<li><?php i18n("Don't generate deprecated code");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Add Kdelibs4Migration::kdeHome() for cases not covered by resources");?></li>
<li><?php i18n("Fix tr() warning");?></li>
<li><?php i18n("Fix KCoreAddons build on Clang+ARM");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("KDBusService: document how to raise the active window, in Activate()");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Fix deprecated KRun::run call");?></li>
<li><?php i18n("Same behavior of MouseArea to map coords of filtered child events");?></li>
<li><?php i18n("Detect initial face icon being created");?></li>
<li><?php i18n("Don't refresh the entire window when we render the plotter (bug 348385)");?></li>
<li><?php i18n("add the userPaths context property");?></li>
<li><?php i18n("Don't choke on empty QIconItem");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("kconfig_compiler_kf5 moved to libexec, use kreadconfig5 instead for the findExe test");?></li>
<li><?php i18n("Document the (suboptimal) replacements for KApplication::disableSessionManagement");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("change sentence about reporting bugs, ack'ed by dfaure");?></li>
<li><?php i18n("adapt german user.entities to en/user.entities");?></li>
<li><?php i18n("Update general.entities: change markup for frameworks + plasma from application to productname");?></li>
<li><?php i18n("Update en/user.entities");?></li>
<li><?php i18n("Update book and man page templates");?></li>
<li><?php i18n("Use CMAKE_MODULE_PATH in cmake_install.cmake");?></li>
<li><?php i18n("BUG: 350799 (bug 350799)");?></li>
<li><?php i18n("Update general.entities");?></li>
<li><?php i18n("Search for required perl modules.");?></li>
<li><?php i18n("Namespace a helper macro in the installed macros file.");?></li>
<li><?php i18n("Adapted key name tranlations to standard tranlations provided by Termcat");?></li>
</ul>

<h3><?php i18n("KEmoticons");?></h3>

<ul>
<li><?php i18n("Install Breeze theme");?></li>
<li><?php i18n("Kemoticons: make Breeze emotions standard instead of Glass");?></li>
<li><?php i18n("Breeze emoticon pack made by Uri Herrera");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Let KHtml be useable w/o searching for private deps");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Remove temporary string allocations.");?></li>
<li><?php i18n("Remove Theme tree debug entry");?></li>
</ul>

<h3><?php i18n("KIdleTime");?></h3>

<ul>
<li><?php i18n("Private headers for platform plugins get installed.");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Kill unneeded QUrl wrappers");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("New proxy: KExtraColumnsProxyModel, allows to add columns to an existing model.");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Fix the starting Y position for fallback popups");?></li>
<li><?php i18n("Reduce dependencies and move to Tier 2");?></li>
<li><?php i18n("catch unknown notification entries (nullptr deref) (bug 348414)");?></li>
<li><?php i18n("Remove pretty much useless warning message");?></li>
</ul>

<h3><?php i18n("Package Framework");?></h3>

<ul>
<li><?php i18n("make the subtitles, subtitles ;)");?></li>
<li><?php i18n("kpackagetool: Fix output of non-latin text to stdout");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Add AllPhoneNumbersProperty");?></li>
<li><?php i18n("PersonsSortFilterProxyModel now available for usage in QML");?></li>
</ul>

<h3><?php i18n("Kross");?></h3>

<ul>
<li><?php i18n("krosscore: Install CamelCase header \"KrossConfig\"");?></li>
<li><?php i18n("Fix Python2 tests to run with PyQt5");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Fix kbuildsycoca --global");?></li>
<li><?php i18n("KToolInvocation::invokeMailer: fix attachment when we have multi attachement");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("guard default log level for Qt &lt; 5.4.0, fix log cat name");?></li>
<li><?php i18n("add hl for Xonotic (bug 342265)");?></li>
<li><?php i18n("add Groovy HL (bug 329320)");?></li>
<li><?php i18n("update J highlighting (bug 346386)");?></li>
<li><?php i18n("Make compile with MSVC2015");?></li>
<li><?php i18n("less iconloader use, fix more pixelated icons");?></li>
<li><?php i18n("enable/disable find all button on pattern changes");?></li>
<li><?php i18n("Improved search &amp; replace bar");?></li>
<li><?php i18n("remove useless ruler from powermode");?></li>
<li><?php i18n("more slim search bar");?></li>
<li><?php i18n("vi: Fix misreading of markType01 flag");?></li>
<li><?php i18n("Use correct qualification to call base method.");?></li>
<li><?php i18n("Remove checks, QMetaObject::invokeMethod guards itself against that already.");?></li>
<li><?php i18n("fix HiDPI issues with color pickers");?></li>
<li><?php i18n("Cleanup coe: QMetaObject::invokeMethod is nullptr safe.");?></li>
<li><?php i18n("more comments");?></li>
<li><?php i18n("change the way the interfaces are null safe");?></li>
<li><?php i18n("only output warnings and above per default");?></li>
<li><?php i18n("remove todos from the past");?></li>
<li><?php i18n("Use QVarLengthArray to save the temporary QVector iteration.");?></li>
<li><?php i18n("Move the hack to indent group labels to construction time.");?></li>
<li><?php i18n("Fixup some serious issues with the KateCompletionModel in tree mode.");?></li>
<li><?php i18n("Fix broken model design, which relied on Qt 4 behavior.");?></li>
<li><?php i18n("obey umask rules when saving new file (bug 343158)");?></li>
<li><?php i18n("add meson HL");?></li>
<li><?php i18n("As Varnish 4.x introduces various syntax changes compared to Varnish 3.x, I wrote additional, separate syntax highlighting files for Varnish 4 (varnish4.xml,  varnishtest4.xml).");?></li>
<li><?php i18n("fix HiDPI issues");?></li>
<li><?php i18n("vimode: don't crash if the &lt;c-e&gt; command gets executed in the end of a document. (bug 350299)");?></li>
<li><?php i18n("Support QML multi-line strings.");?></li>
<li><?php i18n("fix syntax of oors.xml");?></li>
<li><?php i18n("add CartoCSS hl by Lukas Sommer (bug 340756)");?></li>
<li><?php i18n("fix floating point HL, use the inbuilt Float like in C (bug 348843)");?></li>
<li><?php i18n("split directions did got reversed (bug 348845)");?></li>
<li><?php i18n("Bug 348317 - [PATCH] Katepart syntax highlighting should recognize \u0123 style escapes for JavaScript (bug 348317)");?></li>
<li><?php i18n("add *.cljs (bug 349844)");?></li>
<li><?php i18n("Update the GLSL highlighting file.");?></li>
<li><?php i18n("fixed default colors to be more distinguishable");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("Delete old highlighter");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Fix Windows build");?></li>
<li><?php i18n("Print a warning with error code when opening the wallet by PAM fails");?></li>
<li><?php i18n("Return the backend error code rather than -1 when opening a wallet failed");?></li>
<li><?php i18n("Make the backend's \"unknown cipher\" a negative return code");?></li>
<li><?php i18n("Watch for PAM_KWALLET5_LOGIN for KWallet5");?></li>
<li><?php i18n("Fix crash when MigrationAgent::isEmptyOldWallet() check fails");?></li>
<li><?php i18n("KWallet can now be unlocked by PAM using kwallet-pam module");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("New API taking QIcon parameters to set the icons in the tab bar");?></li>
<li><?php i18n("KCharSelect: Fix unicode category and use boundingRect for width calculation");?></li>
<li><?php i18n("KCharSelect: fix cell width to fit contents");?></li>
<li><?php i18n("KMultiTabBar margins now are ok on HiDPI screens");?></li>
<li><?php i18n("KRuler: deprecate unimplemented KRuler::setFrameStyle(), clean up comments");?></li>
<li><?php i18n("KEditListWidget: remove margin, so it aligns better with other widgets");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Harden NETWM data reading (bug 350173)");?></li>
<li><?php i18n("guard for older Qt versions like in kio-http");?></li>
<li><?php i18n("Private headers for platform plugins are installed.");?></li>
<li><?php i18n("Platform specific parts loaded as plugins.");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Fix method behavior KShortcutsEditorPrivate::importConfiguration");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Using a pinch gesture one can now switch between the different zoom levels of the calenda");?></li>
<li><?php i18n("comment about code duplication in icondialog");?></li>
<li><?php i18n("Slider groove color was hardcoded, modified to use color scheme");?></li>
<li><?php i18n("Use QBENCHMARK instead of a hard requirement on the machine's performance");?></li>
<li><?php i18n("Calendar navigation has been significantly improved, providing a year and decade overview");?></li>
<li><?php i18n("PlasmaCore.Dialog now has an 'opacity' property");?></li>
<li><?php i18n("Make some space for the radio button");?></li>
<li><?php i18n("Don't show the circular background if there's a menu");?></li>
<li><?php i18n("Add X-Plasma-NotificationAreaCategory definition");?></li>
<li><?php i18n("Set notifications and osd to show on all desktops");?></li>
<li><?php i18n("Print useful warning when we can not get valid KPluginInfo");?></li>
<li><?php i18n("Fix potential endless recursion in PlatformStatus::findLookAndFeelPackage()");?></li>
<li><?php i18n("Rename software-updates.svgz to software.svgz");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Add in CMake bits to enable building of Voikko plugin.");?></li>
<li><?php i18n("Implement Sonnet::Client factory for Voikko spell chekers.");?></li>
<li><?php i18n("Implement Voikko based spell checker (Sonnet::SpellerPlugin)");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.13");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.3");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
