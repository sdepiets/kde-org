<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships First Beta of Frameworks 5");
  $site_root = "../";
  $release = '4.98.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<img src="frameworks5TP/images/KDE_QT.jpg" style="float: right; padding: 1ex; margin: 1ex; width: 300px;  border: 0; background-image: none; " alt="<?php i18n("Collaboration between Qt and KDE");?>" />
<?php i18n("April 1, 2014. Today KDE makes available the first beta of Frameworks 5. This release is part of a series of releases leading up to the final version planned for June 2014 following the <a href='announce-frameworks5-alpha.php'>previous alpha</a> last month. This release marks the freeze of source incompatible changes and the introduction of the Frameworks 5 Porting Aids.");?>
<h3><?php i18n("Frameworks 5 Porting Aids");?></h3>
<?php i18n("To ease the porting of KDE Platform 4 based applications, the Frameworks team has brought the 'Porting Aids' group into existence. These Frameworks contain kdelibs4 modules and API's that are being deprecated in KF5 and are provided only to assist applications in porting to KF5. As such these Frameworks will only have a limited support period, currently planned to be three release cycles. Application developers are strongly encouraged to port away from these Frameworks during this support period to prevent dependency on obsolete and unsupported code. Once support is ended, some unofficial development may continue on some modules, but they will not be part of the officially supported Frameworks release.");?>
<br /><br />
<?php i18n("Currently, the following Frameworks belong to this group:
<ul>
<li>khtml</li>
<li>kjs</li>
<li>kjsembed</li>
<li>krunner</li>
<li>kmediaplayer</li>
<li>kdelibs4support*</li>
</ul>");?>
<?php i18n("* kdelibs4support contains deprecated API's from modules which no longer exist or deprecated classes from existing modules.");?>
<br /><br />
<p><?php i18n("For information about Frameworks 5, see <a href='http://dot.kde.org/2013/09/25/frameworks-5'>this article on the dot news site</a>. Those interested in following progress can check out the <a href='https://projects.kde.org/projects/frameworks'>git repositories</a>, follow the discussions on the <a href='https://mail.kde.org/mailman/listinfo/kde-frameworks-devel'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='https://git.reviewboard.kde.org/groups/kdeframeworks/'>review board</a>. Policies and the current state of the project and plans are available at the <a href='http://community.kde.org/Frameworks'>Frameworks wiki</a>. Real-time discussions take place on the <a href='irc://#kde-devel@freenode.net'>#kde-devel IRC channel on freenode.net</a>.");?>
</p>

<h2><?php i18n("Discuss, Spread the Word and See What's Happening: Tag as &quot;KDE&quot;");?></h2>
<p>
<?php i18n("KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for these releases of KDE software.");?>
</p>
<p>
<?php i18n("You can discuss this news story <a href='http://dot.kde.org/2014/04/01/kde-ships-first-beta-frameworks-5'>on the Dot</a>, KDE's news site.");?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing frameworks Beta 1 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("A variety of distributions offers frequently updated packages of Frameworks 5. This includes Arch Linux, AOSC, Fedora, Kubuntu and openSUSE. See <a href='http://community.kde.org/Frameworks/Binary_Packages'>this wikipage</a> for an overview.");?>
</p>

<h4>
  <?php i18n("Compiling frameworks");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php print i18n_var("The complete source code for frameworks %1 may be <a href='http://download.kde.org/unstable/frameworks/%1/'>freely downloaded</a>.", $release);?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='http://jointhegame.kde.org/'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
