<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  require('../aether/config.php');

  $pageConfig = array_merge($pageConfig, [
      'title' => "KDE Ships KDE Applications 18.12.0",
      'cssFile' => '/css/announce.css'
  ]);

  require('../aether/header.php');
  $site_root = "../";
  $release = 'applications-18.12.0';
  $version_text = "18.12";
  $version_number = "18.12.0";
?>

<script src="/js/use-ekko-lightbox.js" defer></script>

<main class="releaseAnnouncment container">

<h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Applications %1", $version_text)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<p>
<?php i18n("December 13, 2018.")?>
<br />
<?php print i18n_var("KDE Applications %1 are now released.", $version_text);?>
</p>

<figure class="videoBlock">
	<iframe width="560" height="315" src="https://www.youtube.com/embed/ALNRQiQnjpo" allowfullscreen='true'></iframe>
</figure>

<p>
<?php i18n("We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!");?>
</p>

<h2><?php print i18n_var("What's new in KDE Applications %1", $version_text);?></h2>

<p>
<?php i18n("More than 140 bugs have been resolved in applications including the Kontact Suite, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsole, Lokalize, Okular, Spectacle, Umbrello and more!");?>
</p>
<p>
<?php print i18n_var("If you would like to read more about the changes in this release, <a href='%1'>head over to the complete changelog</a>. Although a bit intimidating due to its breadth, the changelog can be an excellent way to learn about KDE's internal workings and discover apps and features you never knew you had.", "fulllog_applications-aether.php?version=".$version_number);?>
</p>

<h3 style="clear:both;"><?php i18n("File Management")?></h3>

<figure style="float: right; margin: 0px"><a href="app1812_dolphin01.png" data-toggle="lightbox"><img src="app1812_dolphin01.png" width="250" /></a></figure>
<?php print i18n_var("<a href='%1'>Dolphin</a>, KDE's powerful file manager:", "https://www.kde.org/applications/system/dolphin/");?>
<ul>
<li><?php i18n("New MTP implementation that makes it fully usable for production");?></li>
<li><?php i18n("Huge performance improvement for reading files over the SFTP protocol");?></li>
<li><?php i18n("For thumbnail previews, frames and shadows are now only drawn for image files with no transparency, improving the display for icons");?></li>
<li><?php i18n("New thumbnail previews for LibreOffice documents and AppImage apps");?></li>
<li><?php i18n("Video files larger than 5 MB in size are now displayed on directory thumbnails when directory thumbnailing is enabled");?></li>
<li><?php i18n("When reading audio CDs, Dolphin is now able to change CBR bitrate for the MP3 encoder and fixes timestamps for FLAC");?></li>
<li><?php i18n("Dolphin’s 'Control' menu now shows the 'Create New…' items and has a new 'Show Hidden Places' menu item");?></li>
<li><?php i18n("Dolphin now quits when there is only one tab open and the standard 'close tab' keyboard shortcut (Ctrl+w) is pressed");?></li>
<li><?php i18n("After unmounting a volume from the Places panel, it is now possible to mount it again");?></li>
<li><?php i18n("The Recent Documents view (available by browsing to recentdocuments:/ in Dolphin) now only shows actual documents, and automatically filters out web URLs");?></li>
<li><?php i18n("Dolphin now shows a warning before allowing you to rename file or directory in such a manner that would cause it to immediately become hidden");?></li>
<li><?php i18n("It is no longer possible to try to unmount the disks for your active operating system or home directory from the Places Panel");?></li>
</ul>

<?php print i18n_var("<a href='%1'>KFind</a>, KDE's traditional file search, now has a metadata search method based on KFileMetaData.", "https://www.kde.org/applications/utilities/kfind");?>

<h3 style="clear:both;"><?php i18n("Office")?></h3>

<?php print i18n_var("<a href='%1'>KMail</a>, KDE's powerful email client:", "https://www.kde.org/applications/internet/kmail/");?>
<ul>
<li><?php i18n("KMail can now display a unified inbox");?></li>
<li><?php i18n("New plugin: Generate HTML mail from Markdown Language");?></li>
<li><?php i18n("Using Purpose for Sharing Text (as email)");?></li>
<li><?php i18n("HTML emails are now readable no matter what color scheme is in use");?></li>
</ul>

<figure style="float: right; margin: 0px"><a href="app1812_okular01.png" data-toggle="lightbox"><img src="app1812_okular01.png" width="250" /></a></figure>
<?php print i18n_var("<a href='%1'>Okular</a>, KDE's versatile document viewer:", "https://www.kde.org/applications/graphics/okular/");?>
<ul>
<li><?php i18n("New 'Typewriter' annotation tool that can be used to write text anywhere");?></li>
<li><?php i18n("The hierarchical table of contents view now has the ability to expand and collapse everything, or just a specific section");?></li>
<li><?php i18n("Improved word-wrap behavior in inline annotations");?></li>
<li><?php i18n("When hovering the mouse over a link, the URL is now shown anytime it could be clicked on, instead of only when in Browse mode");?></li>
<li><?php i18n("ePub files containing resources with spaces in their URLs now display correctly");?></li>
</ul>

<h3 style="clear:both;"><?php i18n("Development")?></h3>

<figure style="float: right; margin: 0px"><a href="app1812_kate01.png" data-toggle="lightbox"><img src="app1812_kate01.png" width="250" /></a></figure>
<?php print i18n_var("<a href='%1'>Kate</a>, KDE's advanced text editor:", "https://www.kde.org/applications/utilities/kate/");?>
<ul>
<li><?php i18n("When using the embedded terminal, it now automatically synchronizes the current directory with the active document's on-disk location");?></li>
<li><?php i18n("Kate's embedded terminal can now be focused and de-focused using the F4 keyboard shortcut");?></li>
<li><?php i18n("Kate’s built-in tab switcher now shows full paths for similarly-named files");?></li>
<li><?php i18n("Line numbers are now on by default");?></li>
<li><?php i18n("The incredibly useful and powerful Text Filter plugin is now enabled by default and is more discoverable");?></li>
<li><?php i18n("Opening an already-open document using the Quick Open feature now switches back to that document");?></li>
<li><?php i18n("The Quick Open feature no longer shows duplicate entries");?></li>
<li><?php i18n("When using multiple Activities, files are now opened in the correct Activity");?></li>
<li><?php i18n("Kate now displays all the correct icons when run on GNOME using the GNOME icon theme");?></li>
</ul>

<figure style="float: right; margin: 0px"><a href="app1812_konsole.png" data-toggle="lightbox"><img src="app1812_konsole.png" width="250" /></a></figure>
<?php print i18n_var("<a href='%1'>Konsole</a>, KDE's terminal emulator:", "https://www.kde.org/applications/system/konsole/");?>
<ul>
<li><?php i18n("Konsole now fully supports emoji characters");?></li>
<li><?php i18n("Inactive tabs' icons are now highlighted when they receive a bell signal");?></li>
<li><?php i18n("Trailing colons are no longer considered parts of a word for the purposes of double-click selection, making it easier to select paths and 'grep' output");?></li>
<li><?php i18n("When a mouse with back and forward buttons is plugged in, Konsole can now use those buttons for switching between tabs");?></li>
<li><?php i18n("Konsole now has a menu item to reset the font size to the profile default, if it has been enlarged or reduced in size");?></li>
<li><?php i18n("Tabs are now harder to accidentally detach, and faster to accurately re-order");?></li>
<li><?php i18n("Improved shift-click selection behavior");?></li>
<li><?php i18n("Fixed double-clicking on a text line that exceeds the window width");?></li>
<li><?php i18n("The search bar once again closes when you hit the Escape key");?></li>
</ul>

<?php print i18n_var("<a href='%1'>Lokalize</a>, KDE's translation tool:", "https://www.kde.org/applications/development/lokalize/");?>
<ul>
<li><?php i18n("Hide translated files on the Project tab");?></li>
<li><?php i18n("Added basic support of pology, the syntax and glossary checking system");?></li>
<li><?php i18n("Simplified navigation with tab ordering and multi-tab opening");?></li>
<li><?php i18n("Fixed segfaults due to concurrent access to database objects");?></li>
<li><?php i18n("Fixed inconsistent drag and drop");?></li>
<li><?php i18n("Fixed a bug on shortcuts which were different between editors");?></li>
<li><?php i18n("Improved search behavior (search will find and display plural forms)");?></li>
<li><?php i18n("Restore Windows compatibility thanks to the craft building system");?></li>
</ul>

<h3 style="clear:both;"><?php i18n("Utilities")?></h3>

<?php print i18n_var("<a href='%1'>Gwenview</a>, KDE's image viewer:", "https://www.kde.org/applications/graphics/gwenview/");?>
<ul>
<li><?php i18n("The 'Reduce Red Eye' tool received a variety of nice usability improvements");?></li>
<li><?php i18n("Gwenview now displays a warning dialog when you hide the menubar that tells you how to get it back");?></li>
</ul>

<figure style="float: right; margin: 0px"><a href="app1812_spectacle01.png" data-toggle="lightbox"><img src="app1812_spectacle01.png" width="250" /></a></figure>
<?php print i18n_var("<a href='%1'>Spectacle</a>, KDE's screenshot utility:", "https://www.kde.org/applications/graphics/spectacle/");?>
<ul>
<li><?php i18n("Spectacle now has the ability to sequentially number screenshot files, and defaults to this naming scheme if you clear the filename text field");?></li>
<li><?php i18n("Fixed saving images in a format other than .png, when using Save As…");?></li>
<li><?php i18n("When using Spectacle to open a screenshot in an external app, it is now possible to modify and save the image after you are done with it");?></li>
<li><?php i18n("Spectacle now opens the correct folder when you click Tools > Open Screenshots Folder");?></li>
<li><?php i18n("Screenshot timestamps now reflect when the image was created, not when it was saved");?></li>
<li><?php i18n("All of the save options are now located on the “Save” page");?></li>
</ul>

<?php print i18n_var("<a href='%1'>Ark</a>, KDE's archive manager:", "https://www.kde.org/applications/utilities/ark/");?>
<ul>
<li><?php i18n("Added support for the Zstandard format (tar.zst archives)");?></li>
<li><?php i18n("Fixed Ark previewing certain files (e.g. Open Document) as archives instead of opening them in the appropriate application");?></li>
</ul>

<h3 style="clear:both;"><?php i18n("Mathematics")?></h3>

<p>
<?php print i18n_var("<a href='%1'>KCalc</a>, KDE's simple calculator, now has a setting to repeat the last calculation multiple times.", "https://www.kde.org/applications/utilities/kcalc/");?>
</p>

<?php print i18n_var("<a href='%1'>Cantor</a>, KDE's mathematical frontend:", "https://www.kde.org/applications/education/cantor/");?>
<ul>
<li><?php i18n("Add Markdown entry type");?></li>
<li><?php i18n("Animated highlighting of the currently calculated command entry");?></li>
<li><?php i18n("Visualization of pending command entries (queued, but not being calculated yet)");?></li>
<li><?php i18n("Allow to format command entries (background color, foreground color, font properties)");?></li>
<li><?php i18n("Allow to insert new command entries at arbitrary places in the worksheet by placing the cursor at the desired position and by start typing");?></li>
<li><?php i18n("For expressions having multiple commands, show the results as independent result objects in the worksheet");?></li>
<li><?php i18n("Add support for opening worksheets by relative paths from console");?></li>
<li><?php i18n("Add support for opening multiple files in one Cantor shell");?></li>
<li><?php i18n("Change the color and the font for when asking for additional information in order to better discriminate from the usual input in the command entry");?></li>
<li><?php i18n("Added shortcuts for the navigation across the worksheets (Ctrl+PageUp, Ctrl+PageDown)");?></li>
<li><?php i18n("Add action in 'View' submenu for zoom reset");?></li>
<li><?php i18n("Enable downloading of Cantor projects from store.kde.org (at the moment upload works only from the website)");?></li>
<li><?php i18n("Open the worksheet in read-only mode if the backend is not available on the system");?></li>
</ul>

<?php print i18n_var("<a href='%1'>KmPlot</a>, KDE's function plotter, fixed many issues:", "https://www.kde.org/applications/education/kmplot/");?>
<ul>
<li><?php i18n("Fixed wrong names of the plots for derivatives and integral in the conventional notation");?></li>
<li><?php i18n("SVG export in Kmplot now works properly");?></li>
<li><?php i18n("First derivative feature has no prime-notation");?></li>
<li><?php i18n("Unticking unfocused function now hides its graph:");?></li>
<li><?php i18n("Solved Kmplot crash when opening 'Edit Constants' from function editor recursively");?></li>
<li><?php i18n("Solved KmPlot crash after deleting a function the mouse pointer follows in the plot");?></li>
<li><?php i18n("You can now export drawn data as any picture format");?></li>
</ul>


<!-- Boilerplate -->

<section class="row get-it">
    <article class="col-md">
        <h2><?php i18n("Package Downloads");?></h2>
        <p>
            <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
        </p>
        <p><a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Package download wiki page");?></a></p>
    </article>

    <article class="col-md">
        <h2><?php i18n("Source Downloads");?></h2>
        <p><?php print i18n_var("The complete source code for KDE Applications %1 may be <a href='http://download.kde.org/stable/applications/%2/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-%3.php'>KDE Applications %4 Info Page</a>.", $version_text, $version_number, $version_number, $version_text);?></p>
    </article>
</section>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.");?>
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
