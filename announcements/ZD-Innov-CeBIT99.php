<?php
  $page_title = "ZD Innovation of the Year 1998/1999";
  $site_root = "../";
  include "header.inc";
?>

<CENTER>

<TABLE WIDTH="600"><TR><TD>
<br><br>
<p>FOR IMMEDIATE RELEASE

<p>March 19, 1999
<br />
<p>
(Hanover, Germany)  The <a href="http://www.kde.org">K Desktop Environment(KDE)</a>,
 an advanced and user-friendly desktop for
the increasingly popular GNU/Linux - Unix operating system, was awarded
top honors at CeBIT, the worlds largest computer trade fair, as
"Innovation of the Year 1998/99" in the category "Software".
</p>
<p>
According to an <A HREF="http://www.zdnet.de/news/artikel/1999/03/19011-wf.htm">article</A>
published by Ziff-Davis, sponsor of the award, the criterion
for granting this award of technical excellence was not commerical success
but
creativity behind the product's design, an exceptional solution for
a specific problem or a completely new concept. The other two
finalists for the award were Lotus eSuite and Microtest Virtual CD.
<p>
KDE developers were very pleased by the announcement.  In announcing the
award to the KDE community, Kalle Dalheimer, a KDE developer,
beamed, "This award is a great achievement for the whole KDE team!
Congratulations to all of you!"
<p>
The availability of the high quality and mature desktop is considered a
key software for enterprise and home use of GNU/Linux. Several large
software vendors have announced support of KDE in recent weeks,
including Red Hat Software, Inc. and Corel Corporation.
<br><P>
ABOUT KDE
<p>
KDE released version 1.1 of its desktop on February 19, 1999.
KDE runs on GNU/Linux, FreeBSD, Solaris, HP-UX and other Unix variants.  It is
available in all major Linux and BSD distributions, and is also downloadable

free of charge from <A HREF="ftp://ftp.kde.org/pub/kde">KDE's web site</A>.
<p>
KDE's major contributions to Unix are related to
the ease of installation, configuration and use.  KDE provides users
with an attractive, functional desktop, applications that
provide a consistent look-and-feel as well as internationalization.
KDE offers also a consistent user interface across all Unix systems
and numerous hardware platforms, from PCs to
powerful Internet servers, thereby permitting organizations to
freely switch hardware without incurring the costs associated with
switching operating systems.
<p>
The KDE project was launched in October 1996 by a small group of
developers.  The project immediately adopted the open source model
and grew quickly.  Today, it is one of the largest open source development
projects, with several hundred contributing developers, 1.2 million lines of
source code, hundreds of translators who translate KDE into 32 different
languages and thousands of interested users assisting
in testing and debugging.
</TD></TR></TABLE>
<CENTER>
<HR WIDTH=570 SIZE=5 ALIGN=CENTER NOSHADE>
<FONT SIZE=-1>Last updated on 19 March 1999</FONT></CENTER>

<CENTER><FONT SIZE=-1>Maintained by
<A HREF="&#109;&#x61;i&#108;&#00116;&#x6f;&#00058;web&#x6d;&#097;&#115;t&#x65;r&#00064;&#0107;&#0100;e.o&#114;&#00103;">w&#0101;&#x62;ma&#x73;&#x74;e&#0114;&#0064;&#x6b;&#x64;&#101;.&#x6f;&#x72;g</A></FONT>&nbsp;
<!-- ===============  TOP IMAGE MAP =========== -->
<MAP name="topbar">
<AREA shape="rect" coords="90,0,120,18" href="../documentation/faq/kdefaq.html">
<AREA shape="rect" coords="130,0,205,18" href="ftp://ftp.kde.org/pub/kde/">
<AREA shape="rect" coords="215,0,295,18" href="../contact.html">
<AREA shape="rect" coords="305,0,355,18" href="../news_dyn.html">
<AREA shape="default" nohref></MAP>
</CENTER>

<?php
  include "footer.inc"
?>
