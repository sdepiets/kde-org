<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>Always accept notifications for the parent resource. <a href='http://commits.kde.org/akonadi/4683d2ba3e564901252255cf94498a1272568bcf'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-search' href='https://cgit.kde.org/akonadi-search.git'>akonadi-search</a> <a href='#akonadi-search' onclick='toggle("ulakonadi-search", this)'>[Show]</a></h3>
<ul id='ulakonadi-search' style='display: none'>
<li>Fix crash after reinitializing Index. <a href='http://commits.kde.org/akonadi-search/5bef6f440afdf9795b7de23fd5ba1ada91d92dc2'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Singlefileplugin: Open svgz's with correct filename. <a href='http://commits.kde.org/ark/7e0080f1f35143eb867e377e4a28d0645b85337a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/252701'>#252701</a>. Code review <a href='https://git.reviewboard.kde.org/r/125518'>#125518</a></li>
<li>Assume that header encryption is disabled. <a href='http://commits.kde.org/ark/b99086196be108dbc76c202bf78ef0e4b5ac8bee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351608'>#351608</a>. Code review <a href='https://git.reviewboard.kde.org/r/125372'>#125372</a></li>
<li>Fix drag'n'drop extraction with CLI-plugins. <a href='http://commits.kde.org/ark/7c025dd5765c41957787493786248157f29b4e13'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/208384'>#208384</a>. Code review <a href='https://git.reviewboard.kde.org/r/125293'>#125293</a></li>
<li>A fix for drag'n'drop extraction. <a href='http://commits.kde.org/ark/06024b034537c46c242e6643e939ae45c4d8d5d8'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Show]</a></h3>
<ul id='uldolphin' style='display: none'>
<li>Fix layout of ViewPropertiesDialog. <a href='http://commits.kde.org/dolphin/e6af789224cdf65cecf147853078d7c3338889fb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125373'>#125373</a></li>
<li>Fix KF5Activities being optional. <a href='http://commits.kde.org/dolphin/6e218c4c146c08626c6bf4c87e9849a9de7462c4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125360'>#125360</a></li>
</ul>
<h3><a name='dolphin-plugins' href='https://cgit.kde.org/dolphin-plugins.git'>dolphin-plugins</a> <a href='#dolphin-plugins' onclick='toggle("uldolphin-plugins", this)'>[Show]</a></h3>
<ul id='uldolphin-plugins' style='display: none'>
<li>Set the working directory instead of clearing it. <a href='http://commits.kde.org/dolphin-plugins/012664e2d07b82f9b0b68f4f42812a0e66460359'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352625'>#352625</a>. Code review <a href='https://git.reviewboard.kde.org/r/125383'>#125383</a></li>
<li>Revert "Fixed adding files in git repository with Dolphin git plugin". <a href='http://commits.kde.org/dolphin-plugins/f8164b2273b6053e035e386a788f7521eb7c035f'>Commit.</a> See bug <a href='https://bugs.kde.org/352625'>#352625</a></li>
<li>Don't mark untracked items as 'normal'. <a href='http://commits.kde.org/dolphin-plugins/c875c04662df56c53b45b58767dcbbf95ff40236'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352625'>#352625</a>. Code review <a href='https://git.reviewboard.kde.org/r/125385'>#125385</a></li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Show]</a></h3>
<ul id='ulgwenview' style='display: none'>
<li>Set bugzilla product name. <a href='http://commits.kde.org/gwenview/1febc3c1eda5c0dbdd705149c860ce095b2e7301'>Commit.</a> </li>
<li>Fix missing menu items. <a href='http://commits.kde.org/gwenview/2420e0d3108e79c675b7d6a024d3d564ccc79189'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351431'>#351431</a>. Code review <a href='https://git.reviewboard.kde.org/r/125524'>#125524</a></li>
<li>Always assume changeCursorOverIcon. <a href='http://commits.kde.org/gwenview/c05462eb7b95c74a2e0267d8adb4bfafe5129ad1'>Commit.</a> </li>
</ul>
<h3><a name='kaccounts-integration' href='https://cgit.kde.org/kaccounts-integration.git'>kaccounts-integration</a> <a href='#kaccounts-integration' onclick='toggle("ulkaccounts-integration", this)'>[Show]</a></h3>
<ul id='ulkaccounts-integration' style='display: none'>
<li>Stop building the akonadi plugin. <a href='http://commits.kde.org/kaccounts-integration/5f95c467d0a31ba06d82bedcc341d0f3a1eab2a8'>Commit.</a> </li>
<li>When enabling/disabling a service, the whole account needs to be enabled/disabled. <a href='http://commits.kde.org/kaccounts-integration/52fdbbe631890e7c3175872c7057bde7af177a27'>Commit.</a> </li>
<li>[getcredentials] Init the member vars properly. <a href='http://commits.kde.org/kaccounts-integration/3798deecaaf8dfedf7044c825767ffc3ebc18dc9'>Commit.</a> </li>
<li>[createaccount] To enable the account, the selected service must be cleared first. <a href='http://commits.kde.org/kaccounts-integration/100284ebb6bb76e6eaa55de5e9dac2efafb7de55'>Commit.</a> </li>
<li>[createaccount] If there's only one service for account, use it as default. <a href='http://commits.kde.org/kaccounts-integration/abf5533a5d3c554fe823e219b3f80bc7328f5693'>Commit.</a> </li>
<li>[kcm] Connect the Configure button to configUiReady instead of uiReady. <a href='http://commits.kde.org/kaccounts-integration/0f69254c2de08ab67c4c4e651046d2a5025a03f9'>Commit.</a> </li>
<li>[lib] Add new signal configUiReady(). <a href='http://commits.kde.org/kaccounts-integration/617d7dfbddf561758d5775ffc2a3112f5ffca15a'>Commit.</a> </li>
<li>[lib] Add transientParent() wrapper around the transientParent property. <a href='http://commits.kde.org/kaccounts-integration/f1fe80f483712a5423fbe2a20b18a6a4f1740448'>Commit.</a> </li>
<li>[pluginmanager] Set transientParent property on the UI plugins. <a href='http://commits.kde.org/kaccounts-integration/b8f1fd7a5cc8cc8af7224ad549d29db3c8fe8b95'>Commit.</a> </li>
<li>[createaccount] Allow plugins to specify which services should be enabled by default. <a href='http://commits.kde.org/kaccounts-integration/91a98f533d9988061e9de3d6a703679cfa4f4e68'>Commit.</a> </li>
<li>Remove a leftover function declaration. <a href='http://commits.kde.org/kaccounts-integration/44f58081a6cf5988ab89b9d1e37c290970e470cc'>Commit.</a> </li>
<li>[createaccount] Store the data from plugins on the account itself. <a href='http://commits.kde.org/kaccounts-integration/0eeb827d7d41ebb046ef4a5b778e5fd1762f9f9f'>Commit.</a> </li>
<li>Reorder the logic that displays the plugin UIs a bit. <a href='http://commits.kde.org/kaccounts-integration/d3ecae6fdbff08f02abad4441304b7b0a5056b15'>Commit.</a> </li>
<li>[createaccount] Remove the special handling of ownCloud providers. <a href='http://commits.kde.org/kaccounts-integration/8b44623d5f9162a71c7edc7801f82cebfbf88fd8'>Commit.</a> </li>
</ul>
<h3><a name='kamera' href='https://cgit.kde.org/kamera.git'>kamera</a> <a href='#kamera' onclick='toggle("ulkamera", this)'>[Show]</a></h3>
<ul id='ulkamera' style='display: none'>
<li>Set cmake_min_req to match kdelibs policy and enable newer cmake policies. <a href='http://commits.kde.org/kamera/f0d7b3b6164ffdc83b8763bea5ac8c21eb628881'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Show]</a></h3>
<ul id='ulkate' style='display: none'>
<li>Fixed bug #352627: properly use libgit2 resource. <a href='http://commits.kde.org/kate/09b640fccccdae8a24ae09144b7cffeb966900f8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352627'>#352627</a></li>
</ul>
<h3><a name='kbruch' href='https://cgit.kde.org/kbruch.git'>kbruch</a> <a href='#kbruch' onclick='toggle("ulkbruch", this)'>[Show]</a></h3>
<ul id='ulkbruch' style='display: none'>
<li>Fix call to QStringLiteral, pass the proper path. <a href='http://commits.kde.org/kbruch/4c77e5893cb7adbe4476c4d8e368aa4220ced1dd'>Commit.</a> </li>
<li>Few fixes, bump version to 5.0.1. <a href='http://commits.kde.org/kbruch/b3547159b5a00fb7580071a25e6d82fce22c12d0'>Commit.</a> </li>
<li>Removed 'Show StatusBar' menu item from Learning. <a href='http://commits.kde.org/kbruch/87d0a1727fdfdf48edecf010759366b49d3e578c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351519'>#351519</a></li>
<li>Set the toolbar editable. <a href='http://commits.kde.org/kbruch/377390dcbe2df3fe00f443fb648debea5c5f2444'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351520'>#351520</a></li>
<li>Restore slot definition. <a href='http://commits.kde.org/kbruch/697d042ca19f15e1fec0cea6325cef21be83c0f1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349984'>#349984</a>. Code review <a href='https://git.reviewboard.kde.org/r/125406'>#125406</a></li>
<li>Remove warning about setXMLFile. <a href='http://commits.kde.org/kbruch/2626de747f683f707478baa3a85c274295bf5ddc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125406'>#125406</a></li>
<li>Fix file install locations. <a href='http://commits.kde.org/kbruch/6c4f6359768ff7ee0ad8c2444bde443c8712017b'>Commit.</a> </li>
<li>Kbruch.desktop: s/-caption/-qwindowtitle/. <a href='http://commits.kde.org/kbruch/7ef6d2a7d986eea6c02051e3cacf7477ac37ac7b'>Commit.</a> </li>
</ul>
<h3><a name='kcalcore' href='https://cgit.kde.org/kcalcore.git'>kcalcore</a> <a href='#kcalcore' onclick='toggle("ulkcalcore", this)'>[Show]</a></h3>
<ul id='ulkcalcore' style='display: none'>
<li>Fix heap-use-after-free in ICalFormatImpl::readICalDateTime. <a href='http://commits.kde.org/kcalcore/702b55ac4a855fe2a5f7a06915ff2ae8a77a2b04'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125364'>#125364</a></li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Give unique names to the targets created by KDE4Macros.cmake. <a href='http://commits.kde.org/kdelibs/1b0cb8ed48b5bf816cd5fe27d435568e69d94113'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125560'>#125560</a></li>
<li>Align FindGettext with CMP002 policy. <a href='http://commits.kde.org/kdelibs/fd59e696d0a27e28b5551dff3d88d94d16787c2b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125561'>#125561</a></li>
<li>Prepare for 4.14.13. <a href='http://commits.kde.org/kdelibs/9d0b783ff1b21bf23ac7f262572066d672f42012'>Commit.</a> </li>
<li>Make sure the size of the byte array we just dumped into the struct is big enough before calculating the targetInfo, otherwise we're accessing memory that doesn't belong to us. <a href='http://commits.kde.org/kdelibs/20a501245c42da2a5d9b16049e000b43e01f18fc'>Commit.</a> </li>
<li>Remove usage of undefined debug area 7012; turn noisy warning into a debug. <a href='http://commits.kde.org/kdelibs/ca98e574c445d8b8483d4130cab438993a8e5add'>Commit.</a> See bug <a href='https://bugs.kde.org/173760'>#173760</a></li>
<li>Make kfileplacesmodeltest pass. <a href='http://commits.kde.org/kdelibs/74b991503f781c358fa1130d36e2a430bccbb3fb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125367'>#125367</a></li>
<li>Fix crash in UnicodeGroupProber::HandleData with short strings. <a href='http://commits.kde.org/kdelibs/426fabe034bcad5f832d2a897cb20ca37aad09aa'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Fix proxy clips sometimes changed to audio only. <a href='http://commits.kde.org/kdenlive/0115ddbd255c0b6a94453f18b96ab6c3f15251e0'>Commit.</a> </li>
<li>Fix Monitor toolbar too small on high DPI. <a href='http://commits.kde.org/kdenlive/abc4808ed6db8b576a8168b71d42cdcc85e35e45'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353640'>#353640</a></li>
<li>Let Qt handle toolbar sizes so we don't get tiny buttons on high DPI screens. <a href='http://commits.kde.org/kdenlive/972d3d4ac5bafc1d3b538c6013ba8505b25ec9e1'>Commit.</a> </li>
<li>Fix JogShuttle null reciever error, patch by The DiveO. <a href='http://commits.kde.org/kdenlive/da7fa85f6d390e015364f21beda4e2bf19f8471b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353494'>#353494</a>. Code review <a href='https://git.reviewboard.kde.org/r/125520'>#125520</a></li>
<li>Fix total rendering time when a job is over. <a href='http://commits.kde.org/kdenlive/63f3cf049d16efe5036780dd7c150d61e1d66dee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353419'>#353419</a></li>
<li>Fix rendering job not correctly displaying remaining time. <a href='http://commits.kde.org/kdenlive/c832b2346a08444115f80e266aabbaba7191fb7e'>Commit.</a> See bug <a href='https://bugs.kde.org/353419'>#353419</a></li>
<li>Fix changing clip type at position 0 reporting error. <a href='http://commits.kde.org/kdenlive/16c23486e94193af843b34f682d3423d1c60bcb6'>Commit.</a> </li>
<li>Fix script rendering when resizing output, patch by Frédéric COIFFIER. <a href='http://commits.kde.org/kdenlive/7b2e2d8074660251172f2958b94402e870a7d781'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353355'>#353355</a></li>
<li>Add missing FindOpenGLES cmake file. <a href='http://commits.kde.org/kdenlive/146e66c534b7d10871b6abb6ff5d11b41f623e80'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125002'>#125002</a>. Fixes bug <a href='https://bugs.kde.org/350061'>#350061</a></li>
<li>Fix concurrency crash when layout has clip and project monitor side by side. <a href='http://commits.kde.org/kdenlive/3c3877c27c9cd36da90b4a7f29d44390a5be825b'>Commit.</a> </li>
<li>Fix proxy settings for current project cannot be changed. <a href='http://commits.kde.org/kdenlive/36e115d77127a9db2f7f929c6e599341b73c5d64'>Commit.</a> </li>
<li>Fix possible crash on render. <a href='http://commits.kde.org/kdenlive/1e21abf0687bf57db5aba270eedeecbb478efe37'>Commit.</a> See bug <a href='https://bugs.kde.org/352670'>#352670</a></li>
<li>Fix crash in custom render profiles & wrong group on new profile. <a href='http://commits.kde.org/kdenlive/d27316444128a487ccfe4ddcb73238f920c9f33f'>Commit.</a> </li>
<li>Fix compilation (broken by backported patch). <a href='http://commits.kde.org/kdenlive/a126a48ac0942522dd5ee1fee70935c836d0da9a'>Commit.</a> </li>
<li>Fix crash on render profile with empty file extension. <a href='http://commits.kde.org/kdenlive/b23a95cf4c0458a5b65ef7bf417f942d25eca71e'>Commit.</a> See bug <a href='https://bugs.kde.org/352670'>#352670</a></li>
<li>Stop doing stupid column resize in bin tree. <a href='http://commits.kde.org/kdenlive/651bc94d5e06e7149d6741a3aa5636f88e39d0c9'>Commit.</a> </li>
<li>Remove small tip animation when exiting timeline. <a href='http://commits.kde.org/kdenlive/8ec996af625d9f6c604625353928188aa877294a'>Commit.</a> </li>
<li>Fix crash on add/delete track when green resize indicator was displayed. <a href='http://commits.kde.org/kdenlive/5baaf7295272ce057af2f31e72dd5416740ff86c'>Commit.</a> </li>
<li>Fix crash on Show Description with Bin tree in IconView. <a href='http://commits.kde.org/kdenlive/527eb8c560d101d99d2df4ec26a9736eed01d712'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351710'>#351710</a></li>
<li>Right click in empty area of Project Bin should show useful menu. <a href='http://commits.kde.org/kdenlive/b250dc04fd191feb03f8f53f979ec51e0201565a'>Commit.</a> </li>
</ul>
<h3><a name='kdepim' href='https://cgit.kde.org/kdepim.git'>kdepim</a> <a href='#kdepim' onclick='toggle("ulkdepim", this)'>[Show]</a></h3>
<ul id='ulkdepim' style='display: none'>
<li>KMail: Fix scrolling up/down on the message viewer. <a href='http://commits.kde.org/kdepim/9d52b22b3652f4dc274114f41c6ea83a31477c01'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125565'>#125565</a></li>
<li>Make Next/Previous unread message work when using the keypad. <a href='http://commits.kde.org/kdepim/74d26352b9c2c6c3bb75044a3fd8addca3977f16'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125564'>#125564</a></li>
<li>Set/unset messagelist corner widgets. <a href='http://commits.kde.org/kdepim/3363efebc1ec1dd284ce5e59e6e2d8e6e0b41242'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125475'>#125475</a></li>
<li>Fix local QUrl construction. <a href='http://commits.kde.org/kdepim/3b4b3ad63472a7392ce5dd854fd3734402b9db86'>Commit.</a> </li>
<li>Fix wrong connect. <a href='http://commits.kde.org/kdepim/21b959c24975f3e72ffe7f5b3c4c974bca9535d5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125477'>#125477</a></li>
<li>Increase version for 15.08.2. <a href='http://commits.kde.org/kdepim/50a8fa239bbff774a7e1c2765ed2ddac85b997fd'>Commit.</a> </li>
<li>Bug 352889: allow typing in New Alarm dialog while alarm displayed. <a href='http://commits.kde.org/kdepim/8e3b57e2d5c78c56a62ad9fc0c459d10e9141019'>Commit.</a> </li>
<li>Fix test name so ctest finds it. <a href='http://commits.kde.org/kdepim/fba819571cb6c56fd11e400a61a1fe51131f7355'>Commit.</a> </li>
<li>Revert "Fix memory leak and crash in ManageSieveScriptsDialog". <a href='http://commits.kde.org/kdepim/431ee7aa7f79e7fcbdb5a31c2b65d4c22fcf573f'>Commit.</a> </li>
<li>Fix memory leak and crash in ManageSieveScriptsDialog. <a href='http://commits.kde.org/kdepim/9c1a1d87503d4e12990b93dcab63941c7e91ab8f'>Commit.</a> </li>
<li>Fix Bug 350737 - KMail/KF5 Blocking UI Call when Opening ODF Mail Attachement. <a href='http://commits.kde.org/kdepim/ca27705bd161a64a6aa86c0c844036143eea24fb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350737'>#350737</a></li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>IMAP: use UIDNEXT instead of "*" as interval end when doing full mailbox resync. <a href='http://commits.kde.org/kdepim-runtime/813e4dfdcf30bed34397fa578d0d1ae0e61ea625'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351814'>#351814</a></li>
<li>Singlefileresource: Fix setNeedsNetwork(). <a href='http://commits.kde.org/kdepim-runtime/f50c347397684fc6acb1253b9190a10508816c22'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-runtime/e11f4bcfcb52cb53e61d58bfa215fb140eec81fc'>Commit.</a> </li>
<li>Ical: Fix resource It wasn't creating the file with the correct path. <a href='http://commits.kde.org/kdepim-runtime/1612e91eb5540fc25e4fa4a7bfe28d3512439318'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352693'>#352693</a></li>
<li>Fix heap-use-after-free in the imap resource. <a href='http://commits.kde.org/kdepim-runtime/be88f08f630d24863b6e998756f3947429125b05'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125368'>#125368</a></li>
<li>IMAP: switch to GID-based merge when the Collection can contain something else than emails. <a href='http://commits.kde.org/kdepim-runtime/038c604aba0cac22275e03c3497672cd254c2568'>Commit.</a> </li>
</ul>
<h3><a name='kdepimlibs' href='https://cgit.kde.org/kdepimlibs.git'>kdepimlibs</a> <a href='#kdepimlibs' onclick='toggle("ulkdepimlibs", this)'>[Show]</a></h3>
<ul id='ulkdepimlibs' style='display: none'>
<li>Don't assign Del as the default shortcut for item deletion. <a href='http://commits.kde.org/kdepimlibs/678e56e17c6b4fb1c798ca6af9962aec6a97e1fe'>Commit.</a> </li>
<li>Fix license. <a href='http://commits.kde.org/kdepimlibs/279091d2ac68a65f15b9240d2bbccddd9956acfc'>Commit.</a> See bug <a href='https://bugs.kde.org/351752'>#351752</a></li>
<li>Relicense two accidentally GPLed files in libakonadi. <a href='http://commits.kde.org/kdepimlibs/762d4beae2d761b76c3eb8212523c64ddb2ff4c0'>Commit.</a> See bug <a href='https://bugs.kde.org/351752'>#351752</a></li>
<li>Move the add_subdirectoy before the set( EXECUTABLE_OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR} ). <a href='http://commits.kde.org/kdepimlibs/2a8c79768682db70c8c8e4111378778140a594dd'>Commit.</a> </li>
<li>Bump internal version. <a href='http://commits.kde.org/kdepimlibs/d38c3af2c59cc40c2b16b517cc3ef57dc9e397f0'>Commit.</a> </li>
<li>Fix ItemSync merge type fallback. <a href='http://commits.kde.org/kdepimlibs/ffa20e75f388bfa87530e113641f0830a5a58ec4'>Commit.</a> </li>
<li>ItemSync: use RID merge by default, allow optional switch to GID merge. <a href='http://commits.kde.org/kdepimlibs/43d5659a88a6ebb3423c6228986f0bd1e1f496f7'>Commit.</a> </li>
</ul>
<h3><a name='kimap' href='https://cgit.kde.org/kimap.git'>kimap</a> <a href='#kimap' onclick='toggle("ulkimap", this)'>[Show]</a></h3>
<ul id='ulkimap' style='display: none'>
<li>Fix use-after-free issue found by asan on the CI. <a href='http://commits.kde.org/kimap/5720b130d13d216feef9961d7815d1cb392231c3'>Commit.</a> </li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Show]</a></h3>
<ul id='ulkio-extras' style='display: none'>
<li>Fix delete in Fish protocol. <a href='http://commits.kde.org/kio-extras/311f7e124e49b81a37fd49b3ef20e9ca65c8567a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351309'>#351309</a></li>
</ul>
<h3><a name='kmime' href='https://cgit.kde.org/kmime.git'>kmime</a> <a href='#kmime' onclick='toggle("ulkmime", this)'>[Show]</a></h3>
<ul id='ulkmime' style='display: none'>
<li>Don't use pointers to destroyed temporaries here. <a href='http://commits.kde.org/kmime/4bd33a1b03f07056df3abb6aab5d3294be3fad10'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Show]</a></h3>
<ul id='ulkonsole' style='display: none'>
<li>Fix heap-use-after-free in Part destructor. <a href='http://commits.kde.org/konsole/ace53ef482c621b375eea2d9b38eda6b11b69873'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125366'>#125366</a></li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Show]</a></h3>
<ul id='ulkopete' style='display: none'>
<li>Prepare for 15.08.2. <a href='http://commits.kde.org/kopete/34bcf2f8eb93410c7e4a82ee89fc9d3e60be9d20'>Commit.</a> </li>
</ul>
<h3><a name='kpat' href='https://cgit.kde.org/kpat.git'>kpat</a> <a href='#kpat' onclick='toggle("ulkpat", this)'>[Show]</a></h3>
<ul id='ulkpat' style='display: none'>
<li>Create application directory if it doesn't exist. <a href='http://commits.kde.org/kpat/a214428743993ac1ede3fa48d757ca339f498fd9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350160'>#350160</a>. Code review <a href='https://git.reviewboard.kde.org/r/125508'>#125508</a></li>
</ul>
<h3><a name='ktp-auth-handler' href='https://cgit.kde.org/ktp-auth-handler.git'>ktp-auth-handler</a> <a href='#ktp-auth-handler' onclick='toggle("ulktp-auth-handler", this)'>[Show]</a></h3>
<ul id='ulktp-auth-handler' style='display: none'>
<li>If one auth method fails, try another. <a href='http://commits.kde.org/ktp-auth-handler/6c9babe1eb620508fc4c857a21529b2dad3c0ad3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125292'>#125292</a></li>
</ul>
<h3><a name='ktp-text-ui' href='https://cgit.kde.org/ktp-text-ui.git'>ktp-text-ui</a> <a href='#ktp-text-ui' onclick='toggle("ulktp-text-ui", this)'>[Show]</a></h3>
<ul id='ulktp-text-ui' style='display: none'>
<li>Make log viewer usable in multiple instances. <a href='http://commits.kde.org/ktp-text-ui/f1c59e07876a8664d1746b9ac06034b76e909600'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123907'>#123907</a>. Fixes bug <a href='https://bugs.kde.org/346395'>#346395</a></li>
<li>Call parent ::event(). <a href='http://commits.kde.org/ktp-text-ui/42cf29d56ccf3b107f7f063f3ae36d269aa44d30'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125146'>#125146</a></li>
</ul>
<h3><a name='libksane' href='https://cgit.kde.org/libksane.git'>libksane</a> <a href='#libksane' onclick='toggle("ullibksane", this)'>[Show]</a></h3>
<ul id='ullibksane' style='display: none'>
<li>Also allow sources containg "ADF" to trigger batch scanning. <a href='http://commits.kde.org/libksane/ddc17b08dc3923cf134ec5478a62fc7ff10f8497'>Commit.</a> See bug <a href='https://bugs.kde.org/295119'>#295119</a></li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Show]</a></h3>
<ul id='ullokalize' style='display: none'>
<li>Fix saving date for timezone with negative offset. <a href='http://commits.kde.org/lokalize/7c257f5c37c885d508dd67803cfad21860ddce93'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125215'>#125215</a>. Fixes bug <a href='https://bugs.kde.org/342789'>#342789</a></li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Show]</a></h3>
<ul id='ulmarble' style='display: none'>
<li>Fix error: string literal needs QLatin1String. <a href='http://commits.kde.org/marble/e2914a7115e57b16cbe164099acd27d46873f115'>Commit.</a> </li>
<li>Fix build in Kubuntu and Debian where headers are in /usr/include/quazip and library is called quazip-qt5. <a href='http://commits.kde.org/marble/73e18a3054937501c2717ba919512e385e7ea2e5'>Commit.</a> </li>
<li>I18n does not use .arg. <a href='http://commits.kde.org/marble/5cf8f12c6351e9bad58c896d1ba2a407e6efa5b3'>Commit.</a> </li>
<li>Link libmarblewidget to Qt5::Gui publicly. <a href='http://commits.kde.org/marble/0e214cd027ed635204d400134219b2d474f17685'>Commit.</a> </li>
<li>MarbleGlobal.h: Remove unused #include <QColor>. <a href='http://commits.kde.org/marble/62783afb33fcfd9fd09cda52fc46b5c152b60b88'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Show]</a></h3>
<ul id='ulokular' style='display: none'>
<li>Do not --use when invoking kate anymore. <a href='http://commits.kde.org/okular/4fee5131e9daf98443049954e5d70a4fdf363197'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352657'>#352657</a></li>
</ul>
<h3><a name='print-manager' href='https://cgit.kde.org/print-manager.git'>print-manager</a> <a href='#print-manager' onclick='toggle("ulprint-manager", this)'>[Show]</a></h3>
<ul id='ulprint-manager' style='display: none'>
<li>Don't modify member variables in another thread. <a href='http://commits.kde.org/print-manager/29247d6d19c49a20c17ea414a0e108905712a520'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125395'>#125395</a>. Fixes bug <a href='https://bugs.kde.org/345862'>#345862</a></li>
<li>Fix valgrind warning about uninitialized variable. <a href='http://commits.kde.org/print-manager/134f1fa3b8ba851dbbf0e8eb1de33f6031a31385'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Add parse support for mysql 'CHARACTER SET <format>'. <a href='http://commits.kde.org/umbrello/658de93df88e901f4adc6161db67864ea842fe29'>Commit.</a> See bug <a href='https://bugs.kde.org/353668'>#353668</a></li>
<li>Fix 'Hangs on MySQL script import when a column has a character set'. <a href='http://commits.kde.org/umbrello/6aabd97ea7a75ca6ddf17af89bc60b62a656a1cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353668'>#353668</a></li>
<li>Remove obsolate global static variable umlApp in class TestBase. <a href='http://commits.kde.org/umbrello/9bb5283bb039ddff98105eece0189fc0e888c77b'>Commit.</a> </li>
<li>Fix crash in TEST_cppwriter::cleanupTestCase() detected by build.kde.org. <a href='http://commits.kde.org/umbrello/dc5beef1993d1ced380191e86e774aebd70494af'>Commit.</a> </li>
<li>Recreate datatype folder on document close after deleting stereotypes. <a href='http://commits.kde.org/umbrello/36eb4d747445fdf95c358e505f1442ffbf27ced3'>Commit.</a> See bug <a href='https://bugs.kde.org/345546t'>#345546t</a></li>
<li>Do not add i18n calls in stable branches. <a href='http://commits.kde.org/umbrello/01a11118f78d2cd66e9b78ded07b6f4cf1c47fd1'>Commit.</a> See bug <a href='https://bugs.kde.org/345546'>#345546</a></li>
<li>Add dock window for stereotypes using a QTableView. <a href='http://commits.kde.org/umbrello/cba7534e88ce8652dd8ce9ac4252cf23a51e9cd2'>Commit.</a> See bug <a href='https://bugs.kde.org/345546'>#345546</a></li>
<li>Fix incomplete UMLStereotype reference counting. <a href='http://commits.kde.org/umbrello/3b16b05cb74428b1e8a2bec42ec6f327c65ba0dc'>Commit.</a> See bug <a href='https://bugs.kde.org/345546'>#345546</a></li>
<li>Fix 'Stereotype reference count issue on close, open or new document'. <a href='http://commits.kde.org/umbrello/c55cebc95fc0954fd48510fd20c1bbe3e05e07dc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345546'>#345546</a></li>
<li>Coverity check CID 71444: Dereference after null check (FORWARD_NULL). <a href='http://commits.kde.org/umbrello/8801f7981e9fc40ff9c64372246971ac8224f30d'>Commit.</a> See bug <a href='https://bugs.kde.org/340646'>#340646</a></li>
<li>Coverity check CID 152231:  Null pointer dereferences. <a href='http://commits.kde.org/umbrello/ce856e566a7a9e5551e1fa44af8e46f6917acb98'>Commit.</a> See bug <a href='https://bugs.kde.org/340646'>#340646</a></li>
<li>Fix similar bug with documentation dock widget on item selection. <a href='http://commits.kde.org/umbrello/2f83b9e84d5c389963264372b6d985da42d55b86'>Commit.</a> See bug <a href='https://bugs.kde.org/352740'>#352740</a></li>
<li>Fix 'No edit focus after double click on note widget'. <a href='http://commits.kde.org/umbrello/c0b2a9e482131f8ada3a64eb6e036b17b88f84f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352740'>#352740</a></li>
<li>Fix 'Umbrello add's 'package' stereotype after converting a class-or-package stereotyped uml object to package'. <a href='http://commits.kde.org/umbrello/9631f8703c1f54e523e8b1c43f253d4011fccd76'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352748'>#352748</a></li>
<li>Write package widgets into the xmi file using a "packagewidget" tag. <a href='http://commits.kde.org/umbrello/cd350fa7b2c0283331ec15cb784323930511b532'>Commit.</a> See bug <a href='https://bugs.kde.org/352597'>#352597</a>. See bug <a href='https://bugs.kde.org/336012'>#336012</a></li>
<li>Fix 'Umbrello deletes packages from Class Diagram'. <a href='http://commits.kde.org/umbrello/9adde748d73aa5ac9b3101724d880d58ff2514cd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352597'>#352597</a></li>
<li>Add AppStream metadata. <a href='http://commits.kde.org/umbrello/70ecfc71c7bf6400560b964d8ed9dfaf2a6abb57'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352607'>#352607</a></li>
</ul>
