<?php

  $page_title = "Anunţul de lansare KDE 4.0.1";
  $site_root = "../";
  include "header.inc";
?>

<p>PENTRU LANSARE IMEDIATĂ</p> 

<!-- Other languages translations  -->
Anunţ disponibil şi în:
<a href="http://www.kdecn.org/announcements/announce-4.0.1.php">Chinese</a>
<a href="announce-4.0.1.php">English</a>
<a href="http://fr.kde.org/announcements/announce-4.0.1.php">French</a>
<a href="announce-4.0.1-de.php">German</a>
<a href="announce-4.0.1-it.php">Italian</a>
<a href="announce-4.0.1-nl.php">Olandeză</a>

<h3 align="center">
   Proiectul KDE livrează prima versiune cu îmbunătăţiri şi traduceri a celui mai bun desktop din lumea softului liber
</h3>

<p align="justify">
  <strong> Comunitatea KDE livrează prima versiune cu traduceri şi îmbunătăţiri a desktop-ului liber 4.0, conţinând numeroase bugfix-uri, înbunătăţiri de performanţă şi actualizări ale traducerilor
</strong>
</p>

<p align="justify">
 5 Februarie 2008 (INTERNET). <a href=http://www.kde.org/>Comunitatea KDE</a> a anunţat astăzi imediata disponibilitate a KDE 4.0.1, prima versiune de mentenanţă şi bugfix-uri a ultimei generaţii a celui mai avansat şi performant desktop liber. KDE 4.0.1 vine cu un desktop de bază însoţit de multe alte pachete de administrare, reţea, educaţie, utilitare, multimedia, jocuri, grafică, dezvoltare web şi multe altele. Renumitele unelte şi aplicaţii KDE sunt disponibile în aproximativ 50 de limbi.
</p>
<p align="justify">
 KDE, incluzând toate librariile şi aplicaţiile sale, este disponibil gratuit sub licenţele Open Source. KDE poate fi obţinut în format cod sursă şi în diverse formate binare de la <a href=http://download.kde.org/stable/4.0.1/>http://download.kde.org</a> şi, de asemenea, poate fii obţinut pe <a href=http://www.kde.org/download/cdrom.php>CD-ROM</a> sau oricare din <a href=http://www.kde.org/download/distributions.php>sistemele GNU/Linux şi UNIX</a> livrate în ziua de azi.
</p>

<h4>
  <a name="changes">Îmbunătăţiri</a>
</h4>
<p align="justify">

KDE 4.0.1 e o versiune de mentenanţă, care aduce corectări problemelor raportate în <a href=http://bugs.kde.org/ id=hes5 title="Sistemul KDE de raportare şi urmărire a erorilor">Sistemul KDE de raportare şi urmărire a erorilor</a> şi îmbunătăţeşte suportul pentru traducerile existente deja, cât şi pentru cele noi.<p />Îmbunătăţirile din această versiune includ următoarele, dar nu se limitează doar la aceste lucruri:

<ul>
    <li>
    Konqueror, browserul web al KDE a avut parte de o mulţime de îmbunătăţiri legate de stabilitate şi performanţă la motorul de randare KHTML, la integrarea pluginului Flash, cât şi la KJS, motorul JavaScript.
    </li>
    <li>
    Probleme de stabilitate au fost descoperite în componente utilizate în întreg codul KDE, însă au fost rezolvate. Traducerile sunt de asemenea mai complete.
    </li>
    <li>
    KWin, managerul de ferestre al KDE a fost îmbunătăţit pentru a se integra mai bine cu modul de lucru composite (3D) şi anumite probleme legate de efecte au fost rezolvate.
    </li>
</ul>
</p>

<p align="justify">
Pe lângă aceste lucruri fundamentale, s-a lucrat şi la aplicaţii precum Okular, System Settings şi KStars. Noi traduceri au fost incluse: Daneză, Friziană, Cazacă şi Cehă.
 </p>

<p align="justify">
 Pentru mai multe detalii despre îmbunătăţirile aduse faţă de versiunea KDE 4.0 din ianuarie 2008, vă rugăm să aruncaţi o privire peste <a href=http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php id=po21 title="Istoricul de modificări al KDE 4.0.1">Istoricul de modificări al KDE 4.0.1</a>
</p>

<p align="justify">
 Informaţii adiţionale despre îmbunătăţirile versiunii din seria KDE 4.0.x sunt disponibile în <a href=http://www.kde.org/announcements/4.0/>anunţul de lansare KDE 4.0</a>.
</p>
<h4>
  Instalarea pachetelor binare KDE 4.0.1
</h4>
<p align="justify">
  
  Unii producători de Linux/UNIX OS au avut amabilitatea de a oferi pachete binare pentru KDE 4.0.1 la unele versiuni ale distribuţiilor acestora, dar in alte cazuri, voluntarii comunităţii au realizat aceste programe. Câteva dintre aceste pachete sunt gratuite şi disponibile la <a href=http://download.kde.org/binarydownload.html?url=/stable/4.0.1/>http://download.kde.org</a>.
  Pachete binare adiţionale , cât şi actualizarile pachetelor disponibile, pot să devină disponibile în săptămânile următoare.
</p>

<p align="justify">
  <a name="package_locations"><em>Locaţiile pachetelor</em></a>.
Pentru lista curentă a pachetelor binare disponibile despre care proiectul KDE a fost informat, vizitaţi <a href=/info/4.0.1.php>KDE 4.0.1 Info Page</a>.</p>

<h4>
  Compilarea KDE 4.0.1
</h4>
<p align="justify">
  <a name="source_code"></a><em>Codul sursă</em>.
  Codul sursă complet al KDE 4.0.1 poate fi descărcat de
  <a href=http://download.kde.org/stable/4.0.1/src/ id=gcsr title=aici>aici</a>. Instrucţiuni legate de compilarea şi instalarea KDE 4.0.1 sunt disponibile pe <a href=/info/4.0.1.php#binary>KDE 4.0.1 Info Page</a>.
</p>

<h4>
Sprijină KDE
</h4>
<p align="justify">
KDE este un proiect de tip <a href="http://www.gnu.org/philosophy/free-sw.html">Software Liber</a> care există ,şi creşte datorită ajutorului multor voluntari care îşi pun la dispoziţie timpul liber şi depun efort pentru acesta. KDE caută tot timpul voluntari şi contribuitori, fie că este vorba de programare, rezolvarea bug-urilor sau raportarea acestora, scrierea documentaţiei, traduceri, promovare, bani, etc. Toţi cei care contribuie sunt cu recunoştintă apreciaţi şi cu dorinţă acceptaţi. Te rugăm să citeşti in continuare in pagina <a
href="/community/donations/">Susţinerea KDE</a> pentru mai multe informaţii.
</p>

<p align="justify">
Aşteptăm să auzim veşti de la voi!
</p>

<h4>Despre KDE 4</h4>
<p>
KDE 4.0 este un produs inovativ al Free Software ca mediu de lucru ce conţine o mulţime de aplicaţii gratuite pentru utilizarea de zi cu zi cât şi pentru scopuri bine determinate, specifice anumitor arii de lucru. Plasma este noul mediu desktop dezvoltat pentru KDE 4 care asigură o interfaţă intuitivă ce facilitează interacţiunea cu mediul de lucru şi aplicaţiile. Konqueror, navigatorul web integrează mediul online cu spaţiul de lucru. Managerul de fişiere Dolphin, vizualizatorul de documente Okular şi Centrul de control al setărilor de sistem completează un set complet de <i>mediu de lucru</i>.<br>
  KDE este construit în interacţiune cu librăriile KDE care asigură un acces uşor la resursele din reţea şi nu numai, prin intermediul interfeţei KIO şi a capacităţilor avansate oferite de Qt4. Phonon şi Solid, la rândul lor făcând parte din librăriile KDE, adaugă un cadru de lucru multimedia şi o mai bună coordonare a tuturor aplicaţiilor KDE cu resursele hardware.</p>


<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contacte de presă</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
