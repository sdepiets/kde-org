<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.33.0");
  $site_root = "../";
  $release = '5.33.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
April 08, 2017. KDE today announces the release
of KDE Frameworks 5.33.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Added description for commands (balooctl)");?></li>
<li><?php i18n("Search also in symlinked directories (bug 333678)");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Provide device type for Low Energy devices");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Specify qml-root-path as the share directory in the prefix");?></li>
<li><?php i18n("Fix ecm_generate_pkgconfig_file compatibility with new cmake");?></li>
<li><?php i18n("Only register APPLE_* options if(APPLE)");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Added presets to the testing application");?></li>
<li><?php i18n("Properly moving items to the desired position");?></li>
<li><?php i18n("Syncing reordering to other model instances");?></li>
<li><?php i18n("If the order is not defined, sort the entries by the id");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("[Meta] Change maintainer in setup.py");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Backend for Mac");?></li>
<li><?php i18n("Add support for killing a KAuth::ExecuteJob");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Sanitize shortcut list on read/write from kdeglobals");?></li>
<li><?php i18n("Avoid useless reallocs by removing squeeze call on temporary buffer");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("KDBusService: Add accessor for the dbus service name we registered");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("With Qt &gt;= 5.8 use the new API to set scene graph backend");?></li>
<li><?php i18n("Don't set acceptHoverEvents in DragArea as we don't use them");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("meinproc5: link to the files, not to the library (bug 377406)");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Make PlainTextExtractor match \"text/plain\" again");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Error page, correctly load the image (with a real URL)");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Make remote file:// URL redirect to smb:// work again");?></li>
<li><?php i18n("Keep query encoding when HTTP Proxy is used");?></li>
<li><?php i18n("Updated user agents (Firefox 52 ESR, Chromium 57)");?></li>
<li><?php i18n("Handle/truncate url display string assigned to job description. Prevents large data: urls from being included in UI notifications");?></li>
<li><?php i18n("Add KFileWidget::setSelectedUrl() (bug 376365)");?></li>
<li><?php i18n("Fix KUrlRequester save mode by adding setAcceptMode");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("Mention the new QSFPM::setRecursiveFiltering(true) which makes KRecursiveFilterProxyModel obsolete");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Do not remove queued notifications when the fd.o service starts");?></li>
<li><?php i18n("Mac platform adaptations");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("API dox: fix missing note to call setXMLFile with KParts::MainWindow");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Fix 'Not found: \"\"' terminal messages");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Expose additional internal View's functionality to the public API");?></li>
<li><?php i18n("Save a lot of allocation for setPen");?></li>
<li><?php i18n("Fix ConfigInterface of KTextEditor::Document");?></li>
<li><?php i18n("Added font and on-the-fly-spellcheck options in ConfigInterface");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Add support for wl_shell_surface::set_popup and popup_done");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Support building against a qt without a11y enabled");?></li>
<li><?php i18n("Fix wrong size hint when animatedShow is called with hidden parent (bug 377676)");?></li>
<li><?php i18n("Fix characters in KCharSelectTable getting elided");?></li>
<li><?php i18n("Enable all planes in kcharselect test dialog");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("WiredSetting: return autonegotiate even when it's disabled");?></li>
<li><?php i18n("Prevent signals in glib2 be defined by Qt");?></li>
<li><?php i18n("WiredSetting: Speed and duplex can be set only when auto-negotiation is off (bug 376018)");?></li>
<li><?php i18n("Auto-negotiate value for wired setting should be false");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[ModelContextMenu] Use Instantiator instead of Repeater-and-reparent-hack");?></li>
<li><?php i18n("[Calendar] Shrink and elide week names like is done with day delegate (bug 378020)");?></li>
<li><?php i18n("[Icon Item] Make \"smooth\" property actually do something");?></li>
<li><?php i18n("Set implicit size from source size for image/SVG URL sources");?></li>
<li><?php i18n("add a new property in containment, for an edit mode");?></li>
<li><?php i18n("correct maskRequestedPrefix when no prefix is used (bug 377893)");?></li>
<li><?php i18n("[Menu] Harmonize openRelative placement");?></li>
<li><?php i18n("Most (context) menus have accelerators (Alt+letter shortcuts) now (bug 361915)");?></li>
<li><?php i18n("Plasma controls based on QtQuickControls2");?></li>
<li><?php i18n("Handle applyPrefixes with an empty string (bug 377441)");?></li>
<li><?php i18n("actually delete old theme caches");?></li>
<li><?php i18n("[Containment Interface] Trigger context menus on pressing \"Menu\" key");?></li>
<li><?php i18n("[Breeze Plasma Theme] Improve action-overlay icons (bug 376321)");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("TOML: Fix highlighting of string escape sequences");?></li>
<li><?php i18n("Update Clojure syntax highlighting");?></li>
<li><?php i18n("A few updates to OCaml syntax");?></li>
<li><?php i18n("Hightlight *.sbt files as scala code");?></li>
<li><?php i18n("Also use the QML highlighter for .qmltypes files");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.33");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.6");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
