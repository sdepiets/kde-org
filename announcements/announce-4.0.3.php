<?php
  $page_title = "KDE 4.0.3 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Also available in:
<a href="announce-4.0.3-it.php">Italian</a>
<a href="http://fr.kde.org/announcements/announce-4.0.3.php">French</a>
<a href="announce-4.0.3-pt_BR.php">Portuguese (Brazilian)</a>
<a href="announce-4.0.3-es.php">Spanish</a>
<a href="announce-4.0.3-sv.php">Swedish</a>

<!--
<a href="announce-4.0.3-bn_IN.php">Bengali (India)</a>
<a href="announce-4.0.3-ca.php">Catalan</a>
<a href="http://www.kdecn.org/announcements/announce-4.0.3.php">Chinese</a>
<a href="announce-4.0.3-cz.php">Czech</a>
<a href="announce-4.0.3-nl.php">Dutch</a>
<a href="announce-4.0.3.php">English</a>
<a href="http://fr.kde.org/announcements/announce-4.0.3.php">French</a>
<a href="announce-4.0.3-de.php">German</a>
<a href="announce-4.0.3-gu.php">Gujarati</a>
<a href="announce-4.0.3-he.php">Hebrew</a>
<a href="announce-4.0.3-hi.php">Hindi</a>
<a href="announce-4.0.3-it.php">Italian</a>
<a href="announce-4.0.3-lv.php">Latvian</a>
<a href="announce-4.0.3-ml.php">Malayalam</a>
<a href="announce-4.0.3-mr.php">Marathi</a>
<a href="announce-4.0.3-fa.php">Persian</a>
<a href="announce-4.0.3-pl.php">Polish</a>
<a href="announce-4.0.3-pa.php">Punjabi</a>
<a href="announce-4.0.3-pt_BR.php">Portuguese (Brazilian)</a>
<a href="announce-4.0.3-ro.php">Romanian</a>
<a href="announce-4.0.3-ru.php">Russian</a>
<a href="announce-4.0.3-sl.php">Slovenian</a>
<a href="announce-4.0.3-es.php">Spanish</a>
<a href="announce-4.0.3-sv.php">Swedish</a>
<a href="announce-4.0.3-ta.php">Tamil</a>
-->

<!-- // Boilerplate -->

<h3 align="center">
  KDE Project Ships Third Translation and Service Release for Leading Free Software Desktop
</h3>

<p align="justify">
  <strong>
KDE Community Ships Third Translation and Service Release of the 4.0
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and 
Translation Updates
</strong>
</p>

<p align="justify">
 April 2, 2008 (The INTERNET). The <a href="http://www.kde.org/">KDE
Community</a> today announced the immediate availability of KDE 4.0.3, the third
bugfix and maintenance release for the latest generation of the most advanced and powerful
free desktop. KDE 4.0.3 is the third monthly update to <a href="4.0/">KDE 4.0</a>. It 
ships with a basic desktop and many other packages; like administration programs, network tools, 
educational applications, utilities, multimedia software, games, artwork, 
web development tools and more. KDE's award-winning tools and applications are 
available in 49 languages.
</p>
<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.0.3/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<!-- // Meat -->

<h4>
  <a name="changes">Enhancements</a>
</h4>
<p align="justify">
KDE 4.0.3 comes with an impressive amount of bugfixes and improvements. Most of them are 
recorded in the 
<a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php">changelog</a>.
KDE continues to release updates for the 4.0 desktop on a monthly basis. KDE 4.1, which will
bring <a href="http://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Feature_Plan">large 
improvements</a> to the KDE desktop and application will be released in July this year.
<br />
KDE 4.0.3 improvements revolve around lots of bugfixes and translation updates. 
Corrections have been made in such a way that results in only a minimal risk of 
regressions. For KDE, it is also a way to deliver bugfixes quickly to the users.

An excerpt from the changelog reveals that nearly all modules in KDE have seen 
lots of improvements. Again, the KHTML team has done an awesome job
in improving the user experience with the Konqueror web browser.
</p>
<ul>
  <li>Scrolling optimisations in KHTML, KDE's HTML rendering engine </li>
  <li>Improved handling of dialog windows in KWin, KDE's window manager </li>
  <li>Various rendering improvements in Okular, KDE's document viewer  </li>
</ul>

<h4>Extragear</h4>
<p align="justify">
Since KDE 4.0.0, <a href="http://extragear.kde.org">Extragear</a> applications 
are also part of regular KDE releases. 
Extragear applications are KDE applications that are mature, but not part
of one of the other KDE packages. The extragear package that is shipped with KDE 4.0.3
ship the following programs:
</p>
<ul>
    <li><a href="http://en.wikipedia.org/wiki/KColorEdit">KColoredit</a> -  An editor 
        for color palette files that supports KDE and Gimp color palette formats</li>
    <li>KFax - A desktop fax viewer </li>
    <li><a href="http://www.kde-apps.org/content/show.php/KGrab?content=74086">KGrab</a> - 
        A more advanced screenshot taking tool </li>
    <li><a href="http://extragear.kde.org/apps/kgraphviewer/">KGraphviewer</a> - A 
        GraphViz dot graph viewer for KDE</li>
    <li><a href="http://w1.1358.telia.com/~u135800018/prog.html#KICONEDIT">KIconedit</a> - 
        A drawing program for icon graphics</li>
    <li><a href="http://kmldonkey.org/">KMldonkey</a> - A graphical client for the
        EDonkey network</li>
    <li><a href="http://www.kpovmodeler.org/">KPovmodeler</a> - A 3D modeler</li>
    <li>Libksane - An image-scanning library</li>
    <li><a href="http://www.rsibreak.org">RSIbreak</a> - A program that saves you from 
        getting RSI by enforcing breaks </li>
</ul>
<p align="justify">
New in this release of the Extragear application is the 
Gopher KIO slave, a plugin that adds support for  the 
<a href="http://en.wikipedia.org/wiki/Gopher_(protocol)">Gopher protocol</a> to all 
KDE applications.
</p>

<h4>
KDE's Educational Application Suite
</h4>
<p align="justify">
KDE 4.0.3 ships with a suite of high-quality <a href="http://edu.kde.org">educational 
software</a>. The applications range
from <a href="http://edu.kde.org/marble/">Marble</a>, the versatile desktop globe, to a fun 
small game for younger kids.
</p>
<p align="justify">
Kalzium is a graphical periodic table of elements. It visualizes abstract concepts such
as atoms attractively. Kalzium also brings numerous ways to display detailed information
about elements. Kalzium has been built as a tool which makes chemistry easily understandable
for high-school kids - but it is also a lot of fun to play with for grown-ups.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/kalzium.png">
    <img src="announce_4.0.3/kalzium_thumb.png" align="middle"  height="261" alt="Kalzium" />
  </a>
  <br /><em>Experience chemistry with Kalzium</em>
</div>

<p align="justify">
Parley is a program to help you memorize vocabulary. Parley supports many language-specific 
features but can be used for other learning tasks just as well. It uses the spaced 
repetition learning method, also known as flash cards. 
Creating new vocabulary collections with Parley is easy, but of course it is even 
better if you can use some of our premade files that can be downloaded from the
Internet.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/parley.png">
    <img src="announce_4.0.3/parley_thumb.png" align="middle"  height="225" alt="Parley" />
  </a>
  <br /><em>Practise vocabulary with Parley</em>
</div>

<p align="justify">
Kmplot is a mathematical function plotter, providing an easy tool for a better understanding
of maths. You can easily input mathemetical functions with it and see the graphs those
functions describe.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/kmplot.png">
    <img src="announce_4.0.3/kmplot_thumb.png" align="middle"  height="341" alt="Kmplot" />
  </a>
  <br /><em>Do the math with Kmplot</em>
</div>

<p align="justify">
Those that want more information about KDE's educational applications should take
the <a href="http://edu.kde.org/tour_kde4.0/">KDE Education Project Tour</a>.
</p>

<!-- // Boilerplate again -->

<h4>
  Installing KDE 4.0.3 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.0.3
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.0.3/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.0.3.php">KDE 4.0.3 Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.0.3
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 4.0.3 may be <a
href="http://download.kde.org/stable/4.0.3/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.0.3
  are available from the <a href="/info/4.0.3.php#binary">KDE 4.0.3 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
project that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>About KDE 4</h4>
<p align="justify">
KDE 4.0 is the innovative Free Software desktop containing lots of applications
for every day use as well as for specific purposes. Plasma is a new desktop
shell developed for
KDE 4, providing an intuitive interface to interact with the desktop and
applications. The Konqueror web browser integrates the web with the desktop. The
Dolphin file manager, the Okular document reader and the System Settings control
center complete the basic desktop set. 
<br />
KDE is built on the KDE Libraries which provide easy access to resources on the
network by means of KIO and advanced visual capabilities through Qt4. Phonon and
Solid, which are also part of the KDE Libraries add a multimedia framework and
better hardware integration to all KDE applications.
</p>


<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
