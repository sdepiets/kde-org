<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.9.95 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.9.95";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a></h3>
<ul id='ulbreeze' style='display: block'><li>New in this release</li></ul>
<h3><a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a> </h3>
<ul id='ulbreeze-gtk' style='display: block'>
<li>Revert "Revert "No need for WITH_GTK3_VERSION anymore"". <a href='https://commits.kde.org/breeze-gtk/ea50a9231e505b589ff59b0c9ed8d515334266e4'>Commit.</a> </li>
<li>Revert "Revert "Figure out the GTK version instead of using a hardcoded one"". <a href='https://commits.kde.org/breeze-gtk/d219a38b1d17aa13f7912db79f8f8e48f74c33ca'>Commit.</a> </li>
</ul>


<h3><a name='breeze-plymouth' href='https://commits.kde.org/breeze-plymouth'>Breeze Plymouth</a> </h3>
<ul id='ulbreeze-plymouth' style='display: block'>
<li>Install preview image. <a href='https://commits.kde.org/breeze-plymouth/919f19b6338888f0630c54aac4230eae94d394f5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5760'>D5760</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Readability & testability. <a href='https://commits.kde.org/discover/17cb9796cc0e7eb151cbcf56daf4082c2178b744'>Commit.</a> </li>
<li>Use Kirigami.Icon instead of QIconItem. <a href='https://commits.kde.org/discover/361f408eb00cfc11c27c865eb844e019ff765e2f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5769'>D5769</a></li>
<li>Make sure transactions are started. <a href='https://commits.kde.org/discover/f1087b1676c8877e1f632ba5539edcd7d43f735f'>Commit.</a> </li>
<li>Don't add weird spaces on debug messages. <a href='https://commits.kde.org/discover/d29c0e55c51c12d7eff0afd5c59c6026df8fbf01'>Commit.</a> </li>
<li>Remove useless casting. <a href='https://commits.kde.org/discover/8ec3e84f4bdb0e3e10e6dbbb9343d60933208076'>Commit.</a> </li>
<li>Make sure the focus is initially on the drawer. <a href='https://commits.kde.org/discover/7aeec871a06be0ed4ae07890bbd089ea50fda25c'>Commit.</a> </li>
<li>Use flatpak_get_system_installations to set up the system installations. <a href='https://commits.kde.org/discover/21e4312f5257b9376318c21824122f773eae8e31'>Commit.</a> </li>
<li>Remove the concept of scope. <a href='https://commits.kde.org/discover/6d1c4f6a2d2fb370e7d095d687f33af22c1b9465'>Commit.</a> </li>
<li>Reduce semantics between user and system installations. <a href='https://commits.kde.org/discover/d873c0b8a995f7ddd9dac7810a62c15f99f3eddd'>Commit.</a> </li>
<li>Simplify. <a href='https://commits.kde.org/discover/b464b2ef5ae723f4e00a986067da77ad4b670e36'>Commit.</a> </li>
<li>Improve interaction with --list* arguments. <a href='https://commits.kde.org/discover/f3d4cbdbe7d5b9f6b206a1aace16ca8c0f5a73c6'>Commit.</a> </li>
<li>Properly construct strings. <a href='https://commits.kde.org/discover/982f1e17e5620cd50936d54fd424f9cf26a26ae3'>Commit.</a> </li>
<li>Make sure the backend has been set up properly. <a href='https://commits.kde.org/discover/ffd101f129a93eefc5f8a88d25f02b831b27fec0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5495'>D5495</a></li>
<li>Queue some connections which cannot be autodetected. <a href='https://commits.kde.org/discover/639bf33c019266fab13286f0e0811b5a7f133c72'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5409'>D5409</a></li>
<li>Don't open every file with the packagekit backend. <a href='https://commits.kde.org/discover/85ffb0cf458f3516f3004aac7fe1f81489ee288e'>Commit.</a> </li>
<li>Fix warning. <a href='https://commits.kde.org/discover/43051eef19e56eb186322d2ddd89cc12da6eedbd'>Commit.</a> </li>
<li>Improve the FeaturedModel. <a href='https://commits.kde.org/discover/8e9a3aa84a272706d92c71e6ba23b1f59f096277'>Commit.</a> </li>
<li>Don't show anything with the PageHeader extra. <a href='https://commits.kde.org/discover/55b624c15c062a0ce98077957754217790fc3a38'>Commit.</a> </li>
<li>Fix build, we apparently have to always return something. <a href='https://commits.kde.org/discover/8e31cac0ff4c2bd49ce07c624c1fee218c8095f9'>Commit.</a> </li>
<li>Include a background behind the extra part of the controls in PageHeader. <a href='https://commits.kde.org/discover/0735be3a301255ee4b9ba831ab945a8960d4a5ec'>Commit.</a> </li>
<li>Improve the behavior when closing. <a href='https://commits.kde.org/discover/a640cf42a4344715d3a39cfae63d6b3a8bd44d6e'>Commit.</a> </li>
<li>Let install button be flat in the toolbar, full elsewhere. <a href='https://commits.kde.org/discover/3b3f51d406c8a55c5e082f4c93af0bca61946d9f'>Commit.</a> </li>
<li>Introduce the Launch button back. <a href='https://commits.kde.org/discover/2309a527f8885714178174e20f6fad87510c3ba1'>Commit.</a> </li>
<li>Fix few warnings. <a href='https://commits.kde.org/discover/6d923fae5fc87e5b5799040533f6a160cb87ad2b'>Commit.</a> </li>
<li>Fix test when plasma-themes.knsrc is not installed. <a href='https://commits.kde.org/discover/449862d95afd44d56e16db286730c286c4cadf2d'>Commit.</a> </li>
<li>Remove unneeded checks. <a href='https://commits.kde.org/discover/800d197c8f110da17efed70d5914e812dfb8c12d'>Commit.</a> </li>
<li>Report the updatesCount after asserting progress. <a href='https://commits.kde.org/discover/43dee4ec093521430567bd62aebcfdb2ca472553'>Commit.</a> </li>
<li>Properly refer to the actual model. <a href='https://commits.kde.org/discover/0b848eaca447d9d3341a54349b88f51ee6dd738c'>Commit.</a> </li>
<li>Make sure we just run the tests once. <a href='https://commits.kde.org/discover/146df22e2b2616805786d02811e5218028f55f2a'>Commit.</a> </li>
<li>Revert "Properly ensure we don't announce empty lists". <a href='https://commits.kde.org/discover/050451f172c0a107a763b8ca9272966001fb7484'>Commit.</a> </li>
<li>Properly ensure we don't announce empty lists. <a href='https://commits.kde.org/discover/92e06708b0f400046d34379adc3fa6f8ccda92df'>Commit.</a> </li>
<li>Flatpak backend: show error message when we fail to install ref file. <a href='https://commits.kde.org/discover/1ed85d10edc4e6fa4e124a64e67f82ea60bedd7a'>Commit.</a> </li>
<li>Flatpak backend: set that we already know information about size for bundles. <a href='https://commits.kde.org/discover/e462516a14a6bcf385635898e19d8ddd940ebd54'>Commit.</a> </li>
<li>Revert the change of InstallApplicationButton into ToolButton for now. <a href='https://commits.kde.org/discover/0b20c6884b0d1cba71b5cdb2c63de81c1d7f7d49'>Commit.</a> </li>
<li>Fix tests. <a href='https://commits.kde.org/discover/227869c9e821bddd7f6c277909feb1658cb11230'>Commit.</a> </li>
<li>Fix license on the appdata file. <a href='https://commits.kde.org/discover/69cb6cea0f8c50f7eb71c5252a3b8d0e04b3ef5d'>Commit.</a> </li>
<li>Can't cancel if there's just one page either. <a href='https://commits.kde.org/discover/7c93081c7465ef1fa8a47f187bf83de84879949d'>Commit.</a> </li>
<li>Include a bottom toolbar for the application page. <a href='https://commits.kde.org/discover/650eb5ccbeb56e90580ff01a59cef3d8efac85c9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5339'>D5339</a></li>
<li>Flatpak backend: require information about runtime only for applications. <a href='https://commits.kde.org/discover/8bb30e142faf7552ec58995cb7017ebcaf2e548f'>Commit.</a> </li>
<li>Update description for flatpak backend. <a href='https://commits.kde.org/discover/aae56044e6ea100aa63f4253280fa0819fde6aee'>Commit.</a> </li>
<li>Stop perpetual search state for StaticXml KNS stuff. <a href='https://commits.kde.org/discover/a8031fead6a69b273293340e7d50aa39b5accc7e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5240'>D5240</a></li>
<li>Flatpak backend: delay app installation and wait for information about required runtime. <a href='https://commits.kde.org/discover/b1ff80dc72387378cfbd889effdc3a7bbbd726f4'>Commit.</a> </li>
<li>Use better description for size when size is not known. <a href='https://commits.kde.org/discover/c2cc4b1e27c82e8a99731470d522c45405b15702'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5219'>D5219</a></li>
<li>Flatpak backend: emit state changed only when it actually changes. <a href='https://commits.kde.org/discover/6a71d25b43038ec2ebdc21d2c724f0252f469dad'>Commit.</a> </li>
<li>Implement UI feedback for long-running searches, and no-results. <a href='https://commits.kde.org/discover/73b61fcdf1408871de85d60c7341e283194b92c6'>Commit.</a> </li>
<li>Mark "all categories missing" kns sources as invalid. <a href='https://commits.kde.org/discover/bdccef045c12790c30648cb5de0096ab443325a4'>Commit.</a> </li>
<li>Use the back function, rather than pop, when pressing Escape. <a href='https://commits.kde.org/discover/8bb5178bd65ee51e3a487ec22bb031d6bd9233fa'>Commit.</a> </li>
<li>Enable flatpak backend by default. <a href='https://commits.kde.org/discover/23190c98ac787d49feb1893d670fad31ab1789ee'>Commit.</a> </li>
<li>Flatpak notifier: use monitor to watch flatpak installations and re-check updates on change. <a href='https://commits.kde.org/discover/8f9bb31bd19b8f50423f26f14c56e971655b373e'>Commit.</a> </li>
<li>Use QString::split() instead of QString::splitRef as it causes weird issues. <a href='https://commits.kde.org/discover/3449a0039db481af206b123cccfc2cbcbf695a1b'>Commit.</a> </li>
<li>Remove useless debug output. <a href='https://commits.kde.org/discover/00681078593a7188b2cf0edc14a756c5bd5a79cd'>Commit.</a> </li>
<li>Flatpak backend: implement backend notifier. <a href='https://commits.kde.org/discover/c33241be12b1dcdab3cd7bb03356b933270bb46b'>Commit.</a> </li>
<li>Cache screenshots to avoid downloading them over and over. <a href='https://commits.kde.org/discover/d7e78e4c0422f3de3882d6e136b4632323ca9054'>Commit.</a> </li>
<li>ODRS backend: Use correct logic and even one day old ratings should be fine. <a href='https://commits.kde.org/discover/7c2b35cdffa0cdd4130f7939154e375264604477'>Commit.</a> </li>
<li>ODRS backend: download ratings to ratings subdir. <a href='https://commits.kde.org/discover/850cdbe85d043dc78222944361b3150c1fa5715c'>Commit.</a> </li>
<li>Flatpak backend: download temporary icons to icons subdir. <a href='https://commits.kde.org/discover/51f82e97ad7edcd7f57d906407bac38c3ba65c75'>Commit.</a> </li>
<li>Flatpak backend: make fetching remote metadata async. <a href='https://commits.kde.org/discover/e97912c2d1a5a0631912a8f6d2faa7ded5703826'>Commit.</a> </li>
<li>Add sane check for review dialog. <a href='https://commits.kde.org/discover/a7efe9becea107f91b3f96a9347249b1d677c4d8'>Commit.</a> </li>
<li>Mark local file resources as installed once the transaction finishes. <a href='https://commits.kde.org/discover/15d373ca47147f3989382dd6fa2fc97dfb456081'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377658'>#377658</a></li>
<li>ODRS: Support rating reviews. <a href='https://commits.kde.org/discover/b3e555c8f944d48f5f50291c56e168c36f71554c'>Commit.</a> </li>
<li>UI feedback when checking for updates. <a href='https://commits.kde.org/discover/eb8f4ae2b46a0516a99d26b25f120a5be28e910c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5046'>D5046</a></li>
<li>Add a review button when there's no page, for now. <a href='https://commits.kde.org/discover/d99c6e00aac169f7acda39518660bbb0663199c4'>Commit.</a> </li>
<li>Fix warning. <a href='https://commits.kde.org/discover/c059b7955a434962e5ab793d753d62031b4fab4c'>Commit.</a> </li>
<li>Elide text review title. <a href='https://commits.kde.org/discover/5093b6df5374c323791e8e798bd704cf06888b56'>Commit.</a> </li>
<li>Case-insensitive comparison of appstream ids. <a href='https://commits.kde.org/discover/43d7a8656d99414ea0cd297b8b6a9ded5ced05c0'>Commit.</a> </li>
<li>Fix build. <a href='https://commits.kde.org/discover/38b084e1dd5da516d9a0d3986c8d69577aec6a9f'>Commit.</a> </li>
<li>Use units-aware spacing values whenever possible. <a href='https://commits.kde.org/discover/4acd29a52da4907276314783f455c1d54b0af7fb'>Commit.</a> </li>
<li>Fix weird flicker on some ReviewPage instances. <a href='https://commits.kde.org/discover/ca7a7cc0aaad3f61f1448eaca0075817e0505aa8'>Commit.</a> </li>
<li>Let resources announce that they got ratings. <a href='https://commits.kde.org/discover/16a806029ca804b834a2f8fd075c98d57007d0bd'>Commit.</a> </li>
<li>Don't use split if we can use splitRef. <a href='https://commits.kde.org/discover/10958d5e7c442726608c4301adbc496195fcc293'>Commit.</a> </li>
<li>Fix build. <a href='https://commits.kde.org/discover/e109cd4f0c4b9cd4d427a737c876bb87b31a3b3d'>Commit.</a> </li>
<li>DummyBackend: Fix reviewsReady() signal. <a href='https://commits.kde.org/discover/f5b9584f1d6d0fcf8cc7cf25fd031e5ab8ba60f2'>Commit.</a> </li>
<li>ODRS: Support to post reviews. <a href='https://commits.kde.org/discover/f1cbe63b6e6c9836cac12dccf45782db6f53740b'>Commit.</a> </li>
<li>Use correct reference to the review model. <a href='https://commits.kde.org/discover/bf72d2c7107b204c31cf522729a45f344be1e917'>Commit.</a> </li>
<li>Reverse logic. <a href='https://commits.kde.org/discover/d740b01180300f26419a1d6c3fc394fb5b37cc7e'>Commit.</a> </li>
<li>Minor improvements to review delegate. <a href='https://commits.kde.org/discover/ed0aea8216e42d9ded8a5ececcfe69224f7c230a'>Commit.</a> </li>
<li>Initial support for open desktop rating system. <a href='https://commits.kde.org/discover/5440a96cac8cb5ad9a0f1bd0248a40c3f026f844'>Commit.</a> </li>
<li>Check the correct enabled property for updateButton background. <a href='https://commits.kde.org/discover/02e9cd6399d3968743dfa54d343158095e757d04'>Commit.</a> </li>
<li>Remove unneeded logic. <a href='https://commits.kde.org/discover/8f8482d19d53846561f7c6324556b3d9a6448581'>Commit.</a> </li>
<li>Allow to close the current page by pressing escape. <a href='https://commits.kde.org/discover/58c8afba8166a6e91f3180d6443eaabc27603d73'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377384'>#377384</a></li>
<li>Mark flatpak runtimes as technical package. <a href='https://commits.kde.org/discover/d9e73438f8be3b2423451e6dc88521746bbbbb09'>Commit.</a> </li>
<li>Download remote icons during initialization of FlatpakResource items. <a href='https://commits.kde.org/discover/285cda6a8c14a2ffdcfe7c42d2ddc4552beceaee'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4977'>D4977</a></li>
<li>Notify about changed download/installed size. <a href='https://commits.kde.org/discover/0f40189756264776edcce245237c0dac9ba69e92'>Commit.</a> </li>
<li>Clean unused code. <a href='https://commits.kde.org/discover/713b2d151667bbc6b73d7f0d28a3a7554281a8ab'>Commit.</a> </li>
<li>Do not ask users whether they want to add repositories from .flatpakref files. <a href='https://commits.kde.org/discover/fbce3a1fc8de25b246e9acbf577dd7ffe079283c'>Commit.</a> </li>
<li>Do not show repositories which are marked not to be listed. <a href='https://commits.kde.org/discover/2b33557e1f9b0c32094933e89bb53e4c70092cfb'>Commit.</a> </li>
<li>Make UI to do difference between download and installed size. <a href='https://commits.kde.org/discover/f3bb914f21dc14a07f7f275dd38b45c740b8bb90'>Commit.</a> </li>
<li>Add support for .flatpakrepo files. <a href='https://commits.kde.org/discover/c1bd05a1bd3fb79a2c6b76ba9343fa6ac65091c3'>Commit.</a> </li>
<li>Adapt to changes in 5.9. <a href='https://commits.kde.org/discover/3928d93109f91f571b8f1e6707762e0a46bcf576'>Commit.</a> </li>
<li>List flatpak remote repositories. <a href='https://commits.kde.org/discover/6b4ffa5deacdbee6057ef29f28e5ea3224974fea'>Commit.</a> </li>
<li>Get rid of "(Nightly)" suffix. <a href='https://commits.kde.org/discover/03452275d2ee59e8692bf6137cbea7a24023d96a'>Commit.</a> </li>
<li>Property now in Kirigami. <a href='https://commits.kde.org/discover/c3dfdac65854f7e9c3f2357539ecf253ad4b50b0'>Commit.</a> </li>
<li>Update progress once transaction is done. <a href='https://commits.kde.org/discover/f81edd10ec66cfb04214305a21ea3490bf1293db'>Commit.</a> </li>
<li>Add asynchronous jobs for fetching updates and app size. <a href='https://commits.kde.org/discover/490237b30eac0cb1231e3aa35e75475ec53e1fac'>Commit.</a> </li>
<li>Use the headers from Kirigami instead of our own. <a href='https://commits.kde.org/discover/64d334d8bb7b3ee397aba9a3141d731d04167741'>Commit.</a> </li>
<li>Iterate page headers. <a href='https://commits.kde.org/discover/ea811437d6e7a0370783a579eca2e8b8c9ec095c'>Commit.</a> </li>
<li>Make sure we match apps we parsed from exported desktop files with flatpak refs. <a href='https://commits.kde.org/discover/6eb02e9cbed2754b024e60efd08561c2686bf649'>Commit.</a> </li>
<li>Fix parsing decompressed appstream metadata. <a href='https://commits.kde.org/discover/6fc5fd8ac3031ceffd419d15eb29337262ca3c14'>Commit.</a> </li>
<li>Add support for flatpak bundles. <a href='https://commits.kde.org/discover/36148ca07414bcce3d05536ee372f0a6002a4cdf'>Commit.</a> </li>
<li>Get as most information we can from the flatpakref, also set correct name. <a href='https://commits.kde.org/discover/72f63ea03dcccd83a4c5d0f31f8506de790106b1'>Commit.</a> </li>
<li>Update app size to get download size for removed application. <a href='https://commits.kde.org/discover/5420ff8454c743a5b42a24bfee69925167e769a2'>Commit.</a> </li>
<li>Differentiate download and installed size for upgradable applications. <a href='https://commits.kde.org/discover/056e690f455df3a235e99d905ecf29425c33d579'>Commit.</a> </li>
<li>Allow to update applications. <a href='https://commits.kde.org/discover/e75dc3a65489ccf07a4f2ef131f0d9b73cedd479'>Commit.</a> </li>
<li>Remove jobs once they are finished so we don't remove still running thread. <a href='https://commits.kde.org/discover/8cabfa8e4e3cdcaa5160d6674e2b870f05395314'>Commit.</a> </li>
<li>Do not connect runtime job to app related slots and vise versa. <a href='https://commits.kde.org/discover/a3060f485b600e8f6bcc6905baab94d345cce4dd'>Commit.</a> </li>
<li>Add handler for opening flatpakref files. <a href='https://commits.kde.org/discover/aa6cae7516c467d03985a700bae6191f52ee7e39'>Commit.</a> </li>
<li>Fix compilation, obviously passing stringRef as arguments is not possible. <a href='https://commits.kde.org/discover/c48a1c3e1a0a7a8dee660e21e27bbd8f1f8b0a88'>Commit.</a> </li>
<li>Reduce QString -> QByteArray -> QString conversion. <a href='https://commits.kde.org/discover/5a2e1b36f93bab2580d79e5d14ff0a163549b482'>Commit.</a> </li>
<li>Some code clean-up. <a href='https://commits.kde.org/discover/e1b7ed148d08688d40f45fff145b2267ab67c6e9'>Commit.</a> </li>
<li>Don't leak duplicated resources. <a href='https://commits.kde.org/discover/638e468eb983a2616d573ad00c5b06a91fab3678'>Commit.</a> </li>
<li>Implement AppStream urls for Flatpak. <a href='https://commits.kde.org/discover/9583b32b3a68b59955a86c87ac4771356749b47f'>Commit.</a> </li>
<li>Make it possible to open flatpakref files. <a href='https://commits.kde.org/discover/8e30e7fbd500e3ed98574d2d8ca6fd5b03b32458'>Commit.</a> </li>
<li>Share code for FlatpakRef to FlatpakResource synchronizing. <a href='https://commits.kde.org/discover/2f6a492ccad3ccbd3b097d44285744ef304e7a12'>Commit.</a> </li>
<li>Also clean-up transactions that fail. <a href='https://commits.kde.org/discover/3ea813f582910791251f040a9bd1809866ace123'>Commit.</a> </li>
<li>Use smart pointers to track transaction objects. <a href='https://commits.kde.org/discover/cd298256d6bb59b5d2dc804acf39ca131feec2e0'>Commit.</a> </li>
<li>Don't emit jobFinished twice. <a href='https://commits.kde.org/discover/930a04eb7b99866b19b4a4b5d6c350b15420556b'>Commit.</a> </li>
<li>List available updates. <a href='https://commits.kde.org/discover/31f10fbbbd77127a533b714e8dfdd0eda2bcdb56'>Commit.</a> </li>
<li>Automatically install required runtime when it's not installed. <a href='https://commits.kde.org/discover/01d4234fae276ca46d18ce78f42075bd126e1965'>Commit.</a> </li>
<li>Properly set information whether we are fetching data or not. <a href='https://commits.kde.org/discover/0fd62048c747729f7ff4ef9b470ed3d206e49a6f'>Commit.</a> </li>
<li>Add also apps which don't have appstream metadata, but were found by desktop file. <a href='https://commits.kde.org/discover/0bf6257f6256ace5156edacb18331bcf911d27aa'>Commit.</a> </li>
<li>Port from libappstream-glib to libappstream to avoid collision. <a href='https://commits.kde.org/discover/c923d20f9c3f092b3c05bf275e6691b1c247dfff'>Commit.</a> </li>
<li>Improve the information we're offering about Snap packages. <a href='https://commits.kde.org/discover/9111eb655ff42c525e373a2e7cc4f90fed8b5b07'>Commit.</a> </li>
<li>Get rid of compile warnings. <a href='https://commits.kde.org/discover/166dcc2cab0f150dfe7adadb69c2861511087ebc'>Commit.</a> </li>
<li>Filter out runtimes as we care about apps only. <a href='https://commits.kde.org/discover/50823c45071f7493a15a3d86f24ac74e11b946a5'>Commit.</a> </li>
<li>Add option to install/uninstall applications. <a href='https://commits.kde.org/discover/c80f932172e4632af8d32d6f783a75fc39ffbeb8'>Commit.</a> </li>
<li>Expose the adoption command as a launcher. <a href='https://commits.kde.org/discover/7b3b98aeb7cb752c10d1e10ba281629c71e03d83'>Commit.</a> </li>
<li>Move executable processing to AbstractResource. <a href='https://commits.kde.org/discover/4648f06cd2e2dc870facaea925064b825a259d57'>Commit.</a> </li>
<li>Add information about size for not installed apps and runtimes. <a href='https://commits.kde.org/discover/08f12f2980198cfa825bdf757779f1e74ce2c55c'>Commit.</a> </li>
<li>Initial flatpak support. <a href='https://commits.kde.org/discover/4fcda80fc4738fb6cfe458577f3e4878a9187164'>Commit.</a> </li>
<li>Keep the consvervative trigger. <a href='https://commits.kde.org/discover/518cf8ab22bd6cfb09e00668a8c24336dc301367'>Commit.</a> </li>
<li>Use the right data types. <a href='https://commits.kde.org/discover/30d414540dadb28400bb3d5aba20c2aceeeb1b26'>Commit.</a> </li>
<li>Restore discover-exporter. <a href='https://commits.kde.org/discover/0b5a34f1216a9f2f0b90fdb38a50b781538eafe7'>Commit.</a> </li>
<li>Gain control over the applications shown on the main browsing page. <a href='https://commits.kde.org/discover/9c0dd3d491c499292f38cb2afa40833722641db6'>Commit.</a> </li>
</ul>


<h3><a name='kde-cli-tools' href='https://commits.kde.org/kde-cli-tools'>kde-cli-tools</a> </h3>
<ul id='ulkde-cli-tools' style='display: block'>
<li>Rename the translation catalog (avoid conflicts). <a href='https://commits.kde.org/kde-cli-tools/c4b4a040fe55e9f2962cd82f547dbd6b5d4f160f'>Commit.</a> </li>
<li>Fix spelling: MIME type. <a href='https://commits.kde.org/kde-cli-tools/6e8762ba30697dacd30800bcc7bb16a2c614909b'>Commit.</a> </li>
</ul>


<h3><a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Replace warps-slider checkbox with radio buttons. <a href='https://commits.kde.org/kde-gtk-config/57403a78afa20d4bf910dbd6fc52c2e8d0741a13'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4563'>D4563</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[Color Picker] Try to parse color from text drop. <a href='https://commits.kde.org/kdeplasma-addons/dd7f0d965868b8b1cdb66ee818572cf58c26ec93'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5246'>D5246</a></li>
<li>Avoid showing wrong timezone suffix in krunner time. <a href='https://commits.kde.org/kdeplasma-addons/ffc4cb0ee89ef5099475b8565a1f3ed9a320b78d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5770'>D5770</a></li>
<li>Bump frameworks version. <a href='https://commits.kde.org/kdeplasma-addons/8220b6b43c880b773f803b12e84edb8682e27c5b'>Commit.</a> </li>
<li>Revert "Bump frameworks version". <a href='https://commits.kde.org/kdeplasma-addons/2c21566486b0503803fe3997e5af2da70360f776'>Commit.</a> </li>
<li>Bump frameworks version. <a href='https://commits.kde.org/kdeplasma-addons/36b5ad587a9468851cbdd4497536cefb7c6780e0'>Commit.</a> </li>
<li>Show applet context menu when right clicking on Applet tab in gropuing applet. <a href='https://commits.kde.org/kdeplasma-addons/199f5d99aef2490779cde5fc5bc4f75eeea75d6c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5641'>D5641</a></li>
<li>Port grouping applet to Plasma Components 3. <a href='https://commits.kde.org/kdeplasma-addons/619238ab442410e9c8ceecf35d989ec8b28d4065'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5608'>D5608</a></li>
<li>Binary clock QML applet. <a href='https://commits.kde.org/kdeplasma-addons/990d291b8daf89f887871d5debde95a5673251f3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4503'>D4503</a></li>
<li>[Show Desktop Plasmoid] Honor panel icon size hint. <a href='https://commits.kde.org/kdeplasma-addons/cc0621a284a558e703570102424e91e4a6773153'>Commit.</a> </li>
<li>[Minimize All Plasmoid] Honor panel icon size hint. <a href='https://commits.kde.org/kdeplasma-addons/585c31ca0f64495c51e60e30b6c8b67eb1ff28a2'>Commit.</a> </li>
<li>[Color Picker] Give a title to ColorDialog. <a href='https://commits.kde.org/kdeplasma-addons/11f6a38478d8b8aec8b8fd8147aa2c88d59c4bb6'>Commit.</a> </li>
<li>Cmake: remove unused FindQJSON.cmake. <a href='https://commits.kde.org/kdeplasma-addons/73a2afe90b5a890a39c02ff66348f7bf6a2fa677'>Commit.</a> </li>
<li>Fix crash in JSON traversal in Bing potd provider. <a href='https://commits.kde.org/kdeplasma-addons/bd07284ec84af57cefd0c91339656f0bda79e466'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378590'>#378590</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5478'>D5478</a></li>
<li>Remove redundant cmake code. <a href='https://commits.kde.org/kdeplasma-addons/061d2c20a2b8f705847b9aabc35c08f4dc202a9b'>Commit.</a> </li>
<li>Port Kate Sessions Runner. <a href='https://commits.kde.org/kdeplasma-addons/412f19df9cbb80b2f2b10cea8948bbe6885c45b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377284'>#377284</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5077'>D5077</a></li>
<li>Potd: add a new wallpaper source from Bing. <a href='https://commits.kde.org/kdeplasma-addons/8068537eeab0332b421dc1c2afd8183ac6698616'>Commit.</a> </li>
<li>Fix QuickShare ShowUrlDialog getting clipped in hiDPI settings. <a href='https://commits.kde.org/kdeplasma-addons/6cfee0e84d0e0c1d7761a6f0cf4b41d988984790'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4944'>D4944</a></li>
<li>Use the hightlight from Plasma. <a href='https://commits.kde.org/kdeplasma-addons/76f1b7432a32b9d9e09afe84b0a4973d1a3a2e58'>Commit.</a> </li>
<li>Basic keyboard support in QuickShare Plasmoid. <a href='https://commits.kde.org/kdeplasma-addons/8bab9c84ab9a9e577dbf40c4e80949b88f58f44c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4805'>D4805</a></li>
<li>Quicklaunch: Don't use plasma theme for icons. <a href='https://commits.kde.org/kdeplasma-addons/d91b43268939bd9413b2c5b18888d317b3787301'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4784'>D4784</a></li>
<li>Quicklaunch: Open context menu on mouse press. <a href='https://commits.kde.org/kdeplasma-addons/b0436c547873f48f22f447507b75e6f589df3725'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4782'>D4782</a></li>
<li>Add a cache monitor to the System Load Viewer applet. <a href='https://commits.kde.org/kdeplasma-addons/9b2d766b2bb41a6725b585eaa8ca11fc9cf36b36'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129917'>#129917</a></li>
<li>Removed lancelot from kdeplasma-addons :(. <a href='https://commits.kde.org/kdeplasma-addons/5a793c8ca9ba70cf26d572a17cd353491780901a'>Commit.</a> </li>
<li>[Show Desktop Applet] Trigger when dragging something onto it. <a href='https://commits.kde.org/kdeplasma-addons/d0f3fe57ac0da9660dc393aa9f0acfff024a1385'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376212'>#376212</a></li>
<li>Fix icon changing on hover. <a href='https://commits.kde.org/kdeplasma-addons/4cf5a56406ca189ea1b2e69b0615e54bd02eaf99'>Commit.</a> </li>
<li>Activate minimise all plasmoid on mouse enter if the user is dragging mimedata. <a href='https://commits.kde.org/kdeplasma-addons/5fb9a2585d9fdb0b65954970815dd4edd83a1185'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376212'>#376212</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4521'>D4521</a></li>
<li>Proper category for the comic applet. <a href='https://commits.kde.org/kdeplasma-addons/3df5b807041cbbf698fd9a2d008364ac62f2b0bd'>Commit.</a> </li>
<li>[Color Picker] Close popup when picking a color. <a href='https://commits.kde.org/kdeplasma-addons/b3fb3f4b71864771c28047e3d83bc464cd66ae0d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4481'>D4481</a></li>
<li>[Color Picker] Pick color only on left click. <a href='https://commits.kde.org/kdeplasma-addons/55b6011d3b02583424a25fe69a139abf138b70e2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4479'>D4479</a></li>
<li>[Color Picker] Add contrast frame around colors in popup. <a href='https://commits.kde.org/kdeplasma-addons/75fd9dda750c6bc192e56ed16d34a0920bdf41df'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4298'>D4298</a></li>
<li>[Color Picker] Add border around color if contrast to surrounding view is too little. <a href='https://commits.kde.org/kdeplasma-addons/ef3fd95a32572c20b8e8e407b45726be4a95bcfd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4224'>D4224</a></li>
</ul>


<h3><a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a> </h3>
<ul id='ulkhotkeys' style='display: block'>
<li>Update khotkeys kcm docbook. <a href='https://commits.kde.org/khotkeys/d5b63b4132e0295570ae92fb3ffe6de114f9c2da'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4246'>D4246</a></li>
<li>Fix build with -fno-operator-names. <a href='https://commits.kde.org/khotkeys/f8f7eaaf41e2b95ebfa4b2e35c6ee252524a471b'>Commit.</a> </li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Add output scale to Wayland overview. <a href='https://commits.kde.org/kinfocenter/f8296597119af27fa5cde673073e91adce457848'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5683'>D5683</a></li>
<li>Port away from KDELibs4Support. <a href='https://commits.kde.org/kinfocenter/fab046273c6afb0fd873fbdf8248dbf7fff87eec'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/ferential Revision: D5169'>ferential Revision: D5169</a></li>
<li>Port tooltips to KToolTipWidget. <a href='https://commits.kde.org/kinfocenter/1008fcd219f11c3d3ddf8844a0d7c7d5f0e26ca9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4566'>D4566</a></li>
</ul>


<h3><a name='kmenuedit' href='https://commits.kde.org/kmenuedit'>KMenuEdit</a> </h3>
<ul id='ulkmenuedit' style='display: block'>
<li>Add .arcconfig. <a href='https://commits.kde.org/kmenuedit/0c7e783650d8f3b6a31c07517fe6a6d359432bfe'>Commit.</a> </li>
<li>Silence CMake policy CMP0063 warning. <a href='https://commits.kde.org/kmenuedit/93fee425624829e4e9eb5deb2fc4d29450939847'>Commit.</a> </li>
</ul>


<h3><a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Include <QRect> to use it. <a href='https://commits.kde.org/kscreen/ac8e6a7a630b80ec6e73bfc8fced18e562d23c52'>Commit.</a> </li>
<li>Get output size with output-geometry() rather than mode size directly. <a href='https://commits.kde.org/kscreen/1ac226792ae26e78b01b5790fecd2148188c35d3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5659'>D5659</a></li>
<li>Fix KScreen KCM screen placement with scaled outputs. <a href='https://commits.kde.org/kscreen/a76dcadb03813a19792add450faadfc10f6c1c97'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5636'>D5636</a></li>
<li>Include output scale in kscreen-console. <a href='https://commits.kde.org/kscreen/4936193ccbe5dc5952ade7bd39e8e59ccef49ab2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5634'>D5634</a></li>
<li>Remove quoting from kscreen-console json-output. <a href='https://commits.kde.org/kscreen/ab1f2c9a3bc491118b51c22b8cfc9a19f1da0116'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354534'>#354534</a>. Phabricator Code review <a href='https://phabricator.kde.org/D3552'>D3552</a></li>
<li>Fix rotation of outputs in kscreen kcm. <a href='https://commits.kde.org/kscreen/643871b2c4a9792265631650d3519f7b6a55b9da'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/130038'>#130038</a></li>
</ul>


<h3><a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>
<li>Lock Screen look and feel can now access the wallpaper item through the "wallpaper" context property. <a href='https://commits.kde.org/kscreenlocker/2e3790b998657e94652e0a64addec77def3c3ef4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5602'>D5602</a></li>
<li>Prevent kcheckpass from becoming an orphan. <a href='https://commits.kde.org/kscreenlocker/c5e49e466e48aef26c03d6c3784aabf94d52118d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5547'>D5547</a></li>
<li>Fix removal of lock window on unmap. <a href='https://commits.kde.org/kscreenlocker/cc3265cf01ccbfbb0ad14570e6f1cf2ffb9b9457'>Commit.</a> </li>
<li>Terminate kscreenlocker_greet and don't kill it on unlockRequest. <a href='https://commits.kde.org/kscreenlocker/88b502d325d263509e412b2a86df1c511ee62244'>Commit.</a> </li>
<li>[autotests] add missing add_test. <a href='https://commits.kde.org/kscreenlocker/d3e3c2ebd4326c0c8ee7ecfa5222a70640a3dc8c'>Commit.</a> </li>
<li>Use seccomp for implementing a sandbox for kscreenlocker_greet. <a href='https://commits.kde.org/kscreenlocker/5e3c7b337c9525f6b427c06f1814938b0a161db8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5029'>D5029</a></li>
<li>Support for long running kcheckpass supporting multiple authentications. <a href='https://commits.kde.org/kscreenlocker/07d38ba65adc2707a4914d11c0a5307e63a16aca'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4997'>D4997</a></li>
<li>[greeter] Send the auth result through the server instead return value. <a href='https://commits.kde.org/kscreenlocker/d666fe879d4682b1c9a0f9f2a7a29e6644adbf10'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4806'>D4806</a></li>
<li>Add support for emergency message show on Wayland. <a href='https://commits.kde.org/kscreenlocker/565259358ec0b742174289b714adc300c1479cf6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5157'>D5157</a></li>
<li>Require PAM by default and provide an option to not require it. <a href='https://commits.kde.org/kscreenlocker/6ef78dc09dad112f32a95029845407305ce77b90'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4768'>D4768</a></li>
<li>[ksld] Don't unset greeter connection on destroy unconditionally. <a href='https://commits.kde.org/kscreenlocker/06917c1253eb644e139c28427f4fc4b8f3e0a41b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377152'>#377152</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5008'>D5008</a></li>
<li>[kcheckpass] Drop all outdated/obsoleted checkpass variants. <a href='https://commits.kde.org/kscreenlocker/4cf34fea04dceb1ebd7a7a41cf614076e08b4bec'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5005'>D5005</a></li>
<li>[kcheckpass] Drop the conv_legacy support. <a href='https://commits.kde.org/kscreenlocker/a9c44a10b6a60777bf8ca997d3618e66bbd44baa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4780'>D4780</a></li>
<li>[kcheckpass] Drop the caller command line option. <a href='https://commits.kde.org/kscreenlocker/cbc60df2ed07b13476e57b6a30d38b93fa13a61e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4773'>D4773</a></li>
<li>[kcheckpass] Drop command line option for username. <a href='https://commits.kde.org/kscreenlocker/f1125820d9bb372ff568489f960a0b1c691b57be'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4759'>D4759</a></li>
<li>Enable qtvirtualkeyboard QT_IM_MODULE for greeter. <a href='https://commits.kde.org/kscreenlocker/c23f532224b7eb92a1d61e465f2990d77cf88313'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4871'>D4871</a></li>
<li>Add a small test application for kcheckpass. <a href='https://commits.kde.org/kscreenlocker/711565e4195a28d4283faf2baf2846b04e1257b3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4785'>D4785</a></li>
<li>[kcheckpass] Drop the ACCEPT_ENV compatibility define. <a href='https://commits.kde.org/kscreenlocker/3f999c5b86cd081a3b0744534fd894387316896e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4754'>D4754</a></li>
<li>[kcheckpass] Drop the throttle handling code. <a href='https://commits.kde.org/kscreenlocker/bff1b22b375ca1cf4b469b1d244b7e0a1a9accf3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4753'>D4753</a></li>
<li>[Greeter] Also clear selection clipboard. <a href='https://commits.kde.org/kscreenlocker/b69465e5b115717f4dbe7fb1d0a496926816a690'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4701'>D4701</a></li>
<li>[greeter] Clear clipboard on show. <a href='https://commits.kde.org/kscreenlocker/cae1e82fc94f23f735ae3c189030505a95810cb5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376526'>#376526</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4650'>D4650</a></li>
<li>Stop linking X11_Xcursor_LIB in KScreenLocker. <a href='https://commits.kde.org/kscreenlocker/dd9a089cfaefb24f012a137618e00005a48b6009'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376068'>#376068</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4505'>D4505</a></li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Update ksysguard docbook. <a href='https://commits.kde.org/ksysguard/c54d4e4233d2195c1a753f98754dcc68c5cccefe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4378'>D4378</a></li>
</ul>


<h3><a name='kwayland-integration' href='https://commits.kde.org/kwayland-integration'>KWayland-integration</a> </h3>
<ul id='ulkwayland-integration' style='display: block'>
<li>Cleanup wayland resources used in kwindowsystem before they're deleted by QPA. <a href='https://commits.kde.org/kwayland-integration/34fc30c4f7ca6d461ebfa0b7691e05edf2b0e40e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374665'>#374665</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5723'>D5723</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>List both setup and monitor UUIDs in debug. <a href='https://commits.kde.org/kwin/e63a25515a5c1c1666d3967001eacf8640a39cc6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5795'>D5795</a></li>
<li>M_clientSize is already connected in ShellClient::init. <a href='https://commits.kde.org/kwin/3817eddfc6663014e4a4f3229a15fb70530217da'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5787'>D5787</a></li>
<li>[DRM plugin] Correct Atomic Mode Setting. <a href='https://commits.kde.org/kwin/d15cb52682ca251b9d69f43e9b26d797e4dc3310'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5191'>D5191</a></li>
<li>[DRM plugin] Reorganize DrmBuffer. <a href='https://commits.kde.org/kwin/efedddd905c3e4998d164f7702f7cdca4f7fd160'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5179'>D5179</a></li>
<li>[DRM plugin] Remember static kernel objects, amplify use of DrmCrtc. <a href='https://commits.kde.org/kwin/a0571ccf847983fcc5af19f8f921248bdb69f0a6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5118'>D5118</a></li>
<li>Fix showing of window on inactive activities during session save. <a href='https://commits.kde.org/kwin/750843061cf2923f7e75422293186a1bdeda4977'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5614'>D5614</a></li>
<li>Revert the hackish overriding of Client::desktop() for session saving. <a href='https://commits.kde.org/kwin/f4de9618f87d0463859bfb809fd565c24327431d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5613'>D5613</a></li>
<li>Revert "Set pid on the PlasmaWindow based on the client connection.". <a href='https://commits.kde.org/kwin/a2d19c6b9deb1099bd47e09949d4f9adff2c3c42'>Commit.</a> </li>
<li>Revert "Fix typo in API comment.". <a href='https://commits.kde.org/kwin/802b0de36a039b587a02d0445f05db322f484ab0'>Commit.</a> </li>
<li>Fix typo in API comment. <a href='https://commits.kde.org/kwin/ac02f778a2687876444924564addc8cad7e120a7'>Commit.</a> </li>
<li>Set pid on the PlasmaWindow based on the client connection. <a href='https://commits.kde.org/kwin/262fb00b513fc750399d9a9118c1468a57c0fd97'>Commit.</a> </li>
<li>Update min KF5 version. <a href='https://commits.kde.org/kwin/55933d9cc9b19e47bb6e5cfbbea644eb98d5acfc'>Commit.</a> </li>
<li>Keep all touchpad QActions in the main thread. <a href='https://commits.kde.org/kwin/fe231be5e2d4811dc9132aecb931cf759d16ebf8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5733'>D5733</a></li>
<li>Fix regression for timestamp handling for Xwayland windows. <a href='https://commits.kde.org/kwin/32939fa7b5225fc5883b9888cbc6e6ddd8d6aee9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5731'>D5731</a></li>
<li>[helper] Terminate xclipboardsyncer if kwin_wayland goes down. <a href='https://commits.kde.org/kwin/572f730e8ecd39767390d9a1568a8cb50625965b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371862'>#371862</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5589'>D5589</a></li>
<li>Improve the x11 timestamp handling. <a href='https://commits.kde.org/kwin/0bec9ad7337536e319c17c5684d97e1156399fdb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377901'>#377901</a>. Fixes bug <a href='https://bugs.kde.org/348569'>#348569</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5704'>D5704</a></li>
<li>[autotests] Fix touch screen edge test after setting a default. <a href='https://commits.kde.org/kwin/b221d529a297ad7220d6fed2c9af7a2c8e7749cd'>Commit.</a> </li>
<li>Make WindowSwitching (Alt+Tab) the default left touch screen edge. <a href='https://commits.kde.org/kwin/c453eb696cc97208f8ebf2f632995b891bb425c3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5551'>D5551</a></li>
<li>[platformx/x11] Fix touch events for internal Qt windows. <a href='https://commits.kde.org/kwin/014afe1c0559b8d38b8c3df20df8ef48fa5a1192'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5549'>D5549</a></li>
<li>[autotests/integration] Fix SceneQPainter::testWindowScaled. <a href='https://commits.kde.org/kwin/207eb167e1f4107db06c44dc6b633b94e359c156'>Commit.</a> </li>
<li>Set correct KF5_MIN_VERSION. <a href='https://commits.kde.org/kwin/62a3b193bc6ecc024ae7815d5beaf6cbb72559ae'>Commit.</a> </li>
<li>Implement high DPI support in KWin QPA. <a href='https://commits.kde.org/kwin/8ef184e1cd1b2f479b88015e22cb365c1d517887'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5172'>D5172</a></li>
<li>Add scaling support into BlurEffect::doBlur. <a href='https://commits.kde.org/kwin/44a6050e839dc6311132010056ec5218680aea50'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4963'>D4963</a></li>
<li>Restore the backend set viewport when popping the final render target. <a href='https://commits.kde.org/kwin/9cb666f469ce41747ae5b433ad998ad0c57c7687'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4952'>D4952</a></li>
<li>Take into account scaling in blitFromFrameBuffer. <a href='https://commits.kde.org/kwin/71600d0a027d4588328a71db4059842a594d4460'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4951'>D4951</a></li>
<li>Scaling support in ColorPicker effect. <a href='https://commits.kde.org/kwin/24c9194f4c95d61a0f9df6de673d79f28aa6aee6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4950'>D4950</a></li>
<li>Support scaling in BackgroundContrast effect. <a href='https://commits.kde.org/kwin/f445a99a746bf0d5a14924b0ab34c92ec8a6942d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4949'>D4949</a></li>
<li>Store GLRenderTarget::scale. <a href='https://commits.kde.org/kwin/9ce5832e11bcecd125c2ef2cc200a586bdacb12e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4948'>D4948</a></li>
<li>Implment DRM EGL scaling. <a href='https://commits.kde.org/kwin/86b7189b8f2bde65d1439709ed9149a429bbe1cb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3504'>D3504</a></li>
<li>[KWin Scripts KCM] Restore "import" option. <a href='https://commits.kde.org/kwin/13045bb5d74e2297b07525df7c7cda6604f3dbd6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5318'>D5318</a></li>
<li>Animate quick tiling outline. <a href='https://commits.kde.org/kwin/eaf41cb6c5167819067af7d9a1d99b3c61e597bf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5262'>D5262</a></li>
<li>Add a KSharedConfigPtr for kcminputrc to KWin::Application. <a href='https://commits.kde.org/kwin/857d8a9c37c6a177dc546944349ad8d28c52d874'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5540'>D5540</a></li>
<li>Handle modifier updates in the same sequence as Wayland does. <a href='https://commits.kde.org/kwin/a039c2760f6458059dfc91b035a5e03022a31d12'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377155'>#377155</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5452'>D5452</a></li>
<li>Use end() instead of constEnd() for where we compare to an iterator. <a href='https://commits.kde.org/kwin/f3b20b482eb60e3bf8844830c229ad8b5669580f'>Commit.</a> </li>
<li>Don't map screenedge approach window if edge is only used for touch. <a href='https://commits.kde.org/kwin/eec6afe6f5fef80f519c7afb33e4fd568d74aab0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378951'>#378951</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5528'>D5528</a></li>
<li>[plugins/qpa] Do not replace a valid FBO with an invalid one. <a href='https://commits.kde.org/kwin/ac08c5ac9d4532b2506c76e8b69249b32d365aa3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5438'>D5438</a></li>
<li>Support blocking of screen edges on active fullscreen windows also for touch. <a href='https://commits.kde.org/kwin/e9a44a275e747c24afab04b22b1a68f75bc8a75f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5403'>D5403</a></li>
<li>Add support for keyboard layout switching policy "winclass". <a href='https://commits.kde.org/kwin/b132fe7c245c3b107c2e1b2ae1af7e303f96f9fe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5365'>D5365</a></li>
<li>Add support for keyboard layout switching policy "window". <a href='https://commits.kde.org/kwin/c8274dbe5798fa143b5025874de8c899477d8eca'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5315'>D5315</a></li>
<li>Introduce support for keyboard layout switching policies. <a href='https://commits.kde.org/kwin/bf99d9ffdd91bc7a99dcace2672af839fa53e5fb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5301'>D5301</a></li>
<li>[platforms/x11] Do not grab touch events. <a href='https://commits.kde.org/kwin/a5735e19b99d21d843252dcb12a147d122760371'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378951'>#378951</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5520'>D5520</a></li>
<li>[effects/slideback] Consider windows which do have painting disabled as not usable. <a href='https://commits.kde.org/kwin/919b497f9020a4be49c66d7e07f8a15aced317f0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364483'>#364483</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5462'>D5462</a></li>
<li>Don't update the focused pointer Surface if a button is pressed. <a href='https://commits.kde.org/kwin/9a13743c4988fbf653e20df43e919e836766d3c8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372876'>#372876</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5461'>D5461</a></li>
<li>Do not handle ScreenEdges::check for Edges which don't activate for Pointer. <a href='https://commits.kde.org/kwin/c3ecf55bf8e01628c6a661dfaa81750a57051202'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5421'>D5421</a></li>
<li>Remove executable bit from a .cpp file. <a href='https://commits.kde.org/kwin/1427dbad9b0220293716b79ffc27250bafeba3cc'>Commit.</a> </li>
<li>[effects] Use arg="true" in the kcfg files. <a href='https://commits.kde.org/kwin/2132b1e0c8297435fdc4942e0d679e2ad812e1ba'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3571'>D3571</a></li>
<li>Fix uninitialized variable. <a href='https://commits.kde.org/kwin/af9f81700f83c2908f57ca4f4689cd78900129b3'>Commit.</a> </li>
<li>[tabbox] Expose noModifierGrab to QtQuick. <a href='https://commits.kde.org/kwin/259e373bfc93d713b875987de0e1559e8f2beb13'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5414'>D5414</a></li>
<li>Cancel popup if the user clicked window decoration of parent window. <a href='https://commits.kde.org/kwin/3259d5e11326f5f63845941ac245507a57db7831'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5388'>D5388</a></li>
<li>[kcmkwin] Fix saving of TouchScreen actions. <a href='https://commits.kde.org/kwin/5ad6452ddd89eff7fe5b1fe2327a568b609dc6d9'>Commit.</a> </li>
<li>Fix two more const / non-const iterator mismatches. <a href='https://commits.kde.org/kwin/90a33a4bc2896fa6919587f4f736e01ddd18447a'>Commit.</a> </li>
<li>Fix build: Compare non-const iterator with non-const iterator. <a href='https://commits.kde.org/kwin/0fa4c27c66baf4ce1025dd6e2b034bad25deeea6'>Commit.</a> </li>
<li>[kcmkwin] Add a new config module for touch screen gestures. <a href='https://commits.kde.org/kwin/1648844005d167ec396042751a26410d7438acd2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5282'>D5282</a></li>
<li>Add support for new touch screen callbacks to KWin scripts and scripted effects. <a href='https://commits.kde.org/kwin/2be16258224904aea2abab8630b3859cb7b370a1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5277'>D5277</a></li>
<li>Add support for new touch screen edge actions to declarative KWin scripts. <a href='https://commits.kde.org/kwin/63b11383079ff134a2cda6a87c14f7750633c1f3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5272'>D5272</a></li>
<li>Register touch screen edges in effects. <a href='https://commits.kde.org/kwin/c080dca8ec9a3a8e674a86817740d3912845b079'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5269'>D5269</a></li>
<li>Support new touch screen edge swipe callbacks in TabBox. <a href='https://commits.kde.org/kwin/e1a46976e63a9ab8d5478867bd37c9182321191b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5281'>D5281</a></li>
<li>Add callback functionality for touch screen swipe gestures. <a href='https://commits.kde.org/kwin/e6aabf5b9f7a653f2f624866ed2a53d60f754e82'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5263'>D5263</a></li>
<li>Introduce dedicated actions for touch screen swipe gestures. <a href='https://commits.kde.org/kwin/64ce6259a92e63e610db459e7eddf9aeb6b6a159'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5252'>D5252</a></li>
<li>Initial support for popup window handling. <a href='https://commits.kde.org/kwin/1193b0da771a5d1042bf2aed0a2727f89ddf488e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366609'>#366609</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5177'>D5177</a></li>
<li>Support the emergency show property on ksld's lock screen window. <a href='https://commits.kde.org/kwin/489e6954c5fff79620a93faa5db897fb7684679c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5158'>D5158</a></li>
<li>[KWin Killer] Send short caption to it. <a href='https://commits.kde.org/kwin/2cbada20a10ea464ec038c33a55738a6be7b63bc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5307'>D5307</a></li>
<li>Desaturate non-responsive windows. <a href='https://commits.kde.org/kwin/1eb950a9851441d298cf8c252aa19819001b52c8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5245'>D5245</a></li>
<li>Add support for interactive selection through touch events. <a href='https://commits.kde.org/kwin/67295336e57cf1af79f9d15963d896ac0280a7f7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5203'>D5203</a></li>
<li>Scale openGL sub surfaces. <a href='https://commits.kde.org/kwin/61bb907bb4bb8953c422f5311f9cd4d2f4e3967c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3503'>D3503</a></li>
<li>Provide scale offset to WindowVertex calculation. <a href='https://commits.kde.org/kwin/ebebc6ca82b16fb28723742dd2fdc1b4f7c499cf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3502'>D3502</a></li>
<li>Scale surface damage events. <a href='https://commits.kde.org/kwin/e2e2c86f374008fb29a10654ec1f47a6b93da421'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3501'>D3501</a></li>
<li>Scale xwindowed EGL output. <a href='https://commits.kde.org/kwin/bd1d88359f39e8fcf46e082668c44b4f579d05cf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3500'>D3500</a></li>
<li>Drm cursor scaling. <a href='https://commits.kde.org/kwin/add93b3e3655222788cef980f6b6971e6572bc24'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3499'>D3499</a></li>
<li>Add scaling to DRM backend. <a href='https://commits.kde.org/kwin/0dac5d6bfe8c6eab58f315fc0219646c3acc59d6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3498'>D3498</a></li>
<li>Add scaling to virtual backend. <a href='https://commits.kde.org/kwin/584dc1d2d4ae356094d63482e8243a40a282188e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3497'>D3497</a></li>
<li>Scale input on windowed backend so input co-ordinate system matches host. <a href='https://commits.kde.org/kwin/dec76a77e2fa34f543da72f3dfd6e4267c47d0a5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3496'>D3496</a></li>
<li>Scale QPainter x11 windowed backend. <a href='https://commits.kde.org/kwin/59515b1681a835e1ad5afe4438ab755021297f65'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3495'>D3495</a></li>
<li>Set m_client size in global compositor co-ordinates. <a href='https://commits.kde.org/kwin/5c0c0cdb912c5554784d05e2b418b12230339cde'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3494'>D3494</a></li>
<li>Set wayland output scale. <a href='https://commits.kde.org/kwin/fd58c68ba5a4d0d09ac679f0612ba1e5816e2e0a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3159'>D3159</a></li>
<li>Fix build with clang and GCC6. <a href='https://commits.kde.org/kwin/e7b27ab9555a6ef2e581c95ee50ad3399f0277ac'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5229'>D5229</a></li>
<li>[autotests] Adjust ShellClientTest to changes in 634dfc09e42. <a href='https://commits.kde.org/kwin/dce674f3ae6e2fd107bd8bcdc303bcb8cf317755'>Commit.</a> </li>
<li>[autotests] Add test case for showing auto-hiding panel through swipe gesture. <a href='https://commits.kde.org/kwin/4adb2b65e87f410bd844822c54a68aebcab863fd'>Commit.</a> </li>
<li>Fix occassional crash caused by needlessly delayed signals (bko#363224). <a href='https://commits.kde.org/kwin/634dfc09e42ad44672e8f235fabb3110010491ce'>Commit.</a> </li>
<li>Add support for screenedge touchscreen events through XInput 2.2. <a href='https://commits.kde.org/kwin/8672b93bdd14f603f3032e8a74db28a0dd0a28fd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5137'>D5137</a></li>
<li>Add support for activating screenedges through touch swipe gestures. <a href='https://commits.kde.org/kwin/aa6c8f81168e4f89c67b9e88065aee675e306d1a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370323'>#370323</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5106'>D5106</a></li>
<li>Add support for global touchpad swipe gestures. <a href='https://commits.kde.org/kwin/22c91df2ec6c7dbe4cbb8410cd0e00147246fa6e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5097'>D5097</a></li>
<li>Ensure PointerInputRedirection::processMotion finishes prior to warping. <a href='https://commits.kde.org/kwin/0bb587dcb9de0a3bc688019fd66b224e713b3501'>Commit.</a> See bug <a href='https://bugs.kde.org/374867'>#374867</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5182'>D5182</a></li>
<li>Support for high dpi in aurorae. <a href='https://commits.kde.org/kwin/bfbcd0197181657d5dfac953ad77dd3de2ee7e92'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375868'>#375868</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5114'>D5114</a></li>
<li>Fix compilation on old gcc. <a href='https://commits.kde.org/kwin/404f2675f8ca5496a5e528825078b1ce27669cd7'>Commit.</a> </li>
<li>Update Aurorae Shadow when we copy the buffer, not one frame after painting. <a href='https://commits.kde.org/kwin/ec4359926b69fb990a758f94cce5a9e544849fa7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4990'>D4990</a></li>
<li>Add support for pointer gestures to nested Wayland platform. <a href='https://commits.kde.org/kwin/fa4c60e33f1cd48cb2a8f43dd1683fd533929fcc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5059'>D5059</a></li>
<li>[DrmBackend] Straighten out EGL surface buffer release logic on page flip. <a href='https://commits.kde.org/kwin/a347d009d11180f5e5dae4ddf88323ff62bfaf97'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5060'>D5060</a></li>
<li>When the padding aroun the window changes, update. <a href='https://commits.kde.org/kwin/948ff63d65d2e645eeb023387600bee52ef4d684'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377670'>#377670</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5078'>D5078</a></li>
<li>Initialize a member. <a href='https://commits.kde.org/kwin/df861b17028a264480ce23f4d648402852674ad1'>Commit.</a> </li>
<li>Revert unrelated change. <a href='https://commits.kde.org/kwin/926f2136e8a8a3382d6ba71b132f64ad3741c065'>Commit.</a> </li>
<li>Remove offending test. <a href='https://commits.kde.org/kwin/a25a06b6faeeaeb60f7005a6fbebc2b19774ad35'>Commit.</a> </li>
<li>New logout effect. <a href='https://commits.kde.org/kwin/a7b7ce9ec07a9f2e3f037fe2b382fd0f56073ccb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5045'>D5045</a></li>
<li>Add packagestructure for KWin/Decoration. <a href='https://commits.kde.org/kwin/898bee614d5683e35baa22a8590ee1525170137e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4974'>D4974</a></li>
<li>Store contrast matrix per-window. <a href='https://commits.kde.org/kwin/063587db99f29cca78d50f7283719f5112e6c9f9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/339237'>#339237</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5048'>D5048</a></li>
<li>Support for auto-hidden windows to resize. <a href='https://commits.kde.org/kwin/c0b207a2b524b720e54cd583a225b0681ab6e2b2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4718'>D4718</a></li>
<li>Glide effect: skip invisible windows. <a href='https://commits.kde.org/kwin/5c700eefd5ab1c897a944662b37f0fa2d0919ca6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359831'>#359831</a>. Code review <a href='https://git.reviewboard.kde.org/r/127378'>#127378</a></li>
<li>Move the view at the correct index at startup. <a href='https://commits.kde.org/kwin/299d23230cf235ad1e129c411fdd02166c0a52f1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4703'>D4703</a></li>
<li>Track the actual pressed keys in ModifierOnlyShortcuts. <a href='https://commits.kde.org/kwin/4a976d58ec50d6acd22e5cdbff0ca7fbda1367d0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4617'>D4617</a></li>
<li>Split KWin::Xkb into a dedicated .h and .cpp. <a href='https://commits.kde.org/kwin/58f26b8f5563984e3d8945d63f190a6a5d303e53'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4623'>D4623</a></li>
<li>Split modifier only handling into a dedicated InputEventSpy. <a href='https://commits.kde.org/kwin/65ddd32d1a783281610041d24daff7ee92011ded'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4578'>D4578</a></li>
<li>Only export the org.kde.keyboard DBus service if there are at least two layouts. <a href='https://commits.kde.org/kwin/40de5fa75fdd7e78b1ebc2d0a2d362af2b7e05b7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4562'>D4562</a></li>
<li>[autotests] Add test case for per-keyboard-layout global shortcut. <a href='https://commits.kde.org/kwin/df76d83087b9726771d6397c4efa8d3715628e59'>Commit.</a> </li>
<li>Require C++11 through cmake. <a href='https://commits.kde.org/kwin/1a9ece7cd4577fe7b7c7c6793c57960ed229520a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4486'>D4486</a></li>
<li>Require Qt 5.7 and remove pre-5.7 code. <a href='https://commits.kde.org/kwin/16647c3a3c3eda375af3ea934e34736f176b8930'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4485'>D4485</a></li>
<li>Ensure the layoutChanged DBus signal gets emitted when changing layouts through DBus. <a href='https://commits.kde.org/kwin/b16bd4147a6d53adadb56d1e2fe46ba306403b3f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4387'>D4387</a></li>
<li>[autotests] Add test case for changing keyboard layout through DBus. <a href='https://commits.kde.org/kwin/d33c96af5ec57d6617b5359d7f60944828756e96'>Commit.</a> </li>
<li>Implement the DBus org.kde.KeyboardLayouts interface of keyboard kded. <a href='https://commits.kde.org/kwin/551d4a8a5d79392c87cc031f3894cfb5dafc529b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4323'>D4323</a></li>
<li>Provide kxbk config through kwinApp. <a href='https://commits.kde.org/kwin/8d9c4acf4dee6e2fdae783e441c496960a827d11'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4315'>D4315</a></li>
<li>Split Keyboard Repeat handling into a dedicated InputEventSpy. <a href='https://commits.kde.org/kwin/eb92477210a76533ea27686a78a5427450089c9d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4304'>D4304</a></li>
<li>Disable standard actions on VirtualKeyboard SNI. <a href='https://commits.kde.org/kwin/b636feb8eb5784be21f9eccbfa0d34d569a54956'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4299'>D4299</a></li>
<li>Improve MoveResizeWindowTest::testMove expected fail cases. <a href='https://commits.kde.org/kwin/de444194ed755200129db4b5d05aabf02591502e'>Commit.</a> </li>
<li>Support global shortcut to switch to specific layout. <a href='https://commits.kde.org/kwin/6b0b4cf46871de5092e4d159cda9d0d907cacf49'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4256'>D4256</a></li>
<li>Add a basic SNI for keyboard layout. <a href='https://commits.kde.org/kwin/fe561c5c7def52320a045f938d0f0722fd7b473b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4220'>D4220</a></li>
<li>Drop restriction on when to emit the keyboard layout changed OSD. <a href='https://commits.kde.org/kwin/03efd678538ed41a7de91368bfac094f40e01049'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4244'>D4244</a></li>
<li>[autotests] Fix LockScreenTest::testKeyboardShortcut. <a href='https://commits.kde.org/kwin/f2f9aea11eb5038dc8b86498ecc942c2b40ad580'>Commit.</a> </li>
<li>Split implementation of keyboard layout handling into a dedicated class. <a href='https://commits.kde.org/kwin/d6c0a5414eb219cb3f61b4c436e65f5c96d95d9f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4135'>D4135</a></li>
<li>Use an InputEventSpy to notify about key and modifier state changes. <a href='https://commits.kde.org/kwin/437edb45ca84d0a2a374a51568010da6fca10c08'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4128'>D4128</a></li>
<li>Drop KWin's internal global shortcut handling. <a href='https://commits.kde.org/kwin/32f4e115e2c68b75a5eea86e0a582388d5abadb9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4165'>D4165</a></li>
<li>Move X11 specific KGlobalAccel handling into the x11-standalone platform. <a href='https://commits.kde.org/kwin/2904d4a0be4509235d1b9d91b342dbb611f0e8a4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4168'>D4168</a></li>
</ul>


<h3><a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Delete registry before connection. <a href='https://commits.kde.org/libkscreen/7e04d0de3beb64caf4674678210f8e0f571bd552'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5814'>D5814</a></li>
<li>Update unit test to match scaling. <a href='https://commits.kde.org/libkscreen/2bafd15756a12417d84f6041cbd759813738cc58'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5813'>D5813</a></li>
<li>Sync scale to/from KScreen configs. <a href='https://commits.kde.org/libkscreen/16a65d47e332d30560d04d88266420a411cd2b5d'>Commit.</a> </li>
<li>Sync kscreen outputs to wayland. <a href='https://commits.kde.org/libkscreen/a066f07dfd2b6129667d02516dceedfff3993c7c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5710'>D5710</a></li>
<li>Fix compile warning of treating boolean like an integer. <a href='https://commits.kde.org/libkscreen/cd4f61871bbf74882c3823d68b0a3fec47613f44'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5698'>D5698</a></li>
<li>Fix Output::geometry() when dealing with a scaled monitor. <a href='https://commits.kde.org/libkscreen/4a9f18f93948259142a29b24be6005987e955884'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5697'>D5697</a></li>
<li>Sync wayland output scale to KScreen config. <a href='https://commits.kde.org/libkscreen/4ef1c0447748b4b10459bf5efe2c07da88d78166'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5635'>D5635</a></li>
<li>Fix type argument for get property call. <a href='https://commits.kde.org/libkscreen/ca10c17fdeefbede32a942abe3dddcf6c4ad2671'>Commit.</a> </li>
</ul>


<h3><a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Make Plasma optional in exchange of disabling signalplotter. <a href='https://commits.kde.org/libksysguard/a0e69617442d720c76da5ebe3323e7a977929db4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129964'>#129964</a></li>
<li>Fix minor glitches. <a href='https://commits.kde.org/libksysguard/ef0bd80a3605fc5d08e37319cb2ff4c86726d3ad'>Commit.</a> </li>
<li>Fix strings in tooltips. <a href='https://commits.kde.org/libksysguard/6c5f127cb3825802ac504be94995f7bcd94da1f5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128429'>#128429</a></li>
</ul>


<h3><a name='milou' href='https://commits.kde.org/milou'>Milou</a> </h3>
<ul id='ulmilou' style='display: block'>
<li>Only follow mouse when moved (Fixes Bug #372635). <a href='https://commits.kde.org/milou/a41a850a3943dbc1bd43b867def775e41902f987'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372635'>#372635</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5490'>D5490</a></li>
<li>Add arcconfig file. <a href='https://commits.kde.org/milou/c7c17d15580eabb0998d208759a764f7b8a9ed62'>Commit.</a> </li>
</ul>


<h3><a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Add a widget style chooser to oxygen-demo. <a href='https://commits.kde.org/oxygen/3677f75e956e1946cd0ee775ff2969d0b2ecf1c7'>Commit.</a> </li>
<li>Removed unused variable. <a href='https://commits.kde.org/oxygen/48678616b7f689b9004cb0cdbfb48e4f84846518'>Commit.</a> </li>
<li>Fixed warning about unused variable. <a href='https://commits.kde.org/oxygen/7f6421f32eb4c85353b68329d64a0ddef4d13b61'>Commit.</a> </li>
<li>Adds a colour scheme chooser to oxygen-demo. <a href='https://commits.kde.org/oxygen/a6b22976db8eb1d5c65bf7e55f5057316c3af98b'>Commit.</a> </li>
<li>Fix header content size when sorting is disabled. <a href='https://commits.kde.org/oxygen/f6cd73e6d7b45aeeb0ef7bfec97d088ce424579b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4410'>D4410</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Update kf5 version. <a href='https://commits.kde.org/plasma-desktop/af113da8c5500df85b48f1f5f6966539116b5103'>Commit.</a> </li>
<li>Adding an option to show popular documents/apps for Kickoff. <a href='https://commits.kde.org/plasma-desktop/65a42d8a2be1145a9bf29c97a3ed471201419c96'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5620'>D5620</a></li>
<li>Also parse colorsettings.ui file when generating scheme editor. <a href='https://commits.kde.org/plasma-desktop/acddeebba7aa0251595d8c4540a7dc0215017b80'>Commit.</a> </li>
<li>Set correct KF5_MIN_VERSION. <a href='https://commits.kde.org/plasma-desktop/cdb3ef1008a2ececea6b2b9213b58a79397ca3cd'>Commit.</a> </li>
<li>Rename the translation catalog (avoid conflicts). <a href='https://commits.kde.org/plasma-desktop/d601eb794a052b2b61f4d05941aae00a7fe8771a'>Commit.</a> </li>
<li>Set up a cache for attica. <a href='https://commits.kde.org/plasma-desktop/3189f81aa6f38168f488ab2ffe810d717fcf6204'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379193'>#379193</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5616'>D5616</a></li>
<li>Restore the "current" color scheme concept. <a href='https://commits.kde.org/plasma-desktop/ec1f70b3563192abcb6a3ea834ce093819b28504'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5411'>D5411</a></li>
<li>Avoid infinite loop. <a href='https://commits.kde.org/plasma-desktop/5ad6e274b7790d7ac466313a9882c944cee74c91'>Commit.</a> </li>
<li>[Trash Plasmoid] Honor panel icon size hint. <a href='https://commits.kde.org/plasma-desktop/8595c2254075d7e5b75235c512d4e48ed6781731'>Commit.</a> </li>
<li>Fix syntax error. <a href='https://commits.kde.org/plasma-desktop/12da9215ce223812038476a5972be97c9c221754'>Commit.</a> </li>
<li>[Desktop Containment] Silence warning in ResizeHandle. <a href='https://commits.kde.org/plasma-desktop/64a72f8945a19fc3142e90807f90f726f563d134'>Commit.</a> </li>
<li>Switch from undefined to -1. <a href='https://commits.kde.org/plasma-desktop/bba7042f5d1176d194001c81f1df976efa5c72d8'>Commit.</a> </li>
<li>Raise default to 48. <a href='https://commits.kde.org/plasma-desktop/d58b296500f87280b247ad8742121a1241b1dded'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5596'>D5596</a></li>
<li>Configurable icon size. <a href='https://commits.kde.org/plasma-desktop/eae7910ed13724649f5ac0fc98680e11865e721a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378443'>#378443</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5595'>D5595</a></li>
<li>Configurable icon size. <a href='https://commits.kde.org/plasma-desktop/ad08c43ccaa28005750f4a04e2283cb78ecdbed6'>Commit.</a> See bug <a href='https://bugs.kde.org/378443'>#378443</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5592'>D5592</a></li>
<li>[Task Manager] Honor CanPlay/CanPause in context menu media controls. <a href='https://commits.kde.org/plasma-desktop/25a2a4b30ded80b6ecee8eaf83d62082b437da20'>Commit.</a> </li>
<li>[Task Manager] Swap Next and Stop player controls in context menu. <a href='https://commits.kde.org/plasma-desktop/49aa1aecbc6958f0461a8867f9a1bd9eeb513f43'>Commit.</a> </li>
<li>[AppletHandle] Resize button inside of applet handle must always be visible. <a href='https://commits.kde.org/plasma-desktop/a40b2dc0bdae869164ea99058a9dd299f9966922'>Commit.</a> </li>
<li>Add resize handles on all corners/sides. <a href='https://commits.kde.org/plasma-desktop/0a8d8b6d1a2974c968332adcfc1b6c042bd84d6e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5527'>D5527</a></li>
<li>[Applet Appearance] Allow moving applets with Alt+left-click. <a href='https://commits.kde.org/plasma-desktop/68e4be707e2402ddb623bc70043731a751039f6f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5558'>D5558</a></li>
<li>Keep startup notifications launcher-sized when inserting at launcher position. <a href='https://commits.kde.org/plasma-desktop/3739b4216c58ed8ad45f502d28d496b1392e4a27'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5505'>D5505</a></li>
<li>Fix new checkbox for file content indexing in kcm_baloofile. <a href='https://commits.kde.org/plasma-desktop/df9cee28e120167414a5ae79e4835c8b6533f807'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5499'>D5499</a></li>
<li>Adding an option to show popular documents/apps for Kicker and Dash. <a href='https://commits.kde.org/plasma-desktop/87f4c65282b97abc1c292f635df1960de57c7514'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5378'>D5378</a></li>
<li>Update baloo kcm docbook. <a href='https://commits.kde.org/plasma-desktop/12935c0ed304e7644c4062d65b26c5026064f0bd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5445'>D5445</a></li>
<li>Don't close popup when spring-load dragging into it. <a href='https://commits.kde.org/plasma-desktop/1aec30d954b71c7a5b614365e2f7b800e78117ee'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5310'>D5310</a></li>
<li>Hoist the actions overlay out of the frame loader. <a href='https://commits.kde.org/plasma-desktop/c5961c16a69d9c31d3c3c52aad5e2689c33d9621'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5309'>D5309</a></li>
<li>Kcm_baloofile: Add option to disable file content indexing. <a href='https://commits.kde.org/plasma-desktop/a818d2f0ad30bb14718f296690a95ef55ed5a9db'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5351'>D5351</a></li>
<li>Make sure the "default" sheme is actually default. <a href='https://commits.kde.org/plasma-desktop/373fd101c77fd7969f8b9e9683657219d8d13224'>Commit.</a> </li>
<li>Possible to edit the default color scheme. <a href='https://commits.kde.org/plasma-desktop/cca27a026af372ac339db12a66779c858b4b6a17'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5406'>D5406</a></li>
<li>Checkboxes should have labels for the larger click area. <a href='https://commits.kde.org/plasma-desktop/fb2f3718a782a4f913283ce115643e7298422f57'>Commit.</a> </li>
<li>[Folder View] Position sub folder popup left-of visual parent in RTL layout. <a href='https://commits.kde.org/plasma-desktop/830e4a0850fd49a8ce918d4f18c548488ed76dde'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5317'>D5317</a></li>
<li>Mark popupPosition as override. <a href='https://commits.kde.org/plasma-desktop/9dbb36fea5a692bcbd20ade9bdf8cdd65e8f5279'>Commit.</a> </li>
<li>Kimpanel: fix crash. first argument of qapp need to be reference. <a href='https://commits.kde.org/plasma-desktop/d97f34c3bbb6944ef4de876a2f185de3a4906004'>Commit.</a> </li>
<li>Kimpanel: try to workaround some qt/glib issue and fix memory leak. <a href='https://commits.kde.org/plasma-desktop/4046be3cb7bebe44b41c753e2697d6555ce596b1'>Commit.</a> </li>
<li>Kimpanel: be prepared for plasma shell with highdpi flag. <a href='https://commits.kde.org/plasma-desktop/a418979bf1073c1a0b2b825d31d6abf495cb242a'>Commit.</a> </li>
<li>Kimpanel: add a ibus panel launcher. <a href='https://commits.kde.org/plasma-desktop/6e03b6f5465a5df6135be1795b33f8fd50483ced'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378342'>#378342</a></li>
<li>[Folder View] Rename on clicking outside. <a href='https://commits.kde.org/plasma-desktop/45bea58f9bfeedcc06ec8ed086215287c2737155'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378447'>#378447</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5305'>D5305</a></li>
<li>Readd global menu option to kcmstyle documentation. <a href='https://commits.kde.org/plasma-desktop/0aea993091de02afb92a65a10cb6aa5a5b2ff607'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5273'>D5273</a></li>
<li>[Folder View] Port from deprecated ui() to uiDelegate(). <a href='https://commits.kde.org/plasma-desktop/7d49b8ed2c8f65290c24bc2a8554e01955f5acce'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5236'>D5236</a></li>
<li>Update Touchpad KCM README. <a href='https://commits.kde.org/plasma-desktop/5a7b6860ff75d6a82a902aa63d27a204669a29c4'>Commit.</a> </li>
<li>[Widget Explorer] Better align add widgets menu. <a href='https://commits.kde.org/plasma-desktop/7e84eb6786ebd5bbfee1bbf95779b23f7a0951be'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5216'>D5216</a></li>
<li>[Folder View] Use KDirLister's root KFileItem for LabelGenerator. <a href='https://commits.kde.org/plasma-desktop/89bcbb5fb637ac872dfbff0951f5730412debb5e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5223'>D5223</a></li>
<li>[Folder View] Don't break FolderView url binding to configured URL when going home. <a href='https://commits.kde.org/plasma-desktop/4fb4dbe80a84e2370edd3d83e8cb488f31cfd7b3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5224'>D5224</a></li>
<li>[Folder View] Show pin icon only if really in a popup. <a href='https://commits.kde.org/plasma-desktop/fc94d0f2fd3ebd3d087f4cc433d1e84a476b2177'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5204'>D5204</a></li>
<li>[Folder View] Add "home" button. <a href='https://commits.kde.org/plasma-desktop/2f607969ecd43e9f46941062b8f692a0ed32377c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5201'>D5201</a></li>
<li>Port custom image mode to IconItem. <a href='https://commits.kde.org/plasma-desktop/b80a8aa51456804e25385d6386df8afac1d762cb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377652'>#377652</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5162'>D5162</a></li>
<li>Use new property plasmoid.editMode. <a href='https://commits.kde.org/plasma-desktop/c8181c9fe8ebaf45a5fdbadc3ad6d76059d91f82'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5154'>D5154</a></li>
<li>Open applet handles when the toolbox is open. <a href='https://commits.kde.org/plasma-desktop/e635931e433cb31d1fe23e40c8af625a8f7c24f2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4969'>D4969</a></li>
<li>[Task Manager] Add option to toggle grouping on middle click. <a href='https://commits.kde.org/plasma-desktop/2fcd8547c3617dba583973fcf5cb6b9ae6877525'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/323567'>#323567</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5065'>D5065</a></li>
<li>[Kickoff / App Dash] Support "locations" runner. <a href='https://commits.kde.org/plasma-desktop/fb10c7fc2db3b93ebf29e4fa5c587f5ec9c42922'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376642'>#376642</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5033'>D5033</a></li>
<li>Adjust plasma-desktop to the new fallback prefixes. <a href='https://commits.kde.org/plasma-desktop/a521ac8f8882ae90a31654f8e3d7fdf1393181dd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4830'>D4830</a></li>
<li>Fix terminology: use Plasma for the desktop. <a href='https://commits.kde.org/plasma-desktop/b7d8811a95c56b8a09c93357947c64e484d5c399'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5053'>D5053</a></li>
<li>Kimpanel: clean up unused feature in ActionMenu. <a href='https://commits.kde.org/plasma-desktop/a577488ffafbe4ea5068dfc6ca91bb3848cb249a'>Commit.</a> </li>
<li>Partially revert ab89da7d1d0547dc98f423e16d1dd28d3d9830c7. <a href='https://commits.kde.org/plasma-desktop/c090c59513db15757182f97ad2f253d569fad788'>Commit.</a> </li>
<li>Fully switch to anchors. <a href='https://commits.kde.org/plasma-desktop/d12216cb7ce44dc5a814f045073a295185038dbc'>Commit.</a> </li>
<li>[Folder View] Visualize hidden files. <a href='https://commits.kde.org/plasma-desktop/3dd666365245bcca8787df77cf383cd9edaf7dc3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4980'>D4980</a></li>
<li>[Folder View] Cancel renaming if focus is lost or popup closes. <a href='https://commits.kde.org/plasma-desktop/43c91868dc03f03f32d159ac36597acfd8fc7345'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4897'>D4897</a></li>
<li>Kimpanel: add show/hide buttons functionality. <a href='https://commits.kde.org/plasma-desktop/ab89da7d1d0547dc98f423e16d1dd28d3d9830c7'>Commit.</a> </li>
<li>[Folder View] Open context menu when pressing Menu key. <a href='https://commits.kde.org/plasma-desktop/895f617f08876e6d02185514a00d9597c56bf455'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4962'>D4962</a></li>
<li>[Folder View] Lower toolbox when an item is hovered. <a href='https://commits.kde.org/plasma-desktop/d1e81740d37289ac62dce56ceac860f6fd302718'>Commit.</a> See bug <a href='https://bugs.kde.org/337060'>#337060</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4956'>D4956</a></li>
<li>[Folder View] Remove local copy of PlasmaQuick/Dialog. <a href='https://commits.kde.org/plasma-desktop/186f1386b6bec7a1680a95975779a327801c675a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4964'>D4964</a></li>
<li>[Folder View] Turn heading into a "hyperlink" and reduce hit area. <a href='https://commits.kde.org/plasma-desktop/138184afe09ac868683a792249ad54a493b43bab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4959'>D4959</a></li>
<li>[Folder View] Set plasmoid.busy to false when listing canceled. <a href='https://commits.kde.org/plasma-desktop/5a2e6620f75a3d994ce05866d701ecd56e2b47be'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4922'>D4922</a></li>
<li>[TaskManager] For context menu placement use accurate enum on right edge. <a href='https://commits.kde.org/plasma-desktop/67e490f48d2c29554fb0872aec4e1d00e7e9ec19'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4868'>D4868</a></li>
<li>[Folder View] Fix rename editor position in popup with icon mode. <a href='https://commits.kde.org/plasma-desktop/12bd5f33d502a1bb80b7e7b4d8e669356abb1fcc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4896'>D4896</a></li>
<li>[Folder View] Cancel rename when scrolling. <a href='https://commits.kde.org/plasma-desktop/3dc7b62129c5009a8973d0959eb15573174b8368'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4895'>D4895</a></li>
<li>[Folder View] Hide item label while renaming. <a href='https://commits.kde.org/plasma-desktop/b4d8de1ed9ec026730efcd204b13ea4e4a7fc2bd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4894'>D4894</a></li>
<li>[Folder View] Support undo keyboard shortcut. <a href='https://commits.kde.org/plasma-desktop/3491607e70beb01849d50dd2b6b09eda218b30df'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4887'>D4887</a></li>
<li>[Folder View] Offer "Empty Trash" also for trash link. <a href='https://commits.kde.org/plasma-desktop/30b875939abf3e465030680b5e97e80490dfb12d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4886'>D4886</a></li>
<li>Fix actions overlay position not adjusting when switching view modes at runtime. <a href='https://commits.kde.org/plasma-desktop/0accf128d4cd18162e663940bca65b1a47f57314'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4875'>D4875</a></li>
<li>Fix hover out when exiting visible frame. <a href='https://commits.kde.org/plasma-desktop/54309e677bbfd1ad350cfd17500925be52223f33'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4874'>D4874</a></li>
<li>Remove accidentally commited .directory file. <a href='https://commits.kde.org/plasma-desktop/e5378ca2efc90a128e6cc1e5cef8afa973d87d65'>Commit.</a> </li>
<li>Fix rename box width calc. <a href='https://commits.kde.org/plasma-desktop/6a0b3f8a3dc580046f61ae4e01c0553377c12cbf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4862'>D4862</a></li>
<li>Reduce the hitbox for regular icon hover. <a href='https://commits.kde.org/plasma-desktop/5995ea913aebb960fdf8cb72eedbf40ba4a34fa6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4860'>D4860</a></li>
<li>Tighter Folder View item delegates. <a href='https://commits.kde.org/plasma-desktop/8422f020cb1e130385f7e2ffdd73c77331d00b79'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4856'>D4856</a></li>
<li>Task Manager: Make the icon size configurable for task buttons in a vertical task manager. <a href='https://commits.kde.org/plasma-desktop/26b92df48a6a61acbe1a649167e9f03b159ab2a6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4550'>D4550</a></li>
<li>[Kickoff] Increase opacity for item description. <a href='https://commits.kde.org/plasma-desktop/14ed8ceb62376fd321b4b9eb245bb7ab0676a6bf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4817'>D4817</a></li>
<li>Fonts KCM: Fix layout of hinting settings, port away from KDE4. <a href='https://commits.kde.org/plasma-desktop/cc5720494a01022be80788b8b62029896de80324'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4740'>D4740</a></li>
<li>Fix potential crash in KCmFontInst on invalid index. <a href='https://commits.kde.org/plasma-desktop/755993315b6a362c5b782b1770c812f84c99b313'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4394'>D4394</a></li>
<li>Exit early here too. <a href='https://commits.kde.org/plasma-desktop/f10a6f4eba65e091a08aa47e6b87bb9748143cde'>Commit.</a> </li>
<li>Exit loop early. <a href='https://commits.kde.org/plasma-desktop/30388c7361889372cfdc9db77aeaccfd384c9e97'>Commit.</a> </li>
<li>Drop unnecessary 'Desktop'. <a href='https://commits.kde.org/plasma-desktop/8736650c88ee864fd83a3dc544834dc7d0a49908'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376730'>#376730</a></li>
<li>Touchpad KCM: simplify few UI strings. <a href='https://commits.kde.org/plasma-desktop/cc8c82ac1b43161ac670dfddc7b4afd8af3d0920'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4671'>D4671</a></li>
<li>Use folderview as default containment. <a href='https://commits.kde.org/plasma-desktop/151bfb728ce8eed923f8e32ae39e345441af2f7b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4678'>D4678</a></li>
<li>[WIP]: unify file drop menu and containment. <a href='https://commits.kde.org/plasma-desktop/239e2be33fa08a703473c6178c39b940f3bf1f36'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4574'>D4574</a></li>
<li>Fix crash when invoking Present Windows with the group dialog open. <a href='https://commits.kde.org/plasma-desktop/a261ff25a580759183d20725d9f37d9ba25365ee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376205'>#376205</a>. Fixes bug <a href='https://bugs.kde.org/376234'>#376234</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4525'>D4525</a></li>
<li>Pressing Enter should open submenus in Kicker. <a href='https://commits.kde.org/plasma-desktop/5e7cbdd445f6caf8fdf59ca7f098275cc160347d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4401'>D4401</a></li>
<li>[ConfigurationContainmentActions] Pass Info button as context for showAbout. <a href='https://commits.kde.org/plasma-desktop/690e6f2e21fa50e71cda81b31edd5eba58c36960'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4569'>D4569</a></li>
<li>[Task Manager] Add "Places" for entries belonging to a file manager. <a href='https://commits.kde.org/plasma-desktop/ad78eed5be08f219b26d0eaf4d0ff16f2f8d49eb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3815'>D3815</a></li>
<li>Remove the TextFix workaround for QML text mouse handling bug. <a href='https://commits.kde.org/plasma-desktop/b893dc9ad6a24834b20a995978adc50540df8dd5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4345'>D4345</a></li>
<li>Delay setting the model on the view until listing is finished. <a href='https://commits.kde.org/plasma-desktop/7fd62b59b77f151ac5f4e3a4dc7e7fbe1ff42851'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4344'>D4344</a></li>
<li>Greatly improve Folder View performance and mem usage. <a href='https://commits.kde.org/plasma-desktop/99c903059f591f2e9dd15b64e7342298935710e7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4343'>D4343</a></li>
<li>Add discover to the system application tab in kickoff. <a href='https://commits.kde.org/plasma-desktop/12193b07f12b7620a04248cc66b1269ded91ccd4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4531'>D4531</a></li>
<li>[Containment Configuration] Hide containment type drop-down if there's only one containment to choose from. <a href='https://commits.kde.org/plasma-desktop/f94e31e08ab28af3e5bacf2844bc85f3c9286dda'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4523'>D4523</a></li>
<li>[Icons KCM] Take into account device pixel ratio for icon preview. <a href='https://commits.kde.org/plasma-desktop/f7e84d0de6e9066c13b4cd1986d6eed535d88490'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4504'>D4504</a></li>
<li>Set buddy widgets in Font KCM. <a href='https://commits.kde.org/plasma-desktop/ddb2e43cd4c8d3acc9eeece73ea7281a988ef8fa'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126902'>#126902</a></li>
<li>Make it possible to change icon themes from elsewhere in kns. <a href='https://commits.kde.org/plasma-desktop/5df635d804648b039e64a6b372fb5292abc70fa4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4317'>D4317</a></li>
<li>Remove flickering on current item. <a href='https://commits.kde.org/plasma-desktop/11371c25a3037a9e3c975cca41672f0dfb2b9fad'>Commit.</a> </li>
<li>Build with -Werror=missing-include-dirs. <a href='https://commits.kde.org/plasma-desktop/7876865ad938f723e0593de05b83a0a8b300150a'>Commit.</a> </li>
<li>Patch for plasmoid subsystem(containments/desktop) in plasma-desktop. <a href='https://commits.kde.org/plasma-desktop/d7621da69b9b4758b3e877f78545645bcb1c4305'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4204'>D4204</a></li>
<li>Remove local copy of dialog.h. <a href='https://commits.kde.org/plasma-desktop/fdb041bfdad10dd910d53be344384030afabaf65'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4223'>D4223</a></li>
<li>Update componentchooser kcm docbook. <a href='https://commits.kde.org/plasma-desktop/a3ead50375be843b5575099f44a229703d1c9a15'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4109'>D4109</a></li>
<li>Fix some bugs (mem leak, layout, preview not showing colors). <a href='https://commits.kde.org/plasma-desktop/1d05792ad782ce4cd41e31c1ab6eef3026b5218b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372309'>#372309</a>. Fixes bug <a href='https://bugs.kde.org/372407'>#372407</a>. Code review <a href='https://git.reviewboard.kde.org/r/129902'>#129902</a></li>
<li>Clip by bounding delegate size instead of Item.clip. <a href='https://commits.kde.org/plasma-desktop/3b5d1fcb42b6a0a365f92bfc142b97bdbf609ac4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4302'>D4302</a></li>
<li>Fix autotest. <a href='https://commits.kde.org/plasma-desktop/5a938450bd742b395d24118be575531556958637'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4297'>D4297</a></li>
<li>[Task Manager] Focus last item when pressing up with no active item. <a href='https://commits.kde.org/plasma-desktop/9d66f0bc8ff8c2510445661e92abddcf4ff43e5e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4292'>D4292</a></li>
<li>Overhaul group popup dialog. <a href='https://commits.kde.org/plasma-desktop/f6b30cb08d4cf2b45f1e1315b1b1bd681a0073a8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375196'>#375196</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4229'>D4229</a></li>
<li>Support spring-loading on drag hover and drops into preview popups. <a href='https://commits.kde.org/plasma-desktop/383cf19d0a911bdd121decbab9a647fdc7eed104'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4103'>D4103</a></li>
<li>[KRDB] Fix wording in comment at the top of "gtkrc". <a href='https://commits.kde.org/plasma-desktop/85754b3ddcb1df479031d680fabc53363cb87d30'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4170'>D4170</a></li>
<li>Move accessibility kcm to new subgroup personalization. <a href='https://commits.kde.org/plasma-desktop/8925c105206049b8c198039c15d6466b1646ca32'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129036'>#129036</a></li>
<li>Fix loading of translation catalog (thanks to Victor <victorr2007@yandex.ru>). <a href='https://commits.kde.org/plasma-desktop/f5746ff9b77170b1945eae9d378c21baa123e48a'>Commit.</a> </li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a></h3>
<ul id='ulplasma-integration' style='display: block'><li>New in this release</li></ul>
<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Workaround to fix PasswordDialog's focus. <a href='https://commits.kde.org/plasma-nm/2c6b059626dedd1ae82fb06f0d6f8737dae8e5b5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/130109'>#130109</a>. Fixes bug <a href='https://bugs.kde.org/379279'>#379279</a></li>
<li>Remove option to set network type in gsm configuration. <a href='https://commits.kde.org/plasma-nm/f1e39c5458e6fde4e586a64f13ad9f354e219339'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379400'>#379400</a></li>
<li>Improve password dialogs. <a href='https://commits.kde.org/plasma-nm/e5574b458abb86ce658471abd10472f3598aecb1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5382'>D5382</a></li>
<li>Fix minor typo. <a href='https://commits.kde.org/plasma-nm/f5c0a67a570551914a032b4da4d18d737a7f7735'>Commit.</a> </li>
<li>OpenVPN: don't require setting to be non-empty for advanced configuration. <a href='https://commits.kde.org/plasma-nm/d56a0b7872ea80d40866c78a6cb944b76d4c4773'>Commit.</a> </li>
<li>OpenVPN: add new properties (sync with NM). <a href='https://commits.kde.org/plasma-nm/ea13c657dbc34ef0323bb7460a3d8e291d361a2f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378648'>#378648</a></li>
<li>Simplify connection item delegate. <a href='https://commits.kde.org/plasma-nm/53e9d3e514c52bed0835baff6d874a76f44df2de'>Commit.</a> </li>
<li>OpenVPN: Remove previously configured secrets to avoid passing them back. <a href='https://commits.kde.org/plasma-nm/11fba0b008732f28709daba4a9062ce81cc21f0d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375170'>#375170</a></li>
<li>KCM: Do not use Plasma components as the colors are not consistent. <a href='https://commits.kde.org/plasma-nm/e0e8f0608af1910885a7a011b082d533e1fd3c46'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374905'>#374905</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Preserve channel balance when changing volume. <a href='https://commits.kde.org/plasma-pa/462628981947e298c5785864ee709bb1bf24e784'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362832'>#362832</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5546'>D5546</a></li>
<li>Move all streams when changing default device. <a href='https://commits.kde.org/plasma-pa/cb52cc8826d47b3f4634d2d1e6f08d2ba750fdec'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5684'>D5684</a></li>
<li>Applet: Automatically raise maximum volume when over defined maximum volume. <a href='https://commits.kde.org/plasma-pa/d7868925134400dd7003d76075cef5406fd6542a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5695'>D5695</a></li>
<li>Context: Use DBus service watcher to reconnect to pulseaudio. <a href='https://commits.kde.org/plasma-pa/60dc3a5cce37ed72a48693032097df3cff90def9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358772'>#358772</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5171'>D5171</a></li>
<li>Update minimum required versions. <a href='https://commits.kde.org/plasma-pa/c3956564e5ae4bfa249aaff429885330e57cb829'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5686'>D5686</a></li>
<li>Applet: Reset height of item label. <a href='https://commits.kde.org/plasma-pa/d046b6b288b194c0abc8b4ca222557b9a830a59a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5343'>D5343</a></li>
<li>Applet: Add Raise maximum volume action to context menu. <a href='https://commits.kde.org/plasma-pa/090e9e337b79294a83c0f5dfec349c20adbf36f3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5086'>D5086</a></li>
<li>Indicate that a port is unavailable. <a href='https://commits.kde.org/plasma-pa/3e497a1518edeadc8ef7b93597b11b7f59a65400'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4836'>D4836</a></li>
<li>VolumeObject: Make channelVolumes readable from QML. <a href='https://commits.kde.org/plasma-pa/b84ed90c50cdd4778083a3ea503957ca4260ea84'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5665'>D5665</a></li>
<li>Don't use PlasmaComponents.Label in KCM. <a href='https://commits.kde.org/plasma-pa/99d77bd98d24678b004ac03ef9d22efdd9b9120a'>Commit.</a> </li>
<li>StreamRestore: Don't write unnecessary changes. <a href='https://commits.kde.org/plasma-pa/b364a9e6c60cd3681ee7f3680179251e2efd2939'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367688'>#367688</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5657'>D5657</a></li>
<li>KCM: Fix 100% volume label position with RTL. <a href='https://commits.kde.org/plasma-pa/86c4c2921104cd8a644e5ef92217f994b239840a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5548'>D5548</a></li>
<li>Add license header to SmallToolButton.qml. <a href='https://commits.kde.org/plasma-pa/d2323d77ec0e69ad55251f6a0e105388c97a30f5'>Commit.</a> </li>
<li>Change the volume icon/mute button into a ToolButton. <a href='https://commits.kde.org/plasma-pa/2ac365e96b70c7ba6880d26bb558c4ed730df4d4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5144'>D5144</a></li>
<li>Make the applet DBus activated. <a href='https://commits.kde.org/plasma-pa/50157cc0de87d391998938a3ab817836fa05d496'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5222'>D5222</a></li>
<li>Applet: Add spacing to context menu button + same size for volume indicator. <a href='https://commits.kde.org/plasma-pa/2afa5a416eab0fe6d46874cb9f259ed51358b86a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5024'>D5024</a></li>
<li>Fix context menu button's icon size when on default DPI. <a href='https://commits.kde.org/plasma-pa/4d4867b8999617ac6ebfc613a1716b4950084765'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4845'>D4845</a></li>
<li>Fix build. <a href='https://commits.kde.org/plasma-pa/06c5ae6ba280cf521c26b4ef5b3e2ae84e835834'>Commit.</a> </li>
<li>Drop building QPulseAudioPrivate library. <a href='https://commits.kde.org/plasma-pa/eaa85666039c291d63f09d9eedf68204a2b923fc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4903'>D4903</a></li>
<li>[Applet] Open contextMenu relatively to visual parent contextMenuButton. <a href='https://commits.kde.org/plasma-pa/d8b305292240277309fc4dadd84ff2a3cbf6a154'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4904'>D4904</a></li>
<li>PulseObject: Also use "device.icon_name" for icon. <a href='https://commits.kde.org/plasma-pa/1966f954cf0a3776482a577bee5bf780056072ec'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4843'>D4843</a></li>
<li>Applet: Make volume indicator icon bigger. <a href='https://commits.kde.org/plasma-pa/7b6c9418516407f7d5485b0a5d0b26b2a742d2ab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4837'>D4837</a></li>
<li>Make a string translatable (Patch by Victor <victorr2007@narod.ru>). <a href='https://commits.kde.org/plasma-pa/6eb85cd2d9c8887d16219672d57e840ba908173b'>Commit.</a> </li>
<li>[Applet] Context Menu per device/stream to offer additional functionality. <a href='https://commits.kde.org/plasma-pa/f063ff829928b16d9ae2d76f6c71c93f67354eee'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4751'>D4751</a></li>
<li>Add Module class + disable advanced options when module-gconf is not loaded. <a href='https://commits.kde.org/plasma-pa/817af4b68f6b199d91d8ff3d3900e7f2dbc95268'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4781'>D4781</a></li>
<li>Applet: Fix binding loop on ListItem height. <a href='https://commits.kde.org/plasma-pa/a82bccea2d33196730e218116a8f79c4f3d3c253'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4767'>D4767</a></li>
<li>KCM: Only scroll when mouse is over tabbar. <a href='https://commits.kde.org/plasma-pa/8b2acc7c6f428de1a465b80d7e93bdaec527bbac'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4770'>D4770</a></li>
<li>KCM: Switch tabs with mouse wheel. <a href='https://commits.kde.org/plasma-pa/87df4c77cb6969bb5ccab5b1d08d2c80f290877b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4764'>D4764</a></li>
<li>Fix some code and style issues. <a href='https://commits.kde.org/plasma-pa/b3668638c04847db43319464da7df951bb79cd56'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4756'>D4756</a></li>
<li>Fix binding loops from daeafed202. <a href='https://commits.kde.org/plasma-pa/3eda4c89cd125b585bd41d0a4dc6e26f8542a3aa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4755'>D4755</a></li>
<li>Port: Expose port availability to QML. <a href='https://commits.kde.org/plasma-pa/fc4ee177396e1d6f06fed3b6f7b4b16e11b3fe54'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4736'>D4736</a></li>
<li>[KCM] Rework design and structure. <a href='https://commits.kde.org/plasma-pa/daeafed202f5a68a3f56090f6ab62b6fad4d04af'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4732'>D4732</a></li>
<li>Applet: Show info message when no devices found / applications playing audio. <a href='https://commits.kde.org/plasma-pa/2322d0b4c669205579442e8335978e58503b71bd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4677'>D4677</a></li>
<li>Applet: Add configure button next to tabbar. <a href='https://commits.kde.org/plasma-pa/aeac3023a71953edc38f6ff015fcedc9f155c9a5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365282'>#365282</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4675'>D4675</a></li>
<li>Show percentage in tooltip maintext and indicate when muted. <a href='https://commits.kde.org/plasma-pa/dcfad7d1233a7accd6be6316e36d64826c86ce82'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4100'>D4100</a></li>
</ul>


<h3><a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Clean up code. <a href='https://commits.kde.org/plasma-sdk/fd8ab049a2790ad0c579a77a4e9f6bad836510b1'>Commit.</a> </li>
<li>Allow switching between monochrome (Plasma's SVG icons) and traditional rendering. <a href='https://commits.kde.org/plasma-sdk/f595ff832a5c39a5976ab5957f4d3d7284a59c24'>Commit.</a> </li>
<li>Use plasma theme switch for all icons. <a href='https://commits.kde.org/plasma-sdk/ee4c42283937099b2297f6342ca574d782535089'>Commit.</a> </li>
<li>Allow to switch between monochrome and full-color icons. <a href='https://commits.kde.org/plasma-sdk/c84a79322f99773482a5d1f7da1f63550cc3711f'>Commit.</a> </li>
<li>Switch to newer KDevelop API. <a href='https://commits.kde.org/plasma-sdk/4a24c7d3ca1044c56a1ff4b6d07aefa8eac2d2ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5127'>D5127</a></li>
<li>Make cmake search for the right version of Kirigami. <a href='https://commits.kde.org/plasma-sdk/930621daed5622126aa7b35be3f3b0a3a6c7bc3f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4853'>D4853</a></li>
<li>Port to kirigami2. <a href='https://commits.kde.org/plasma-sdk/db66899283cf5dcbe133ee8786b7665361182955'>Commit.</a> </li>
<li>Make the model being loaded after startup. <a href='https://commits.kde.org/plasma-sdk/5f96526cf1048f7cb307d8f5f2b09ab6bfb75f9b'>Commit.</a> </li>
<li>User-visible strings: Plasma is a proper noun. <a href='https://commits.kde.org/plasma-sdk/80d1db012f1d516c51752c5e095294bfd0b1148d'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[Notification Jobs] Enforce plain text and escape HTML. <a href='https://commits.kde.org/plasma-workspace/8a95911c1ca5ab52f69e02653bc36f9d4dd94ca1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5706'>D5706</a></li>
<li>Make klipper launch mimetype-handling apps normally using KRun. <a href='https://commits.kde.org/plasma-workspace/c5dde9da6859da55ec4f33ffb9da75f4f1d67796'>Commit.</a> </li>
<li>Update min frameworks version. <a href='https://commits.kde.org/plasma-workspace/5f73bbc527505e59487e624470b7361435c7dd25'>Commit.</a> </li>
<li>DBusMenuImporter: Use unique connection for QMenu::aboutToHide. <a href='https://commits.kde.org/plasma-workspace/16629c8ea8e2caca93d4c8ae22b74a3ec9a6a56e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5706'>D5706</a></li>
<li>Set our own QQC style at startup. <a href='https://commits.kde.org/plasma-workspace/2616610ebc514332630a5ec56cbdd5126ded68cf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5720'>D5720</a></li>
<li>Avoid deep copy of image data when getting pixmaps in SNIs. <a href='https://commits.kde.org/plasma-workspace/26d855ea63b13068d78b3c363570f9436ae2a791'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5670'>D5670</a></li>
<li>Update unit test to match change to reselecting top entry. <a href='https://commits.kde.org/plasma-workspace/13f63a3f3897abfbec64066e7a3f3af22661329f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5570'>D5570</a></li>
<li>Set correct KF5_MIN_VERSION. <a href='https://commits.kde.org/plasma-workspace/abb6b2ed3ddd17ff354e85fbb0dc9007e2429315'>Commit.</a> </li>
<li>Rename the translation catalog (avoid conflicts). <a href='https://commits.kde.org/plasma-workspace/039bff026c83ee389a90ab5786f7b1ffbfed4e63'>Commit.</a> </li>
<li>Add back include changes lost in revert and merge. <a href='https://commits.kde.org/plasma-workspace/3b305e79f530b816f50304118f96914256fdc604'>Commit.</a> </li>
<li>Revert "launch autostart apps in ksmserver using KRun". <a href='https://commits.kde.org/plasma-workspace/ea3f87c5df0251838da71c473fd7b790c932d8b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379254'>#379254</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5618'>D5618</a></li>
<li>Fix minimum and maximum size for icon. <a href='https://commits.kde.org/plasma-workspace/352221ce47b884a69a3fbe02c57188e1c46d72dc'>Commit.</a> </li>
<li>Configurable icon size. <a href='https://commits.kde.org/plasma-workspace/0f8302c2c38ca8f60e76b118daa651a3b1ee89c9'>Commit.</a> See bug <a href='https://bugs.kde.org/378443'>#378443</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5594'>D5594</a></li>
<li>[Media Controller] Support CanPlay/CanPause. <a href='https://commits.kde.org/plasma-workspace/08aa1175172b80270db2af20bd6d74f8ff14df27'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5582'>D5582</a></li>
<li>[Lock Screen] Show Previous/Next buttons only if supported. <a href='https://commits.kde.org/plasma-workspace/18ee82361164b48e809080a49a9a77dcd0480260'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5583'>D5583</a></li>
<li>Selecting the topmost klipper item should always set it as clipboard contents. <a href='https://commits.kde.org/plasma-workspace/921f89cb62e2fa118ba16ea87bcec0ba41751a82'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348390'>#348390</a>. Fixes bug <a href='https://bugs.kde.org/251222'>#251222</a></li>
<li>Selecting the topmost klipper item should always set it as clipboard contents. <a href='https://commits.kde.org/plasma-workspace/f78b0869f01d255d0e17a0a246e5a9456f2bba4e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348390'>#348390</a>. Fixes bug <a href='https://bugs.kde.org/251222'>#251222</a></li>
<li>Update ungrab mouse hack for Qt 5.8. <a href='https://commits.kde.org/plasma-workspace/fdd3e3fc7ad2b8ed8f79f204fc8f33c2a22ee357'>Commit.</a> </li>
<li>Introduce a concept of runtime executables that can be ignored. <a href='https://commits.kde.org/plasma-workspace/1c8a46791c4eda644920fd0b6abb6197aac423ce'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5522'>D5522</a></li>
<li>Use KProcess instead of QProcess to launch apps in ksmserver. <a href='https://commits.kde.org/plasma-workspace/17eeb58ae42596195d6a841c06d17721b7af2c14'>Commit.</a> See bug <a href='https://bugs.kde.org/369391'>#369391</a>. See bug <a href='https://bugs.kde.org/370528'>#370528</a></li>
<li>Launch autostart apps in ksmserver using KRun, not QProcess. <a href='https://commits.kde.org/plasma-workspace/0f19e92f3e85d064de9cebf280fa8e085485c2e0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369391'>#369391</a>. Fixes bug <a href='https://bugs.kde.org/370528'>#370528</a></li>
<li>Recognize the WM even if given with a full path. <a href='https://commits.kde.org/plasma-workspace/7fca524e0846fd16558a4ac085afa551a9f22c99'>Commit.</a> See bug <a href='https://bugs.kde.org/377756'>#377756</a></li>
<li>Use the right path for VirtualKeyboard.qml. <a href='https://commits.kde.org/plasma-workspace/3671cbdbe50712933ac77a77675d80520203fde3'>Commit.</a> </li>
<li>Delay these HasLauncher updates until the launcher tasks model is lazy-initialized. <a href='https://commits.kde.org/plasma-workspace/faca0710ad73292970dbf0ba0caac5e76a247541'>Commit.</a> </li>
<li>Introduce a HasLauncher data role. <a href='https://commits.kde.org/plasma-workspace/3546de22154979f896a3ef84b234ebde28fce404'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5504'>D5504</a></li>
<li>[Lock Screen] Enforce plainText for media controls. <a href='https://commits.kde.org/plasma-workspace/0c9255510a6796eb10e98df75b93900f6761ed21'>Commit.</a> </li>
<li>[Media Controller] nly fold hours (to get rid of them) when track is shorter than an hour. <a href='https://commits.kde.org/plasma-workspace/e72aedace91651eefc711912cede12ae0492b1be'>Commit.</a> </li>
<li>[Plasma Windowed] Emit contextualActionsAboutToShow before showing them. <a href='https://commits.kde.org/plasma-workspace/27efb8b19577cde1c332259a12bc5e85375e67b1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5239'>D5239</a></li>
<li>[Plasma Windowed] Disable standard actions on SNI. <a href='https://commits.kde.org/plasma-workspace/71dda89bb80d69bd8fe0d77c3e28075341270a34'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5333'>D5333</a></li>
<li>[Notifications] Introduce "settings" action. <a href='https://commits.kde.org/plasma-workspace/e99f396713db6d13b0ba620df96b1513e1576f78'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5424'>D5424</a></li>
<li>Selecting the topmost klipper item should always set it as clipboard contents. <a href='https://commits.kde.org/plasma-workspace/37014e643cec4ee9aed54421f66c675e1bc91b70'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348390'>#348390</a>. Fixes bug <a href='https://bugs.kde.org/251222'>#251222</a></li>
<li>Re-enable the 'logout cancelled by <app>' notification. <a href='https://commits.kde.org/plasma-workspace/616f57aecf6193efc333174360190549e2e4dcdb'>Commit.</a> </li>
<li>Use KProcess instead of QProcess to launch apps in ksmserver. <a href='https://commits.kde.org/plasma-workspace/eadaf1cd6f55e6092deb2114c5477559ebf063e0'>Commit.</a> See bug <a href='https://bugs.kde.org/369391'>#369391</a>. See bug <a href='https://bugs.kde.org/370528'>#370528</a></li>
<li>Launch autostart apps in ksmserver using KRun, not QProcess. <a href='https://commits.kde.org/plasma-workspace/0377d6a992725e0b0b469b87d17343fa3730b829'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369391'>#369391</a>. Fixes bug <a href='https://bugs.kde.org/370528'>#370528</a></li>
<li>[lnf] Honor the new noModifierGrab in TabBox. <a href='https://commits.kde.org/plasma-workspace/ea9489f32a6ce455b7b91d2f68565bac62fdca6f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5415'>D5415</a></li>
<li>Fix error text. <a href='https://commits.kde.org/plasma-workspace/ffe5ff3b1f5b00e790d8c46411222ebb05453ebb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5410'>D5410</a></li>
<li>Correctly handle when a new primary screen displaces the old. <a href='https://commits.kde.org/plasma-workspace/e0a8df2910fa272f5ed5913ea38b578f5c7145d0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377808'>#377808</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5323'>D5323</a></li>
<li>In doubt return displayName. <a href='https://commits.kde.org/plasma-workspace/591dbd2640b637ee363a749707ca2da6f7e92a2e'>Commit.</a> </li>
<li>A bit more debug to investigate on multiscreen bugs. <a href='https://commits.kde.org/plasma-workspace/affee8c8dafd4b998cff7d85747df29ef0db31ff'>Commit.</a> </li>
<li>Better layout and animate keyboard. <a href='https://commits.kde.org/plasma-workspace/7611f250cce6e39c88db2861fdec3cc1cf2fd35d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5254'>D5254</a></li>
<li>Layout and animations on the on screen keyboard. <a href='https://commits.kde.org/plasma-workspace/298e58158941d73de0fa6710ab0328f99c3183f8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4893'>D4893</a></li>
<li>Merge the AppStream runner into the Plasma Workspace. <a href='https://commits.kde.org/plasma-workspace/a59b9e61897887ea93cf736aff99243facdc45d9'>Commit.</a> </li>
<li>[desktop:/ KIO] Add descriptive name for root item. <a href='https://commits.kde.org/plasma-workspace/d6121f3f5c2504bfff17c68ac5518f5b1caa3b7d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5221'>D5221</a></li>
<li>[System Tray] Fix regression on SNI fallback. <a href='https://commits.kde.org/plasma-workspace/ba260e72a19a7e53a2d8aa366349da459204eebb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5270'>D5270</a></li>
<li>Keep system tray sidebar y position independent of current applet heading. <a href='https://commits.kde.org/plasma-workspace/882fc8d0ff94abc0cbddb34af1b2853f89dc16ca'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378194'>#378194</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5300'>D5300</a></li>
<li>[Notifications] Port from ProcessRunner to KCMShell.open and respect KIOSK restriction. <a href='https://commits.kde.org/plasma-workspace/a3a7b95f5f41f49cda5fd73989cfa6c02cbd9c1e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5256'>D5256</a></li>
<li>Fixes for plasmawindowed sni support. <a href='https://commits.kde.org/plasma-workspace/bf19dbba136e4332a43532c30244ab08cac7f456'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5242'>D5242</a></li>
<li>Drop remote ioslave, which is now in KIO 5.32. <a href='https://commits.kde.org/plasma-workspace/f81c73b8e25b06a58d89ef42a8ccfd338c6e556f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371825'>#371825</a>. Phabricator Code review <a href='https://phabricator.kde.org/D3531'>D3531</a></li>
<li>[Plasma Windowed] Forward plasmoid status to SNI and update title and icon live. <a href='https://commits.kde.org/plasma-workspace/d9d4737b3f547788f0a4403bdcfd5a7ef4be4421'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5238'>D5238</a></li>
<li>Close the plasmoid window with ctrl+q. <a href='https://commits.kde.org/plasma-workspace/522276e34e269c2c59cc477ab1707efbf26daf88'>Commit.</a> </li>
<li>Setting a sensible minimum size for plasmawindowed. <a href='https://commits.kde.org/plasma-workspace/e20f155190ac4e47bd2a9c1967b79f6e125c95ec'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376277'>#376277</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5235'>D5235</a></li>
<li>[Calculator Runner] Use "approximate" approximation mode. <a href='https://commits.kde.org/plasma-workspace/df7f1ed1eb7df7e3c039dc19db4893ca01b3327f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/277011'>#277011</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4290'>D4290</a></li>
<li>[DrKonqi] Drop "It is safe to close this dialog if you don't want to report a bug". <a href='https://commits.kde.org/plasma-workspace/91deac7c6a19b9dc83d6944cd8151d7cc5550d0d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5085'>D5085</a></li>
<li>[sddm-theme] Add virtual keyboard support. <a href='https://commits.kde.org/plasma-workspace/78d2c9ffbed64c12578e8b94aead3ecad6bf48bb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5061'>D5061</a></li>
<li>Remove unused include. <a href='https://commits.kde.org/plasma-workspace/64d4217e35d2cf6a8aa1f15d2cb3ec736ab35436'>Commit.</a> </li>
<li>Revert "Do not merge: activation support in appmenu". <a href='https://commits.kde.org/plasma-workspace/d9f4e11261ef0fcc31bf144d880009b629a985d9'>Commit.</a> </li>
<li>Inject mouse clicks from SNI to xembedded icons with XTest. <a href='https://commits.kde.org/plasma-workspace/9df815e843a4385465fff0cb9a76ddc94ed35b38'>Commit.</a> See bug <a href='https://bugs.kde.org/375017'>#375017</a>. See bug <a href='https://bugs.kde.org/362941'>#362941</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5156'>D5156</a></li>
<li>Improve position of Clock. <a href='https://commits.kde.org/plasma-workspace/3dc5886e62d1340fc021ad6b151f277d3cda624e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5126'>D5126</a></li>
<li>Do not merge: activation support in appmenu. <a href='https://commits.kde.org/plasma-workspace/69d8eb717cce792405d15372763a5e5efa45ed09'>Commit.</a> </li>
<li>Remove ifdef round qCDebugs. <a href='https://commits.kde.org/plasma-workspace/436abf6c2147bc4c77081597b8942a0a31236cd2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4965'>D4965</a></li>
<li>Recognize the WM even if given with a full path. <a href='https://commits.kde.org/plasma-workspace/70584412e35cd82b95f60790e545851e31f350e3'>Commit.</a> See bug <a href='https://bugs.kde.org/377756'>#377756</a></li>
<li>UI fixes for logout dialog. <a href='https://commits.kde.org/plasma-workspace/5f2f343ef6a31702af6e71f6aee6b76a94d953d3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5036'>D5036</a></li>
<li>[Logout Greeter] Properly fullscreen window and don't bypass WM. <a href='https://commits.kde.org/plasma-workspace/8777aeb91d9120ebb31db89d163718a383746821'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5115'>D5115</a></li>
<li>Add functions for locking/unlocking the corona. <a href='https://commits.kde.org/plasma-workspace/c64aa75c90f0078eb72f2060feed3a5f63ac7db7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5009'>D5009</a></li>
<li>Fix compilation with old gcc. <a href='https://commits.kde.org/plasma-workspace/2f22f0a935b69f2f2a866050be5b905f9df4e4b6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5083'>D5083</a></li>
<li>Add program to wait for D-Bus Notifications services to register. <a href='https://commits.kde.org/plasma-workspace/5bf889f7b94b388b71a357fe2e9517b1ca3bbbdc'>Commit.</a> </li>
<li>Fix a crash caused by bad disconnects during debug install cancellation. <a href='https://commits.kde.org/plasma-workspace/9fd5fbd95465e8590323e141b1e1311c3fb79c6b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5084'>D5084</a></li>
<li>Rip out ancient compat code which was meant to have disappeared in 4.11. <a href='https://commits.kde.org/plasma-workspace/78199d3f39389e58b1e8e569ccd99b8b1e16071d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5082'>D5082</a></li>
<li>Do not add debugger button unless it is meant to be visible. <a href='https://commits.kde.org/plasma-workspace/18fb32bbd29ad8ad36a8518643617de7b9025df4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5080'>D5080</a></li>
<li>[System Tray] Add KAcceleratorManager. <a href='https://commits.kde.org/plasma-workspace/48fe6ba01c7f473e57b0db294834f1abde531960'>Commit.</a> See bug <a href='https://bugs.kde.org/361915'>#361915</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5058'>D5058</a></li>
<li>[lnf] Only show virtual keyboard button if virtual keyboard is available. <a href='https://commits.kde.org/plasma-workspace/54479d32a96430dd093b4489c6f2a6fe449d2f80'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5062'>D5062</a></li>
<li>Do not use fixed steps when scrolling on battery icon. <a href='https://commits.kde.org/plasma-workspace/82a44a3f264cecc9982b3e0ed164aeb0b11ef18c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5025'>D5025</a></li>
<li>Revert "After a notification action is triggered, remove the action". <a href='https://commits.kde.org/plasma-workspace/40d42d55e3ab5a10ccec45dac6b0196f36a2a587'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376444'>#376444</a></li>
<li>[Notifications] Thumbnail context menu can never be empty. <a href='https://commits.kde.org/plasma-workspace/a2f9c78a3a889d1edf99efe243d06df8594e1c25'>Commit.</a> </li>
<li>[Notifications] Add "Properties" for thumbnail context menu. <a href='https://commits.kde.org/plasma-workspace/bdba2968be46026defc17a85aed450426f7cde91'>Commit.</a> </li>
<li>[Notifications] Add context menu for thumbnail. <a href='https://commits.kde.org/plasma-workspace/f98a90df2541af27e9063ab48741c3c09c08b52b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4838'>D4838</a></li>
<li>Fix AppMenuApplet Called C++ object pointer is null found by Clang Analyzer. <a href='https://commits.kde.org/plasma-workspace/2b7ef97f8104db6212e2a75d2578c6edca96e05a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129994'>#129994</a></li>
<li>[Notifications Dataengine] Create KConfig object on the stack. <a href='https://commits.kde.org/plasma-workspace/891d8a3bcb236ff79f4dc81261e3b1f7051b998b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4869'>D4869</a></li>
<li>Fix KillRunner Memory leak issue. <a href='https://commits.kde.org/plasma-workspace/55237430bdab1bfbbfb442281ca5e34f5eb5e212'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129995'>#129995</a></li>
<li>Set a longer wallpaper transition time. <a href='https://commits.kde.org/plasma-workspace/f255af63d2c9eb919dbe75e7a8ba681646890fda'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365979'>#365979</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4370'>D4370</a></li>
<li>[System Tray] Listen to external collapse. <a href='https://commits.kde.org/plasma-workspace/f873432893eea52fdc20da85e6a227c74cc8f37a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4865'>D4865</a></li>
<li>[lookandfeel] Add virtual keyboard to the LockScreen. <a href='https://commits.kde.org/plasma-workspace/b6705f8d73bd0023e72ce2aafbe7526ae5fcfd2a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4870'>D4870</a></li>
<li>[lookandfeel] Add Input Method hints for the password field. <a href='https://commits.kde.org/plasma-workspace/1ce5ed3b7e0d12c6ff7b43647ff2949c2c1c8d99'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4876'>D4876</a></li>
<li>[lookandfeel] Improve positioning of Clock. <a href='https://commits.kde.org/plasma-workspace/097db85e297aba8b4b3f0ddabcddf8a03b5482c0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4877'>D4877</a></li>
<li>[Icon Applet] Reject unacceptable drags. <a href='https://commits.kde.org/plasma-workspace/2c286ee8d27e65a68077b72b1e2b8b28993bbd92'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4737'>D4737</a></li>
<li>Try harder to make the selected wallpaper visible. <a href='https://commits.kde.org/plasma-workspace/ed2f9de7f4c2837723275aaa74c8b55f398cd3dc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4648'>D4648</a></li>
<li>[KSplashQML] Drop "Loading stage" debug output. <a href='https://commits.kde.org/plasma-workspace/faf76152ef02304b5e07a3d006ee6421dc3e9088'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4709'>D4709</a></li>
<li>Overhaul app associativity heuristic to give precendence to StartupWMClass. <a href='https://commits.kde.org/plasma-workspace/f17a395a40e932069863419166721579a11712bc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4706'>D4706</a></li>
<li>Cmake: Use ${KF5_MIN_VERSION} to search for prison. <a href='https://commits.kde.org/plasma-workspace/ab9c3bc68edfd0b00eaaac1fe6e464871579bd04'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4568'>D4568</a></li>
<li>Fix merge of dda4d42e891a3. <a href='https://commits.kde.org/plasma-workspace/1a1d6445149c8543be66472906f69ef6ba135e21'>Commit.</a> </li>
<li>Fix more merge weirdness, somehow this code got lost. <a href='https://commits.kde.org/plasma-workspace/eaf5c71e3b26ff2e399499c24d10c45093d1fb65'>Commit.</a> </li>
<li>Fix up merge mistake. <a href='https://commits.kde.org/plasma-workspace/adb954ef80552535b484b6a8a3a03c910b5dff8b'>Commit.</a> </li>
<li>Don't show 0% when no battery. <a href='https://commits.kde.org/plasma-workspace/9131740a82f8a50a74ff29bda8a58efb0785381d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4606'>D4606</a></li>
<li>Add Media Controls on lock screen. <a href='https://commits.kde.org/plasma-workspace/f32960cf38053c35305d88f98fa9e0c08add70b6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3684'>D3684</a></li>
<li>[PanelView] Emit availableScreenRegionChanged in resizeEvent, not when setting a new length. <a href='https://commits.kde.org/plasma-workspace/d67aea401703517340ea3d9744ab64cb5c3e853e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4590'>D4590</a></li>
<li>Add missing summaries. <a href='https://commits.kde.org/plasma-workspace/034bf808787540bfae4facee15a54bac15c3cf71'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4447'>D4447</a></li>
<li>Centralize interaction with notifications. <a href='https://commits.kde.org/plasma-workspace/eb0991bc8387b521370c7ce6eb0aee0d865541bd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4545'>D4545</a></li>
<li>[Widget Explorer] Remove pointless QSignalMapper. <a href='https://commits.kde.org/plasma-workspace/e6dc96f30c4a5864ee88c1bd929370909f88208f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4591'>D4591</a></li>
<li>[CurrentContainmentActionsModel] Allow making the action about dialog modal. <a href='https://commits.kde.org/plasma-workspace/2f66c0d11c68f13dd81d6a941d4ee95a63d4bdfe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4567'>D4567</a></li>
<li>Display speeds in bits per second instead of KiB/s. <a href='https://commits.kde.org/plasma-workspace/8da91cde831c75111c62f2bc7e2e9481f227d7a6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4551'>D4551</a></li>
<li>Address comments in D4215. <a href='https://commits.kde.org/plasma-workspace/1c513a0dcd9a26bcdd4129271014db300c6a60d7'>Commit.</a> </li>
<li>After a notification action is triggered, remove the action. <a href='https://commits.kde.org/plasma-workspace/86b7aaf408383aad4f27910c9259494e7d0e1e51'>Commit.</a> </li>
<li>Make notifications execute the "default" action on click. <a href='https://commits.kde.org/plasma-workspace/6f2b57b4925d7d24bf9b9b7945c4c806ba03dc1f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4215'>D4215</a></li>
<li>[kioslave/remote] Use kf5 coding style. <a href='https://commits.kde.org/plasma-workspace/e32b68ed614be99b5222ca6d05e83d38741e0035'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4489'>D4489</a></li>
<li>Fix wallpaper configuration for plasma mobile. <a href='https://commits.kde.org/plasma-workspace/1750080bdd95452a5552d10bed0fdf43b6baaae7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4528'>D4528</a></li>
<li>Port image wallpaper code to use qCDebug. <a href='https://commits.kde.org/plasma-workspace/d6aad9eb710441f246e8c2718a56e0974641a780'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4529'>D4529</a></li>
<li>[System Tray] Introduce "effectiveStatus" property and update visibility only when that changes. <a href='https://commits.kde.org/plasma-workspace/a7d56be57a1c1258608d8eb363191d69f40c12e3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375112'>#375112</a>. See bug <a href='https://bugs.kde.org/365630'>#365630</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4488'>D4488</a></li>
<li>[Calculator Runner] Port non-Qalculate codepath from QtScript to QtQml. <a href='https://commits.kde.org/plasma-workspace/605fb9acd867e22e171184a08d9dfd2d1d4e893e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4472'>D4472</a></li>
<li>Introduce an adoption command in the wallpaper knsrc file. <a href='https://commits.kde.org/plasma-workspace/a5f2d35e1b0da350748fcd1bacbd6b48bad1dfe0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4307'>D4307</a></li>
<li>React to config change. <a href='https://commits.kde.org/plasma-workspace/87fa4542aca623a83d00c866c01f284ba183efc3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4461'>D4461</a></li>
<li>Adjust KeyboardLayout to non-kded DBus service. <a href='https://commits.kde.org/plasma-workspace/1b8bb904763b649ac1650ad6c0133b04453b0129'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4322'>D4322</a></li>
<li>[System Tray] SNI fallback to context menu on failing Activate. <a href='https://commits.kde.org/plasma-workspace/5d5518455d10406292495c6e6874eeeb7c4534f1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375351'>#375351</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4301'>D4301</a></li>
<li>[Shell Corona] Avoid iterating keys() and then doing a value() lookup. <a href='https://commits.kde.org/plasma-workspace/7600d5f72e99c31515546186243b508113fe9605'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4349'>D4349</a></li>
<li>QStandardPaths::enableTestMode -> setTestModeEnabled. <a href='https://commits.kde.org/plasma-workspace/0ef5ce08003c1a29fd8b01155daaf84acf3a9750'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4337'>D4337</a></li>
<li>Fix isOutputRedundant logic. <a href='https://commits.kde.org/plasma-workspace/996f5b199f40ade64b4d833f5ece5300c582670c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375507'>#375507</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4309'>D4309</a></li>
<li>Remove unneeded QString() call. <a href='https://commits.kde.org/plasma-workspace/2212c42417398d4e6237e578c43206e94d5f3120'>Commit.</a> </li>
<li>Remove pointless QObject inheritance in model item. <a href='https://commits.kde.org/plasma-workspace/63cfbd991e0146044d716c62dec1a48e96138c68'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4218'>D4218</a></li>
<li>Remove unused feature "favorite" from PlasmaAppletItemModel. <a href='https://commits.kde.org/plasma-workspace/c11aac0f58628825ee0e7023de0d88c2f3d9155e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4217'>D4217</a></li>
<li>Fix clang address sanitiser error in SNI memcpy. <a href='https://commits.kde.org/plasma-workspace/24a7f4e3ba72471218fd54decaaddc5baa74482f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4196'>D4196</a></li>
<li>Require Qt 5.7. <a href='https://commits.kde.org/plasma-workspace/cb2803900fa53074dc6048aa511b722be7b4cd20'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4198'>D4198</a></li>
<li>Revert "use Q_FOREACH in this branch". <a href='https://commits.kde.org/plasma-workspace/b3c0cb946569f06b56e81be9e32b0a10044928c6'>Commit.</a> </li>
<li>Screen geometry parameter not needed. <a href='https://commits.kde.org/plasma-workspace/a0470a089f91f4f381690b48eafe33587ce72671'>Commit.</a> </li>
<li>Corona::screenGeometryChanged on qscreen resized. <a href='https://commits.kde.org/plasma-workspace/9831eee505f9dc089e72f8f6d285e53ca8cb0be3'>Commit.</a> </li>
<li>Try to put annotations before parameters definition. <a href='https://commits.kde.org/plasma-workspace/fa92dad1fff534ebe321b68a1dfe2ea98b8f6064'>Commit.</a> </li>
<li>Fix dbus xml compilation. <a href='https://commits.kde.org/plasma-workspace/918aa7482567b4ff61585deb3fe851d5d765be9d'>Commit.</a> </li>
<li>Make debug info a bit more useful. <a href='https://commits.kde.org/plasma-workspace/25297320c2514c75abf734abf3f2d97e6c902a2d'>Commit.</a> </li>
<li>Minor: Fix annoying -Wdocumentation warning. <a href='https://commits.kde.org/plasma-workspace/bff73ef91d3563e41b7673bde2da4a8e0b2ee30d'>Commit.</a> </li>
<li>Keep AppletMenu as small as possible. <a href='https://commits.kde.org/plasma-workspace/e7dc85aa0b5f7e96e50f073a7b3e2662436a8c0c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374996'>#374996</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4115'>D4115</a></li>
</ul>


<h3><a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a></h3>
<ul id='ulplymouth-kcm' style='display: block'><li>New in this release</li></ul>
<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Fix the skip disabled backlight device. <a href='https://commits.kde.org/powerdevil/86c3548d103ff5966366788220796de629be9230'>Commit.</a> </li>
<li>Pause media players on suspend. <a href='https://commits.kde.org/powerdevil/9ac78b69ddca15c7ba3f86791eb03eee3318db89'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4960'>D4960</a></li>
<li>Re-trigger lid action when lid is closed and external monitor is disconnected. <a href='https://commits.kde.org/powerdevil/c57301685cc628ab05a932067e4d506cc427163a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379265'>#379265</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5606'>D5606</a></li>
<li>Revert "Get rid of KDELibs4Support". <a href='https://commits.kde.org/powerdevil/55603160e9b894270a34e7d01e0aef5bbbfe8025'>Commit.</a> </li>
<li>Skip the disabled backlight device. <a href='https://commits.kde.org/powerdevil/5c0d35ca6caf7bb0a0e9897a1a868e8406f3b112'>Commit.</a> </li>
<li>Remove no longer needed config-workspace header. <a href='https://commits.kde.org/powerdevil/543e6c4235d7d64067d878d20175556161b87ee4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5599'>D5599</a></li>
<li>Get rid of KDELibs4Support. <a href='https://commits.kde.org/powerdevil/cdd1a63d40d1972d27f30a690d851b95249a4652'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4939'>D4939</a></li>
<li>Get rid of QWeakPointer. <a href='https://commits.kde.org/powerdevil/29c3c91c56d0aaa1a063e234c86a3d7a542edbf7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4970'>D4970</a></li>
<li>Add UI for hybrid suspend mode. <a href='https://commits.kde.org/powerdevil/c6b76da7a67977deacadb65e6e9d1713e8b3f7b6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5016'>D5016</a></li>
<li>Rename wireless power saving to wireless. <a href='https://commits.kde.org/powerdevil/f211aabfaf245aeaf1fcce641253a98143044cbc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4524'>D4524</a></li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>I18n: fix string puzzle. <a href='https://commits.kde.org/sddm-kcm/8243f60728625a71a32f88d42ec3d18c57807314'>Commit.</a> </li>
<li>Also list wayland sessions in autostart list. <a href='https://commits.kde.org/sddm-kcm/6c910d9f56e394c55055d0e5e9bbe62a9f239f91'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5588'>D5588</a></li>
<li>Fix bad merge of 5.9 into master. <a href='https://commits.kde.org/sddm-kcm/59589ec5af57d87a65a32ae56fe827ae85d3c181'>Commit.</a> </li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Fix terminology: "by KDE", not "for KDE". <a href='https://commits.kde.org/systemsettings/0bdd25af961788517b7cbfbfa11f040b317b8183'>Commit.</a> </li>
<li>Hide tooltips after clicking items. <a href='https://commits.kde.org/systemsettings/9fc1ddb235c81c0062ed1b5e6dbee00a8143379e'>Commit.</a> </li>
<li>Port tooltips to KToolTipWidget. <a href='https://commits.kde.org/systemsettings/dfedcd46e2b3c9a10809ac65fab9b8a6fba37a9c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4565'>D4565</a></li>
<li>Proofread/update systemsettings docbook. <a href='https://commits.kde.org/systemsettings/2ddc4993caca3b0f7ef80f679caa84b14631a41f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4279'>D4279</a></li>
<li>Add the subgroup accessibility to personalization. <a href='https://commits.kde.org/systemsettings/011f398a7d51c45f24fa5ecc4afbe7dba5520f8a'>Commit.</a> </li>
</ul>


<h3><a name='user-manager' href='https://commits.kde.org/user-manager'>User Manager</a> </h3>
<ul id='uluser-manager' style='display: block'>
<li>Make sure the new avatar is always saved. <a href='https://commits.kde.org/user-manager/826e41429917f6c1534e84e8b7821b8b53675910'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350836'>#350836</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5779'>D5779</a></li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a></h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'><li>New in this release</li></ul>
</main>
<?php
	require('../aether/footer.php');
