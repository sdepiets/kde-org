﻿<?php
  $page_title = "Aankondiging KDE SC 4.4.0 Caikaku";
  $site_root = "../";
  include "header.inc";
?>

<p>Ook beschikbaar in:
<?php
  $release = '4.4';
  include "../announce-i18n-bar.inc";
?>
</p>

<!-- // Boilerplate -->

<h3 align="center">
  KDE Softwarecompilatie 4.4.0 introduceert netbookinterface, vensters met tabbladen en authenticatieraamwerk
</h3>

<p align="justify">
  <strong>
    KDE Softwarecompilatie 4.4.0 (Codenaam: <i>"Caikaku"</i>) uitgebracht
  </strong>
</p>

<p align="justify">
9 februari 2010 - Vandaag kondigt <a href="http://www.kde.org">KDE</a> de onmiddelijke beschikbaarheid aan van de KDE Softwarecompilatie 4.4, <i>"Caikaku"</i>. Deze brengt een innovatieve verzameling programma's voor gebruikers van vrije software. Belangrijke nieuwe technologie&euml;n zijn ge&iuml;ntroduceerd, waaronder mogelijkheden voor sociale netwerken en online samenwerking, een nieuwe interface gericht op netbooks en infrastructurele innovaties zoals het authenticatieraamwerk KAuth. Volgens KDE's bugdatabase zijn er 7293 bugs opgelost en 1433 nieuwe functionaliteiten ge&iuml;mplementeerd. De KDE-gemeenschap wil iedereen bedanken die deze uitgave mogelijk gemaakt hebben.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/general-desktop.png"><img src="screenshots/thumbs/general-desktop_thumb.png" align="center" width="540" height="338"  /></a>
<br />
<em>De KDE Plasma-werkomgeving</em></div>

<p align=justify>
 Raadpleeg de <a href="http://www.kde.org/announcements/4.4/guide.php">Visuele gids</a> over de KDE-softwarecompilatie 4.4 voor meer details over de verbeteringen in 4.4, of lees verder voor een overzicht.
</p>

<h3>
  Plasma-werkomgeving introduceert sociale- en webmogelijkheden
</h3>
<br />
<p align=justify>
De KDE Plasma-werkomgeving biedt de noodzakelijke functionaliteit voor het openen en beheren van programma's, bestanden en globale instellingen. De ontwikkelaars van de KDE-werkomgeving bieden een nieuwe netbookinterface, verder uitgewerkte artwork en een betere werkwijze voor Plasma. De introductie van sociale en online diensten komen voort uit de samenwerking binnen de KDE-gemeenschap.
</p>

<!-- Plasma Screencast -->
<p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<embed src="http://blip.tv/play/hZElgcKJVgA" type="application/x-shockwave-flash" width="500" height="346" allowscriptaccess="always" allowfullscreen="true"></embed>
<br />
<em>Verbeterde interactie met de bureaubladomgeving van Plasma (<a href="http://blip.tv/file/get/Sebasje-KDEPlasmaIn44540.ogg">download</a>)</em>
</div>
</p>


<p align="justify">
<ul>
  <li>
    <strong>Plasma voor netbooks</strong> debuteert in 4.4.0. Dit is een alternatieve interface voor het Plasma-bureaublad, speciaal ontworpen voor ergonomisch gebruik op netbooks en kleine notebooks. Het Plasma-raamwerk was van het begin af aan gebouwd om ook andere apparaten te ondersteunen dan desktops. De netbookomgeving deelt veel componenten met het Plasma-bureaublad, maar is zodanig ontworpen om optimaal gebruik te maken van de beperkte ruimte en is geschikter voor touchscreens.
    Plasma voor netbooks biedt een programmastarter in een volledig scherm, een zoekinterface en een nieuwsoverzicht met vele widgets om informatie van het web te tonen. Tenslotte biedt het een aantal kleine hulpmiddelen die reeds bekend zijn bij het grotere broertje van de Plasma-netbookinterface.
  </li>
  <li>
   Het initiatief van het <strong>sociale bureaublad</strong> biedt verbeteringen voor de Community-widget (voorheen bekend onder de naam Sociaal Bureaublad), waarbij gebruikers berichten kunnen sturen en vrienden kunnen vinden vanaf het bureaublad. De widget Sociaal Nieuws toont live een stroom van berichten uit het sociale netwerk van de gebruiker. Een ander nieuw widget stelt de gebruiker in staat om naar antwoorden en vragen te zoeken van verschillende diensten, waaronder openDesktop.org's eigen kennisdatabase.
  </li>
  <li>
    De nieuwe tabbladfunctionaliteit in KWin stelt de gebruiker in staat om <strong>vensters te groeperen</strong> door middel van tabbladen, zodat het beheren van en groot aantal programma's makkelijker en effici&euml;nter wordt. Bovendien kunnen vensters tegen de zijkant van het scherm gezet worden en kunnen vensters gemaximaliseerd worden door ze te slepen. Het team achter KWin heeft samengewerkt met de Plasma-ontwikkelaars om de samenwerking tussen de werkomgevingprogramma's te verbeteren, zodat ze vloeiendere animaties en hogere prestaties tonen. Tenslotte kunnen artiesten eenvoudiger vensterthema's ontwikkelen en delen, dankzij de beter instelbare vensterdecorator en de mogelijkheid om schaalbare afbeeldingen te gebruiken.
  </li>
</ul>
</p>

<!-- Window grouping Screencast -->
<p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<embed src="http://blip.tv/play/hZElgcKPKwA" type="application/x-shockwave-flash" width="500" height="346" allowscriptaccess="always" allowfullscreen="true"></embed>
<br />
<em>Vensterbeheer nog makkelijker gemaakt (<a href="http://blip.tv/file/get/Sebasje-WindowManagingFeaturesInKDEPlasma44222.ogg">download</a>)</em>
</div>
</p>


<h3>
  KDE-programma's innoveren
</h3>
<p align=justify>
De KDE-gemeenschap biedt een groot aantal krachtige, maar nog steeds eenvoudig te gebruiken, programma's. Deze uitgave biedt een groot scala aan geleidelijke verbeteringen en innovatieve nieuwe technologie&euml;n.
</p>
<p align=justify>
<ul>
  <li>
    In deze uitgave van de KDE-softwarecompilatie is de interface om nieuwe zaken op te halen verbeterd, als resultaat van langdurig ontwerp en plannen. Het raamwerk is bedoeld om bijdragers van KDE <strong>eenvoudiger in contact te laten komen</strong> met de miljoenen gebruikers van hun bijdrage. Gebruikers kunnen bijvoorbeeld <a href="http://dimsuz.wordpress.com/2009/11/11/katomic-new-feature-level-sets-support/">nieuwe levels voor KAtomic</a> downloaden, nieuwe sterren downloaden of uitbreidingen in functionaliteit vanuit het programma. Nieuw zijn de <strong>sociale mogelijkheden</strong> zoals commentaar geven, het beoordelen van bijdragen of fan worden van een product, zodat de gebruiker updates krijgt over deze producten in de widget Sociaal Nieuws. Gebruikers kunnen <strong>resultaten uploaden</strong> van hun eigen creativiteit vanuit verschillende toepassingen, waarbij het proces van inpakken en uploaden naar een website tot het verleden behoort.
  </li>
  <li>
   Twee andere lange-termijnprojecten door de KDE-gemeenschap komen in deze uitgave met resultaten. Nepomuk, een internationaal onderzoek gefinancieerd door de Europese Gemeenschap, biedt nu voldoende stabiliteit en prestaties. <strong><a href="http://ppenz.blogspot.com/2009/11/searching.html">De zoekintegratie in Dolphin</a></strong> maakt gebruik van Nepomuks semantisch raamwerk om de gebruiker te helpen met het terugvinden en organiseren van gegevens. De tijdlijnweergave toont recent gebruikte bestanden, op tijd gesorteerd. Ondertussen heeft het team achter KDE PIM de eerste programma's aangepast zodat het gebruik maakt van het nieuwe opslagsysteem <strong>Akonadi</strong>. Het KDE-adresboek is herschreven, met een eenvoudige driedelige interface. Meer programma's zullen in toekomstige uitgaven van de KDE-softwarecompilatie omgezet worden.
  </li>
  <li>
    Naast de integratie van deze twee belangrijke technologie&euml;n hebben de verschillende ontwikkelteams in veel opzichten verbeteringen aangebracht aan de toepassingen. De ontwikkelaars van KGet hebben ondersteuning voor het controleren van digitale handtekeningen en het downloaden van meerdere bronnen aangebracht. Gwenview komt met een gebruiksvriendelijk hulpmiddel om foto's te importeren. Bovendien zien nieuwe of volledig herschreven programma's het levenslicht in deze uitgave. Palepeli is een puzzelspel waarbij de gebruiker legpuzzels kan oplossen op de computer. Gebruikers kunnen ook hun eigen puzzels maken en delen. Cantor is een eenvoudig te gebruiken schil voor statistische en wetenschappelijke software (<a href="http://r-project.org/">R</a>, <a href="http://sagemath.org/">SAGE</a>, en <a href="http://maxima.sourceforge.net/">Maxima</a>). Het KDE PIM-pakket komt met een een blogprogramma: Blogilo.
  </li>
</ul>
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/dolphin-search.png"><img src="screenshots/thumbs/dolphin-search_thumb.png" align="center" width="540" height="338"  /></a><br />
<em>Zoekopdrachten ge&iuml;ntegreerd in de bestandsbeheerder van KDE: Dolphin</em></div>

<h3>
  Platform versnelt ontwikkeling
</h3>
<p align=justify>
De sterke focus op uitstekende technologie binnen de KDE-gemeenschap heeft geresulteerd in een het meest complete, consistente en effici&euml;nte ontwikkelplatform dat momenteel beschikbaar is. De 4.4-uitgave introduceert vele samenwerkings- en sociale netwerktechnologie&euml;n in de KDE-bibliotheken. We bieden krachtige en flexibele open alternatieven op bestaande technologie&eulm;n. In plaats van te streven naar het vastbinden van gebruikers aan onze producten, focussen we erop om gebruikers te laten innoveren, zowel online als op het bureaublad.
<p align=justify>
<ul>
  <li>
    De onderliggende infrastructuur van de KDE-software is flink onder handen genomen. Om te beginnen introduceert <strong>Qt 4.6</strong> ondersteuning voor het Symbianplatform, een nieuw animatieraamwerk, multitouch en betere prestaties. De <strong>technologie voor het sociale bureaublad</strong>, ge&iuml;ntroduceerd in de vorige uitgave, is verbeterd met een centraal identiteitsbeheer en introduceert libattica als een transparante toegang tot online diensten. <strong>Nepomuk</strong> maakt nu gebruik van een stabielere achtergrondcomponent, waardoor het nu een geschikte keuze is voor alles dat met metadata, zoeken en indexeren te maken heeft in programma's.
  </li>
  <li>
    Met KAuth komt een nieuw authenticatieraamwerk om de hoek kijken. <strong>KAuth biedt veilige authenticatie</strong> en bijbehorende elementen voor de gebruikersinterface, zodat ontwikkelaars bepaalde taken onder bepaalde rechten kunnen uitvoeren. In Linux maakt KAuth gebruik van PolicyKit, zodat een <strong>naadloze integratie tussen bureaubladomgevingen</strong> mogelijk is. KAuth wordt al gebruikt in enkele dialoogvensters binnen de Systeeminstellingen en zal in de komende maanden ge&iuml;ntegreerd worden in KDE Plasma en de KDE-programma's. Met KAuth kan op nauwkeurige wijze rechten toegekend of teruggetrokken worden, wachtwoorden kunnen worden opgeslagen en het biedt een aantal dynamische interface-elementen die visuele hints geven in programma's.
  </li>
  <li>
   <strong>Akonadi</strong>, de groupware-cache van Free Desktop is voor het eerst te zien in KDE-programma's met 4.4.0. Het adresboek is het eerste KDE-programma dat gebruik maakt van de Akonadi-infrastructuur. KAddressbook ondersteunt zowel lokale adresboeken als verschillende groupware-servers. KDE SC 4.4.0 markeert het begin van het Akonadi-era in de KDE-softwarecompilatie. Akonadi zal de centrale interface tot e-mails, contactpersonen, agenda-items of andere persoonlijke informatie worden. Het gedraagt zich als een transparante cache met betrekking tot e-mail- en groupware-servers of andere online diensten.
  </li>
</ul>
Vrij gelicenseerd onder de LGPL (zodat zowel gesloten als open-source-ontwikkeling mogelijk is) en multiplatform (Linux, UNIX, Mac en MS Windows).
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/social-web-widgets.png"><img src="screenshots/thumbs/social-web-widgets_thumb.png" align="center" width="540" height="338"  /></a><br />
<em>Het web en sociale netwerken op het Plasma-bureaublad</em></div>

<h4>
Meer veranderingen
</h4>
<p align=justify>
Zoals is vermeld, is het bovenstaande slechts een selectie van alle veranderingen en verbeteringen aan de KDE-werkomgeving, KDE-programmabundels en het KDE-ontwikkelraamwerk. Een uitgebreidere maar nog steeds onvolledige lijst kan gevonden worden in het <a href="http://techbase.kde.org/Schedules/KDE4/4.4_Feature_Plan">KDE SC 4.4 functionaliteitoverzicht</a> op <a href="http://techbase.kde.org">TechBase</a>. Informatie over de programma's ontwikkeld door de KDE-gemeenschap maar die niet in de KDE-programmabundels zitten kan gevonden worden op de <a href="https://www.kde.org/family/">KDE-familie webpagina</a> en op de <a href="http://kde-apps.org">de kde-apps website</a>.
</p>


<h4>
    Vertel het iedereen
</h4>
<p align="justify">
De KDE-gemeenschap moedigt iedereen aan om <strong>het iedereen te vertellen</strong> op het sociale web. Plaats artikelen op nieuwssites, gebruik kanalen als Delicious, Digg, Reddit, Twitter en identi.ca. Upload schermafdrukken naar diensten als Facebook, FlickR, ipernity en Picasa en plaats ze onder de juiste groepen. Maak screencasts en upload ze naar YouTube, Blip.tv, Vimeo en andere sites. Vergeet niet het ge&uuml;ploade materiaal te taggen met <em>tag <strong>kde</strong></em> zodat het makkelijker is om materiaal terug te vinden, en zodat het KDE-team een rapport kan maken over de verspreiding van de aankondiging van KDE 4.4. <strong>Help ons door het iedereen te vertellen!</strong></p>

<p align="justify">
U kunt de gebeurtenissen op het sociale web rond KDE SC 4.4 live volgen op de nieuwe <a href="http://buzz.kde.org"><strong>KDE Community livefeed</strong></a>. Deze site aggregeert real-time wat er gebeurt op identi.ca, Twitter, FlickR, Picasa, blogs en andere sociale netwerksites. Deze livefeed kan gevonden worden op <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/linux_unix/KDE_Software_Compilation_4_4_0_Introduces_new_innovations"><img src="buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/technology/comments/azx29/kde_software_compilation_440_introduces_netbook/"><img src="buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://www.twitter.com"><img src="buttons/twitter.gif" /></a>
    </td>
    <td>
        <a href="http://www.identi.ca"><img src="buttons/identica.gif" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde44/"><img src="buttons/flickr.gif" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde44"><img src="buttons/youtube.gif" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com"><img src="buttons/facebook.gif" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde44"><img src="buttons/delicious.gif" /></a>
    </td>
</tr>
</table>
<style="font-size: 5pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></style>
</div>


<h4>
  KDE SC 4.4.0 installeren
</h4>
<p align="justify">
KDE, inclusief bibliotheken en haar programma's, is beschikbaar onder een open-source-licentie. KDE-software kan uitgevoerd worden op verscheidene hardwareconfiguraties, besturingssystemen en kan samenwerken met iedere vensterbeheerder of bureaubladomgeving. Naast Linux en andere UNIX-gebaseerde besturingssystemen kunt u voor de meeste KDE-programma's Microsoft Windows-versies vinden op de website van <a href="http://windows.kde.org">KDE voor Windows</a> en voor Apple Mac OS X op de website van <a href="http://mac.kde.org/">KDE voor Mac</a>. Experimentele versies voor mobiele platformen zoals Microsoft Windows Mobile en Symbian kunnen gevonden worden op het internet, maar worden momenteel niet ondersteund.
<br />
KDE kan verkregen worden in broncode en verscheidene binaire formaten van <a href="http://download.kde.org/stable/4.4.0/">http://download.kde.org</a>, maar kan ook verkregen worden op <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a> of via de <a href="http://www.kde.org/download/distributions.php">grotere GNU/Linux- en UNIX-systemen</a>.
</p>
<p align="justify">
  <em>Pakketbouwers</em>
  Enkele Linux/UNIX OS-distribiteurs hebben binaire pakketten beschikbaar gesteld van KDE SC 4.4.0 voor enkele distribitieversies, in andere gevallen worden deze beschikbaar gesteld door vrijwilligers.<br />
  Enkele van deze binaire pakketten kunnen gratis gedownload worden van <a href="http://download.kde.org/binarydownload.html?url=/stable/4.4.0/">http://download.kde.org</a>. Meer binaire pakketten en updates zullen in de komende weken beschikbaar komen.

</p>

<p align="justify">
  <a name="package_locations"><em>Pakketlocaties</em></a>.
  Voor een actuele lijst van beschikbare pakketten waar het KDE-project zich van bewust is kunt u de <a href="/info/4.4.0.php">KDE SC 4.4.0 informatiepagina</a> raadplegen.
</p>

<h4>
  KDE SC 4.4.0 compileren
</h4>
<p align="justify">
  <a name="source_code"></a>
  De volledge broncode van KDE SC 4.4.0 kan <a href="http://download.kde.org/stable/4.4.0/src/">zonder restricties gedownload</a> worden. Instructies voor het compileren en installeren van KDE SC 4.4.0 zijn beschikbaar op de <a href="/info/4.4.0.php#binary">KDE SC 4.4.0 informatiepagina</a>.
</p>
<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
