<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.41.0");
  $site_root = "../";
  $release = '5.41.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
December 10, 2017. KDE today announces the release
of KDE Frameworks 5.41.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Strip down and re-write the baloo tags KIO slave (bug 340099)");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Do not leak rfkill file descriptors (bug 386886)");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add missing icon sizes (bug 384473)");?></li>
<li><?php i18n("add install and uninstall icons for discover");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Add the description tag to the generated pkgconfig files");?></li>
<li><?php i18n("ecm_add_test: Use proper path sep on Windows");?></li>
<li><?php i18n("Add FindSasl2.cmake to ECM");?></li>
<li><?php i18n("Only pass the ARGS thing when doing Makefiles");?></li>
<li><?php i18n("Add FindGLIB2.cmake and FindPulseAudio.cmake");?></li>
<li><?php i18n("ECMAddTests: set QT_PLUGIN_PATH so locally built plugins can be found");?></li>
<li><?php i18n("KDECMakeSettings: more docu about the layout of the build dir");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Support downloading the 2nd or 3rd download link from a KNS product (bug 385429)");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Start fixing libKActivitiesStats.pc: (bug 386933)");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Fix race that starts kactivitymanagerd multiple times");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Allow to only build the kauth-policy-gen code generator");?></li>
<li><?php i18n("Add a note about calling the helper from multithreaded applications");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Do not show edit bookmarks action if keditbookmarks is not installed");?></li>
<li><?php i18n("Port from deprecated KAuthorized::authorizeKAction to authorizeAction");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("keyboard navigation in and out QML kcms");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("Do not crash when setting new line edit on an editable combo box");?></li>
<li><?php i18n("KComboBox: Return early when setting editable to previous value");?></li>
<li><?php i18n("KComboBox: Reuse the existing completion object on new line edit");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Don't look for /etc/kderc every single time");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Update default colors to match new colors in D7424");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Input validation of SubJobs");?></li>
<li><?php i18n("Warn about errors when parsing json files");?></li>
<li><?php i18n("Install mimetype definitions for kcfg/kcfgc/ui.rc/knotify &amp; qrc files");?></li>
<li><?php i18n("Add a new function to measure the length by text");?></li>
<li><?php i18n("Fix KAutoSave bug on file with white space in it");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Make it compile on windows");?></li>
<li><?php i18n("make it compile with QT_NO_CAST_FROM_ASCII/QT_NO_CAST_FROM_BYTEARRAY");?></li>
<li><?php i18n("[MouseEventListener] Allow accepting mouse event");?></li>
<li><?php i18n("use a single QML engine");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("kded: remove dbus calls to ksplash");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Update Brasilian Portuguese translation");?></li>
<li><?php i18n("Update Russian translation");?></li>
<li><?php i18n("Update Russian translation");?></li>
<li><?php i18n("Update customization/xsl/ru.xml (nav-home was missing)");?></li>
</ul>

<h3><?php i18n("KEmoticons");?></h3>

<ul>
<li><?php i18n("KEmoticons: port plugins to JSON and add support for loading with KPluginMetaData");?></li>
<li><?php i18n("Do not leak symbols of pimpl classes, protect with Q_DECL_HIDDEN");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("The usermetadatawritertest requires Taglib");?></li>
<li><?php i18n("If the property value is null, remove the user.xdg.tag attribute (bug 376117)");?></li>
<li><?php i18n("Open files in TagLib extractor readonly");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Group some blocking dbus calls");?></li>
<li><?php i18n("kglobalacceld: Avoid loading an icon loader for no reason");?></li>
<li><?php i18n("generate correct shortcut strings");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("KUriFilter: filter out duplicate plugins");?></li>
<li><?php i18n("KUriFilter: simplify data structures, fix memory leak");?></li>
<li><?php i18n("[CopyJob] Don't start all over after having removed a file");?></li>
<li><?php i18n("Fix creating a directory via KNewFileMenu+KIO::mkpath on Qt 5.9.3+ (bug 387073)");?></li>
<li><?php i18n("Created an auxiliary function 'KFilePlacesModel::movePlace'");?></li>
<li><?php i18n("Expose KFilePlacesModel 'iconName'  role");?></li>
<li><?php i18n("KFilePlacesModel: Avoid unnecessary 'dataChanged' signal");?></li>
<li><?php i18n("Return a valid bookmark object for any entry in KFilePlacesModel");?></li>
<li><?php i18n("Create a 'KFilePlacesModel::refresh' function");?></li>
<li><?php i18n("Create 'KFilePlacesModel::convertedUrl' static function");?></li>
<li><?php i18n("KFilePlaces: Created 'remote' section");?></li>
<li><?php i18n("KFilePlaces: Add a section for removable devices");?></li>
<li><?php i18n("Added baloo urls into places model");?></li>
<li><?php i18n("Fix KIO::mkpath with qtbase 5.10 beta 4");?></li>
<li><?php i18n("[KDirModel] Emit change for HasJobRole when jobs change");?></li>
<li><?php i18n("Change label \"Advanced options\" &gt; \"Terminal options\"");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Offset the scrollbar by the header size (bug 387098)");?></li>
<li><?php i18n("bottom margin based on actionbutton presence");?></li>
<li><?php i18n("don't assume applicationWidnow() to be available");?></li>
<li><?php i18n("Don't notify about value changes if we are still in the constructor");?></li>
<li><?php i18n("Replace the library name in the source");?></li>
<li><?php i18n("support colors in more places");?></li>
<li><?php i18n("color icons in toolbars if needed");?></li>
<li><?php i18n("consider icon colors in the main action buttons");?></li>
<li><?php i18n("start for an \"icon\" grouped property");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Revert \"Detach before setting the d pointer\" (bug 386156)");?></li>
<li><?php i18n("do not install development tool to aggregate desktop files");?></li>
<li><?php i18n("[knewstuff] Do not leak ImageLoader on error");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Properly do strings in the kpackage framework");?></li>
<li><?php i18n("Don't try to generate metadata.json if there's no metadata.desktop");?></li>
<li><?php i18n("fix kpluginindex caching");?></li>
<li><?php i18n("Improve error output");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Fix VI-Mode buffer commands");?></li>
<li><?php i18n("prevent accidental zooming");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Port from QDom to QXmlStreamReader");?></li>
<li><?php i18n("Use https for downloading currency exchange rates");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Expose wl_display_set_global_filter as a virtual method");?></li>
<li><?php i18n("Fix kwayland-testXdgShellV6");?></li>
<li><?php i18n("Add support for zwp_idle_inhibit_manager_v1 (bug 385956)");?></li>
<li><?php i18n("[server] Support inhibiting the IdleInterface");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Avoid inconsistent passworddialog");?></li>
<li><?php i18n("Set enable_blur_behind hint on demand");?></li>
<li><?php i18n("KPageListView: Update width on font change");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("[KWindowEffectsPrivateX11] Add reserve() call");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Fix translation of toolbar name when it has i18n context");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("The #warning directive is not universal and in particular is NOT supported by MSVC");?></li>
<li><?php i18n("[IconItem] Use ItemSceneHasChanged rather than connect on windowChanged");?></li>
<li><?php i18n("[Icon Item] Explicitly emit overlaysChanged in the setter rather than connecting to it");?></li>
<li><?php i18n("[Dialog] Use KWindowSystem::isPlatformX11()");?></li>
<li><?php i18n("Reduce the amount of spurious property changes on ColorScope");?></li>
<li><?php i18n("[Icon Item] Emit validChanged only if it actually changed");?></li>
<li><?php i18n("Suppress unnecessary scroll indicators if the flickable is a ListView with known orientation");?></li>
<li><?php i18n("[AppletInterface] Emit change signals for configurationRequired and -Reason");?></li>
<li><?php i18n("Use setSize() instead of setProperty width and height");?></li>
<li><?php i18n("Fixed an issue where PlasmaComponents Menu would appear with broken corners (bug 381799)");?></li>
<li><?php i18n("Fixed an issue where context menus would appear with broken corners (bug 381799)");?></li>
<li><?php i18n("API docs: add deprecation notice found in the git log");?></li>
<li><?php i18n("Synchronize the component with the one in Kirigami");?></li>
<li><?php i18n("Search all KF5 components as such instead as separate frameworks");?></li>
<li><?php i18n("Reduce spurious signal emissions (bug 382233)");?></li>
<li><?php i18n("Add signals indicating if a screen was added or removed");?></li>
<li><?php i18n("install Switch stuff");?></li>
<li><?php i18n("Don't rely in includes of includes");?></li>
<li><?php i18n("Optimize SortFilterModel role names");?></li>
<li><?php i18n("Remove DataModel::roleNameToId");?></li>
</ul>

<h3><?php i18n("Prison");?></h3>

<ul>
<li><?php i18n("Add Aztec code generator");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("determine QQC2 version at build time (bug 386289)");?></li>
<li><?php i18n("by default, keep the background invisible");?></li>
<li><?php i18n("add a background in ScrollView");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Faster UDevManager::devicesFromQuery");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Make it possible to crosscompile sonnet");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Add PKGUILD to bash syntax");?></li>
<li><?php i18n("JavaScript: include standard mime types");?></li>
<li><?php i18n("debchangelog: add Bionic Beaver");?></li>
<li><?php i18n("Update SQL (Oracle) syntax file (bug 386221)");?></li>
<li><?php i18n("SQL: move detecting comments before operators");?></li>
<li><?php i18n("crk.xml: added &lt;?xml&gt; header line");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.41");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.7");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
