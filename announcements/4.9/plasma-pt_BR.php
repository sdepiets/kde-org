<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "Espaço de Trabalho do Plasma – Principais Melhorias";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Also available in:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>

<p>
O KDE está feliz em anunciar a disponibilidade imediata da versão 4.9 do Espaço de Trabalho do Plasma para Desktops e Netbooks. Ocorreram muitas melhorias `as funcionalidades j´a existentes do Plasma e um número significante de novas funcionalidades também foi introduzido.
</p>
<h2>Gerenciador de Arquivos Dolphin</h2>
<p>
O poderoso gerenciador de arquivos Dolphin do KDE agora inclui botões para voltar e avançar e a função para renomear diretamente o arquivo está de volta. O Dolphin pode agora mostrar metadados como classificação, marcas, tamanho da imagem e arquivo, autor, data e mais, bem como agrupar e ordenar por propriedades dos metadados. O novo plugin Mercurial manipula o sistema de versões da maneira conveniente com suporte para o git, o SVN e o CVS, assim os usuários pode baixar, enviar e submeter alterações diretamente do gerenciador de arquivos. A Interface do Usuário do Dolphin teve diversas pequenas melhorias, incluindo um painel de Locais aprimorado, suporte à busca melhorado e sincronização com a localização do terminal.
<div align="center" class="screenshot">
<a href="screenshots/kde49-dolphin_.png"><img src="screenshots/kde49-dolphin_thumb.png" /></a>
</div>
</p>
<h2>Emulador de Terminal X do Konsole</h2>
<p>
O Konsole possui agora a habilidade de buscar por uma seleção de texto suado os Atalhos Web do KDE. Ele oferece a opção de contexto 'Mudar diretório para' quando uma pasta e solta na janela do Konsole. Os usuários tem um controle maior para organizar as janelas do terminal <strong>desacoplando abas</strong> e arrastando-as para criar uma nova janela com apenas aquela aba. As abas existentes pode ser clonadas em novas com o mesmo perfil. A visibilidade do menu de da barra de abas pode ser controlada ao iniciar o Kontole. Para aqueles acostumados a scripting, os títulos das abas podem ser mudados com uma sequência de escape.
<div align="center" class="screenshot">
<a href="screenshots/kde49-konsole1.png"><img src="screenshots/kde49-konsole1-cropped.png" /></a></div>
<div align="center" class="screenshot">
<a href="screenshots/kde49-konsole2.png"><img src="screenshots/kde49-konsole2-cropped.png" /></a></div>
</p>
<h2>Gerenciador de Janelas KWin</h2>
<p>
O Gerenciador de Janelas KWin do KDE presenciou muito trabalho. As melhorias incluem mudanças sutis como elevar janelas durante a alternância entre eles a ajuda para as Configurações específicas da janela, bem como mudanças mais visíveis como um KCM melhorado para a alternância em caixa e melhor desempenho para as janelas Wobbly. Existem mudanças que fazem com que o KWin lide melhor com as Atividades, incluindo a adição de regras de janelas relacionadas às Atividades. No período ocorreu um foco geral na melhoria da qualidade e desempenho do KWin.
<div align="center" class="screenshot">
<a href="screenshots/kde49-window-behaviour_settings.png"><img src="screenshots/kde49-window-behaviour_settings_thumb.png" /></a></div>
</p>
<h2>Atividades</h2>
<p>
As Atividades estão agora mais integradas por todo o Espaço de Trabalho. Arquivos podem ser ligados às Atividades no Dolphin, no Konqueror e na Visão de Pasta. A Visão de Pasta pode também mostrar somente os arquivos relacionados com uma Atividade na área de trabalho ou em um painel. Um KIO de Atividade é novo, e a criptografia para atividades privadas é agora possível.
<div align="center" class="screenshot">
<a href="screenshots/kde49-link-files-to-activities.png"><img src="screenshots/kde49-link-files-to-activities-cropped.png"/></a></div>
</p>
<p>
O Espaço de Trabalho introduziu o suporte ao MPRIS2, com o KMix tendo a capacidade de manipular streams e um motor de dados do Plasma para manipular este protocolo de controle de reprodutor de música. Estas mudanças amarram o suporte ao MPRIS2 no Juk e no Dragon, o reprodutor de vídeo e música do KDE.
</p>
<p>
Existem muitas outras pequenas mudanças no Espaço de Trabalho, incluindo diversos portes para o QML. O mini-reprodutor do Plasma inclui um diálogo de propriedades da faixa e melhor filtragem. O menu do Kickoff pode agora ser usado somente com o teclado. O plasmóide de Gerenciamento de Rede teve seu layout e usabilidade alterados. O widget de Transporte Público teve também mudanças consideráveis.
</p>

<h4>Instalando o Plasma</h4>
<?php
  include("boilerplate-pt_BR.inc");
?>

<h2>Também anunciado hoje:</h2>

<h2><a href="applications-pt_BR.php"><img src="images/applications.png" class="app-icon" alt="Os Aplicativos do KDE 4.9"/>Novidades e Melhorias nos Aplicativos do KDE</a></h2>
<p>
As novidades e melhorias nos Aplicativos do KDE lançados hoje incluem o Okular, o Kopete, o KDE PIM, os aplicativos educacionais e os jogos. Leia os detalhes no <a href="applications-pt_BR.php">'Anúncio dos Aplicativos do KDE'</a>
</p>
<h2><a href="platform-pt_BR.php"><img src="images/platform.png" class="app-icon" alt="A Plataforma de Desenvolvimento KDE 4.9"/> Plataforma KDE</a></h2>
<p>
O lançamento de hoje da Plataforma KDE inclui correções de erros, outras melhorias de qualidade, conectividade e preparação para o Framework 5
</p>

<?php
  include("footer.inc");
?>
