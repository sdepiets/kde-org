<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "Plataforma KDE Platform – Melhor Interoperabilidade, Desenvolvimento Mais Fácil";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Also available in:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>

<p>
O KDE orgulhosamente anuncia o lançamento da Plataforma KDE 4.9. A Plataforma KDE é a base para o Espaço de Trabalho do Plasma e os Aplicativos do KDE. A Equipe KDE está trabalhando atualmente na próxima iteração da Plataforma de Desenvolvimento do KDE denominada "Frameworks 5". Enquanto o Frameworks 5 será em grande parte compatível a nível de código fonte, ele será baseado no Qt5. Ele fornecerá mais granularidade, tornando mais fácil usar partes da plataforma de maneira seletiva e reduzindo as dependências consideravelmente. Como resultado, a Plataforma KDE está praticamente congelada. A maior parte do esforço na Plataforma será na correção de erros e melhoria de desempenho.
</p>
<p>
Tem havido progresso na separação do QGraphicsView do Plasma para criar um caminho para um Plasma puramente baseado em QML e melhorias posteriores no suporte ao Qt Quick nas bibliotecas do KDE e no Espaço de Trabalho do Plasma.
</p>
<p>
Algum trabalho de baixo nível foi feito nas bibliotecas do KDE na área de rede. O acesso aos compartilhamentos de rede se tornou muito mais rápido para todos os aplicativos do KDE. Conexões NFS, Samba e SSHFS e KIO não mais contarão itens na pasta, tornando-as mais rápidas. O protocolo HTTP foi agilizado evitando-se acessos desnecessários ao servidor. Isto é especialmente notado em aplicativos de rede como o Korganizer, que está cerca de 20% mais rápido devido a esta correção. Existe também um armazenamento melhorado de senhas para compartilhamentos na rede.
</p>
<p>
Muitos erros críticos foram corrigidos no Nepomuk e Soprano, tornando a pesquisa mais rápida e confiável para Aplicativos e o Espaço de Trabalho compilado na Plataforma KDE 4.9. A melhoria na performance e estabilidade foram os principais objetivos destes projetos para este lançamento.
</p>

<h4>Instalando a Plataforma de Desenvolvimento KDE</h4>
<?php
  include("boilerplate-pt_BR.inc");
?>

<h2>Também anunciado hoje:</h2>
<h2><a href="plasma-pt_BR.php"><img src="images/plasma.png" class="app-icon" alt="Espaço de Trabalho do Plasma 4.9" width="64" height="64" /> Espaço de Trabalho do Plasma – Principais Melhorias</a></h2>
<p>
Os destaques para o Espaço de Trabalho do Plasma incluem melhorias substanciais no Gerenciador de Arquivos Dolphin, no Emulador de Terminal X do Konsole, nas Atividades e no Gerenciador de Janelas KWin. Leia os detalhes no <a href="plasma-pt_BR.php">'Anúncio do Espaço de Trabalho do Plasma'.</a>
</p>
<h2><a href="applications-pt_BR.php"><img src="images/applications.png" class="app-icon" alt="Os Aplicativos do KDE 4.9"/>Novidades e Melhorias nos Aplicativos do KDE</a></h2>
<p>
As novidades e melhorias nos Aplicativos do KDE lançados hoje incluem o Okular, o Kopete, o KDE PIM, os aplicativos educacionais e os jogos. Leia os detalhes no <a href="applications-pt_BR.php">'Anúncio dos Aplicativos do KDE'</a>
</p>
<?php
  include("footer.inc");
?>
