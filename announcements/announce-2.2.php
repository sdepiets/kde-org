<?php
  $page_title = "Free KDE Desktop Ready for Enterprise Deployment";
  $site_root = "../";
  include "header.inc";
?>
<P>DATELINE AUGUST 15, 2001</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">Free KDE Desktop Ready for Enterprise Deployment</H3>
<P><STRONG>KDE Ships Leading Desktop with Advanced Web Browser and Development
Environment for Linux and Other UNIXes</STRONG></P>
<P>August 15, 2001 (The INTERNET).
The <A HREF="http://www.kde.org/">KDE
Project</A> today announced the immediate release of KDE 2.2,
a powerful and easy-to-use Internet-enabled desktop for Linux and other UNIXes.
Consistent with KDE's rapid and disciplined development pace, the
release of KDE 2.2 features an
<A HREF="http://www.kde.org/announcements/changelog2_1to2_2.html">impressive
catalog</A>
of speed/performance enhancements, feature additions and stability
improvements.  KDE 2.2 is available in
<A HREF="http://i18n.kde.org/teams/distributed.html">34 languages</A>
and ships with the core KDE libraries, the core
desktop environment, and over 100 applications from the other
base KDE packages (administration, multimedia, network, PIM,
development, etc.).
</P>
<P>
"The productivity tools and interface improvements in the new KDE
desktop provide a powerful client environment," said Kent Ferson, vice
president of <A HREF="http://www.compaq.com/">Compaq</A>'s
<A HREF="http://www.tru64unix.compaq.com/">Tru64</A> UNIX Systems
Division. "We're pleased to offer
KDE 2.2 on Tru64 UNIX and to enhance our Linux-Tru64 UNIX
<A HREF="http://www.compaq.com/affinity/">affinity</A>
capabilities for interoperability and application mobility. Compaq
acknowledges <A HREF="http://www.radar.tugraz.at/people/tom.html">Dr.
Tom Leitner</A> of <A HREF="http://www.TUGraz.at/">Graz
University of Technology</A> and the KDE
development team for their support in delivering this affinity capability."
</P>
<P>
KDE 2.2 will be complemented later this month by the stable release
of KOffice 1.1, a comprehensive, modular, component-based
suite of office productivity applications.  This
combination is the first to provide a complete
Open Source desktop and productivity environment for Linux/Unix.
</P>
<P>
KDE and all its components are available <EM><STRONG>for free</STRONG></EM>
under Open Source licenses from the KDE
<A HREF="http://download.kde.org/stable/2.2/src/">server</A>
and its <A HREF="http://www.kde.org/mirrors.html">mirrors</A> and can
also be obtained on <A HREF="http://www.kde.org/cdrom.html">CD-ROM</A>.
</P>
<P>
"Thanks to the high quality of the KDE 2 development framework and the
invaluable feedback of our users we are delivering a more polished,
better integrated and more feature-rich desktop experience in a relative
short time.  With the pending release of a stable KOffice this
month, KDE offers a complete high-quality desktop and development platform
without the costs and restrictions associated with proprietary
desktops," said Waldo Bastian, release manager for KDE 2.2 and a
<A HREF="http://www.suse.com/">SuSE</A> Labs developer.
</P>
<P>
Dave Richards, the System Administrator for
<A HREF="http://www.largo.com/">The City of
Largo, Florida</A> who recently spearheaded the
successful roll-out of KDE on 400 thin clients serving 800 users in
City offices, explained why the City selected KDE and Linux:
"All city terminals log into one big 'desktop'
system to get the KDE desktop.  Since uptime
of this server is so critical, we picked Linux.
KDE gave us an excellent presentation layer
with which to run all of the City applications
that are running on other servers (both Unix
and Windows)."
</P>
<P>
"It is important for enterprises to realize that the huge
cost savings they can obtain immediately upon conversion to KDE
do not require any sacrifice in terms of software available to their users,"
added Andreas Pour, Chairman of the KDE League.  "The
capabilities of Konqueror, the expansive array of
free software available for <A HREF="http://apps.kde.com/">KDE/Qt</A> and
the large number of
<A HREF="http://www.freshmeat.net/">other Open Source projects</A> will
solve most if not all problems natively.  For those applications only
available for Windows at this juncture, several
alternative products, both commercial and Open Source, exist:
running Windows terminal sessions in KDE, running Windows
applications natively under KDE/Linux, and providing remote desktop
sessions.  There is no reason
companies cannot today shed themselves of at least a substantial
portion of wasteful licensing fees for their desktop users."
</P>
<P>
<STRONG><EM>KDE 2:  The K Desktop Environment</EM></STRONG>.
<A NAME="Konqueror"></A><A HREF="http://konqueror.kde.org/">Konqueror</A>
is KDE 2's next-generation web browser, file manager and document viewer.
The standards-compliant Konqueror has a component-based architecture
which combines the features and functionality of Internet Explorer/Netscape
Communicator and Windows Explorer.
Konqueror supports the full gamut of current Internet technologies,
including JavaScript, Java, XML, HTML 4.0, CSS-1 and -2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications),
and Netscape Communicator plug-ins (for
<A HREF="http://macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash">Flash</A>,
<A HREF="http://www.real.com/player/index.html?src=010709realhome_1">RealAudio
and RealVideo</A>, and similar technologies; Konqueror also supports
some <A HREF="http://www.microsoft.com/com/tech/ActiveX.asp">ActiveX</A>
controls, such as
<A HREF="http://macromedia.com/software/shockwaveplayer/">Shockwave Player</A>.
Support for all these features is provided through KHTML, a KDE library
widget which is available to all KDE applications as either a widget
(using normal window parenting) or as a component (using the
<A HREF="#KParts">KParts</A> technology).
</P>
<P>
In addition, KDE offers seamless network transparency for accessing
or browsing files on Linux, NFS shares, MS Windows
SMB shares, HTTP pages, FTP directories, LDAP directories, digital
cameras and audio CDs.  The modular,
plug-in nature of KDE's file architecture makes it simple to add additional
protocols (such as IPX or WebDAV) to KDE, which would
then automatically be available to all KDE applications.
</P>
<P>
Besides the exceptional compliance with Internet and file-sharing standards
<A HREF="#Konqueror">mentioned above</A>, KDE 2 is a leader in
compliance with the available Linux desktop standards.
KWin, KDE's new
re-engineered window manager, complies to the new
<A HREF="http://www.freedesktop.org/standards/wm-spec.html">Window Manager
Specification</A>.
Konqueror and KDE comply with the <A
HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/kio/DESKTOP_ENTRY_STANDARD">Desktop
Entry Standard</A>.
Konqueror uses the
<A HREF="http://pyxml.sourceforge.net/topics/xbel/docs/html">XBEL</A>
standard for its bookmarks.
KDE 2 largely complies with the
<A HREF="http://www.newplanetsoftware.com/xdnd/">X Drag-and-Drop (XDND)
protocol</A> as well as with the
<A HREF="http://www.rzg.mpg.de/rzg/batch/NEC/sx4a_doc/g1ae04e/chap12.html">X11R6 session management protocol (XSMP)</A>.
</P>
<P>
The KDE 2.2 release features a number of enhancements which remove
any potential obstacles to widespread, productive enterprise adoption
of the free KDE desktop:
<UL>
<LI>up to 50% improvement in application startup time on GNU/Linux systems
using an experimental
<A HREF="http://www.research.att.com/~leonb/objprelink/">object file
pre-linking method</A>;</LI>
<LI>increased stability and capabilities of HTML rendering and JavaScript;</LI>
<LI>the addition of IMAP support (including SSL and TLS) to KDE's mail
client KMail;</LI>
<LI>a new plugin-based print architecture with integrated filter and
page layout capabilities (currently supports LPR,
CUPS, RLPR, external command, generic LPD and pseudo-printers such
as print-to-fax or print-to-email gateways);</LI>
<LI>enhanced startup speed and user feedback;</LI>
<LI>a number of new plugins for Konqueror, including a
<A HREF="http://babelfish.altavista.com/">Babelfish</A> translator,
an image gallery generator, an HTML validator and a web archiver;</LI>
<LI>native iCalendar support in KOrganizer, KDE's PIM tool; and</LI>
<LI>a new personalization wizard.</LI>
</UL>
</P>
<P>
<STRONG><EM>KDE 2:  The K Development Environment</EM></STRONG>.
KDE 2.2 offers developers a sophisticated IDE as well as a rich set
of major technological improvements over the critically acclaimed
KDE 1 series.
Chief among the technologies are
the <A HREF="#DCOP">Desktop COmmunication Protocol (DCOP)</A>, the
<A HREF="#KIO">I/O libraries (KIO)</A>, <A HREF="#KParts">the component
object model (KParts)</A>, an XML-based GUI class, and 
a standards-compliant HTML rendering engine (KHTML).
</P>
<P>
KDevelop is a leading Linux IDE
with numerous features for rapid application
development, including a GUI dialog builder, integrated debugging, project
management, documentation and translation facilities, built-in concurrent
development support, and much more.  This release includes a number of
new features, including a setup wizard, code navigation, a console, man
page support and colored text, as well as a number of new project
templates, including KControl modules, Kicker (panel) applets, kio-slaves,
Konqueror plugins and desktop themes.
</P>
<P>
<A NAME="KParts">KParts</A>, KDE 2's proven component object model, handles
all aspects of application embedding, such as positioning toolbars and inserting
the proper menus when the embedded component is activated or deactivated.
KParts can also interface with the KIO trader to locate available handlers for
specific mimetypes or services/protocols.
This technology is used extensively by the
<A HREF="http://www.koffice.org/">KOffice</A> suite and Konqueror.
</P>
<P>
<A NAME="KIO">KIO</A> implements application I/O in a separate
process to enable a
non-blocking GUI without the use of threads.
The class is network and protocol transparent
and hence can be used seamlessly to access HTTP, FTP, POP, IMAP,
NFS, SMB, LDAP and local files.
Moreover, its modular
and extensible design permits developers to "drop in" additional protocols,
such as WebDAV, which will then automatically be available to all KDE
applications.
KIO also implements a trader which can locate handlers
for specified mimetypes; these handlers can then be embedded within
the requesting application using the KParts technology.
</P>
<P>
<A NAME="DCOP">DCOP</A> is a client-to-client communications
protocol intermediated by a
server over the standard X11 ICE library.
The protocol supports both
message passing and remote procedure calls using an XML-RPC to DCOP "gateway".
Bindings for C, C++ and Python, as well as experimental Java bindings, are
available.
</P>
<P>
KDE also provides a number of language bindings.  In particular, KDE
kdejava provides full Java bindings for KDE and Qt, which look and
behave identically to a C++ version, including access to the
C++ signals/slots.
</P>
<P>
<H4>Installing Binary Packages</H4>
</P>
<P>
<EM>Binary Packages</EM>.
All major Linux distributors and some Unix distributors have provided
binary packages of KDE 2.2 for recent versions of their distribution.  Some
of these binary packages are available for free download under
<A HREF="http://download.kde.org/stable/2.2/">http://download.kde.org/stable/2.2/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution (if you cannot find a binary package for your distribution,
please read the <A HREF="http://dot.kde.org/986933826/">KDE Binary Package
Policy</A>).
</P>
<P>
<EM>Library Requirements</EM>.
The library requirements for a particular binary package vary with the
system on which the package was compiled.  Please bear in mind that
some binary packages may require a newer version of Qt and other libraries
than was included with the applicable distribution (e.g., LinuxDistro 8.0
may have shipped with qt-2.2.3 but the packages below may require
qt-2.3.x).  For general library requirements for KDE, please see the text at
<A HREF="#Source_Code-Library_Requirements">Source Code - Library
Requirements</A> above.
</P>
<P>
<A NAME="Package_Locations"><EM>Package Locations</EM></A>.
At the time of this release, pre-compiled packages are available for:
</P>
<P>
<UL>
  <LI><A HREF="http://www.caldera.com/">Caldera Systems</A></LI>
  <UL>
    <LI>OpenLinux-3.1:  <A HREF="http://download.kde.org/stable/2.2/Caldera/OpenLinux-3.1/RPMS/">Intel i386</A></LI>
  </UL>
  <LI><A HREF="http://www.conectiva.com/">Conectiva Linux</A> (<A HREF="http://download.kde.org/stable/2.2/Conectiva/7.0/README">README</A>)</LI>
  <UL>
    <LI>7.0:  <A HREF="http://download.kde.org/stable/2.2/Conectiva/7.0/i386/RPMS.main/">Intel i386</A></LI>
  </UL>
  <LI><A HREF="http://www.debian.org/">Debian GNU/Linux</A> (package "kde"):  <A HREF="ftp://ftp.debian.org/">ftp.debian.org</A>:  sid (devel) (see also <A HREF="http://http.us.debian.org/debian/pool/main/k/">here</A>)</LI>
  <LI><A HREF="http://www.FreeBSD.org/">FreeBSD</A> (<A HREF="http://download.kde.org/stable/2.2/FreeBSD/README">README</A>)</LI>
  <UL>
    <LI>Unpecified version:  <A HREF="http://download.kde.org/stable/2.2/FreeBSD/">Intel i386</A></LI>
  </UL>
  <LI><A HREF="http://www.ibm.com/servers/aix/products/aixos/">IBM AIX</A> (<EM>Note</EM>:  These are expected to be available in the coming week)</LI>
  <UL>
    <LI>4.3.3.0 or higher:  <A HREF="http://www-1.ibm.com/servers/aix/products/aixos/linux/download.html">PowerPC</A></LI>
  </UL>
  <LI><A HREF="http://www.linux-mandrake.com/en/">Linux-Mandrake</A> (<A HREF="http://download.kde.org/stable/2.2/Mandrake/README">README</A>)</LI>
  <UL>
    <LI>8.0:  <A HREF="http://download.kde.org/stable/2.2/Mandrake/8.0/i586/">Intel i586</A> (see also the <A HREF="http://download.kde.org/stable/2.2/Mandrake/8.0/noarch/">noarch</A> directory) and <A HREF="http://download.kde.org/stable/2.2/Mandrake/ppc/RPMS/">PowerPC</A> (see also the <A HREF="http://download.kde.org/stable/2.2/Mandrake/ppc/noarch/">noarch</A> directory)</LI>
    <LI>7.2:  <A HREF="http://download.kde.org/stable/2.2/Mandrake/7.2/i586/">Intel i586</A> (see also the <A HREF="http://download.kde.org/stable/2.2/Mandrake/7.2/noarch/">noarch</A> directory)</LI>
  </UL>
  <LI><A HREF="http://www.redhat.com/">RedHat Linux</A> (<A HREF="http://download.kde.org/stable/2.2/RedHat/README">README</A>):
  <UL>
    <LI>7.x:  <A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/i386/">Intel i386</A> (see also the <A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/non-kde/i386/">add-ons</A> and <A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/noarch/">noarch</A> directories), <A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/ia64/">HP/Intel IA-64</A> (see also the <A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/non-kde/ia64/">add-ons</A> and <A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/noarch/">noarch</A> directories), <A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/alpha/">Alpha</A> (see also the <A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/non-kde/alpha/">add-ons</A> and <A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/noarch/">noarch</A> directories) and <A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/s390/">IBM S390</A> (see also the <A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/noarch/">noarch</A> directory)</LI>
  </UL>
  <LI><A HREF="http://www.suse.com/">SuSE Linux</A> (<A HREF="http://download.kde.org/stable/2.2/SuSE/README">README</A>):
  <UL>
    <LI>7.2:  <A HREF="http://download.kde.org/stable/2.2/SuSE/i386/7.2/">Intel i386</A> and <A HREF="http://download.kde.org/stable/2.2/SuSE/ia64/7.2/">HP/Intel IA-64</A> (see also the <A HREF="http://download.kde.org/stable/2.2/SuSE/noarch/">noarch</A> directory)</LI>
    <LI>7.1:  <A HREF="http://download.kde.org/stable/2.2/SuSE/i386/7.1/">Intel i386</A>, <A HREF="http://download.kde.org/stable/2.2/SuSE/ppc/7.1/">PowerPC</A>, <A HREF="http://download.kde.org/stable/2.2/SuSE/sparc/7.1/">Sun Sparc</A> and <A HREF="http://download.kde.org/stable/2.2/SuSE/axp/7.1/">Alpha</A> (see also the <A HREF="http://download.kde.org/stable/2.2/SuSE/noarch/">noarch</A> directory)</LI>
    <LI>7.0:  <A HREF="http://download.kde.org/stable/2.2/SuSE/i386/7.0/">Intel i386</A>, <A HREF="http://download.kde.org/stable/2.2/SuSE/ppc/7.0/">PowerPC</A> and <A HREF="http://download.kde.org/stable/2.2/SuSE/s390/7.0/">IBM S390</A> (see also the <A HREF="http://download.kde.org/stable/2.2/SuSE/noarch/">noarch</A> directory)</LI>
    <LI>6.4:  <A HREF="http://download.kde.org/stable/2.2/SuSE/i386/6.4/">Intel i386</A> (see also the <A HREF="http://download.kde.org/stable/2.2/SuSE/noarch/">noarch</A> directory)</LI>
  </UL>
  <LI><A HREF="http://www.tru64unix.compaq.com/">Tru64 Systems</A> (<A HREF="http://download.kde.org/stable/2.2/Tru64/README.Tru64">README</A>)</LI>
  <UL>
    <LI>Tru64 4.0d, e, f and g and 5.x:  <A HREF="http://download.kde.org/stable/2.2/Tru64/">Alpha</A></LI>
  </UL>
</UL>
</P>
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages will become available over the
coming days and weeks.
</P>
<H4>Downloading and Compiling KDE 2.2</H4>
<P>
<A NAME="Source_Code-Library_Requirements"></A><EM>Library
Requirements</EM>.
KDE 2.2 requires qt-2.2.4, which is available in source code from Trolltech as
<A HREF="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.4.tar.gz">qt-x11-2.2.4.tar.gz</A>.  In addition, for SSL support, KDE 2.2 requires <A HREF="http://www.openssl.org/">OpenSSL</A> &gt;= 0.9.6x; versions 0.9.5x are not supported. For
Java support, KDE 2.2 requires a JVM &gt;= 1.3(?).  For Netscape plugin
support, KDE requires a recent version of
<A HREF="http://www.lesstif.org/">Lesstif</A> or Motif.  Searching
local documentation requires <A HREF="http://www.htdig.org/">htdig</A>.
Other special features, such as drag'n'drop audio CD ripping, require
other packages.
</P>
<P>
<EM>Compiler Requirements</EM>.
Please note that some components of
KDE 2.2 will not compile with older versions of
<A HREF="http://gcc.gnu.org/">gcc/egcs</A>, such as egcs-1.1.2 or
gcc-2.7.2.  At a minimum gcc-2.95-* is required.  In addition, some
components of KDE 2.2 (such as the multimedia backbone of KDE,
<A HREF="http://www.arts-project.org/">aRts</A>) will not compile with
<A HREF="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc 3.0</A> (the
problems are being corrected by the KDE and GCC teams).
</P>
<P>
<A NAME="Source_Code"></A><EM>Source Code/RPMs</EM>.
The complete source code for KDE 2.2 is available for free download at
<A HREF="http://download.kde.org/stable/2.2/src/">http://download.kde.org/stable/2.2/src/</A>
or in the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>.
Additionally, source rpms are available for the following distributions:
<UL>
  <LI><A HREF="http://download.kde.org/stable/2.2/Conectiva/7.0/SRPMS/">Conectiva Linux</A></LI>
  <LI><A HREF="http://download.kde.org/stable/2.2/Caldera/OpenLinux-3.1/SRPMS/">Caldera Systems</A></LI>
  <LI><A HREF="http://download.kde.org/stable/2.2/Mandrake/SRPMS/">Linux-Mandrake</A></LI>
  <LI><A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/SRPMS/">RedHat Linux</A> (see also the <A HREF="http://download.kde.org/stable/2.2/RedHat/7.x/non-kde/SRPMS/">add-ons</A> directory)</LI>
  <LI><A HREF="http://download.kde.org/stable/2.2/SuSE/SRPMS/">SuSE Linux</A></LI>
</UL>
</P>
<P>
<EM>Further Information</EM>.  For further
instructions on compiling and installing KDE 2.2, please consult
the <A HREF="http://www.kde.org/install-source.html">installation
instructions</A> and, if you should encounter problems, the
<A HREF="http://www.kde.org/compilationfaq.html">compilation FAQ</A>.  For
problems with source rpms, please contact the person listed in the .spec
file.
</P>
<P>
<H4>About KDE</H4>
</P>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide working over the Internet to create a sophisticated,
customizable and stable desktop environment employing a component-based,
network-transparent architecture.  KDE is working proof of the power of
the Open Source "Bazaar-style" software development model to create
first-rate technologies on par with and superior to even the most complex
commercial software.
</P>
<P>
Please visit the KDE family of web sites for the
<A HREF="http://www.kde.org/faq.html">KDE FAQ</A>,
<A HREF="http://www.kde.org/screenshots/kde2shots.html">screenshots</A>,
<A HREF="http://www.koffice.org/">KOffice information</A> and
<A HREF="http://developer.kde.org/documentation/kde2arch.html">developer
information</A>.
Much more <A HREF="http://www.kde.org/whatiskde/">information</A>
about KDE is available from KDE's
<A HREF="http://www.kde.org/family.html">family of web sites</A>.
</P>
<P>
<H4>Corporate KDE Sponsors</H4>
</P>
<P>
Besides the valuable and excellent efforts by the
<A HREF="http://www.kde.org/gallery/index.html">KDE developers</A>
themselves, significant support for KDE development has been provided by
<A HREF="http://www.mandrakesoft.com/">MandrakeSoft</A> and
<A HREF="http://www.suse.com/">SuSE</A>.  Thanks!
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
KDE, K Desktop Environment and KOffice are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix and Motif are registered trademarks of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
MS Windows, Internet Explorer, Windows Explorer and ActiveX are trademarks
or registered trademarks of Microsoft Corporation.
Shockwave is a trademark or registered trademark of Macromedia, Inc. in the United States and/or other countries.
Netscape and Netscape Communicator are trademarks or registered trademarks of Netscape Communications Corporation in the United States and other countries and JavaScript is a trademark of Netscape Communications Corporation.
Java is a trademark of Sun Microsystems, Inc.
Flash is a trademark or registered trademark of Macromedia, Inc. in the United States and/or other countries.
RealAudio and RealVideo are trademarks or registered trademarks of RealNetworks, Inc.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
</FONT>
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Eunice Kim<BR>
The Terpin Group<BR>
ekim@terpin.com<BR>
(1) 650 344 4944 ext. 105<BR>&nbsp;<BR>
Kurt Granroth<BR>
&#x67;&#x72;a&#110;rot&#x68;&#0064;&#107;d&#0101;&#46;or&#103;<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
KDE League, Inc.<BR>
p&#111;ur&#x40;&#x6b;d&#x65;&#46;&#x6f;&#x72;g<BR>
(1) 917 312 3122
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
&#x66;a&#x75;r&#101;&#x40;&#x6b;&#100;&#101;&#x2e;&#00111;&#114;&#103;<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Ralf Nolden<BR>
n&#111;l&#100;&#00101;n&#00064;&#x6b;d&#x65;&#0046;o&#x72;g<BR>
(49) 2421 502758
</TD></TR>
</TABLE>
<?php
  include "footer.inc"
?>
