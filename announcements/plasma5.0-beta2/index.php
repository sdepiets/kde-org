<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships Second Beta of Next Generation Plasma Workspace");
  $site_root = "../../";
  $release = 'plasma-4.97.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<a href="plasma50b2-main-wee.png"><img src="plasma50b2-main-wee.png" style="float: right; padding: 1ex; margin: 1ex; border: 0; background-image: none; " width="400" height="225" alt="<?php i18n("Plasma 5");?>" /></a>

<p>
<?php i18n("

June 10, 2014.
KDE today releases the <a
href='http://www.kde.org/announcements/plasma5.0-beta2/'>second
Beta version of the next-generation Plasma workspace</a>. The Plasma
team would like to ask the wider Free Software community to test this
release and give any feedback. Plasma 5 provides a visually
updated core desktop experience that will be easy and familiar for current users of
KDE workspaces or alternative Free Software or proprietary offerings. Plasma 5 is <a
href='http://techbase.kde.org/Schedules/Plasma/5.0_Release_Schedule'>planned
to be released</a> in early July.

");?></p>

<?php i18n("<h2>Changes in Plasma 5</h2>");?>

<p align="justify">
<?php i18n("Plasma 5 is an evolutionary release of the popular desktop workspace. While it aims at keeping existing workflows intact, there are some significant improvements worth mentioning.
");?>

<h2><?php i18n("For users");?></h2>

<p align="justify">
<?php i18n("
The new Breeze theme, which is still in its infancy, welcomes the user with a <strong>cleaner, modernized user interface</strong>, which improves contrast and reduces visual clutter throughout the workspace. Stronger reliance on typography, and vertical lists instead of horizontal ones go together with flatter UI elements and improved contrast to improve the ease of use. Breeze being a new artwork concept, it is only starting to show its face. A theme for the workspace components is already available, theming of traditional widgets is under way, and the work on a new icon theme has commenced. The migration to a fully Breeze-themed workspace will be a gradual one, with its first signs showing up in Plasma 5.0.
");?>

<p align="justify">
<?php i18n("
Plasma 5 brings a <strong>greater level of flexibility and consistency</strong> to core components of the desktop. The widget explorer, window and <a href=\"http://ivan.fomentgroup.org/blog/2014/06/07/the-future-of-activity-switching/\">activity switcher</a> now share a common interaction scheme through the use of the new-in-Plasma-5 Look and Feel package, which allows swapping these parts of the user experience in and out as a whole. The Plasma 5 workspace shell is able to load and switch between user experience for a given target device, introducing a truely convergent workspace shell. The workspace demonstrated in this pre-release is
Plasma Desktop. It represents an evolution of known desktop and laptop
paradigms. A <a href=\"http://plasma-active.org\">tablet-centric</a> and <a href=\"http://community.kde.org/Plasma/Plasma_Media_Center\">mediacenter</a> user experience are under development as alternatives. While Plasma 5 will feel familiar, users will notice a more
modern workspace.");?>
</p>

<a href="plasma50b2-lockscreen.png"><img src="plasma50b2-lockscreen-wee.png" style="float: left; padding: 1ex; margin: 1ex; border: 0; background-image: none; " width="400" height="225" alt="<?php i18n("New lockscreen in Plasma 5");?>" /></a>


<h2><?php i18n("For developers");?></h2>

<p align="justify">
<?php i18n("Since the <a href=\"https://www.kde.org/announcements/announce-plasma-next-beta1.php\">first beta release</a>, a wide range of
changes has been made. In the workspace components itself, more than 150 bugs have been fixed, with many fixes across the stack. Plasma 5.0 Beta 2 has increased the minimal Qt dependency to 5.3, in order to require a number of improvements which have been made upstream in Qt. Some missing functions, which were lacking proper solutions have been restored. The developer team has also settled on a version number scheme where the first release will be version 5.0 and the family is called Plasma 5.");?>
</p>

<p align="justify">
<?php i18n("Plasma 5 runs on top of a <strong>fully hardware-accelerated graphics stack</strong>, using Qt 5, QML 2 and an OpenGL(-ES) scenegraph to deliver graphics onto the users' screens. This allows the rendering to be faster, more efficient, less power-hungry and enables a smoother user experience by freeing up resources of the system processor. Plasma 5 completes the migration of the workspace to Qt Quick that has begun in earlier releases.
");?>

<p><?php i18n("Plasma 5 is the first complex codebase to transition
to <a href='http://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks
5</a>, which is a modular evolution of the KDE development platform
into leaner, less interdependent libraries.");?></p>



<br clear="all" />

<h2><?php i18n("Ready for testing, not production");?></h2>

<a href="plasma50b2-networks.png"><img src="plasma50b2-networks-wee.png" style="float: right; padding: 1ex; margin: 1ex;  border: 0; background-image: none; " width="400" height="291" alt="<?php i18n("Networking Setup in Plasma 5");?>" /></a>

<p><?php i18n("As a Beta release, this pre-release is not suitable
for production use. It is meant as a base for testing and gathering
feedback to ensure that the initial stable release of Plasma 5 in July
will be a smooth ride for everybody involved and lay a stable
foundation for future versions. Plasma 5 is intended for end users,
but will not provide feature parity with the latest 4.x release, which
will come in follow-up version.  The team is concentrating on the
core desktop features first, instead of trying to transplant every
single feature into the new workspaces. The feature set presented in
Plasma 5.0 will suffice for most users, though some might miss a button
here and there. This is not because the Plasma team wants to remove
features, but simply that not everything has been done yet. Of course,
everybody is encouraged to help bringing Plasma back to its original
feature set and beyond.");?></p>

<h2><?php i18n("Known issues");?></h2>

<p><?php i18n("<strong>Stability</strong> is not yet up to the level
where the developers want Plasma 5. With a substantial new toolkit
stack below come exciting new crashes and problems that need time to
be shaken out.");?></p>

<p><?php i18n("<strong>Performance</strong> of Plasma 5 is heavily
dependent on specific hardware and software configurations and usage
patterns. While it has great potential, it takes time to wrangle this
out of it and the underlying stack is not entirely ready for this
either. In some scenarios, Plasma 5 will display the buttery
smooth performance it is capable off - while at other times, it will
be hampered by various shortcomings. These can and will be addressed,
however, much is dependent on components like Qt, Mesa and hardware
drivers lower in the stack. Again, this will need time, as fixes made
elsewhere in the software stack might be released by the time the first Plasma 5
version becomes available.");?></p>

<p><?php i18n("<strong>Polish</strong> is a major benefit of Qt Quick 2, as
it allows seamless usage of OpenGL(-ES), much more precise positioning and
many other abilities. At the same time, the immaturity of Qt Quick
Controls, the brand new successor to the 15+ year old Qt Widgets
technology, brings some rough edges yet to be smoothed out.");?></p>

<!-- // Boilerplate again -->

<h2><?php i18n("Installing and providing feedback");?></h2>

<p><?php i18n("The easiest way to try it out is the <a
href='http://neon.blue-systems.com/live-iso/'>Neon 5 ISO</a>,
a live OS image updated with the latest builds straight from
source.");?></p>

<p><?php i18n("Some distributions have created, or are in the process
of creating, packages; for an overview of Beta 2 packages, see <a
href='http://community.kde.org/Plasma/Next/UnstablePackages'>our
unstable packages wiki page</a>");?></p>

<p><?php i18n("<a
href='http://download.kde.org/unstable/plasma/4.97.0/src/'>Source
download</a>.  You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions</a>.
Note that Plasma 5 does not co-install with Plasma 4.x, you will need
to uninstall older versions or install into a separate prefix.");?>
</p>

<p><?php i18n("You can provide feedback either via the <a
href='irc://#plasma@freenode.net'>#Plasma IRC channel</a>, <a
href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel
mailing list</a> or report issues via <a
href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided'>bugzilla</a>. Plasma
Next is also <a
href='http://forum.kde.org/viewforum.php?f=287'>discussed on the KDE
Forums</a>. Your feedback is greatly appreciated. If you like what the
team is doing, please let them know!");?></p>


<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
