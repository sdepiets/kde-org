<?php
  $page_title = "Announcing KDE 3.3";
  $site_root = "../";
  include "header.inc";
?>

<p>DATELINE AUGUST 19, 2004</p>

<h3 align="center">KDE Project Ships New Major Release Of Leading Open Source Desktop Environment</h3>

<img src="announce-3.3.jpeg" align="right" width="250" height="193" hspace="10" alt="Splash"/>


<p>August 19, 2004 (The Internet) - The <a href="http://www.kde.org">KDE Project</a> is pleased to announce the immediate availability of KDE 3.3, the fourth major release of the <a href="http://www.kde.org/awards">award-winning</a> KDE3 desktop platform. Over the past six months, hundreds of applications and desktop components have been enhanced by a community of developers, with a particular focus on integration of components.</p>

<p>Stephan Kulow, KDE Release Coordinator, said: "The desktop reached a quality hard to top in previous releases. Nevertheless, KDE 3.3 is a great improvement and will further strengthen KDE's position as the leading Free desktop environment."</p>

<p>Building upon previous releases, version 3.3 provides an integrated desktop and a comprehensive set of applications that make KDE the most usable and feature-rich environment of its kind. Responding to the needs of their users, KDE developers have implemented an impressive range of new features, as well as several new applications that make your desktop even more productive and pleasing to use. Of note in this release are the many integration efforts, linking applications across the desktop together to save you unecessary time and effort. Stephan Kulow added that "improvements in terms of usability, stability and integration are particularly noticeable in the KDE PIM (Personal Information Management) suite."</p>

<p>Reflecting its international team and focus, KDE 3.3 is currently available in over 50 different languages. Partial translations into 30 other languages are also available, many of which are expected to be completed during the KDE 3.3 life cycle. During the past six months, Qt gained increased support for Indic languages, and languages as diverse as Farsi and Frisian were added. With 89 different languages and full localization support, no other desktop is as ready to serve the needs of today's global community.</p>

<p>KDE has earned a reputation for quality and a comprehensive feature set among its global user base that is estimated to number in the millions. KDE is also proud to be the default user interface for several operating systems including Ark Linux, Conectiva, Knoppix, Linspire, Lycoris, Mandrake Linux, SUSE Linux, TurboLinux and Xandros. KDE is also  available as a part of Debian, Free/Open/NetBSD, Gentoo, Libranet, Fedora, Slackware and Solaris, among others. In addition to these operating system vendors, more and more companies are offering commercial support for KDE, some of which are listed in the <a href="http://enterprise.kde.org/bizdir/">business directory</a> of the <a href="http://enterprise.kde.org">KDE::Enterprise website</a>. With the release of KDE 3.3, the KDE Project looks to enhance and grow this ecosystem of users and supporters.</p>

<h3>Highlights At A Glance</h3>

<p>Some of the highlights in KDE 3.3 are listed below.</p>

<ul>
<li><em>New applications</em>
    <ul>
        <li>Kolourpaint, an easy-to-use replacement for KPaint</li>
	<li>KWordQuiz, KLatin and KTurtle expand the list of education packages for schools and families</li>
	<li>Kimagemapeditor and klinkstatus make life easier for web designers</li>
	<li>KSpell2, a new spellchecking library that fixes all of KSpell's shortcomings</li>
	<li>KThemeManager, a new control center module to globally handle KDE visual themes</li>
	<li>The Python bindings PyQT and PyKDE are now maintained with KDE in our CVS</li>
    </ul>
</li>
<li><em>Integration of desktop components</em>
    <ul>
	<li>Kontact is now integrated with <a href="http://kolab.org">Kolab</a>, KDE's groupware solution, and Kpilot</li>
        <li>Konqueror features better support for Instant Messenging contacts, with the capability to send files to IM contacts, and support for IM protocols (e.g. irc://)</li>
	<li>KMail can display the online presence of IM contacts</li>
	<li>Kopete can display a "now listening to" message from amaroK</li>
	<li>Juk has support for burning audio CDs with K3B</li>
    </ul>
</li>
<li><em>Many small desktop enhancements</em>
    <ul>
	<li>Tab improvements in Konqueror, including scrollwheel switching</li>
	<li>An RSS feed viewer sidebar in Konqueror</li>
	<li>A searchbar for Konqueror, compatible with all keyword: searches</li>
	<li>HTML composing, anti-spam/anti-virus wizards, automatic handling of mailing lists, improved support for cryptography and a handy quick search bar all make their way into KMail</li>
	<li>Kopete gains support for file transfers with Jabber</li>
	<li>Quanta Plus has a VPL (Visual Page Layout) mode to make editing even easier</li>
	<li>aRts gains jack support, and aKode, a new multithreaded audio decoding/encoding library to replace mpeglib</li>
	<li>KWin has new buttons to support its full features, including "always on top"</li>
	<li>Over 7,000 bugs have been closed, and over 2,000 wishes have been fulfilled</li>
	<li>Over 60,000 lines of code, documentation and other contributions have been committed to CVS</li>
    </ul>
</li>
</ul>

<p>
 For a more detailed list of improvements since the KDE 3.2 release, please refer to the <a href="http://www.kde.org/announcements/changelogs/changelog3_2_3to3_3.php">KDE 3.3 Change Log</a>.
</p>

<h3>Getting KDE 3.3</h3>

<p>KDE 3.3 can be downloaded over the Internet by visiting <a href="http://download.kde.org/stable/3.3/">download.kde.org</a>. Source code and vendor supplied and supported binary packages are available. KDE 3.3 will also be featured in upcoming releases of various Linux and UNIX operating systems and can be purchased separately on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>. For additional information on package availability and to read further release notes, please
visit the <a href="http://www.kde.org/info/3.3.php">KDE 3.3 information page</a>.</p>

<p>KDE is available at no cost and all source code, art and documentation is freely available
under Open Source licenses. Additional third-party KDE applications can be downloaded from <a href="http://kde-apps.org">kde-apps.org</a>. KDE 3.3 is also compatible with other Linux and UNIX software packages including popular Open Source applications such as Open Office and the Mozilla web browser.</p>

<h3>Supporting KDE</h3>

<p>KDE is supported through voluntary contributions of time, money and resources by individuals
and companies from around the world. To discover how you or your company can join in and help
support KDE please visit the <a href="http://www.kde.org/community/donations/">Supporting KDE</a> web page. There
may be more ways to support KDE than you imagine, and every bit of support helps make KDE
a better project and a better product for everyone. Communicate your support today with a monetary donation,
new hardware or a few hours of your time!</p>

<hr/>
<p style="font-size: 85%">
  <em>Press Release</em>:  Written by <a href="http://www.tomchance.org.uk">Tom Chance</a>.
  <br />
  <em>Release Coordinator</em>: <a href="mai&#108;to:&#x63;o&#111;lo&#64;&#0107;de&#0046;o&#114;&#x67;">Stephan Kulow</a>.
</p>

<p style="font-size: 85%">
  <em>Trademarks Notices.</em>

  KDE, K Desktop Environment and KOffice are trademarks of KDE e.V.
  Incorporated.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<h4>Press Contacts</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#x6d;a&#00105;lto:&#0105;n&#0102;&#111;-&#00097;f&#00114;ic&#x61;&#64;&#107;&#100;&#x65;&#046;&#x6f;&#114;&#103;">i&#110;&#x66;&#x6f;&#x2d;a&#x66;&#x72;&#x69;&#x63;&#97;&#0064;kde&#x2e;&#00111;rg</a><br />
</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="&#x6d;ai&#108;&#x74;&#x6f;&#x3a;info&#00045;a&#115;&#00105;a&#0064;&#107;&#100;e.org">info&#0045;as&#00105;a&#x40;&#x6b;&#100;&#101;&#00046;&#x6f;&#x72;&#103;</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="ma&#105;&#0108;t&#00111;:&#x69;&#110;f&#0111;-e&#117;r&#x6f;pe&#x40;kde&#46;o&#114;g">&#0105;nfo-&#x65;&#117;ro&#x70;e&#064;kd&#101;.org</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="m&#97;ilt&#x6f;&#58;info-&#x6e;o&#x72;t&#0104;&#97;&#00109;&#101;r&#105;c&#097;&#64;kd&#101;.&#x6f;r&#103;">i&#x6e;&#x66;o-no&#x72;&#x74;&#104;&#x61;&#x6d;e&#00114;&#00105;c&#x61;&#64;&#107;&#0100;&#x65;.o&#114;&#x67;</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="m&#00097;i&#x6c;&#0116;&#x6f;:inf&#0111;-oc&#x65;ania&#64;kde.&#x6f;r&#103;">&#x69;n&#x66;o&#0045;oc&#101;&#97;&#x6e;ia&#064;&#x6b;&#00100;e.o&#x72;g</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#x69;&#108;&#00116;o:inf&#x6f;&#45;&#115;&#x6f;&#00117;tha&#x6d;er&#105;ca&#00064;k&#100;&#x65;&#00046;&#x6f;r&#103;">i&#110;&#102;&#x6f;-sou&#0116;&#104;&#097;&#x6d;&#x65;rica&#x40;&#x6b;&#x64;&#x65;&#46;&#0111;rg</a><br />
</td>


</tr></table>

<?php
  include("footer.inc");
?>
