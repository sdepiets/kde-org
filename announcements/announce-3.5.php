<?php
  $page_title = "K Desktop Environment 3.5 Released";
  $site_root = "../";
  include "header.inc";
?>

<p>DATELINE NOVEMBER 29, 2005</p>

<p>
Many new features and refinements make <a href="http://www.kde.org">KDE</a> the complete free desktop environment.</p>

<img src="announce-3.5.jpeg" align="right" width="250" height="155" hspace="10" alt="Splash"/>

<p>
November 29, 2005 (The Internet) - <a href="http://www.kde.org">The KDE Project</a> is happy to announce a new major release of the <a href="http://www.kde.org/awards">award-winning</a> K Desktop Environment. Many features have been added or refined, making KDE the most complete, stable and integrated free desktop environment available.</p>

<p>
Notable changes include:</p>

<ul>
  <li>Konqueror is the second web browser to pass the Acid2 CSS test, ahead of Firefox and Internet Explorer</li>
  <li>Konqueror can also now free web pages from adverts with its ad-block feature</li>
  <li>SuperKaramba is included in KDE, providing well-integrated and easy-to-install widgets for the user's desktop</li>
  <li>Kopete has support for MSN and Yahoo! webcams</li>
  <li>The edutainment module has three new applications (KGeography, Kanagram and blinKen), and has seen huge improvements in Kalzium</li>
</ul>

<p>
For a more complete log of changes, complete with screenshots and explanations, see <a href="http://www.kde.org/announcements/visualguide-3.5.php">the KDE 3.5 visual guide to new features</a>.</p>

<p>
Stephan Kulow, KDE Release Coordinator, said: "The improvements made in the past year show how mature the KDE Project is. KDE is the most powerful desktop environment and development platform in the market. With huge changes expected in KDE 4, our next release, KDE 3.5 should provide users with the perfect productivity platform for the next couple of years."</p>

<h2>Getting KDE 3.5</h2>

<p>
Full information on how to download and install KDE 3.5 is available on 
our official website at <a href="http://www.kde.org/info/3.5.php">http://www.kde.org/info</a>. Being free and 
open source software, it is available for download at no cost. If you 
use a major Linux distribution then precompiled packages may be 
available from your distributions website or from 
<a href="http://download.kde.org/">http://download.kde.org</a>. The source code can also be downloaded from 
there. If you prefer to build KDE from source you should consider using 
<a href="http://developer.kde.org/build/konstruct/">Konstruct</a>, a tool that 
automatically downloads, configures and builds KDE 3.5 for you.
</p>

<p>
Many more KDE applications are freely available from <a href="http://www.kde-apps.org/">KDE-Apps.org</a> and different look and feel improvements can be downloaded from <a href="http://www.kde-look.org/">KDE-Look.org</a>.
</p>

<h2>Supporting KDE</h2>

<p>
KDE is an open source project that exists and grows only because of the 
help of many volunteers that donate their time and effort. KDE 
is always looking for new volunteers and contributions, whether its 
help with coding, bug fixing or reporting, writing documentation, 
translations, promotion, money, etc. All contributions are gratefully 
appreciated and eagerly accepted. Please read through the <a href="http://www.kde.org/community/donations/">Supporting 
KDE page</a> for further information. <br />
We look forward to hear from you soon!
</p>

<h2>About The KDE Project</h2>

<p align="justify">
  The KDE project consists of hundreds of developers, translators, artists and other contributors worldwide collaborating over the Internet. The community creates and freely distributes a stable, integrated and free desktop and office environment. KDE provides a flexible, component-based, network-transparent architecture and powerful development tools, offering an outstanding development platform. Reflecting its international team and focus, KDE 3.5 is currently available in over 80 different languages.</p>
<p align="justify">
 KDE, which is based on Qt technology from Trolltech, is working proof that the Open Source "Bazaar-style" software development model can yield first-rate technologies on par with and superior to even the most complex commercial software.
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<h2>Press Contacts</h2>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>


</tr></table>

<?php

  include("footer.inc");
?>
