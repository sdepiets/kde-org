<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.72.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.72.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>


<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
July 04, 2020. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.72.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("KDAV : New Module");?></h3>

<p>DAV protocol implementation with KJobs.</p>

<p>Calendars and todos are supported, using either GroupDAV
or CalDAV, and contacts are supported using GroupDAV or
CardDAV.</p>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("[Indexers] Ignore name-based mimetype for initial indexing decisions (bug 422085)");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Expose the service advertisement data of a device");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Fixed three 16px icons for application-x-... to be more pixel-perfect (bug 423727)");?></li>
<li><?php i18n("Add support for &gt;200% scale for icons which should stay monochrome");?></li>
<li><?php i18n("Cut out center of window-close-symbolic");?></li>
<li><?php i18n("cervisia app and action icon update");?></li>
<li><?php i18n("Add README section about the webfont");?></li>
<li><?php i18n("Use grunt-webfonts instead of grunt-webfont and disable generation for symbolic linked icons");?></li>
<li><?php i18n("add kup icon from kup repo");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Remove support for png2ico");?></li>
<li><?php i18n("Deal with Qt's CMake code modifying CMAKE_SHARED_LIBRARY_SUFFIX");?></li>
<li><?php i18n("Add FindTaglib find module");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Support \"repo_id\" in metainfo.yaml to override guessed repo name");?></li>
</ul>

<h3><?php i18n("KCalendarCore");?></h3>

<ul>
<li><?php i18n("Check for write error in save() if the disk is full (bug 370708)");?></li>
<li><?php i18n("Correct icon names for recurring to-dos");?></li>
<li><?php i18n("Fix serialization of recurring to-do's start date (bug 345565)");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Fix changed signal for plugin selector");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("Add some \n otherwise text can be very big in messagebox");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Also pass locationType to KConfigSkeleton when construction from group");?></li>
<li><?php i18n("Make \"Switch Application Language...\" text more consistent");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Make \"Switch Application Language...\" text more consistent");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KRandom::random -&gt; QRandomGenerator::global()");?></li>
<li><?php i18n("Deprecate KRandom::random");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("Add missing declaration of environ, otherwise available only on GNU");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Use consistent style for FDL notice (bug 423211)");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Adapt kfilemetadata to \"audio/x-speex+ogg\" as recently changed in shared-mime-info");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Also add quotes around rich text &lt;filename&gt; tagged text");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Drop assignment in resetPalette");?></li>
<li><?php i18n("Return QPalette() if we have no custom palette");?></li>
<li><?php i18n("Respect QIcon::fallbackSearchpaths() (bug 405522)");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("[kcookiejar] Fix reading \"Accept For Session\" cookie setting (bug 386325)");?></li>
<li><?php i18n("KDirModel: fix hasChildren() regression for trees with hidden files, symlinks, pipes and character devices (bug 419434)");?></li>
<li><?php i18n("OpenUrlJob: fix support for shell scripts with a space in the filename (bug 423645)");?></li>
<li><?php i18n("Add webshortcut for DeepL and ArchWiki");?></li>
<li><?php i18n("[KFilePlacesModel] Show AFC (Apple File Conduit) devices");?></li>
<li><?php i18n("Also encode space characters for webshortcut URLs (bug 423255)");?></li>
<li><?php i18n("[KDirOperator] Actually enable dirHighlighting by default");?></li>
<li><?php i18n("[KDirOperator] Highlight the previous dir when going back/up (bug 422748)");?></li>
<li><?php i18n("[Trash] Handle ENOENT error when renaming files (bug 419069)");?></li>
<li><?php i18n("File ioslave: set nano sec timestamp when copying (bug 411537)");?></li>
<li><?php i18n("Adjust URLs for Qwant searchproviders (bug 423156)");?></li>
<li><?php i18n("File ioslave: Add support for reflink copying (bug 326880)");?></li>
<li><?php i18n("Fix setting of default shortcut in webshortcuts KCM (bug 423154)");?></li>
<li><?php i18n("Ensure readability of webshortcuts KCM by setting minimum width (bug 423153)");?></li>
<li><?php i18n("FileSystemFreeSpaceJob: emit error if the kioslave didn't provide the metadata");?></li>
<li><?php i18n("[Trash] Remove trashinfo files referencing files/dirs that don't exist (bug 422189)");?></li>
<li><?php i18n("kio_http: Guess the mime-type ourselves if server returns application/octet-stream");?></li>
<li><?php i18n("Deprecate totalFiles and totalDirs signals, not emitted");?></li>
<li><?php i18n("Fix reloading of new web shortcuts (bug 391243)");?></li>
<li><?php i18n("kio_http: Fix status of rename with overwriting enabled");?></li>
<li><?php i18n("Do not interpret name as hidden file or file path (bug 407944)");?></li>
<li><?php i18n("Do not display deleted/hidden webshortcuts (bug 412774)");?></li>
<li><?php i18n("Fix crash when deleting entry (bug 412774)");?></li>
<li><?php i18n("Do not allow to assign existing shortcuts");?></li>
<li><?php i18n("[kdiroperator] Use better tooltips for back and forward actions (bug 422753)");?></li>
<li><?php i18n("[BatchRenameJob] Use KJob::Items when reporting the progress info (bug 422098)");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("[aboutpage] Use OverlaySheet for license text");?></li>
<li><?php i18n("fix collapsed mode icons");?></li>
<li><?php i18n("Use light separators for DefaultListItemBackground");?></li>
<li><?php i18n("Add weight property to Separator");?></li>
<li><?php i18n("[overlaysheet] Avoid fractional height for contentLayout (bug 422965)");?></li>
<li><?php i18n("Fix pageStack.layers docs");?></li>
<li><?php i18n("Reintroduce layer support to Page.isCurrentPage");?></li>
<li><?php i18n("support icon color");?></li>
<li><?php i18n("use the internal component MnemonicLabel");?></li>
<li><?php i18n("Reduce global toolbar left padding when title is not used");?></li>
<li><?php i18n("Only set implicit{Width,Height} for Separator");?></li>
<li><?php i18n("Update KF5Kirigami2Macros.cmake to use https with git repo in order to avoid error when trying to access");?></li>
<li><?php i18n("Fix: avatar loading");?></li>
<li><?php i18n("Make PageRouterAttached#router WRITE-able");?></li>
<li><?php i18n("Fix OverlaySheet closing when clicking inside layout (bug 421848)");?></li>
<li><?php i18n("Add missing id to GlobalDrawerActionItem in GlobalDrawer");?></li>
<li><?php i18n("Fix OverlaySheet being too tall");?></li>
<li><?php i18n("Fix PlaceholderMessage code example");?></li>
<li><?php i18n("render border at the bottom of groups");?></li>
<li><?php i18n("use an LSH component");?></li>
<li><?php i18n("Add background to headers");?></li>
<li><?php i18n("Better collapsing handling");?></li>
<li><?php i18n("Better presentation for list header items");?></li>
<li><?php i18n("Add bold property to BasicListItem");?></li>
<li><?php i18n("Fix Kirigami.Units.devicePixelRatio=1.3 when it should be 1.0 at 96dpi");?></li>
<li><?php i18n("Hide OverlayDrawer handle when not interactive");?></li>
<li><?php i18n("Adjust ActionToolbarLayoutDetails calculations to make better use of screen real estate");?></li>
<li><?php i18n("ContextDrawerActionItem: Prefer text property over tooltip");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("Integrate the KJob::Unit::Items (bug 422098)");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Fix crash when using KJSContext::interpreter");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Move explanatory text from sheet header to list header");?></li>
<li><?php i18n("Fix paths for install script and uncompression");?></li>
<li><?php i18n("Hide the ShadowRectangle for non-loaded previews (bug 422048)");?></li>
<li><?php i18n("Don't allow content to overflow in the grid delegates (bug 422476)");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Don't use notifybysnore.h on MSYS2");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Deprecate PartSelectEvent and co");?></li>
</ul>

<h3><?php i18n("KQuickCharts");?></h3>

<ul>
<li><?php i18n("Elide value Label of LegendDelegate when there isn't enough width");?></li>
<li><?php i18n("Fix for error \"C1059: non constant expression ...\"");?></li>
<li><?php i18n("Account for line width when bounds checking");?></li>
<li><?php i18n("Don't use fwidth when rendering line chart lines");?></li>
<li><?php i18n("Rewrite removeValueSource so it doesn't use destroyed QObjects");?></li>
<li><?php i18n("Use insertValueSource in Chart::appendSource");?></li>
<li><?php i18n("Properly initialise ModelHistorySource::{m_row,m_maximumHistory}");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Fix RunnerContextTest to not assume presence of .bashrc");?></li>
<li><?php i18n("Use embedded JSON metadata for binary plugins &amp; custom for D-Bus plugins");?></li>
<li><?php i18n("Emit queryFinished when all jobs for current query have finished (bug 422579)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Make \"goto line\" work backwards (bug 411165)");?></li>
<li><?php i18n("fix crash on view deletion if ranges are still alive (bug 422546)");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Introduce three new methods that return all \"entries\" in a folder");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Fix KTimeComboBox for locales with unusual characters in formats (bug 420468)");?></li>
<li><?php i18n("KPageView: remove invisible pixmap on right side of header");?></li>
<li><?php i18n("KTitleWidget: move from QPixmap property to QIcon property");?></li>
<li><?php i18n("Deprecate some KMultiTabBarButton/KMultiTabBarTab API using QPixmap");?></li>
<li><?php i18n("KMultiTabBarTab: make styleoption state logic follow QToolButton even more");?></li>
<li><?php i18n("KMultiTabBar: do not display checked buttons in QStyle::State_Sunken");?></li>
<li><?php i18n("[KCharSelect] Initially give focus to the search lineedit");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("[xcb] Send correctly scaled icon geometry (bug 407458)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Use kcm_users instead of user_manager");?></li>
<li><?php i18n("Use new KTitleWidget::icon/iconSize API");?></li>
<li><?php i18n("Move \"Switch Application Language\" to Settings menu (bug 177856)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Show clearer warning if the requested KCM could not be found");?></li>
<li><?php i18n("[spinbox] Don't use QQC2 items when we should use PlasmaComponents (bug 423445)");?></li>
<li><?php i18n("Plasma components: restore lost color control of TabButton label");?></li>
<li><?php i18n("Introduce PlaceholderMessage");?></li>
<li><?php i18n("[calendar] Reduce month label's size");?></li>
<li><?php i18n("[ExpandableListItem] Use same logic for action and arrow button visibility");?></li>
<li><?php i18n("[Dialog Shadows] Port to KWindowSystem shadows API (bug 416640)");?></li>
<li><?php i18n("Symlink widgets/plasmoidheading.svgz in breeze light/dark");?></li>
<li><?php i18n("Fix Kirigami.Units.devicePixelRatio=1.3 when it should be 1.0 at 96dpi");?></li>
<li><?php i18n("Add property to access the ExpandableListItem loader's item");?></li>
</ul>

<h3><?php i18n("Prison");?></h3>

<ul>
<li><?php i18n("Deprecate AbstractBarcode::minimumSize() also for the compiler");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Explicitly use Kirigami units instead of implicitly using Plasma units");?></li>
<li><?php i18n("Port job dialogs to Kirigami.PlaceholderMessage");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Set selectByMouse to true for SpinBox");?></li>
<li><?php i18n("Fix menu icons becoming blurry with some font sizes (bug 423236)");?></li>
<li><?php i18n("Prevent delegates and scrollbar overlapping in combobox popups");?></li>
<li><?php i18n("Fix Slider vertical implicitWidth and a binding loop");?></li>
<li><?php i18n("Make ToolTips more consistent with Breeze widget style tooltips");?></li>
<li><?php i18n("Set editable to true by default for SpinBox");?></li>
<li><?php i18n("Fix Connections warnings in Menu.qml");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Enable UDisks2 backend on FreeBSD unconditionally");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Fix \"Using QCharRef with an index pointing outside the valid range of a QString\"");?></li>
<li><?php i18n("Restore default auto-detect behavior");?></li>
<li><?php i18n("Fix default language (bug 398245)");?></li>
</ul>

<h3><?php i18n("Syndication");?></h3>

<ul>
<li><?php i18n("Fix a couple BSD-2-Clause license headers");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("CMake: Updates for CMake 3.18");?></li>
<li><?php i18n("Add Snort/Suricata language highlighting");?></li>
<li><?php i18n("Add YARA language");?></li>
<li><?php i18n("remove deprecated binary json in favor of cbor");?></li>
<li><?php i18n("JavaScript/TypeScript: highlight tags in templates");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.72");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.12");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
