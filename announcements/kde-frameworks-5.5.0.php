<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.5.0");
  $site_root = "../";
  $release = '5.5.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
December 11, 2014. KDE today announces the release
of KDE Frameworks 5.5.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("

<h3>Attica</h3>

<ul>
<li>Use all of QT_PLUGIN_PATH paths rather than just QLibraryInfo path to look for plugins</li>
</ul>

<h3>KActivities</h3>

<ul>
<li>Fix plugin loading with KDE_INSTALL_USE_QT_SYS_PATHS ON</li>
</ul>

<h3>KCMUtils</h3>

<ul>
<li>Restore KStandardGuiItems to get consistent icons and tooltips</li>
</ul>

<h3>KCodecs</h3>

<ul>
<li>Introduce KEmailAddress class for email validation</li>
<li>Use more robust implementation of MIME codecs taken from the KMime library</li>
<li>Add KCodecs::encodeRFC2047String()</li>
</ul>

<h3>KCompletion</h3>

<ul>
<li>Fix PageUp/Down actions in the completion popup box</li>
</ul>

<h3>KCoreAddons</h3>

<ul>
<li>Add KTextToHTML class for plaintext->HTML conversion</li>
<li>Add KPluginMetaData::metaDataFileName()</li>
<li>Allow to read KPluginMetaData from .desktop files</li>
<li>Kdelibs4Migration now gives priority to distro-provided KDE4_DEFAULT_HOME</li>
</ul>

<h3>KDeclarative</h3>

<ul>
<li>Use Qt's method of blocking for component completion rather than our own</li>
<li>Make it possible to delay initialization of object incubated from QmlObject</li>
<li>Add guard when trying to access root object before component is complete</li>
</ul>

<h3>KEmoticons</h3>

<ul>
<li>Add KEmoticonsIntegrationPlugin for KTextToHTML from KCoreAddons</li>
</ul>

<h3>KHTML</h3>

<ul>
<li>A number of forward-ported fixes from kdelibs, no API changes.</li>
</ul>

<h3>KIO</h3>

<ul>
<li>Fix Size columns being empty in the KFileWidget detailed views</li>
<li>Do not drop ASN passed to KRun when executing desktop files</li>
<li>Fix passing of DESKTOP_STARTUP_ID to child process in kioexec</li>
<li>Fix compilation with Qt 5.2, which also fixes a race condition</li>
<li>KFileItem: cleanup overlay icon usage    </li>
<li>Implement back/forward side mouse buttons to navigate in the history</li>
<li>Allow user to cancel out of the certificate accept duration dialog box.</li>
</ul>

<h3>KJobWidgets</h3>

<ul>
<li>Fix compilation with Qt 5.2.0</li>
</ul>

<h3>KNewStuff</h3>

<ul>
<li>Also allow absolute filepaths for configfile parameter.</li>
<li>Fix compilation on Windows</li>
</ul>

<h3>KNotifications</h3>

<ul>
<li>Make KNotificationPlugin a public class</li>
<li>KPassivePopup - Set default hide delay</li>
</ul>

<h3>KRunner</h3>

<ul>
<li>Add a simple cli tool to run a query on all runners</li>
</ul>

<h3>KService</h3>

<ul>
<li>Fix KPluginTrader::query() for old JSON</li>
<li>Deprecate kservice_desktop_to_json for kcoreaddons_desktop_to_json</li>
<li>Implement KPluginTrader::query() using KPluginLoader::findPlugins()</li>
<li>Fix KPluginInfo::entryPath() being empty when not loaded from .desktop</li>
</ul>

<h3>KTextEditor</h3>

<ul>
<li>Fix bug #340212: incorrect soft-tabs alignment after beginning-of-line</li>
<li>Add libgit2 compile-time check for threads support</li>
</ul>

<h3>KWidgetsAddons</h3>

<ul>
<li>Add class KSplitterCollapserButton, a button which appears on the side of</li>
  a splitter handle and allows easy collapsing of the widget on the opposite side
<li>Support monochrome icon themes (such as breeze)</li>
</ul>

<h3>KWindowSystem</h3>

<ul>
<li>Add KStartupInfo::createNewStartupIdForTimestamp</li>
<li>Add support for more multimedia keys</li>
<li>Add support for initial mapping state of WM_HINTS</li>
<li>Drop incorrect warnings when using KXMessages without QX11Info</li>
</ul>

<h3>Plasma Framework</h3>

<ul>
<li>Fix compilation with Qt 5.2.0</li>
<li>Fix the platformstatus kded module</li>
<li>Migrate BusyIndicator, ProgressBar to QtQuick.Controls</li>
<li>Add thumbnailAvailable property to PlasmaCore.WindowThumbnail</li>
</ul>

<h3>Solid</h3>

<ul>
<li>Fix warning: No such signal org::freedesktop::UPower::Device...</li>
</ul>

<h3>Extra cmake modules</h3>

<ul>
<li>Set CMAKE_INSTALL_SYSCONFDIR to /etc when CMAKE_INSTALL_PREFIX is /usr (instead of /usr/etc)</li>
<li>Enable -D_USE_MATH_DEFINES on Windows</li>
</ul>

<h3>Frameworkintegration</h3>

<ul>
<li>Implement standardButtonText().</li>
<li>Fix restoring the view mode and sizes in the file dialog</li>
</ul>

");?>

<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php
//download directory now misses last bugfix version
$downloadDir = split("\.", $release)[0] . "." . split("\.", $release)[1];

print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/Attic/frameworks/%1/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%2.php'>KDE Frameworks %1 Info Page</a>.", $downloadDir, $release);?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.2");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://freenode.net/kde-devel");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
