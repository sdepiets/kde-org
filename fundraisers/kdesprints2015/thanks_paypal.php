<?php
    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Donation received"
    ]);

    require('../../aether/header.php');
    $site_root = "../../";
?>

<main class="container">

<h1>Donation received</h1>

<h2>Thank you!</h2>

<p>
Thank you very much for your donation to the KDE Sprints 2015 fundraiser!<br>
Don't forget to write us an <a href="mailto:fundraiser@kde.org">email</a> if you want a memorable thank you from the Randa Meetings. Please include your name, full mailing address and the amount you donated in the mail.
<br>
<br>
You can see your donation on the <a href="index.php">the KDE Sprints 2015 fundraiser page</a>.
</p>

</main>
<?php
  require('../../aether/footer.php');
