<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>KDE - End of Year 2017 Fundraising</title>
  <meta name="description" content="End of Year 2017 Fundraising - KDE Community">
  <link rel="shortcut icon" href="static/images/kde-mini.png" />
  <link rel="stylesheet" href="static/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="static/css/styles.css">
  <link rel="stylesheet" href="static/css/progress.css">
  <link rel="stylesheet" href="static/css/countdown.css">
  <link rel="stylesheet" href="static/assets/flat-icon/flaticon.css">
</head>

<body>
  <div class="main-wrapper">
    <header class="header header--bg">
      <div class="container-fluid">
        <img src="static/images/kde-mini.png" alt="KDE Logo" style="padding-top: 10px;">
        <div class="header__content text-center">
          <h1 class="header__content__title">KDE Powers You - You Can Power KDE, Too!</h1>
          <p class="header__content__paragraph">Technology and software developed by KDE powers devices and empowers people all around the world. By donating to our end-of-year fundraiser, you will play a part in recharging KDE!</p>
          <!-- <a class="button button--hover" data-toggle="modal" data-target="#myModal">Donate!</a> -->
        </div>
      </div>
    </header>
    <!-- Introduction_and_Status -->
    <section>
      <div class="container-fluid">
        <!-- Introduction -->
        <div class="text-left">
          <p class="page-section__paragraph">
            As you probably already know, KDE software is free for everyone to use and modify. We would like to keep it that way.<br/> Help us build a world in which everyone has control over their digital life and enjoys freedom and privacy.
          </p>
          <p class="page-section__paragraph">
            If you support our campaign, you will directly impact the future of KDE. Donations are also a great way to show gratitude to your favorite developers. <br/>And if you‘ve always wanted to contribute to FOSS, but never had the time, a donation
            also counts as a contribution!
          </p>
        </div>
        <div class="text-center" >
          <strong class="page-section__paragraph" style="margin-left:0px; margin-right:0px;">
            <h2>But wait...what are we going to do with your money?</h2>
            <br />
          </strong>
        </div>
          <div>
          <p class="page-section__paragraph">
            <strong>Power community growth and KDE promotion</strong><br /> You will contribute to the organization of Akademy, our biggest community event. Thanks to you, our community members will be able to travel to conferences and give talks about
            KDE software, or set up booths at tech events around the globe to present our technology.
          </p>
          <p class="page-section__paragraph">
            <strong>Empower people by teaching and mentoring them</strong><br /> Your donations will make it easier for us to organize events like Season of KDE, where we mentor students and new FOSS contributors.
          </p>
          <p class="page-section__paragraph">
            <strong>Boost our developers to make even better software</strong><br /> Is Plasma your favorite desktop environment? What about other software developed by KDE - do you use Marble Maps, Kdenlive, Dolphin, digiKam, or KMail? Your donations will
            help us make that software better. We'll be able to work on it together during our developer sprints, such as Randa Meetings.
          </p>
          <p class="page-section__paragraph">
            <strong>Power our websites and infrastructure</strong><br /> With your help, we will be able to pay for hosting and administration expenses, as well as finish the redesign project of our main KDE website.
          </p>
          <strong class="page-section__paragraph" >Notes</strong>
          <ul>
            <li class="page-section__paragraph">
              <p >
                PayPal is the default transaction method for this fundraiser. If you prefer to use international bank transfers please see <a href="https://www.kde.org/community/donations/others.php#moneytransfer">this page.</a>
              </p>
            </li>
            <li class="page-section__paragraph">
              <p>
                If there are any problems with your donation, or if you have any additional questions, contact us at <a href="mailto:kde-ev-campaign@kde.org?subject=KDE End of Year 2017 Fundraiser">kde-ev-campaign@kde.org</a>. Put "KDE End of Year 2017 Fundraiser" in the subject of your email.
              </p>
            </li>
            <li class="page-section__paragraph">
              <p>
                Your donation can be anonymous. You can set this in the Donate dialog by answering "No" to "Show my name on the donors list".
              </p>
            </li>
            <li class="page-section__paragraph">
              <p>
                Multiple donations from the same account will be listed and treated as separate donations.
              </p>
            </li>
          </ul>
        </div>
        <!-- End of Introduction -->
        <?php
          require_once("www_config.php");

          $countStmt = $dbConnection->prepare("SELECT COUNT(*) FROM endofyear2017donations ORDER BY CREATED_AT DESC;");
          $countStmt->execute();
          $n = $countStmt->fetchColumn();

          $res = $dbConnection->prepare("SELECT *, UNIX_TIMESTAMP(CREATED_AT) AS date_t FROM endofyear2017donations ORDER BY CREATED_AT DESC;");
          $res->execute();
          $total = 0;
          $table = "";
          while ($row = $res->fetch()) {
              $name = htmlspecialchars($row["donor_name"]);
              if ($name == "") {
                  $name = "<i>Anonymous donation</i>";
              }
              $total += $row["amount"];

              $table.="<tr>";
              $table.="<td>".$n."</td>";
              $table.="<td>".date("jS F Y", $row["date_t"])."</td>";
              $table.="<td>&euro;".number_format($row["amount"], 2)."</td>";
              $table.="<td>".$name."</td>";
              $table.="</tr>";
              $n--;
          }
          $dbConnection = null;
          $goal_fmt=number_format(20000);
          $goal=20000;
          $percent=round($total * 100 / $goal);
          $percent=min($percent, 100);
          ?>
        <div class="text-center ">
          <h2 class="page-section__title ">Status</h2>
        </div>
        <!-- Status -->
        <div class="container-fluid ">
          <!-- Progress Bar !-->
          <div class="col-md-4">
            <div class="progress" data-percentage=<?php echo $percent ?>>
              <span class="progress-left">
                <span class="progress-bar"></span>
              </span>
              <span class="progress-right">
                <span class="progress-bar"></span>
              </span>
              <div class="progress-value">
                <div>
                  <?php echo $percent ?>%<br>
                  <span>of 20.000€ </span>
                </div>
              </div>
            </div>
            <div>
              <div class="smalltext text-center text__blue">
                Raised: <?php echo number_format($total) ?> €<br>
              </div>
            </div>
          </div>
          <!-- Countdown !-->
          <div class="cards col-md-4">
            <div id="clockdiv">
              <div>
                <span class="days">0</span>
                <div class="smalltext">Days</div>
              </div>
              <div>
                <span class="hours">0</span>
                <div class="smalltext">Hours</div>
              </div>
              <div>
                <span class="minutes">0</span>
                <div class="smalltext">Minutes</div>
              </div>
              <div>
                <span class="seconds">0</span>
                <div class="smalltext">Seconds</div>
              </div>
            </div>
          </div>
          <!-- Backers -->
          <div class="cards col-md-4">
            <div id="backersdiv">
              <div>
                <?php echo "<span>".$count."</span>" ?>
                <div class="smalltext">Backers</div>
              </div>
            </div>
          </div>
        </div>
        <!-- End of Status -->
      </div>
    </section>
    <!-- End of Introduction_and_Status -->

    <!-- Start Retrospective -->
    <section class="container--bg">
      <h2 class="page-section__title--white">2017 Retrospective</h2>
    </section>
    <!-- Start of list of projects -->
    <section>
      <div class="container-fluid">
        <section class="project">
          <div class="row gutters-50 ">
            <div class="col-md-4">
              <div class="thumbnail ">
                <a href="https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond" target="_blank ">
                      <img class="img-responsive " src="static/images/goals.png" alt=" ">
                      <div class="caption ">
                        <h4 class="project__title ">KDE Goals 2018 and Beyond</h4>
                        <p class="project__paragraph ">We recently announced the three major goals we'll be focusing on in the next three years. We will develop better privacy settings and more usable software that helps you stay productive, as well as help new contributors feel more welcome in the community.</p>
                      </div>
                    </a>
              </div>
            </div>
            <div class="col-md-4 ">
              <div class="thumbnail ">
                <a href="https://dot.kde.org/2017/09/02/randa-roundup-part-i" target="_blank ">
                      <img class="img-responsive " src="static/images/randa.png" alt=" ">
                      <div class="caption ">
                        <h4 class="project__title ">Randa Meetings</h4>
                        <p class="project__paragraph ">Our traditional developer sprint in Randa, Switzerland, was focused on accessibility and making software easier to use for people with disabilities. We are committed to empowering everyone and making sure no one feels left behind.</p>
                      </div>
                    </a>
              </div>
            </div>
            <div class="col-md-4 ">
              <div class="thumbnail ">
                <a href="https://dot.kde.org/2017/08/06/akademy-2017-retrospect" target="_blank ">
                      <img class="img-responsive " src="static/images/akademy.png " alt=" ">
                      <div class="caption ">
                        <h4 class="project__title ">Akademy</h4>
                        <p class="project__paragraph ">Our biggest community event, Akademy, was held in Almeria, Spain, and attracted more than a hundred attendees to over 40 talks and workshops.</p>
                      </div>
                    </a>
              </div>
            </div>
          </div>
          <div class="row gutters-50 ">
            <div class="col-md-4 ">
              <div class="thumbnail ">
                <a href="https://dot.kde.org/2017/09/04/kde-and-google-summer-code-2017-fun-features-bugs-blogs" target="_blank ">
                      <img class="img-responsive " src="static/images/gsoc.png" alt=" ">
                      <div class="caption ">
                        <h4 class="project__title ">Google Summer of Code</h4>
                        <p class="project__paragraph ">We participated in Google Summer of Code 2017, where our students worked on more than 20 projects. Their contributions are now part of digiKam, Dolphin, Krita, KDevelop, GCompris and other KDE software you use every day.</p>
                      </div>
                    </a>
              </div>
            </div>
            <div class="col-md-4 ">
              <div class="thumbnail ">
                <a href="https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans" target="_blank ">
                      <img class="img-responsive " src="static/images/slimbook.png" alt=" ">
                      <div class="caption ">
                        <h4 class="project__title ">Slimbook</h4>
                        <p class="project__paragraph ">Right at the beginning of 2017, we announced Slimbook, a modern, lightweight laptop completely powered by KDE software thanks to KDE neon. </p>
                      </div>
                    </a>
              </div>
            </div>
            <div class="col-md-4 ">
              <div class="thumbnail ">
                <a href="https://dot.kde.org/2017/09/14/plasma-mobile-and-purisms-librem-5-free-smartphone" target="_blank ">
                      <img class="img-responsive " src="static/images/purism.png" alt=" ">
                      <div class="caption ">
                        <h4 class="project__title ">Plasma Mobile on Purism Librem 5</h4>
                        <p class="project__paragraph ">We partnered with Purism to develop Plasma Mobile for Librem 5, the world's first Free smartphone, and helped their fundraiser reach 2 million USD.</p>
                      </div>
                    </a>
              </div>
            </div>
          </div>
        </section>
    </section>
    <!-- End of list of Retrospective -->
    <!-- Start Table -->
    <section class="container--bg ">
      <h2 class="page-section__title--white">List of Donations</h2>
    </section>
    <!-- Build table -->
    <section>
      <div class="container-fluid">
        <p class="page-section__paragraph text-center">
          Thank you for powering the KDE Community! Your support makes a difference.
        </p>
        <table class="table">
          <tr>
            <th>#</th>
            <th>Date </th>
            <th>Amount </th>
            <th>Donor Name </th>
          </tr>
          <tbody>
            <tbody>
              <?php echo $table; ?>
            </tbody>
          </tbody>
        </table>
      </div>
    </section>
    <!-- End build table -->
    <footer class="footer footer--bg ">
      <div class="page-section">
        <div class="row gutters-100 ">
          <div class="col-md-4 col-lg-6 ">
            <div class="footer__first ">
              <h2 class="footer__title ">KDE Community</h2>
              <ul class="footer-first__social-icons ">
                <li><a href="https://www.facebook.com/kde/"><i class="flaticon-facebook-letter-logo "></i></a></li>
                <li><a href="https://twitter.com/kdecommunity"><i class="flaticon-twitter-logo "></i></a></li>
                <li><a href="mailto:kde-ev-board@kde.org">kde-ev-board@kde.org</a></li>
              </ul>
            </div>
          </div>
        </div>
        <hr class="footer__horizontal-bar ">
        <p class="footer__bottom-paragraph ">KDE® and the K Desktop Environment® logo are registered trademarks of KDE e.V. | <a href="https://www.kde.org/community/whatiskde/impressum.php">Legal</a></p>
      </div>
    </footer>
  </div> <!-- End of main-wrapper -->
  <!-- Modal  -->
  <div class="container-fluid">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">KDE End of the Year Fundraising</h4>
          </div>
          <div class="modal-body">
            <row>
              <form role="form" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <input type="hidden" name="business" value="kde-ev-paypal@kde.org">
                <input type="hidden" name="cmd" value="_donations" />
                <input type="hidden" name="lc" value="GB" />
                <input type="hidden" name="item_name" value="KDE e.V. - Donation to End of Year Fundraising Campaign" />
                <input type="hidden" name="currency_code" value="EUR" />
                <input type="hidden" name="cbt" value="Return to www.kde.org" />
                <input type="hidden" name="on0" value="Show name on donor List" />
                <input type="hidden" name="no_note" value="1" />
                <input type="hidden" name="return" value="https://www.kde.org/fundraisers/yearend2017/index.php">
                <input type="hidden" name="notify_url" value="https://www.kde.org/fundraisers/yearend2017/notify.php">

                <div class="form-group"><label>Amount (&euro;)</label> <input type="text" class="form-control" name="amount" value="50"></div>

                <div class="form-group">
                  <label>Show my name on the <a href="#donations">donors list</a>?</label>
                  <div class="i-checks"><label><input type="radio" value="Yes" name="os0"> <i></i> Yes </label></div>
                  <div class="i-checks"><label><input type="radio" value="No" name="os0"> <i></i> No </label></div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="button--donation"><strong>Donate</strong></button>
                </div>
              </form>
            </row>
          </div>
          <div class=" modal-footer ">
            <p>
              Your support is quite appreciated!
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End of Modal -->
  <!-- <script src="static/js/countdown.js"></script> -->
  <script src="static/assets/jquery/jquery-3.2.1.min.js"></script>
  <script src="static/assets/bootstrap/js/bootstrap.min.js"></script>
  <!-- Piwik -->
  <script type="text/javascript">
      var pkBaseURL = "https://stats.kde.org/";
      document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
  </script>
  <script type="text/javascript">
      piwik_action_name = 'Experience Freedom!';
      piwik_idsite = 1;
      piwik_url = pkBaseURL + "piwik.php";
      piwik_log(piwik_action_name, piwik_idsite, piwik_url);
  </script>
  <script type="text/javascript" src="/media/javascripts/piwikbanner.js"></script>
  <object><noscript><p>Analytics <img src="https://stats.kde.org/piwik.php?idsite=1" style="border:0" alt=""/></p></noscript></object>
  <!-- End Piwik Tag -->
</body>
</html>
