<?php
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Miscellaneous KDE Resources",
        'description' => 'Miscellaneous KDE resources',
    ]);

    require('../aether/header.php');
    $site_root = "../";
?>

<main class="container">

<h1>Miscellaneous KDE Resources</h1>

<ul>
<li>
<a href="https://community.kde.org/Books">Books on KDE</a>
    <div style="margin: 10px;">You'd like to learn more about KDE, a specific application, or how to develop an application for this project? Then this page will lead you to the best books about those topics.</div>
</li>

<li>
<a href="clipart.php">KDE Clipart</a>
    <div style="margin: 10px;">Here you can find an almost complete list of major artwork used in the KDE project, from the official KDE and Plasma logo to the KDE mascots.</div>
</li>

<li>
<a href="metastore.php">KDE MetaStore</a>
    <div style="margin: 10px;">Here you can find links to various stores that provide products and merchandising related to the KDE project.</div>
</li>

<li>
<a href="/code-of-conduct/">KDE Code of Conduct</a>
    <div style="margin: 10px;">Principles for interacting within our community.</div>
</li>

<li>
<a href="/thanks.php">Thank Yous</a>
    <div style="margin: 10px;">Those who have helped KDE.</div>
</li>
</ul>

<!-- END CONTENT -->
</main>
<?php
  require('../aether/footer.php');
