<?php
	require('aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Choose a Linux distribution and start using KDE software",
		'cssFile' => 'https://cdn.kde.org/aether-devel/plasma-desktop.css',
        'description' => "Choose a Linux distribution and start using KDE applications and Plasma",
	]);

	require('aether/header.php');

?>
<style>
.distribution-logo {
  width: 100px;
  height: auto;
  float: right !important;
  padding-left: 20px;
}
.header-image {
  width: 700px;
}
.kubuntu-section {
  background-color: rgba(0, 115, 184, 0.1);
}
.opensuse-section {
  background-color: #dceec8;
}
.neon-section {
  background-color: rgba(34, 152, 173, 0.1);
}
.manjaro-section {
  background-color: rgba(53, 191, 92, 0.1);
}
.fedora-section {
  background-color: rgba(41, 65, 114, 0.1);
}
@media (prefers-color-scheme: dark) {
.opensuse-section {
  background-color: #173f4f;
}
}
main section {
  margin-top: 0;
}
.made-by-kde-inline::before {
  content: "K";
  color: #4d4d4d;
  font-family: glyph;
  font-size: 32px;
  line-height: 50px;
  position: relative;
  top: 7px;
}
a.made-by-kde-inline {
  color: #4d4d4d !important;
}

</style>
<main>
  <div class="container">
    <h1>Distributions with Plasma and KDE Applications</h1>

    <p>This page lists some of the most popular Linux distributions with pre-installed KDE software.
    We recommend that you familiarize yourself with the descriptions on the project
    sites in order to get a more complete picture.</p>
  </div>

  <section class="kubuntu-section pt-4 pb-5">
    <div class="container">
      <h2 id="kubuntu">Kubuntu</h2>
      <img src="/content/distributions/logos/kubuntu.svg" alt="Logo Kubuntu" class="distribution-logo">

      <p>
        Kubuntu is the Ubuntu version with KDE software by default. Developers strive to provide usability for
        beginners. The default installation includes many popular programs and a utility for managing drivers. <br />
      </p>
        
      <p>
        Canonical, the developer of Ubuntu and Kubuntu, is a KDE Patron.<br />
      </p>

      <a href="https://kubuntu.org/" class="learn-more button mt-3 mb-3 d-inline-block">Learn more </a><br />
      <img src="/content/distributions/bg/kubuntu.png" alt="Screeenshot Kubuntu" class="img-fluid rounded header-image">
    </div>
  </section>

  <section class="opensuse-section pb-5 pt-4">
    <div class="container">
      <h2 id="opensuse">openSUSE</h2>
      <img src="/content/distributions/logos/opensuse.svg" alt="Logo openSUSE" class="distribution-logo">

      <p>
        openSUSE comes in two versions: <br />
      </p>

      <ul>
        <li>Leap - stable distribution with regular releases, comes with LTS versions of Linux, Qt and Plasma</li>
        <li>Tumbleweed - rolling distribution with the latest versions of all packages</li>
      </ul>

      <p>
        SUSE is a KDE Patron. <br />
      </p>

      <a href="https://www.opensuse.org" class="learn-more button d-inline-block mt-3 mb-3">Learn more</a><br />
      <img src="/content/distributions/bg/opensuse.png" alt="Screeenshot openSUSE" class="img-fluid rounded header-image">
    </div>
  </section>
  
  <section class="neon-section pb-5 pt-4">
    <div class="container">
      <h2 id="kde-neon">KDE neon</h2>
      <a class="made-by-kde-inline" href="https://kde.org">
        Made by KDE
      </a>
      <img src="/content/distributions/logos/neon.svg" alt="Logo KDE neon" class="distribution-logo">

      <p>
        KDE neon takes the latest Plasma desktop and KDE apps and builds them fresh each day for your
        pleasure, using the stable Ubuntu LTS base.
      </p>

      <p>
        Most people will want our User edition built from released software, but we also have Testing
        and Unstable editions built directly from unreleased Git for helping develop our software.
        It is installable as your Linux distro or from Docker images.
      </p>

      <a href="https://neon.kde.org" class="learn-more button d-inline-block mb-3">Learn more</a><br />
      <img src="/content/plasma-desktop/plasma-launcher.png" alt="Screeenshot KDE neon" class="img-fluid rounded header-image">
    </div>
  </section>

  <section class="fedora-section pb-5 pt-4">
    <div class="container">
      <h2 id="fedora-kde">Fedora KDE</h2>
      <img src="/content/distributions/logos/fedora.svg" alt="Logo Fedora" class="distribution-logo">
      <p>
        Fedora is a stable distribution sponsored by Red Hat. A new version of Fedora is released every 6 months.
      </p>

      <a href="https://spins.fedoraproject.org/kde/" class="learn-more button d-inline-block mb-3">Learn more</a> <br />
      <img src="/content/distributions/bg/fedora.jpg" alt="Screeenshot Fedora" class="img-fluid rounded header-image">
    </div>
  </section>

  <section class="manjaro-section pb-5 pt-4">
    <div class="container">
      <h2 id="manjaro-kde">Manjaro KDE</h2>
      <img src="/content/distributions/logos/manjaro.svg" alt="Logo Manjaro" class="distribution-logo">
      <p>
        Manjaro is a distribution based on Arch Linux, aimed at simplifying its installation and configuration.
        The Manjaro KDE version, in particular, is configured to use KDE Plasma and KDE core applications.
      </p>

      <a href="https://manjaro.org/" class="learn-more button d-inline-block mb-3">Learn more</a> <br />
      <img src="/content/distributions/bg/manjaro.png" alt="Screeenshot Manjaro" class="img-fluid rounded header-image">
    </div>
  </section>

  <section class="container">
    <p>Install using ROSA Image Writer for:</p>
    <ul>
      <li><a href="http://wiki.rosalab.ru/ru/images/6/62/RosaImageWriter-2.6.2-win.zip" class="internal" title="RosaImageWriter-2.6.2-win.zip">Windows</a></li>
      <li><a href="http://wiki.rosalab.ru/ru/images/4/45/RosaImageWriter-2.6.2-lin-i686.tar.xz" class="internal" title="RosaImageWriter-2.6.2-lin-i686.tar.xz">Linux 32-bit</a></li>
      <li><a href="http://wiki.rosalab.ru/ru/images/7/7f/RosaImageWriter-2.6.2-lin-x86_64.tar.xz" class="internal" title="RosaImageWriter-2.6.2-lin-x86_64.tar.xz">Linux 64-bit</a></li>
      <li><a href="http://wiki.rosalab.ru/ru/images/3/33/RosaImageWriter-2.6.2-osx.dmg" class="internal" title="RosaImageWriter-2.6.2-osx.dmg">Mac OS X</a></li>
    </ul>
    <p>The distribution should also contain installation instructions and a quick-start guide.</p>

    <p>You can install KDE applications and Plasma in other Linux distributions and other operating systems. A list of those is available on the
    <a href="https://community.kde.org/Distributions">KDE Community wiki</a>.</p>
    <a href="/hardware" class="learn-more button mt-3 mb-3 d-inline-block">Buy device with plasma</a>
  </section>
</main>

<?php
	require('aether/footer.php');
?>
