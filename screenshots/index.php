<?php
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Screenshots"
    ]);

    require('../aether/header.php');
    $site_root = "../";
?>

<style>
figure {
    margin: 0px auto;
    text-align: center;
    border: 0px;
    /* background-color: #e8e8e8; */
    padding: 1ex;
}
figure:hover {
    background-color: #c3c3c3
}
figure img {
    max-width: 100%;
    height: auto;
}
</style>

<main class="container">

<h1>Screenshots</h1>

<p>At KDE we pride ourselves on creating beautiful software.</p>

<h2><a href="https://www.kde.org/announcements/plasma-5.19.0">Plasma</a></h2>
<p>Our flagship computer desktop.</p>
<figure>
<a href="https://kde.org/announcements/plasma-5.19/plasma-5.19-2.png" data-toggle="lightbox">
<img src="https://www.kde.org/announcements/plasma-5.19/plasma-5.19-2-wee.png" width="800" height="450" />
</a>
</figure> 

<h2><a href="https://krita.org">Krita</a></h2>
<p>World class painting.</p>
<figure>
<a href="https://cdn.kde.org/screenshots/krita/2018-03-17_screenshot_005.png" data-toggle="lightbox">
<img src="https://cdn.kde.org/screenshots/krita/2018-03-17_screenshot_005.png" width="600" height="" />
</a>
</figure> 

<h2><a href="https://community.kde.org/KDEConnect">KDE Connect</a></h2>
<p>Integrate your laptop and your phone.</p>
<figure>
<a href="https://cdn.kde.org/screenshots/kdeconnect/plasmoid.png" data-toggle="lightbox">
<img src="https://cdn.kde.org/screenshots/kdeconnect/plasmoid.png" width="600" height="" />
</a>
</figure> 

<h2><a href="https://kdenlive.org/en/">Kdenlive</a></h2>
<p>Create home movies.</p>
<figure>
<a href="https://cdn.kde.org/screenshots/kdenlive/k3.png" data-toggle="lightbox">
<img src="https://cdn.kde.org/screenshots/kdenlive/k3.png" width="600" height="" />
</a>
</figure> 

<h2><a href="https://gcompris.net/index-en.html">GCompris</a></h2>
<p>Makes learning fun.</p>
<figure>
<a href="https://cdn.kde.org/screenshots/gcompris/gcompris.png" data-toggle="lightbox">
<img src="https://cdn.kde.org/screenshots/gcompris/gcompris.png" width="600" height="" />
</a>
</figure> 

</main>
<script src="/js/use-ekko-lightbox.js" defer></script>
<?php
  require('../aether/footer.php');
