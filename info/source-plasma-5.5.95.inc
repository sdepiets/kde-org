<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha256 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/bluedevil-5.5.95.tar.xz">bluedevil-5.5.95</a></td>
   <td align="right">130kB</td>
   <td><tt>0d14012f95ca96328b7427139463e7f739d89e650acb2a1cd5cf9285d835bc3d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/breeze-5.5.95.tar.xz">breeze-5.5.95</a></td>
   <td align="right">18MB</td>
   <td><tt>b6439a25f90624d80ff3949222ad8b2b1d500cc0054f9a37ea78f3b38eb0e3cc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/breeze-grub-5.5.95.tar.xz">breeze-grub-5.5.95</a></td>
   <td align="right">150kB</td>
   <td><tt>d9f1bd67181375d4f888b7f89c5bbbbd1af57756ecee024ba9b9c0bfd9699c8d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/breeze-gtk-5.5.95.tar.xz">breeze-gtk-5.5.95</a></td>
   <td align="right">251kB</td>
   <td><tt>f389870bbed38bc34a022e0bda29244d912f25d5bd327b76b4546e2ee4a386a1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/breeze-plymouth-5.5.95.tar.xz">breeze-plymouth-5.5.95</a></td>
   <td align="right">47kB</td>
   <td><tt>06f8500d3201f270deb21b201db16fe73fc50bde0b80d73ac8663aa5dc9ab3ff</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/discover-5.5.95.tar.xz">discover-5.5.95</a></td>
   <td align="right">818kB</td>
   <td><tt>5d714dd25c94a879d8b2b1a0b5b2eea7fed49e58d15cba20710257610ccc25bc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kactivitymanagerd-5.5.95.tar.xz">kactivitymanagerd-5.5.95</a></td>
   <td align="right">73kB</td>
   <td><tt>9f548565368195b68058b23dbf410c52f3863937a4928581c8f96ca7ce2d568d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kde-cli-tools-5.5.95.tar.xz">kde-cli-tools-5.5.95</a></td>
   <td align="right">474kB</td>
   <td><tt>0ae75c2466f3a32fa30397c99744141f900d2785e8baad08b17e6498a11baab7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kdecoration-5.5.95.tar.xz">kdecoration-5.5.95</a></td>
   <td align="right">34kB</td>
   <td><tt>c662f6174de54c180247e147d463389750a3b65642e5c9acce88772d4150a097</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kde-gtk-config-5.5.95.tar.xz">kde-gtk-config-5.5.95</a></td>
   <td align="right">143kB</td>
   <td><tt>d2ece89f67e49108899b7fa7a3e2e1eab6d7afe664bbf57aaf9460c98c4eb7ed</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kdeplasma-addons-5.5.95.tar.xz">kdeplasma-addons-5.5.95</a></td>
   <td align="right">1.9MB</td>
   <td><tt>372b48f9bfde041feb79455c403bbd998df2ca09d5696495adb4bc35c9189355</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kgamma5-5.5.95.tar.xz">kgamma5-5.5.95</a></td>
   <td align="right">58kB</td>
   <td><tt>75ca6cd43a523a2efa3858f7938d2f6b83f481c3a5d0c7043f4fc70f57bac078</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/khelpcenter-5.5.95.tar.xz">khelpcenter-5.5.95</a></td>
   <td align="right">1.6MB</td>
   <td><tt>3ba6b15e680a3866dc71b226a8e908fc7d49a39edbc358e1e51b4944d02a23f8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/khotkeys-5.5.95.tar.xz">khotkeys-5.5.95</a></td>
   <td align="right">590kB</td>
   <td><tt>a91f36c8851cf7ad68a2e57570b90039259132144eddeb0728fcfd89659a9782</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kinfocenter-5.5.95.tar.xz">kinfocenter-5.5.95</a></td>
   <td align="right">1.0MB</td>
   <td><tt>ffa5e63f03f3d5a33f89c81e0bd0a54238a93b81f1342c2ecb562ccac98492d3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kmenuedit-5.5.95.tar.xz">kmenuedit-5.5.95</a></td>
   <td align="right">431kB</td>
   <td><tt>7bfdcf5dc715374a2a781fd540b5838ee7affeb0747877122bc61faeb741acef</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kscreen-5.5.95.tar.xz">kscreen-5.5.95</a></td>
   <td align="right">101kB</td>
   <td><tt>96dab2aebe8e62f627560a2659f659b4219cae4d68db1040741a3d2271095f9a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kscreenlocker-5.5.95.tar.xz">kscreenlocker-5.5.95</a></td>
   <td align="right">98kB</td>
   <td><tt>9f47181b81266ee4641aaca11b6c57eae469c9e83b6c72f7c551e17a523c0785</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/ksshaskpass-5.5.95.tar.xz">ksshaskpass-5.5.95</a></td>
   <td align="right">19kB</td>
   <td><tt>a0c783ccf833b4a96f5aa7cbed3f4953a2b10c443c9ec49fa034ee4614aeb724</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/ksysguard-5.5.95.tar.xz">ksysguard-5.5.95</a></td>
   <td align="right">474kB</td>
   <td><tt>f0e6549994a4fad1fdba4dcba50367da7cd39c5381aee958c8581824da95ccd8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kwallet-pam-5.5.95.tar.xz">kwallet-pam-5.5.95</a></td>
   <td align="right">17kB</td>
   <td><tt>b5152423221095f870595b9fde0a294868b9266e1b76d0a8339e7bb1b7093650</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kwayland-5.5.95.tar.xz">kwayland-5.5.95</a></td>
   <td align="right">163kB</td>
   <td><tt>314c285db9d21a95ffb8a7b64cbced1dc4cf06c43ed5497c1193a0e402c9d619</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kwayland-integration-5.5.95.tar.xz">kwayland-integration-5.5.95</a></td>
   <td align="right">16kB</td>
   <td><tt>c0a63a0cacf3f749a7c2c94e16e71f2cd667fc6cdf194e5bc1748e3603bc5f99</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kwin-5.5.95.tar.xz">kwin-5.5.95</a></td>
   <td align="right">3.6MB</td>
   <td><tt>000fe5c6753c8f46a75380bbff9cb550f1d0b64808d177e5b6f168856bd6ba10</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/kwrited-5.5.95.tar.xz">kwrited-5.5.95</a></td>
   <td align="right">19kB</td>
   <td><tt>77bdf4cfa70376547bf10a6c7de942242de0d64c6aea8a1d240c4c4e461c726a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/libkscreen-5.5.95.tar.xz">libkscreen-5.5.95</a></td>
   <td align="right">81kB</td>
   <td><tt>77483d27465c87a32f222e5d557796d0c77438dc1a288fc9decb1fbce96868bd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/libksysguard-5.5.95.tar.xz">libksysguard-5.5.95</a></td>
   <td align="right">555kB</td>
   <td><tt>97b49170fcf657fd367d19b55c18913d79a334e77c47e4f4d4a21d5473ff628a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/milou-5.5.95.tar.xz">milou-5.5.95</a></td>
   <td align="right">52kB</td>
   <td><tt>e01ef795a56261ac2eaeec4cd38ec8f7bc1d60c905a5ba2caef41489a9f4132e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/oxygen-5.5.95.tar.xz">oxygen-5.5.95</a></td>
   <td align="right">4.2MB</td>
   <td><tt>ebe177050543f4b24c98da0d01d4abd52ad8d23aaf1e72e17603c6a6a743c07a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/plasma-desktop-5.5.95.tar.xz">plasma-desktop-5.5.95</a></td>
   <td align="right">6.0MB</td>
   <td><tt>f3af4ebe66c6bf25ccbf51770498954d215e173e53251147aa5303d2e012e9d5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/plasma-integration-5.5.95.tar.xz">plasma-integration-5.5.95</a></td>
   <td align="right">43kB</td>
   <td><tt>6f7bea69bc522500f0cbdf4a15af5f7e6e92ca03ca956ab7f5a0f6e24a3535be</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/plasma-mediacenter-5.5.95.tar.xz">plasma-mediacenter-5.5.95</a></td>
   <td align="right">156kB</td>
   <td><tt>3d364ccc90da4b1789544ef1d1fc18d53b2705c788096a82d743b606de71e599</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/plasma-nm-5.5.95.tar.xz">plasma-nm-5.5.95</a></td>
   <td align="right">596kB</td>
   <td><tt>be26297c40194538c9da2c8059e0ee874db1917b1ff1113579f777d2c9c7865f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/plasma-pa-5.5.95.tar.xz">plasma-pa-5.5.95</a></td>
   <td align="right">52kB</td>
   <td><tt>068aa1dfe238bd79a93d8cc77f4039160a162ed2cdf9990613a0ec5e368ad916</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/plasma-sdk-5.5.95.tar.xz">plasma-sdk-5.5.95</a></td>
   <td align="right">672kB</td>
   <td><tt>bdc6ab7bf16e309c7907cfa0558945e99dccdba2e4868d90758b024590a480fb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/plasma-workspace-5.5.95.tar.xz">plasma-workspace-5.5.95</a></td>
   <td align="right">6.6MB</td>
   <td><tt>d433fb71b59742dc37c9ba5b5393da067af68018c1e96745e8449720b2f5a39a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/plasma-workspace-wallpapers-5.5.95.tar.xz">plasma-workspace-wallpapers-5.5.95</a></td>
   <td align="right">43MB</td>
   <td><tt>5d6232d29c11721df6a2d00a18b9c2174b00b471f4f35e2afa90066413e802c3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/polkit-kde-agent-1-5.5.95.tar.xz">polkit-kde-agent-1-5.5.95</a></td>
   <td align="right">38kB</td>
   <td><tt>4eca1e7743b6f5045cea22d16550bb61aeff893197355e26eb34566d9ac90ab7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/powerdevil-5.5.95.tar.xz">powerdevil-5.5.95</a></td>
   <td align="right">336kB</td>
   <td><tt>9ba907b5cdcdd267dd9a66e1531506163da43b568b6f7926e4cb4057e70227fb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/sddm-kcm-5.5.95.tar.xz">sddm-kcm-5.5.95</a></td>
   <td align="right">45kB</td>
   <td><tt>94782bfce6b63b5ecd1e3e2090a73150a2ee929899147e05d3f23823bf0ed8bc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/simplesystray-5.5.95.tar.xz">simplesystray-5.5.95</a></td>
   <td align="right">20kB</td>
   <td><tt>f13990b96a3a372ec4489564dec9f702075fa6196e6da9fc4dd0399926e86450</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/systemsettings-5.5.95.tar.xz">systemsettings-5.5.95</a></td>
   <td align="right">154kB</td>
   <td><tt>670e861d723f88c05d8ad366bccb811515c0924e4af7ad718088e5352b69ef35</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.5.95/user-manager-5.5.95.tar.xz">user-manager-5.5.95</a></td>
   <td align="right">54kB</td>
   <td><tt>9143f7df67bea63b470585783334f997cc9526fc9de194cf7c48eb9349e70fbe</tt></td>
</tr>

</table>
