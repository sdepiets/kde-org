<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/arts-1.5.4.tar.bz2">arts-1.5.4</a></td>
   <td align="right">944kB</td>
   <td><tt>886ba4a13dc0da312d94c09f50c3ffe6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdeaccessibility-3.5.4.tar.bz2">kdeaccessibility-3.5.4</a></td>
   <td align="right">8.2MB</td>
   <td><tt>bc64350a56a585e4d2c88fe3fc8b24f8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdeaddons-3.5.4.tar.bz2">kdeaddons-3.5.4</a></td>
   <td align="right">1.5MB</td>
   <td><tt>840d18fbcadec9f94bb70563876b4dcd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdeadmin-3.5.4.tar.bz2">kdeadmin-3.5.4</a></td>
   <td align="right">2.0MB</td>
   <td><tt>579a2e3e8e05cf0851a88def57fcc0a3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdeartwork-3.5.4.tar.bz2">kdeartwork-3.5.4</a></td>
   <td align="right">15MB</td>
   <td><tt>c7ff9048b9b68c976de08ed418598e40</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdebase-3.5.4.tar.bz2">kdebase-3.5.4</a></td>
   <td align="right">22MB</td>
   <td><tt>882a9729c08b197caef2c8712c980d9c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdebindings-3.5.4.tar.bz2">kdebindings-3.5.4</a></td>
   <td align="right">5.1MB</td>
   <td><tt>097eb4311f5715f36fdc83dc5badb476</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdeedu-3.5.4.tar.bz2">kdeedu-3.5.4</a></td>
   <td align="right">28MB</td>
   <td><tt>81dfdea2ed6eaaa440ee868a69b8fc7e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdegames-3.5.4.tar.bz2">kdegames-3.5.4</a></td>
   <td align="right">10MB</td>
   <td><tt>60bb42c51c2e86a826188da457ed21d0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdegraphics-3.5.4.tar.bz2">kdegraphics-3.5.4</a></td>
   <td align="right">6.9MB</td>
   <td><tt>de1f6d0597235f52186aaeeaabe7dc08</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdelibs-3.5.4.tar.bz2">kdelibs-3.5.4</a></td>
   <td align="right">14MB</td>
   <td><tt>979d056ca0e21c12caed270126e60e3e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdemultimedia-3.5.4.tar.bz2">kdemultimedia-3.5.4</a></td>
   <td align="right">5.9MB</td>
   <td><tt>921680248b5f5793b9201715fffe6e33</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdenetwork-3.5.4.tar.bz2">kdenetwork-3.5.4</a></td>
   <td align="right">7.1MB</td>
   <td><tt>81660b1a73e81b6c01a55861c154dd3b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdepim-3.5.4.tar.bz2">kdepim-3.5.4</a></td>
   <td align="right">12MB</td>
   <td><tt>4a01ceaeb6067d03003edb77b104f559</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdesdk-3.5.4.tar.bz2">kdesdk-3.5.4</a></td>
   <td align="right">4.7MB</td>
   <td><tt>2150e6a4ce5e42886a4afc6c0198c30c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdetoys-3.5.4.tar.bz2">kdetoys-3.5.4</a></td>
   <td align="right">3.0MB</td>
   <td><tt>f959e936d05697ac6b1a0b066632b2fd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdeutils-3.5.4.tar.bz2">kdeutils-3.5.4</a></td>
   <td align="right">2.8MB</td>
   <td><tt>e24cd91576db3d2414fb30cab47e44e5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdevelop-3.3.4.tar.bz2">kdevelop-3.3.4</a></td>
   <td align="right">7.7MB</td>
   <td><tt>e5278e37468bda7fdf6d019f84aeed16</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.4/src/kdewebdev-3.5.4.tar.bz2">kdewebdev-3.5.4</a></td>
   <td align="right">5.7MB</td>
   <td><tt>310a40555c089e88cc5ff7620a89b444</tt></td>
</tr>

</table>
