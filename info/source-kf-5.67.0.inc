
<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha1 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/attica-5.67.0.tar.xz">attica-5.67.0</a></td>
   <td align="right">63kB</td>
   <td><tt>64e3c8a23980028da2045ed1c49d13351a9acd90</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/baloo-5.67.0.tar.xz">baloo-5.67.0</a></td>
   <td align="right">267kB</td>
   <td><tt>428321b808e7ea5584c8f2bf5171e44725f9fc92</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/bluez-qt-5.67.0.tar.xz">bluez-qt-5.67.0</a></td>
   <td align="right">98kB</td>
   <td><tt>e6412f30ce718449c7f0a287b9e51e2ffb2972be</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/breeze-icons-5.67.0.tar.xz">breeze-icons-5.67.0</a></td>
   <td align="right">2.6MB</td>
   <td><tt>fbe45c99624d68411092f5e448e7d935b43df341</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/extra-cmake-modules-5.67.0.tar.xz">extra-cmake-modules-5.67.0</a></td>
   <td align="right">331kB</td>
   <td><tt>493b06ae94f5731ce64ef91e60f30ac3bce86061</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/frameworkintegration-5.67.0.tar.xz">frameworkintegration-5.67.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>256bf56cb68b57ad23b37a1308034850736d00da</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kactivities-5.67.0.tar.xz">kactivities-5.67.0</a></td>
   <td align="right">59kB</td>
   <td><tt>9b59036f4b8e88dd3944edbc6d8cd88cab825b89</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kactivities-stats-5.67.0.tar.xz">kactivities-stats-5.67.0</a></td>
   <td align="right">61kB</td>
   <td><tt>e9bbd244809aa6d776a8935b00e88165b0442a9d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kapidox-5.67.0.tar.xz">kapidox-5.67.0</a></td>
   <td align="right">387kB</td>
   <td><tt>fa657de7753fa67f05a5ba25b4ce8683deb92caa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/karchive-5.67.0.tar.xz">karchive-5.67.0</a></td>
   <td align="right">448kB</td>
   <td><tt>1b6b5eca8c4d065f3386f04c41487aad60e05446</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kauth-5.67.0.tar.xz">kauth-5.67.0</a></td>
   <td align="right">84kB</td>
   <td><tt>fc53cf725da21c5fc5c6d83db3019a4646205823</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kbookmarks-5.67.0.tar.xz">kbookmarks-5.67.0</a></td>
   <td align="right">117kB</td>
   <td><tt>59fb4fe11a5298bdb8f1ca8bed759b5d0a4b698b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kcalendarcore-5.67.0.tar.xz">kcalendarcore-5.67.0</a></td>
   <td align="right">242kB</td>
   <td><tt>b3a6c52fad32afb6c62bc03ddfa54ba51f80b791</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kcmutils-5.67.0.tar.xz">kcmutils-5.67.0</a></td>
   <td align="right">234kB</td>
   <td><tt>f6a18d12b0b026ce356eb6e2169e6e61ee3a072e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kcodecs-5.67.0.tar.xz">kcodecs-5.67.0</a></td>
   <td align="right">216kB</td>
   <td><tt>1c17c6ded29fdd90830d77a3939149da61866f68</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kcompletion-5.67.0.tar.xz">kcompletion-5.67.0</a></td>
   <td align="right">117kB</td>
   <td><tt>0268e272adcfd2cd2d953b553bdcfd0acb8869b1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kconfig-5.67.0.tar.xz">kconfig-5.67.0</a></td>
   <td align="right">245kB</td>
   <td><tt>93d8d32c93bae7a23d2e4c2c4341e70374afbaaf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kconfigwidgets-5.67.0.tar.xz">kconfigwidgets-5.67.0</a></td>
   <td align="right">372kB</td>
   <td><tt>e0c5d33a9ba4b4a80cae88c361d60166d8c469a2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kcontacts-5.67.0.tar.xz">kcontacts-5.67.0</a></td>
   <td align="right">533kB</td>
   <td><tt>f729255b429cc6076fb463e2805e1a4cd2f1b655</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kcoreaddons-5.67.0.tar.xz">kcoreaddons-5.67.0</a></td>
   <td align="right">360kB</td>
   <td><tt>da97af82afa55ade033f9dab14c433f5b6e6819f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kcrash-5.67.0.tar.xz">kcrash-5.67.0</a></td>
   <td align="right">22kB</td>
   <td><tt>d62164a632ca57f51d96e065998e3153ce35c184</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kdbusaddons-5.67.0.tar.xz">kdbusaddons-5.67.0</a></td>
   <td align="right">39kB</td>
   <td><tt>3d46e44f10f137a111653072ecbee988f73a60e4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kdeclarative-5.67.0.tar.xz">kdeclarative-5.67.0</a></td>
   <td align="right">170kB</td>
   <td><tt>bd5fd8a6ea1ded68fd92a86bfa0d9d611801c7ac</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kded-5.67.0.tar.xz">kded-5.67.0</a></td>
   <td align="right">37kB</td>
   <td><tt>8a2e6b22f5e9ee00fb0d2963da884a6511ebe3d5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kdesu-5.67.0.tar.xz">kdesu-5.67.0</a></td>
   <td align="right">46kB</td>
   <td><tt>fabea81de17ca1d5c4e62ed0333c2844c9c86da9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kdnssd-5.67.0.tar.xz">kdnssd-5.67.0</a></td>
   <td align="right">57kB</td>
   <td><tt>7c36fab6f2da7ae64c3f7c02d522ee03e5cc42b2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kdoctools-5.67.0.tar.xz">kdoctools-5.67.0</a></td>
   <td align="right">416kB</td>
   <td><tt>67290a5afc3b7e8f1f99e987aa910d32ac26ac75</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kemoticons-5.67.0.tar.xz">kemoticons-5.67.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>e0fdf3a1daf6670481bade15c1571efb3950b891</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kfilemetadata-5.67.0.tar.xz">kfilemetadata-5.67.0</a></td>
   <td align="right">412kB</td>
   <td><tt>e3fdcd5341690e15a7891b79a2f95e3e76b8226d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kglobalaccel-5.67.0.tar.xz">kglobalaccel-5.67.0</a></td>
   <td align="right">82kB</td>
   <td><tt>ebbc21a9d6f0637ce20e3bc150b26bd6e598afe0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kguiaddons-5.67.0.tar.xz">kguiaddons-5.67.0</a></td>
   <td align="right">39kB</td>
   <td><tt>12e81bcf9f100f7df41d1c88d8207d596866b986</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kholidays-5.67.0.tar.xz">kholidays-5.67.0</a></td>
   <td align="right">204kB</td>
   <td><tt>55b79f01087c0b3eb3568d30d027afe274b00013</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/ki18n-5.67.0.tar.xz">ki18n-5.67.0</a></td>
   <td align="right">574kB</td>
   <td><tt>0c5dd78190bdc878806cb5f4d7d38abe47b8953c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kiconthemes-5.67.0.tar.xz">kiconthemes-5.67.0</a></td>
   <td align="right">204kB</td>
   <td><tt>42cc8045e807ae7f751cf95fffd3086a0d04f285</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kidletime-5.67.0.tar.xz">kidletime-5.67.0</a></td>
   <td align="right">28kB</td>
   <td><tt>2a9de131277a0db939f1985618157aeb8de7a17c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kimageformats-5.67.0.tar.xz">kimageformats-5.67.0</a></td>
   <td align="right">238kB</td>
   <td><tt>f5d9f48c1171d301baa5b52ad4322625bf0ff3d6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kinit-5.67.0.tar.xz">kinit-5.67.0</a></td>
   <td align="right">116kB</td>
   <td><tt>0e7063e7d825d305ec6b9cbd48a2560bec21696b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kio-5.67.0.tar.xz">kio-5.67.0</a></td>
   <td align="right">3.0MB</td>
   <td><tt>54dc701ef3375f42f7af01e710709dc2ab004b24</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kirigami2-5.67.0.tar.xz">kirigami2-5.67.0</a></td>
   <td align="right">156kB</td>
   <td><tt>4a7a8642974be12785aea202df42b28f77989a6e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kitemmodels-5.67.0.tar.xz">kitemmodels-5.67.0</a></td>
   <td align="right">385kB</td>
   <td><tt>f36532d84da0557616bcc519c891cf9c2682731a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kitemviews-5.67.0.tar.xz">kitemviews-5.67.0</a></td>
   <td align="right">74kB</td>
   <td><tt>e0333e49bf987e49243c2c8f493d5181b7523b9f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kjobwidgets-5.67.0.tar.xz">kjobwidgets-5.67.0</a></td>
   <td align="right">86kB</td>
   <td><tt>b6d0261576a616f70f2b2d80a1fe0334edeb1811</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/knewstuff-5.67.0.tar.xz">knewstuff-5.67.0</a></td>
   <td align="right">1.0MB</td>
   <td><tt>f94682c3dd5bb2fb7db287199f85321080a02ef2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/knotifications-5.67.0.tar.xz">knotifications-5.67.0</a></td>
   <td align="right">114kB</td>
   <td><tt>1a66e64382c5e4c37eb5915a2d59b0772ea28e89</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/knotifyconfig-5.67.0.tar.xz">knotifyconfig-5.67.0</a></td>
   <td align="right">82kB</td>
   <td><tt>edddcff0ca7b71ada25c64534c218561178d5ac2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kpackage-5.67.0.tar.xz">kpackage-5.67.0</a></td>
   <td align="right">134kB</td>
   <td><tt>9be096f75d09978873cb4e4fdc5a0445b71a8ad0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kparts-5.67.0.tar.xz">kparts-5.67.0</a></td>
   <td align="right">172kB</td>
   <td><tt>0210328817d0e5b89cca90ad1bc5a068a8330bad</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kpeople-5.67.0.tar.xz">kpeople-5.67.0</a></td>
   <td align="right">60kB</td>
   <td><tt>915dbf4f8fb013aaa8a280dabbc794eb264e0f98</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kplotting-5.67.0.tar.xz">kplotting-5.67.0</a></td>
   <td align="right">29kB</td>
   <td><tt>4bf626d56f6bae18c5107088c5919abe9b395651</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kpty-5.67.0.tar.xz">kpty-5.67.0</a></td>
   <td align="right">56kB</td>
   <td><tt>ca36fe14ec8cc5f0dcb070a45c079df82f40eb3c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kquickcharts-5.67.0.tar.xz">kquickcharts-5.67.0</a></td>
   <td align="right">90kB</td>
   <td><tt>f1fe49fe197c4e5d4ece4447b6e20e4c151cc791</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/krunner-5.67.0.tar.xz">krunner-5.67.0</a></td>
   <td align="right">61kB</td>
   <td><tt>f03cc9a5fc94759edca11b6eb9b7e50b4e5a892b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kservice-5.67.0.tar.xz">kservice-5.67.0</a></td>
   <td align="right">250kB</td>
   <td><tt>21c91681534528506946d9d0ec0f1182dbb1cdc0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/ktexteditor-5.67.0.tar.xz">ktexteditor-5.67.0</a></td>
   <td align="right">2.2MB</td>
   <td><tt>4bd875d0cc2d4dc586588512bd72f691ccf8d40e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/ktextwidgets-5.67.0.tar.xz">ktextwidgets-5.67.0</a></td>
   <td align="right">303kB</td>
   <td><tt>473289b85da819a5237886564c6d8dd620037118</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kunitconversion-5.67.0.tar.xz">kunitconversion-5.67.0</a></td>
   <td align="right">818kB</td>
   <td><tt>0dea13aed6d20aabf1f6ad481099f7a4d8b0d60c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kwallet-5.67.0.tar.xz">kwallet-5.67.0</a></td>
   <td align="right">289kB</td>
   <td><tt>1ca676f063a857c04cc412f5249fa73f5311c9a2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kwayland-5.67.0.tar.xz">kwayland-5.67.0</a></td>
   <td align="right">331kB</td>
   <td><tt>f3e1a983d4521ea5054fedc58f878b107ba6bb32</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kwidgetsaddons-5.67.0.tar.xz">kwidgetsaddons-5.67.0</a></td>
   <td align="right">2.1MB</td>
   <td><tt>eb85c06f86fa7d349a74eba585be01501404ac13</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kwindowsystem-5.67.0.tar.xz">kwindowsystem-5.67.0</a></td>
   <td align="right">171kB</td>
   <td><tt>405c588201c579c76992e8b7e5d9c3ce82493929</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kxmlgui-5.67.0.tar.xz">kxmlgui-5.67.0</a></td>
   <td align="right">837kB</td>
   <td><tt>a4d89a74e01803b622f9fac0ccad95a8652d3c65</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/kxmlrpcclient-5.67.0.tar.xz">kxmlrpcclient-5.67.0</a></td>
   <td align="right">28kB</td>
   <td><tt>5541e5fbdca8dbbe85e001ff8866a54596d5ce24</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/modemmanager-qt-5.67.0.tar.xz">modemmanager-qt-5.67.0</a></td>
   <td align="right">99kB</td>
   <td><tt>186472b21aec804dd52928b1e5e7dd30204abd52</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/networkmanager-qt-5.67.0.tar.xz">networkmanager-qt-5.67.0</a></td>
   <td align="right">181kB</td>
   <td><tt>83ef9e9353be31895ed6f0632d51ec7939abf5c4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/oxygen-icons5-5.67.0.tar.xz">oxygen-icons5-5.67.0</a></td>
   <td align="right">226MB</td>
   <td><tt>63a25d4bb454deeb4530f53a834ce6a16ed11cfa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/plasma-framework-5.67.0.tar.xz">plasma-framework-5.67.0</a></td>
   <td align="right">2.9MB</td>
   <td><tt>07f4f708f83ee60f16b906faee961b9120b577f8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/prison-5.67.0.tar.xz">prison-5.67.0</a></td>
   <td align="right">41kB</td>
   <td><tt>a13548e1be302200fc4aeb2db0091f9c35cab208</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/purpose-5.67.0.tar.xz">purpose-5.67.0</a></td>
   <td align="right">153kB</td>
   <td><tt>e34cf34ef86fd5cf4e18249875728f4578c89f3c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/qqc2-desktop-style-5.67.0.tar.xz">qqc2-desktop-style-5.67.0</a></td>
   <td align="right">45kB</td>
   <td><tt>fdb175b2b53bf9abc4e7c0444e21c5d26a2fdd02</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/solid-5.67.0.tar.xz">solid-5.67.0</a></td>
   <td align="right">259kB</td>
   <td><tt>9cebd94b93da6d739f21f64782f74fc274d91f79</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/sonnet-5.67.0.tar.xz">sonnet-5.67.0</a></td>
   <td align="right">281kB</td>
   <td><tt>381784ff0037b6b770455736bf0e2b9d8523671f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/syndication-5.67.0.tar.xz">syndication-5.67.0</a></td>
   <td align="right">496kB</td>
   <td><tt>105b6f8098cd5c3abd5f32306bd85b9dede4f0e1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/syntax-highlighting-5.67.0.tar.xz">syntax-highlighting-5.67.0</a></td>
   <td align="right">1.5MB</td>
   <td><tt>f73fdfa75077a56a78226986c32640650de8c771</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.67/threadweaver-5.67.0.tar.xz">threadweaver-5.67.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>f151e1897af7cef39a55013f0735dc02b53fe784</tt></td>
</tr>

</table>
