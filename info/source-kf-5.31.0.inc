
<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha1 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/attica-5.31.0.tar.xz">attica-5.31.0</a></td>
   <td align="right">58kB</td>
   <td><tt>ff23abde0782ff8c5919ab59689f1a9b4e394cb7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/baloo-5.31.0.tar.xz">baloo-5.31.0</a></td>
   <td align="right">186kB</td>
   <td><tt>c29120043b0d1f162c0d19551a3dfd57063bd81d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/bluez-qt-5.31.0.tar.xz">bluez-qt-5.31.0</a></td>
   <td align="right">72kB</td>
   <td><tt>8848be5d6be95e2135d62ecbf717b7b63b80e7f0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/breeze-icons-5.31.0.tar.xz">breeze-icons-5.31.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>31df3d07cc3bed5a1dfa6249f2feaa2968c6f28b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/extra-cmake-modules-5.31.0.tar.xz">extra-cmake-modules-5.31.0</a></td>
   <td align="right">303kB</td>
   <td><tt>764866a98dd2471a8768c584c7d16d0cd6214b21</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/frameworkintegration-5.31.0.tar.xz">frameworkintegration-5.31.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>328f792617e92a122b4760228f8b8a33e379a06c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kactivities-5.31.0.tar.xz">kactivities-5.31.0</a></td>
   <td align="right">59kB</td>
   <td><tt>0f24396e8ac69420192277c3a96a3a8f2804b5e1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kactivities-stats-5.31.0.tar.xz">kactivities-stats-5.31.0</a></td>
   <td align="right">57kB</td>
   <td><tt>2920535efee12b9b98ac692114c507216a52a8c7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kapidox-5.31.0.tar.xz">kapidox-5.31.0</a></td>
   <td align="right">386kB</td>
   <td><tt>acf96f328a8b6fbc0419dbd9583864f9648541df</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/karchive-5.31.0.tar.xz">karchive-5.31.0</a></td>
   <td align="right">109kB</td>
   <td><tt>e686333ba6859a2dd35be9413c29c5ac61bcb70b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kauth-5.31.0.tar.xz">kauth-5.31.0</a></td>
   <td align="right">81kB</td>
   <td><tt>70be181c78a8e127cd3f7808ab558a8cb9e170b8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kbookmarks-5.31.0.tar.xz">kbookmarks-5.31.0</a></td>
   <td align="right">113kB</td>
   <td><tt>43b33d19e7e53e68844d1cabe82075936312ad61</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kcmutils-5.31.0.tar.xz">kcmutils-5.31.0</a></td>
   <td align="right">230kB</td>
   <td><tt>9b43c14d96d4f4705a50f2fa7040bd37962cbebb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kcodecs-5.31.0.tar.xz">kcodecs-5.31.0</a></td>
   <td align="right">216kB</td>
   <td><tt>081de77b3cb02467ce0d669377a0ec8ba5f105ee</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kcompletion-5.31.0.tar.xz">kcompletion-5.31.0</a></td>
   <td align="right">114kB</td>
   <td><tt>4cc1d422d9827d2398c0062e8b9d8824dda97748</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kconfig-5.31.0.tar.xz">kconfig-5.31.0</a></td>
   <td align="right">226kB</td>
   <td><tt>c64a8f390b11af3aed348a33e567ddd5ba2fc686</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kconfigwidgets-5.31.0.tar.xz">kconfigwidgets-5.31.0</a></td>
   <td align="right">364kB</td>
   <td><tt>1da70b39c4cdb52b0ed7a2dedb343e0d8a152102</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kcoreaddons-5.31.0.tar.xz">kcoreaddons-5.31.0</a></td>
   <td align="right">334kB</td>
   <td><tt>1e73343833a88ed7bff1589efca85d1a945bccd3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kcrash-5.31.0.tar.xz">kcrash-5.31.0</a></td>
   <td align="right">20kB</td>
   <td><tt>8a8d61df85de4f8d585d7e74d998f5e1361d717f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kdbusaddons-5.31.0.tar.xz">kdbusaddons-5.31.0</a></td>
   <td align="right">34kB</td>
   <td><tt>01bfd7613fb9121df17cd7569fc503b0e8e0f970</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kdeclarative-5.31.0.tar.xz">kdeclarative-5.31.0</a></td>
   <td align="right">165kB</td>
   <td><tt>525f84615fb29a793d028ddb848562909227e847</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kded-5.31.0.tar.xz">kded-5.31.0</a></td>
   <td align="right">36kB</td>
   <td><tt>b0e55e8d115ae6a087517064a64b9ad86ad894d6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kdesignerplugin-5.31.0.tar.xz">kdesignerplugin-5.31.0</a></td>
   <td align="right">87kB</td>
   <td><tt>8901c183b88a74988f5449e19e84ba8880bc6477</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kdesu-5.31.0.tar.xz">kdesu-5.31.0</a></td>
   <td align="right">43kB</td>
   <td><tt>63d58fdcd26004fa34cec41aaac4b7929f8f8eb8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kdewebkit-5.31.0.tar.xz">kdewebkit-5.31.0</a></td>
   <td align="right">28kB</td>
   <td><tt>79940b2d59bd4c1e174e5f59e620d5db8c99cec4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kdnssd-5.31.0.tar.xz">kdnssd-5.31.0</a></td>
   <td align="right">55kB</td>
   <td><tt>e30faf9b9a26b1f06121f14a9b52f4f20505251d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kdoctools-5.31.0.tar.xz">kdoctools-5.31.0</a></td>
   <td align="right">404kB</td>
   <td><tt>b9e049981592e87372c14fd280aa59f73692eca8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kemoticons-5.31.0.tar.xz">kemoticons-5.31.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>ae29b8abb1e8689ca052ec400dd73b9a22c1c9c6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kfilemetadata-5.31.0.tar.xz">kfilemetadata-5.31.0</a></td>
   <td align="right">130kB</td>
   <td><tt>d871af7ae697ee56b40f1ea0afccbcff9a1d7458</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kglobalaccel-5.31.0.tar.xz">kglobalaccel-5.31.0</a></td>
   <td align="right">81kB</td>
   <td><tt>812a19c86b31a3afda73a7f7beab1ec0909fb9c1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kguiaddons-5.31.0.tar.xz">kguiaddons-5.31.0</a></td>
   <td align="right">38kB</td>
   <td><tt>8782d1ab7c21a207686196e8e8f76c83c8d0c750</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/ki18n-5.31.0.tar.xz">ki18n-5.31.0</a></td>
   <td align="right">591kB</td>
   <td><tt>a2b88f05e863244b0ee9f82960afaf60ac825a4b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kiconthemes-5.31.0.tar.xz">kiconthemes-5.31.0</a></td>
   <td align="right">199kB</td>
   <td><tt>4c15188fb8035907252e53fde96eb89837bdb02e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kidletime-5.31.0.tar.xz">kidletime-5.31.0</a></td>
   <td align="right">25kB</td>
   <td><tt>29c3e14dfa0335e46528a2f025858809450737a0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kimageformats-5.31.0.tar.xz">kimageformats-5.31.0</a></td>
   <td align="right">199kB</td>
   <td><tt>d2cad07015940fed02833c5f0864fcd7bbf0f78c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kinit-5.31.0.tar.xz">kinit-5.31.0</a></td>
   <td align="right">117kB</td>
   <td><tt>04235748d267ff43ad0246e9413a22d1c4a32bf8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kio-5.31.0.tar.xz">kio-5.31.0</a></td>
   <td align="right">2.9MB</td>
   <td><tt>a3ca46bb7612790c0483fb51374795cd7fc0ab42</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kitemmodels-5.31.0.tar.xz">kitemmodels-5.31.0</a></td>
   <td align="right">379kB</td>
   <td><tt>629b5f5599f5ea69edd1fd72b1060c7dc91667ea</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kitemviews-5.31.0.tar.xz">kitemviews-5.31.0</a></td>
   <td align="right">73kB</td>
   <td><tt>1598ee4004a89c599b03da999db8f510204a65f8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kjobwidgets-5.31.0.tar.xz">kjobwidgets-5.31.0</a></td>
   <td align="right">87kB</td>
   <td><tt>d035e36aa972545e8d6994f8bfd392b62e086f21</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/knewstuff-5.31.0.tar.xz">knewstuff-5.31.0</a></td>
   <td align="right">861kB</td>
   <td><tt>f9e8cfd3519d0774d9a0d7f6153001ded960b506</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/knotifications-5.31.0.tar.xz">knotifications-5.31.0</a></td>
   <td align="right">95kB</td>
   <td><tt>bdb19d3f75dcf51aee85abf0d38bed367b2890d5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/knotifyconfig-5.31.0.tar.xz">knotifyconfig-5.31.0</a></td>
   <td align="right">81kB</td>
   <td><tt>68c2b58c154138a90320fba80b9a5685d7502908</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kpackage-5.31.0.tar.xz">kpackage-5.31.0</a></td>
   <td align="right">117kB</td>
   <td><tt>9a661293389218e9a9e1da99df403ea08c5212be</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kparts-5.31.0.tar.xz">kparts-5.31.0</a></td>
   <td align="right">151kB</td>
   <td><tt>b82558b06012747618f160cfa163f73e640f222c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kpeople-5.31.0.tar.xz">kpeople-5.31.0</a></td>
   <td align="right">57kB</td>
   <td><tt>739604d5833f05c38c315cf3d58d6c221986205a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kplotting-5.31.0.tar.xz">kplotting-5.31.0</a></td>
   <td align="right">28kB</td>
   <td><tt>6a267d97025ce5871b5b5fc2a5be043403c31c23</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kpty-5.31.0.tar.xz">kpty-5.31.0</a></td>
   <td align="right">56kB</td>
   <td><tt>8a0d248b73284edd608cdcff9d9f5363d9f0e146</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/krunner-5.31.0.tar.xz">krunner-5.31.0</a></td>
   <td align="right">65kB</td>
   <td><tt>20bf0340719008294f7f3417758f59e003bba7f3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kservice-5.31.0.tar.xz">kservice-5.31.0</a></td>
   <td align="right">241kB</td>
   <td><tt>9f177d552dd29b50e6304e082af9179fd404cfa9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/ktexteditor-5.31.0.tar.xz">ktexteditor-5.31.0</a></td>
   <td align="right">2.1MB</td>
   <td><tt>e762c9f52021f075ed83399d99bf9d2d9dbc5cf6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/ktextwidgets-5.31.0.tar.xz">ktextwidgets-5.31.0</a></td>
   <td align="right">300kB</td>
   <td><tt>de50fee279df4db5f943c5d60929fb708603eeee</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kunitconversion-5.31.0.tar.xz">kunitconversion-5.31.0</a></td>
   <td align="right">757kB</td>
   <td><tt>0444cf8693478df921f8c19329c507995d0df2b1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kwallet-5.31.0.tar.xz">kwallet-5.31.0</a></td>
   <td align="right">295kB</td>
   <td><tt>82d876f56a286887845b7501b89ca15248b66db7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kwayland-5.31.0.tar.xz">kwayland-5.31.0</a></td>
   <td align="right">253kB</td>
   <td><tt>caacf242cfe563f4b86dca9b498d890f4972198a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kwidgetsaddons-5.31.0.tar.xz">kwidgetsaddons-5.31.0</a></td>
   <td align="right">2.0MB</td>
   <td><tt>d0623594cf5a37a180c599953a9d58e4bf4c6b88</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kwindowsystem-5.31.0.tar.xz">kwindowsystem-5.31.0</a></td>
   <td align="right">163kB</td>
   <td><tt>aae521a12c5099b38f8e68c773109d0680878a51</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kxmlgui-5.31.0.tar.xz">kxmlgui-5.31.0</a></td>
   <td align="right">861kB</td>
   <td><tt>2ad9b43744ebad02ef27e7ff4f9f75194dc5d96d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/kxmlrpcclient-5.31.0.tar.xz">kxmlrpcclient-5.31.0</a></td>
   <td align="right">27kB</td>
   <td><tt>d3a53bcc87be997c64ead688e37d7ab2f158d3cc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/modemmanager-qt-5.31.0.tar.xz">modemmanager-qt-5.31.0</a></td>
   <td align="right">95kB</td>
   <td><tt>fec7318207b0798a675287c455f57a9568cb49e4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/networkmanager-qt-5.31.0.tar.xz">networkmanager-qt-5.31.0</a></td>
   <td align="right">153kB</td>
   <td><tt>295e5624e819eb241a62b9932048d4199625a25b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/oxygen-icons5-5.31.0.tar.xz">oxygen-icons5-5.31.0</a></td>
   <td align="right">223MB</td>
   <td><tt>8691f7e513b48c0aabec03bc97611e39e84803f8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/plasma-framework-5.31.0.tar.xz">plasma-framework-5.31.0</a></td>
   <td align="right">4.3MB</td>
   <td><tt>70a9fe997a0874ab8f6c18f300ca86e0f187fdd9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/prison-5.31.0.tar.xz">prison-5.31.0</a></td>
   <td align="right">13kB</td>
   <td><tt>c9ba6897e517b81a92a76f7049d7177b07e6e838</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/solid-5.31.0.tar.xz">solid-5.31.0</a></td>
   <td align="right">240kB</td>
   <td><tt>3bddd3a3c0931637925652a0d91c1270280afd13</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/sonnet-5.31.0.tar.xz">sonnet-5.31.0</a></td>
   <td align="right">276kB</td>
   <td><tt>a76edfd9829466ab5f82dff59cce8697aa33f922</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/syntax-highlighting-5.31.0.tar.xz">syntax-highlighting-5.31.0</a></td>
   <td align="right">953kB</td>
   <td><tt>85ad1a23634a5198d3e56cf8bea458e7bdb5e01b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.31/threadweaver-5.31.0.tar.xz">threadweaver-5.31.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>18311a1baae6d123c2834dedd5de93bced0adf37</tt></td>
</tr>

</table>
