<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdeaccessibility-4.0.3.tar.bz2">kdeaccessibility-4.0.3</a></td><td align="right">6.1MB</td><td><tt>16b96c1ab93cfa0f7908e45bb07e9994</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdeadmin-4.0.3.tar.bz2">kdeadmin-4.0.3</a></td><td align="right">1.3MB</td><td><tt>21aa2c7f86dec2cf4f389688fe9b02f2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdeartwork-4.0.3.tar.bz2">kdeartwork-4.0.3</a></td><td align="right">41MB</td><td><tt>63a23db63c6883496747a6be2107b7ef</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdebase-4.0.3.tar.bz2">kdebase-4.0.3</a></td><td align="right">4.2MB</td><td><tt>edff276a45b5348e9fa26924519fabbf</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdebase-runtime-4.0.3.tar.bz2">kdebase-runtime-4.0.3</a></td><td align="right">47MB</td><td><tt>41a47f084ed47c6e44b1a582d2a88fe0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdebase-workspace-4.0.3.tar.bz2">kdebase-workspace-4.0.3</a></td><td align="right">29MB</td><td><tt>11c339f329176a01ff2a8fce57025fe2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdebindings-4.0.3.tar.bz2">kdebindings-4.0.3</a></td><td align="right">3.8MB</td><td><tt>eb1988b419a0e0b97e3a4ed7c70e71f7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdeedu-4.0.3.tar.bz2">kdeedu-4.0.3</a></td><td align="right">41MB</td><td><tt>01c9bb9c13bda190fcab94b15751f0a8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdegames-4.0.3.tar.bz2">kdegames-4.0.3</a></td><td align="right">23MB</td><td><tt>e8a48b59f33e34558388665d32d41e8f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdegraphics-4.0.3.tar.bz2">kdegraphics-4.0.3</a></td><td align="right">2.4MB</td><td><tt>bb64c0013a8682e84654eeaad1158fa4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdelibs-4.0.3.tar.bz2">kdelibs-4.0.3</a></td><td align="right">8.7MB</td><td><tt>7ac2cb9fb2eb24ea6b9d4babfb906101</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdemultimedia-4.0.3.tar.bz2">kdemultimedia-4.0.3</a></td><td align="right">1.1MB</td><td><tt>e14bc666dac01573f59da81055d78e31</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdenetwork-4.0.3.tar.bz2">kdenetwork-4.0.3</a></td><td align="right">6.1MB</td><td><tt>5b74d8eec483f2cfdde93319056b1a1f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdepimlibs-4.0.3.tar.bz2">kdepimlibs-4.0.3</a></td><td align="right">1.7MB</td><td><tt>cd202abc2637aa887d74b54690a432f5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdesdk-4.0.3.tar.bz2">kdesdk-4.0.3</a></td><td align="right">4.1MB</td><td><tt>c6dde9c2197bdaf394c560c0c23d786f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdetoys-4.0.3.tar.bz2">kdetoys-4.0.3</a></td><td align="right">2.2MB</td><td><tt>64b45a5b5b1c92db00934c39ba249672</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdeutils-4.0.3.tar.bz2">kdeutils-4.0.3</a></td><td align="right">2.3MB</td><td><tt>9ec540a6ff7e3c09515096d50b2b3e22</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.3/src/kdewebdev-4.0.3.tar.bz2">kdewebdev-4.0.3</a></td><td align="right">1.2MB</td><td><tt>6bad09edaee7e46be7740c8ef05e0294</tt></td></tr>
</table>
