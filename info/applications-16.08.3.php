<?php
  $version = "16.08.3";
  $page_title = "KDE Applications ".$version." Info Page";
  $site_root = "../";
  include "header.inc";
?>

<p>
For new features, bugfixes and changes read the <a href="../announcements/announce-applications-<?php echo $version; ?>.php">KDE Applications <?php echo $version; ?> announcement</a>.
</p>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#x61;i&#00108;&#x74;o:&#115;ec&#117;&#x72;&#00105;&#x74;&#121;&#x40;kde.&#00111;&#x72;g">&#x73;&#101;&#x63;u&#114;i&#x74;y&#x40;kd&#101;.&#x6f;&#00114;&#103;</a>.</p>

<h2>Download and Installation</h2>

<p>
 <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">Build instructions</a>
 are available on Techbase, our developer wiki.
</p>

<h3><a name='desktop'>KDE Applications <?php echo $version; ?> Release</a></h3>

<h4><a name="binary">Binary packages</a></h4>

<p>
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE Applications <?php echo $version; ?> for some versions of their distribution.
  You can find a list at <a href="https://community.kde.org/Binary_Packages">https://community.kde.org/Binary_Packages</a>
</p>

<h4><a name="source">Source Code</a></h4>
<p>
  The complete source code for KDE Applications <?php echo $version; ?> is available for download:
</p>

<?php
include "source-applications-$version.inc"
?>

<?php
  include "footer.inc";
?>
