
<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha1 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/attica-5.8.0.tar.xz">attica-5.8.0</a></td>
   <td align="right">58kB</td>
   <td><tt>aea15d99d1d48969b8e53091295d18802231c6cc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/extra-cmake-modules-1.8.0.tar.xz">extra-cmake-modules-1.8.0</a></td>
   <td align="right">126kB</td>
   <td><tt>34a57abe092dce75b7f25808e686fb9175ceafc3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/frameworkintegration-5.8.0.tar.xz">frameworkintegration-5.8.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>5ee49f3b9cffb44b4557234f000af69d7e4c000e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kactivities-5.8.0.tar.xz">kactivities-5.8.0</a></td>
   <td align="right">113kB</td>
   <td><tt>b78e40acd4dca570c11dea69d3a4f334f59d078c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kapidox-5.8.0.tar.xz">kapidox-5.8.0</a></td>
   <td align="right">127kB</td>
   <td><tt>743d832c06eb2774f023e2d8db070b3e491929a1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/karchive-5.8.0.tar.xz">karchive-5.8.0</a></td>
   <td align="right">105kB</td>
   <td><tt>e31c948b021e2da3597edf6d650691264d8ad177</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kauth-5.8.0.tar.xz">kauth-5.8.0</a></td>
   <td align="right">78kB</td>
   <td><tt>2723d951c1be06f8483c03933599b02d6be48f4e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kbookmarks-5.8.0.tar.xz">kbookmarks-5.8.0</a></td>
   <td align="right">112kB</td>
   <td><tt>b893da19487320f1a5649ac2d671b366393ea3b0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kcmutils-5.8.0.tar.xz">kcmutils-5.8.0</a></td>
   <td align="right">227kB</td>
   <td><tt>b66afcdb018c539174d5fa8f748e639c6b55c46a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kcodecs-5.8.0.tar.xz">kcodecs-5.8.0</a></td>
   <td align="right">215kB</td>
   <td><tt>95692e6693fe9ffde6350be47741620429d07488</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kcompletion-5.8.0.tar.xz">kcompletion-5.8.0</a></td>
   <td align="right">113kB</td>
   <td><tt>452f017bdf7262d98b1660cb4be08e8ca5f76d96</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kconfig-5.8.0.tar.xz">kconfig-5.8.0</a></td>
   <td align="right">210kB</td>
   <td><tt>f1e78e1b29a6d255c5cb67172c8c23ac585b6df3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kconfigwidgets-5.8.0.tar.xz">kconfigwidgets-5.8.0</a></td>
   <td align="right">349kB</td>
   <td><tt>4632522230ef115ba4871c37cf2e19241dcf979b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kcoreaddons-5.8.0.tar.xz">kcoreaddons-5.8.0</a></td>
   <td align="right">293kB</td>
   <td><tt>41aa49d1c6580e5d00818270e0272e2478d6e3b2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kcrash-5.8.0.tar.xz">kcrash-5.8.0</a></td>
   <td align="right">19kB</td>
   <td><tt>6a2da848ea154d10c5a46960797d54d97762054a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kdbusaddons-5.8.0.tar.xz">kdbusaddons-5.8.0</a></td>
   <td align="right">31kB</td>
   <td><tt>c08022914d159e85d875bc6d9e292dacec8a6ac6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kdeclarative-5.8.0.tar.xz">kdeclarative-5.8.0</a></td>
   <td align="right">147kB</td>
   <td><tt>37ff6e3b96784c91a5af1fdf56088bc7c8503262</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kded-5.8.0.tar.xz">kded-5.8.0</a></td>
   <td align="right">32kB</td>
   <td><tt>07b2e35291320875300ee844f708b3b256302ab7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kdesignerplugin-5.8.0.tar.xz">kdesignerplugin-5.8.0</a></td>
   <td align="right">82kB</td>
   <td><tt>00a9b826d8d3a7eb39bb018a2783cb32e569fa57</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kdesu-5.8.0.tar.xz">kdesu-5.8.0</a></td>
   <td align="right">42kB</td>
   <td><tt>42c74327f5c9f7adc981120e1521816396853ddc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kdewebkit-5.8.0.tar.xz">kdewebkit-5.8.0</a></td>
   <td align="right">28kB</td>
   <td><tt>c1e5bf22f6081ab5833836868a0b09a0ae335074</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kdnssd-5.8.0.tar.xz">kdnssd-5.8.0</a></td>
   <td align="right">54kB</td>
   <td><tt>c3a73ed275d8dd4835a51b6653d1520d43dfcaa7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kdoctools-5.8.0.tar.xz">kdoctools-5.8.0</a></td>
   <td align="right">394kB</td>
   <td><tt>8ea833b450df2cf25dcdf36effc8e15b5b3fab11</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kemoticons-5.8.0.tar.xz">kemoticons-5.8.0</a></td>
   <td align="right">71kB</td>
   <td><tt>935f9f76b69ba1a468db0b30760089502d180a01</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kglobalaccel-5.8.0.tar.xz">kglobalaccel-5.8.0</a></td>
   <td align="right">82kB</td>
   <td><tt>95d285e1d7f36f006c082af0a0a8239019876670</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kguiaddons-5.8.0.tar.xz">kguiaddons-5.8.0</a></td>
   <td align="right">38kB</td>
   <td><tt>6d7699ed6d70dc0360c7a400e5a92b444a39162f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/ki18n-5.8.0.tar.xz">ki18n-5.8.0</a></td>
   <td align="right">571kB</td>
   <td><tt>5443bc6f606dd162c246925ec3f04867f9533672</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kiconthemes-5.8.0.tar.xz">kiconthemes-5.8.0</a></td>
   <td align="right">190kB</td>
   <td><tt>44d720c39294ce6a12da73dd4acc0303fb420184</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kidletime-5.8.0.tar.xz">kidletime-5.8.0</a></td>
   <td align="right">22kB</td>
   <td><tt>5b6132d778a9e265cafc8e402a368157a4ba7476</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kimageformats-5.8.0.tar.xz">kimageformats-5.8.0</a></td>
   <td align="right">91kB</td>
   <td><tt>aa8a16046e106b5eacbb307fc7b11f3b742c0b86</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kinit-5.8.0.tar.xz">kinit-5.8.0</a></td>
   <td align="right">112kB</td>
   <td><tt>e15f8f1b8e37e471abf00d36104922def3903c99</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kio-5.8.0.tar.xz">kio-5.8.0</a></td>
   <td align="right">2.7MB</td>
   <td><tt>422122b6aa715472aad5ce86938940a241e1fbd0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kitemmodels-5.8.0.tar.xz">kitemmodels-5.8.0</a></td>
   <td align="right">361kB</td>
   <td><tt>e65692e84ee86a369888403431e2d489add858e6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kitemviews-5.8.0.tar.xz">kitemviews-5.8.0</a></td>
   <td align="right">72kB</td>
   <td><tt>483af2ff63a8de102522520e791240c6e77f9056</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kjobwidgets-5.8.0.tar.xz">kjobwidgets-5.8.0</a></td>
   <td align="right">86kB</td>
   <td><tt>8b97c8cf2a01d6ab387c77b5e14c52d18b443ff1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/knewstuff-5.8.0.tar.xz">knewstuff-5.8.0</a></td>
   <td align="right">400kB</td>
   <td><tt>1c39d9304ac2c7b467c81374253543b73e9f1b2f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/knotifications-5.8.0.tar.xz">knotifications-5.8.0</a></td>
   <td align="right">89kB</td>
   <td><tt>d7c784173c96fb598e61046cf6f818a8d2dd7d5d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/knotifyconfig-5.8.0.tar.xz">knotifyconfig-5.8.0</a></td>
   <td align="right">79kB</td>
   <td><tt>2b874c9adf980ed9e547918ea5779ac0310b4e52</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kpackage-5.8.0.tar.xz">kpackage-5.8.0</a></td>
   <td align="right">92kB</td>
   <td><tt>8882c10020f8763f4d40707da377bd24be9b24ef</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kparts-5.8.0.tar.xz">kparts-5.8.0</a></td>
   <td align="right">150kB</td>
   <td><tt>33d1529176fc5957c77128dc889b0963eb9698f2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kpeople-5.8.0.tar.xz">kpeople-5.8.0</a></td>
   <td align="right">52kB</td>
   <td><tt>b5d7c5cf182a11f53eeb346f70d885a957b97b66</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kplotting-5.8.0.tar.xz">kplotting-5.8.0</a></td>
   <td align="right">28kB</td>
   <td><tt>beddec71add49c48e76a3edd6e5574c8a61499a1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kpty-5.8.0.tar.xz">kpty-5.8.0</a></td>
   <td align="right">54kB</td>
   <td><tt>a0e7618c45cf6bfda002125cf1596efc2ba69812</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kservice-5.8.0.tar.xz">kservice-5.8.0</a></td>
   <td align="right">254kB</td>
   <td><tt>30c5d582efb0c21447c5b018df6af54a1c764f7a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/ktexteditor-5.8.0.tar.xz">ktexteditor-5.8.0</a></td>
   <td align="right">2.6MB</td>
   <td><tt>14e212af889efdbe2f2d0a099d9fad9fdaad44e9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/ktextwidgets-5.8.0.tar.xz">ktextwidgets-5.8.0</a></td>
   <td align="right">298kB</td>
   <td><tt>e05b62d147cea1a40a2d624b41fdc4d8466d79e1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kunitconversion-5.8.0.tar.xz">kunitconversion-5.8.0</a></td>
   <td align="right">595kB</td>
   <td><tt>9eb97efe7f66a4b3555a83bbff4a625a58e47619</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kwallet-5.8.0.tar.xz">kwallet-5.8.0</a></td>
   <td align="right">260kB</td>
   <td><tt>0e341586d72f98e04a38e8ddf7991247324e4884</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kwidgetsaddons-5.8.0.tar.xz">kwidgetsaddons-5.8.0</a></td>
   <td align="right">2.0MB</td>
   <td><tt>ef4b3987accfa7ff933ca577a846e27f8d1d8a4a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kwindowsystem-5.8.0.tar.xz">kwindowsystem-5.8.0</a></td>
   <td align="right">156kB</td>
   <td><tt>d49b39ed177f83bb2f40587a6897f899193e55c4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kxmlgui-5.8.0.tar.xz">kxmlgui-5.8.0</a></td>
   <td align="right">835kB</td>
   <td><tt>a4368e07119e498cba24c4a999b35443982490c9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/kxmlrpcclient-5.8.0.tar.xz">kxmlrpcclient-5.8.0</a></td>
   <td align="right">19kB</td>
   <td><tt>11eb0e30b860c3e308151c6ae740bb61e6bd0ddb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/networkmanager-qt-5.8.0.tar.xz">networkmanager-qt-5.8.0</a></td>
   <td align="right">149kB</td>
   <td><tt>8a783cad4a2d426ea3b676eb44c4acb1560ea9e4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/plasma-framework-5.8.0.tar.xz">plasma-framework-5.8.0</a></td>
   <td align="right">3.8MB</td>
   <td><tt>0473a56cc45192470effa18caf3884b22dbc8ace</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/solid-5.8.0.tar.xz">solid-5.8.0</a></td>
   <td align="right">257kB</td>
   <td><tt>cdc064fe1044e24f701454ec2b01689e69fc43d5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/sonnet-5.8.0.tar.xz">sonnet-5.8.0</a></td>
   <td align="right">266kB</td>
   <td><tt>fd7c2d3bbb87194841569c126cf76223147c656d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.8/threadweaver-5.8.0.tar.xz">threadweaver-5.8.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>83f20747d64cbfa3840132d0e73717bd10cacf99</tt></td>
</tr>

</table>
