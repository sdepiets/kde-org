<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha256 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/baloo-5.0.0.tar.xz">baloo-5.0.0</a></td>
   <td align="right">205kB</td>
   <td><tt>92b5de11db66fe6037606ef34687f78ca626afbee1dea27c3ceb842782dc3af8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/breeze-5.0.0.tar.xz">breeze-5.0.0</a></td>
   <td align="right">15MB</td>
   <td><tt>409ad1a98280a64ae81ea7cc718b3c2f3e6dfa37395b1c521c404b63fa1491e6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/kde-cli-tools-5.0.0.tar.xz">kde-cli-tools-5.0.0</a></td>
   <td align="right">456kB</td>
   <td><tt>ccf1f0c460ffe5148c8dbc12caad2be6a1c40fee27eab75ecd6c88c338be6c8b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/kfilemetadata-5.0.0.tar.xz">kfilemetadata-5.0.0</a></td>
   <td align="right">83kB</td>
   <td><tt>57b74e6315473c821759ee232b23073429bb3b8b51d1fe1cbcc4465c40a158be</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/khelpcenter-5.0.0.tar.xz">khelpcenter-5.0.0</a></td>
   <td align="right">1.9MB</td>
   <td><tt>6df48ec83522e235b6d7df3d1e64d7aaab89e5869473cc87141071de6fbba713</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/khotkeys-5.0.0.tar.xz">khotkeys-5.0.0</a></td>
   <td align="right">727kB</td>
   <td><tt>db20e117f9cff6608bfe5a3a4dc817cf84b3adde58ca8fde2b4e1dd1482d3267</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/kinfocenter-5.0.0.tar.xz">kinfocenter-5.0.0</a></td>
   <td align="right">1.0MB</td>
   <td><tt>2c45de42f52ba197c23765e786becd64a9ac0ce96285a985136fb54e2c6110c9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/kio-extras-5.0.0.tar.xz">kio-extras-5.0.0</a></td>
   <td align="right">563kB</td>
   <td><tt>e425ef3e6297ace11301e3af8e3e7e029b06948c8574be16fda460653da7c938</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/kmenuedit-5.0.0.tar.xz">kmenuedit-5.0.0</a></td>
   <td align="right">335kB</td>
   <td><tt>bea544a36ad43247b46191cb64005a301a9184b5f196b11944c412511920b018</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/ksysguard-5.0.0.tar.xz">ksysguard-5.0.0</a></td>
   <td align="right">432kB</td>
   <td><tt>37391afa3990c86247432c34499fe40068b6ee8432d6d343e99fdfdc1900f128</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/kwin-5.0.0.tar.xz">kwin-5.0.0</a></td>
   <td align="right">3.6MB</td>
   <td><tt>d4f247ddabb4f5fcf871dd5041ea0dd54ad88b86af228efd874d30050c457a79</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/kwrited-5.0.0.tar.xz">kwrited-5.0.0</a></td>
   <td align="right">18kB</td>
   <td><tt>a4abb9ddd20d197f063aa385a16dab474ac50a72e1982f7130f52ce7bc66037b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/libkscreen-5.0.91.tar.xz">libkscreen-5.0.91</a></td>
   <td align="right">55kB</td>
   <td><tt>d13a32d4eda3384808e54f90ed799741bf36638c5b862dba1d172f48ba4a670a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/libksysguard-5.0.0.tar.xz">libksysguard-5.0.0</a></td>
   <td align="right">537kB</td>
   <td><tt>03b4d0eaeb3ba9721393a4fec02fa96e0991de880a34d6fdaf4ab7011c36946d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/libmm-qt-5.0.91.tar.xz">libmm-qt-5.0.91</a></td>
   <td align="right">73kB</td>
   <td><tt>19e0247f7c4068e986450f365c3b6c5caefb00e203c9e69fbfb3d5fb94b858f2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/libnm-qt-5.0.91.tar.xz">libnm-qt-5.0.91</a></td>
   <td align="right">138kB</td>
   <td><tt>bb73a6d74d8407f2f295013a7d03f65b6e10f50e45e56e7a0d9f518afbc74506</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/milou-5.0.0.tar.xz">milou-5.0.0</a></td>
   <td align="right">32kB</td>
   <td><tt>2b1a46de2ce0c17c90331c6ecb61667bac607eca95ded96838320b64f3ae5a81</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/oxygen-5.0.0.tar.xz">oxygen-5.0.0</a></td>
   <td align="right">2.7MB</td>
   <td><tt>39fb046b8da35305e41e3d64980cbab90cd52ee5e476016c673750f682091df8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/oxygen-fonts-0.4.0.tar.xz">oxygen-fonts-0.4.0</a></td>
   <td align="right">529kB</td>
   <td><tt>03abb08f82aefd0adc0dff914e52ab0a6294a021daa952e143f035b7d8082eb2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/plasma-desktop-5.0.0.tar.xz">plasma-desktop-5.0.0</a></td>
   <td align="right">5.7MB</td>
   <td><tt>f34afc43c9cc4f1f0e9a66f9ee97e78789642e83daa12eb7996e960206381cb5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/plasma-nm-5.0.0.tar.xz">plasma-nm-5.0.0</a></td>
   <td align="right">432kB</td>
   <td><tt>0260cc5d880835c952585531d778141f74ae604a9574769e59acba337f7e58b2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/plasma-workspace-5.0.0.tar.xz">plasma-workspace-5.0.0</a></td>
   <td align="right">5.8MB</td>
   <td><tt>0bdfbefa751be5a1dcbf6e8ccf692bd296a42fb028090d82a336ec3a492a58fd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/plasma-workspace-wallpapers-5.0.0.tar.xz">plasma-workspace-wallpapers-5.0.0</a></td>
   <td align="right">27MB</td>
   <td><tt>ce526b892f5f539070e786d4ec839a1713b5bc6e97568bce6e9d7d650dc18b28</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/powerdevil-5.0.0.tar.xz">powerdevil-5.0.0</a></td>
   <td align="right">405kB</td>
   <td><tt>5127cc5082edc656a53ac602b8d8550ddb814d7931eb04820f971f278d8d422c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.0.0/systemsettings-5.0.0.tar.xz">systemsettings-5.0.0</a></td>
   <td align="right">132kB</td>
   <td><tt>e94fccce67bc77bf49d1f3cb59c8e89a0a8cc641ef92757e0c8ba0b6c4b70ca6</tt></td>
</tr>

</table>
