<?php
  $page_title = "KDE 3.1.5 Info Page";
  $site_root = "../";
  include "header.inc";
?>

<p>

KDE 3.1.5 was released on January 14, 2004. Read the <a
href="/announcements/announce-3.1.5.php">official announcement</a>.</p>

<?php include "unmaintained.inc" ?>

<!-- <h2>Updates</h2> -->

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#97;&#105;&#x6c;t&#111;&#058;s&#x65;&#x63;u&#0114;i&#x74;y&#64;kd&#x65;.org">securit&#0121;&#00064;kde&#46;&#00111;rg</a>.</p>

<ul>
<li>KDE 3.1.5 <em>corrects</em> a buffer overflow vulnerability
in the .VCF file information reader. Read the <a href="security/advisory-20040114-1.txt">detailed
advisory</a>.
</li>

<li>
The telnet, rlogin, ssh and mailto URI handlers in KDE do not check for '-' at the beginning of the hostname
passed, which makes it possible to pass an option to the programs started by the handlers.
Read the <a href="security/advisory-20040517-1.txt">detailed advisory</a>.
All versions of KDE up to KDE 3.2.2 are affected.
</li>
</ul>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
fixed after the release date:</p>

<ul>
<li>Currently none known</li>
</ul>

<p>Please check the <a href="http://bugs.kde.org/">bugs database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>FAQ</h2>

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed
<a href="http://konqueror.kde.org/faq/">to the Konqueror FAQ</a>
and sound related questions are answered <a
href="http://www.arts-project.org/doc/handbook/faq.html">in the FAQ of
the aRts Project</a>

<h2>Download and Installation</h2>

<p>
<u>Library Requirements</u>.
 <a href="/info/requirements/3.1.php">KDE 3.1 requires or benefits</a>
 from the given list of libraries, most of which should be already installed
 on your system or available from your OS CD or your vendor's website.
</p>
<p>
  The complete source code for KDE 3.1.5 is available for download:
</p>

<?php
include "source-3.1.5.inc"
?>

<!-- Comment the following if Konstruct is not up-to-date -->
<p>The <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> build toolset can help you
downloading and installing these tarballs.</p>

<u><a name="binary">Binary packages</a></u>

<p>
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.1.5 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.5/">download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p>
  At the time of this writing, pre-compiled packages are available for:
</p>

<?php
include "binary-3.1.5.inc"
?>

<p>
Additional binary packages might become available in the coming weeks,
as well as updates to the current packages.
</p>

<h2>Developer Info</h2>

If you need help porting your application to KDE 3.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/3.5/kdelibs/KDE3PORTING.html">
porting guide</a> or subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a>
to ask specific questions about porting your applications.

<p>There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html">architecture</a>
and the <a href="http://developer.kde.org/documentation/library/3.1-api/">
programming interface of KDE 3.1</a>.
</p>

<!-- END CONTENT -->
<?php
  include "footer.inc";
?>
