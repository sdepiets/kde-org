
<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha1 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/attica-5.7.0.tar.xz">attica-5.7.0</a></td>
   <td align="right">58kB</td>
   <td><tt>b5882fd95b867a0d74574bf90111a6c530506324</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/extra-cmake-modules-1.7.0.tar.xz">extra-cmake-modules-1.7.0</a></td>
   <td align="right">126kB</td>
   <td><tt>1a7e6085719e80202fe8077539457152d1f85d0e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/frameworkintegration-5.7.0.tar.xz">frameworkintegration-5.7.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>9f8b1f6c6d26208aeaa8bbae7634e89cef2037b7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kactivities-5.7.0.tar.xz">kactivities-5.7.0</a></td>
   <td align="right">110kB</td>
   <td><tt>f2e2f3d70bfc7cf95701b5b55036ca2ac9a2c52b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kapidox-5.7.0.tar.xz">kapidox-5.7.0</a></td>
   <td align="right">127kB</td>
   <td><tt>165fb954ad7dcb4a44db9a9847a4643ebf497d67</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/karchive-5.7.0.tar.xz">karchive-5.7.0</a></td>
   <td align="right">105kB</td>
   <td><tt>fda59fe0dd22fda0d658d38d4d3e66a5da67db21</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kauth-5.7.0.tar.xz">kauth-5.7.0</a></td>
   <td align="right">78kB</td>
   <td><tt>ed14fc03db7242037d5904cbe5efc5fec79b4738</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kbookmarks-5.7.0.tar.xz">kbookmarks-5.7.0</a></td>
   <td align="right">112kB</td>
   <td><tt>c53fc05106f5a7e73c0ddd741550a990c6ba5205</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kcmutils-5.7.0.tar.xz">kcmutils-5.7.0</a></td>
   <td align="right">227kB</td>
   <td><tt>5f1aa3e5fa77d4d57e6b7d1d42c9f470eecf9314</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kcodecs-5.7.0.tar.xz">kcodecs-5.7.0</a></td>
   <td align="right">215kB</td>
   <td><tt>39b06b112a03b184b907a3fc3d750307c9a55bbf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kcompletion-5.7.0.tar.xz">kcompletion-5.7.0</a></td>
   <td align="right">113kB</td>
   <td><tt>e7491ac49211eaf838d5741d2044dea1cd7d57fd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kconfig-5.7.0.tar.xz">kconfig-5.7.0</a></td>
   <td align="right">210kB</td>
   <td><tt>625b67cae786fda3d6bb6ac47ebf712cd36edc72</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kconfigwidgets-5.7.0.tar.xz">kconfigwidgets-5.7.0</a></td>
   <td align="right">343kB</td>
   <td><tt>32964981274ee475b3dfc5deef54a952a61f446c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kcoreaddons-5.7.0.tar.xz">kcoreaddons-5.7.0</a></td>
   <td align="right">291kB</td>
   <td><tt>c51566b30943be1b323f627fd68361a2c78227d4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kcrash-5.7.0.tar.xz">kcrash-5.7.0</a></td>
   <td align="right">19kB</td>
   <td><tt>56cbe4b857136df2feb4824ca0761683dbc9f933</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kdbusaddons-5.7.0.tar.xz">kdbusaddons-5.7.0</a></td>
   <td align="right">31kB</td>
   <td><tt>8d45e5384ff9b9094ab90b9a61a87ca2875c559b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kdeclarative-5.7.0.tar.xz">kdeclarative-5.7.0</a></td>
   <td align="right">105kB</td>
   <td><tt>ffc2eca80d9681e3d85e89aebe8f4f1eca98686e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kded-5.7.0.tar.xz">kded-5.7.0</a></td>
   <td align="right">32kB</td>
   <td><tt>e964111dce23af27f2ec364458441e661b3a80ea</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kdesignerplugin-5.7.0.tar.xz">kdesignerplugin-5.7.0</a></td>
   <td align="right">82kB</td>
   <td><tt>af7203acced37c08de79e196632c83fc67c68d14</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kdesu-5.7.0.tar.xz">kdesu-5.7.0</a></td>
   <td align="right">42kB</td>
   <td><tt>1e322cbfb17df443bd82a0fbb867faa3a3b257bc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kdewebkit-5.7.0.tar.xz">kdewebkit-5.7.0</a></td>
   <td align="right">28kB</td>
   <td><tt>1055d9683f318d3446b0ff10e6372ada37ee03d6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kdnssd-5.7.0.tar.xz">kdnssd-5.7.0</a></td>
   <td align="right">55kB</td>
   <td><tt>cb6652041f129a7b07ded6e568da4653cbfbefbd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kdoctools-5.7.0.tar.xz">kdoctools-5.7.0</a></td>
   <td align="right">381kB</td>
   <td><tt>ec8b4da9c68821ad29f1b82b5dcb1b77c9e352e8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kemoticons-5.7.0.tar.xz">kemoticons-5.7.0</a></td>
   <td align="right">71kB</td>
   <td><tt>8e51f7ecfd4d4ef3366c7cd697cea5006b49d624</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kglobalaccel-5.7.0.tar.xz">kglobalaccel-5.7.0</a></td>
   <td align="right">82kB</td>
   <td><tt>5b13d4dd3dd2232fffe5d2dde0b7a19c42d48d66</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kguiaddons-5.7.0.tar.xz">kguiaddons-5.7.0</a></td>
   <td align="right">38kB</td>
   <td><tt>a8c85b3201597bd5cb963261c5acbf9b146f76e1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/ki18n-5.7.0.tar.xz">ki18n-5.7.0</a></td>
   <td align="right">571kB</td>
   <td><tt>11f394cb6806fee293994cc561408f8f76ecc4d9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kiconthemes-5.7.0.tar.xz">kiconthemes-5.7.0</a></td>
   <td align="right">189kB</td>
   <td><tt>7b37327314cfbbb3a9b7d03c2d4d8eb14f629dfe</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kidletime-5.7.0.tar.xz">kidletime-5.7.0</a></td>
   <td align="right">22kB</td>
   <td><tt>470bf135ff060c2a33abee70e6f87b7238992c32</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kimageformats-5.7.0.tar.xz">kimageformats-5.7.0</a></td>
   <td align="right">90kB</td>
   <td><tt>941021dcaa72c87484ba151f7623654544b5cac1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kinit-5.7.0.tar.xz">kinit-5.7.0</a></td>
   <td align="right">112kB</td>
   <td><tt>7f69b4eaffc95f4c0c77892f6d01dc2f4ef32175</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kio-5.7.0.tar.xz">kio-5.7.0</a></td>
   <td align="right">2.7MB</td>
   <td><tt>9897aafc8c6e27f6e4d3112cead3db83f903897b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kitemmodels-5.7.0.tar.xz">kitemmodels-5.7.0</a></td>
   <td align="right">361kB</td>
   <td><tt>a765467f8729240aa24c852110c889c23b8f984e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kitemviews-5.7.0.tar.xz">kitemviews-5.7.0</a></td>
   <td align="right">72kB</td>
   <td><tt>0aae8a77ae7e406f053c755f64a137af208d0de6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kjobwidgets-5.7.0.tar.xz">kjobwidgets-5.7.0</a></td>
   <td align="right">87kB</td>
   <td><tt>c140908d3ce484ea65b609245754fa46fc541d0d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/knewstuff-5.7.0.tar.xz">knewstuff-5.7.0</a></td>
   <td align="right">400kB</td>
   <td><tt>8447b28d35416cc48344a44841412c6a647dbc99</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/knotifications-5.7.0.tar.xz">knotifications-5.7.0</a></td>
   <td align="right">89kB</td>
   <td><tt>4514c2e6168563242f254eed97d3aa1b47db0a6c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/knotifyconfig-5.7.0.tar.xz">knotifyconfig-5.7.0</a></td>
   <td align="right">77kB</td>
   <td><tt>95ebed9be15076e0eb595ad9b7bd46af696479d5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kpackage-5.7.0.tar.xz">kpackage-5.7.0</a></td>
   <td align="right">90kB</td>
   <td><tt>c27b5c62e1b424e10b477abb652f9d6807b97f35</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kparts-5.7.0.tar.xz">kparts-5.7.0</a></td>
   <td align="right">150kB</td>
   <td><tt>a133935e3a009ade8d8e2239261411a90a4adf19</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kplotting-5.7.0.tar.xz">kplotting-5.7.0</a></td>
   <td align="right">28kB</td>
   <td><tt>e402ae82a3f31d4584635c71b624f4a8706f40ab</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kpty-5.7.0.tar.xz">kpty-5.7.0</a></td>
   <td align="right">54kB</td>
   <td><tt>1078f0ac72b95d517dd19338b670013df043ee3d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kservice-5.7.0.tar.xz">kservice-5.7.0</a></td>
   <td align="right">254kB</td>
   <td><tt>8c8c5b31c523bb2eddae5c6d4c014fac9e5948fb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/ktexteditor-5.7.0.tar.xz">ktexteditor-5.7.0</a></td>
   <td align="right">2.6MB</td>
   <td><tt>44c63e326e906761c646732fbe3e9757acefbf8f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/ktextwidgets-5.7.0.tar.xz">ktextwidgets-5.7.0</a></td>
   <td align="right">298kB</td>
   <td><tt>50b532877ec7fefd8962576eb58390815337321f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kunitconversion-5.7.0.tar.xz">kunitconversion-5.7.0</a></td>
   <td align="right">595kB</td>
   <td><tt>e99483dab116df56f9131893e33dabca52d176ea</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kwallet-5.7.0.tar.xz">kwallet-5.7.0</a></td>
   <td align="right">260kB</td>
   <td><tt>9a297ff2812e71df83eeab8a1a5d46a11cb6dc31</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kwidgetsaddons-5.7.0.tar.xz">kwidgetsaddons-5.7.0</a></td>
   <td align="right">2.0MB</td>
   <td><tt>ef9e1f7ecd0f1d52deb90ff857c9eb629dc60d32</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kwindowsystem-5.7.0.tar.xz">kwindowsystem-5.7.0</a></td>
   <td align="right">156kB</td>
   <td><tt>079665ed2aaa4e0661ec8a744c6f866c486ea421</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/kxmlgui-5.7.0.tar.xz">kxmlgui-5.7.0</a></td>
   <td align="right">835kB</td>
   <td><tt>913c293578ee745af2ce64a28a92c0d4caf55ce5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/networkmanager-qt-5.7.0.tar.xz">networkmanager-qt-5.7.0</a></td>
   <td align="right">148kB</td>
   <td><tt>ac89d249814579bbac8d5cf1e7f38427c691a724</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/plasma-framework-5.7.0.tar.xz">plasma-framework-5.7.0</a></td>
   <td align="right">3.8MB</td>
   <td><tt>59fe0cca3b2881820872b4c1715cf3ee0a0d936e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/solid-5.7.0.tar.xz">solid-5.7.0</a></td>
   <td align="right">255kB</td>
   <td><tt>5e1e2f9ff4f45806fd9b5e65ee394446697ee358</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/sonnet-5.7.0.tar.xz">sonnet-5.7.0</a></td>
   <td align="right">266kB</td>
   <td><tt>06da89b3a1553bd1fe9083141ab4c08a34b30c22</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.7/threadweaver-5.7.0.tar.xz">threadweaver-5.7.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>588fad984288ff54e5d5bc288cfe4f789422f793</tt></td>
</tr>

</table>
