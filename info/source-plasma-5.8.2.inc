<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha256 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/bluedevil-5.8.2.tar.xz">bluedevil-5.8.2</a></td>
   <td align="right">137kB</td>
   <td><tt>cd6a3629703313e8ca66c927e629afd550a7fbc259b3e83187c8a923e0860bd5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/breeze-5.8.2.tar.xz">breeze-5.8.2</a></td>
   <td align="right">31MB</td>
   <td><tt>680ea6e0c4a1d205b07a59ce0425a2fc991fa8e9665559dee567bdaeb6b007d9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/breeze-grub-5.8.2.tar.xz">breeze-grub-5.8.2</a></td>
   <td align="right">2.9MB</td>
   <td><tt>c1d55c017f6278ef0601368c9f23e5801abfcd4f33b936e9315faabefb4191e0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/breeze-gtk-5.8.2.tar.xz">breeze-gtk-5.8.2</a></td>
   <td align="right">206kB</td>
   <td><tt>f36c98a0f1d41c6c6084341f26f179439456a308526d665a970cd91da03db6ed</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/breeze-plymouth-5.8.2.tar.xz">breeze-plymouth-5.8.2</a></td>
   <td align="right">92kB</td>
   <td><tt>19a5496b905c36e8fd4100623372b3e70080972ef6cb9af95eeefe16070477b6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/discover-5.8.2.tar.xz">discover-5.8.2</a></td>
   <td align="right">8.7MB</td>
   <td><tt>045c60e726e840b4a5ac31007b22fd1fa9534a289f68cd2c66f98cd84c6ef503</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kactivitymanagerd-5.8.2.tar.xz">kactivitymanagerd-5.8.2</a></td>
   <td align="right">80kB</td>
   <td><tt>4c53828d5ab9f2beab592b8690c769c8af20ee72af298d12a1e0b7d9dbededb5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kde-cli-tools-5.8.2.tar.xz">kde-cli-tools-5.8.2</a></td>
   <td align="right">474kB</td>
   <td><tt>8a2d9095112ac3d7030af4b562e7537cffccd93475921d58ef322a77273497bf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kdecoration-5.8.2.tar.xz">kdecoration-5.8.2</a></td>
   <td align="right">34kB</td>
   <td><tt>c69c6bf3478a1633a86597de1158eef937c6a1cd8a1eb6437428e834c7377171</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kde-gtk-config-5.8.2.tar.xz">kde-gtk-config-5.8.2</a></td>
   <td align="right">146kB</td>
   <td><tt>4731d0e772578f719c124b3c7f4bb427f1a98dac97c77bdd953ff4703c37769a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kdeplasma-addons-5.8.2.tar.xz">kdeplasma-addons-5.8.2</a></td>
   <td align="right">1.9MB</td>
   <td><tt>18db83b75ef8122c18f14ad769c4fc86cdafa1947a7b10eaa393e4cda350a9a1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kgamma5-5.8.2.tar.xz">kgamma5-5.8.2</a></td>
   <td align="right">59kB</td>
   <td><tt>769777585d87dbca1ce475882ce3e595df808e24045cae3ce967ef9b95e0e350</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/khotkeys-5.8.2.tar.xz">khotkeys-5.8.2</a></td>
   <td align="right">590kB</td>
   <td><tt>452d824790d7b3de20701d852678ca7966704faa9842a7da64e04694bca5959d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kinfocenter-5.8.2.tar.xz">kinfocenter-5.8.2</a></td>
   <td align="right">1.1MB</td>
   <td><tt>c52037d88850ec8237eae7fda3e4c9e4bbedf7e5ac02e7da205e1ffe991b34c8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kmenuedit-5.8.2.tar.xz">kmenuedit-5.8.2</a></td>
   <td align="right">637kB</td>
   <td><tt>41f4aa6ee77318d64a228fd4c501707af771266744fe5b032d22557c85264164</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kscreen-5.8.2.tar.xz">kscreen-5.8.2</a></td>
   <td align="right">110kB</td>
   <td><tt>99cfd99f1c5631894fe0f83a759b6edf2a87e7668acf85860f8d60aa2eff220e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kscreenlocker-5.8.2.tar.xz">kscreenlocker-5.8.2</a></td>
   <td align="right">105kB</td>
   <td><tt>e17abef1b648a6ed77d5646d3378a44273ddd9d4a06c2747fbbf2a4f0a145e55</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/ksshaskpass-5.8.2.tar.xz">ksshaskpass-5.8.2</a></td>
   <td align="right">18kB</td>
   <td><tt>7a00622a12993d14d68e3def143a4291e32cef766e3e0b976e969e4c799668f2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/ksysguard-5.8.2.tar.xz">ksysguard-5.8.2</a></td>
   <td align="right">476kB</td>
   <td><tt>d6ee1f2b83471c6e5cd3774c7a4b1e86c9a42d779f37c551d8721dbb8011cfd7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kwallet-pam-5.8.2.tar.xz">kwallet-pam-5.8.2</a></td>
   <td align="right">17kB</td>
   <td><tt>4b0bc61aec955815924439eec10f9a9e6c79819776f9e02076924f42b26788c7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kwayland-integration-5.8.2.tar.xz">kwayland-integration-5.8.2</a></td>
   <td align="right">17kB</td>
   <td><tt>ea064b1dfd33dd52e6b7c3068ef5498e5da95ff1aefa03c6fdc59f104270f58b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kwin-5.8.2.tar.xz">kwin-5.8.2</a></td>
   <td align="right">3.7MB</td>
   <td><tt>1e014a4fb7c197fa3bfb3591f064e33065d9bceafba843897bf720d2d65e8911</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/kwrited-5.8.2.tar.xz">kwrited-5.8.2</a></td>
   <td align="right">19kB</td>
   <td><tt>43608730165073da39fd462f5d043a01035ba35de177987564102896073aa998</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/libkscreen-5.8.2.tar.xz">libkscreen-5.8.2</a></td>
   <td align="right">90kB</td>
   <td><tt>ecdc4d514e3e19403d3db16da4b9cdd639cce98396cd6fb404b8b391f783565c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/libksysguard-5.8.2.tar.xz">libksysguard-5.8.2</a></td>
   <td align="right">558kB</td>
   <td><tt>d42456a89bcbd6e5198489fd2105688d436e212a4c43d8d5e0ebe9bb38181695</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/milou-5.8.2.tar.xz">milou-5.8.2</a></td>
   <td align="right">53kB</td>
   <td><tt>091399bd1f20e8a15bc9f107282f5c336f95503bc9d206b3319b3431bb506b46</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/oxygen-5.8.2.tar.xz">oxygen-5.8.2</a></td>
   <td align="right">4.2MB</td>
   <td><tt>59e18bee1baaca6e07d10154098765f59f74670474db645a1b25817aec730fd8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/plasma-desktop-5.8.2.tar.xz">plasma-desktop-5.8.2</a></td>
   <td align="right">6.0MB</td>
   <td><tt>127be446ffbb48bd3aa07e27cc799e7ee33da079917fb1cf550c4cebc6e24300</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/plasma-integration-5.8.2.tar.xz">plasma-integration-5.8.2</a></td>
   <td align="right">49kB</td>
   <td><tt>2e13ec9fc4fd9d61e0328fb96218d42462ed79ca04134e760c4c517a6469f7fc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/plasma-nm-5.8.2.tar.xz">plasma-nm-5.8.2</a></td>
   <td align="right">628kB</td>
   <td><tt>4260fbcd437b899eb17001f58a0a3973131faab2dc0702dab8ef9cf49186b60c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/plasma-pa-5.8.2.tar.xz">plasma-pa-5.8.2</a></td>
   <td align="right">67kB</td>
   <td><tt>11fa2025e3326a2c49d54d5291adf56a2395ee4beb9e5c6388e7409917c1a5c8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/plasma-sdk-5.8.2.tar.xz">plasma-sdk-5.8.2</a></td>
   <td align="right">698kB</td>
   <td><tt>be247c71ee89dc8b1d10d408e77e5586fa3551d0dfdb6470eb4057def148c340</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/plasma-tests-5.8.2.tar.xz">plasma-tests-5.8.2</a></td>
   <td align="right">1kB</td>
   <td><tt>c573a0519585ab1370219bc1d3fff46f2e6065b009d16cd1ea69f0bd54af4853</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/plasma-workspace-5.8.2.tar.xz">plasma-workspace-5.8.2</a></td>
   <td align="right">6.6MB</td>
   <td><tt>84f48fecb68b9061d1f12d58f00207e3d0718a7ef8a424d381e1cd8dd3efbbb7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/plasma-workspace-wallpapers-5.8.2.tar.xz">plasma-workspace-wallpapers-5.8.2</a></td>
   <td align="right">43MB</td>
   <td><tt>b167834a2e716f0f47966d749e306cad661e8c59f56b6ae463f13d6303bd7372</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/polkit-kde-agent-1-5.8.2.tar.xz">polkit-kde-agent-1-5.8.2</a></td>
   <td align="right">39kB</td>
   <td><tt>0e71455004718a67f5c44ae1c1d743000de898d4dfe1aabca5883582fb88f4c6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/powerdevil-5.8.2.tar.xz">powerdevil-5.8.2</a></td>
   <td align="right">347kB</td>
   <td><tt>fdc8a5f43c27ebfa78940de031b113880ea6213e47b33b3e233ed2351c02532e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/sddm-kcm-5.8.2.tar.xz">sddm-kcm-5.8.2</a></td>
   <td align="right">47kB</td>
   <td><tt>a3cb792e8ed07eac190e292642dbffb31023eeebc4ddf9fee7b1ebcc251479d8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/systemsettings-5.8.2.tar.xz">systemsettings-5.8.2</a></td>
   <td align="right">154kB</td>
   <td><tt>71b3bf4496de31e42dd180b8fd9e48772698f1221c652913328bc07c9db2f394</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.2/user-manager-5.8.2.tar.xz">user-manager-5.8.2</a></td>
   <td align="right">529kB</td>
   <td><tt>2139e68bd23d5f9d84fc2c1a6e7a342a770ec060112bbb79328c6b9affc6848c</tt></td>
</tr>

</table>
