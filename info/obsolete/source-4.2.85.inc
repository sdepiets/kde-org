<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdeaccessibility-4.2.85.tar.bz2">kdeaccessibility-4.2.85</a></td><td align="right">6,3MB</td><td><tt>4be21a16e24cd1dd8d9325e483ee42c2d63ca280</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdeadmin-4.2.85.tar.bz2">kdeadmin-4.2.85</a></td><td align="right">1,8MB</td><td><tt>7e04ca2e0069ae39a64d80d84a77c7e1f9ac0ba2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdeartwork-4.2.85.tar.bz2">kdeartwork-4.2.85</a></td><td align="right">44MB</td><td><tt>b16db7715c391296dc986aac59b5a7b635236c96</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdebase-4.2.85.tar.bz2">kdebase-4.2.85</a></td><td align="right">4,1MB</td><td><tt>c0cf8e5a23e74eea8eecf5c640176b6f86e2532e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdebase-runtime-4.2.85.tar.bz2">kdebase-runtime-4.2.85</a></td><td align="right">6,4MB</td><td><tt>a8567b619b0ff5f40e4bd23df97b3e2c392ab7e7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdebase-workspace-4.2.85.tar.bz2">kdebase-workspace-4.2.85</a></td><td align="right">50MB</td><td><tt>c4e2de0dad0b5b5c7f096f5622abe737af2412f2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdeedu-4.2.85.tar.bz2">kdeedu-4.2.85</a></td><td align="right">58MB</td><td><tt>e814fe66d00807694669c5a54337bde6c1797f68</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdegames-4.2.85.tar.bz2">kdegames-4.2.85</a></td><td align="right">50MB</td><td><tt>5f5935ef6e8368a148b6a38eefb4c593e6436c8f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdegraphics-4.2.85.tar.bz2">kdegraphics-4.2.85</a></td><td align="right">3,5MB</td><td><tt>cbfd026213d9f2f10d3e3f04e9c83319f65d41fc</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdelibs-4.2.85.tar.bz2">kdelibs-4.2.85</a></td><td align="right">9,6MB</td><td><tt>228fea238efb857bcbd95c7f0ec758c48d6c7628</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdelibs-experimental-4.2.85.tar.bz2">kdelibs-experimental-4.2.85</a></td><td align="right">16KB</td><td><tt>285791dee06a10342418a50eba78c7acbeb6c98a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdemultimedia-4.2.85.tar.bz2">kdemultimedia-4.2.85</a></td><td align="right">1,6MB</td><td><tt>8915aa85b9a6d306bb6fd8b6035efacc18e34585</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdenetwork-4.2.85.tar.bz2">kdenetwork-4.2.85</a></td><td align="right">7,1MB</td><td><tt>fc005ab86b451e0a5ce731c3129b924ee779ec2d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdepim-4.2.85.tar.bz2">kdepim-4.2.85</a></td><td align="right">12MB</td><td><tt>6a22f38662aba32d17899dc41e117dda520a3efd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdepimlibs-4.2.85.tar.bz2">kdepimlibs-4.2.85</a></td><td align="right">1,7MB</td><td><tt>d8625f22e72ba89ec18744d6ba3626f094c81b99</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdeplasma-addons-4.2.85.tar.bz2">kdeplasma-addons-4.2.85</a></td><td align="right">4,6MB</td><td><tt>7f2d851246af88a3453654607c245a22468814df</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdesdk-4.2.85.tar.bz2">kdesdk-4.2.85</a></td><td align="right">5,4MB</td><td><tt>186c788bfaa5e1399b8fe16405ef7b766a6e0f4d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdetoys-4.2.85.tar.bz2">kdetoys-4.2.85</a></td><td align="right">1,3MB</td><td><tt>8c8a7ad81e413e45b093dfabaad7a34549d9c941</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdeutils-4.2.85.tar.bz2">kdeutils-4.2.85</a></td><td align="right">2,5MB</td><td><tt>9b9be7cd67df440500a74a2d151a83453a703549</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/kdewebdev-4.2.85.tar.bz2">kdewebdev-4.2.85</a></td><td align="right">3,3MB</td><td><tt>501d942bc0bbf458b6ab297a32224cb05417423b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.2.85/src/oxygen-icons-4.2.85.tar.bz2">oxygen-icons-4.2.85</a></td><td align="right">75MB</td><td><tt>5d905bc0ed488c2c8db4208b8be4d3f9d94c5740</tt></td></tr>
</table>
