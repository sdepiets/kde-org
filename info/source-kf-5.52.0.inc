
<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha1 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/attica-5.52.0.tar.xz">attica-5.52.0</a></td>
   <td align="right">61kB</td>
   <td><tt>ecc3d908a86acd01d2faa2dc83544bb09a1244fe</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/baloo-5.52.0.tar.xz">baloo-5.52.0</a></td>
   <td align="right">229kB</td>
   <td><tt>f282ee3a1b2c1165556f0c3c5cab3ab3ccf14d29</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/bluez-qt-5.52.0.tar.xz">bluez-qt-5.52.0</a></td>
   <td align="right">78kB</td>
   <td><tt>1092422fc0e8001609e6da88c66572a0b11814e5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/breeze-icons-5.52.0.tar.xz">breeze-icons-5.52.0</a></td>
   <td align="right">1.4MB</td>
   <td><tt>1715a031e90ef793588e32de450484f71b3cf3bf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/extra-cmake-modules-5.52.0.tar.xz">extra-cmake-modules-5.52.0</a></td>
   <td align="right">316kB</td>
   <td><tt>f0ffb027159ef35e514178f05c9aca0f6f59b2fc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/frameworkintegration-5.52.0.tar.xz">frameworkintegration-5.52.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>cd84311dbe93b784a5fa763af0b15b1adbf581f5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kactivities-5.52.0.tar.xz">kactivities-5.52.0</a></td>
   <td align="right">59kB</td>
   <td><tt>e72fb4a49c6c4600e5988a6901deb28204ea9f0c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kactivities-stats-5.52.0.tar.xz">kactivities-stats-5.52.0</a></td>
   <td align="right">59kB</td>
   <td><tt>9fe6b41d735f196b7f9e27ded73080f8e0d57bbf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kapidox-5.52.0.tar.xz">kapidox-5.52.0</a></td>
   <td align="right">387kB</td>
   <td><tt>4b50ac15e4482f8b327f8b41d9797f7c2206663f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/karchive-5.52.0.tar.xz">karchive-5.52.0</a></td>
   <td align="right">429kB</td>
   <td><tt>652f6b99a10187cf242fac1ef0a490d732a82dee</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kauth-5.52.0.tar.xz">kauth-5.52.0</a></td>
   <td align="right">82kB</td>
   <td><tt>a3f22c3252ee194f878ece4d64c14587b9d7a9f0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kbookmarks-5.52.0.tar.xz">kbookmarks-5.52.0</a></td>
   <td align="right">115kB</td>
   <td><tt>44a079ad881446adcf605e739007d9ad9625f8ae</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kcmutils-5.52.0.tar.xz">kcmutils-5.52.0</a></td>
   <td align="right">231kB</td>
   <td><tt>9aa68ca3550d5a0a9a8bcde8f42749cc43aecaf5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kcodecs-5.52.0.tar.xz">kcodecs-5.52.0</a></td>
   <td align="right">214kB</td>
   <td><tt>536932b5f2ae2a9c30911092b3a67f86c550253f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kcompletion-5.52.0.tar.xz">kcompletion-5.52.0</a></td>
   <td align="right">112kB</td>
   <td><tt>f98bddcc02bcafd0ec58184e5ccb2af72a449a5e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kconfig-5.52.0.tar.xz">kconfig-5.52.0</a></td>
   <td align="right">232kB</td>
   <td><tt>d6471047bf7f0f5ebb15038928a2e1f213b43666</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kconfigwidgets-5.52.0.tar.xz">kconfigwidgets-5.52.0</a></td>
   <td align="right">364kB</td>
   <td><tt>abb7ed6e6d92eea7f093a2b6d850e92e71b65f30</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kcoreaddons-5.52.0.tar.xz">kcoreaddons-5.52.0</a></td>
   <td align="right">343kB</td>
   <td><tt>1c5d483aa0e582c602e5a83a6aee39591c8ed095</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kcrash-5.52.0.tar.xz">kcrash-5.52.0</a></td>
   <td align="right">21kB</td>
   <td><tt>0ad8b105de857c8da1df68ec8913bb6d43ac12ab</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kdbusaddons-5.52.0.tar.xz">kdbusaddons-5.52.0</a></td>
   <td align="right">36kB</td>
   <td><tt>b03df5103b0e7fee1134b7904a6d544ead81b500</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kdeclarative-5.52.0.tar.xz">kdeclarative-5.52.0</a></td>
   <td align="right">168kB</td>
   <td><tt>b2351cf502dac86332ad4146f47f76cf322521d7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kded-5.52.0.tar.xz">kded-5.52.0</a></td>
   <td align="right">37kB</td>
   <td><tt>b1cf969bd54a1f535d576bd32108cea6987ed2a2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kdesignerplugin-5.52.0.tar.xz">kdesignerplugin-5.52.0</a></td>
   <td align="right">88kB</td>
   <td><tt>16e15cb41c74c3d9e872b1f47676a442fb4b508c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kdesu-5.52.0.tar.xz">kdesu-5.52.0</a></td>
   <td align="right">43kB</td>
   <td><tt>4da01c68bb3e69ca92712d1cb86158befbe3c78f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kdewebkit-5.52.0.tar.xz">kdewebkit-5.52.0</a></td>
   <td align="right">28kB</td>
   <td><tt>fee17fe11fea4f371c7719527844e97f66330795</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kdnssd-5.52.0.tar.xz">kdnssd-5.52.0</a></td>
   <td align="right">55kB</td>
   <td><tt>88e88518d1d3fe98d339ed6f5600d889289998d2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kdoctools-5.52.0.tar.xz">kdoctools-5.52.0</a></td>
   <td align="right">414kB</td>
   <td><tt>ffcbe9f57f01ab348fac97a983923c35c86d4cd3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kemoticons-5.52.0.tar.xz">kemoticons-5.52.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>6bf0e5b17b341a05e17d9b13a4f251cf9a2d2a95</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kfilemetadata-5.52.0.tar.xz">kfilemetadata-5.52.0</a></td>
   <td align="right">285kB</td>
   <td><tt>75aa6d4f7963ee819744cf77bead61cd0330a448</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kglobalaccel-5.52.0.tar.xz">kglobalaccel-5.52.0</a></td>
   <td align="right">81kB</td>
   <td><tt>2255156d57d632faddbec49d511d7b1bf5b42247</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kguiaddons-5.52.0.tar.xz">kguiaddons-5.52.0</a></td>
   <td align="right">39kB</td>
   <td><tt>a36691de74e42c9b02f862220aa009f7c36d902f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kholidays-5.52.0.tar.xz">kholidays-5.52.0</a></td>
   <td align="right">197kB</td>
   <td><tt>471481184c0650f33b4e195653e6bfe3f7bef484</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/ki18n-5.52.0.tar.xz">ki18n-5.52.0</a></td>
   <td align="right">573kB</td>
   <td><tt>b5ed9459ec9ee3a81d618445ec2b14d83ea61d33</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kiconthemes-5.52.0.tar.xz">kiconthemes-5.52.0</a></td>
   <td align="right">202kB</td>
   <td><tt>26e4fd47cc692f27208e047d5d5a4176ffb8a665</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kidletime-5.52.0.tar.xz">kidletime-5.52.0</a></td>
   <td align="right">25kB</td>
   <td><tt>cc4ee1c4e2aa036763e1d11f3f306c73f9d13b13</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kimageformats-5.52.0.tar.xz">kimageformats-5.52.0</a></td>
   <td align="right">199kB</td>
   <td><tt>d2c1b1a13d82390a1cdc49436c47f17c18cf4797</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kinit-5.52.0.tar.xz">kinit-5.52.0</a></td>
   <td align="right">117kB</td>
   <td><tt>3ac8beee48acd5243bd2138f504a0b8e4754e55c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kio-5.52.0.tar.xz">kio-5.52.0</a></td>
   <td align="right">3.0MB</td>
   <td><tt>722c248926c996c9a369190dbfc73bb1fbbea0aa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kirigami2-5.52.0.tar.xz">kirigami2-5.52.0</a></td>
   <td align="right">118kB</td>
   <td><tt>6a6d205acf45a1d94e0e6243d2488b5917b0dbd5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kitemmodels-5.52.0.tar.xz">kitemmodels-5.52.0</a></td>
   <td align="right">381kB</td>
   <td><tt>36217dc8005f4b7bb1b2881b40b97380b1679fe8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kitemviews-5.52.0.tar.xz">kitemviews-5.52.0</a></td>
   <td align="right">73kB</td>
   <td><tt>238d374853364f67c521b9d3da54bcbaf2d52443</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kjobwidgets-5.52.0.tar.xz">kjobwidgets-5.52.0</a></td>
   <td align="right">85kB</td>
   <td><tt>bb30a3e29e018168f5c0d56f0dca25f270f1964c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/knewstuff-5.52.0.tar.xz">knewstuff-5.52.0</a></td>
   <td align="right">984kB</td>
   <td><tt>a06a2be00e165ad122024ea13099d4cf995b1031</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/knotifications-5.52.0.tar.xz">knotifications-5.52.0</a></td>
   <td align="right">100kB</td>
   <td><tt>a93c57d5fc0fd57705e7c77738546d5a252a01f9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/knotifyconfig-5.52.0.tar.xz">knotifyconfig-5.52.0</a></td>
   <td align="right">81kB</td>
   <td><tt>29dfc1c7ba5d2ce29a02703b07654f2ae6498584</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kpackage-5.52.0.tar.xz">kpackage-5.52.0</a></td>
   <td align="right">129kB</td>
   <td><tt>048f196def2a72ffe58d2b0873832ae4a8caa2e1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kparts-5.52.0.tar.xz">kparts-5.52.0</a></td>
   <td align="right">171kB</td>
   <td><tt>1765caf3829c577dd01c30428c768bd3ae32f75f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kpeople-5.52.0.tar.xz">kpeople-5.52.0</a></td>
   <td align="right">58kB</td>
   <td><tt>81c4940442b3b97ac7b7de97cef166def8d08c45</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kplotting-5.52.0.tar.xz">kplotting-5.52.0</a></td>
   <td align="right">29kB</td>
   <td><tt>b735114a2e9116c60b323e41a2cebe68783aa470</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kpty-5.52.0.tar.xz">kpty-5.52.0</a></td>
   <td align="right">56kB</td>
   <td><tt>775b7a7e41099471d41df229fc241e12d63354da</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/krunner-5.52.0.tar.xz">krunner-5.52.0</a></td>
   <td align="right">60kB</td>
   <td><tt>3272e5fdbad4a18da5d0866cb93f3ed4ebfacd57</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kservice-5.52.0.tar.xz">kservice-5.52.0</a></td>
   <td align="right">244kB</td>
   <td><tt>8437e1bbe2c9b162796d7cb4d0affdae065f6c30</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/ktexteditor-5.52.0.tar.xz">ktexteditor-5.52.0</a></td>
   <td align="right">2.1MB</td>
   <td><tt>c7a657266f317161e4f5bb763118967ccfecf178</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/ktextwidgets-5.52.0.tar.xz">ktextwidgets-5.52.0</a></td>
   <td align="right">301kB</td>
   <td><tt>c55cecc9d81656b123a9ed6f2922fc8dc6f40452</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kunitconversion-5.52.0.tar.xz">kunitconversion-5.52.0</a></td>
   <td align="right">749kB</td>
   <td><tt>954b48536463ddc377e9a69181b303bd6b345871</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kwallet-5.52.0.tar.xz">kwallet-5.52.0</a></td>
   <td align="right">300kB</td>
   <td><tt>05174bdf83cda6f5346573408942b95cb73843d7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kwayland-5.52.0.tar.xz">kwayland-5.52.0</a></td>
   <td align="right">325kB</td>
   <td><tt>177807600016dad7bb9d64b79faba1919803a89e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kwidgetsaddons-5.52.0.tar.xz">kwidgetsaddons-5.52.0</a></td>
   <td align="right">2.0MB</td>
   <td><tt>2725a09554c48e19f8091a35fc80303437c80e2e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kwindowsystem-5.52.0.tar.xz">kwindowsystem-5.52.0</a></td>
   <td align="right">165kB</td>
   <td><tt>fea1746ee3963a87bd0ba71745ef36f0282364ca</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kxmlgui-5.52.0.tar.xz">kxmlgui-5.52.0</a></td>
   <td align="right">848kB</td>
   <td><tt>9ac51a4c2caf97335231d2fedb548cb6a892e602</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/kxmlrpcclient-5.52.0.tar.xz">kxmlrpcclient-5.52.0</a></td>
   <td align="right">28kB</td>
   <td><tt>d0bba33153ce3e0f0509215d9385e1b710a6d0da</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/modemmanager-qt-5.52.0.tar.xz">modemmanager-qt-5.52.0</a></td>
   <td align="right">98kB</td>
   <td><tt>0c3cd36273717dbb0122633a8e54c2cf6d344e93</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/networkmanager-qt-5.52.0.tar.xz">networkmanager-qt-5.52.0</a></td>
   <td align="right">162kB</td>
   <td><tt>1b35f0e881c5ebfbe2df48b6ce015dc5bdc712aa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/oxygen-icons5-5.52.0.tar.xz">oxygen-icons5-5.52.0</a></td>
   <td align="right">225MB</td>
   <td><tt>df0c9cd1ca335eb271171621a337ce7b8fe5e5d5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/plasma-framework-5.52.0.tar.xz">plasma-framework-5.52.0</a></td>
   <td align="right">4.4MB</td>
   <td><tt>57637c3ee98d66a575f99ecd5ec914a904264299</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/prison-5.52.0.tar.xz">prison-5.52.0</a></td>
   <td align="right">40kB</td>
   <td><tt>1ce8a93dfcf9be569fa73c7e0fd3df39032c69ff</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/purpose-5.52.0.tar.xz">purpose-5.52.0</a></td>
   <td align="right">138kB</td>
   <td><tt>8d372b8104efaa46829caa74d485961c8795325e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/qqc2-desktop-style-5.52.0.tar.xz">qqc2-desktop-style-5.52.0</a></td>
   <td align="right">40kB</td>
   <td><tt>fbb452ec6176b4f018ee175331bed23edbd23032</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/solid-5.52.0.tar.xz">solid-5.52.0</a></td>
   <td align="right">252kB</td>
   <td><tt>0e697e30391ceb029c32ead8284c8b4a621e1b04</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/sonnet-5.52.0.tar.xz">sonnet-5.52.0</a></td>
   <td align="right">278kB</td>
   <td><tt>e9e3df825daf281efb50a61575b8b075f9a42bf8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/syndication-5.52.0.tar.xz">syndication-5.52.0</a></td>
   <td align="right">155kB</td>
   <td><tt>d6f9f53282ee4c5298ed1e706fb289452d6e9f29</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/syntax-highlighting-5.52.0.tar.xz">syntax-highlighting-5.52.0</a></td>
   <td align="right">1.2MB</td>
   <td><tt>f9129e841d276fbb7560f9a62b47d4b7469fe0b3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.52/threadweaver-5.52.0.tar.xz">threadweaver-5.52.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>e2eae6453d189230bbfd91ff7f19f4cccf19d924</tt></td>
</tr>

</table>
