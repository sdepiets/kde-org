<?php
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Press Contact"
    ]);

    require('../aether/header.php');
    $site_root = "../";
?>

<main class="container">

<h1>Download the KDE Press Kit</h1>

<p>If you would like to learn more about KDE, represent us in the media, or create content about KDE, our Press Kit can be a valuable resource.</p>

<p>The Press Kit contains a booklet with written guidelines and resources, and a selection of images that can be freely used in any KDE-related content.</p>

<h3><a href="https://share.kde.org/s/XgobC4pWK5LxXFt">Download the latest KDE Press Kit here.</a></h3>

<h1>Press Contact</h1>

<h3>Get in touch with KDE's Press Room at <a href="mailto:press@kde.org">press@kde.org</a></h3>

<p>The people working in KDE's Press Room can help you or your media organisation arrange an interview with KDE developers and contributors, obtain further information about projects and people within KDE, and officially contact the KDE Project. We can also provide you with statements, stories, quotes, graphical material and videos for your outlet.</p>

<p>Unfortunately we cannot provide you with technical support, but we know who can:</p>

<ul>
<li>If you have problems with the KDE.org website, e-mail <a href="mailto:webmaster@kde.org">webmaster@kde.org</a>.</li>

<li>If you need technical support, get in touch with your operating system vendor or subscribe to one of our  <a href="/mailinglists/">mailing lists</a> and ask there. You can also check <a href="http://forum.kde.org" rel="nofollow">our forums</a> for help.</li>

<li>If you are a lawyer and need contact information as required by German law, go to the <a href="/community/whatiskde/impressum-en.php">Imprint</a> (<a href="/community/whatiskde/impressum.php" lang="de">German Impressum</a>).</li>
</ul>

</main>
<?php
  require('../aether/footer.php');
