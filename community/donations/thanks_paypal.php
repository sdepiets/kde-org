<?php
    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Donation received - Thank you!"
    ]);

    require('../../aether/header.php');
    $site_root = "../../";
?>

<main class="container">

<h1>Donations</h1>

<p>
Thank you very much for your donation to the KDE project!
</p>
<p>
Remember you can become a "KDE Supporting Member" by doing recurring donations. Learn more at <a href="https://relate.kde.org/">https://relate.kde.org/</a>.
</p>
<p>
You can see your donation in the <a href="previousdonations.php">list</a>.
</p>

</main>
<?php
  require('../../aether/footer.php');
