<?php
    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Donations"
    ]);

    require('../../aether/header.php');
    $site_root = "../../";
?>

<main class="container">

<h1>Donations</h1>
<a id="why"></a>

<p>Donating is the easiest and fastest way to efficiently support KDE and its projects. KDE projects are available for free therefore your donation is needed to cover things that require money like servers, contributor meetings, etc.</p>

<p><a href="http://ev.kde.org">KDE e.V.</a> is the non-profit organization behind the KDE community. It is based in Germany and thus donations are <a href="https://ev.kde.org/donations-taxes-de.php">tax deductible in Germany</a>. If you live in another EU country your donation might also be tax deductible. Please consult your tax advisor for details.</p>

<script type="text/javascript">
    function showOneTime() {
        document.getElementById('recurring').style.display = "none";
        document.getElementById('onetime').style.display = "inline";
        document.getElementById('recurringtab').style.background = "#c3c3c3";
        document.getElementById('onetimetab').style.background = "#E8E8E8";
    }
    function showRecurring() {
        document.getElementById('recurring').style.display = "inline";
        document.getElementById('onetime').style.display = "none";
        document.getElementById('recurringtab').style.background = "#E8E8E8";
        document.getElementById('onetimetab').style.background = "#c3c3c3";
    }
</script>

<br/>

<div id="radio">
    <a id="onetimetab" href="#" onclick="showOneTime();" style="float:left;padding:10px 15px;background:#E8E8E8;border:1px solid #C7CDCE;border-bottom:none;margin-right:1px;text-decoration:none">One Time Donation</a>
    <a id="recurringtab" href="#" onclick="showRecurring();" style="float:left;padding:10px 15px;background:#c3c3c3;border:1px solid #C7CDCE;border-bottom:none;margin-right:1px;text-decoration:none">Recurring Donation</a>
</div>
<div style="clear:both;"></div>

<div style="box-shadow: 0px 2px 3px #888; max-width: 800px; padding-left: 3em">

<div id="recurring" style="display: none; ">
<p style="margin-left: 1em; padding-top: 1em;">
<a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=9"><img src="images/Jtg.png" class="noborder" style="float: right; margin: 0px; background-image: none; border: 0; padding-top: 0" alt="Join the Game and support KDE" /></a>
We have two different ways to do recurring donations:
</p>
<ul>
<li>For <em>individuals</em>, the <a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=9">Join the Game</a> initiative.</li>
<li>For <em>corporations</em>, the <a href="http://ev.kde.org/getinvolved/supporting-members.php">Supporting Membership Program</a>.</li>
</ul>
</div>

<div id="onetime" style="display: inline;">
<p style="margin-left: 1em; margin-bottom: 0px;">
<form style="border: none; box-shadow: none; margin-top: 0; padding-top: 1em;" action="https://www.paypal.com/cgi-bin/webscr" method="post" onsubmit="return amount.value >= 2 || window.confirm('<?php print i18n_var("Your donation is smaller than %1€. This means that most of your donation\\nwill end up in processing fees. Do you want to continue?", 2);?>');">
  <input type="hidden" name="business" value="kde-ev-paypal@kde.org" />
 <input type="hidden" name="cmd" value="_donations" />
 <input type="hidden" name="lc" value="GB" />
 <input type="hidden" name="item_name" value="KDE e.V." />
 <input type="hidden" name="item_number" value="Donation to KDE e.V." />
 <input type="hidden" name="currency_code" value="EUR" />
 <input type="hidden" name="custom" value="<?php print "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]/donation_button"; ?>" />
 <input type="hidden" name="cn" value="Enter your name or other publically listed donation message" />
 <input type="hidden" name="cbt" value="Return to www.kde.org" />
 <input type="hidden" name="return" value="https://kde.org/community/donations/thanks_paypal.php" />
 <input type="hidden" name="notify_url" value="https://kde.org/community/donations/notify.php" />
 <input type='text' name="amount" value='30' style="vertical-align:middle;text-align:right;" size="4" /> €
 <input type="image" name="submit" src="paypal_donate.gif" alt="PayPal" style="vertical-align:middle;" />
</form>
</p>
<p style="margin-left: 1em; padding-bottom: 1em">
<a href="previousdonations.php">Previous donations page</a>
</p>

</div>

<div style="clear:both;"></div>


</div>

<p style="margin-top: 1em;">
<a href="others.php">Other ways to donate</a>
</p>

</main>
<?php
  require('../../aether/footer.php');
